
log.debug("Starting image_to_hl7_order")
LOAD('common.groovy')
LOAD('normalization.groovy')

initialize('ORM', 'O01', '2.3')

import com.karos.rtk.common.HL7v2Date
import org.joda.time.DateTime

set('MSH-7', HL7v2Date.format(new DateTime(), HL7v2Date.DateLength.MILLISECOND3))

/*
 * PID segment
 */
set('PID-3-1', get(PatientID))

def name = get(PatientName)
log.debug("name = '{}'", name);

if (name != null) {
    name = name.split('\\^')
    set('PID-5-1', name[0])

    if (name.length > 1) {
        set('PID-5-2', name[1])
    }

    if (name.length > 2) {
        set('PID-5-3', name[2])
    }
}

set('PID-7', get(PatientBirthDate))
set('PID-8', get(PatientSex))

set('PID-19', get(MedicalRecordLocator))

/*
 * PV1 segment
 */
set('PV1-2',    'O')
set('PV1-11-1', 'EXTIMPRT')
set('PV1-11-2', 'ERI')

/*
 * ORC
 */
set('ORC-1', 'SC')
//set('ORC-2', need to set to order placer/order number
set('ORC-3', get(AccessionNumber))
set('ORC-5', 'IP')

/*
 * OBR
 */
set('OBR-2', get(AccessionNumber))
set('OBR-3', get(AccessionNumber))

// Hack! We need the original modality, but foreign_image_localizer.groovy
// overwrites it with the normalized modality.  It stashes the original for
// us here:
def originalModality = get(ErrorComment)
def studyDesc = get(StudyDescription)
def (procCode, procDesc) = Normalization.getNormalizedCodeAndDesc(originalModality, studyDesc)

if (procDesc == null) {
    log.warn("Unable to find normalized procedure description based on modality '{}' " +
             " and study description '{}'. Not normalizing.", originalModality, studyDesc)
} else {
    set('OBR-4-2', procDesc)
}

if (procCode == null) {
    log.warn("Unable to find normalized procedure code based on modality '{}'. " +
             "Not normalizing.", originalModality)
} else {
    set('OBR-4-1', procCode)
}

set('OBR-14',   get(StudyDate))
set('OBR-16-1', 'EXTERNAL')
set('OBR-16-2', 'External,Physician')
set('OBR-18',   get(AccessionNumber))
set('OBR-25',   'R')

def date = getDate(StudyDate, StudyTime)
if (date != null) {
    set('OBR-27-4', HL7v2Date.format(date, HL7v2Date.DateLength.MILLISECOND3))
    set('OBR-36', HL7v2Date.format(date, HL7v2Date.DateLength.MILLISECOND3))
}

set('OBR-31-4', 'Imported Images')
set('OBR-32-1', 'EXTERNAL')
set('OBR-32-2', 'External,Physician')
set('OBR-33-1', 'EXTERNAL')
set('OBR-33-2', 'External,Physician')

/*
 * ZDS
 */
output.getMessage().addNonstandardSegment('ZDS')
set('ZDS-1', get(StudyInstanceUID))
