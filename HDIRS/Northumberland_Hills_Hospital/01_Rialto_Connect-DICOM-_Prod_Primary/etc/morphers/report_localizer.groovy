log.debug("Starting $this")

LOAD('common.groovy')
LOAD('normalization.groovy')

context = Contexts.getContext(config.context)
if (context == null) {
    log.warn("Context is unknown from id: {}. Not localizing this order. " +
        "Please correct either the connect ContextID configuration or common.groovy",
        config.context)
    return false
}

import com.karos.rtk.common.HL7v2Date
import org.joda.time.DateTime

// TODO: deduplicate with image_to_hl7_order.groovy

// TODO: why is this necessary? doesn't Connect do this?
set('MSH-7', HL7v2Date.format(new DateTime(), HL7v2Date.DateLength.MILLISECOND3))

/*
 * PID segment
 */
new Pids(originalInput, context, prefetchOrder).localizeHL7(output)

def name = get(PatientName)
if (name != null) {
    name = name.split('\\^').eachWithIndex { component, index ->
        // +1 because hl7 indexes are 1-based:
        set("PID-5-${index+1}", component)
    }
}

set('PID-7',  get(PatientBirthDate))
set('PID-8',  get(PatientSex))
set('PID-19', get(MedicalRecordLocator))

/*
 * PV1 segment
 */
set('PV1-2',    'O')
set('PV1-11-1', 'EXTIMPRT')
set('PV1-11-2', 'ERI')

/*
 * ORC
 */
set('ORC-1', 'RE')
set('ORC-5', 'CM')

/*
 * OBR
 */
set('OBR-2',    get(AccessionNumber))

//set('OBR-4',    get(StudyDescription))
// TODO: deduplicate this code with the order creator script:
// Hack! We need the original modality, but foreign_image_localizer.groovy
// overwrites it with the normalized modality.  It stashes the original for
// us here:
def originalModality = get(ErrorComment)
def studyDesc = get(StudyDescription)
def (procCode, procDesc) = Normalization.getNormalizedCodeAndDesc(originalModality, studyDesc)

if (procDesc == null) {
    log.warn("Unable to find normalized procedure description based on modality '{}' " +
             " and study description '{}'. Not normalizing.", originalModality, studyDesc)
} else {
    set('OBR-4-2', procDesc)
}

if (procCode == null) {
    log.warn("Unable to find normalized procedure code based on modality '{}'. " +
             "Not normalizing.", originalModality)
} else {
    set('OBR-4-1', procCode)
}

set('OBR-14',   get(StudyDate))
set('OBR-16-1', 'EXTERNAL')
set('OBR-16-2', 'External,Physician')
set('OBR-18',   get(AccessionNumber))
set('OBR-25',   'F')

def date = getDate(StudyDate, StudyTime)
if (date != null) {
    set('OBR-27-4', HL7v2Date.format(date, HL7v2Date.DateLength.MILLISECOND3))
}

set('OBR-31',   'Imported Images')
set('OBR-32-1', 'EXTERNAL')
set('OBR-32-2', 'External,Physician')
set('OBR-33-1', 'EXTERNAL')
set('OBR-33-2', 'External,Physician')

/*
 * ZDS
 */
output.getMessage().addNonstandardSegment('ZDS')
set('ZDS-1', get(StudyInstanceUID))

