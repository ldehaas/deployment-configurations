/* CFind Response RANK Filter
 *
 *   This morpher is part of the Fetch Prior Studies workflow only.
 *
 *   It takes THE ENTIRE SET of individual study responses (CFind Responses from the DIR) and optionally
 *   filters it out studies based on criteria, such as study age, or a maximum number of responses.
 *   This limits the number of studies that are preloaded into a local PACS as part of a Fetch Prior Studies workflow.
 *
 *   Common Uses for this Cfind Response Filter script can be:_
 *
 *       - Exclude studies that are NOT the X most recent studies (i.e. absolute number of responses, by age)
 */

LOAD("common.groovy")

log.debug("Starting cfind_response_rank_filter")

context = Contexts.getContext(config.context)
if (context == null) {
    log.warn("Context is unknown from id: {}. Fetching ALL priors. " +
        "Please correct either the connect ContextID configuration or common.groovy", config.context)
    return inputs
}

def modality = PrefetchRules.getOrderModality(order)
def limits = PrefetchRules.getPrefetchLimits(modality, context, log)

// Count backwards from today to years of prior studies required
def earliestStudyDate =  new org.joda.time.DateTime().minusYears(limits.maxAge)

log.debug("Incoming modality: '{}', limits: {}, earliest study date: {}",
            modality, limits, earliestStudyDate)

log.debug("Complete list of studies being examined:\n{}",
    inputs.collect({
        it.get(StudyInstanceUID) + ": " + it.getDate(StudyDate, StudyTime)
    }).join("\n"))

def validPriors = inputs.findAll {
    def studyDate = it.getDate(StudyDate, StudyTime)
    boolean valid = studyDate >= earliestStudyDate

    log.debug("Prior with date {} falls {} of maximum age {} for modality '{}'",
              studyDate,
              valid ? "inside" : "outside",
              earliestStudyDate,
              modality)

    return valid
}

validPriors = first(validPriors, limits.maxStudies)

log.debug("Studies after filtering:\n{}",
    validPriors.collect({
        it.get(StudyInstanceUID) + ": " + it.getDate(StudyDate, StudyTime)
    }).join("\n"))

return validPriors
