log.debug("morphing c-find response from PACS in getting MPI {}", input);


if (get(IssuerOfPatientID) == '' || get(IssuerOfPatientID) == null) {
  
  log.debug("IssuerOfPatientID in c-find response from pacs for getting MPI is missing, going to forge it!");
  set(IssuerOfPatientID, 'GE PACS MASTER PATIENT INDEX FOR NHC CMH');
}

