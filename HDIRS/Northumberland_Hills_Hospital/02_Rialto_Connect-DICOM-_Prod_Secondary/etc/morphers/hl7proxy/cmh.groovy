/*
 * Filter traffic not meant for this context.
 */

LOAD("../common.groovy")
LOAD("hl7_proxy_common.groovy")

return HL7Proxy.messageMatchesContext(input, "CMH", log)

