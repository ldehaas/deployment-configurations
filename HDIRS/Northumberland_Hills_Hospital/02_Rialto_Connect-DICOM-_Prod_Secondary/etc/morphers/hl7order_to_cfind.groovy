/*
 * Convert an order message into a cfind message.
 * get() works on the order message, while set() works on the
 * cfind message.
 *
 * The order is ignored if this script returns false,
 * e.g. no images will be fetched for the order.
 */

log.debug("Starting hl7order_to_cfind")
LOAD('common.groovy')

log.debug("Incoming HL7 Message:\n{}", input)

context = Contexts.getContext(config.context)
if (context == null) {
    log.warn("Context is unknown from id: {}. Dropping this order. " +
        "Please correct either the connect ContextID configuration or common.groovy", config.context)
    return false
}

def pid = get("PID-3-1")
log.debug("Found pid '{}'", pid)
if (pid == '' || pid == null) {
    log.warn("PID-3-1 is not populated for this order. Dropping this order.")
    return false
}

// Check for a valid health card number with 10-digits
def hcn = get("PID-19")
log.debug("Health Card Number is '{}'", hcn);

if (!(hcn ==~ '[0-9]{10}')) {
    log.warn("Health Card Number '{}' not valid. Ignoring this order.", hcn);
    return false
}

/* Construct the C-FIND */

// Matching keys: PatientID + IssuerOfPatientID

set(PatientID, pid)
// IssuerOfPID is hard coded to local issuer:
set(IssuerOfPatientID, context.PACSIssuer)

// Set the return keys
set(StudyInstanceUID, null)
// The following isn't necessary, but useful for debugging
set(AccessionNumber, null)

log.debug("Constructing C-FIND to DI-r from local HL7 order to find ALL studies for - PatientID '{}', with Health Card Number '{}'", pid, hcn)
