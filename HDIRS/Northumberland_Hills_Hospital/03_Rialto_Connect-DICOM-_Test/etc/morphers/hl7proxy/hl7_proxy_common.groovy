class HL7Proxy {
    static final String RISIdentifierField = 'PV1-39'

    /**
     * @returns whether or not the message should be forwarded for the given context
     */
    public static boolean messageMatchesContext(msg, contextId, log) {
        def context = Contexts.getContext(contextId)
        if (context == null) {
            log.error("Context is unknown from id: {}. Please configure your hl7 proxy scripts " +
                      "to match a context in common.groovy", contextId)
            return false
        }

        if (msg.get('MSH-18') == null) {
            // Weird to do this here, since this method is just evaluating the message,
            // but it's the easiest way without modifying each site's morpher.  This
            // matches the default we use to interpret the message when receiving it
            // (see hl7 server configuration) but is needed in the message when serializing
            // it to send it on properly:
            msg.set('MSH-18', '8859/1')
            log.trace('Set default character encoding of incoming message to 8859/1')
        }

        def site = msg.get(RISIdentifierField)
        def matches = site == context.RISIdentifier

        log.debug("Expected {}: {} {} actual: {}. {} message to context {}.",
            RISIdentifierField,
            context.RISIdentifier,
            matches ? "matches" : "doesn't match",
            site,
            matches ? "Forwarding" : "Not forwarding",
            contextId)

        if (!Contexts.isKnownRIS(site)) {
            log.warn("Unknown RIS ('{}' in {}) sent message. Expected prefetch may not occur!\n{}",
                     site, RISIdentifierField, msg)
        }

        return matches
    }
}

