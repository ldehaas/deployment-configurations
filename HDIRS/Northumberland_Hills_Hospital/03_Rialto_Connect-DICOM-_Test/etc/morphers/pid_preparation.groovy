/* pid preparation morpher to change the issuer of patient id from the DIR
*  to match what is on the local PACS.  So 'NHH' should be morphed to "NORTHUMBERLAND HILLS HOSPITAL"
*  and 'CMH' should be morphed to "CAMPBELLFORD MEMORIAL HOSPITAL"
*/

log.debug("pid preparation morpher started on {}", input);

def changeIssuerIfRequired(o) {
  if (o.get(IssuerOfPatientID) == 'NHH') {
    o.set(IssuerOfPatientID, 'NORTHUMBERLAND HILLS HOSPITAL');
  }
  if (o.get(IssuerOfPatientID) == 'CMH') {
    o.set(IssuerOfPatientID, 'CAMPBELLFORD MEMORIAL HOSPITAL');
  }
}

// change IssuerOfPatientID
changeIssuerIfRequired(input);

// change IssuerOfPatientIDSequence
input.get(OtherPatientIDsSequence).each {
    changeIssuerIfRequired(it);
}

log.debug("for collecting local pids morphed sop to {}", input);

