
//
// common.groovy - Lower-level include of common utility pieces
//

class Constants {
    static final UnwantedDomains = []

    // Global Issuer of PID that goes with the top-level Patients
    //static final GlobalIssuerOfPID = "GLOBALDOMAIN"    // PRODUCTION ONLY
    static final GlobalIssuerOfPID = "HDIRS"             // TEST ONLY - historical quirk

    // A PACS can not typically understand a foreign Presentation State
    // or Structured Report.  In addition, if these came first and we
    // used them to localize, we'd get the wrong procedure code:
    static final UnwantedModalities = ["PR", "SR", "KO"]

    static final RegionalDomain = "GE PACS MASTER PATIENT INDEX FOR NHC CMH"
    static final RegionalPidLocation = "PID-4"
}

class Contexts {

    /**
     * If true, the contexts in the PACS cannot see eachother's images and
     * we should fetch each study into each context.  In this case the hash
     * prefix should also be set.
     *
     * If false, they can see eachother's images.  We should not fetch any
     * studies that are local to any of the contexts.  Hashing is unnecessary.
     *
     * See Context.isLocal below.
     */
    public static final CONTEXTS_ARE_ISOLATED = false

    // Add one entry per context here.  The key should
    // correspond to the ContextID configuration parameter.
    // Set the HashPrefix for multi-context sites and leave
    // it null for single-context deployments.
    private static final ALL_CONTEXTS = [
        "NHH": new Context(
            RialtoRetrieveAE:       "NHH_RIALTO_TEST",
            RISIdentifier:          "NHC",
            PACSIssuer:             "NHH"),
        "CMH": new Context(
            RialtoRetrieveAE:       "CMH_RIALTO_TEST",
            RISIdentifier:          "CMH",
            PACSIssuer:             "CMH"),
    ]

    /**
     * Returns the context configuration for the given contextId
     * @return null if the context id isn't known
     */
    public static Context getContext(contextId) {
        // single context case, context id might not be configured
        if (contextId == null && ALL_CONTEXTS.size() == 1) {
            // return the only value:
            return ALL_CONTEXTS.findResult { it.value }
        }

        // If this returns null, we're almost certainly misconfigured
        // so we should blow up.  However, the best way to do that might
        // be to have the caller simply return false to drop the current
        // data, so we have to let the caller handle it.
        return ALL_CONTEXTS[contextId]
    }

    public static boolean isKnownRIS(risId) {
        return ALL_CONTEXTS.find({ it.value.RISIdentifier == risId }) != null
    }
}

/**
 * Encapsulates a dicom object to provide easy access to
 * the patient identifiers.
 */
class Pids {
    public static final HCN_PATTERN = /[0-9]{10}/

    private issuerToPid = [:]
    private sourceDomain
    private sop
    private context

    public Pids(sop, context, order=null) {
        this.sop = sop
        this.context = context

        // null check probably not necessary, but it's a safety feature for
        // when we might be using this on originalInput
        if (sop != null) {
            sourceDomain = sop.get(IssuerOfPatientID)
            issuerToPid[sourceDomain] = sop.get(PatientID)

            sop.get(OtherPatientIDsSequence).each {
                issuerToPid[it.get(IssuerOfPatientID)] = it.get(PatientID)
            }
        }

        if (order != null) {
            // bit of a hack because we don't have a front-end morpher
            // adding domains to the prefetch order messages:
            def regional = order.get(Constants.RegionalPidLocation)
            if (regional != null) {
                issuerToPid[Constants.RegionalDomain] = regional
            }
        }
    }

    public boolean isSourceUnwantedDomain() {
        return Constants.UnwantedDomains.contains(sourceDomain)
    }

    public boolean isLocal() {
        return context.isLocal(sourceDomain)
    }

    /**
     * Find a patient identifier in one of the local domains.
     */
    public getLocalPid() {
        // first try for the local domain
        def bestLocal = issuerToPid[context.PACSIssuer]
        if (bestLocal != null) {
            return [bestLocal, context.PACSIssuer]
        }

        // Odd historical behaviour: falling back on any local
        // domain.  Is this safe?  Is it required?
        Contexts.ALL_CONTEXTS.findResult { id, context ->
            def localPid = issuerToPid[context.PACSIssuer]
            if (localPid != null) {
                return [localPid, context.PACSIssuer]
            }
            return null
        }
    }

    /**
     * Get the health card number, aka the pid in the global
     * domain.
     */
    public getHealthCardNumber(validate = true) {
        def hcn = issuerToPid[Constants.GlobalIssuerOfPID]
        if (validate && !(hcn ==~ HCN_PATTERN)) {
            return null
        }
        return hcn
    }

    /**
     * Return the source domain, or null if it is not known.
     */
    public getSourceDomain() {
        return sourceDomain
    }

    /**
     * Get the local pid, with fallbacks trying to make one up if the
     * local one isn't known.  The first fallback is the health card
     * number and the second is to prefix the source pid with the source
     * domain.
     *
     * @return [pid, issuer] or null if none of the fallbacks works
     */
    public getLocalPidWithFallbacks() {
        def local = getLocalPid()
        if (local != null) {
            return local
        }

        def hcn = getHealthCardNumber()
        if (hcn != null) {
            // KHC995.  This allows PACS users to predict the
            // patient id, even if there isn't a local record
            // already:
            return ["HN" + hcn, Constants.GlobalIssuerOfPID]
        }

        if (sourceDomain != null) {
            return [sourceDomain + "_" + issuerToPid[sourceDomain], this.context.PACSIssuer]
        }

        return null
    }

    public getPid(domain) {
        return issuerToPid[domain]
    }

    /**
     * Install the given pid as the local one and clear out
     * extra data that the PACS might not want to see.
     */
    public localize(localPid) {
        sop.set(PatientID, localPid[0])
        sop.set(MedicalRecordLocator, getHealthCardNumber())
        sop.remove(IssuerOfPatientID)
        sop.remove(OtherPatientIDsSequence)
    }

    public localizeHL7(msg) {
        def localPid = getLocalPidWithFallbacks()
        if (localPid != null) {
            localPid = localPid[0]
        }

        def regionalPid = getPid(Constants.RegionalDomain)
        if (regionalPid == null) {
            regionalPid = localPid
        }

        msg.set("PID-3", localPid)
        msg.set(Constants.RegionalPidLocation, regionalPid)
    }
}

class PrefetchRules {
    // modality -> limits
    // KHC12802 - Updated Prefetch rules to return max 10 OBSP studies within 10 years
    static final Map StandardPrefetchLimits = [
        "OBSP": [maxAge: 10, maxStudies: 10]
        // KHC14638 - Changed to MaxAge 10 maxStudies 10 for all prefetches
    ].withDefault{ [maxAge: 10, maxStudies: 10] }
    // non-standard modality found in order message -> standard modality
    static final Map StandardModalities = [
        "MAM": "MG"
    ].withDefault{ it }

    static getPrefetchLimits(modality, context, log) {
        def limits = context.PrefetchLimits
        if (limits == null) {
            limits = StandardPrefetchLimits
        }

        return limits[modality]
    }

    static getOrderModality(order) {
        def orderModality = order != null ? order.get("OBR-4") : null
        return StandardModalities[orderModality]
    }
}

/**
 * Representation of a single context.  See Contexts class above for
 * a set of instances representing the contexts for this deployment.
 */
class Context {
    /** AE title of Rialto's CMove interface */
    String RialtoRetrieveAE

    String InstitutionName

    /** Assigning Authority for the local PACS */
    String PACSIssuer

    /** The calling ae title used by the PACS for query/retrieve.
     * Get this from the connectivity spreadsheet or the SAD */
    String PACSCallingAE

    /** Context ID as known by the PACS; returned as IssuerOfPatientID
     * in cfind queries; used to identify contexts in demographics queries */
    String ContextID

    /** The HL7 sending facilities for the local RIS.
     * The first one will be used when we need to set the value somewhere. */
    List RISSendingFacility

    /** The HL7 sending applications for the local RIS.
     * The first one will be used when we need to set the value somewhere. */
    List RISSendingApplication

    /** Similar to the sending application/facility, this identifies the site
     * an order belongs to, except that it comes from a different field. */
    String RISIdentifier

    String HashPrefix

    /** modality -> {maxAge: ?, maxStudies: ?} */
    Map PrefetchLimits

    public String localizeUID(uid, hash) {
        if (HashPrefix == null || uid == null) {
            return uid
        }

        return hash(HashPrefix, uid)
    }

    public String unlocalizeUID(uid, unhash) {
        //  if we're not hashing, we're also not unhashing:
        if (HashPrefix == null) {
            return uid
        }

        return unhash(uid)
    }

    /**
     * @return the preferred sending application for this context's RIS
     */
    public String preferredRISSendingApplication() {
        return RISSendingApplication.isEmpty() ? null : RISSendingApplication[0]
    }

    /**
     * @return the preferred sending facility for this context's RIS
     */
    public String preferredRISSendingFacility() {
        return RISSendingFacility.isEmpty() ? null : RISSendingFacility[0]
    }

    /**
     * @return if the issuer is local to the PACS
     */
    public boolean isLocal(issuer) {
        if (Contexts.CONTEXTS_ARE_ISOLATED) {
            return issuer == PACSIssuer
        } else {
            return Contexts.ALL_CONTEXTS.any { id, c -> c.PACSIssuer == issuer }
        }
    }
}