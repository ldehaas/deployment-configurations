
class Normalization {
    /**
     * Each foreign modality has a list of rules containing keywords.  The
     * first keywords that match anything in the study description is
     * the rule to use.  The last rule should always have an empty
     * list of keywords, making it the default for that modality.
     *
     * Most modalities will map to the same normalized value, but we keep
     * them explicit because there are a few cases where they don't.
     */
    static final map = [
        'MG': [
            [ keywords: ["mammogram", "diagnostic", "screening", "obsp", "mammotome"],
              normalizedModality: "MG",
              procCode: "XMAM1",
              procDesc: "Mammo Reference"
            ],

            [ keywords: ['breast', 'needle', 'specimen', 'cone', 'mag',
                         'mammography', 'mammary', 'ductography', 'stereotactic', 'biopsy'],
              normalizedModality: "MG",
              procCode: "XMAM2",
              procDesc: "Mammo Reference Procedure",
            ],

            [ keywords: [],
              normalizedModality: "MG",
              procCode: "XMAM3",
              procDesc: "Mammo Reference Other",
            ],
        ],

        'NM': [
            [ keywords: ['pet'],
              normalizedModality: "NM",
              procCode: "1NM1",
              procDesc: "NM Pet Reference",
            ],
              
            [ keywords: [],
              normalizedModality: "NM",
              procCode: "1NM2",
              procDesc: "NM Reference",
            ],
        ],

        'XA': [
            [ keywords: [],
              normalizedModality: "XA",
              procCode: "XAN1",
              procDesc: "IR Reference",
            ],
        ],

        'US': [
            [ keywords: ['axilla', 'breast'],
              normalizedModality: "US",
              procCode: "XUS1",
              procDesc: "US Reference Breast",
            ],

            [ keywords: [],
              normalizedModality: "US",
              procCode: "XUS2",
              procDesc: "US Reference Body",
            ],
        ],

        'CR': [
            [ keywords: ['chest', 'abdomen', 'pacemaker', 'fluoroscopy', 'ribs', 'sternum'],
              normalizedModality: "CR",
              procCode: "XCR1",
              procDesc: "CR Reference Chest/Abdomen",
            ],

            [ keywords: ['ankle', 'calcaneus', 'femur', 'foot', 'knee', 'leg', 'pelvhip',
                         'penis', 'knee', 'tibia', 'fibula', 'toe'],
              normalizedModality: "CR",
              procCode: "XCR2",
              procDesc: "CR Reference Lower Extremity",
            ],

            [ keywords: ['cine', 'palatogram', 'enema', 'cystogram', 'urethrogram',
                         'bowel', 'defecogram', 'endoscopic', 'retrograde',
                         'cholangiopancreaton', 'fistulogram', 'sinogram', 'fluoroscopy',
                         'tilt', 'table', 'hysterosalpinogram', 'loopogram', 'pouchogram',
                         'lymphangiogram', 'oesophagus', 'operative', 'cholangiogram',
                         'retrograde', 'pyelogram', 'basket', 'extraction', 'sialogram',
                         'sniff', 'gi', 'ventriculoperitoneal', 'shunt', 'fluoroscopic',
                         'swallowing', 'urodynamics'],
              normalizedModality: "CR",
              procCode: "XCR3",
              procDesc: "CR Reference GI/GU",
            ],

            [ keywords: ['ac', 'joints', 'bone age', 'clavicle', 'elbow', 'finger',
                         'humerus', 'radius', 'ulna', 'scapula', 'shoulder',
                         'sternoclavicular', 'wrist', 'hand'],
              normalizedModality: "CR",
              procCode: "XCR4",
              procDesc: "CR Reference Upper Extremity",
            ],

            [ keywords: [],
              normalizedModality: "CR",
              procCode: "XCR5",
              procDesc: "CR Reference Body",
            ],
        ],

        'MR': [
            [ keywords: ['breast', 'cardiac', 'chest'],
              normalizedModality: "MR",
              procCode: "XMR1",
              procDesc: "MRI Reference Breast",
            ],

            [ keywords: ['ac', 'joints', 'bone age', 'clavicle', 'elbow', 'finger',
                         'humerus', 'radius', 'ulna', 'scapula', 'shoulder',
                         'sternoclavicular', 'wrist', 'hand'],
              normalizedModality: "MR",
              procCode: "XMR2",
              procDesc: "MRI Reference Upper Extremity",
            ],

            [ keywords: ['brain', 'head', 'neck', 'spine', 'temporomandibular', 'joint'],
              normalizedModality: "MR",
              procCode: "XMR3",
              procDesc: "MRI Reference Head/Neck/Spine",
            ],

            [ keywords: ['ankle', 'calcaneus', 'femur', 'foot', 'knee', 'leg', 'pelvhip',
                         'pelvis', 'hip', 'knee', 'tibia', 'fibula', 'toe'],
              normalizedModality: "MR",
              procCode: "XMR4",
              procDesc: "MRI Reference Lower Extremity",
            ],

            [ keywords: [],
              normalizedModality: "MR",
              procCode: "XMR5",
              procDesc: "MRI Reference Body",
            ],
        ],

        'CT': [
            [ keywords: ['abdomen', 'chest', 'cardiac', 'pelvis'],
              normalizedModality: "CT",
              procCode: "XCT1",
              procDesc: "CT Reference Body",
            ],

            [ keywords: ['ac', 'joints', 'bone age', 'clavicle', 'elbow', 'finger',
                         'humerus', 'radius', 'ulna', 'scapula', 'shoulder',
                         'sternoclavicular', 'wrist', 'hand'],
              normalizedModality: "CT",
              procCode: "XCT2",
              procDesc: "CT Reference Upper Extremity",
            ],

            [ keywords: ['brain', 'head', 'neck', 'spine'],
              normalizedModality: "CT",
              procCode: "XCT3",
              procDesc: "CT Reference Neck/Head/Spine",
            ],

            [ keywords: ['ankle', 'calcaneus', 'femur', 'foot', 'knee', 'leg', 'pelvhip',
                         'pelvis', 'hip', 'knee', 'tibia', 'fibula', 'toe'],
              normalizedModality: "CT",
              procCode: "XCT4",
              procDesc: "CT Reference Lower Extremity",
            ],

            [ keywords: [],
              normalizedModality: "CT",
              procCode: "XCT5",
              procDesc: "CT Reference Other",
            ],
        ],

        'OT': [
            [ keywords: ['bone', 'bmd'],
              normalizedModality: "NM",
              procCode: "1NM3",
              procDesc: "NM Reference Bone Density",
            ],

            [ keywords: [],
              normalizedModality: "CR",
              procCode: "XCR6",
              procDesc: "CR Reference Other",
            ],
        ],
    ]

    static final defaultRule = 
            [ keywords: [],
              normalizedModality: "CR",
              procCode: "XCR7",
              procDesc: "CR Reference Other",
            ]

    static {
        // add entries for cases when multiple foreign modalities map to
        // the same rules:
        ["DX", "DR"].each {
            map[it] = map["CR"]
        }
    }

    private static findRule(foreignModality, studyDesc) {
        def rules = foreignModality == null ? null : map[foreignModality.toUpperCase()]

        if (rules != null) {
            return rules.find { rule ->
                rule.keywords.isEmpty() ||
                    (studyDesc != null &&
                     rule.keywords.find({ studyDesc.toLowerCase().contains(it) }) != null)
            }
        }

        return defaultRule
    }

    /**
     * Find the normalized modality based on the foreign modality and
     * study description.
     */
    public static getNormalizedModality(foreignModality, studyDesc) {
        // TODO: support composite foreign modalities
        // Would require customer to define rules for which is authoritative
        return findRule(foreignModality, studyDesc).normalizedModality
    }

    /**
     * Find the normalized procedure code and description based on the
     * foreign modality and study description.
     */
    public static getNormalizedCodeAndDesc(foreignModality, studyDesc) {
        def rule = findRule(foreignModality, studyDesc)
        return [rule.procCode, rule.procDesc]
    }
}

