<?xml version="1.0" encoding="UTF-8"?>
<!-- Configuration file for Rialto Connect -->

<config>
    <!-- ####################### -->
    <!--  VARIABLE DECLARATIONS  -->
    <!-- ####################### -->

    <!-- Be sure to copy sample scripts into etc/morphers folder and
         update the ${morphers} variable below. -->
    <var name="morphers">${rialto.rootdir}/etc/morphers/sample</var>

    <var name="AuditRecordRepositoryPort">4000</var>
    <var name="InternalAPIPort">8081</var>
    <var name="WebUIPort">8080</var>

    <include location="local/cluster.xml" />

    <!-- ####################### -->
    <!--         DEVICES         -->
    <!-- ####################### -->

    <!-- Defines hosts and ports for the DIR -->
    <device type="dicom" id="DIR1">
        <host>hostname</host>
        <port>11112</port>
        <aetitle>AETitle1</aetitle>
    </device>
    
    <device type="dicom" id="DIR2">
        <host>hostname</host>
        <port>11112</port>
        <aetitle>AETitle2</aetitle>
    </device>

    <!-- Defines host and port for the local PACS -->
    <device type="dicom" id="PACS">
        <host>hostname</host>
        <port>11112</port>
        <aetitle>PACS</aetitle>
    </device>

    <device type="audit" id="audit">
        <host>localhost</host>
        <port>${AuditRecordRepositoryPort}</port>
    </device>

    <!-- ####################### -->
    <!--         SERVERS         -->
    <!-- ####################### -->

    <!-- Specify port used to listen for DICOM C-Find and C-Move -->
    <server id="dicomserver" type="dicom">
        <port>4104</port>
    </server>

    <!-- Specify port used to listen for HL7 orders for prefetch -->    
    <server id="hl7srv" type="hl7v2">
        <port>5555</port>
    </server>
    
    <server type="http" id="internal-http">
        <port>${InternalAPIPort}</port>
        <!-- in production, constraint this to accept connections from
             localhost only, for security reasons -->
    </server>

    <!-- ####################### -->
    <!--  SERVICE CONFIGURATION  -->
    <!-- ####################### -->

    <!-- Specify where to store transaction records
         this has to be the first service -->
    <service id="txn-records" type="txn-records">
        <config>
            <prop name="StorageDir">${rialto.rootdir}/var/txn-records</prop>
        </config>
    </service>
    
    <!-- Audit Record Repository Service
         this has to be the second service -->
    <service id="arr" type="arr">
        <server idref="internal-http" name="arr-api">
            <url>/auditRecords/*</url>
        </server>

        <config>
            <prop name="IndexDir" value="${rialto.rootdir}/var/arr/index" />
            <prop name="IndexBackupDir" value="${rialto.rootdir}/var/arr/index-backup" />
            <prop name="ArchiveDir" value="${rialto.rootdir}/var/arr/archive" />
            <prop name="TempDir" value="${rialto.rootdir}/var/arr/temp" />
            <prop name="Listener" value="udp:${AuditRecordRepositoryPort},tcp:${AuditRecordRepositoryPort}" />
            
            <!-- for replication
            <prop name="ForwardDestination"
                  value="${ClusterPartnerHost}:${AuditRecordRepositoryPort}" />
            <prop name="ForwarderCheckpointFolder"
                  value="${rialto.rootdir}/var/arr/checkpoints" />
            -->
        </config>
    </service>
    
    <!-- Connect XDS Service -->
    <service id="connect" type="connect-xds">
        <device idref="PACS" name="PACSStorage" />

        <server idref="dicomserver" name="cstore" />
        <server idref="dicomserver" name="cfind" />
        <server idref="dicomserver" name="cmove" />
        <server idref="hl7srv" name="fps" />

        <config>
            <!-- query and retrieve -->
            <prop name="CFindToFindDocumentsMorpher">${morphers}/cfind_to_find_documents.groovy</prop>
            <prop name="RepositoryUIDToDIRAETitleMap">
                1.2.3.4.5.6=AE_TITLE1
                1.2.3.4.5.8=AE_TITLE2
            </prop>
            
            <prop name="CFindResponseFilter">${morphers}/cfind_response_filter.groovy</prop>
            <!-- ingestion -->
            <prop name="ForeignImageMorpher">${morphers}/foreign_image_morpher.groovy</prop>
    
            <!-- prefetch -->
            <prop name="HL7ToCFindMorpher">${morphers}/hl7_to_cfind_morpher.groovy</prop>
            
            <!-- connect-xds has many more features / tweaks. see the docs
                 in sites for more information. -->
        </config>
    </service>



    <!-- Rialto Navigator GUI -->
    <!-- Off by default; may be useful for debugging
    
    <device type="dicom" id="RIALTO">
        <host>localhost</host>
        <port>4104</port>
        <aetitle>RIALTO</aetitle>
    </device>
    
    <server type="http" id="navigator-http" default="true">
        <port>${WebUIPort}</port>
    </server>

    <service id="navigator" type="navigator">
        <device idref="RIALTO" name="proxyDIR" />

        <server idref="navigator-http" name="navigator-war">
            <url>/</url>
        </server>

        <config>
            <prop name="ArrURL">http://localhost:${InternalAPIPort}</prop>
        </config>
    </service>
    -->

    <!-- New Rialto Navigator GUI, Usermanagement services, and their configuration -->
    <include location="navigator/navigator.xml" />

</config>
