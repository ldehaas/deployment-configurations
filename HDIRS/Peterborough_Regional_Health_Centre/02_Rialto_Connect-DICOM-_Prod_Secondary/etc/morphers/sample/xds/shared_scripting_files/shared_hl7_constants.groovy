class SharedHl7Constants {
    static final supported_ADT_subtypes = ['A01','A04','A05','A08','A31']
}

class QuebecHL7Validation {
    /**
     * This function validates that the format of a passed potential RAMQ actually
     * conforms to the RAMQ creation algorythm. This function is needed in order
     * to protect the PIX lookup mechanism, which is what eventually is used to
     * query the XDS Registry.
     *
     * Please note that the function actually evaluates for INVALID data,
     * meaning that it will return true if the RAMQ passed is invalid.  This is
     * coded in this way for better readability in the calling script.
     *
     * The function follows the RAMQ formula, which is as follows:
     *      the first three letters of the last name;
     *      the first letter of the first name;
     *      the last two digits of the year of birth;
     *      the month of birth (to which 50 is added to indicate female);
     *      the day of birth;
     *      a two digit administrative code used by the Rée.
     */
    public static final RAMQformatIsInvalid(ramq) {
        //                 name      year    month, maybe +50       day            admin code
        return ramq !=~ /[A-Za-z]{4}[0-9]{2}([05][1-9]|[16][0-2])([0-2][0-9]|3[01])[0-9]{2}/
    }
}
