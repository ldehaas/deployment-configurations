/* CFind Response Filter
 *
 * Fetch Priors workflow only.
 *
 * Runs once per study located in the DIRs.  Should not modify the input.
 * Should only optionally return false to drop the current study.
 */

LOAD("shared_scripting_files/shared_constants.groovy")

log.debug("\n\n**Starting Fetch Priors General FILTER Script**\n")
log.debug("This is the received object for this script:\n{}", input)
log.debug("The current Retreive AETitle from the the DIR is '{}'.", get(RetrieveAETitle))
log.debug("The Study Instance UID, as retreived from the DIR was '{}'.", get(StudyInstanceUID))

 /* Ignore local studies */
def sourceIssuer = get(IssuerOfPatientID)
log.debug("The Issuer of Patient ID, as retreived from the XDS Reg / DIR (check preceding lines) was '{}'.", sourceIssuer)

if (SharedConstants.Local_PACS_Issuer_of_Patient_ID == sourceIssuer) {
    log.debug("This is considered a LOCAL study")
    log.debug("This is a Local Study. Discarding this study. You should see *nothing* in the object output for this study. " +
              "The Event Viewer should say '(Ignored)'.")
    return false

} else {
    log.debug("Processing study, as it is deemed a FOREIGN PRIOR from the issuer: '{}'.", sourceIssuer)
}

log.debug("\n\n**Ending Fetch Priors General FILTER Script**\n")
