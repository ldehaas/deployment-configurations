
/**
 * this script localizes foreign images (dicom headers) so that they can be
 * ingested by the PACS
 *
 * this script can return false to filter (drop) an image
 *
 * sometimes it's desirable to use this script for localizing both foreign
 * images and cfind responses so common logic can be shared
 */


/*
 * in the dicom environment, the input dicom header is directly from the DIR
 *
 * in the xds environment, the input dicom header is modified:
 *   - the PatientID and IssuerOfPatientID tag are overwritten with xds
 *     metadata's SourcePatientID, to indicate where the study is originated
 *   - the OtherPatientIDsSequence is added / updated to contain a pair of
 *     PatientID and IssuerOfPatientID with localized patient information
 *
 * the above two points apply to adhoc_cfind_resp_morpher as well
 */

// source pid / issuer usually identifies where the study is originated
//def sourcePid = get(PatientID)
//def sourceIssuer = get(IssuerOfPatientID)

// script can throw exception and cause a cstore failure
//if (sourcePid == null || sourceIssuer == null) {
//    throw new UnsupportedOperationException(...);
//}

// there are usually a few pieces of information to be localized
// - pid
// - accession number to avoid collsion
// - study description (procedure codes)


//set(PatientID, localizedPid)

// usually PACS do not like the following tags
//remove(OtherPatientIDsSequence)
//remove(IssuerOfPatientID)
//prepend(AccessionNumber, sourceIssuer+"_")
//set(StudyDescription, mappedProcedureCodes)

/*
 * localDemographics is a list of dicom objects representing cfind results from
 * local pacs.  If such a query was configured, these should contain the local
 * demographics for the patient as they exist in the PACS and may be used to
 * override the values in the SOP so as not to confuse the PACS.
 */
if (localDemographics != null && !localDemographics.isEmpty()) {
	set(PatientName, localDemographics.get(0).get(PatientName))
} else {
	// Since there are no local demographics, just use what's already in the DICOM image.
}

/*
 * In the xds environment, xdsMetadata may contain the xds metadata representing
 * the study.
 */
if (xdsMetadata != null) {
    // Potential usage of the metadata.  Other uses might be pulling procedure
    // codes and such.
    put(StudyDescription, xdsMetadata.get(XDSTitle))
}
