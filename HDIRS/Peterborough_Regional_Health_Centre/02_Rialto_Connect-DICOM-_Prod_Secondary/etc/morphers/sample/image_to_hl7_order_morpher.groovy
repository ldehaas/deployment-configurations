
/*
 * this script creates an order from an image header
 *
 * this script is optional. only some PACS requires an order before ingesting
 * images
 *
 * this script can return false to signal no order is needed for a particular
 * study
 */

// initialize must be called first, with arguments MSH-9-1, MSH-9-2, MSH-12 
initialize( "ORM", "O01", "2.3" )

// to access the image header, for example
//get(PatientID)

// to set the hl7 order, for example
//set('PID-3', value)

// in general, the following needs to be present
set('PID-3', get(PatientID))

if (localDemographics != null && !localDemographics.isEmpty()) {
	set('PID-5', localDemographics.get(0).get(PatientName))
} else {
	set('PID-5', get(PatientName))
}

def accessionNumber = get(AccessionNumber)
set('OBR-2', accessionNumber)
set('ORC-2', accessionNumber)
set('ORC-3', accessionNumber)

// some systems also require procedure codes in OBR-4