
/*
 * this script localizes foreign reports so the local PACS / RIS can ingest it
 *
 * this script can optionally return false to signal a report should be dropped
 *
 * this script will work in a way similar to the foreign image localizer script,
 * except sets will be called on hl7 objects
 *
 * LOAD feature may come in handy to reduce duplicate logic. see documentation
 * for more details
 *
 */

// to read the foreign hl7 field:
//def var = get(string)
//where string is the hl7 field location, e.g. "OBR-3" or "PID-4-2-2"

// to read the foreign image header associated with this study
//def var = get(tag)
//where tag is a dicom tag, e.g. PatientID, IssuerOfPatientID

// to set an hl7 field
//set(string, value)
//string is once again the hl7 field location

if (localDemographics != null && !localDemographics.isEmpty()) {
	set(PatientName, localDemographics.get(0).get(PatientName))
} else {
	// Since there are no local demographics, just use what's already in the DICOM image.
}
