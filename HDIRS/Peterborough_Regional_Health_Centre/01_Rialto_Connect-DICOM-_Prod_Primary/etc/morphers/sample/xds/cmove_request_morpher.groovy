// Optionally and transparently modifies the incoming Study Instance UIDs to honor the original UIDs in a multi-context PACS

LOAD("shared_scripting_files/common_localization_routines.groovy")

def pacsStudyInstanceUID = get(StudyInstanceUID)
def studyInstanceUID = null

log.debug("Study Instance UID as received from the PACS is '{}'",
    pacsStudyInstanceUID)

studyInstanceUID = unhash(pacsStudyInstanceUID) 

if (studyInstanceUID == null) {
    log.error("Unexpected NULL Study Instance UID.  Dropping this C-Move request!! " +
        "Original Study Instance UID '{}'", pacsStudyInstanceUID) 
    return false

} else if (studyInstanceUID != pacsStudyInstanceUID) {
    log.info("PACS requesting C-MOVE for PACS Study Instance UID '{}' for " +
        "original (DI-r) Study Instance UID '{}'.", pacsStudyInstanceUID, studyInstanceUID)
    set(StudyInstanceUID, studyInstanceUID)

} else {
    // PACS had an unhashed Study Instance UID which would be the same as the DI-r would expect
    log.trace("The PACS passed a non-hashed Study Instance UID.  Respecting that value.")
    set(StudyInstanceUID, pacsStudyInstanceUID)
}
