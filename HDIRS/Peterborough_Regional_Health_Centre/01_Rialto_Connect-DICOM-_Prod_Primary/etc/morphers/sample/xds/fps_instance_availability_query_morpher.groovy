/*
 * Runs for Fetch Prior Studies
 * Modifies the Instance Availability C-FIND used to query the PACS to see if 
 * specific studies are already in the foreign studies cache.
 */

set(StudyInstanceUID, hash("1.2", get(StudyInstanceUID)))

log.debug("This is the outgoing Instance Availability Query C-FIND going to the PACS: {}", output)

