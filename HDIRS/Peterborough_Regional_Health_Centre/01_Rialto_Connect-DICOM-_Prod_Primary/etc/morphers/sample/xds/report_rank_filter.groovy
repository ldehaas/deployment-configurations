/* Report Rank Filter
 *
 * Runs during image ingestion.
 *
 * Receives a list of all reports that were converted from SRs and returns those
 * which should be forwarded.
 * May return a subset of the list or a single report.
 */

final dateAndTimeField = 'OBR-7'
final verificationField = 'OBX-11'

log.debug("Original number of ORUs converted for this study: {}", reports.size())

def logDates = { statusFunc, description, reports ->
    def selected = reports.findAll { report -> statusFunc(report.get(verificationField)) }
    def dates = selected.collect { report -> report.get(dateAndTimeField) }
    log.debug("There are {} {} reports with dates: {}", dates.size(), description, dates)
}
logDates({ status -> status == "F" }, "final", reports)
logDates({ status -> status != "F" }, "preliminary", reports)


// Sort the set of reports by report time
def sortedReports = reports.sort { report ->
    report.get(dateAndTimeField)
}

// Pass only the latest report to the PACS
lastReport = sortedReports.last()
log.debug("Passing 1 report with date '{}' and study instance uid '{}'", 
    lastReport.get(dateAndTimeField), lastReport.get("ZDS-1-1"))

return lastReport
