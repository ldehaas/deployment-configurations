/**
 * Called during ingestion, once an entire study has been received.  This script
 * will receive a list of all reports as ORU messages (created previously by the
 * sr_to_oru.groovy script) and should return the subset of those reports which
 * should be actually sent on.
 *
 * Multiple reports may be returned, in which case they will be forwarded in order.
 *
 * If this script is not configured, all reports will be forwarded.
 *
 * The only input to the script is a variable called "reports", which is a list
 * of hl7 messages and uses the usual hl7 get and set methods.  This script will
 * *not* run if no reports are received.
 *
 * This sample returns the latest report as stored in OBX-14 (presumably the
 * sr_to_oru script populated that from ContentDate and ContenetTime in the
 * SR version of the report)
 */

// sort the reports by the report time:
sorted = reports.sort { report ->
    report.get("OBX-14")
}

// take only the most recent:
return sorted.last()
