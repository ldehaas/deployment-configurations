/*
 * Morphs the dicom header image in the retrieve prior exam workflow.
 *
 * Rialto morphes the StudyInstanceUID to match what is in the PACS and passes down the header image to the morpher.
 * 
 * the following information is passed down to the morpher:
 *  - the header image (input)
 *  - the result of c-find response querying for study metadata (if any). passed as studyMetadataInPACS
 
 * the result of the c-find to PACS also is passed down to the morpher along the arriving dicom image.
 *
 * NOTE: absence of the morpher does not disable any part of the workflow
 */


import org.apache.commons.lang.StringUtils;
def parsePatientName(var) {
    String[] result = new String[5];
    for (int i = 0; i < 5; i++) {
        result[i] = null;
    }

    String full = var.get(PatientName);
    if (full == null) {
        return result;
    }

    // chop off sections after first '='
    int endOfLatin = full.indexOf('=');
    if (full.indexOf('=') > -1) {
        full = full.substring(0, endOfLatin);
    }

    // and split into components separated by ^
    String[] split = full.split("\\^");
    for (int i = 0; i < split.length && i < result.length; i++) {
        String component = split[i].trim();
        if (component.length() > 0) {
            result[i] = component;
        }
    }

    return result;
}




set(SkipFrameRangeFlag, "FEM")
remove(tag(0x6003, 0x0010))
remove(tag(0x6003, 0x1010))

// the FEM last name and PACS lastname are the same AND DOB is different: prefix the lastname with "FEM_"
patientDOBInPACS = studyMetadataInPACS.get(PatientBirthDate)
patientDOBInFEM = get(PatientBirthDate)

patientLastNameInFEM = (parsePatientName(input))[0];
patientLastNameInPACS = (parsePatientName(studyMetadataInPACS))[0];

log.debug("FEM: PatientLastName {}, PatientBirthDate {}", patientLastNameInFEM, patientDOBInFEM);
log.debug("PACS: PatientLastName {}, PatientBirthDate {}", patientLastNameInPACS, patientDOBInPACS );

if (StringUtils.equalsIgnoreCase(patientLastNameInFEM, patientLastNameInPACS) && patientDOBInFEM != patientDOBInPACS) {
	log.warn("patient date of birth in study {} is different from in PACS {}", patientDOBInFEM, patientDOBInPACS);
	set(PatientName, "FEM_" + get(PatientName))
	log.warn("setting the patient name to {}", get(PatientName));
}

log.debug("rp: received header image [{}]", input);
