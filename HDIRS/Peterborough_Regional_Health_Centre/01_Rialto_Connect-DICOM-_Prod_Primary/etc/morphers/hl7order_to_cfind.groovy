/*
 * Convert an order message into a cfind message.
 * get() works on the order message, while set() works on the
 * cfind message.
 *
 * The order is ignored if this script returns false,
 * e.g. no images will be fetched for the order.
 */

log.debug("Starting hl7order_to_cfind")
LOAD('common.groovy')

//Debugging Messages
log.debug("Incoming HL7 Message: \n {}", input)

def pid = get("PID-3-1")
log.debug("Found pid '{}'", pid)


// Check for a valid health card number with 10-digits
def healthCardNumber = get("PID-2")
log.debug("Health Card Number is '{}'", healthCardNumber);

if (healthCardNumber == null || !(healthCardNumber ==~ '[0-9]{10}') && !healthCardNumber.startsWith('0000000000') && !healthCardNumber.startsWith('1111111111')) {
    log.warn("Health Card Number '{}' not valid. Ignoring this order.", healthCardNumber);
    return false

}else{

// Construct the C-FIND
set(PatientID, healthCardNumber)
log.debug("INFO: Setting PatientID '{}' to HCN '{}'", PatientID, healthCardNumber);

// IssuerOfPID is hard coded to local issuer:
//set(IssuerOfPatientID, Constants.PrimaryLocalIssuerID)
set(IssuerOfPatientID, Constants.GlobalIssuerOfPID)

//set(StudyID, hcn)
//set(StudyIDIssuer, Constants.GlobalIssuerOfPID)

}

// Set the return keys
set(StudyInstanceUID, null)
set(ModalitiesInStudy, null)

// The following isn't necessary, but useful for debugging
set(AccessionNumber, null)

log.debug("Constructing C-FIND - PatientID, pid, Issuer Of Patient ID, AccessionNumber '{}', '{}', '{}','{}'",
          healthCardNumber, pid, IssuerOfPatientID, AccessionNumber);
