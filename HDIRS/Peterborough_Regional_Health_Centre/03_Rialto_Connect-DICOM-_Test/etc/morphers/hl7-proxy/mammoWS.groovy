/*
DBTBILNS        Bilateral Tomo - No Symptoms
DBTBILSYM	Bilateral Tomo + Symptoms
DBTCONMA2B	BILATERAL TOMO CON MAG 2 BIL
DBTCONMA2L	LT UNILATERAL TOMO CON MAG V
DBTCONMA2R	RT UNILATERAL TOMO CON MAG V
DBTUNILNS	LT Unilateral Tomo No Symptoms
DBTUNILSYM	LT Unilateral Tomo + Symptoms
DBTUNIRNS	RT Unilateral Tomo No Symptoms
DBTUNIRSYM	RT Unilateral Tomo + Symptoms

OBR-4-1
*/

log.info("mammoWS Morpher - START")

def messageType = get('/.MSH-9-1')
def triggerEvent = get('/.MSH-9-2')
def procID = get('OBR-4-2')

log.info("messageType is: {}", messageType)
log.info("triggerEvent is: {}", triggerEvent)

def orderProcedureID = ['DBTBILNS',
                        'DBTBILSYM',
                        'DBTCONMA2B',
                        'DBTCONMA2L',
                        'DBTCONMA2R',
                        'DBTUNILNS',
                        'DBTUNILSYM',
                        'DBTUNIRNS',
                        'DBTUNIRSYM']

if (procID == null || !orderProcedureID.contains(procID)) {
    log.info("OBR-4-2 : {} : Isn't a Known Procedure Code.  Not Forwarding Study To Mammo Workstations.", procID)
    return false
} else {
    log.info("OBR-4-2 : {} : Is on the procedure code list.  Forwarding to Mammo Workstation.", procID)
}

log.info("mammoWS Morpher - End")


