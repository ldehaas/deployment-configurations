/* 
    1. Determine whether the passed PID is a Health Card Number (Regional Identifier). If passed, this would be passed in the overloaded
        "StudyID" field.  This takes presedence over other identifiers???
    2. Validate and pass a local Patient ID only. (The DI-r does PIX-like behaviours of matching different patients to the same person)
    3. Pass a Patient Name if 1. and 2. above not available
*/


log.debug("Starting adhoc_cfind_req_morpher")
LOAD('common.groovy')

log.debug("Incoming AD-HOC C-Find from PACS: {}", input)

/* As per KHC 1527, clear the SIUID value passed in from the PACS, if any, as to not get erroneous query results  */
//set(StudyInstanceUID, "")


// Define valid Query as including either a Health Card Number, Patient ID or Patient Name
def validData = false

// Health Card Number Search Logic
def healthCardNum = get(PatientID)
if (healthCardNum != null) {

    // Check for 10-digit health card that is all numbers only
    if (healthCardNum.matches("[0-9]{10}")) {
        set(PatientID, healthCardNum)
    //    set(IssuerOfPatientID, "HDIRS")
        set(IssuerOfPatientID, Constants.GlobalIssuerOfPID)    
        validData = true

        log.info("Currently Health Card Number is Valid!")
        log.info("Setting Patient ID as '{}' and Issuer of PID as '{}'", get(PatientID), Constants.GlobalIssuerOfPID)

    } else {

        log.error("Ad Hoc Query:'{}' is not a valid Health Card Number.  Continuing search using Patient ID.", healthCardNum)        
    //    return false
    }

    // Clear passed in PatientName
    set(PatientName, "")

    // Leave the passed in PatientID
}

// Patient ID Logic
if (!validData) {

    def mrn = get(PatientID)
    if (mrn != null) {

        // Passed PID is not null
        // Qualify the PID with IssuerOfPID for the DIr to service the requests
        parts = mrn.split('_')
        if (parts.length == 2) {

            set(PatientID, parts[1])
            set(IssuerOfPatientID, parts[0])

        } else {

            set(IssuerOfPatientID, Constants.PrimaryLocalIssuerID)
        }

        // The DI-r may know this patient under a different name and accession number
        // Removing Patient Name as not to confuse the DI-r
        set(PatientName, '')
        set(AccessionNumber, '')
        validData = true

        // Clear the passed in PatientName
        set(PatientName, "")
    }
}


// Patient Name handling logic
if (!validData) {

    def name = get(PatientName)
    log.debug("The Patient Name in Ad-Hoc Query search is '{}'", name)

    if (name != null) {

        if (name.size() <= 2) {
            log.error("Size of name string passed was only {} characters", name.length())
            return false
        }

        // Here we attempt to fix up patient names into something the DIR can understand.
        // It's unlikely we can cover every input case. These are just some common ones
        // we saw while testing with HDIRS.  More could be added.

        // split by commas and only take the first two parts:
        nameParts = name.split(',').take(2).collect { part ->

            // normalize whitespace and add trailing *
            part.trim().replaceAll("\\s+", " ") + "*"
        }

        set(PatientName, nameParts.join("^"))

        log.debug("Current Patient Name Query to be passed is: '{}'", get(PatientName))
        validData = true
    }
}

// Study Instance UID handling logic
if (!validData) {

    def studyInstance = get(StudyInstanceUID)

    if (studyInstance != null) {
        log.debug("The Study Instance UID in Ad-Hoc Query search is '{}'", studyInstance)
        set(IssuerOfPatientID, Constants.GlobalIssuerOfPID)
        validData = true
    }
}

if (!validData) {
    log.error("Query cannot be processed since it has no health card number, " +
              "patient id or patient name.")
    return false
}


// StudyDate Logic:
def studyDate = get(StudyDate)
if (studyDate != null) {

    // Take out the periods
    studyDate = studyDate.replaceAll("\\.+", "")

    // If user passed in a 4-char year, query for all studies for that entire year
    if (studyDate.length() == 4) {

        log.debug("User passed a 4 char year '{}'", studyDate)
        def studyYear = studyDate
        studyDate = studyYear + "0101" + "-" + studyYear + "1231"
        log.debug("Current Study Date range is '{}'", studyDate)
    }

    set(StudyDate, studyDate)
}


// Date of Birth Handling Logic:
def dob = get(PatientBirthDate)
if (dob != null) {

    log.debug("The Date of Birth Passed was {}", dob)
    dob = dob.replaceAll("\\.+", "")
    set(PatientBirthDate, dob)
}


// Modality Logic:
def modality = get(ModalitiesInStudy)
if (modality != null) {

    modality = modality.toUpperCase()
    set(ModalitiesInStudy, modality)
}


// PatientSex logic:
def patientSex = get(PatientSex)
if (patientSex != null) {

    patientSex = patientSex.toUpperCase()

    if (["M", "F", "O"].contains(patientSex)) {
        set(PatientSex, patientSex)

    } else {
        log.error("Ad Hoc Query: Patient Sex passed '$patientSex' was invalid!")
        return false
    }
}


/* Use specific fields as Clear specific fields as return keys explicitly.
*  Since they are empty, they will be used as return keys but 
*   not  as query constraints (i.e. matching keys)
*/

set(ReferringPhysicianName, "")

set(StudyTime, "")

set(StudyID, "")

set(AccessionNumber, "")


/* As per new mappings, put the StudyDescription field to get it back */
set(StudyDescription, "")


/* Log outgoing C-FIND to DI-r */
log.debug("This is the outgoing C-FIND object to the DI-r: \n[{}]", output)
