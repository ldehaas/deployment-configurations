
/*
 * this script converts dicom cfind request to xds find documents query
 *
 * this script can return false to signal a particular cfind 
 * to be dropped
 */

// obtaining properties from cfind
pid = get(PatientID)
issuer = get(IssuerOfPatientID)

// setting properties in find documents query
set(XDSPatientID, pid, (String) issuer)

// constraint by study date, if specified in cfind
// dateRange = get(StudyDate)
// set(XDSServiceStartTime, dateRange)

// setting extended metadata in find documents query
// set(XDSExtendedMetadata("studyInstanceUID"), get(StudyInstanceUID))

// creating xds codes
// codes = [code(code1, scheme1), code(code2, scheme2)]
// set(XDSEventCode, code(code1, scheme1), code(code2, scheme2))
