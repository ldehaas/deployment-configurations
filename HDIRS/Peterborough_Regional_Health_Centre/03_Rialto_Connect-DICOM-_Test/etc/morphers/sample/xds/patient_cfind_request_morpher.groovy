/* Patient C-Find to PACS Morpher
 *
 * Shared in both the Ad-Hoc Query and Fetch Prior Studies workflow
 *
 * When the first image in a study is received, this script sets up a cfind query
 * that will be sent to the PACS to find local patient demographics for the patient.
 * The results can later be used to localize the entire study.
 *
 * Returning 'false' causes Connect not to issue a C-Find to the local PACS for this Study.
 *
 * This script is optional.  If not listed in rialto.cfg.xml, Connect will not attempt
 * to query the Local PACS for local Patient Demographics.
 */

LOAD("shared_scripting_files/shared_constants.groovy")

log.debug("Starting 'Patient C-Find to PACS' Morpher " +
    "--Creating the C-Find to query for local PACS demographics")
log.debug("Current Input received by the Script (i.e. base C-Find):\n{}", input)


if (get(IssuerOfPatientID) == SharedConstants.Local_PACS_Issuer_of_Patient_ID) {
    // Remove the Issuer of Patient ID passed in the Java code. Some PACS will
    // fail the query if Issuer Of PID is specified
    remove(IssuerOfPatientID)

    // Patient Name is needed from the PACS
    set(PatientName, "")

    // Get the Patient Date of Birth as well
    set(PatientBirthDate, "")

    log.debug("C-Find that will be used for querying local PACS Demographics:\n{}", output)


} else {
    log.warn("Skipping C-FIND to PACS to get local Patient Demographics " +
        "because the local pid is not known.")

    return false
}
