/* This script takes the DICOM C-Find responses from the DIR and filters
   out local studies based on the IssuerOfPatientID returned by the DI-r
*/

log.debug("Starting cfind_response_filter")
LOAD("common.groovy")

def pids = new Pids(input)

if (pids.getSourceDomain() == null) {
    log.error("Unable to process cfind response: missing source issuer")
    return false
}

if (pids.isLocal()) {
    log.debug("Source issuer '{}' is local.  Filtering this cfind response", pids.getSourceDomain())
    return false
}

if (pids.isSourceUnwantedDomain()) {
    log.info("Source issuer '{}' is an unwanted domain.  Filtering this cfind response", pids.getSourceDomain())
    return false
}

if (pids.getHealthCardNumber() == null) {
    log.error("A Valid HCN was not found, rejecting this study")
    return false
}

//if (get(Modality) == null || get(Modality) != "MG") {
//    log.info("Study is not a MG, removing from the list")
//    return false
//}

// Special case for TNI, KHC1990.
// Prior to December 1, 2014, TNI produced multiple study instance uids
// with the same accession number, which some PACSs can't understand:
if (pids.getSourceDomain() == "TNI") {
    // This keeps studies from December 1:
    if (get(StudyDate) < "20141201") {
        log.info("Dropping TNI study from before December 1, 2014")
        return false
    }
}

