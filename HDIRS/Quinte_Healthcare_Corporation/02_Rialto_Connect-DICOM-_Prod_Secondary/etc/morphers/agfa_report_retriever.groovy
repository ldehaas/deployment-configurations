// Runs after Rialto gets an html reponse back from the Agfa Reports Server

if (html.body.h2.text().contains(
    "There is no report information for this patient")) {
    log.debug("Report not found with the given combination of " +
              "pid, issuer, and accession number")
    return false
}

def body = html.body

def table0tr = body.table[0].tr[1]
def table1tr = body.table[1].tr[1]
def table2tr = body.table[2].tr[1]
def table3tr = body.table[3].tr[1]
def table4tr = body.table[4].tr[2] // there is an extra of tr before this

def accNum = table1tr.td[0].text()
def physician = table1tr.td[1].text()
def status = parseResultStatus(table3tr.td[0].text())
def procCodeText = table2tr.td[0].text()


def mapping = [
    [parseFamilyName(table0tr.td[0].text()),  "/.PID-5-1"],
    [parseGivenName(table0tr.td[0].text()),   "/.PID-5-2"],
    [table0tr.td[1].text(),                   "/.PID-3-1"],
    [parseSex(table0tr.td[2].text()),         "/.PID-8-1"],
    
    ["NW",                                    "/.ORC-1"], // new order
    [accNum,                                  "/.ORC-2"],
    [accNum,                                  "/.ORC-3"],
    ["CM",                                    "/.ORC-5"],
    [parseFamilyName(physician),              "/.ORC-12-1"], // ordering provider
    [parseGivenName(physician),               "/.ORC-12-2"], // ordering provider
    
    
    [accNum,                                , "/.OBR-2-1"], // placer order number
    [parseFamilyName(physician),              "/.OBR-16-1"], // ordering provider
    [parseGivenName(physician),               "/.OBR-16-2"], // ordering provider
    
    [procCodeText,                            "/.OBR-4-2"], // universal service id
    [table2tr.td[1].text(),                   "/.OBR-31"], // reason for study
    
    [status,                                  "/.OBR-25"], // results tatus
    [table3tr.td[1].text(),                   "/.OBR-32-1-1"], // principle result interpreter
]    
    
initializeReport("ORU", "R01", "2.3", mapping)

try {
    def date = convertToHL7Date(table0tr.td[3].text(), "yyyy-MM-dd")
    set("/.PID-7-1", date)
} catch (Exception e) {
    // sink it, no valid date format
}

// impression
addOBX('', 'IMP', procCodeText, table4tr.td[0].text())

// body:
// ugly ugly
// normal iterator only iterates thru non string nodes, have to use
// nodeIterator here to iterate thru all nodes
def prev = null
table4tr.td[1].div.nodeIterator().each {node->
    node.children().each {
        if (it instanceof String) {
            addOBX('', 'GDT', procCodeText, it.trim())
            
        } else if ( // not a string, we want to check consequtive <br>'s
            prev != null &&
            !(prev instanceof String) &&
            "br" == prev.name && 
            "br" == it.name) {
            addOBX('', 'GDT', procCodeText, ' ')
        }
        
        prev = it
    }
}

log.debug("This is the Report that Rialto Converted to HL7 from HTML (Agfa) format:\n{}", output)
