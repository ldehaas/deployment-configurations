
/**
 * Accession number localization rules for HDIRS QHC site
 *
 * @author Sam Jiang <sam.jiang@karoshealth.com>
 * @since Oct 5, 2011
 */
class AccessionNumbers {
    /*
     * these hash sets are checked in the order of declaration
     */


    /**
     * If an accession number is not supposed to be prefixed or suffixed, no action required
     */
    private static noAction = ["EXR"] as HashSet;

    /**
     * If an accession number already contains a prefix or suffix from this
     * set, no action required
     */
    private static alreadyAdded =
        ["HHHS", "LACH", "PROV", "PSFH", "RMH", "RVHC", "SMH", "TEGH", "TSH", "TSHH", "SRHC"] as HashSet;

    /**
     * For accession numbers from the following issuers, prefix them
     */
    private static toPrefix = 
        ["BGH", "HDH", "KGH", "KNG1", "LCH", "MSH", "NYGH", "PRHC", "QHC", "SHSC", "SUNB", "STMH", "YCH", "KMRIKMI", "TNI"] as HashSet;

    /**
     * For accession numbers from the following issuers, suffix them only if
     * the resulting accession numbers don't exceed 16 chars
     */
    private static toSuffix =
        ["CMH":"C", "NHH":"N", "SRHC":"S"] as HashMap

    /**
     * @param sourceIssuer issuer of the accession number
     * @param sourceAccessionNum raw accession number
     * @return localized accession number
     */
    static prefix(sourceIssuer, sourceAccessionNum, log) {
        if (sourceAccessionNum == null) {
            return null;
        }

        if (sourceIssuer == null) {
            sourceIssuer = "";
        }

        // Some sites do not get a prefix or suffix adding
        //
        def uIssuer = sourceIssuer.toUpperCase();
        if (noAction.contains(uIssuer)) {
            return sourceAccessionNum;
        }

        // Some sites get a prefix or suffix only if not already present
        //
        if (alreadyAdded.contains(uIssuer)) {

            def uAccNum = sourceAccessionNum.toUpperCase();
            def alreadyLocalized = uAccNum.startsWith(uIssuer) || uAccNum.endsWith(uIssuer)
            if (alreadyLocalized) {
                return sourceAccessionNum;
            }
            log.warn("Expect accession numbers from '{}' to already" +
                     "contain the issuer as a prefix or suffix, but got '{}'. " +
                     "Will prefix with the issuer name as fallback",
                     sourceIssuer, sourceAccessionNum);
            return sourceIssuer + sourceAccessionNum;
        }

        if (toPrefix.contains(uIssuer)) {
            return sourceIssuer + sourceAccessionNum;
        }

        if (toSuffix.keySet().contains(uIssuer)) {

            // The suffix ones are special.  Only suffix if it has room
            //
            def ret = sourceAccessionNum + toSuffix.get(uIssuer);
            if (ret.length() > 16) {

                log.debug("suffixing accession number '{}' from '{}' will exceed the 16 chars limit. " +
                          "Assuming it doesn't need to be suffixed.",
                          sourceAccessionNum, sourceIssuer);
                return sourceAccessionNum;
            }
            return ret;
        }
        log.warn("Unrecognized issuer '{}'. " +
                 "Will prefix accession '{}' with the issuer as a fallback",
                 sourceIssuer, sourceAccessionNum);
        return sourceIssuer + sourceAccessionNum;
    }
}
