/**
 * this localizer is responsible for
 * any ingested image headers
 */

LOAD("common.groovy")
LOAD("accession_number.groovy")
LOAD("procedure_codes.groovy")

log.debug("Starting to localize a DICOM image received from the DI-r.")
log.trace("This is the received DICOM header from the DIR:\n{}", input)

def sourcePid = get(PatientID)
def sourceIssuer = get(IssuerOfPatientID)

if(sourceIssuer == null) {
    log.warn("Dropping study '{}' because the source domain is unknown. " +
             "This prevents localization of the accession number.", get(StudyInstanceUID) )
    return false
}

if (sourcePid != null || sourceIssuer != null) {
    localizedPid = Pids.localize([sourcePid, sourceIssuer], getAllPidsQualified())

    if (localizedPid[1] == sourceIssuer) {
        // A local study / image, sink it
        // This is just for safety, as local studies should have been filtered / dropped earlier
        log.info("Dropping local study / image.")
        return false
    
    } else {
        // Need to localize incoming image header
        
        if (localizedPid[0] != null && localizedPid[0].length() > 64) {
            // we don't prohibit this. other DICOM speakers may not like this
            log.warn("Prefixed patient id exceeds 64 chars: {}.  This might cause issues at PACS.", localizedPid[0] )
        }
        
        set(PatientID, localizedPid[0])
        log.debug("Setting Patient ID to '{}'", localizedPid[0])
        
        def newAccNum = AccessionNumbers.prefix(sourceIssuer, get(AccessionNumber), log)
        if (newAccNum != null && newAccNum.length() > 16) {
            // again, we don't prohibit this. other DICOM speakers may not like this
            log.warn("Localized accession number exceeds 16 chars: {}", newAccNum)
        }
        log.debug("Setting Accession Number and Study ID to '{}'", newAccNum) 

        set(AccessionNumber, newAccNum)
        set(StudyID, newAccNum)
    }
}

// PACS doesn't like these
remove(OtherPatientIDsSequence)
remove(IssuerOfPatientID) 


// Localize Procedure Codes
def procCodes = get(StudyDescription)

if (procCodes != null) {
    def modality = get(Modality)
    log.debug("localizing study description {} for modality {}", procCodes, modality)

    // Modality aliasing
    // TODO move these out of here, and into the common file?
    modality_aliases = [
        'BMD': 'NM',
        'XA':  'CR',
        'RF':  'CR',
        'DX':  'CR',
    ]
    def alias = modality_aliases[modality]
    if (alias != null) {
        modality = alias
    }
    // End modality aliasing

    // Procedure code aliasing
    def localizedProcCodes = ProcedureCodes.localize(modality, procCodes)
    if (localizedProcCodes == null) {
        log.warn("cannot localize procedure codes: '{}' for modality {}", 
		procCodes, modality)
    } else {
        log.debug("localized proc codes from {} to {}", procCodes, localizedProcCodes)
        set(StudyDescription, localizedProcCodes)
    }
    // End procedure code aliasing
}


// Force retrieve ae title to Rialto's own
set(RetrieveAETitle, 'QHC_RIALTO_T1')

log.info("Preparing to move localized SOP to PACS.  Accession Number '{}', Patient ID '{}', " +
         "Study Instance UID '{}', SOP Instance UID '{}', SOP Modality '{}'",
         output.get(AccessionNumber), output.get(PatientID), output.get(StudyInstanceUID), output.get(SOPInstanceUID), output.get(Modality) )

log.trace("Image (SOP) headers in outgoing C-Store to Local PACS (after morphing):\n{}", output)
log.debug("Ending Localization of DI-r image")
