/*
 * Principal script driving the Automatic Prefetch workflow
 *
 * Convert an HL7 order message into a cfind message.
 * get() works on the order message, while set() works on the 
 * cfind message.
 * 
 * The order is ignored if this script returns false,
 * e.g. no images will be fetched for the order.
 */
 
LOAD('common.groovy')

log.debug("Received HL7 Order from the RIS.  Converting to DICOM C-Find.")
log.debug("Original HL7 Order from the RIS:\n{}", input)

def pid = get( "PID-4-1" )
log.debug("Found pid '{}'", pid)

// filter out out-of-province patients
def hcn = get( "PID-19" )
log.debug("Found HCN '{}'", hcn)

if (hcn ==~ /\d{10}[^\d]*/ &&
    ! hcn.startsWith('0000000000') &&
    ! hcn.startsWith('1111111111')) {

    // construct the cfind
    set ( PatientID, pid )
    set ( IssuerOfPatientID, Constants.PrimaryLocalIssuerID )

    // set return keys
    set ( StudyInstanceUID, null )

    // the following isn't necessary, but useful for debugging
    set ( AccessionNumber, null )

    //now = new org.joda.time.DateTime()
    //oneWeekOld = now.minusDays(7)
    //set ( StudyDate, org.joda.time.format.DateTimeFormat.forPattern('yyyyMMdd-').print(oneWeekOld) )


} else {
    log.debug("HCN is missing or it's a test HCN, not going to trigger prefetch.\n" +
              "A valid HCN must start with a 10 digit number that's not all 0s or all 1s")
    return false
}

//log.debug("This is the outgoing DICOM C-FIND to the DI-r:\n{}", ouput)
