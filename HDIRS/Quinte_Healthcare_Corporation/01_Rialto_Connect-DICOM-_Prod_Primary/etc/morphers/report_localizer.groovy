
// AccessionNumber has been localized in the image already.
// This overwrites the unlocalized value set in the agfa report scraper.
def accnum = get(AccessionNumber)
set("ORC-2", accnum)
set("ORC-3", accnum)
set("OBR-2", accnum)

