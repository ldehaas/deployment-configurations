class Constants {
    static final LocalIssuerIDs = ["QUINTE"]
    static final PrimaryLocalIssuerID = LocalIssuerIDs[0]
    static final LocalAETitleIssuerLookup = [
        "QUINTE":PrimaryLocalIssuerID
    ]
}

class Pids {
    /**
     * @return a two element array containing the localized pid and issuer
     * @param qualifiedSourcePid a two element array containing source
     *      pid and source issuer
     * @param qualifiedOtherPids an array of qualified pids
     */
    static localize(qualifiedSourcePid, qualifiedOtherPids) {
        def sourcePid = qualifiedSourcePid[0]
        def sourceIssuer = qualifiedSourcePid[1]
    
        if ( Constants.LocalIssuerIDs.contains(sourceIssuer) ) {
            // this image originated from the local PACS
            return qualifiedSourcePid;
        }
    
        def foundPid = null
        for (def pid : qualifiedOtherPids) {
            if ( Constants.LocalIssuerIDs.contains(pid[1]) ) {
                foundPid = pid
                break
            }
        }
    
        // no linked pid found, create one by prefixing
        if (foundPid == null) {
            foundPid = [ sourceIssuer+"_"+sourcePid, Constants.PrimaryLocalIssuerID]
        }
        
        return foundPid
    }
}


