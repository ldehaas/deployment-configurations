/* Used to filter out C-Find Responses from the DI-r (both Ad-Hoc and Automatic Pre-fetch)
*  Seems to run once per response
*/

LOAD("common.groovy")

log.debug("Evaluating C-Find responses from DI-r that might need to be filtered out.  Using input:\n{}", input)

sourceIssuer = get(IssuerOfPatientID)
log.debug("Found source issuer '{}'", sourceIssuer)

if (sourceIssuer == null) {
    log.warn("Unable to process C-Find response from DI-r because it has a null IssuerOfPatientID.  " + 
    "(Foreign?) Patient ID '{}', Study Instance UID '{}'  Accession Number '{}'", get(PatientID), get(StudyInstanceUID), get(AccessionNumber) )

    return false
}

if (Constants.LocalIssuerIDs.contains(sourceIssuer)) {

    log.info("Source issuer '{}' is local.  Filtering this cfind response.  " +
    "Study Instance UID '{}'  Accession Number '{}'", sourceIssuer, get(StudyInstanceUID), get(AccessionNumber))

    return false
}

// Special case for TNI, KHC2452.
// Prior to December 1, 2014, TNI produced multiple study instance uids
// with the same accession number, which some PACSs can't understand:
if (sourceIssuer == "TNI") {
    // This keeps studies from December 1:
    if (get(StudyDate) < "20141201") {
        log.info("Dropping TNI study from before December 1, 2014")
        return false
    }
}

