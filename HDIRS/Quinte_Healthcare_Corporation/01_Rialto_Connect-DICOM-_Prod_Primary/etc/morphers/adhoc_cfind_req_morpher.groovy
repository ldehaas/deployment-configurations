log.debug("Starting adhoc_cfind_req_morpher")
LOAD('common.groovy')

log.debug("Received an Ad-Hoc C-Find Query containting the following:\n{}", input)

// Define valid Query as including either a Health Card Number, Patient ID
def validData = false

// Health Card Number Search Logic
def healthCardNum = get(PatientID)
if (healthCardNum != null) {

    // Check for 10-digit health card that is all numbers only
    if (healthCardNum.matches("[0-9]{10}")) {
        set(PatientID, healthCardNum)
    //    set(IssuerOfPatientID, "HDIRS")
        set(IssuerOfPatientID, Constants.GlobalIssuerOfPID)
        validData = true

        log.info("Currently Health Card Number is Valid!")
        log.info("Setting Patient ID as '{}' and Issuer of PID as '{}'", get(PatientID), Constants.GlobalIssuerOfPID)

    } else {

        log.warn("Ad Hoc Query:'{}' is not a valid Health Card Number.  Continuing search using Patient ID.", healthCardNum)
    //    return false
    }

    // Clear passed in PatientName
    set(PatientName, "")

    // Leave the passed in PatientID
}


if (get(StudyID) != null) {
    log.debug("Dropping ad hoc cfind containing a StudyID")
    return false
}

if (get(AccessionNumber) != null) {
    log.debug("Dropping ad hoc cfind containing an AccessionNumber")
    return false
}

// Associated with KHC 10640 - Do not retrieve studies when the Modality is KO
def modality = get(Modality)
if (modality != null && modality == "KO") {
    log.error("Exiting because Modality is KO")
    return false
}

if (!validData) {
    mrn = get(PatientID)
    if (mrn == null) {
        // not to touch, but put in return keys
        return
    }

    parts = mrn.split('_')
    if (parts.length == 2) {
        set(PatientID, parts[1])
        set(IssuerOfPatientID, parts[0])
        log.debug("Assuming a qualified Patient Number of MRN '{}' and IssuerOfPatientID '{}'", parts[1], parts[0]) 

    } else {
        set(IssuerOfPatientID, Constants.PrimaryLocalIssuerID)
        log.debug("Manually Qualifying the IssuerOfPatientID to '{}'", Constants.PrimaryLocalIssuerID)
    }
}

log.debug("Querying the DI-r with with the following:\n{}", output)
