
class ProcedureCodes {
    static localize(modality, rawText) {
        if (rawText == null || rawText.trim().isEmpty()) {
            return null
        }
        rawText = rawText.toLowerCase()
        
        if (modality == null) {
            modality = "" // so no NPE
        }
        
        def instructions = lookup[modality.toUpperCase()]
        
        if (instructions == null) {
            return null; // can't localize
        }
        
        // check direct translations
        def direct = instructions['direct']
        if (direct != null) {
            for (def mapped : direct) {
                if (rawText.contains( mapped.toLowerCase() )) {
                    return mapped
                }
            }
        }
        
        // check maps
        def map = instructions['map']
        if (map != null) {
            for (def mapped : map.keySet()) {
                def texts = map[mapped]
                for (def text : texts) {
                    if (rawText.contains(text)) {
                        return mapped
                    }
                }
            }
        }
        
        // default return
        return instructions['default'] // and this returns null if no default is provided
    }
    
    private static final lookup = [
        'CT': [
            direct: ['Head', 'Neck', 'Chest', 'Abdomen', 'Pelvis', 'Thorax', 'Spine'],
            default: 'Other'
        ],
            
        'MR': [
            direct: ['Head', 'Neck', 'Chest', 'Abdomen', 'Pelvis', 'Thorax', 'Spine'],
            default: 'Other'
        ],

        'MG': [
            direct: ['Mammogram', 'mammo', 'obsp'],
            map: ['Mammogram': ['breast']],
            default: 'Other',
        ],

        'NM': [
            direct: ['Bone Density','bmd'],
            default: 'Other',
        ],

        'US': [
            direct: ['Breast', 'Abdomen', 'Pelvis', 'TV'],
            default: 'Other',
        ],

        'CR': [
            direct: ['Chest', 'Abdomen', 'Spine', 'Hand', 'Wrist', 'Foot', 'Humerus', 'GI', 'Joint'],
            default: 'Other',
        ],
    ]
}


