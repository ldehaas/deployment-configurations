
class Normalization {
    static final BONE_DENSITY_KWS = ['bmd', 'bone', 'density', 'mineral', 'dxa']

    static final CHEST_KWS = ['cardiac', 'chest', 'pacemaker', 'ribs', 'sternum', 'thoracic', 'thorax']

    static final NEURO_KWS = ['acoustics', 'brain', 'head', 'joint', 'neck', 'spine', 'temporomandibular', 'microdenoma', 'carotid']

    static final UPPER_EXTREMITY_KWS = [
            'ac', 'bone age', 'clavicle', 'elbow', 'finger', 'hand', 'humerus', 'joints', 'radius',
            'scapula', 'shoulder', 'sternoclavicular', 'ulna', 'wrist'
        ]

    static final LOWER_EXTREMITY_KWS = [
            // NB: "calcaneous" is a mispelling, but it occurs in the DIR:
            'ankle', 'calcaneous', 'calcaneus', 'femur', 'fibula', 'foot', 'hip', 'knee', 'leg',
            'pelvhip', 'penis', 'tibia', 'toe'
        ]

    static final EXTREMITY_KWS = UPPER_EXTREMITY_KWS + LOWER_EXTREMITY_KWS + 'extremity' + 'extrem'

    /**
     * Each foreign modality has a list of rules containing keywords.  The
     * first keywords that match anything in the study description is
     * the rule to use.  The last rule should always have an empty
     * list of keywords, making it the default for that modality.
     *
     * Most modalities will map to the same normalized value, but we keep
     * them explicit because there are a few cases where they don't.
     */
    static final map = [
        'MG': [
            [ keywords: ['diagnostic', 'mammogram', 'obsp', 'screening'],
              normalizedModality: "MG",
              procCode: "8759",
              procDesc: "Mammo Reference"
            ],

            [ keywords: [],
              normalizedModality: "MG",
              procCode: "8760",
              procDesc: "Mammo Work-up Reference",
            ],
        ],

        'NM': [
            [ keywords: ['pet'],
              normalizedModality: "NM",
              procCode: "8756",
              procDesc: "NM Reference PET",
            ],

            [ keywords: [],
              normalizedModality: "NM",
              procCode: "8755",
              procDesc: "NM Reference",
            ],
        ],

        'XA': [
            [ keywords: ['asp', 'aspiration', 'change', 'coiling', 'drain', 'insertion', 'picc',
                         'port', 'pyelogram', 'tube', 'angio'],
              normalizedModality: "XA",
              procCode: "8761",
              procDesc: "IVR Reference",
            ],

            [ keywords: UPPER_EXTREMITY_KWS,
              normalizedModality: "CR",
              procCode: "8750",
              procDesc: "DX Reference Upper",
            ],

            [ keywords: LOWER_EXTREMITY_KWS,
              normalizedModality: "CR",
              procCode: "8751",
              procDesc: "DX Reference Lower",
            ],
        ],

        'US': [
            [ keywords: ['axilla', 'breast'],
              normalizedModality: "US",
              procCode: "8742",
              procDesc: "US Reference Breast",
            ],

            [ keywords: [],
              normalizedModality: "US",
              procCode: "8741",
              procDesc: "US Reference",
            ],
        ],

        'CR': [
            [ keywords: CHEST_KWS,
              normalizedModality: "CR",
              procCode: "8748",
              procDesc: "DX Reference Chest",
            ],

            [ keywords: LOWER_EXTREMITY_KWS,
              normalizedModality: "CR",
              procCode: "8751",
              procDesc: "DX Reference Lower",
            ],

            [ keywords: ['basket', 'bowel', 'cholangiogram', 'cholangiopancreaton', 'cine',
                         'cystogram', 'defecogram', 'endoscopic', 'enema', 'extraction',
                         'fistulogram', 'fluoroscopic', 'fluoroscopy', 'gi', 'hysterosalpinogram',
                         'loopogram', 'lymphangiogram', 'oesophagus', 'operative', 'palatogram',
                         'pouchogram', 'pyelogram', 'retrograde', 'shunt', 'sialogram', 'sinogram',
                         'sniff', 'swallowing', 'table', 'tilt', 'urethrogram', 'urodynamics',
                         'ventriculoperitoneal'],
              normalizedModality: "CR",
              procCode: "8752",
              procDesc: "DX Reference Special/GI/GU",
            ],

            [ keywords: UPPER_EXTREMITY_KWS,
              normalizedModality: "CR",
              procCode: "8750",
              procDesc: "DX Reference Upper",
            ],

            [ keywords: ['mammo'],
              normalizedModality: "MG",
              procCode: "8759",
              procDesc: "Mammo Reference",
            ],

            [ keywords: [],
              normalizedModality: "CR",
              procCode: "8749",
              procDesc: "DX Reference Body",
            ],
        ],

        'MR': [
            [ keywords: ['breast'],
              normalizedModality: "MR",
              procCode: "8747",
              procDesc: "MR Reference Breast",
            ],

            [ keywords: CHEST_KWS,
              normalizedModality: "MR",
              procCode: "8745",
              procDesc: "MR Reference Chest",
            ],

            [ keywords: EXTREMITY_KWS,
              normalizedModality: "MR",
              procCode: "8746",
              procDesc: "MR Reference Extremity",
            ],

            [ keywords: NEURO_KWS,
              normalizedModality: "MR",
              procCode: "8743",
              procDesc: "MR Reference Neuro",
            ],

            [ keywords: [],
              normalizedModality: "MR",
              procCode: "8744",
              procDesc: "MR Reference Body",
            ],
        ],

        'CT': [
            [ keywords: ['abdomen', 'pelvis'],
              normalizedModality: "CT",
              procCode: "8738",
              procDesc: "CT Reference Body",
            ],

            [ keywords: CHEST_KWS,
              normalizedModality: "CT",
              procCode: "8739",
              procDesc: "CT Reference Chest",
            ],

            [ keywords: EXTREMITY_KWS,
              normalizedModality: "CT",
              procCode: "8746",
              procDesc: "CT Reference Extremity",
            ],

            [ keywords: NEURO_KWS,
              normalizedModality: "CT",
              procCode: "8729",
              procDesc: "CT Reference Neuro",
            ],

            // Mislabeled data in the DIR:
            [ keywords: ['nm', 'pet'],
              normalizedModality: "NM",
              procCode: "8756",
              procDesc: "NM Reference Upper",
            ],

            [ keywords: [],
              normalizedModality: "CT",
              procCode: "8738",
              procDesc: "CT Reference Other",
            ],
        ],

        'IO': [
            // Mislabeled data in the DIR.  Usually dental.
            [ keywords: [],
              normalizedModality: "CR",
              procCode: "8750",
              procDesc: "DX Reference Upper",
            ],
        ],

        // "Secondary Capture". These tend to be captures of other modalities:
        'SC': [
            [ keywords: UPPER_EXTREMITY_KWS,
              normalizedModality: "CR",
              procCode: "8750",
              procDesc: "DX Reference Upper",
            ],

            [ keywords: LOWER_EXTREMITY_KWS,
              normalizedModality: "CR",
              procCode: "8751",
              procDesc: "DX Reference Lower",
            ],

            // weird data from Brockville:
            [ keywords: BONE_DENSITY_KWS,
              normalizedModality: "OT",
              procCode: "8757",
              procDesc: "BMD Reference",
            ],
        ],

        'BD': [
            [ keywords: [],
              normalizedModality: "OT",
              procCode: "8757",
              procDesc: "BMD Reference",
            ],
        ],

        'OT': [
            [ keywords: BONE_DENSITY_KWS,
              normalizedModality: "OT",
              procCode: "8757",
              procDesc: "BMD Reference",
            ],

            [ keywords: [],
              normalizedModality: "CR",
              procCode: "8749",
              procDesc: "DX Reference Body",
            ],
        ],

        'PT': [
            [ keywords: [],
              normalizedModality: "OT",
              procCode: "8756",
              procDesc: "NM Reference PET",
            ],
        ],
    ]

    static final defaultRule =
            [ keywords: [],
              normalizedModality: null,
              procCode: null,
              procDesc: null,
            ]

    static {
        // add entries for cases when multiple foreign modalities map to
        // the same rules:
        ["DX", "DR", "CX", "RF"].each {
            map[it] = map["CR"]
        }

        // MM is sometimes used in place of MG:
        map["MM"] = map["MG"]

        map["BMD"] = map["BD"]
    }

    private static findRule(foreignModality, studyDesc) {
        def rules = foreignModality == null ? null : map[foreignModality.toUpperCase()]

        if (rules != null) {
            return rules.find { rule ->
                rule.keywords.isEmpty() ||
                    (studyDesc != null &&
                     rule.keywords.find({ studyDesc.toLowerCase().contains(it) }) != null)
            }
        }

        return defaultRule
    }

    /**
     * Find the normalized modality based on the foreign modality and
     * study description.
     */
    public static getNormalizedModality(foreignModality, studyDesc) {
        def rule = findRule(foreignModality, studyDesc)
        // default is to keep original modality if there's no known mapping:
        return rule == null ? foreignModality : rule.normalizedModality
    }

    /**
     * Find the normalized procedure code and description based on the
     * foreign modality and study description.
     * <p>
     * @return [code, desc], which may be null if not known
     */
    public static getNormalizedCodeAndDesc(foreignModality, studyDesc) {
        def rule = findRule(foreignModality, studyDesc)
        return rule == null ? [null, null] : [rule.procCode, rule.procDesc]
    }

    /**
     * Find the "best" modality from ModalitiesInStudy, as per Appendix B
     */
    public static findDominantModality(modalitiesInStudy) {
        return modalitiesInStudy.find { !["PR", "SR"].contains(it) }
    }
}

