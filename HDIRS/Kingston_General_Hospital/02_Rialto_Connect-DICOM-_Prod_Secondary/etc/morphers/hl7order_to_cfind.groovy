/*
 * Convert an order message into a cfind message.
 * get() works on the order message, while set() works on the
 * cfind message.
 *
 * The order is ignored if this script returns false,
 * e.g. no images will be fetched for the order.
 */

log.debug("Starting $this")
LOAD('common.groovy')

log.debug("Incoming HL7 Message:\n{}", input)

context = Contexts.getContext(config.context)
if (context == null) {
    log.warn("Context is unknown from id: {}. Dropping this order. " +
        "Please correct either the connect ContextID configuration or common.groovy", config.context)
    return false
}

def pid = get("PID-3-1")
log.debug("Found pid '{}'", pid)


// Check for a valid health card number with 10-digits
def hcn = get(context.HCNLocation)
log.debug("Health Card Number is '{}'", hcn);

if (!(hcn ==~ '[0-9]{10}')) {
    log.warn("Health Card Number '{}' not valid. Ignoring this order.", hcn);
    return false
}

/* Construct the C-FIND */

// Matching keys: PatientID + IssuerOfPatientID

set(PatientID, pid)
// IssuerOfPID is hard coded to local issuer:
set(IssuerOfPatientID, context.PACSIssuer)

// Set the return keys
set(StudyInstanceUID, null)
// The following isn't necessary, but useful for debugging
set(AccessionNumber, null)

log.debug("Created cfind request for Pid '{}' and Health Card Number '{}':\n{}", pid, hcn, output)

