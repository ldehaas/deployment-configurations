/**
 * This localizer is responsible for localizing any ingested study headers for ingestion from adhoc cfind responses
 */

log.debug("Starting $this")

LOAD("common.groovy")
LOAD("accession_number.groovy")
LOAD("normalization.groovy")

log.trace("This is the received response from the DI-r: {}", output)

context = Contexts.getContext(config.context)
if (context == null) {
    log.warn("Context is unknown from id: {}. Dropping this cfind response. " +
        "Please correct either the connect ContextID configuration or common.groovy", config.context)
    return false
}

// Change the Retrieve AE Title to that of Connect itself
set(RetrieveAETitle, context.RialtoRetrieveAE)

def suid = get(StudyInstanceUID)

def pids = new Pids(input, context)

if (pids.isLocal()) {
    log.debug("Dropping local study: '{}'", suid)
    return false
}

if (pids.isSourceUnwantedDomain()) {
    log.info("Dropping study: '{}' from unwanted domain '{}'.", suid, get(IssuerOfPatientID))
    return false
}

if (pids.getSourceDomain() == null) {
    log.error("Dropping study '{}' because the source domain is unknown. " +
              "This prevents localization of the accession number.")
    return false
}

def localPid = pids.getLocalPidWithFallbacks()
if (localPid[0].length() > 64) {
    log.warn("Local patient id exceeds 64 chars: '{}'", localizedPid[0])
}

pids.localize(localPid)


// Localize Accession Numbers and Study IDs
def oldAccNum = get(AccessionNumber)
def newAccNum = AccessionNumbers.localize(pids.getSourceDomain(), oldAccNum, log)
log.debug("Source issuer is '{}', old accession number is '{}', new accession number and study ID is '{}'",
    pids.getSourceDomain(), oldAccNum, newAccNum);
set(AccessionNumber, newAccNum)

// Localize Study Modality
def modality = Normalization.findDominantModality(getList(ModalitiesInStudy))
if (modality != null) {
    def localModality = Normalization.getNormalizedModality(modality, get(StudyDescription))
    log.debug("Localizing modality '{}' to '{}'", modality, localModality)
    set(Modality, localModality)
} else {
    log.debug("Study has no dominant modality.")
}

// KHC1527: this makes allows cosmetic improvements in the PACS display:
set(StudyID, get(StudyDescription))
log.debug("Setting the Study Description in the StudyID field to '{}'", get(StudyDescription))

log.debug("This is the outgoing morphed C-FIND response to PACS:\n{}", output)

