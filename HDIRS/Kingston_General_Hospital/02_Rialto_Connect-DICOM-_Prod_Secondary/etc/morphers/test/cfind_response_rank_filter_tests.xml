<tests service="connect-dicom"
       script="CFindResponseRankFilter"
       file="cfind_response_rank_filter.groovy">

   <!-- These tests will break when studies that are valid at time
        of writing get old enough.  Need to have a way to let the
        test adjust the input values according to the current date. -->

    <test name="matchAll">
        <inputs>
            <list name="inputs">
                <dicom>
                    StudyInstanceUID: 1
                    StudyDate: 20130101
                    StudyTime: 000000
                </dicom>
            </list>
            <hl7 name="order">
                MSH|^~\&amp;|||||20131122140834||ORM^O01|15271192|P|2.3
                PID|1|pid
                ORC|
                OBR|1|||CT|
            </hl7>
            <string name="context">KGH</string>
        </inputs>
        <assertions>
            assert returned.size() == 1
            assert returned[0].get(StudyInstanceUID) == "1"
        </assertions>
    </test>

    <test name="filterOldStudies">
        <inputs>
            <list name="inputs">
                <dicom>
                    StudyInstanceUID: 1
                    StudyDate: 20150101
                    StudyTime: 000000
                </dicom>
                <dicom>
                    StudyInstanceUID: 2
                    StudyDate: 20140101
                    StudyTime: 000000
                </dicom>
                <!-- older than 5 years: -->
                <dicom>
                    StudyInstanceUID: 3
                    StudyDate: 20100101
                    StudyTime: 000000
                </dicom>
            </list>
            <hl7 name="order">
                MSH|^~\&amp;|||||20131122140834||ORM^O01|15271192|P|2.3
                PID|1|pid
                ORC|
                OBR|1|||CT|
            </hl7>
            <string name="context">KGH</string>
        </inputs>
        <assertions>
            assert returned.size() == 2
            assert returned.collect { it.get(StudyInstanceUID) } as Set == ["1", "2"] as Set
        </assertions>
    </test>

    <test name="filterOldStudiesLACH">
        <inputs>
            <list name="inputs">
                <dicom>
                    StudyInstanceUID: 1
                    StudyDate: 20150101
                    StudyTime: 000000
                </dicom>
                <dicom>
                    StudyInstanceUID: 2
                    StudyDate: 20140101
                    StudyTime: 000000
                </dicom>
                <!-- older than 3 years: -->
                <dicom>
                    StudyInstanceUID: 3
                    StudyDate: 20120101
                    StudyTime: 000000
                </dicom>
            </list>
            <hl7 name="order">
                MSH|^~\&amp;|||||20131122140834||ORM^O01|15271192|P|2.3
                PID|1|pid
                ORC|
                OBR|1|||CT|
            </hl7>
            <string name="context">LACH</string>
        </inputs>
        <assertions>
            assert returned.size() == 2
            assert returned.collect { it.get(StudyInstanceUID) } as Set == ["1", "2"] as Set
        </assertions>
    </test>

    <test name="filterOldMGsLACH">
        <inputs>
            <list name="inputs">
                <dicom>
                    StudyInstanceUID: 1
                    StudyDate: 20150101
                    StudyTime: 000000
                </dicom>
                <dicom>
                    StudyInstanceUID: 2
                    StudyDate: 20140101
                    StudyTime: 000000
                </dicom>
                <!-- older than 5 years: -->
                <dicom>
                    StudyInstanceUID: 3
                    StudyDate: 20100101
                    StudyTime: 000000
                </dicom>
            </list>
            <hl7 name="order">
                MSH|^~\&amp;|||||20131122140834||ORM^O01|15271192|P|2.3
                PID|1|pid
                ORC|
                OBR|1|||MG|
            </hl7>
            <string name="context">LACH</string>
        </inputs>
        <assertions>
            assert returned.size() == 2
            assert returned.collect { it.get(StudyInstanceUID) } as Set == ["1", "2"] as Set
        </assertions>
    </test>

    <test name="tooManyStudiesKGH">
        <inputs>
            <list name="inputs">
                <dicom>
                    StudyInstanceUID: 1
                    StudyDate: 20130101
                    StudyTime: 000000
                </dicom>
                <dicom>
                    StudyInstanceUID: 3
                    StudyDate: 20130103
                    StudyTime: 000000
                </dicom>
                <dicom>
                    StudyInstanceUID: 6
                    StudyDate: 20130106
                    StudyTime: 000000
                </dicom>
                <dicom>
                    StudyInstanceUID: 4
                    StudyDate: 20130104
                    StudyTime: 000000
                </dicom>
                <dicom>
                    StudyInstanceUID: 2
                    StudyDate: 20130102
                    StudyTime: 000000
                </dicom>
                <dicom>
                    StudyInstanceUID: 5
                    StudyDate: 20130105
                    StudyTime: 000000
                </dicom>
                <dicom>
                    StudyInstanceUID: 8
                    StudyDate: 20130108
                    StudyTime: 000000
                </dicom>
                <dicom>
                    StudyInstanceUID: 7
                    StudyDate: 20130107
                    StudyTime: 000000
                </dicom>
                <dicom>
                    StudyInstanceUID: 10
                    StudyDate: 20130110
                    StudyTime: 000000
                </dicom>
                <dicom>
                    StudyInstanceUID: 9
                    StudyDate: 20130109
                    StudyTime: 000000
                </dicom>
                <dicom>
                    StudyInstanceUID: 12
                    StudyDate: 20130112
                    StudyTime: 000000
                </dicom>
                <dicom>
                    StudyInstanceUID: 11
                    StudyDate: 20130111
                    StudyTime: 000000
                </dicom>
            </list>
            <hl7 name="order">
                MSH|^~\&amp;|||||20131122140834||ORM^O01|15271192|P|2.3
                PID|1|pid
                ORC|
                OBR|1|||CT|
            </hl7>
            <string name="context">KGH</string>
        </inputs>
        <assertions>
            assert returned.size() == 10
            assert returned.collect { it.get(StudyInstanceUID) } as Set ==
                ["12", "11", "10", "9", "8", "7", "6", "5", "4", "3"] as Set
        </assertions>
    </test>

    <test name="tooManyStudiesLACH">
        <inputs>
            <list name="inputs">
                <dicom>
                    StudyInstanceUID: 1
                    StudyDate: 20130101
                    StudyTime: 000000
                </dicom>
                <dicom>
                    StudyInstanceUID: 3
                    StudyDate: 20130103
                    StudyTime: 000000
                </dicom>
                <dicom>
                    StudyInstanceUID: 6
                    StudyDate: 20130106
                    StudyTime: 000000
                </dicom>
                <dicom>
                    StudyInstanceUID: 4
                    StudyDate: 20130104
                    StudyTime: 000000
                </dicom>
                <dicom>
                    StudyInstanceUID: 2
                    StudyDate: 20130102
                    StudyTime: 000000
                </dicom>
                <dicom>
                    StudyInstanceUID: 5
                    StudyDate: 20130105
                    StudyTime: 000000
                </dicom>
                <dicom>
                    StudyInstanceUID: 8
                    StudyDate: 20130108
                    StudyTime: 000000
                </dicom>
                <dicom>
                    StudyInstanceUID: 7
                    StudyDate: 20130107
                    StudyTime: 000000
                </dicom>
                <dicom>
                    StudyInstanceUID: 10
                    StudyDate: 20130110
                    StudyTime: 000000
                </dicom>
                <dicom>
                    StudyInstanceUID: 9
                    StudyDate: 20130109
                    StudyTime: 000000
                </dicom>
                <dicom>
                    StudyInstanceUID: 12
                    StudyDate: 20130112
                    StudyTime: 000000
                </dicom>
                <dicom>
                    StudyInstanceUID: 11
                    StudyDate: 20130111
                    StudyTime: 000000
                </dicom>
            </list>
            <hl7 name="order">
                MSH|^~\&amp;|||||20131122140834||ORM^O01|15271192|P|2.3
                PID|1|pid
                ORC|
                OBR|1|||CT|
            </hl7>
            <string name="context">LACH</string>
        </inputs>
        <assertions>
            assert returned.size() == 5
            assert returned.collect { it.get(StudyInstanceUID) } as Set ==
                ["12", "11", "10", "9", "8"] as Set
        </assertions>
    </test>
</tests>
