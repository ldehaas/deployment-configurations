<tests service="connect-dicom" script="ImageToHL7OrderMorpher" file="image_to_hl7_order.groovy">
    <!-- This is actually targeted at normalization.groovy
         but we use other scripts to get at that content.
         Procedure codes are only used when generating hl7 orders and reports.
         Modalities are only changed when localizing images and cfind responses.

         It's very tedious to hit every single possibility, so we try to at least hit
         each row in the procedure code mapping tables, as well as cases for modalities
         with no default codes.  Rules that change the modality also have a separate test
         to make sure that that happens.

         More attention is payed to specific weird cases (usually the ones that change
         the modality) to make sure that we preserve the non-obvious behaviour.
      -->

    <test name="firstMG">
        <inputs>
            <dicom name="input">
                ErrorComment: MG
                Modality: MG
                StudyDescription: This is a mammogram
            </dicom>
            <string name="context">KGH</string>
        </inputs>
        <assertions>
            assert returned != false

            // procedure code
            assert output.get("OBR-4-1") == "8759"
            // procedure description:
            assert output.get("OBR-4-2") == "Mammo Reference"
        </assertions>
    </test>

    <test name="secondMG">
        <inputs>
            <dicom name="input">
                ErrorComment: MG
                Modality: MG
                StudyDescription: This is mammography
            </dicom>
            <string name="context">KGH</string>
        </inputs>
        <assertions>
            assert returned != false

            // procedure code
            assert output.get("OBR-4-1") == "8760"
            // procedure description:
            assert output.get("OBR-4-2") == "Mammo Work-up Reference"
        </assertions>
    </test>

    <test name="defaultMG">
        <inputs>
            <dicom name="input">
                ErrorComment: MG
                Modality: MG
                StudyDescription: I don't know what this is!
            </dicom>
            <string name="context">KGH</string>
        </inputs>
        <assertions>
            assert returned != false

            // procedure code
            assert output.get("OBR-4-1") == "8760"
            // procedure description:
            assert output.get("OBR-4-2") == "Mammo Work-up Reference"
        </assertions>
    </test>

    <test name="NM_PET">
        <inputs>
            <dicom name="input">
                ErrorComment: NM
                Modality: NM
                StudyDescription: pet
            </dicom>
            <string name="context">KGH</string>
        </inputs>
        <assertions>
            assert returned != false

            // procedure code
            assert output.get("OBR-4-1") == "8756"
            // procedure description:
            assert output.get("OBR-4-2") == "NM Reference PET"
        </assertions>
    </test>

    <test name="NM">
        <inputs>
            <dicom name="input">
                ErrorComment: NM
                Modality: NM
                StudyDescription: other
            </dicom>
            <string name="context">KGH</string>
        </inputs>
        <assertions>
            assert returned != false

            // procedure code
            assert output.get("OBR-4-1") == "8755"
            // procedure description:
            assert output.get("OBR-4-2") == "NM Reference"
        </assertions>
    </test>

    <test name="XA">
        <inputs>
            <dicom name="input">
                ErrorComment: XA
                Modality: XA
                StudyDescription: coiling
            </dicom>
            <string name="context">KGH</string>
        </inputs>
        <assertions>
            assert returned != false

            // procedure code
            assert output.get("OBR-4-1") == "8761"
            // procedure description:
            assert output.get("OBR-4-2") == "IVR Reference"
        </assertions>
    </test>

    <test name="unknownXA">
        <inputs>
            <dicom name="input">
                ErrorComment: XA
                Modality: XA
                StudyDescription: other
            </dicom>
            <string name="context">KGH</string>
        </inputs>
        <assertions>
            assert returned != false

            // procedure code
            assert output.get("OBR-4-1") == null
            // procedure description:
            assert output.get("OBR-4-2") == null
        </assertions>
    </test>

    <test name="XA_CR_Upper">
        <inputs>
            <dicom name="input">
                ErrorComment: XA
                # Modality has already been corrected by foreign image morpher:
                Modality: CR
                StudyDescription: clavicle
            </dicom>
            <string name="context">KGH</string>
        </inputs>
        <assertions>
            assert returned != false

            // procedure code
            assert output.get("OBR-4-1") == "8750"
            // procedure description:
            assert output.get("OBR-4-2") == "DX Reference Upper"
        </assertions>
    </test>

    <test name="XA_CR_Upper_Modality" script="ForeignImageMorpher" file="foreign_image_localizer.groovy">
        <inputs>
            <dicom name="input">
                PatientID: foreignPid
                IssuerOfPatientID: MSH
                OtherPatientIDsSequence[2]/PatientID: localPid
                OtherPatientIDsSequence[2]/IssuerOfPatientID: LHN

                Modality: XA
                StudyDescription: clavicle
            </dicom>
            <string name="context">KGH</string>
        </inputs>
        <assertions>
            assert returned != false

            assert get(Modality) == "CR"
            // original modality:
            assert get(ErrorComment) == "XA"
        </assertions>
    </test>

    <test name="XA_CR_Lower">
        <inputs>
            <dicom name="input">
                ErrorComment: XA
                # Modality has already been corrected by foreign image morpher:
                Modality: CR
                StudyDescription: calcaneus
            </dicom>
            <string name="context">KGH</string>
        </inputs>
        <assertions>
            assert returned != false

            // procedure code
            assert output.get("OBR-4-1") == "8751"
            // procedure description:
            assert output.get("OBR-4-2") == "DX Reference Lower"
        </assertions>
    </test>

    <test name="XA_CR_Lower_Modality" script="ForeignImageMorpher" file="foreign_image_localizer.groovy">
        <inputs>
            <dicom name="input">
                PatientID: foreignPid
                IssuerOfPatientID: MSH
                OtherPatientIDsSequence[2]/PatientID: localPid
                OtherPatientIDsSequence[2]/IssuerOfPatientID: LHN

                Modality: XA
                StudyDescription: clavicle
            </dicom>
            <string name="context">KGH</string>
        </inputs>
        <assertions>
            assert returned != false

            assert get(Modality) == "CR"
            // original modality:
            assert get(ErrorComment) == "XA"
        </assertions>
    </test>

    <test name="US_breast">
        <inputs>
            <dicom name="input">
                ErrorComment: US
                Modality: US
                StudyDescription: axilla
            </dicom>
            <string name="context">KGH</string>
        </inputs>
        <assertions>
            assert returned != false

            // procedure code
            assert output.get("OBR-4-1") == "8742"
            // procedure description:
            assert output.get("OBR-4-2") == "US Reference Breast"
        </assertions>
    </test>

    <test name="US">
        <inputs>
            <dicom name="input">
                ErrorComment: US
                Modality: US
                StudyDescription: other
            </dicom>
            <string name="context">KGH</string>
        </inputs>
        <assertions>
            assert returned != false

            // procedure code
            assert output.get("OBR-4-1") == "8741"
            // procedure description:
            assert output.get("OBR-4-2") == "US Reference"
        </assertions>
    </test>

    <test name="CR">
        <inputs>
            <dicom name="input">
                ErrorComment: CR
                Modality: CR
                StudyDescription: Oh my sternum!
            </dicom>
            <string name="context">KGH</string>
        </inputs>
        <assertions>
            assert returned != false

            // procedure code
            assert output.get("OBR-4-1") == "8748"
            // procedure description:
            assert output.get("OBR-4-2") == "DX Reference Chest"
        </assertions>
    </test>

    <test name="CR_chest_abdomen">
        <!-- 'abdomen' means body, but 'chest' should take precendence -->
        <inputs>
            <dicom name="input">
                ErrorComment: CR
                Modality: CR
                StudyDescription: chest and abdomen
            </dicom>
            <string name="context">KGH</string>
        </inputs>
        <assertions>
            assert returned != false

            assert output.get("OBR-4-1") == "8748"
        </assertions>
    </test>

    <test name="CR_body_abdomen">
        <!-- related to the above, 'abdomen' without a chest keyword
             falls into body -->
        <inputs>
            <dicom name="input">
                ErrorComment: CR
                Modality: CR
                StudyDescription: just abdomen
            </dicom>
            <string name="context">KGH</string>
        </inputs>
        <assertions>
            assert returned != false

            assert output.get("OBR-4-1") == "8749"
        </assertions>
    </test>

    <test name="CX">
        <inputs>
            <dicom name="input">
                ErrorComment: CX
                Modality: CX
                StudyDescription: Oh my fibula!
            </dicom>
            <string name="context">KGH</string>
        </inputs>
        <assertions>
            assert returned != false

            // procedure code
            assert output.get("OBR-4-1") == "8751"
            // procedure description:
            assert output.get("OBR-4-2") == "DX Reference Lower"
        </assertions>
    </test>

    <test name="DR">
        <inputs>
            <dicom name="input">
                ErrorComment: DR
                Modality: DR
                StudyDescription: Oh my oesophagus!
            </dicom>
            <string name="context">KGH</string>
        </inputs>
        <assertions>
            assert returned != false

            // procedure code
            assert output.get("OBR-4-1") == "8752"
            // procedure description:
            assert output.get("OBR-4-2") == "DX Reference Special/GI/GU"
        </assertions>
    </test>

    <test name="DX">
        <inputs>
            <dicom name="input">
                ErrorComment: DX
                Modality: DX
                StudyDescription: Oh my clavicle!
            </dicom>
            <string name="context">KGH</string>
        </inputs>
        <assertions>
            assert returned != false

            // procedure code
            assert output.get("OBR-4-1") == "8750"
            // procedure description:
            assert output.get("OBR-4-2") == "DX Reference Upper"
        </assertions>
    </test>

    <test name="RF">
        <inputs>
            <dicom name="input">
                ErrorComment: RF
                Modality: RF
                StudyDescription: Oh my medulla oblongata!
            </dicom>
            <string name="context">KGH</string>
        </inputs>
        <assertions>
            assert returned != false

            // procedure code
            assert output.get("OBR-4-1") == "8749"
            // procedure description:
            assert output.get("OBR-4-2") == "DX Reference Body"
        </assertions>
    </test>

    <test name="DXtoCR" script="ForeignImageMorpher" file="foreign_image_localizer.groovy">
        <inputs>
            <dicom name="input">
                PatientID: foreignPid
                IssuerOfPatientID: MSH
                OtherPatientIDsSequence[2]/PatientID: localPid
                OtherPatientIDsSequence[2]/IssuerOfPatientID: LHN

                Modality: DX
                StudyDescription: something something
            </dicom>
            <string name="context">KGH</string>
        </inputs>
        <assertions>
            assert get(Modality) == "CR"
            // original modality:
            assert get(ErrorComment) == "DX"
        </assertions>
    </test>

    <test name="MR_breast">
        <inputs>
            <dicom name="input">
                ErrorComment: MR
                Modality: MR
                StudyDescription: Oh my breast!
            </dicom>
            <string name="context">KGH</string>
        </inputs>
        <assertions>
            assert returned != false

            // procedure code
            assert output.get("OBR-4-1") == "8747"
            // procedure description:
            assert output.get("OBR-4-2") == "MR Reference Breast"
        </assertions>
    </test>

    <test name="MR_chest">
        <inputs>
            <dicom name="input">
                ErrorComment: MR
                Modality: MR
                StudyDescription: Oh my thoracic thorax!
            </dicom>
            <string name="context">KGH</string>
        </inputs>
        <assertions>
            assert returned != false

            // procedure code
            assert output.get("OBR-4-1") == "8745"
            // procedure description:
            assert output.get("OBR-4-2") == "MR Reference Chest"
        </assertions>
    </test>

    <test name="MR_extremity">
        <inputs>
            <dicom name="input">
                ErrorComment: MR
                Modality: MR
                StudyDescription: Oh my ulna!
            </dicom>
            <string name="context">KGH</string>
        </inputs>
        <assertions>
            assert returned != false

            // procedure code
            assert output.get("OBR-4-1") == "8746"
            // procedure description:
            assert output.get("OBR-4-2") == "MR Reference Extremity"
        </assertions>
    </test>

    <test name="MR_neuro">
        <inputs>
            <dicom name="input">
                ErrorComment: MR
                Modality: MR
                StudyDescription: Oh my temporomandibular!
            </dicom>
            <string name="context">KGH</string>
        </inputs>
        <assertions>
            assert returned != false

            // procedure code
            assert output.get("OBR-4-1") == "8743"
            // procedure description:
            assert output.get("OBR-4-2") == "MR Reference Neuro"
        </assertions>
    </test>

    <test name="MR_body">
        <inputs>
            <dicom name="input">
                ErrorComment: MR
                Modality: MR
                StudyDescription: Oh my liver!
            </dicom>
            <string name="context">KGH</string>
        </inputs>
        <assertions>
            assert returned != false

            // procedure code
            assert output.get("OBR-4-1") == "8744"
            // procedure description:
            assert output.get("OBR-4-2") == "MR Reference Body"
        </assertions>
    </test>

    <test name="MR_body_default">
        <inputs>
            <dicom name="input">
                ErrorComment: MR
                Modality: MR
                StudyDescription: Oh my medulla oblongata!
            </dicom>
            <string name="context">KGH</string>
        </inputs>
        <assertions>
            assert returned != false

            // procedure code
            assert output.get("OBR-4-1") == "8744"
            // procedure description:
            assert output.get("OBR-4-2") == "MR Reference Body"
        </assertions>
    </test>

    <test name="CT_body">
        <inputs>
            <dicom name="input">
                ErrorComment: CT
                Modality: CT
                StudyDescription: Oh my pelvis!
            </dicom>
            <string name="context">KGH</string>
        </inputs>
        <assertions>
            assert returned != false

            // procedure code
            assert output.get("OBR-4-1") == "8738"
            // procedure description:
            assert output.get("OBR-4-2") == "CT Reference Body"
        </assertions>
    </test>

    <test name="CT_chest">
        <inputs>
            <dicom name="input">
                ErrorComment: CT
                Modality: CT
                StudyDescription: Oh my thorax!
            </dicom>
            <string name="context">KGH</string>
        </inputs>
        <assertions>
            assert returned != false

            // procedure code
            assert output.get("OBR-4-1") == "8739"
            // procedure description:
            assert output.get("OBR-4-2") == "CT Reference Chest"
        </assertions>
    </test>

    <test name="CT_extremity">
        <inputs>
            <dicom name="input">
                ErrorComment: CT
                Modality: CT
                StudyDescription: Oh my radius!
            </dicom>
            <string name="context">KGH</string>
        </inputs>
        <assertions>
            assert returned != false

            // procedure code
            assert output.get("OBR-4-1") == "8746"
            // procedure description:
            assert output.get("OBR-4-2") == "CT Reference Extremity"
        </assertions>
    </test>

    <test name="CT_neuro">
        <inputs>
            <dicom name="input">
                ErrorComment: CT
                Modality: CT
                StudyDescription: Oh my spine!
            </dicom>
            <string name="context">KGH</string>
        </inputs>
        <assertions>
            assert returned != false

            // procedure code
            assert output.get("OBR-4-1") == "8729"
            // procedure description:
            assert output.get("OBR-4-2") == "CT Reference Neuro"
        </assertions>
    </test>

    <test name="CT_NM">
        <inputs>
            <dicom name="input">
                ErrorComment: CT
                Modality: NM
                StudyDescription: Oh my pet!
            </dicom>
            <string name="context">KGH</string>
        </inputs>
        <assertions>
            assert returned != false

            // procedure code
            assert output.get("OBR-4-1") == "8756"
            // procedure description:
            assert output.get("OBR-4-2") == "NM Reference Upper"
        </assertions>
    </test>

    <test name="CT_NM_modality" script="ForeignImageMorpher" file="foreign_image_localizer.groovy">
        <inputs>
            <dicom name="input">
                PatientID: foreignPid
                IssuerOfPatientID: MSH
                OtherPatientIDsSequence[2]/PatientID: localPid
                OtherPatientIDsSequence[2]/IssuerOfPatientID: LHN

                Modality: CT
                StudyDescription: The word pet is in this sentence.

            </dicom>
            <string name="context">KGH</string>
        </inputs>
        <assertions>
            assert returned != false

            assert get(Modality) == "NM"
            // original modality:
            assert get(ErrorComment) == "CT"
        </assertions>
    </test>

    <test name="CT_other">
        <inputs>
            <dicom name="input">
                ErrorComment: CT
                Modality: CT
                StudyDescription: Oh my medulla oblongata!
            </dicom>
            <string name="context">KGH</string>
        </inputs>
        <assertions>
            assert returned != false

            // procedure code
            assert output.get("OBR-4-1") == "8738"
            // procedure description:
            assert output.get("OBR-4-2") == "CT Reference Other"
        </assertions>
    </test>

    <test name="IO">
        <inputs>
            <dicom name="input">
                ErrorComment: IO
                Modality: CR
                StudyDescription: Oh my medulla oblongata!
            </dicom>
            <string name="context">KGH</string>
        </inputs>
        <assertions>
            assert returned != false

            // procedure code
            assert output.get("OBR-4-1") == "8750"
            // procedure description:
            assert output.get("OBR-4-2") == "DX Reference Upper"
        </assertions>
    </test>

    <test name="SC_upper">
        <inputs>
            <dicom name="input">
                ErrorComment: SC
                Modality: CR
                StudyDescription: Oh my humerus!
            </dicom>
            <string name="context">KGH</string>
        </inputs>
        <assertions>
            assert returned != false

            // procedure code
            assert output.get("OBR-4-1") == "8750"
            // procedure description:
            assert output.get("OBR-4-2") == "DX Reference Upper"
        </assertions>
    </test>

    <test name="SC_upper_modality" script="ForeignImageMorpher" file="foreign_image_localizer.groovy">
        <inputs>
            <dicom name="input">
                PatientID: foreignPid
                IssuerOfPatientID: MSH
                OtherPatientIDsSequence[2]/PatientID: localPid
                OtherPatientIDsSequence[2]/IssuerOfPatientID: LHN

                Modality: SC
                StudyDescription: The word humerus is in this sentence.

            </dicom>
            <string name="context">KGH</string>
        </inputs>
        <assertions>
            assert returned != false

            assert get(Modality) == "CR"
            // original modality:
            assert get(ErrorComment) == "SC"
        </assertions>
    </test>

    <test name="SC_lower">
        <inputs>
            <dicom name="input">
                ErrorComment: SC
                Modality: CR
                StudyDescription: Oh my calcaneus!
            </dicom>
            <string name="context">KGH</string>
        </inputs>
        <assertions>
            assert returned != false

            // procedure code
            assert output.get("OBR-4-1") == "8751"
            // procedure description:
            assert output.get("OBR-4-2") == "DX Reference Lower"
        </assertions>
    </test>

    <test name="SC_lower_modality" script="ForeignImageMorpher" file="foreign_image_localizer.groovy">
        <inputs>
            <dicom name="input">
                PatientID: foreignPid
                IssuerOfPatientID: MSH
                OtherPatientIDsSequence[2]/PatientID: localPid
                OtherPatientIDsSequence[2]/IssuerOfPatientID: LHN

                Modality: SC
                StudyDescription: The word calcaneus is in this sentence.

            </dicom>
            <string name="context">KGH</string>
        </inputs>
        <assertions>
            assert returned != false

            assert get(Modality) == "CR"
            // original modality:
            assert get(ErrorComment) == "SC"
        </assertions>
    </test>

    <test name="SC_bmd">
        <inputs>
            <dicom name="input">
                ErrorComment: SC
                Modality: OT
                StudyDescription: Oh my bone density!
            </dicom>
            <string name="context">KGH</string>
        </inputs>
        <assertions>
            assert returned != false

            // procedure code
            assert output.get("OBR-4-1") == "8757"
            // procedure description:
            assert output.get("OBR-4-2") == "BMD Reference"
        </assertions>
    </test>

    <test name="SC_bmd_modality" script="ForeignImageMorpher" file="foreign_image_localizer.groovy">
        <inputs>
            <dicom name="input">
                PatientID: foreignPid
                IssuerOfPatientID: MSH
                OtherPatientIDsSequence[2]/PatientID: localPid
                OtherPatientIDsSequence[2]/IssuerOfPatientID: LHN

                Modality: SC
                StudyDescription: The word bone is in this sentence.
            </dicom>
            <string name="context">KGH</string>
        </inputs>
        <assertions>
            assert returned != false

            assert get(Modality) == "OT"
            // original modality:
            assert get(ErrorComment) == "SC"
        </assertions>
    </test>

    <test name="BMD">
        <inputs>
            <dicom name="input">
                ErrorComment: BMD
                Modality: OT
                StudyDescription: Oh my bone density!
            </dicom>
            <string name="context">KGH</string>
        </inputs>
        <assertions>
            assert returned != false

            // procedure code
            assert output.get("OBR-4-1") == "8757"
            // procedure description:
            assert output.get("OBR-4-2") == "BMD Reference"
        </assertions>
    </test>

    <test name="BMD_modality" script="ForeignImageMorpher" file="foreign_image_localizer.groovy">
        <inputs>
            <dicom name="input">
                PatientID: foreignPid
                IssuerOfPatientID: MSH
                OtherPatientIDsSequence[2]/PatientID: localPid
                OtherPatientIDsSequence[2]/IssuerOfPatientID: LHN

                Modality: BMD
                StudyDescription: The word bone is in this sentence.
            </dicom>
            <string name="context">KGH</string>
        </inputs>
        <assertions>
            assert returned != false

            assert get(Modality) == "OT"
            // original modality:
            assert get(ErrorComment) == "BMD"
        </assertions>
    </test>

    <test name="BD">
        <inputs>
            <dicom name="input">
                ErrorComment: BD
                Modality: OT
                StudyDescription: Oh my bone density!
            </dicom>
            <string name="context">KGH</string>
        </inputs>
        <assertions>
            assert returned != false

            // procedure code
            assert output.get("OBR-4-1") == "8757"
            // procedure description:
            assert output.get("OBR-4-2") == "BMD Reference"
        </assertions>
    </test>

    <test name="BD_modality" script="ForeignImageMorpher" file="foreign_image_localizer.groovy">
        <inputs>
            <dicom name="input">
                PatientID: foreignPid
                IssuerOfPatientID: MSH
                OtherPatientIDsSequence[2]/PatientID: localPid
                OtherPatientIDsSequence[2]/IssuerOfPatientID: LHN

                Modality: BD
                StudyDescription: The word bone is in this sentence.
            </dicom>
            <string name="context">KGH</string>
        </inputs>
        <assertions>
            assert returned != false

            assert get(Modality) == "OT"
            // original modality:
            assert get(ErrorComment) == "BD"
        </assertions>
    </test>

    <test name="BD_other">
        <inputs>
            <dicom name="input">
                ErrorComment: BD
                Modality: OT
                StudyDescription: Oh my medulla oblongata!
            </dicom>
            <string name="context">KGH</string>
        </inputs>
        <assertions>
            assert returned != false

            // procedure code
            assert output.get("OBR-4-1") == "8757"
            // procedure description:
            assert output.get("OBR-4-2") == "BMD Reference"
        </assertions>
    </test>

    <test name="BD_other_modality" script="ForeignImageMorpher" file="foreign_image_localizer.groovy">
        <inputs>
            <dicom name="input">
                PatientID: foreignPid
                IssuerOfPatientID: MSH
                OtherPatientIDsSequence[2]/PatientID: localPid
                OtherPatientIDsSequence[2]/IssuerOfPatientID: LHN

                Modality: BD
                StudyDescription: The word fuzzy is in this sentence.
            </dicom>
            <string name="context">KGH</string>
        </inputs>
        <assertions>
            assert returned != false

            assert get(Modality) == "OT"
            // original modality:
            assert get(ErrorComment) == "BD"
        </assertions>
    </test>

    <test name="OT_bmd">
        <inputs>
            <dicom name="input">
                ErrorComment: OT
                Modality: OT
                StudyDescription: Oh my bone density!
            </dicom>
            <string name="context">KGH</string>
        </inputs>
        <assertions>
            assert returned != false

            // procedure code
            assert output.get("OBR-4-1") == "8757"
            // procedure description:
            assert output.get("OBR-4-2") == "BMD Reference"
        </assertions>
    </test>

    <test name="OT_other">
        <inputs>
            <dicom name="input">
                ErrorComment: OT
                Modality: OT
                StudyDescription: Oh my medulla oblongata!
            </dicom>
            <string name="context">KGH</string>
        </inputs>
        <assertions>
            assert returned != false

            // procedure code
            assert output.get("OBR-4-1") == "8749"
            // procedure description:
            assert output.get("OBR-4-2") == "DX Reference Body"
        </assertions>
    </test>

    <test name="OT_other_modality" script="ForeignImageMorpher" file="foreign_image_localizer.groovy">
        <inputs>
            <dicom name="input">
                PatientID: foreignPid
                IssuerOfPatientID: MSH
                OtherPatientIDsSequence[2]/PatientID: localPid
                OtherPatientIDsSequence[2]/IssuerOfPatientID: LHN

                Modality: OT
                StudyDescription: The word fuzzy is in this sentence.
            </dicom>
            <string name="context">KGH</string>
        </inputs>
        <assertions>
            assert returned != false

            assert get(Modality) == "CR"
            // original modality:
            assert get(ErrorComment) == "OT"
        </assertions>
    </test>

    <test name="PR" script="ForeignImageMorpher" file="foreign_image_localizer.groovy">
        <inputs>
            <dicom name="input">
                PatientID: foreignPid
                IssuerOfPatientID: MSH
                OtherPatientIDsSequence[2]/PatientID: localPid
                OtherPatientIDsSequence[2]/IssuerOfPatientID: LHN

                Modality: PR
                StudyDescription: The word fuzzy is in this sentence.
            </dicom>
            <string name="context">KGH</string>
        </inputs>
        <assertions>
            assert returned == false
        </assertions>
    </test>

    <test name="SR" script="ForeignImageMorpher" file="foreign_image_localizer.groovy">
        <inputs>
            <dicom name="input">
                PatientID: foreignPid
                IssuerOfPatientID: MSH
                OtherPatientIDsSequence[2]/PatientID: localPid
                OtherPatientIDsSequence[2]/IssuerOfPatientID: LHN

                Modality: SR
                StudyDescription: The word fuzzy is in this sentence.
            </dicom>
            <string name="context">KGH</string>
        </inputs>
        <assertions>
            assert returned == false
        </assertions>
    </test>

    <test name="unknownModality">
        <inputs>
            <dicom name="input">
                ErrorComment: lewha?
                Modality: lewha?
                StudyDescription: blahs
            </dicom>
            <string name="context">KGH</string>
        </inputs>
        <assertions>
            assert returned != false

            // Kingston does not have a default for an unknown modality:
            // procedure code
            assert output.get("OBR-4-1") == null
            // procedure description
            assert output.get("OBR-4-2") == null
        </assertions>
    </test>

    <test name="caseInsensitivity">
        <inputs>
            <dicom name="input">
                ErrorComment: mR
                Modality: mR
                StudyDescription: ExtREmitY
            </dicom>
            <string name="context">KGH</string>
        </inputs>
        <assertions>
            assert returned != false

            // procedure code
            assert output.get("OBR-4-1") == "8746"
            // procedure description:
            assert output.get("OBR-4-2") == "MR Reference Extremity"
        </assertions>
    </test>

    <!-- Test fixing of messed up modalities that some studies have.
         Commented out because not yet implemented. -->

    <testx name="ModalityCTPR">
        <inputs>
            <dicom name="input">
                # based on a guess from some old prototype fixes;
                # really need real data to confirm what's going on here:
                ErrorComment: CTPR
                Modality: CT
                StudyDescription: extremity
            </dicom>
            <string name="context">KGH</string>
        </inputs>
        <assertions>
            assert returned != false

            assert input.get(Modality) == "CT"
            // make sure we're still localizing like a CT too:
            assert output.get("OBR-4-1") == '8746'
        </assertions>
    </testx>

    <testx name="ModalityCTslashPR">
        <inputs>
            <dicom name="input">
                # based on a guess from some old prototype fixes;
                # really need real data to confirm what's going on here:
                ErrorComment: CT/PR
                Modality: CT
                StudyDescription: extremity
            </dicom>
            <string name="context">KGH</string>
        </inputs>
        <assertions>
            assert returned != false

            assert input.get(Modality) == "CT"
            // make sure we're still localizing like a CT too:
            assert output.get("OBR-4-1") == '8746'
        </assertions>
    </testx>
</tests>
