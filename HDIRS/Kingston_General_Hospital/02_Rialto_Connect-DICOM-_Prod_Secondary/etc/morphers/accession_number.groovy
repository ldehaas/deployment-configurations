
/**
 * Accession number localization rules for HDIR sites
 */
class AccessionNumbers {
    /**
     * Maps issuer to a function that localizes the accession number.
     * The default function is just to use the issuer as a simple prefix
     * and no entry needs to be added for these cases.
     */
    private static config = [
        'BGH1': prefix("BGH"),

        'BWI':  prefix("BWI"),

        'CMH':  suffix("C"),

        'DFM':  prefix("DFM"),

        'EXR':  prefix("EXR"),

        'HHHS': prefixIfNotAlreadySuffixed("HHHS"),

        'KMRIKMI': prefix("KMRIKMI"),

        'KNG1': prefix("KGH"),

        'LACH': prefixIfNotAlreadySuffixed("LACH"),

        'LHN':  prefix("LHC"),

        'MSH':  prefix("MSH"),

        // rather a special case:
        'NHH':  { accn ->
                    if (accn.length() > 15) {
                        return accn
                    } else if (accn.length() > 13) {
                        return accn + 'H'
                    }

                    return 'NHH' + accn
                },

        'NYGH': prefix("NYGH"),

        'OSH':  prefix("OSH"),

        'PCHC': prefix("PCHC"),

        'PRHC': prefix("PRHC"),

        'PROV': prefixIfNotAlreadySuffixed("PROV"),

        'PSFH': prefixIfNotAlreadySuffixed("PSFH"),

        'QUINTE': prefix("QHC"),

        'RMH1': prefixIfNotAlreadySuffixed("RMH1"),

        'RVHS': prefixIfNotAlreadySuffixed("RVHC", "RVHC", "RVHA"),

        'SMH1': suffix("S"),

        // another pretty unique case:
        'SRHC': { accn ->
                    if (accn.toUpperCase().endsWith("SRHC")) {
                        return accn
                    } else if (accn.contains('Y')) {
                        return suffix('S')(accn)
                    } else {
                        return prefix('SRHC')(accn)
                    }
                },

        'STMH': prefix("STMH"),

        'SUNB': prefix("SHSC"),

        'TEGH': prefix("TEGH"),

        'TNI':  prefix("TNI"),

        'TSHH': prefixIfNotAlreadySuffixed("TSH", "TSHH", "TSHG"),

        'YCH':  prefix("YCH"),

        'YKR':  prefix("YKR"),

        'XRA':  prefix("XRA"),

    ].withDefault { issuer ->
        // the default is to just prefix with the issuer:
        prefix(issuer)
    }

    // Common localization methods.  Each takes config and returns a closure
    // that localizes and accesion number:

    /**
     * Creates function to add prefix only if none of the given suffixes are
     * present.  If there are no suffixes to check, we check for the prefix
     * itself as a suffix instead.
     */
    private static final prefixIfNotAlreadySuffixed(prefix, String ... suffixes) {
        return { accn ->
            if (suffixes.length > 0) {
                for (suffix in suffixes) {
                    if (accn.toUpperCase().endsWith(suffix)) {
                        return accn
                    }
                }
            } else if (accn.toUpperCase().endsWith(prefix)) {
                return accn
            }

            return prefix + accn
        }
    }

    private static final prefix(prefix) {
        return { accn -> prefix + accn }
    }

    private static final suffix(suffix) {
        return { accn -> accn + suffix }
    }

    /**
     * Localizes the accession number according to appendix A.
     */
    public static localize(issuer, accn, log) {
        if (accn == null) {
            return null
        }

        accn = config[issuer](accn)

        if (accn.length() > 16) {
            log.warn("Localized accession number '{}' is longer than 16 characters.", accn)
        }

        return accn
    }
}

