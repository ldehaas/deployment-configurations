<config>
    <var name="aeTitle">NYGH_RIALTO_TEST</var>
    <var name="morphers">${rialto.rootdir}/etc/morphers</var>
    <var name="ClusterPartnerHost" value="ClusterPartner" />

    <!-- Core service #0. Declaration of arr cannot be moved -->
    <service id="arr" type="arr">
        <server idref="internal-http" name="arr-api">
            <url>/arr/auditRecords/*</url>
        </server>

        <config>
            <prop name="IndexDir">${rialto.rootdir}/var/arr/index</prop>
            <prop name="IndexBackupDir">${rialto.rootdir}/var/arr/indexbackup</prop>
            <prop name="ArchiveDir">${rialto.rootdir}/var/arr/archive</prop>
            <prop name="TempDir">${rialto.rootdir}/var/arr/temp</prop>
            <prop name="Listener">UDP:4000</prop>
            <prop name="Purge">30d</prop>
        </config>
    </service>

    <server type="http" id="internal-http">
        <port>8081</port>
    </server>

    <!-- Core service #1. Declaration of txn-records cannot be moved -->
    <service type="txn-records" id="txn-records">
        <server idref="internal-http" name="forwardReceiver">
            <url>/txn-records/replication</url>
        </server>

        <config>
            <prop name="StorageDir">${rialto.rootdir}/var/event-viewer</prop>
            <prop name="Purge">30d</prop>
        </config>
    </service>

    <server id="dicomserver" type="dicom">
        <port>4104</port>
        <aetitle>${aeTitle}</aetitle>
    </server>

    <device id="SELF" type="dicom">
        <host>localhost</host>
        <port>4104</port>
        <aetitle>${aeTitle}</aetitle>
    </device>

    <server id="hl7server" type="hl7v2">
        <port>5555</port>
        <timeout>1h</timeout>
    </server>


    <service id="connect" type="connect-dicom">
        <server idref="hl7server" />
        <server idref="dicomserver" name="cstore" />
        <server idref="dicomserver" name="cfind" />
        <server idref="dicomserver" name="cmove" />

        <device idref="PACS_CSTORE" name="PACSStorage" />
        <!-- Following required for the local PACS query device to get the patient demographics from -->
        <device idref="PACS_CSTORE" name="PACSQuery" />
        <device idref="PACS_HL7" name="PACSHL7" />
        <device idref="DIR" name="DIR" />

        <config>
            <prop name="HL7ToCFindMorpher">
                ${morphers}/hl7order_to_cfind.groovy
            </prop>

            <prop name="CFindResponseFilter">
                ${morphers}/cfind_response_filter.groovy
            </prop>

            <prop name="CFindResponseRankFilter">
                ${morphers}/cfind_response_rank_filter.groovy
            </prop>

            <!-- Morph the local pacs patient cfind to customize what demographics information is returned. -->
            <prop name="PatientCFindToPACSRequestMorpher">
                ${morphers}/patient_cfind_morpher.groovy                                                                                     
            </prop>

            <prop name="ImageToHL7OrderMorpher">
                ${morphers}/image_to_hl7_order.groovy
            </prop>

            <prop name="ForeignImageMorpher">
                ${morphers}/foreign_image_localizer.groovy
            </prop>

            <prop name="AdHocCFindRequestMorpher">
                ${morphers}/adhoc_cfind_req_morpher.groovy
            </prop>

            <prop name="AdHocCFindResponseMorpher">
                ${morphers}/adhoc_cfind_response_localizer.groovy
            </prop>

            <prop name="AdHocCMoveRequestMorpher">
                ${morphers}/adhoc_cmove_req_morpher.groovy
            </prop>

            <prop name="ReportRetrievalHostPort">138.218.201.77:8080</prop>
            <prop name="ReportHTMLToHL7Morpher">${morphers}/agfa_report_retriever.groovy</prop>
            <prop name="ForeignReportMorpher">${morphers}/report_localizer.groovy</prop>

            <prop name="LocalAETitle">${aeTitle}</prop>

            <prop name="DisableCStoreTransferSyntaxes">
                JPEGLSLossless,
                JPEGLossless,
                JPEGLosslessNonHierarchical14,
                JPEG2000LosslessOnly,
                DeflatedExplicitVRLittleEndian,
                RLELossless,
                <!-- Allowing this syntax for Local PACS to be able to handle US multiframe images - KHC1681
                JPEGBaseline1,
                -->
                JPEGExtended24,
                JPEGLSLossyNearLossless,
                JPEG2000,
                MPEG2,
            </prop>

            <prop name="PriorStudyPIDCache">1d</prop>
            <prop name="PriorStudyCMoveThrottle">0s</prop>
            <prop name="PriorStudyImmediateAck">true</prop>

            <!-- Allows concurrent C-Moves of multiple studies. Needs to be paired with the "Pool Size" attribute
                  of the "OrderRetry" property -->
            <prop name="PriorStudyCMovePoolSize">3</prop>

            <prop name="OrderRetry">
                <storageDir>${rialto.rootdir}/var/connect/orders</storageDir>
                <poolSize>3</poolSize>
            </prop>

            <prop name="ReportSendingTiming">IMMEDIATELY</prop>
        </config>
    </service>


    <server type="http" id="navigator-http">
        <port>8080</port>
    </server>

    <service id="navigator" type="navigator">
        <device idref="SELF" name="proxyDIR" />

        <server idref="navigator-http" name="navigator-war">
            <url>/</url>
        </server>

        <config>
            <prop name="ArrURL">http://localhost:8081/arr</prop>
        </config>
    </service>

    <device type="dicom" id="DIR">
        <host>idcdicom.dc1.hdirs.ca</host>
        <port>11112</port>
        <aetitle>IDC2TEST</aetitle>
    </device>

    <device type="dicom" id="PACS_CSTORE">
        <aetitle>D20GEPACS_R</aetitle>
        <host>10.189.210.20</host>
        <!-- <host>localhost</host-->
        <port>4100</port>
    </device>

    <device type="dicom" id="DCMRCV">
        <aetitle>DCMRCV</aetitle>
        <host>localhost</host>
        <port>4100</port>
    </device>

    <device type="hl7v2" id="PACS_HL7">
        <host>10.189.210.88</host>
        <port>6006</port>
        <sendingFacility>Rialto</sendingFacility>
        <sendingApplication>Rialto</sendingApplication>
        <timeout>5m</timeout>
    </device>

    <device type="audit" id="audit">
        <host>localhost</host>
        <port>4000</port>
    </device>

</config>
