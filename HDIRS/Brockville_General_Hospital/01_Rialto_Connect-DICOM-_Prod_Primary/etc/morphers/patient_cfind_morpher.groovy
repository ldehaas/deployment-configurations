/*
 * This script modifies the cfind request we send to the PACS
 * for discovering local patient demographics.  All it generally
 * does is add return keys for interesting demographics fields
 * that we need to localize in foreign studies.
 *
 * The first image of the study is available with the name "image".
 * That first image has *not* yet been localized.
 */

log.debug("Starting patient_cfind_morpher")
LOAD("common.groovy")


set(PatientName, '')
set(PatientBirthDate, '')
set(PatientSex, '')
set(MedicalRecordLocator, '')

def pids = new Pids(image)

// We probably don't need to fall back on the prefix, since the PACS
// is very unlikely to have patient demographics for a patient it
// doesn't known about, but it does no harm to try (maybe there's an
// off chance that we've imported a study before with this made-up
// id and different demograhics)
def localPid = pids.getLocalPidWithPrefixedSourceFallback()

if (localPid == null) {
    log.warn("No local patient known. Cannot query for local demographics. Skipping query.")
    return false
}

log.debug("Found local pid '{}'", localPid)

set(PatientID, localPid[0])

