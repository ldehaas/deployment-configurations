log.info("GTAWest Morpher - START")

def messageType = get('/.MSH-9-1')
def triggerEvent = get('/.MSH-9-2')

//set('MSH-5', 'RIALTO_CONNECT');
//set('MSH-6', 'RIALTO');

log.info("messageType is: {}", messageType)
log.info("triggerEvent is: {}", triggerEvent)

// KHC11133 - Adding support for ADT message processing.  Request to delay processing for 30 seconds if messagetype is ADT.

if (messageType == 'ADT') {
        sleep 30000
}

log.info("GTAWest Morpher - End")
