
//
// common.groovy - Lower-level include of common utility pieces
//

class Constants {
    static final LocalIssuerIDs = ['LHN']      // Lakeridge only has one local hospital
    static final PrimaryLocalIssuerID = LocalIssuerIDs[0]

    static final UnwantedDomains = ['TNI']

    // Global Issuer of PID that goes with the top-level Patients
    // (in NYGH's case, the top-level Patient ID is the Health Card Number)
    //static final GlobalIssuerOfPID = "GLOBALDOMAIN"    // PRODUCTION ONLY
    static final GlobalIssuerOfPID = "HDIRS"             // TEST ONLY - historical quirk

    // KHC1578 - removing 'PR' from the list of invalid Modalities.
    // The PACS has isues with foreign SR objects so HDIRS decided not to pass them along:
    static final InvalidModalities = ["SR", "KO"]
}

/**
 * Encapsulates a dicom object to provide easy access to
 * the patient identifiers.
 */
class Pids {
    private issuerToPid = [:]
    private sourceDomain
    private sop

    public Pids(sop) {
        this.sop = sop

        sourceDomain = sop.get(IssuerOfPatientID)
        issuerToPid[sourceDomain] = sop.get(PatientID)

        sop.get(OtherPatientIDsSequence).each {
            issuerToPid[it.get(IssuerOfPatientID)] = it.get(PatientID)
        }
    }

    public boolean isSourceUnwantedDomain() {
        return Constants.UnwantedDomains.contains(sourceDomain)
    }

    public boolean isLocal() {
        return Constants.LocalIssuerIDs.contains(sourceDomain)
    }

    /**
     * Find a patient identifier in one of the local domains.
     */
    public getLocalPid() {
        Constants.LocalIssuerIDs.findResult { issuer ->
            def localPid = issuerToPid[issuer]
            if (localPid != null) {
                return [localPid, issuer]
            }
            return null
        }
    }

    /**
     * Get the health card number, aka the pid in the global
     * domain.
     */
    public getHealthCardNumber() {
        // TODO: consider validation of the format
        return issuerToPid[Constants.GlobalIssuerOfPID]
    }

    /**
     * Return the source domain, or null if it is not known.
     */
    public getSourceDomain() {
        return sourceDomain
    }

    /**
     * Get the local pid, falling back on creating a fake one by
     * prefixing the source pid with the source issuer if there is
     * no known local.
     *
     * @return null if there is no local pid and no known source domain
     */
    public getLocalPidWithPrefixedSourceFallback() {
        def local = getLocalPid()
        if (local != null) {
            return local
        }

        if (sourceDomain != null) {
            return [sourceDomain + "_" + issuerToPid[sourceDomain], Constants.PrimaryLocalIssuerID]
        }

        return null
    }

    /**
     * Get the local pid, falling back on the health card number
     * prefixed with "HN" if that is known.
     *
     * @return null if no local pid or health card is known
     */
    public getLocalPidWithHCNFallback() {
        local = getLocalPid()
        if (local != null) {
            return local
        }

        hcn = getHealthCardNumber()
        if (hcn != null) {
            return ["HCN" + hcn, Constants.GlobalIssuerOfPID]
        }

        return null
    }

    // TODO: add a getLocal implementation that picks a fallback based on global configuration
    // Some sites might want a prefix fallback and some might want healthcard and we should
    // be able to switch strategies with a single flag.

    /**
     * Install the given pid as the local one and clear out
     * extra data that the PACS might not want to see.
     */
    public localize(localPid) {
        sop.set(PatientID, localPid[0])
        sop.set(MedicalRecordLocator, getHealthCardNumber())
        sop.remove(IssuerOfPatientID)
        sop.remove(OtherPatientIDsSequence)
    }
}

class PrefetchRules {

    /** Determine which incoming modalities need 5 years and 5 priors */
    private static modalitiesWith5years = [
        // At Lakeridge, HL7 messages from MEDITECH uses OBR-4-1 'XMAM' to represent 'MG' studies
        "XMAM",
    ]

    /** Get the Number and Maximum Age of Priors to be Prefetched */
    static getNumberAndMaxAgeOfPriors(order, log) {
        // Default number of years is 3, default number of studies is 5
        def years = 3
        def studies = 5

        if (order != null) {
            if (modalitiesWith5years.contains(getOrderModality(order))) {
                years = 5
            }
            if  (order.get("/.MSH-9-1") == 'ADT') {
                years = 1
                studies = 3
            }
        } else {
            log.warn("No order received when attempting to determine priors number and age to be prefetched. " +
                "Cannot determine original study Modality. Falling back to default of 5 studies within 3 years.")
        }

        return [years, studies]
    }


    static getOrderModality(order) {
        if  (order.get("/.MSH-9-1") == 'ADT') {
            return order = "CR"
        } else {
            return order != null ? order.get("OBR-4") : null
        }
    }
}
