/* CFind Response RANK Filter
 *
 *   This morpher is part of the Fetch Prior Studies workflow only.
 *
 *   It takes THE ENTIRE SET of individual study responses (CFind Responses from the DIR) and optionally
 *   filters it out studies based on criteria, such as study age, or a maximum number of responses.
 *   This limits the number of studies that are preloaded into a local PACS as part of a Fetch Prior Studies workflow.
 *
 *   Common Uses for this Cfind Response Filter script can be:_
 *
 *       - Exclude studies that are NOT the X most recent studies (i.e. absolute number of responses, by age)
 */

LOAD("common.groovy")

log.debug("Starting cfind_response_rank_filter")

//
// HDIRS Requirements (from the SAD):
//  5 most recent studies up to 3 years old for all but MG studies
//  5 most recent studies up to 5 years old for MG studies
//

// Use the original order to get the number and years of priors
def priorsNumberAndAge = PrefetchRules.getNumberAndMaxAgeOfPriors(order, log)
def studyYears = priorsNumberAndAge[0]
def numberOfPriorsToReturn = priorsNumberAndAge[1]

// Get the Modality for this Order
def requestedModality = PrefetchRules.getOrderModality(order)
    if (requestedModality == null || requestedModality.empty) {
        requestedModality = 'CR'
    }

def validPriorList = []
// Count backwards from today to years of prior studies required
def earliestStudyDate =  new org.joda.time.DateTime().minusYears(studyYears)

log.debug("Modality is '{}' and studyYears is '{}' for this study.", requestedModality, studyYears)
log.debug("Calculated earliestStudyDate is '{}'.", earliestStudyDate)
log.debug("Complete list of studies being examined:\n{}",
    inputs.collect({
        it.get(StudyInstanceUID) + ": " + it.getDate(StudyDate, StudyTime)
    }).join("\n"))

inputs.each {

    if (it.getDate(StudyDate, StudyTime) < earliestStudyDate) {

        log.trace("Prior for modality '{}' falls outside of maximum age '{}'.", requestedModality, earliestStudyDate)

    } else {

        // Add it to the list of studies that are within the correct date range
        log.trace("Prior for modality '{}' falls inside of maximum age '{}'", requestedModality, earliestStudyDate)
        validPriorList.add(it)
    }
}

validPriorList.sort(byRecency)

/* KHC 10640 - Do not retrieve studies when the Accession # is blank
def accessionCheck = it.get(AccessionNumber)
if (accessionCheck == null || accessionCheck == "") {
    log.error("Exiting because Accession number for study is null/empty")
    return false;
} */

log.debug("Studies after removing ones that are too old:\n{}",
    validPriorList.collect({
        it.get(StudyInstanceUID) + ": " + it.getDate(StudyDate, StudyTime)
    }).join("\n"))

// Returns the correct number of priors based on prior age
return first(validPriorList, numberOfPriorsToReturn)

