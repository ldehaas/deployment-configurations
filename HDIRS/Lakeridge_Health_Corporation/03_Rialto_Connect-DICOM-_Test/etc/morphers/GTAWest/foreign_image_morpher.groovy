/**
 * this script localizes foreign images (dicom headers) so that they can be
 * ingested by the PACS
 *
 * this script can return false to filter (drop) an image
 *
 * sometimes it's desirable to use this script for localizing both foreign
 * images and cfind responses so common logic can be shared
 */


/*
 * in the dicom environment, the input dicom header is directly from the DIR
 *
 * in the xds environment, the input dicom header is modified:
 *   - the PatientID and IssuerOfPatientID tag are overwritten with xds
 *     metadata's SourcePatientID, to indicate where the study is originated
 *   - the OtherPatientIDsSequence is added / updated to contain a pair of
 *     PatientID and IssuerOfPatientID with localized patient information
 *
 * the above two points apply to adhoc_cfind_resp_morpher as well
 */

LOAD("GTAW_common.groovy")
LOAD("../normalization.groovy")
LOAD("GTAW_accession_number_OID.groovy")

issuer = get(IssuerOfPatientID)
accession = get(AccessionNumber)

def DIRbIRTHDAY = get(PatientBirthDate)
def PACSbIRTHDAY = null

log.info("FIM: Pre-localized header: \n{}", input)

/*
 * localDemographics is a list of dicom objects representing cfind results from
 * local pacs.  If such a query was configured, these should contain the local
 * demographics for the patient as they exist in the PACS and may be used to
 * override the values in the SOP so as not to confuse the PACS.
 */
if (localDemographics != null && !localDemographics.isEmpty()) {
    PACSbIRTHDAY = localDemographics.get(0).get(PatientBirthDate)
    if (DIRbIRTHDAY == PACSbIRTHDAY) {
            set(PatientName, localDemographics.get(0).get(PatientName))
            set(PatientSex, localDemographics.get(0).get(PatientSex))
            }
    } else {
        log.debug("XDS: No Local Demographics exist at PACS for this study.  Falling back to DI-r values.")
            set(PatientName, get(PatientName))
            set(PatientSex, get(PatientSex))
            set(PatientBirthDate, DIRbIRTHDAY)
    }

/*
 * In the xds environment, xdsMetadata may contain the xds metadata representing
 * the study.
 */
def identifiers = new Identifiers(input)

def localPatientId = null

if (prefetchOrder != null) {
        localPatientId = prefetchOrder.get("PID-3-1")
        log.info("Using PatientID from originating HL7 message: '{}'", localPatientId)
        set(PatientID, localPatientId)
}

//if (localPatientId == null) {
//        localPatientId = identifiers.getLocalPatientId()
//        log.info("Using PatientID from SOP instance: '{}'", localPatientId)
//}

if (localPatientId == null) {
        localPatientId = identifiers.getLocalPatientIdWithFailover()
        log.info("Using prefixed PatientID from SOP instance: '{}'", localPatientId)
        set(PatientID, localPatientId)
}

// Localize the modality
def originalModality = get(Modality)
def normalizedModality = Normalization.getNormalizedModality(originalModality, get(StudyDescription))
set(Modality, normalizedModality)
log.debug("Original Modality was [{}], normalizedModality is [{}]", originalModality, normalizedModality)


// Stash the original modality so that the order creation script can use it
// to properly normalize the procedure description and code.
// We would prefer to update the code and description in the dicom header, but
// they want the originals available to the PACS.
// This is a big hack.  ErrorComment is in group 0, which means we won't transmit
// it over the wire, but it should stick around long enough for the order creator
// to see it:
set(ErrorComment, originalModality)

//Localize the AccessionNumber
// If we have the old model where there is no accession number suffix set that variable to null
try {
    log.info("newAccessionNumberSuffix = {}", newAccessionNumberSuffix)
} catch (Exception e) {
    newAccessionNumberSuffix = null
}
log.info("newAccessionNumberSuffix = {}", newAccessionNumberSuffix)
// If the newAccessionNumberSuffix is not null then start localizing stuff
if (newAccessionNumberSuffix != null) {
    // we're in the middle of ingestion
    if (pacsLocalAccessionNumber != null) {
        log.info("Found pacsLocalAccessionNumber {}, setting the AccessionNumber in the image to it!", pacsLocalAccessionNumber)
        set(AccessionNumber, pacsLocalAccessionNumber)
    } else {
        log.info("Did not find an accession number in the PACS")
        newAccNo = AccessionNumbers.prefixIfNotPrefixedWithKnownPrefixes(issuer, newAccessionNumberSuffix, log)
        log.info("new accession number is {}", newAccNo)
        log.info("foreignAccn = {}", get(AccessionNumber))
        def finalString = newAccNo.replaceAll(/\$/,/\$/ + get(AccessionNumber) + "_")
        log.info("final string = {}", finalString)
        set(AccessionNumber, finalString)
    }
} else {
//    def newAccNo = AccessionNumbers.prefixIfNotPrefixedWithKnownPrefixes(issuer, accession, log)
    def newAccNo = AccessionNumbers.prefixIfNotAlreadyPrefixed(issuer, accession, log)
    log.info("Prefixed new accession: {}", newAccNo)
    set(AccessionNumber, newAccNo)
}

log.info("Using PatientID: '{}'", localPatientId)
log.debug("Other Patient ID Sequence: {}", get(OtherPatientIDsSequence))

/* Requested by Shannon @GE - the issuer of PID was causing studies to become unassociated at the PACs.
   This line needs to be kept below the line: 
      def localAccessionNumber = identifiers.getLocalAccessionNumberWithFailover() 
   Otherwise the Accession number prefix fails. */
set(IssuerOfPatientID, null)

// KHC7745 - Request from Jason Nagels Stamp StationName (0008,1010) for studies coming from GTA West with GTAW
set(StationName, "GTAW")


// Add logging of outbound DICOM headers to confirm Issuer of Patient ID is being sent as null.

log.info("FIM: Outbound Image DICOM header {}", output)
log.info("End Foreign Image Morpher Script...")

