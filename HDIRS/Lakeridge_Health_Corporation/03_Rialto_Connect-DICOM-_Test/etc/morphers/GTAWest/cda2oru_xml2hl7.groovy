import groovy.util.slurpersupport.GPathResult

LOAD("GTAW_common.groovy")

log.info("Started conversion from CDA document to HL7 ORU message ...")

initialize("ORU", "R01", "2.3")
set('MSH-3', 'LHC_RIALTO_GTAW')
set('MSH-4', 'KAROS_HEALTH')
set('MSH-10', '1555473220052803268')

//log.info("prefetch order: {}", prefetchOrder)
def LocalPIDIssuer = null
def name = null
def encounter   = input.componentOf.encompassingEncounter
def participant = input.componentOf.encompassingEncounter.encounterParticipant
def localAccessionNumber = header.get(AccessionNumber)
def CDAAccessionNumber = null
def cdaAccessionSlotA = input.inFulfillmentOf.order.id.@extension.text()
def cdaAccessionSlotB = input.componentOf.encompassingEncounter.id.@extension.text()
def DIRbIRTHDAY = input.recordTarget.patientRole.patient.birthTime.@value.text()
def newAccNo = localAccessionNumber

// *** the 'header' variable includes other patient ID sequence with the local PID ** Uncomment the following line to log the DICOM header.
// log.info("MENGLER: DICOM header:\n {}", header)


log.info("CDA2ORU: local Accession Number: {}", localAccessionNumber)
log.info("CDA2ORU: CDA Accession Number Slot A: {}", cdaAccessionSlotA)
log.info("CDA2ORU: CDA Accession Number Slot B: {}", cdaAccessionSlotB)

def cdaAccessionNumber = null

if (cdaAccessionSlotA != null && !cdaAccessionSlotA.isEmpty()) {
    cdaAccessionNumber = cdaAccessionSlotA
}
if (cdaAccessionSlotB != null && !cdaAccessionSlotB.isEmpty()) {
    cdaAccessionNumber = cdaAccessionSlotB
}

if (cdaAccessionNumber == null || localAccessionNumber == null || !localAccessionNumber.contains(cdaAccessionNumber)) {
    log.info("XDS: Discarding Report As Accession number does not match first instance in study.")
    return false
}


//Ensure that the reports which do not belong to the study are filtered out
def headerStudyInstanceUID = header.get(StudyInstanceUID)
def metadataStudyInstanceUID = metadata.get(XDSExtendedMetadata("studyInstanceUid"))[0]
log.info("headerStudyInstanceUID = {}", headerStudyInstanceUID)
log.info("metadataStudyInstanceUID = {}", metadataStudyInstanceUID)
if (headerStudyInstanceUID != metadataStudyInstanceUID) {
    return false
}

// PID segment

    set('PID-5-1', input.recordTarget.patientRole.patient.name.family.text())                             // Patient Name (Family)
    set('PID-5-2', input.recordTarget.patientRole.patient.name.given.text())                              // Patient Name (Given)
    set('PID-7-1', input.recordTarget.patientRole.patient.birthTime.@value.text())                        // Patient Birth Date
    set('PID-8', input.recordTarget.patientRole.patient.administrativeGenderCode.@code.text())            // Patient Gender

def identifiers = new Identifiers(header)

def localPatientId = null
def PACSbIRTHDAY = null

if (prefetchOrder != null) {
        localPatientId = prefetchOrder.get("PID-3-1")
        set('PID-3-1', localPatientId)
        log.info("XDS: Using PatientID from originating HL7 message: '{}'", localPatientId)
}

//if (localPatientId == null) {
//        localPatientId = identifiers.getLocalPatientId()
//        set('PID-3-1', localPatientId[0])
//        set('PID-3-4-2', localPatientId[1])
//        set('PID-3-4-3', 'ISO') 
//        log.info("Using PatientID from SOP instance: '{}'", localPatientId)
//}

if (localDemographics != null && !localDemographics.isEmpty()) {
    PACSbIRTHDAY = localDemographics.get(0).get(PatientBirthDate)

    name = localDemographics.get(0).get(PatientName)

        if (DIRbIRTHDAY == PACSbIRTHDAY) {
            if (name != null) {
                name = name.split('\\^')
                set('PID-5-1', name[0])

            if (name.length > 1) {
                set('PID-5-2', name[1])
            }

            if (name.length > 2) {
                set('PID-5-3', name[2])
            }
        }
            set('PID-7-1', PACSbIRTHDAY)        
            set('PID-8', localDemographics.get(0).get(PatientSex))
    }
} else {
        log.debug("XDS: Local Demographics exist at PACS for this study but the birth dates are not a match.  Falling back to report values.")

        set('PID-5-1', input.recordTarget.patientRole.patient.name.family.text())                             // Patient Name (Family)
        set('PID-5-2', input.recordTarget.patientRole.patient.name.given.text())                              // Patient Name (Given)
        set('PID-7-1', input.recordTarget.patientRole.patient.birthTime.@value.text())                        // Patient Birth Date
        set('PID-8', input.recordTarget.patientRole.patient.administrativeGenderCode.@code.text())            // Patient Gender
}


if (localPatientId == null) {
        localPatientId = header.get(PatientID)
        set('PID-3-1', localPatientId)                                                                       // Patient ID
        set('PID-3-4-2', header.get(IssuerOfPatientID))
        set('PID-3-4-3', 'ISO')
        log.info("XDS: Using prefixed PatientID from SOP instance: '{}'", localPatientId)
}

// OBR segment

def codingSystem = input.component[0].section.code.@codeSystemName.text()

set('OBR-2', newAccNo)                                                                                  // Accession Number
set('OBR-3', newAccNo)                                                                                  // Accession Number 
set('OBR-4-1', input.component[0].section.code.@code.text())						// Procedure Code (Identifier)
set('OBR-4-2', input.component[0].section.text.text())							// Procedure Code (Text)
set('OBR-4-3', codingSystem == 'LOINC' ? 'LN' : codingSystem)						// Procedure Code (Coding System)

set('OBR-7', encounter.effectiveTime.@value.text())							// Procedure Completed Datetime

set('OBR-16-1', input.participant.associatedEntity.associatedPerson.name.family.text())			// Referring Physician Name (Family)
set('OBR-16-2', input.participant.associatedEntity.associatedPerson.name.given.text())			// Referring Physician Name (Given)
set('OBR-25', input.title.text().toLowerCase().contains('final') ? 'F' : 'P')				// Report Status

// ORC segment
//set('ORC-1', "NW")
//set('ORC-2', input.inFulfillmentOf.order.id.@extension.text())
//set('ORC-3', input.inFulfillmentOf.order.id.@extension.text())
//set('ORC-5', "CM")
//set('ORC-12-1', null)
//set('ORC-12-2', null)

// OBX segment
set("OBX-1", "1")
set("OBX-2", "TX")

def rep=input.component.nonXMLBody.text.@representation.text()
def non_xml = input.component.nonXMLBody.text.text()
def decoded = null

if (rep != null && rep == 'B64') {
    decoded = new String(non_xml.decodeBase64())
} else {
    decoded = non_xml
}

def repetitions = decoded.split("\r")
    for (int r = 0 ; r < repetitions.size() ; ++r) {
        set('OBX-5('+ r +')', repetitions[r])
}

log.info("... finished conversion from CDA document to HL7 ORU message as follows:")
log.info("ORU: {}", output)

return true
