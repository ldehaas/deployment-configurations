/* We are using this script to modify the incoming AdHoc requests to GTA West made from the PACS as they use 
   either the patientID for PID or the studyID for HCN lookups.  Connect needs the request to be in the patientID
   field for the cfind to proceed as expected.  
*/

log.info("Starting HomeDIR cfind request morpher...")

def sTUDYid = get(StudyID)
def patientID = get(PatientID)
def sERIESid = get(SeriesInstanceUID)

if (patientID != null && ! patientID.isEmpty()) {
    set(IssuerOfPatientID, "LHN")
}

if (sTUDYid != null && ! sTUDYid.isEmpty()) {
    set(StudyIDIssuer, "HDIRS")
}


// The PACS automatically pulls Accession and SUID from the local study which causes lookup against GTA West.  Need to add this to remove those values.

set(AccessionNumber, null)

if (!(get(QueryRetrieveLevel) == 'SERIES')) {
    set(StudyInstanceUID, null)
} else {
    set(PatientID, null)
    set(StudyIDIssuer, null)
    set(IssuerOfPatientID, null)
    set(StudyID, null)
}

log.info("Done HomeDIR cfind request morpher...")
