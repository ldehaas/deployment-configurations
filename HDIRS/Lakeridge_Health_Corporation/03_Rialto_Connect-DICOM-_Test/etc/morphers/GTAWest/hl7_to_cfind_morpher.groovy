/*
 * this scripts constructs a cfind query from an hl7 order message to search
 * for prior studies of interest
 * 
 * this script can return false to signal not to do any prefetching for a 
 * given order
 */


def pid19 = get( "PID-19" )
def pid3 = get( "PID-3" )

log.info("found HCN pid '{}'", pid19)
log.info("found Patient ID '{}'", pid3)

/*
def issuer = get( "PID-3(0)-4-2" )

if (issuer == null) {
    issuer = "2.16.840.1.113883.3.239.16.10000"
    }

log.info("found issuer '{}'", issuer)
*/
// if (pid ==~ /regular expression/) {
//    return false
//}


// construct the cfind
set ( PatientID, pid3 )
//set ( IssuerOfPatientID, "LHN")
set ( StudyID, pid19 )
set ( StudyIDIssuer, "HDIRS" )

// etc etc
