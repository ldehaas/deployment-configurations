//
// common.groovy - Lower-level include of common utility pieces
//

class Constants {
    static final LocalIssuerIDs = ['LHN']      // Lakeridge only has one local hospital
    static final PrimaryLocalIssuerID = LocalIssuerIDs[0]
    static final PrimaryLocalAccessionIssuer = LocalIssuerIDs[0]

    static final UnwantedDomains = []

    // Global Issuer of PID that goes with the top-level Patients
    // (in NYGH's case, the top-level Patient ID is the Health Card Number)
    static final GlobalIssuerOfPID = "GLOBALDOMAIN"    // PRODUCTION ONLY
    //static final GlobalIssuerOfPID = "HDIRS"             // TEST ONLY - historical quirk

    // KHC1578 - removing 'PR' from the list of invalid Modalities.
    // The PACS has isues with foreign SR objects so HDIRS decided not to pass them along:
    static final InvalidModalities = ["SR"]
}

class Identifiers {

    private prefixes = [
    '2.16.840.1.113883.3.239.18.162':'BGHS',
    '2.16.840.1.113883.3.239.18.25':'CGMH',
    '2.16.840.1.113883.3.239.18.26':'CVH',
    '2.16.840.1.113883.3.239.18.137':'HSC',
    '2.16.840.1.113883.3.239.18.164':'HBKR',
    '2.16.840.1.113883.3.239.18.52':'HHCC',
    '2.16.840.1.113883.3.239.18.47':'HHSO',
    '2.16.840.1.113883.3.239.18.46':'HHSM',
    '2.16.840.1.113883.3.239.18.45':'HHSG',
    '2.16.840.1.113883.3.239.16.8882':'HHSU',
    '2.16.840.1.113883.3.239.18.98':'MAHC',
    '2.16.840.1.113883.3.239.18.163':'GBGH',
    '2.16.840.1.113883.3.239.18.121':'ASMH',
    '2.16.840.1.113883.3.239.18.133':'RVH',
    '2.16.840.1.113883.3.239.18.140':'SJHC',
    '2.16.840.1.113883.3.239.18.146':'THCM',
    '2.16.840.1.113883.3.239.18.147':'THCT',
    '2.16.840.1.113883.3.239.18.156':'CAMH',
    '2.16.840.1.113883.3.239.18.158':'TRI',
    '2.16.840.1.113883.3.239.18.148':'UHN',
    '2.16.840.1.113883.3.239.18.157':'WCH',
    '2.16.840.1.113883.3.239.18.97':'MTSH',
    '2.16.840.1.113883.3.239.18.150':'WOHSC',
    '2.16.840.1.113883.3.239.18.152':'WOHSE',
    '2.16.840.1.113883.3.239.16.8881':'WOHSR',
    '2.16.840.1.113883.3.239.18.160':'WPHC',
    '2.16.840.1.113883.3.239.18.54':'HRRH',
    '2.16.840.1.113883.3.239.18.180':'KMH',
    '2.16.840.1.113883.3.239.18.181':'DXRA',
    '2.16.840.1.113883.3.239.18.200':'GAMX',
    '2.16.840.1.113883.3.239.18.234':'PMC'
    ]

    private domain2pid = [:]
    private accession
    private domain
    private sop

    public Identifiers(sop) {
        this.sop = sop
        domain = sop.get(IssuerOfPatientID)
        accession = sop.get(AccessionNumber)
        domain2pid[domain] = sop.get(PatientID)

        sop.get(OtherPatientIDsSequence).each {
            domain2pid[it.get(IssuerOfPatientID)] = it.get(PatientID)
        }
    }

    public getLocalPatientId() {
        Constants.LocalIssuerIDs.findResult { issuerac ->
            def localPatientId = domain2pid[issuerac]
            if (localPatientId != null) {
                return [localPatientId, issuerac]
            }
            return null
        }
    }

//    public getLocalAccessionNumber() {
//        Constants.LocalIssuerIDs.findResult { issuerac ->
//            def localPatientId = domain2pid[issuerac]
//            if (localPatientId != null) {
//                return [accession, issuerac]
//            }
//            return null
//        }
//    }

    public getLocalAccessionNumber() {
        Constants.LocalIssuerIDs.findResult { issuerac ->
            def localPatientId = domain2pid[issuerac]
            if (localPatientId != null && issuerac == domain) {
                return [accession, issuerac]
            }
            return null
        }
    }


    public getLocalPatientIdWithFailover() {
        
        def localPatientId = getLocalPatientId()
        if (localPatientId != null) {
            return localPatientId
        }

        if (domain != null && !domain.isEmpty() && domain2pid[domain] != null && !domain2pid[domain].isEmpty()) {
            return [prefixes[domain] + domain2pid[domain], Constants.PrimaryLocalIssuerID]
        }

        return null
    }

    public getLocalAccessionNumberWithFailover() {

        def localAccessionNumber = getLocalAccessionNumber()
        if (localAccessionNumber != null) {
            return localAccessionNumber
        }

        if (domain != null && !domain.isEmpty() && accession != null && !accession.isEmpty()) {
            return [prefixes[domain] + accession, Constants.PrimaryLocalIssuerID]
        }

        return null
    }


    public getLocalPacsDemographics(fieldName) {

    if (localDemographics != null && !localDemographics.isEmpty()) {
        log.debug("Local Demographics Exist at the PACS: [{}]", localDemographics[0].get(fieldName) )
        return localDemographics[0].get(fieldName)

    } else {
        log.debug("No Local Demographics exist at PACS for this study.  Falling back to GTAW values.")
        return get(fieldName)
    }
    }

    /**
     * Install the given pid as the local one and clear out
     * extra data that the PACS might not want to see.
     */
    public localizer(localPatientId, localAccessionNumber) {
        
        if (localPatientId != null && localPatientId.size() == 2) {
            sop.set(PatientID, localPatientId[0])
            sop.set(IssuerOfPatientID,localPatientId[1])
        }

        if (localAccessionNumber != null && localAccessionNumber.size() == 2) {
            sop.set(AccessionNumber, localAccessionNumber[0])
        }

        sop.remove(IssuerOfAccessionNumberSequence)
        sop.remove(OtherPatientIDsSequence)
    }
}

class PrefetchRules {

    /** Determine which incoming modalities need 5 years and 5 priors */
    private static modalitiesWith5years = [
        // At Lakeridge, HL7 messages from MEDITECH uses OBR-4-1 'XMAM' to represent 'MG' studies
        "XMAM",
    ]

    /** Get the Number and Maximum Age of Priors to be Prefetched */
    static getNumberAndMaxAgeOfPriors(order, log) {
        // Default number of years is 3, default number of studies is 5
        def years = 3
        def studies = 5

        if (order != null) {
            if (modalitiesWith5years.contains(getOrderModality(order))) {
                years = 5
            } 
        } else {
            log.warn("No order received when attempting to determine priors number and age to be prefetched. " +
                "Cannot determine original study Modality. Falling back to default of 5 studies within 3 years.")
        }

        return [years, studies]
    }


    static getOrderModality(order) {
        return order != null ? order.get("OBR-4") : null  
    }
}

