/*   This script localizes studies for ingestion */

log.debug("Starting foreign_image_localizer")
LOAD("common.groovy")
LOAD("accession_number.groovy")
LOAD("normalization.groovy")

def getLocalDemographicsAttribute(fieldName) {
    if (localDemographics != null && !localDemographics.isEmpty()) {
        log.debug("Local Demographics Exist at the PACS: [{}]", localDemographics[0].get(fieldName) )
        return localDemographics[0].get(fieldName)

    } else {
        log.debug("No Local Demographics exist at PACS for this study.  Falling back to DI-r values.")
        return get(fieldName)
    }
}

def pids = new Pids(input)

if (pids.getSourceDomain() == null) {
    // This means we can't safely prefix the accession number and,
    // if we don't have a local patient id, we can't make up a local
    // one based on the foreign one.
    log.error("Image has no issuer. Unable to safely localize. Dropping.")
    return false
}

if (pids.isLocal()) {
    log.debug("Dropping local study [{}]", get(AccessionNumber) )
    return false
}


// Localize Patient ID
def localPid = pids.getLocalPidWithPrefixedSourceFallback()
pids.localize(localPid)

// Localize the accession number
def newAccNum = AccessionNumbers.localize(pids.getSourceDomain(), get(AccessionNumber), log)
log.debug("Localized accession number is '{}'", newAccNum)
if (newAccNum != null && newAccNum.length() > 16) {
    log.warn("Localized accession number exceeds 16 chars: '{}'", newAccNum)
}
set(AccessionNumber, newAccNum)
set(StudyID,         newAccNum)

// Drop Unwanted Modalities.  Write full details to log.
def currentSOPmodality = get(Modality)
if (Constants.InvalidModalities.contains(currentSOPmodality)) {
    log.info("Dropping sop of type [{}]. PatientID [{}], Local Accession Number [{}], Foreign Accession Number [{}].", currentSOPmodality, localPid, newAccNum, get(AccessionNumber) )
    return false
}

// Localize Patient demographics
set(PatientName,      getLocalDemographicsAttribute(PatientName))
set(PatientBirthDate, getLocalDemographicsAttribute(PatientBirthDate))
set(PatientSex,       getLocalDemographicsAttribute(PatientSex))

// Localize the modality
def originalModality = get(Modality)
def normalizedModality = Normalization.getNormalizedModality(originalModality, get(StudyDescription))
set(Modality, normalizedModality)
log.debug("Original Modality was [{}], normalizedModality is [{}]", originalModality, normalizedModality)


// Stash the original modality so that the order creation script can use it
// to properly normalize the procedure description and code.
// We would prefer to update the code and description in the dicom header, but
// they want the originals available to the PACS.
// This is a big hack.  ErrorComment is in group 0, which means we won't transmit
// it over the wire, but it should stick around long enough for the order creator
// to see it:
set(ErrorComment, originalModality)

// KHC7745 - Request from Jason Nagels Stamp StationName (0008,1010) for studies coming from HDIRS with HDIRS
set(StationName, "HDIRS")

// Print full details for tracing study localizatoin
log.debug("Localizing SOP with Modality [{}], for Study with Accession Number [{}], Patient ID[{}].  SOP IUID [{}], Study Instance UID [{}].", currentSOPmodality, newAccNum, localPid, get(SOPInstanceUID), get(StudyInstanceUID) )

// Output full SOP header if necessary
log.trace("These are the full localized headers for this SOP: [{}]", output)
