/*
 * This script modifies the cfind request we send to the PACS
 * for discovering local patient demographics.  All it generally
 * does is add return keys for interesting demographics fields
 * that we need to localize in foreign studies.
 *
 * The first image of the study is available with the name "image".
 * That first image has *not* yet been localized.
 */

log.debug("Starting patient_cfind_morpher")
LOAD("GTAW_common.groovy")

// We probably don't need to fall back on the prefix, since the PACS
// is very unlikely to have patient demographics for a patient it
// doesn't known about, but it does no harm to try (maybe there's an
// off chance that we've imported a study before with this made-up
// id and different demograhics)

def identifiers = new Identifiers(input)

def localPatientId = identifiers.getLocalPatientId()

if (localPatientId == null) {
    log.warn("No local patient known. Cannot query for local demographics. Skipping query.")
    return false
}

log.debug("Found local pid '{}'", localPatientId)

set(PatientID, localPatientId[0])

// Setting these values as null here causes the actual values to be returned by the c-find lookup.
set(PatientName, null)
set(PatientBirthDate, null)
set(PatientSex, null)

remove(IssuerOfPatientID)

