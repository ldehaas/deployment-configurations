
class Normalization {
    /**
     * Each foreign modality has a list of rules containing keywords.  The
     * first keywords that match anything in the study description is
     * the rule to use.  The last rule should always have an empty
     * list of keywords, making it the default for that modality.
     *
     * Most modalities will map to the same normalized value, but we keep
     * them explicit because there are a few cases where they don't.
     */
    static final map = [
        'MG': [
            [ keywords: ["mammogram", "diagnostic", "screening", "obsp"],
              normalizedModality: "MG",
              procCode: "XMAMMAMREF",
              procDesc: "Mammo Reference"
            ],

            [ keywords: ['breast', 'needle', 'specimen', 'cone', 'mag',
                         'mammography', 'mammary', 'ductography', 'stereotactic', 'biopsy'],
              normalizedModality: "MG",
              procCode: "XMAMMAMREFPROC",
              procDesc: "Mammo Reference Procedure",
            ],

            [ keywords: [],
              normalizedModality: "MG",
              procCode: "XMAMMAMREFOT",
              procDesc: "Mammo Reference Other",
            ],
        ],

        'NM': [
            [ keywords: [],
              normalizedModality: "NM",
              procCode: "1NMNMREF",
              procDesc: "NM Reference",
            ],
        ],

        'NM PET': [
            [ keywords: ['pet'],
              normalizedModality: "PT",
              procCode: "XPTPTREFNM",
              procDesc: "PT Reference NM",
            ],
        ],

        'PT': [
            [ keywords: [],
              normalizedModality: "PT",
              procCode: "XPTPTREFCT",
              procDesc: "PT Reference CT",
            ],
        ],

        'PET': [
            [ keywords: [],
              normalizedModality: "PT",
              procCode: "XPTPTREFCT",
              procDesc: "PT Reference CT",
            ],
        ],

        'XA': [
            [ keywords: [],
              normalizedModality: "XA",
              procCode: "XANANREF",
              procDesc: "IR Reference",
            ],
        ],

        'US': [
            [ keywords: ['axilla', 'breast'],
              normalizedModality: "US",
              procCode: "XUSUSREFBREAS",
              procDesc: "US Reference Breast",
            ],

            [ keywords: [],
              normalizedModality: "US",
              procCode: "XUSUSREFBODY",
              procDesc: "US Reference Body",
            ],
        ],

        'CR': [
            [ keywords: ['chest', 'abdomen', 'pacemaker', 'fluoroscopy', 'ribs', 'sternum'],
              normalizedModality: "CR",
              procCode: "XRADRADREFCHST",
              procDesc: "XR Reference Chest",
            ],

            [ keywords: ['ankle', 'calcaneus', 'femur', 'foot', 'knee', 'leg', 'pelvhip',
                         'penis', 'knee', 'tibia', 'fibula', 'toe'],
              normalizedModality: "CR",
              procCode: "XRADRADREFEXTL",
              procDesc: "XR Reference Extremity Lower",
            ],

            [ keywords: ['cine', 'palatogram', 'enema', 'cystogram', 'urethrogram',
                         'bowel', 'defecogram', 'endoscopic', 'retrograde',
                         'cholangiopancreaton', 'fistulogram', 'sinogram', 'fluoroscopy',
                         'tilt', 'table', 'hysterosalpinogram', 'loopogram', 'pouchogram',
                         'lymphangiogram', 'oesophagus', 'operative', 'cholangiogram',
                         'retrograde', 'pyelogram', 'basket', 'extraction', 'sialogram',
                         'sniff', 'gi', 'ventriculoperitoneal', 'shunt', 'fluoroscopic',
                         'swallowing', 'urodynamics'],
              normalizedModality: "CR",
              procCode: "XRADRADREFGIGU",
              procDesc: "XR Reference GI GU",
            ],

            [ keywords: ['ac', 'joints', 'bone age', 'clavicle', 'elbow', 'finger',
                         'humerus', 'radius', 'ulna', 'scapula', 'shoulder',
                         'sternoclavicular', 'wrist', 'hand'],
              normalizedModality: "CR",
              procCode: "XRADRADREFEXTU",
              procDesc: "XR Reference Extremity Upper",
            ],

            [ keywords: [],
              normalizedModality: "CR",
              procCode: "XRADRADREFBODY",
              procDesc: "XR Reference Body",
            ],
        ],

        'MR': [
            [ keywords: ['breast', 'cardiac', 'chest'],
              normalizedModality: "MR",
              procCode: "XMRMRREFBREAS",
              procDesc: "MR Reference Breast",
            ],

            [ keywords: ['extremity'],
              normalizedModality: "MR",
              procCode: "XMRMRREFEXT",
              procDesc: "MR Reference Extremity",
            ],

            [ keywords: ['head', 'neck', 'spine', 'temporomandibular', 'joint'],
              normalizedModality: "MR",
              procCode: "XMRMRREFHDSP",
              procDesc: "MR Reference Neck Head Spine",
            ],

            [ keywords: [],
              normalizedModality: "MR",
              procCode: "XMRMRREFBODY",
              procDesc: "MR Reference Body",
            ],
        ],

        'CT': [
            [ keywords: ['abdomen', 'chest', 'cardiac', 'pelvis'],
              normalizedModality: "CT",
              procCode: "XCTCTREFBODY",
              procDesc: "CT Reference Body",
            ],

            [ keywords: ['extremity'],
              normalizedModality: "CT",
              procCode: "XCTCTREFEXT",
              procDesc: "CT Reference Extremity",
            ],

            [ keywords: ['head', 'neck', 'spine'],
              normalizedModality: "CT",
              procCode: "XCTCTREFHDSP",
              procDesc: "CT Reference Neck Head Spine",
            ],

            [ keywords: ['pet'],
              normalizedModality: "PT",
              procCode: "XPTPTREFCT",
              procDesc: "PT Reference CT",
            ],

            [ keywords: [],
              normalizedModality: "CT",
              procCode: "XCTCTREFOT",
              procDesc: "CT Reference Other",
            ],
        ],

        'OT': [
            [ keywords: ['bone', 'bmd'],
              normalizedModality: "NM",
              procCode: "1NMNMREFBMD",
              procDesc: "NM Reference BMD",
            ],

            [ keywords: [],
              normalizedModality: "CR",
              procCode: "XRADRADREFOT",
              procDesc: "XR Reference Other",
            ],
        ],

        'PR': [
            [ keywords: [],
              normalizedModality: "PR",
              // KHC1578: these codes match the default rule, since
              // we don't know the actual modality and study description
              // of the rest of the study if a PR comes in first.
              procCode: "XRADRADREFMOD",
              procDesc: "XR Reference Other Modality",
            ],
        ],
    ]

    static final defaultRule = 
            [ keywords: [],
              normalizedModality: "CR",
              procCode: "XRADRADREFMOD",
              procDesc: "XR Reference Other Modality",
            ]

    static {
        // add entries for cases when multiple foreign modalities map to
        // the same rules:
        ["DX", "DR"].each {
            map[it] = map["CR"]
        }
    }

    private static findRule(foreignModality, studyDesc) {
        def rules = foreignModality == null ? null : map[foreignModality.toUpperCase()]

        if (rules != null) {
            return rules.find { rule ->
                rule.keywords.isEmpty() ||
                    (studyDesc != null &&
                     rule.keywords.find({ studyDesc.toLowerCase().contains(it) }) != null)
            }
        }

        return defaultRule
    }

    /**
     * Find the normalized modality based on the foreign modality and
     * study description.
     */
    public static getNormalizedModality(foreignModality, studyDesc) {
        // TODO: support composite foreign modalities
        // Would require customer to define rules for which is authoritative
        return findRule(foreignModality, studyDesc).normalizedModality
    }

    /**
     * Find the normalized procedure code and description based on the
     * foreign modality and study description.
     */
    public static getNormalizedCodeAndDesc(foreignModality, studyDesc) {
        def rule = findRule(foreignModality, studyDesc)
        return [rule.procCode, rule.procDesc]
    }
}

