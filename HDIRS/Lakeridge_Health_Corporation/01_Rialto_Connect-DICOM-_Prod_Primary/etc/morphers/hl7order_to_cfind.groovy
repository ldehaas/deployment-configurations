/*
 * Convert an order message into a cfind message.
 * get() works on the order message, while set() works on the
 * cfind message.
 *
 * The order is ignored if this script returns false,
 * e.g. no images will be fetched for the order.
 */

log.debug("Starting hl7order_to_cfind")
LOAD('common.groovy')

//Debugging Messages
log.debug("Incoming HL7 Message: \n {}", input)

def pid = get("PID-3-1")
log.debug("Found pid '{}'", pid)


// Check for a valid health card number with 10-digits
def hcn = get("PID-19")
log.debug("Health Card Number is '{}'", hcn);

if (!(hcn ==~ '[0-9]{10}')) {
    log.warn("Health Card Number '{}' not valid. Ignoring this order.", hcn);
    return false
}

// Construct the C-FIND
set(PatientID, pid)
// IssuerOfPID is hard coded to local issuer:
set(IssuerOfPatientID, Constants.PrimaryLocalIssuerID)

// Set the return keys
set(StudyInstanceUID, null)

// The following isn't necessary, but useful for debugging
set(AccessionNumber, null)

log.debug("Constructing C-FIND - PatientID, pid, StudyInstanceUID, AccessionNumber '{}', '{}', '{}','{}'", PatientID, pid, StudyInstanceUID, AccessionNumber);

