log.info("Start pid lookup morpher!")

def GTAWestOHCNIssuer = "2.16.840.1.113883.4.59"
def HDIRSOHCNIssuerORM = "2.16.840.1.113883.3.239.16.10000"
//def HDIRSOHCNIssuer = "HDIRS"
def HDIRSOHCNIssuer = "GLOBALDOMAIN"

def pids = getAllPidsQualified()
log.info("Found pids '{}'", pids)

def ohcn = new ArrayList()
def dedupe = new HashSet()

for (int i = 0 ; i < pids.size() ; ++i) {
    if (pids[i][0].matches("[0-9]{10}")) {
        ohcn.add(pids[i])
        continue
        }
    if (pids[i][1] == GTAWestOHCNIssuer || pids[i][1] == HDIRSOHCNIssuer || pids[i][1] == HDIRSOHCNIssuerORM ) {
        log.info("Found healthcard number '{}'", pids[i][0], pids[i][1])
        pids[i][1] = GTAWestOHCNIssuer
        if (dedupe.add(pids[i])) {
            ohcn.add(pids[i])
        }
    }
}
  
// remove all the 10-digit pids that have the wrong issuer ...
ohcn.removeAll { it[1] != null && it[1] != HDIRSOHCNIssuer && it[1] != HDIRSOHCNIssuerORM }

if (ohcn.size() != 1) {    
    log.warn("Patient contained {} healthcard numbers.", ohcn.size())
     return false
}

set(StudyIDIssuer, GTAWestOHCNIssuer)
set(StudyID, ohcn[0][0])
log.info("End pid lookup morpher")
return true
