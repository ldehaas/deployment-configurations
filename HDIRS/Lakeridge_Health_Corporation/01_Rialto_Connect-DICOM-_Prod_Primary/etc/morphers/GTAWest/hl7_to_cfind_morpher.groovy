/*
 * this scripts constructs a cfind query from an hl7 order message to search
 * for prior studies of interest
 * 
 * this script can return false to signal not to do any prefetching for a 
 * given order
 */


def pid19 = get( "PID-19" )
def pid3 = get( "PID-3" )

log.info("found HCN pid '{}'", pid19)
log.info("found Patient ID '{}'", pid3)

// construct the cfind
set ( PatientID, pid3 )
set ( StudyID, pid19 )
//set ( StudyIDIssuer, "HDIRS" )
set ( StudyIDIssuer, "GLOBALDOMAIN" )

// etc etc
