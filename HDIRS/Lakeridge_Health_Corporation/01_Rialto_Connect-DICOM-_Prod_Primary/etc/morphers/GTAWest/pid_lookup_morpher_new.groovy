/* 
    1. Determine whether the passed PID is a Health Card Number (Regional Identifier). If passed, this would be passed in the overloaded
        "StudyID" field.  This takes presedence over other identifiers???
    2. Validate and pass a local Patient ID only. (The DI-r does PIX-like behaviours of matching different patients to the same person)
    3. Pass a Patient Name if 1. and 2. above not available
*/

log.debug("Starting pid_lookup_morpher...")
LOAD('../common.groovy')

// Define Issuers of OHCN for GTA West and HDIRS
def GTAWestOHCNIssuer = "2.16.840.1.113883.4.59"
def HDIRSOHCNIssuer = "2.16.840.1.113883.3.239.16.10000"

// Define valid Query as including either a Health Card Number or Patient ID
def validData = false

// Health Card Number Search Logic
def healthCardNum = get(StudyID)
if (healthCardNum != null) {

    // Check for 10-digit health card that is all numbers only
    if (healthCardNum.matches("[0-9]{10}")) {

        set(PatientID, healthCardNum)
        set(IssuerOfPatientID, GTAWestOHCNIssuer)
        validData = true

        log.info("Currently Health Card Number is Valid!")
        log.info("Setting Patient ID as '{}' and Issuer of PID as '{}'", get(PatientID), Constants.GlobalIssuerOfPID)

    } else {

        log.error("Ad Hoc Query: The Health Card Number '{}' was INVALID, rejecting study", healthCardNum)
        return false
    }

    // Leave the passed in PatientID
}

// Patient ID Logic
if (!validData) {

    def mrn = get(PatientID)
    if (mrn != null) {

        // Passed PID is not null
        // Qualify the PID with IssuerOfPID for the DIr to service the requests
        parts = mrn.split('_')
        if (parts.length == 2) {

            set(PatientID, parts[1])
            set(IssuerOfPatientID, parts[0])

        } else {

            set(IssuerOfPatientID, Constants.PrimaryLocalIssuerID)
        }

        // The DI-r may know this patient under a different name and accession number
        // Removing Patient Name as not to confuse the DI-r
        set(PatientName, '')
        set(AccessionNumber, '')
        validData = true
    }
}

if (!validData) {
    log.error("Query cannot be processed since it has no health card number or patient id.")
    return false
}

// Ignore fields that LHC wants explicitly

// ReferringPhysicianName Logic: remove it from query if passed (i.e. ignore it)
set(ReferringPhysicianName, "")

// StudyTime Logic: remove it if present
set(StudyTime, "")

// StudyID Logic: Remove it (even if it was used to pass the Health Card Number)
set(StudyID, "")

// Accession Number Logic: Remove if present
set(AccessionNumber, "")

/* Log the outgoing C-FIND to DI-r */
log.debug("This is the outgoing AD-HOC C-Find to GTA West: {}", output)

