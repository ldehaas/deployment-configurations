/* This script takes the DICOM C-Find responses from the DIR and filters
   out local studies based on the IssuerOfPatientID returned by the DI-r
*/

log.debug("Starting cfind_response_filter")
LOAD("common.groovy")

def pids = new Pids(input)

if (pids.getSourceDomain() == null) {
    log.error("Unable to process cfind response: missing source issuer")
    return false
}

if (pids.isLocal()) {
    log.debug("Source issuer '{}' is local.  Filtering this cfind response", pids.getSourceDomain())
    return false
}

if (pids.isSourceUnwantedDomain()) {
    log.info("Source issuer '{}' is an unwanted domain.  Filtering this cfind response", pids.getSourceDomain())
    return false
}

if (pids.getHealthCardNumber() == null) {
    log.error("A Valid HCN was not found, rejecting this study")
    return false
}

// SFC 436101 - Do not return value if the on;y Modality in Study is KO
def allTheModalities = getList(ModalitiesInStudy)
    log.info("Modalities in study == {}", allTheModalities)
    log.info("Number of modalities in study == {}", allTheModalities.size())

    if (allTheModalities.contains("KO") && allTheModalities.size() == 1) {
        log.error("Exiting because Modality in Study is KO")
        return false
    }
