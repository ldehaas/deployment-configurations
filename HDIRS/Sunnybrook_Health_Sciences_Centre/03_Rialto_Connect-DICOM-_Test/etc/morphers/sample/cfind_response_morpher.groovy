
/*
 * localize cfind responses for queries
 * 
 * often this script shares similar logic with foreign image localizer
 *
 * NOTE that set calls will only work on the tags that have been requested as
 * return keys or matching keys
 *
 * the cfind response is generated differently for dicom and xds environments.
 * see foreigm_image_morpher.groovy for more information.
 */