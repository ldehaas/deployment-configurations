<tests service="connect-dicom"
       script="AdHocCFindRequestMorpher"
       file="adhoc_cfind_req_morpher.groovy">

    <test name="validHealthCardNumber">
        <inputs>
            <dicom name="input">
                StudyID: 0123456789
                PatientName: Simpson^Homer
                ReferringPhysicianName: Riviera^Nick
                StudyTime: 201312031300
                AccessionNumber: accession
            </dicom>
        </inputs>
        <assertions>
            assert returned == true || returned == null
            assert get(PatientID) == '0123456789'
            assert get(IssuerOfPatientID) == 'GLOBALDOMAIN'
            assert get(StudyID) == null
            assert get(AccessionNumber) == null
            assert get(PatientName) == null
        </assertions>
    </test>

    <test name="invalidHealthCardNumber">
        <inputs>
            <dicom name="input">
                StudyID: invalid
            </dicom>
        </inputs>
        <assertions>
            assert returned == false
        </assertions>
    </test>

    <test name="localPatientID">
        <inputs>
            <dicom name="input">
                PatientID: localpid
                PatientName: Simpson^Homer
                ReferringPhysicianName: Riviera^Nick
                StudyTime: 201312031300
                AccessionNumber: accession
            </dicom>
        </inputs>
        <assertions>
            assert returned == true || returned == null
            assert get(PatientID) == 'localpid'
            assert get(IssuerOfPatientID) == 'LHN'
            assert get(StudyID) == null
            assert get(AccessionNumber) == null
            assert get(PatientName) == null
        </assertions>
    </test>

    <test name="foreignPatientID">
        <inputs>
            <dicom name="input">
                PatientID: foreignissuer_foreignpid
                PatientName: Simpson^Homer
                ReferringPhysicianName: Riviera^Nick
                StudyTime: 201312031300
                AccessionNumber: accession
            </dicom>
        </inputs>
        <assertions>
            assert returned == true || returned == null
            assert get(PatientID) == 'foreignpid'
            assert get(IssuerOfPatientID) == 'foreignissuer'
            assert get(StudyID) == null
            assert get(AccessionNumber) == null
            assert get(PatientName) == null
        </assertions>
    </test>

    <!-- one of name, patient id or health card are required -->
    <test name="noCriteria">
        <inputs>
            <dicom name="input">
                ReferringPhysicianName: Riviera^Nick
                StudyTime: 201312031300
                AccessionNumber: accession
            </dicom>
        </inputs>
        <assertions>
            assert returned == false
        </assertions>
    </test>

    <test name="nameTooShort">
        <inputs>
            <dicom name="input">
                PatientName: HS
                ReferringPhysicianName: Riviera^Nick
                StudyTime: 201312031300
                AccessionNumber: accession
            </dicom>
        </inputs>
        <assertions>
            assert returned == false
        </assertions>
    </test>

    <test name="nameWithComma">
        <inputs>
            <dicom name="input">
                # note the double space before middle initial:
                PatientName: Simpson, Homer  J
                ReferringPhysicianName: Riviera^Nick
                StudyTime: 201312031300
                AccessionNumber: accession
            </dicom>
        </inputs>
        <assertions>
            assert returned == true || returned == null
            // double space condensed to one:
            assert get(PatientName) == "Simpson*^Homer J*"
        </assertions>
    </test>

    <test name="nameWithCommaMissingGiven">
        <inputs>
            <dicom name="input">
                PatientName: Simpson,
                ReferringPhysicianName: Riviera^Nick
                StudyTime: 201312031300
                AccessionNumber: accession
            </dicom>
        </inputs>
        <assertions>
            assert returned == true || returned == null
            assert get(PatientName) == "Simpson*"
        </assertions>
    </test>

    <test name="nameWithNoComma">
        <inputs>
            <dicom name="input">
                PatientName: Simpson
                ReferringPhysicianName: Riviera^Nick
                StudyTime: 201312031300
                AccessionNumber: accession
            </dicom>
        </inputs>
        <assertions>
            assert returned == true || returned == null
            assert get(PatientName) == "Simpson*"
        </assertions>
    </test>

    <test name="properDicomName">
        <inputs>
            <dicom name="input">
                PatientName: Simpson^Homer
                ReferringPhysicianName: Riviera^Nick
                StudyTime: 201312031300
                AccessionNumber: accession
            </dicom>
        </inputs>
        <assertions>
            assert returned == true || returned == null
            assert get(PatientName) == "Simpson^Homer*"
        </assertions>
    </test>

    <test name="validStudyDate">
        <inputs>
            <dicom name="input">
                StudyID: 0123456789
                StudyDate: 201312031300
            </dicom>
        </inputs>
        <assertions>
            assert returned == true || returned == null
            assert get(StudyDate) == "201312031300"
        </assertions>
    </test>

    <test name="expandStudyDateToRange">
        <inputs>
            <dicom name="input">
                StudyID: 0123456789
                StudyDate: 2013
            </dicom>
        </inputs>
        <assertions>
            assert returned == true || returned == null
            assert get(StudyDate) == "20130101-20131231"
        </assertions>
    </test>

    <test name="removeDotsInDates">
        <inputs>
            <dicom name="input">
                StudyID: 0123456789
                StudyDate: 2013.12.03
                PatientBirthDate: 1972.03.24
            </dicom>
        </inputs>
        <assertions>
            assert returned == true || returned == null
            assert get(StudyDate) == "20131203"
            assert get(PatientBirthDate) == "19720324"
        </assertions>
    </test>

    <test name="upperCaseModality">
        <inputs>
            <dicom name="input">
                StudyID: 0123456789
                # nb: the script currently removes all but the first
                # modality in the list if there are multiple
                ModalitiesInStudy: mr
            </dicom>
        </inputs>
        <assertions>
            assert returned == true || returned == null
            assert get(ModalitiesInStudy) == "MR"
        </assertions>
    </test>

    <test name="sexToUpperCase">
        <inputs>
            <dicom name="input">
                StudyID: 0123456789
                PatientSex: m
            </dicom>
        </inputs>
        <assertions>
            assert returned == true || returned == null
            assert get(PatientSex) == "M"
        </assertions>
    </test>

    <test name="invalidPatientSex">
        <inputs>
            <dicom name="input">
                StudyID: 0123456789
                PatientSex: X
            </dicom>
        </inputs>
        <assertions>
            assert returned == false
        </assertions>
    </test>
</tests>
