<tests service="connect-dicom"
       script="AdHocCFindResponseMorpher"
       file="adhoc_cfind_response_localizer.groovy">

    <!-- These tests are aimed at accession_number.groovy but use
         other scripts to get at that functionality -->

    <test name="simplePrefix">
        <inputs>
            <dicom name="input">
                PatientID: foreignPid
                IssuerOfPatientID: MSH
                OtherPatientIDsSequence/PatientID: localPid
                OtherPatientIDsSequence/IssuerOfPatientID: LHN

                AccessionNumber: accn
            </dicom>
        </inputs>
        <assertions>
            assert get(AccessionNumber) == "MSHaccn"
        </assertions>
    </test>

    <test name="differentPrefix">
        <inputs>
            <dicom name="input">
                PatientID: foreignPid
                IssuerOfPatientID: BGH1
                OtherPatientIDsSequence/PatientID: localPid
                OtherPatientIDsSequence/IssuerOfPatientID: LHN

                AccessionNumber: accn
            </dicom>
        </inputs>
        <assertions>
            assert get(AccessionNumber) == "BGHaccn"
        </assertions>
    </test>

    <test name="simpleSuffix">
        <inputs>
            <dicom name="input">
                PatientID: foreignPid
                IssuerOfPatientID: CMH
                OtherPatientIDsSequence/PatientID: localPid
                OtherPatientIDsSequence/IssuerOfPatientID: LHN

                AccessionNumber: accn
            </dicom>
        </inputs>
        <assertions>
            assert get(AccessionNumber) == "accnC"
        </assertions>
    </test>

    <test name="prefixOnlyIfNotSuffixed_noSuffix">
        <inputs>
            <dicom name="input">
                PatientID: foreignPid
                IssuerOfPatientID: HHHS
                OtherPatientIDsSequence/PatientID: localPid
                OtherPatientIDsSequence/IssuerOfPatientID: LHN

                AccessionNumber: accn
            </dicom>
        </inputs>
        <assertions>
            assert get(AccessionNumber) == "HHHSaccn"
        </assertions>
    </test>

    <test name="prefixOnlyIfNotSuffixed_withSuffix">
        <inputs>
            <dicom name="input">
                PatientID: foreignPid
                IssuerOfPatientID: HHHS
                OtherPatientIDsSequence/PatientID: localPid
                OtherPatientIDsSequence/IssuerOfPatientID: LHN

                AccessionNumber: accnHHHS
            </dicom>
        </inputs>
        <assertions>
            assert get(AccessionNumber) == "accnHHHS"
        </assertions>
    </test>

    <test name="prefixOnlyIfNotSuffixed_multipleSuffixes_noSuffix">
        <inputs>
            <dicom name="input">
                PatientID: foreignPid
                IssuerOfPatientID: TSHH
                OtherPatientIDsSequence/PatientID: localPid
                OtherPatientIDsSequence/IssuerOfPatientID: LHN

                AccessionNumber: accn
            </dicom>
        </inputs>
        <assertions>
            assert get(AccessionNumber) == "TSHaccn"
        </assertions>
    </test>

    <test name="prefixOnlyIfNotSuffixed_multipleSuffixes_firstSuffix">
        <inputs>
            <dicom name="input">
                PatientID: foreignPid
                IssuerOfPatientID: TSHH
                OtherPatientIDsSequence/PatientID: localPid
                OtherPatientIDsSequence/IssuerOfPatientID: LHN

                AccessionNumber: accnTSHH
            </dicom>
        </inputs>
        <assertions>
            assert get(AccessionNumber) == "accnTSHH"
        </assertions>
    </test>

    <test name="prefixOnlyIfNotSuffixed_multipleSuffixes_secondSuffix">
        <inputs>
            <dicom name="input">
                PatientID: foreignPid
                IssuerOfPatientID: TSHH
                OtherPatientIDsSequence/PatientID: localPid
                OtherPatientIDsSequence/IssuerOfPatientID: LHN

                AccessionNumber: accnTSHG
            </dicom>
        </inputs>
        <assertions>
            assert get(AccessionNumber) == "accnTSHG"
        </assertions>
    </test>

    <!-- Northumberland has requirements pretty unique to that site... -->

    <test name="northumberland16chars">
        <inputs>
            <dicom name="input">
                PatientID: foreignPid
                IssuerOfPatientID: NHH
                OtherPatientIDsSequence/PatientID: localPid
                OtherPatientIDsSequence/IssuerOfPatientID: LHN

                AccessionNumber: 1234567890123456
            </dicom>
        </inputs>
        <assertions>
            assert get(AccessionNumber) == "1234567890123456"
        </assertions>
    </test>

    <test name="northumberland15chars">
        <inputs>
            <dicom name="input">
                PatientID: foreignPid
                IssuerOfPatientID: NHH
                OtherPatientIDsSequence/PatientID: localPid
                OtherPatientIDsSequence/IssuerOfPatientID: LHN

                AccessionNumber: 123456789012345
            </dicom>
        </inputs>
        <assertions>
            assert get(AccessionNumber) == "123456789012345H"
        </assertions>
    </test>

    <test name="northumberland14chars">
        <inputs>
            <dicom name="input">
                PatientID: foreignPid
                IssuerOfPatientID: NHH
                OtherPatientIDsSequence/PatientID: localPid
                OtherPatientIDsSequence/IssuerOfPatientID: LHN

                AccessionNumber: 12345678901234
            </dicom>
        </inputs>
        <assertions>
            assert get(AccessionNumber) == "12345678901234H"
        </assertions>
    </test>

    <test name="northumberland13chars">
        <inputs>
            <dicom name="input">
                PatientID: foreignPid
                IssuerOfPatientID: NHH
                OtherPatientIDsSequence/PatientID: localPid
                OtherPatientIDsSequence/IssuerOfPatientID: LHN

                AccessionNumber: 1234567890123
            </dicom>
        </inputs>
        <assertions>
            assert get(AccessionNumber) == "NHH1234567890123"
        </assertions>
    </test>

    <!-- And Southlake has really quite bizarre rules: -->

    <test name="southlakeNoY">
        <inputs>
            <dicom name="input">
                PatientID: foreignPid
                IssuerOfPatientID: SRHC
                OtherPatientIDsSequence/PatientID: localPid
                OtherPatientIDsSequence/IssuerOfPatientID: LHN

                AccessionNumber: accn
            </dicom>
        </inputs>
        <assertions>
            assert get(AccessionNumber) == "Saccn"
        </assertions>
    </test>

    <test name="southlakeWithY">
        <inputs>
            <dicom name="input">
                PatientID: foreignPid
                IssuerOfPatientID: SRHC
                OtherPatientIDsSequence/PatientID: localPid
                OtherPatientIDsSequence/IssuerOfPatientID: LHN

                AccessionNumber: acYcn
            </dicom>
        </inputs>
        <assertions>
            assert get(AccessionNumber) == "acYcnS"
        </assertions>
    </test>

    <test name="southlakeAlreadySuffixed">
        <inputs>
            <dicom name="input">
                PatientID: foreignPid
                IssuerOfPatientID: SRHC
                OtherPatientIDsSequence/PatientID: localPid
                OtherPatientIDsSequence/IssuerOfPatientID: LHN

                AccessionNumber: accnSRHC
            </dicom>
        </inputs>
        <assertions>
            assert get(AccessionNumber) == "accnSRHC"
        </assertions>
    </test>

</tests>
