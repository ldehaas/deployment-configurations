log.trace("These are the raw studies from the XDS Reg being passed to the rules morpher:\n{}", studies)
log.debug("Checking {} priors", studies.size())

def excludeSiteIDs = [ '2.16.840.1.113883.3.239.18.160',
                       '2.16.840.1.113883.3.239.18.54']

// This ensures that NO LOCAL STUDIES (i.e. only foreign studies) get processed, and that studies from specific domains are also excluded

studies = studies.findAll { study ->
    !excludeSiteIDs.contains(study.get(XDSSourcePatientID).domainUUID)
}

log.debug("After filtering  studies from ignored domains, we are fetching {} priors", studies.size())

return studies
