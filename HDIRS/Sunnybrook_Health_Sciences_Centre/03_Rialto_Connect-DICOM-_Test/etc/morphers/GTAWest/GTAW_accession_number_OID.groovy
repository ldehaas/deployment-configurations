
/**
 * Accession number localization rules for GTA West  sites
 */
class AccessionNumbers {
    /**
     * Maps issuer to a function that localizes the accession number.
     * The default function is just to use the issuer as a simple prefix
     * and no entry needs to be added for these cases.
     */
    private static config = [
        '2.16.840.1.113883.3.239.18.162': prefix("BGHS"),
        '2.16.840.1.113883.3.239.18.25': prefix("CGMH"),
        '2.16.840.1.113883.3.239.18.26': prefix("CVH"),
        '2.16.840.1.113883.3.239.18.137': prefix("HSC"),
        '2.16.840.1.113883.3.239.18.164': prefix("HBKR"),
        '2.16.840.1.113883.3.239.18.52': prefix("HHCC"),
        '2.16.840.1.113883.3.239.18.47': prefix("HHSO"),
        '2.16.840.1.113883.3.239.18.46': prefix("HHSM"),
        '2.16.840.1.113883.3.239.18.45': prefix("HHSG"),
        '2.16.840.1.113883.3.239.16.8882': prefix("HHSU"),
        '2.16.840.1.113883.3.239.18.98': prefix("MAHC"),
        '2.16.840.1.113883.3.239.18.163': prefix("GBGH"),
        '2.16.840.1.113883.3.239.18.121': prefix("ASMH"),
        '2.16.840.1.113883.3.239.18.133': prefix("RVH"),
        '2.16.840.1.113883.3.239.18.140': prefix("SJHC"),
        '2.16.840.1.113883.3.239.18.146': prefix("THCM"),
        '2.16.840.1.113883.3.239.18.147': prefix("THCT"),
        '2.16.840.1.113883.3.239.18.156': prefix("CAMH"),
        '2.16.840.1.113883.3.239.18.158': prefix("TRI"),
        '2.16.840.1.113883.3.239.18.148': prefix("UHN"),
        '2.16.840.1.113883.3.239.18.157': prefix("WCH"),
        '2.16.840.1.113883.3.239.18.97': prefix("MTSH"),
        '2.16.840.1.113883.3.239.18.150':  prefix("WOHSC"),
        '2.16.840.1.113883.3.239.18.152': prefix("WOHSE"),
        '2.16.840.1.113883.3.239.16.8881': prefix("WOHSR"),
        '2.16.840.1.113883.3.239.18.160': prefix("WPHC"),
        '2.16.840.1.113883.3.239.18.54': prefix("HRRH"),
        '2.16.840.1.113883.3.239.18.180': prefix("KMH"),
        '2.16.840.1.113883.3.239.18.181': prefix("DXRA"),
        '2.16.840.1.113883.3.239.18.200': prefix("GAMX"),

    ].withDefault { issuer ->
        // the default is to just prefix with the issuer:
        prefix(issuer)
    }

    // Common localization methods.  Each takes config and returns a closure
    // that localizes and accesion number:

//    private static final prefix(prefix) {
//        return { accn -> prefix + accn }
//    }

private static final prefix(prefix) {
        return prefix
    }

/**
     * Localizes the accession number according to appendix C.
     */

    // RegionalPatientIdentifier ??

    public static localize(issuer, accn, log) {
        log.info("trying to get config[{}] = {}", issuer, config[issuer])
        log.info("accn = {}", accn)
        accn = config[issuer] + "\$" + accn
        log.info("accn after: {}", accn)

        if (accn.length() > 16) {
            log.warn("Localized accession number '{}' is longer than 16 characters.", accn)
        }

        return accn
    }


    /**
     * Creates function to add prefix only if none of the given prefixes are
     * present.  If there are no prefixes to check, we check for the prefix
     * itself.
     */
    private static prefixIfNotAlreadyPrefixed(prefix, accn, String ... prefixes) {
        if (prefixes.length > 0) {
            for (prefx in prefixes) {
                if (accn.toUpperCase().startsWith(prefx)) {
                    return accn
                }
            }
        } else if (accn.toUpperCase().startsWith(prefix)) {
            return accn
        }
        return prefix + "\$" + accn
    }

    /**
     * Creates function to add prefix only if none of the given prefixes are
     * present.  If there are no prefixes to check, we check for the prefix
     * itself. Prefixes are built from the config above
     */
    private static prefixIfNotPrefixedWithKnownPrefixes(issuer, accn, log) {
        for (e in config) {
            log.info(e.key)
            log.info("Checking if accession number {} has prefix {}", accn, config[e.key])
            if (accn.toUpperCase().startsWith(e.key)) {
                return accn
            }
        }
        if (accn.toUpperCase().startsWith(config[issuer])) {
            return accn
        }

        return config[issuer] + "\$" + accn
    }
}

