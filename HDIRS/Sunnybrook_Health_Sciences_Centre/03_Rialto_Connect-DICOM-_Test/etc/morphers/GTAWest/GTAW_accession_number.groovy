
/**
 * Accession number localization rules for GTA West  sites
 */
class AccessionNumbers {
    /**
     * Maps issuer to a function that localizes the accession number.
     * The default function is just to use the issuer as a simple prefix
     * and no entry needs to be added for these cases.
     */
    private static config = [
        '1315': prefix("BGHS"),
        '1315': prefix("BGHS"),
        '4197': prefix("CGMH"),
        '3986': prefix("CVH"),
        '3969': prefix("HSC"),
        '3855': prefix("HBKR"),
        '4161': prefix("HHCC"), 
        '4192': prefix("OTMH"),
        '4193': prefix("MDH"),
        '4624': prefix("GTH"),
        '4618': prefix("MAHC"),
        '4241': prefix("GBGH"),
        '4108': prefix("OSMH"), 
        '3987': prefix("RVH"),
        '4056': prefix("SJHC"),
        '4090': prefix("THPM"),
        '4363': prefix("THPT"),
        '4242': prefix("CAMH"),
        '3949': prefix("TRI"),
        '4265': prefix("UHN"),
        '4631': prefix("WCH"),
        '4805': prefix("MTSH"),
        '4685': prefix("BCH"),
        '4245': prefix("EGH"),
        '1469': prefix("WPHC"),
        '4260': prefix("HRRH"),
        '5501': prefix("KMH"),
        '5502': prefix("DXRA"),
        '5503': prefix("GAMX"),
  
    ].withDefault { issuer ->
        // the default is to just prefix with the issuer:
        prefix(issuer)
    }

    // Common localization methods.  Each takes config and returns a closure
    // that localizes and accesion number:

    private static final prefix(prefix) {
        return { accn -> prefix + accn }
    }

    /**
     * Localizes the accession number according to appendix C.
     */

    // RegionalPatientIdentifier ??

    public static localize(issuer, accn, log) {
        log.info("trying to get config[{}] = {}", issuer, config[issuer])
        log.info("accn = {}", accn)
        accn = config[issuer] + "\$" + accn
        log.info("accn after: {}", accn)

        if (accn.length() > 16) {
            log.warn("Localized accession number '{}' is longer than 16 characters.", accn)
        }

        return accn
    }


    /**
     * Creates function to add prefix only if none of the given prefixes are
     * present.  If there are no prefixes to check, we check for the prefix
     * itself.
     */
    private static prefixIfNotAlreadyPrefixed(prefix, accn, String ... prefixes) {
        if (prefixes.length > 0) {
            for (prefx in prefixes) {
                if (accn.toUpperCase().startsWith(prefx)) {
                    return accn
                }
            }
        } else if (accn.toUpperCase().startsWith(prefix)) {
            return accn
        }
        return prefix + "\$" + accn
    }

    /**
     * Creates function to add prefix only if none of the given prefixes are
     * present.  If there are no prefixes to check, we check for the prefix
     * itself. Prefixes are built from the config above
     */
    private static prefixIfNotPrefixedWithKnownPrefixes(issuer, accn, log) {
        for (e in config) {
            log.info(e.key)
            log.info("Checking if accession number {} has prefix {}", accn, config[e.key])
            if (accn.toUpperCase().startsWith(e.key)) {
                return accn
            }
        }
        if (accn.toUpperCase().startsWith(config[issuer])) {
            return accn
        }

        return config[issuer] + "\$" + accn
    }

}

