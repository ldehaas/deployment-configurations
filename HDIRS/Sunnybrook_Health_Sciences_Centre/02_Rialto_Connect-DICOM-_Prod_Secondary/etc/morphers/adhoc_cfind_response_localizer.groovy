/**
 * This localizer is responsible for localizing any ingested study headers for ingestion from adhoc cfind responses
 */

log.debug("Starting adhoc_cfind_response_foreign_image_localizer")
LOAD("common.groovy")
LOAD("accession_number.groovy")
LOAD("normalization.groovy")

// Change the Retrieve AE Title to that of Connect itself
set(RetrieveAETitle, 'SHSC_RIALTO_GTAW')

def suid = get(StudyInstanceUID)

def pids = new Pids(input)

if (pids.isLocal()) {
    log.debug("Dropping local study: '{}'", suid)
    return false
}

if (pids.isSourceUnwantedDomain()) {
    log.info("Dropping study: '{}' from unwanted domain '{}'.", suid, get(IssuerOfPatientID))
    return false
}

if (pids.getSourceDomain() == null) {
    log.error("Dropping study '{}' because the source domain is unknown. " +
              "This prevents localization of the accession number.")
    return false
}

def localPid = pids.getLocalPidWithPrefixedSourceFallback()
if (localPid[0].length() > 64) {
    log.warn("Prefixed patient id exceeds 64 chars: '{}'", localizedPid[0])
}

pids.localize(localPid)


// Localize Accession Numbers and Study IDs
def oldAccNum = get(AccessionNumber)
def newAccNum = AccessionNumbers.localize(pids.getSourceDomain(), oldAccNum, log)
log.debug("Source issuer is '{}', old accession number is '{}', new accession number and study ID is '{}'",
    pids.getSourceDomain(), oldAccNum, newAccNum);
set(AccessionNumber, newAccNum)

//Localize SOP Modality
def modality = get(Modality)
def localModality = Normalization.getNormalizedModality(modality, get(StudyDescription))
log.debug("Localizing modality '{}' to '{}'", modality, localModality)
set(Modality, localModality)

// New GE PACS Mapping - Pass the Study Description back in the StudyID field
set(StudyID, get(StudyDescription))
log.debug("Setting the Study Description in the StudyID field to '{}'", get(StudyDescription))

// Log SOP DICOM metadata being sent to PACS
log.debug("This is the outgoing morphed C-FIND response to PACS: [{}]", output)
