/*
 * this scripts constructs a cfind query from an hl7 order message to search
 * for prior studies of interest
 * 
 * this script can return false to signal not to do any prefetching for a 
 * given order
 */
def pid = get( "PID-3-1" )
log.info("found pid '{}'", pid)
def issuer = get("PID-3-2")

if (issuer == null) {
    issuer = "SUNB"
    } //Populate it with the right SB OID

log.info("found issuer '{}'", issuer)

// if (pid ==~ /regular expression/) {
//    return false
//}


// construct the cfind
set ( PatientID, pid )
set ( IssuerOfPatientID, "SUNB")
//set ( StudyID, pid )
//set ( StudyIDIssuer, "SUNB" )


// etc etc
