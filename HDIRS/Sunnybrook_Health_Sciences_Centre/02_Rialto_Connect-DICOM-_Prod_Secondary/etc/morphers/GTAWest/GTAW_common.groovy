//
// common.groovy - Lower-level include of common utility pieces
//

class Constants {
    static final LocalIssuerIDs = ['SUNB']      // Lakeridge only has one local hospital
    static final PrimaryLocalIssuerID = LocalIssuerIDs[0]
    static final PrimaryLocalAccessionIssuer = LocalIssuerIDs[0]

    static final UnwantedDomains = []

    // Global Issuer of PID that goes with the top-level Patients
    // (in NYGH's case, the top-level Patient ID is the Health Card Number)
    //static final GlobalIssuerOfPID = "GLOBALDOMAIN"    // PRODUCTION ONLY
    static final GlobalIssuerIDs = ['HDIRS','2.16.840.1.113883.4.59']        // TEST ONLY - historical quirk

    // KHC1578 - removing 'PR' from the list of invalid Modalities.
    // The PACS has isues with foreign SR objects so HDIRS decided not to pass them along:
    static final InvalidModalities = ["SR"]
}

class Identifiers {

    private prefixes = [
    '2.16.840.1.113883.3.239.18.162':'BGHS',
    '2.16.840.1.113883.3.239.18.25':'CGMH',
    '2.16.840.1.113883.3.239.18.26':'CVH',
    '2.16.840.1.113883.3.239.18.137':'HSC',
    '2.16.840.1.113883.3.239.18.164':'HBKR',
    '2.16.840.1.113883.3.239.18.52':'HHCC',
    '2.16.840.1.113883.3.239.18.47':'HHSO',
    '2.16.840.1.113883.3.239.18.46':'HHSM',
    '2.16.840.1.113883.3.239.18.45':'HHSG',
    '2.16.840.1.113883.3.239.16.8882':'HHSU',
    '2.16.840.1.113883.3.239.18.98':'MAHC',
    '2.16.840.1.113883.3.239.18.163':'GBGH',
    '2.16.840.1.113883.3.239.18.121':'ASMH',
    '2.16.840.1.113883.3.239.18.133':'RVH',
    '2.16.840.1.113883.3.239.18.140':'SJHC',
    '2.16.840.1.113883.3.239.18.146':'THCM',
    '2.16.840.1.113883.3.239.18.147':'THCT',
    '2.16.840.1.113883.3.239.18.156':'CAMH',
    '2.16.840.1.113883.3.239.18.158':'TRI',
    '2.16.840.1.113883.3.239.18.148':'UHN',
    '2.16.840.1.113883.3.239.18.157':'WCH',
    '2.16.840.1.113883.3.239.18.97':'MTSH',
    '2.16.840.1.113883.3.239.18.150':'WOHSC',
    '2.16.840.1.113883.3.239.18.152':'WOHSE',
    '2.16.840.1.113883.3.239.16.8881':'WOHSR',
    '2.16.840.1.113883.3.239.18.160':'WPHC',
    '2.16.840.1.113883.3.239.18.54':'HRRH',
    '2.16.840.1.113883.3.239.18.180':'KMH',
    '2.16.840.1.113883.3.239.18.181':'DXRA',
    '2.16.840.1.113883.3.239.18.200':'GAMX',
    '2.16.840.1.113883.3.239.18.234':'PMC',
    ]

    private domain2pid = [:]
    private accession
    private domain
    private sop

    public Identifiers(sop) {
        this.sop = sop
        domain = sop.get(IssuerOfPatientID)
        accession = sop.get(AccessionNumber)
        domain2pid[domain] = sop.get(PatientID)

        sop.get(OtherPatientIDsSequence).each {
            domain2pid[it.get(IssuerOfPatientID)] = it.get(PatientID)
        }
    }

    public getLocalPatientId() {
        //Constants.LocalIssuerIDs.findResult { issuerac ->
            //def localPatientId = domain2pid[issuerac]
            //if (localPatientId != null) {
                //return [localPatientId, issuerac]
            //}
        return null
        //}
    }

    public getGlobalPatientId() {
        Constants.GlobalIssuerIDs.findResult { issuerac ->
            def globalPatientId = domain2pid[issuerac]
            if (globalPatientId != null) {
                return [globalPatientId, issuerac]
            }
            return null
        }
    }

    public getLocalAccessionNumber() {
        Constants.LocalIssuerIDs.findResult { issuerac ->
            def localPatientId = domain2pid[issuerac]
            if (localPatientId != null) {
                return [accession, issuerac]
            }
            return null
        }
    }

    public getLocalPatientIdWithFailover() {
        
        def localPatientId = getLocalPatientId()
        if (localPatientId != null) {
            return localPatientId
        }

        if (domain != null && !domain.isEmpty() && domain2pid[domain] != null && !domain2pid[domain].isEmpty()) {
            return [prefixes[domain] + domain2pid[domain], Constants.PrimaryLocalIssuerID]
        }

        return null
    }

    public getLocalAccessionNumberWithFailover() {

        def localAccessionNumber = getLocalAccessionNumber()
        if (localAccessionNumber != null) {
            return localAccessionNumber
        }

        if (domain != null && !domain.isEmpty() && accession != null && !accession.isEmpty()) {
            return [prefixes[domain] + accession, Constants.PrimaryLocalIssuerID]
        }

        return null
    }
    
    /**
     * Install the given pid as the local one and clear out
     * extra data that the PACS might not want to see.
     */
    public localizer(localPatientId, localAccessionNumber) {
        
        if (localPatientId != null && localPatientId.size() == 2) {
            sop.set(PatientID, localPatientId[0])
            sop.set(IssuerOfPatientID,localPatientId[1])
        }

        if (localAccessionNumber != null && localAccessionNumber.size() == 2) {
            sop.set(AccessionNumber, localAccessionNumber[0])
        }

        sop.remove(IssuerOfAccessionNumberSequence)
        sop.remove(OtherPatientIDsSequence)
    }
}

class PrefetchRules {

    /** Determine which incoming modalities need 5 years and 5 priors */
    private static modalitiesWith5years = [
        // At Lakeridge, HL7 messages from MEDITECH uses OBR-4-1 'XMAM' to represent 'MG' studies
        "ADDV1B","ADDV1B","ADDV2B","ADDV2B","ADDV3B","ADDV3B","ADDV4B","ADDV4B","BASPBB","BASPBB","BASPLB","BASPLB","BASPRB","BASPRB","BRLOBB",
        "BRLOBB","BRLOLB","BRLOLB","BRLORB","BRLORB","BRSPBB","BRSPBB","BRSPLB","BRSPLB","BRSPRB","BRSPRB","BXBBUB","BXBBUB","BXSTBB","BXSTBB",
        "BXSTLB","BXSTLB","BXSTRB","BXSTRB","COM4BB","COM4BB","DUCTBB","DUCTBB","DUCTLB","DUCTLB","DUCTRB","DUCTRB","FILERVW","FILERVW","ICBRBB",
        "ICBRBB","IDUCTB","IDUCTB","INLOCB","INLOCB","INPPIB","INPPIB","INPTPB","INPTPB","ISBRBB","ISBRBB","MA+CBB","MA+CBB","MA+CLB","MA+CLB",
        "MA+CRB","MA+CRB","MAMCOB","MAMCOB","MAMMBB","MAMMBB","MAMMLB","MAMMLB","MAMMRB","MAMMRB","MAMREB","MAMREB","MAMSCB","MAMSCB","MAMTOMO",
        "MAMTOMO","MB+CLB","MB+CLB","MB+CRB","MB+CRB","MRBBXB","MRBBXB","MRBBXL","MRBBXL","MRBBXR","MRBBXR","OBSPB","OBSPB","OBSPBHR","OBSPBHR",
        "OBSPTOMO","OBSPTOMO","OBSPU","OBSPU","OBSPUHR","OBSPUHR","SDLMAB","SDLMAB","SDLMAL","SDLMAL","SDLMAR","SDLMAR","SENNDM","SENNDM","3BRBCM",
        "3BRBCM","3BRLCM","3BRLCM","3BRRCM","3BRRCM","B100BREA","B100BREA","B1CBREACM","B1CBREACM","B200BREA","B200BREA","B201BREA","B201BREA",
        "B203BREA","B203BREA","B2BREAOM","B2BREAOM","B3CBBREABCM","B3CBBREABCM","B3CLBREALM","B3CLBREALM","B3CRBREARCM","B3CRBREARCM","B4CBREACM",
        "B4CBREACM","B5CBREACM","B5CBREACM","BREBCM","BREBCM","BREBIM","BREBIM","BREBOM","BREBOM","BRELCM","BRELCM","BRELOM","BRELOM","BRELTM","BRELTM",
        "BRERCM","BRERCM","BREREM","BREREM","BREROM","BREROM","BRERTM","BRERTM","BIBOP","BIBOP","BIBOP45","BIBOP45","BIBOP60","BIBOP60","BIBOP90","BIBOP90",
        "BILOC","BILOC","BIMAU","BIMAU","BIRDU","BIRDU","BIUS","BIUS","BRADBB","BRADBB","BRADLB","BRADLB","BRADRB","BRADRB","BRAXB","BRAXB","BRAXBB",
        "BRAXBB","BRAXBL","BRAXBL","BRAXBR","BRAXBR","BRAXCB","BRAXCB","BRAXCL","BRAXCL","BRAXCR","BRAXCR","BRAXL","BRAXL","BRAXR","BRAXR","BRCW",
        "BRCW","BREABB","BREABB","BREALB","BREALB","BREARB","BREARB","BXBAX1","BXBAX1","BXBAX2","BXBAX2","BXBAX3","BXBAX3","BXBAX4","BXBAX4","BXBBUN",
        "BXBBUN","BXBCBB","BXBCBB","BXBCBN","BXBCBN","BXBCLB","BXBCLB","BXBCLN","BXBCLN","BXBCRB","BXBCRB","BXBCRN","BXBCRN","BXBLUB","BXBLUB",
        "BXBLUN","BXBLUN","BXBRUB","BXBRUB","BXBRUN","BXBRUN","CLIPUSB","CLIPUSB","CLIPUSL","CLIPUSL","CLIPUSR","CLIPUSR","LOBUSB","LOBUSB","LOLUSB",
        "LOLUSB","LORUSB","LORUSB","OBSPHRUS","OBSPHRUS","SDLUSB","SDLUSB","SDLUSL","SDLUSL","SDLUSR","SDLUSR","SEEDST","SEEDST",
    ]

    /** Get the Number and Maximum Age of Priors to be Prefetched */
    static getNumberAndMaxAgeOfPriors(order, log) {
        // Default number of years is 3, default number of studies is 5
        def years = 3
        def studies = 5

        if (order != null) {
            if (modalitiesWith5years.contains(getOrderModality(order))) {
                years = 5
            } 
        } else {
            log.warn("No order received when attempting to determine priors number and age to be prefetched. " +
                "Cannot determine original study Modality. Falling back to default of 5 studies within 3 years.")
        }

        return [years, studies]
    }


    static getOrderModality(order) {
        return order != null ? order.get("OBR-4") : null  
    }
}

