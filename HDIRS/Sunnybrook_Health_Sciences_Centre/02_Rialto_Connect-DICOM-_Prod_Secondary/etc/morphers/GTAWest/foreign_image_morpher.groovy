/**
 * this script localizes foreign images (dicom headers) so that they can be
 * ingested by the PACS
 *
 * this script can return false to filter (drop) an image
 *
 * sometimes it's desirable to use this script for localizing both foreign
 * images and cfind responses so common logic can be shared
 */


/*
 * in the dicom environment, the input dicom header is directly from the DIR
 *
 * in the xds environment, the input dicom header is modified:
 *   - the PatientID and IssuerOfPatientID tag are overwritten with xds
 *     metadata's SourcePatientID, to indicate where the study is originated
 *   - the OtherPatientIDsSequence is added / updated to contain a pair of
 *     PatientID and IssuerOfPatientID with localized patient information
 *
 * the above two points apply to adhoc_cfind_resp_morpher as well
 */

LOAD("GTAW_common.groovy")
LOAD("../normalization.groovy")

log.debug("DICOM header before we localize: {}", input)

// source pid / issuer usually identifies where the study is originated
//def sourcePid = get(PatientID)
//def sourceIssuer = get(IssuerOfPatientID)

// script can throw exception and cause a cstore failure
//if (sourcePid == null || sourceIssuer == null) {
//    throw new UnsupportedOperationException(...);
//}

// there are usually a few pieces of information to be localized
// - pid
// - accession number to avoid collsion
// - study description (procedure codes)

//set(PatientID, localizedPid)

// usually PACS do not like the following tags
//remove(OtherPatientIDsSequence)
//remove(IssuerOfPatientID)
//prepend(AccessionNumber, sourceIssuer+"_")
//set(StudyDescription, mappedProcedureCodes)

issuer = get(IssuerOfPatientID)
accession = get(AccessionNumber)

/* Removing localization

 * localDemographics is a list of dicom objects representing cfind results from
 * local pacs.  If such a query was configured, these should contain the local
 * demographics for the patient as they exist in the PACS and may be used to
 * override the values in the SOP so as not to confuse the PACS.

if (localDemographics != null && !localDemographics.isEmpty()) {
	set(PatientName, localDemographics.get(0).get(PatientName))
} else {
	// Since there are no local demographics, just use what's already in the DICOM image.
}
*/

/*
 * In the xds environment, xdsMetadata may contain the xds metadata representing
 * the study.
 */
def identifiers = new Identifiers(input)

/* Removing localization

def localPatientId = null

if (prefetchOrder != null) {
        localPatientId = prefetchOrder.get("PID-3-1")
        log.info("Using PatientID from originating HL7 message: '{}'", localPatientId)
        set(PatientID, localPatientId)
}
*/
def localPatientId = null

//if (localPatientId == null) {
//        localPatientId = identifiers.getLocalPatientId()
//        log.info("Using PatientID from SOP instance: '{}'", localPatientId)
//}

if (localPatientId == null) {
        localPatientId = identifiers.getLocalPatientIdWithFailover()
        log.debug("Using prefixed PatientID from SOP instance: '{}'", localPatientId)
}

// Localize the modality
def originalModality = get(Modality)
def normalizedModality = Normalization.getNormalizedModality(originalModality, get(StudyDescription))
set(Modality, normalizedModality)
log.debug("Original Modality was [{}], normalizedModality is [{}]", originalModality, normalizedModality)


// Agfa requested setting DICOM Tag 0031,1020 with the HCN...  so here we go.
def globalPatientId = identifiers.getGlobalPatientId()
if (globalPatientId != null && !globalPatientId.isEmpty()) {
    set(0x00310010, "MITRA LINKED ATTRIBUTES 1.0", VR.LO)
    set(0x00311020, globalPatientId[0], VR.LO)
    log.info("Setting Private DICOM tag 0031,1020 to value '{}'", globalPatientId[0])
    log.debug("Private creator for the group we're adding to is {}", get(tag(0x0031, 0x0010)))
} else {
    log.info("globalPatientID is null - Private tag not set")
}

// Stash the original modality so that the order creation script can use it
// to properly normalize the procedure description and code.
// We would prefer to update the code and description in the dicom header, but
// they want the originals available to the PACS.
// This is a big hack.  ErrorComment is in group 0, which means we won't transmit
// it over the wire, but it should stick around long enough for the order creator
// to see it:
set(ErrorComment, get(Modality))

// Agfa has noticed that Autopilot is archiving the foreign studies to HDIRS and has requested that we set the uniform values to the following fields
// so that they can et a filter to not archive foriegn studies from GTAWest:
//        0008 0080  | institution_name
//        0008 1010  | station_name
//        0008 1040  | institutional_department_name
def staticForAgfa = "GTAWEST"
log.info("Setting InstitutionName, StationName, and InstitutionalDepartmentName to '{}' as requested by Agfa", staticForAgfa)
set(InstitutionName, staticForAgfa)
set(StationName, staticForAgfa)
set(InstitutionalDepartmentName, staticForAgfa)

def localAccessionNumber = identifiers.getLocalAccessionNumberWithFailover()

log.debug("Using PatientID: '{}'", localPatientId)
log.debug("Found localized accession no: {}", localAccessionNumber)

identifiers.localizer(localPatientId, localAccessionNumber)

