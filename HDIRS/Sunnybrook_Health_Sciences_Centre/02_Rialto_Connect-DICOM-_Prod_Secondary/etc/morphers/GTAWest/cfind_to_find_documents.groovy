
/*
 * this script converts dicom cfind request to xds find documents query
 *
 * this script can return false to signal a particular cfind 
 * to be dropped
 */

// obtaining properties from cfind
//pid = get(PatientID)
pid = get(StudyID)
//issuer = get(IssuerOfPatientID)
issuer = '2.16.840.1.113883.4.59'

log.info("in cfind to xds and pid is '{}'", pid)
log.info("in cfind to xds and issuer is '{}'", issuer)

// setting properties in find documents query
set(XDSPatientID, pid, (String) issuer)
// the reports have format code CDAR2/IHE 1.0 while the manifests have Exam
//+check for all those codes
// set(XDSFormatCode, code("1.2.840.10008.5.1.4.1.1.88.59", "1.2.840.10008.2.6.1") )//, code("CDAR2/IHE 1.0", "Connect-a-thon formatCodes"))
def fCode1 = code('1.2.840.10008.5.1.4.1.1.88.59', '1.2.840.10008.2.6.1')
def fCode2 = code('urn:ihe:rad:1.2.840.10008.5.1.4.1.1.88.59', '1.2.840.10008.2.6.1')
set(XDSFormatCode, fCode1, fCode2)
log.info("in cfind to xds setting code format 1 to '{}'", fCode1)
log.info("in cfind to xds setting code format 2 to '{}'", fCode2)
log.info("accession number: {}", get(AccessionNumber))

//set(XDSClassCode, code("DI_EXAM", "UHN"))
// constraint by study date, if specified in cfind
// dateRange = get(StudyDate)
// set(XDSServiceStartTime, dateRange)

// set(XDSAvailabilityStatus, [])

// setting extended metadata in find documents query
// set(XDSExtendedMetadata("studyInstanceUID"), get(StudyInstanceUID))

// creating xds codes
// codes = [code(code1, scheme1), code(code2, scheme2)]
// set(XDSEventCode, code(code1, scheme1), code(code2, scheme2))

