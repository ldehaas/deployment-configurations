log.debug("Starting image_to_hl7_order")
LOAD('GTAW_common.groovy')
LOAD('../normalization.groovy')

initialize('ORM', 'O01', '2.3')

import com.karos.rtk.common.HL7v2Date
import org.joda.time.DateTime

set('MSH-7', HL7v2Date.format(new DateTime(), HL7v2Date.DateLength.MILLISECOND3))

log.info("DICOM header before extracting local PID:\n{}", input)

def localAccessionNumber = get(AccessionNumber)

    log.info("Patient Accession Number from local: {}", localAccessionNumber)

def identifiers = new Identifiers(input)

set('PID-3-1', input.get(PatientID))

/* removing localization

def localPatientId = null

if (prefetchOrder != null) {
        localPatientId = prefetchOrder.get("PID-3-1")
        log.debug("Using PatientID from originating HL7 message: '{}'", localPatientId)
}

if (localPatientId == null) {
        localPatientId = identifiers.getLocalPatientId()
        log.debug("Using PatientID from SOP instance: '{}'", localPatientId)
}

if (localPatientId == null) {
        localPatientId = identifiers.getLocalPatientIdWithFailover()
        log.debug("Using prefixed PatientID from SOP instance: '{}'", localPatientId)
}
if (localPatientId != null && localPatientId.size() >= 1) {
   set('PID-3-1', localPatientId[0])
}

if (localPatientId != null && localPatientId.size() >= 2) {
   set('PID-3-2', localPatientId[1])
}

*/

        set('OBR-2', localAccessionNumber)
        set('OBR-3', localAccessionNumber)
        set('ORC-2', localAccessionNumber)
        set('ORC-3', localAccessionNumber)

def name = get(PatientName)
log.debug("name = '{}'", name);

if (name != null) {
    name = name.split('\\^')
    set('PID-5-1', name[0])

    if (name.length > 1) {
        set('PID-5-2', name[1])
    }

    if (name.length > 2) {
        set('PID-5-3', name[2])
    }
}

set('PID-7', get(PatientBirthDate))
set('PID-8', get(PatientSex))
set('PID-18', get(AdmissionID))
set('PID-19', get(MedicalRecordLocator))

/*
 * PV1 segment
 */
set('PV1-2',    'O')
set('PV1-3-4', 'SB')
set('PV1-11-1', 'EXTIMPRT')
set('PV1-11-2', 'ERI')
set('PV1-19', get(AdmissionID))

/*
 * ORC
 */
set('ORC-1', 'SC')
set('ORC-5', 'IP')

// Hack! We need the original modality, but foreign_image_localizer.groovy
// overwrites it with the normalized modality.  It stashes the original for
// us here:
def originalModality = get(ErrorComment)
def studyDesc = get(StudyDescription)
def (procCode, procDesc) = Normalization.getNormalizedCodeAndDesc(originalModality, studyDesc)

if (procDesc == null) {
    log.warn("Unable to find normalized procedure description based on modality '{}' " +
             " and study description '{}'. Not normalizing.", originalModality, studyDesc)
} else {
    set('OBR-4-2', procDesc)
}

if (procCode == null) {
    log.warn("Unable to find normalized procedure code based on modality '{}'. " +
             "Not normalizing.", originalModality)
} else {
    set('OBR-4-1', procCode)
}

set('OBR-14',   get(StudyDate))
set('OBR-16-1', 'EXTERNAL')
set('OBR-16-2', 'External,Physician')
set('OBR-18',   get(AccessionNumber))
set('OBR-25',   'R')

def date = getDate(StudyDate, StudyTime)
if (date != null) {
    set('OBR-27-4', HL7v2Date.format(date, HL7v2Date.DateLength.MILLISECOND3))
}

set('OBR-31', get(ReasonForStudy))

/*
 * ZDS
 */
output.getMessage().addNonstandardSegment('ZDS')
set('ZDS-1', get(StudyInstanceUID))

log.debug("This is the Foreign Study outgoing to PACS:\n {}", output)
