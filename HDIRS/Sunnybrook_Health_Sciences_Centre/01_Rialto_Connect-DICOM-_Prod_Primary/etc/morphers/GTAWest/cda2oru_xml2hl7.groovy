import groovy.util.slurpersupport.GPathResult

LOAD("GTAW_common.groovy")

log.info("Started conversion from CDA document to HL7 ORU message ...")

initialize("ORU", "R01", "2.3")
set('MSH-3', 'SHSC_RIALTO_GTAW')
set('MSH-4', 'FEM')
set('MSH-10', '1555473220052803268')

//log.info("prefetch order: {}", prefetchOrder)
def LocalPIDIssuer = null

def encounter   = input.componentOf.encompassingEncounter
def participant = input.componentOf.encompassingEncounter.encounterParticipant
def localAccessionNumber = header.get(AccessionNumber)
def CDAAccessionNumber = input.inFulfillmentOf.order.id.@extension.text()
def cdaAccessionSlotA = input.inFulfillmentOf.order.id.@extension.text()
def cdaAccessionSlotB = input.componentOf.encompassingEncounter.id.@extension.text()
def DIRbIRTHDAY = input.recordTarget.patientRole.patient.birthTime.@value.text()

log.info("CDA2ORU: local Accession Number: {}", localAccessionNumber)
log.info("CDA2ORU: CDA Accession Number Slot A: {}", cdaAccessionSlotA)
log.info("CDA2ORU: CDA Accession Number Slot B: {}", cdaAccessionSlotB)

def cdaAccessionNumber = null

if (cdaAccessionSlotA != null && !cdaAccessionSlotA.isEmpty()) {
    cdaAccessionNumber = cdaAccessionSlotA
}
if (cdaAccessionSlotB != null && !cdaAccessionSlotB.isEmpty()) {
    cdaAccessionNumber = cdaAccessionSlotB
}

if (cdaAccessionNumber == null || localAccessionNumber == null || !localAccessionNumber.contains(cdaAccessionNumber)) {
    log.info("XDS: Discarding Report As Accession number does not match first instance in study.")
    return false
}

// the 'header' variable includes other patient ID sequence with the local PID

//def identifiers = new Identifiers(header)

// Localize Patient ID
//def localPid = identifiers.getLocalPatientIdWithFailover()
//def localAccession = identifiers.getLocalAccessionNumberWithFailover()

// PID segment

log.info("CDA2ORU: DICOM header:\n {}", header)

def identifiers = new Identifiers(header)

// PID Segment

set('PID-3-1', header.get(PatientID))

set('PID-2', null)     // HCN
   set('PID-3-4-3', 'ISO')
set('PID-5-1', input.recordTarget.patientRole.patient.name.family.text())				// Patient Name (Family)
set('PID-5-2', input.recordTarget.patientRole.patient.name.given.text())				// Patient Name (Given)
set('PID-7-1', input.recordTarget.patientRole.patient.birthTime.@value.text())				// Patient Birth Date
set('PID-8', input.recordTarget.patientRole.patient.administrativeGenderCode.@code.text())		// Patient Gender



// OBR segment

def codingSystem = input.component[0].section.code.@codeSystemName.text()

set('OBR-2', cdaAccessionNumber)                                                                         // Accession Number
set('OBR-3', header.get(AccessionNumber))//input.inFulfillmentOf.order.id.@extension.text()             // Accession Number 
set('OBR-4-1', input.component[0].section.code.@code.text())						// Procedure Code (Identifier)
set('OBR-4-2', input.component[0].section.text.text())							// Procedure Code (Text)
set('OBR-4-3', codingSystem == 'LOINC' ? 'LN' : codingSystem)						// Procedure Code (Coding System)

set('OBR-7', encounter.effectiveTime.@value.text())							// Procedure Completed Datetime

set('OBR-16-1', input.participant.associatedEntity.associatedPerson.name.family.text())			// Referring Physician Name (Family)
set('OBR-16-2', input.participant.associatedEntity.associatedPerson.name.given.text())			// Referring Physician Name (Given)

set('OBR-5', input.confidentialityCode.text())        //(WTS) RIS priority of the exam    

if (prefetchOrder != null) {
    
//    set('PV-2', prefetchOrder.get('PV-2'))             // Patient type (One of: I, O, E, P)
    set('OBR-13', prefetchOrder.get('OBR-13'))         // Clinical information
    set('OBR-15-4', prefetchOrder.get('OBR-15-4'))     // Body part
    set('OBR-21-1', prefetchOrder.get('OBR-21-1'))     // (WTS) exam type -> MR or CT       
    set('OBR-21-2', prefetchOrder.get('OBR-21-2'))     // Exam room 
//    set('PID-3-1', prefetchOrder.get('PID-3-1'))       // Patient ID
    set('PID-11-1', prefetchOrder.get('PID-11-1'))      // Street name and num
    set('PID-11-3', prefetchOrder.get('PID-11-3'))     // City
    set('PID-11-5', prefetchOrder.get('PID-11-5'))     // Postal code
    set('PID-11-6', prefetchOrder.get('PID-11-6'))     // Country
    set('PID-13', prefetchOrder.get('PID-13'))    // Home phone
    set('PID-18', prefetchOrder.get('PID-18'))    // Account (visit number)
}

set('OBR-22', input.documentationOf.serviceEvent.effectiveTime.low.text())        //Date time from the report status
set('OBR-25', input.title.text().toLowerCase().contains('final') ? 'F' :          // Report status
              input.title.text().toLowerCase().contains('preliminary') ? 'P' :
              input.title.text().toLowerCase().contains('addendum') ? 'A' :
              null)
set('OBR-32', input.documentationOf.serviceEvent.code.codeSystemName.text())      //Hospital code of the radiologist that signed/validated the report

// ORC segment
set('ORC-1', "RE")
set('ORC-2', input.inFulfillmentOf.order.id.@extension.text()) //Order placer number
set('ORC-3', header.get(AccessionNumber))//input.inFulfillmentOf.order.id.@extension.text()          // Accession Number 
//set('ORC-5', "CM")
set('ORC-10', input.inFulfillmentOf.order.id.@extension.text()) //Order Enterer
set('ORC-12-1', input.componentOf.encompasingEncounter.encounterParticipant.assignedEntity.id.text()) //Hospital code of Requesting Physician
set('ORC-12-2', input.componentOf.encompasingEncounter.encounterParticipant.assignedEntity.assignedPerson.name.family.text()) //Requesting Physician Last Name
set('ORC-12-3', input.componentOf.encompasingEncounter.encounterParticipant.assignedEntity.assignedPerson.name.given.text()) //Requesting Physician First Name

// OBX segment

//set("OBX-3", input.documentationOf.code.code.text()) //Exam code

def rep=input.component.nonXMLBody.text.@representation.text()
def non_xml = input.component.nonXMLBody.text.text()
def decoded = null

if (rep != null && rep == 'B64') {
    decoded = new String(non_xml.decodeBase64())
} else {
    decoded = non_xml
}

def repetitions = decoded.split("\r")
    for (int r = 0 ; r < repetitions.size() ; ++r) {
        addOBX('GDT', null, null, repetitions[r])
}

set('OBX-11', input.title.text().toLowerCase().contains('final') ? 'F' :          // Report status
              input.title.text().toLowerCase().contains('preliminary') ? 'P' :
              input.title.text().toLowerCase().contains('addendum') ? 'A' :
              null)

log.info("... finished conversion from CDA document to HL7 ORU message as follows:")
log.info("ORU: {}", output)

return true
