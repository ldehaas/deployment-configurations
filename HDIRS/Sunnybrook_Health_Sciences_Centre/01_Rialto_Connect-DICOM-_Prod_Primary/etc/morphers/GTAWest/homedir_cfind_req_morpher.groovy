/* We are using this script to modify the incoming AdHoc requests to GTA West made from the PACS as they use 
   either the patientID for PID or the studyID for HCN lookups.  Connect needs the request to be in the patientID
      field for the cfind to proceed as expected.  
*/

log.debug("Starting HomeDIR cfind request morpher...")

def patientID = get(PatientID)

if (patientID == null || patientID.isEmpty()) {
log.error("Query does not contain patient ID")
return false
}

if (patientID.endsWith('*')) {
    patientID = patientID.substring(0, patientID.length()-1)
    log.info("Removing the trailing asterisk from the patientID - {}", patientID)
}

if (patientID ==~ '[0-9]{10}') {
   set(PatientID, null)
   set(StudyID, patientID)
   set(StudyIDIssuer, 'GLOBALDOMAIN')
} else {
set(IssuerOfPatientID, 'SUNB')
    if (patientID.endsWith('SHSC')) {
        log.info("Patient ID already suffixed - no changes made: {}", patientID)
    } else {
        patientID = patientID + 'SHSC'
        log.info("Patient ID not suffixed - adding suffix: {}", patientID)
    }
}

// The PACS automatically pulls Accession and SUID from the local study which causes lookup against GTA West.  Need to add this to remove those values.

//set(AccessionNumber, null)
//set(StudyInstanceUID, null)

log.debug("Done HomeDIR cfind request morpher...")
