/* This script takes the DICOM C-Find responses from the DIR and filters
   out local studies based on the IssuerOfPatientID returned by the DI-r
*/

log.debug("Starting cfind_response_filter")
LOAD("common.groovy")

log.info("This is the recevied object from the DIR: \n {}", input)

def pids = new Pids(input)

if (pids.getSourceDomain() == null) {
    log.error("Unable to process cfind response: missing source issuer")
    return false
}

if (pids.isLocal()) {
    log.debug("Source issuer '{}' is local.  Filtering this cfind response", pids.getSourceDomain())
    return false
}

if (pids.isSourceUnwantedDomain()) {
    log.info("Source issuer '{}' is an unwanted domain.  Filtering this cfind response", pids.getSourceDomain())
    return false
}

if (pids.getHealthCardNumber() == null) {
    log.error("A Valid HCN was not found, rejecting this study")
    return false
}
