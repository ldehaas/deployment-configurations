
/*
 * morphs the scn message to be sent to the dir
 *
 * Rialto generates SCN hl7 message to be sent to DIR, and passes down the hl7 message to the morpher
 *
 * NOTE: absence of the morpher does not disable any part of the workflow
 */
// KHC1497 - add unique IssuerOfPatientID field as 'TOH' if it is missing in the HL7 messages we send out to the DIR
if (get('PID-3-4-2')==null || get('PID-3-4-2') == "" ) {
    set('PID-3-4-2','TOH')
    set('PID-3-4-3','ISO')
}
