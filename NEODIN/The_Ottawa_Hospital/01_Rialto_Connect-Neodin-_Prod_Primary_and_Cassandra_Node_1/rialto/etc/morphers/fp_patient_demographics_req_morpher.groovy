/*
 * The presense of the morpher enables doing a patient level query in Forward Prior Exam workflow.
 * The c-find response will be available to morpher configured as ForwardPriorExamsForeignImageMorpher
 */

// PatientID is set before passing down the request to the morpher


set(PatientName, '');
set(PatientBirthDate, '');

