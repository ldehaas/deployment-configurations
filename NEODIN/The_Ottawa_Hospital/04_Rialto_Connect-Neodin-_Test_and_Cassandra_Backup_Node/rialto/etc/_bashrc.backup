#
# Standard .bashrc for Rialto deployments
#
# Feel free to send suggestions to support@karoshealth.com
#
# Written by John E. Lammers, August 2010
# Karos Health Inc., www.karoshealth.com
#

# Get system defaults
if [ -f /etc/bashrc ]; then
    . /etc/bashrc
fi

# Configuration
RIALTO_HOME=~/rialto
export PGDATABASE=ids

# Enable vi editing on the command line (activated by pressing ESC)
set -o vi

#######################################
# Handy commands for supporting Rialto
# (alphabetical order)
#######################################

# Edit Rialto configuration file
alias cfg="vi $RIALTO_HOME/etc/rialto.cfg.xml"

# List Rialto's file descriptors (open files and sockets currently open)
check-fds() {
    command ls -lrt /proc/$(cat /tmp/rialto.pid)/fd
}

# Search Rialto's log files for errors and warnings
check-logs() {
    pushd $RIALTO_HOME/log
    echo '---------- Scanning for errors, warnings and exceptions ----------'
    grep -P 'WARN|ERROR|^[^2].*xception|^[\t ]*at ' *.log
    grep -i -E 'warn|error|fatal|exception' stdout.log
    if [ -s stderr.log ]; then
        echo '---------- Contents of stderr.log ----------'
        cat stderr.log
    fi
    popd
}

alias cl=check-logs

# Append note to Rialto log file
note() {
    echo '###############' "$*" '###############' >>$RIALTO_HOME/log/rialto.log
}

# Thin logs - delete log files that contain no errors or warnings
thin-logs() {
    pushd $RIALTO_HOME/log
    for i in rialto.log* ; do
        if [ -f "$i" ]; then
            echo -n "Checking $i ... "
            if grep -q -E 'WARN|ERROR|FATAL|xception' $i 2>/dev/null ; then
                echo 'contains problems'
            else
                rm -f "$i"
                echo 'no errors - deleted'
            fi
        fi
    done
    popd
}

# Delete Rialto's log files (must be done while Rialto is shut down)
alias clean-logs="rm -f $RIALTO_HOME/log/rialto* $RIALTO_HOME/log/std*"

# Preserve file permissions and datestamp when copying files
alias cp='cp -p'

# Edit .bashrc (this file), then execute it to activate changes
alias eb='vi ~/.bashrc ; . ~/.bashrc'

# Retrieve a file from the specified CAStor node
get-from-castor() {
    if [ $# -ne 2 ]; then
        echo "Usage: get-from-castor <host> <ticket>"
    else
        wget "http://$1/$2"
    fi
}

# Display information about a file stored in CAStor
get-ticket-info() {
    if [ $# -ne 1 ]; then
        echo "Usage: get-ticket-info <ticket>"
    else
        castor.sh info $1
    fi
}

# Prevent connection timeout (handy for working over a VPN)
keepalive() {
    while true ; do
        echo -n '.'
        sleep 5
    done
}

# For lazy typists like me
alias l='ls -l'
alias l.='ls -d .* --color=tty'
alias less='less -R'

# Go to Rialto's log file directory
alias log="cd $RIALTO_HOME/log ; ls -l"

# Edit Rialto's logging configuration file
alias log4="vi $RIALTO_HOME/etc/log4j.xml"

# List by columns, tag with indicator characters, use color when available
alias ls='ls -CF --color=tty'

# Display master tickets
master() {
    echo -n 'Master ticket for database backup tickets = '
    file=~/var/db-backup/.master
    if [ -f $file ]; then
        cat $file
    else
        echo '(no database backups yet)'
    fi
    echo -n 'Master ticket for ticket log backup tickets = '
    file=~/var/vault-tickets-backup/.master
    if [ -f $file ]; then
        cat $file
    else
        echo '(no ticket log backups yet)'
    fi
    echo 'RECORD THESE TICKETS AND STORE OFFSITE!'
}

# Display horizontal rule (handy visual marker)
alias n='echo "##############################################################"'

# Grep for a running process, all owned by default
psg() {
    local pattern="$1"
    if [ -z "$pattern" ]; then
        pattern=$USER
    fi
    ps -ef | grep -v grep | grep "$pattern"
}

# Archive old Rialto logs and start fresh (must be done while Rialto is shut down)
save-logs() {
    pushd $RIALTO_HOME/log
    mkdir -p old
    local file="old/$(date '+%Y%m%d_%H%M%S').tgz"
    tar czf "$file" rialto* st*
    ls -l "$file"
    rm -f rialto* std*
    popd
}

# Follow (tail) the end of the Rialto log file.
# (Press CTRL-C to break out so you can page up/down etc.  Use "vi" commands)
tl() { # Tail Log
    local file=$RIALTO_HOME/log/rialto.log
    if [ ! -r $file ]; then
        echo "Waiting for $file to be created"
        while [ ! -r $file ]; do
            echo -n '.'
            sleep 5
        done
        echo
    fi
    less -i +F $file
}

# Display Rialto software version
alias build-info="cat $RIALTO_HOME/*buildinfo.txt"
alias version="cat $RIALTO_HOME/*buildinfo.txt"
alias versions='grep rialto\.version $HOME/rialto*/*buildinfo.txt'

# Summary to paste into JIRA's "Environment:" field when reporting an issue.
show-env() {
    echo '### Product ###'
    grep -E 'rialto.product|rialto.version' ~/rialto/*_buildinfo.txt
    echo
    echo '### Environment ###'
    cat /etc/*-release
    uname -a
    java -version
    x86info | grep CPU
    free
}

# Start / stop Rialto
alias start='echo -n "root ";su -c "service rialto start"'
alias stop='echo -n "root ";su -c "service rialto stop"'

# Vi IMproved
if [ -x /usr/bin/vim ]; then
    alias vi='vim'
fi

# Set xterm window title (handy to keep your windows organized)
wtl() {
    if [ "$1" ]; then
        echo -n "]0;$1"
    else
        echo -n "]0;${USER}@$(hostname):${PWD}"
    fi
}

# Interactive process killing by name
alias zap='killall -i'
