
/*
 * morphs the purge message to be sent to the dir.
 * 
 * Rialto generates hl7 purge message and passes down the hl7 message to the morpher.
 * 
 * NOTE: absence of the morpher does not disable any part of the workflow
 *
 */
if (get('PID-3-4-2')==null || get('PID-3-4-2') == "" ) {
    set('PID-3-4-2','<NEODIN>')
}
