
def parsePatientName(var) {
    String[] result = new String[5];
    for (int i = 0; i < 5; i++) {
        result[i] = null;
    }

    String full = var.get(PatientName);
    if (full == null) {
        return result;
    }

    // chop off sections after first '='
    int endOfLatin = full.indexOf('=');
    if (full.indexOf('=') > -1) {
        full = full.substring(0, endOfLatin);
    }

    // and split into components separated by ^
    String[] split = full.split("\\^");
    for (int i = 0; i < split.length && i < result.length; i++) {
        String component = split[i].trim();
        if (component.length() > 0) {
            result[i] = component;
        }
    }

    return result;
}


def setPatientLastName(var, patientLastName) {

  oldPatientName = var.get(PatientName)
  if (oldPatientName == null) {
		log.warn("patient name is missing in {}", var.get(StudyInstanceUID));
		oldPatientName = "";
  }
	int i = oldPatientName.indexOf("^");
	if (i == -1) {
		patientName = patientLastName;
  }
	patientName = patientLastName + "^" + oldPatientName.substring(i+1)
	log.debug("setting PatientName to {}", patientName);
	var.set(PatientName, patientName);
}

