/*
 * Morphs the c-find to pacs in AdhocRetrieve workflow to get back the study metadata. 
 * the c-find response will be available to the morpher configured as
 * AdhocRetrievePriorExamsForeignImageMorpher 
 */


// Query for Acession Number
set(AccessionNumber, "")
// Query for Patient Name
set(PatientName, "")
// Qeury for Patient ID
set(PatientID, "")
