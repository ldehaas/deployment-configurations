
/*
 * Morphs the dicom header image in the retrieve prior exam workflow.
 *
 * Rialto morphes the StudyInstanceUID to match what is in the PACS and passes down the header image to the morpher.
 * the result of the c-find to PACS also is passed down to the morpher along the arriving dicom image.
 *
 * NOTE: absence of the morpher does not disable any part of the workflow
 */


def getPatientName(var) {
    String[] result = new String[5];
    for(int i =0; i < 5; i++){
        result[i] = null;
    }

    String full = var.get(PatientName);
    if (full == null) {
        return result;
    }

    int endOfLatin = full.indexOf('=');
    if (full.indexOf('=') > -1) {
        full = full.substring(0, endOfLatin);
    }

    //String[] split = full.split("\\^");
    // OsiriX is not happy with ^
    String[] split = full.split("\\^| ");
    for (int i = 0; i < split.length && i < result.length; i++) {
        String component = split[i].trim();
        if (component.length() > 0) {
            result[i] = component;
        }
    }
    return result;
}



patientLastName = (getPatientName(input))[0];
patientLastNameInPACS = (getPatientName(cfindResponse))[0];

log.info("patientLastName is {}", patientLastName);
log.info("patientLastName in the pacs is {}", patientLastNameInPACS);

patientFirstName = (getPatientName(input))[1];
patientFirstNameInPACS = (getPatientName(cfindResponse))[1];


log.info("patientFirstName is {}", patientFirstName);
log.info("patientFirstName in the pacs is {}", patientFirstNameInPACS);


if ( patientLastNameInPACS != patientLastName ) {
    return false;
}


// morph the accession number the same as what is stored in the PACS
set(AccessionNumber, cfindResponse.get(AccessionNumber))

// morph the patient name the same as what is stored in the PACS
patientName = cfindResponse.get(PatientName);


set(PatientName, cfindResponse.get(PatientName))

// morph the patientID the same as what is stored in the PACS
set(PatientID, cfindResponse.get(PatientID))




