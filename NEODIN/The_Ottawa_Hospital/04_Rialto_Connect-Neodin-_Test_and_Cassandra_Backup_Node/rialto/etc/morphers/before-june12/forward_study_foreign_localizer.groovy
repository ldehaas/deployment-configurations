/*
 * Morphs the dicom header image in the forward prior exam workflow.
 *
 * Rialto morphs the StudyInstanceUID to match what is in the PACS and passes down the header image to the morpher.
 * the result of the c-find to PACS (if any) also is passed down to the morpher along the arriving dicom image.
 * 
 * If the morpher returns false, the study won't be pushed to the PACS but SCN will be sent to the DI'R.
 * If the morpher throws an exception, the c-store from the DI'R will be rejected and no SCN will be sent to the DI'R.
 *
 * NOTE: absence of the morpher does not disable any part of the workflow
 */

patientID = get(PatientID)


if (cfindResponse != null) {
    // the result of cfindResponse to PACS is present
    patientIDInPACS = cfindResponse.get(PatientID) 
    if (cfindResponse.get(PatientID) != get(PatientID)) {
    
        log.warn("the patientID in the pacs {} does not match with the patientID in the image {}", patientID, patientIDInPACS );
        // if you return false here, rialto does not proxy the image to pacs but the SCN will be sent to the DIR
        // if you throw exception, rialto reject the cstore from DIR and does not send SCN to the DIR
        return false;
    }
}



