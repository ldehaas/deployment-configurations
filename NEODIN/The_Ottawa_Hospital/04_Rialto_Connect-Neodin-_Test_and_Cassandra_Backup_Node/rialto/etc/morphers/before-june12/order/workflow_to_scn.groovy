
/*
 * Generates an HL7 SCN to be sent to DIR out of the workflow
 */
LOAD('common.groovy')

initialize('ORM', 'O01', '2.5')

import com.karos.rtk.common.HL7v2Date
import org.joda.time.DateTime


//sending application
//set('MSH-3', "RIALTO")
//Sending Facility
//set('MSH-4', "TOH")
//Receiving Application
set('MSH-5', "Centricity RIS-IC")

//Receiving Facility
set('MSH-6', "NEODIR")


//Patient Identifier List
set('PID-3', workflow.getLocalPatientId())
for(int i = 0; i < workflow.getOtherPatientIds().size(); i++) {
    set('PID-3(' + (i+1) +')' , workflow.getOtherPatientIds().get(i));
}

//Patient Nmae
set('PID-5', workflow.getPatientName())

//Date of Birth
set('PID-7', workflow.getDateOfBirth())

//Sex
set('PID-8', workflow.getSex())

//Accession Number
set('OBR-3', workflow.getAccessionNumber())

//Study Instance UID
set('ZDS-1', workflow.getLocalStudyInstanceUID())



//Exam status
set('ZDS-2', 'CM')

set('ZDS-3', localAE)

