
/*
 * Convert an order message into a cfind message.
 * get() works on the order message, while set() works on the 
 * cfind message.
 * 
 * The order is ignored if this script returns false,
 * e.g. no images will be fetched for the order.
 */
LOAD('common.groovy')
LOAD('procedure_codes.groovy')

initialize('ORM', 'O01', '2.3')

import com.karos.rtk.common.HL7v2Date
import org.joda.time.DateTime



def siuid = orderExtraInfo.getStudyInstanceUIDInPACS();
set('ZDS-1-1', siuid)
set('PID-3-1', get('PID-3-1'))
set('ORC-2', get('ORC-2'))

//output.setPlainHL7Message(input.toString())

