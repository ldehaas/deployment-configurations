
/*
 * Convert an order message into a cfind message.
 * get() works on the order message, while set() works on the 
 * cfind message.
 * 
 * The order is ignored if this script returns false,
 * e.g. no images will be fetched for the order.
 */

LOAD('common.groovy')

// Get the PID from PID-3-1
def pid = get( "PID-3-1" )
def accessionNumber = get( "ORC-2" )

def siuid = get ("ZDS-1-1")


output.setPid( pid )

output.setIssuerOfPatientID("neodin")
output.setAccessionNumber( accessionNumber )
output.setStudyInstanceUID( siuid) 


