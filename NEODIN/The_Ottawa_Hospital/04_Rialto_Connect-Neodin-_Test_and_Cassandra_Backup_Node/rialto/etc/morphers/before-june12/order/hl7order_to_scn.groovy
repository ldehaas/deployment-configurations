
/*
 * Convert an order message into a cfind message.
 * get() works on the order message, while set() works on the 
 * cfind message.
 * 
 * The order is ignored if this script returns false,
 * e.g. no images will be fetched for the order.
 */
LOAD('common.groovy')
LOAD('procedure_codes.groovy')


import com.karos.rtk.common.HL7v2Date
import org.joda.time.DateTime


set('PID-3-1', get('PID-3-1'))
set('ORC-2', get('ORC-2'))

// set study instance uid
set('ZDS-1-1', get('ZDS-1-1'))

// set CM
set('ZDS-2', "CM")

//output.setPlainHL7Message(input.toString())

