
/*
 * Convert an order message into a cfind message.
 * get() works on the order message, while set() works on the 
 * cfind message.
 * 
 * The order is ignored if this script returns false,
 * e.g. no images will be fetched for the order.
 */

LOAD('common.groovy')

// Get the PID from PID-3-1
def pid = get( "PID-3-1" )
if ( null != pid) {
	log.debug("found pid '{}'", pid)
}
else {
	log.debug("Did not find a PatientID for this order")
}

//Checking for a valid health card number with 10-digits
def hcn = get("/.PID-3(1)-1")
def valid = hcn ==~ /\d{10}.*/
if (!valid) {
    log.warn("Health Card Number '{}' not valid ignoring this order", hcn);
    return false
}

// construct the cfind
set ( PatientID, pid )

//FIXME I commented setting issuerOfPatientID for now
set( IssuerOfPatientID, '' )

//set ( IssuerOfPatientID, Constants.PrimaryLocalIssuerID )
	// IssuerOfPID is hard coded to Kingston's issuer of PID

// set return keys
set ( StudyInstanceUID, null )

// the following isn't necessary, but useful for debugging
set ( AccessionNumber, null )

//now = new org.joda.time.DateTime()
//limit = now.minusMonths(7)
//set ( StudyDate, org.joda.time.format.DateTimeFormat.forPattern('yyyyMMdd-').print(limit) )






