#! /bin/bash

# KHC10999 - Check for active server and failover to passive server using Corosync

# Check to see what server is active
ActiveServer=$(crm_mon -1 | grep Started | awk '{print $4}')
myDate=`date`

# Failover and get the Exit code for crm_resource
if [ $ActiveServer = "connect-ha1.neodin.ca" ] ; then
	passiveServer="connect-ha2.neodin.ca"
    /usr/sbin/crm_resource --resource virtualIP --move --node connect-ha2.neodin.ca
    retCode=$?
    echo $retCode
else
	passiveServer="connect-ha1.neodin.ca"
    /usr/sbin/crm_resource --resource virtualIP --move --node connect-ha1.neodin.ca
    retCode=$?
    echo $retCode
fi 

# If the Exit code is 0, write to the log file /var/log/switchServer.log it worked
if [ $retCode = 0 ] ; then
    /usr/sbin/crm_resource --resource virtualIP --un-move
    echo "$myDate - Failover to $passiveServer completed with status code $retCode." >> /var/log/switchServer.log
else
    echo "$myDate - Failover from $ActiveServer failed with status code $retCode." >> /var/log/switchServer.log
fi