// Morphing rules for the Imaging Archive

// Get Sending
def sendingFacility = get('/.MSH-4')

// Set Sending
set('/.MSH-3', 'EMPI')
set('/.MSH-4', 'EMPI')

// Set Receiving Application
set('/.MSH-5', 'GE_ENTARCH')

// Set Virtual Archive
def virtualArchiveIDMap = [ 'BGHS':'GTAWPROD_BGHS',
                            'CGMH':'GTAWPROD_CGMH',
                            'CVH':'GTAWPROD_CVH',
                            'SICKKIDS':'GTAWPROD_HSC',
                            'HBKR':'GTAWPROD_HSC',
                            'HHCC':'GTAWPROD_HHCC',
                            'HHS':'GTAWPROD_HHS',
                            'HHSO':'GTAWPROD_HHS',
                            'HHSM':'GTAWPROD_HHS',
                            'HHSG':'GTAWPROD_HHS',
                            'MAHC':'GTAWPROD_OSMH',
                            'GBGH':'GTAWPROD_OSMH',
                            'OSMH':'GTAWPROD_OSMH',
                            'RVH':'GTAWPROD_RVH',
                            'SJHC':'GTAWPROD_SJHC',
                            'WOHS':'GTAWPROD_WOHS',
                            'THC':'GTAWPROD_THC',
                            'THCM':'GTAWPROD_THC',
                            'THCT':'GTAWPROD_THC',
                            'CAMH':'GTAWPROD_UHN',
                            'TRI':'GTAWPROD_UHN',
                            'UHN':'GTAWPROD_UHN',
                            'WCH':'GTAWPROD_UHN',
                            'MSH':'GTAWPROD_UHN',
                            'WOHS':'GTAWPROD_WOHS',
                            'WOHSC':'GTAWPROD_WOHS',
                            'WOHSE':'GTAWPROD_WOHS',
                            'WOHSR':'GTAWPROD_WOHS',
                            'WPHC':'GTAWPROD_HRRH',
                            'HRRH':'GTAWPROD_HRRH' ]

// Set Receiving Facility
if (virtualArchiveIDMap[sendingFacility] != null) {
    set('/.MSH-6', virtualArchiveIDMap[sendingFacility])
} else {
    set('/.MSH-6', 'UNKNOWN')
}

// Remove the &&ISO from the first PID along with all repeating fields in PID-3
def pid_iter = 0
while ((curr_pid = get('/.PID-3(' + pid_iter + ')-1')) != null ||
     (curr_uid = get('/.PID-3(' + pid_iter + ')-4-2')) != null) {
    if (pid_iter == 0) {
        set('/.PID-3(' + pid_iter + ')-4-1', get('/.PID-3(' + pid_iter + ')-4-2'))
    } else {
        set('/.PID-3(' + pid_iter + ')-1', null)
    }

    set('/.PID-3(' + pid_iter + ')-4-2', null)
    set('/.PID-3(' + pid_iter + ')-4-3', null)
    set('/.PID-3(' + pid_iter + ')-5', null)

    pid_iter++
}

// Remove the &&ISO from the first MRG PID along with all repeating fields in MRG-1
def messageType = get('/.MSH-9-1')
def triggerEvent = get('/.MSH-9-2')

if (!("ADT".equals(messageType) && "A40".equals(triggerEvent))) {
    return
}

def mrg_iter = 0
while ((curr_pid = get('/.MRG-1(' + mrg_iter + ')-1')) != null ||
     (curr_uid = get('/.MRG-1(' + mrg_iter + ')-4-2')) != null) {
    if (mrg_iter == 0) {
        set('/.MRG-1(' + mrg_iter + ')-4-1', get('/.MRG-1(' + mrg_iter + ')-4-2'))
    } else {
        set('/.MRG-1(' + mrg_iter + ')-1', null)
    }

    set('/.MRG-1(' + mrg_iter + ')-4-2', null)
    set('/.MRG-1(' + mrg_iter + ')-4-3', null)
    set('/.MRG-1(' + mrg_iter + ')-5', null)

    mrg_iter++
}
