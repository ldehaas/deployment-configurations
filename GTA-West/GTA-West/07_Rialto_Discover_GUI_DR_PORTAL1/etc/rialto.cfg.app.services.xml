<config>

    <!-- ####################### -->
    <!--         SERVERS         -->
    <!-- ####################### -->

    <!-- Audit Record Repository User Interface and Restful Web Service -->
    <server type="http" id="auditRecordRepository">
        <!--host>${BindAddress}</host-->
        <port>${ArrApiPort}</port>
    </server>

    <!-- Discover KOS Manifest Sync Web Interface -->
    <server type="http" id="kosmanifestsync-http">
        <!--host>${BindAddress}</host-->
        <port>${SyncToolPort}</port>
    </server>

    <!-- Master Patient Index HL7 V2 Inteface -->
    <server id="mpi-hl7v2" type="hl7v2">
        <!--host>${BindAddress}</host-->
        <port>${PIXPDQProviderPort}</port>
    </server>

    <!-- XDS Repository Interface -->
    <server id="xds" type="http">
        <!--host>${BindAddress}</host-->
        <port>${Repository-Port}</port>
    </server>

    <!-- ####################### -->
    <!--  SERVICE CONFIGURATION  -->
    <!-- ####################### -->

    <!-- Audit Record Repository Service -->
    <service id="arrudp" type="arr">

        <tls name="repository">
            <keystore>samplePrivateKeyA.jks</keystore>
            <truststore>samplePublicKeyA.jks</truststore>
            <clientauth>true</clientauth>
            <ciphers>AES</ciphers>
        </tls>

        <tls name="forwarder">
          <keystore>client.jks</keystore>
          <keystorepass>clientKeyStorePassword</keystorepass>
          <truststore>client.jks</truststore> 
          <truststorepass>clientKeyStorePassword</truststorepass>
          <ciphers>AES</ciphers>
          <v2hello>false</v2hello>
        </tls>

        <!-- Restful Web Service for ARR -->
        <server idref="auditRecordRepository" name="arr-api">
            <url>/auditRecords/*</url>
        </server>

        <server idref="auditRecordRepository" name="arr-index-backup-api">
            <url>/auditRecordsIndexBackup/*</url>
        </server>

        <config>
            <prop name="IndexDir" value="${rialto.rootdir}/var/arrudp/index" />
            <prop name="IndexBackupDir" value="${rialto.rootdir}/var/arrudp/index-backup" />
            <prop name="ArchiveDir" value="${rialto.rootdir}/var/arrudp/archive" />
            <prop name="TempDir" value="${rialto.rootdir}/var/arrudp/temp" />
            <prop name="Listener" value="udp:${ArrPort}" />
            <prop name="ForwardDestination" value="${ArrForwardDestination}" />
            <prop name="ForwarderCheckpointFolder" value="${rialto.rootdir}/var/arrudp/checkpoints" />
        </config>

    </service>

    <!-- XDS Repository Service -->
    <service id="xdsrepo" type="xdsrepo">

        <device idref="pix" />
        <device idref="pdq" />
        <device idref="xdsreg" />

        <server idref="xds" name="repository">
            <url>/repo</url>
        </server>

        <config>
            <prop name="UniqueId" value="${Repository-ID}" />
            <prop name="StorageType" value="JPA" />
            <prop name="TempDir" value="${rialto.rootdir}/var/xdsrepo/tmp" />
            <prop name="FileStorage" value="${rialto.rootdir}/var/xdsrepo" />
            <prop name="IndexerDatabaseURL">${OracleURLWithPlaintextPassword}</prop>
            <!-- <prop name="DatabaseURL">${OracleDBURLPrefix}<encrypted>${OracleDBEncryptedPassword}</encrypted>${OracleDBURLSuffix}</prop> -->
        </config>

    </service>

    <!-- Discover Admin service -->
    <service id="discoveradmin.server" type="discoveradmin.server">

        <device idref="pix" />
        <device idref="pdq" />
        <device idref="xdsrep" />
        <device idref="xdsreg" />
        <device idref="auditDevice" />

        <!-- RESTful API for Discover Admin service-->
        <server idref="discover-admin-http" name="web-api">
            <url>${AdminUIApiURL}/api/*</url>
        </server>

        <config>

            <prop name="ServiceTimeout" value="60s" />
            <prop name="DatabaseURL">${OracleURLWithPlaintextPassword}</prop>
            <!-- <prop name="DatabaseURL">${OracleDBURLPrefix}<encrypted>${OracleDBEncryptedPassword}</encrypted>${OracleDBURLSuffix}</prop> -->
            <prop name="RegionalPatientIdentifierDomain" value="${RegionalPIDDomain}" />
            <prop name="PDQProviderHost" value="${PIXPDQProviderHost}" />
            <prop name="PDQProviderPort" value="${PIXPDQProviderPort}" />
            <prop name="SendingApplication" value="RIALTO_DISCOVER" />
            <prop name="SendingFacility" value="KAROS_HEALTH" />
            <prop name="ReceivingApplication" value="SPIRIT" />
            <prop name="ReceivingFacility" value="OTHER" />

            <!-- Optional: configure refresh interval for GUI - default is 5 seconds -->
            <!-- prop name="GUIRefreshInterval" value="5000ms" / -->

            <!-- Patient Identity Domains -->
            <include location="patientidentitydomains.cfg.xml" />

        </config>

    </service>



    <!-- Master Patient Index Service -->
    <service type="mpi" id="mpi">

        <device idref="pix" />
        <device idref="xdsrep" />
        <device idref="xdsreg" />
        <device idref="auditDevice" />

        <server idref="mpi-hl7v2" />

        <tls>
            <keystore>samplePrivateKeyA.jks</keystore>
            <!--keystorepass>passphrase</keystorepass-->
            <truststore>samplePublicKeyA.jks</truststore>
            <!--truststorepass>passphrase</truststorepass-->
            <clientauth>true</clientauth>
            <ciphers>NULL, AES</ciphers>
        </tls>

        <config>

            <prop name="DatabaseURL">${OracleURLWithPlaintextPassword}</prop>
            <!-- <prop name="DatabaseURL">${OracleDBURLPrefix}<encrypted>${OracleDBEncryptedPassword}</encrypted>${OracleDBURLSuffix}</prop> -->
            <prop name="RegionalDomain" value="${RegionalPIDDomain}" />
            <prop name="SendingApplication" value="RIALTO_DISCOVER" />
            <prop name="SendingFacility" value="KAROS_HEALTH" />
            <prop name="ReceivingApplication" value="SPIRIT" />
            <prop name="ReceivingFacility" value="OTHER" />

            <prop name="XDSDocumentRegistry">
                <host>${XDSDocumentRegistryHost}</host>
                <port>${XDSDocumentRegistryHL7v2Port}</port>
                <enableTLS>false</enableTLS>
                <morphingScript>${rialto.rootdir}/etc/mpi/morphing/XDSDocumentRegistry.groovy</morphingScript>
            </prop>

            <prop name="RegionalPID">
                <prefix>UHN</prefix>
                <domain>${RegionalPIDDomain}</domain>
            </prop>

            <prop name="PIXPDQProvider">
                <host>${PIXPDQProviderHost}</host>
                <port>${PIXPDQProviderHL7v2Port}</port>
                <enableTLS>false</enableTLS>
                <morphingScript>${rialto.rootdir}/etc/mpi/morphing/PIXPDQProvider.groovy</morphingScript>
                <timeoutMS>60000</timeoutMS>
            </prop>

            <prop name="PatientIdentityFeedDestinations">
                <destination name="Imaging Archive">
                    <host>${ImagingArchiveHost}</host>
                    <port>${ImagingArchiveHL7Port}</port>
                    <enableTLS>false</enableTLS>
                    <morphingScript>${rialto.rootdir}/etc/mpi/morphing/ImagingArchive.groovy</morphingScript>

                    <messageFilter>
                        <triggerEventCode>A08</triggerEventCode>
                        <triggerEventCode>A40</triggerEventCode>
                    </messageFilter>
                    <timeoutMS>30000</timeoutMS>
                </destination>
            </prop>
        </config>
    </service>

    <!-- KOS Manifest Sync Service -->
    <service type="discover.kosmanifestsync" id="discover.kosmanifestsync">

        <device idref="pix" />
        <device idref="xdsrep" />
        <device idref="xdsreg" />
        <device idref="imagingArchiveHL7" name="imagingArchiveHL7"/>
        <device idref="imagingArchiveDICOM" name="imagingArchiveDICOM"/>
        <device idref="auditDevice" />

        <server idref="kosmanifestsync-http" name="web-api">

            <url>/api/*</url>

        </server>

        <config>

            <prop name="DatabaseURL">${OracleURLWithPlaintextPassword}</prop>
            <!-- <prop name="DatabaseURL">${OracleDBURLPrefix}<encrypted>${OracleDBEncryptedPassword}</encrypted>${OracleDBURLSuffix}</prop> -->
            <prop name="RegionalPatientIdentifierDomain" value="${RegionalPIDDomain}" />
            <prop name="AE" value="RIALTO_DISCOVER" />
            <prop name="TaskScheduleCleanupInterval" value="60m" />
            <prop name="DocRegistryScanInterval" value="5m" />

            <!-- Uncomment this to enable the startup scan (used for fail-over)
            <prop name="StartupDocRegistryScanInterval" value="30m" />
            -->

            <prop name="AutoSyncEnabled" value="false" />

            <!-- Uncomment this to enable clustering 
            <prop name="Cluster" >

                <name>RIALTO_DISCOVER_KOSMANIFESTSYNC</name>

                <udpstack>

                    <bindAddress>10.0.1.61</bindAddress>
                    <multicastAddress>232.10.10.232</multicastAddress>
                    <multicastPort>45500</multicastPort>

                </udpstack>

            </prop>
            -->

            <include location="patientidentitydomains.cfg.xml" />

        </config>

    </service>

    <!-- Doc Drentelen service -->
    <service type="discover.docdrentelen" id="discover.docdrentelen">

        <device idref="pix" />
        <device idref="xdsrep" />
        <device idref="xdsreg" />
        <device idref="auditDevice" />

        <server idref="discover-admin-http" name="web-api">

            <url>${DrentelenApiURL}/api/*</url>

        </server>

        <config>

            <prop name="DatabaseURL">${OracleURLWithPlaintextPassword}</prop>
            <!-- <prop name="DatabaseURL">${OracleDBURLPrefix}<encrypted>${OracleDBEncryptedPassword}</encrypted>${OracleDBURLSuffix}</prop> -->
            <prop name="RegionalPatientIdentifierDomain" value="${RegionalPIDDomain}" />
            <prop name="TaskScheduleCleanupInterval" value="60m" />
            <prop name="AutoDrentelenEnabled" value="true" />

            <!-- Uncomment this to enable clustering
            <prop name="Cluster" >

                <name>RIALTO_DISCOVER_DOCDRENTELEN</name>

                <udpstack>

                    <bindAddress>10.0.1.96</bindAddress>
                    <multicastAddress>232.10.10.221</multicastAddress>
                    <multicastPort>45501</multicastPort>

                </udpstack>

            </prop>
            -->

            <prop name="PatientLinkHistoryScans" >

                <scan name="Immediate" delay="1m" />
                <scan name="Half-time Show" delay="12h" />
                <scan name="Nightly" delay="24h" />

            </prop>

            <include location="patientidentitydomains.cfg.xml" />

        </config>

    </service>

</config>
