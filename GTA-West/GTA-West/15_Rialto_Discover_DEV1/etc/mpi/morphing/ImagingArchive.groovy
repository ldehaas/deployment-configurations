// Morphing rules for the Imaging Archive

// Get Sending
def sendingFacility = get('/.MSH-4')

// Set Sending
set('/.MSH-3', 'EMPI')
set('/.MSH-4', 'EMPI')

// Set Receiving Application
set('/.MSH-5', 'GE_ENTARCH')

// Set Virtual Archive
def virtualArchiveIDMap = [ 'BGHS':'GTAWDEV_BGHS',
                            'CGMH':'GTAWDEV_CGMH',
                            'CVH':'GTAWDEV_CVH',
                            'SICKKIDS':'GTAWDEV_HSC',
                            'HSC':'GTAWDEV_HSC',
                            'HBKR':'GTAWDEV_HSC',
                            'HHCC':'GTAWDEV_HHCC',
                            'HHS':'GTAWDEV_HHS',
                            'HHSO':'GTAWDEV_HHS',
                            'HHSM':'GTAWDEV_HHS',
                            'HHSG':'GTAWDEV_HHS',
                            'MAHC':'GTAWDEV_OSMH',
                            'GBGH':'GTAWDEV_OSMH',
                            'OSMH':'GTAWDEV_OSMH',
                            'RVH':'GTAWDEV_RVH',
                            'SJHC':'GTAWDEV_SJHC',
                            'WOHS':'GTAWDEV_WOHS',
                            'THC':'GTAWDEV_THC',
                            'THCM':'GTAWDEV_THC',
                            'THCT':'GTAWDEV_THC',
                            'CAMH':'GTAWDEV_UHN',
                            'TRI':'GTAWDEV_UHN',
                            'UHN':'GTAWDEV_UHN',
                            'WCH':'GTAWDEV_UHN',
                            'MSH':'GTAWDEV_UHN',
                            'WOHS':'GTAWDEV_WOHS',
                            'WOHSC':'GTAWDEV_WOHS',
                            'WOHSE':'GTAWDEV_WOHS',
                            'WOHSR':'GTAWDEV_WOHS',
                            'WPHC':'GTAWDEV_HRRH',
                            'HRRH':'GTAWDEV_HRRH' ]

// Set Receiving Facility
if (virtualArchiveIDMap[sendingFacility] != null) {
    set('/.MSH-6', virtualArchiveIDMap[sendingFacility])
} else {
    set('/.MSH-6', 'UNKNOWN')
}

// Remove the &&ISO from the first PID along with all repeating fields in PID-3
def pid_iter = 0
while ((curr_pid = get('/.PID-3(' + pid_iter + ')-1')) != null ||
     (curr_uid = get('/.PID-3(' + pid_iter + ')-4-2')) != null) {
    if (pid_iter == 0) {
        set('/.PID-3(' + pid_iter + ')-4-1', get('/.PID-3(' + pid_iter + ')-4-2'))
    } else {
        set('/.PID-3(' + pid_iter + ')-1', null)
    }

    set('/.PID-3(' + pid_iter + ')-4-2', null)
    set('/.PID-3(' + pid_iter + ')-4-3', null)
    set('/.PID-3(' + pid_iter + ')-5', null)

    pid_iter++
}

// Remove the &&ISO from the first MRG PID along with all repeating fields in MRG-1
def messageType = get('/.MSH-9-1')
def triggerEvent = get('/.MSH-9-2')

if (!("ADT".equals(messageType) && "A40".equals(triggerEvent))) {
    return
}

def mrg_iter = 0
while ((curr_pid = get('/.MRG-1(' + mrg_iter + ')-1')) != null ||
     (curr_uid = get('/.MRG-1(' + mrg_iter + ')-4-2')) != null) {
    if (mrg_iter == 0) {
        set('/.MRG-1(' + mrg_iter + ')-4-1', get('/.MRG-1(' + mrg_iter + ')-4-2'))
    } else {
        set('/.MRG-1(' + mrg_iter + ')-1', null)
    }

    set('/.MRG-1(' + mrg_iter + ')-4-2', null)
    set('/.MRG-1(' + mrg_iter + ')-4-3', null)
    set('/.MRG-1(' + mrg_iter + ')-5', null)

    mrg_iter++
}
