// Morphing rules for the Imaging Archive

// Get Sending
def sendingFacility = get('/.MSH-4')

// Set Sending
set('/.MSH-3', 'EMPI')
set('/.MSH-4', 'EMPI')

// Set Receiving Application
set('/.MSH-5', 'GE_ENTARCH')

// Set Virtual Archive
def virtualArchiveIDMap = [ 'BGHS':'GTAWSYS_BGHS',
                            'CGMH':'GTAWSYS_CGMH',
                            'CVH':'GTAWSYS_CVH',
                            'SICKKIDS':'GTAWSYS_HSC',
                            'HBKR':'GTAWSYS_HSC',
                            'HHCC':'GTAWSYS_HHCC',
                            'HHS':'GTAWSYS_HHS',
                            'HHSO':'GTAWSYS_HHS',
                            'HHSM':'GTAWSYS_HHS',
                            'HHSG':'GTAWSYS_HHS',
                            'MAHC':'GTAWSYS_OSMH',
                            'GBGH':'GTAWSYS_OSMH',
                            'OSMH':'GTAWSYS_OSMH',
                            'RVH':'GTAWSYS_RVH',
                            'SJHC':'GTAWSYS_SJHC',
                            'WOHS':'GTAWSYS_WOHS',
                            'THC':'GTAWSYS_THC',
                            'THCM':'GTAWSYS_THC',
                            'THCT':'GTAWSYS_THC',
                            'CAMH':'GTAWSYS_UHN',
                            'TRI':'GTAWSYS_UHN',
                            'UHN':'GTAWSYS_UHN',
                            'WCH':'GTAWSYS_UHN',
                            'MSH':'GTAWSYS_UHN',
                            'WOHS':'GTAWSYS_WOHS',
                            'WOHSC':'GTAWSYS_WOHS',
                            'WOHSE':'GTAWSYS_WOHS',
                            'WOHSR':'GTAWSYS_WOHS',
                            'WPHC':'GTAWSYS_HRRH',
                            'HRRH':'GTAWSYS_HRRH' ]

// Set Receiving Facility
if (virtualArchiveIDMap[sendingFacility] != null) {
    set('/.MSH-6', virtualArchiveIDMap[sendingFacility])
} else {
    set('/.MSH-6', 'UNKNOWN')
}

// Remove the &&ISO from the first PID along with all repeating fields in PID-3
def pid_iter = 0
while ((curr_pid = get('/.PID-3(' + pid_iter + ')-1')) != null ||
     (curr_uid = get('/.PID-3(' + pid_iter + ')-4-2')) != null) {
    if (pid_iter == 0) {
        set('/.PID-3(' + pid_iter + ')-4-1', get('/.PID-3(' + pid_iter + ')-4-2'))
    } else {
        set('/.PID-3(' + pid_iter + ')-1', null)
    }

    set('/.PID-3(' + pid_iter + ')-4-2', null)
    set('/.PID-3(' + pid_iter + ')-4-3', null)
    set('/.PID-3(' + pid_iter + ')-5', null)

    pid_iter++
}

// Remove the &&ISO from the first MRG PID along with all repeating fields in MRG-1
def messageType = get('/.MSH-9-1')
def triggerEvent = get('/.MSH-9-2')

if (!("ADT".equals(messageType) && "A40".equals(triggerEvent))) {
    return
}

def mrg_iter = 0
while ((curr_pid = get('/.MRG-1(' + mrg_iter + ')-1')) != null ||
     (curr_uid = get('/.MRG-1(' + mrg_iter + ')-4-2')) != null) {
    if (mrg_iter == 0) {
        set('/.MRG-1(' + mrg_iter + ')-4-1', get('/.MRG-1(' + mrg_iter + ')-4-2'))
    } else {
        set('/.MRG-1(' + mrg_iter + ')-1', null)
    }

    set('/.MRG-1(' + mrg_iter + ')-4-2', null)
    set('/.MRG-1(' + mrg_iter + ')-4-3', null)
    set('/.MRG-1(' + mrg_iter + ')-5', null)

    mrg_iter++
}
