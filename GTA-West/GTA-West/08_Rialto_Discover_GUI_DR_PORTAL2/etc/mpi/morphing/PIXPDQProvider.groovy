/**
 *
 * Morphing rules for the PIX/PDQ Provider
 *
 */


/**
 * Function definitions
 */

// This function will set the exclusion flag in the message so 
// the patient will be excluded from the matching algorithm.
def setExclusion() {
    set('/.MSH-11-2', 'I');
    set('/.MSH-11-3', 'I');
}

// This is the function housing the main logic in determining if the
// patient should be excluded or not.
//
// This function will first look up if the patient feed is originating from
// an identity source that has an exclusion rule defined for it.  Then, it 
// will validate that the demographics correspond to a patient that should 
// be excluded before ultimately setting the exclusion flag on the message.
def exclude() {
    // Let's grab the pertinent information from the message that may qualify
    // this message as a positive exclusion
    def first = get('/.PID-5-2')
    def last = get('/.PID-5-1')
    def dob = get('/.PID-7-1')
    def gender = get('/.PID-8-1')
    def address = get('/.PID-11-1')
    def city = get('/.PID-11-3')
    def hcn = null

    // First, we'll obtain the health card number.  We assume that the health
    // card number for John Doe's will always be qualified with the Ontario
    // universal id.
    def hcd_iter = 0
    while ((curr_domain = get('/.PID-3(' + hcd_iter + ')-4-2')) != null) {
        if (curr_domain == '2.16.840.1.113883.4.59') {
            hcn = get('/.PID-3(' + hcd_iter + ')-1')
        }

        hcd_iter++
    }

    // Iterate through the PID-3 field to see if the patient is coming from
    // an identity source that has an exclusion rule defined for it.
    // By looping, there is no assumption about the order that the patient
    // ids occur in.
    def pid_iter = 0
    while ((curr_uid = get('/.PID-3(' + pid_iter + ')-4-2')) != null) {
        log.info('Universal id #' + pid_iter + ': ' + curr_uid)

        switch (curr_uid) {

            // Muskoka
            case '2.16.840.1.113883.3.239.16.4618':
                if (last != null && last.toUpperCase().startsWith('UNIDENTIFIED') &&
                (('HDMH'.equalsIgnoreCase(first) && 'HUNTSVILLE'.equalsIgnoreCase(city)) ||
                ('SMMH'.equalsIgnoreCase(first) && 'BRACEBRIDGE'.equalsIgnoreCase(city))) &&
                dob == '18000101' &&
                'U'.equalsIgnoreCase(gender) &&
                '0'.equals(hcn)) {
                    setExclusion()
                    return
                }
                break

            // Orillia
            case '2.16.840.1.113883.3.239.16.4108':
                if (hcn == null &&
                last != null && last.toUpperCase().startsWith('DOE') &&
                (('JOHN'.equalsIgnoreCase(first) && 'M'.equalsIgnoreCase(gender)) ||
                ('JANE'.equalsIgnoreCase(first) && 'F'.equalsIgnoreCase(gender)))) {
                    setExclusion()
                    return
                }
                break

            // West Park
            case '2.16.840.1.113883.3.239.16.1469':
                if ('N/A'.equalsIgnoreCase(last) &&
                'N/A'.equalsIgnoreCase(first) &&
                'N/A'.equalsIgnoreCase(address) &&
                'N/A'.equalsIgnoreCase(hcn)) {
                    setExclusion()
                    return
                }
                break

            // Georgian Bay
            case '2.16.840.1.113883.3.239.16.4241':
                if ('DOE'.equalsIgnoreCase(last) &&
                (('JOHN'.equalsIgnoreCase(first) && 'M'.equalsIgnoreCase(gender)) ||
                ('JANE'.equalsIgnoreCase(first) && 'F'.equalsIgnoreCase(gender))) &&
                'X'.equalsIgnoreCase(address) &&
                'N/V'.equalsIgnoreCase(hcn)) {
                    setExclusion()
                    return
                }
                break

            // RVH
            case '2.16.840.1.113883.3.239.16.3987':
                if (hcn == null &&
                'DOE'.equalsIgnoreCase(last) &&
                (('JOHN'.equalsIgnoreCase(first) && 'M'.equalsIgnoreCase(gender)) ||
                ('JANE'.equalsIgnoreCase(first) && 'F'.equalsIgnoreCase(gender))) &&
                'NFA'.equalsIgnoreCase(address) &&
                'BARRIE'.equalsIgnoreCase(city)) {
                    setExclusion()
                    return
                }
                break

            // Halton
            case '2.16.840.1.113883.3.239.16.4192':
                if (hcn == null &&
                'DOE'.equalsIgnoreCase(last) &&
                (('JOHN'.equalsIgnoreCase(first) && 'M'.equalsIgnoreCase(gender)) ||
                ('JANE'.equalsIgnoreCase(first) && 'F'.equalsIgnoreCase(gender))) &&
                '18500101'.equals(dob)) {
                    setExclusion()
                    return
                }
                break

            // Collingwood
            case '2.16.840.1.113883.3.239.16.4197':
                if (hcn == null &&
                ('JOHN'.equalsIgnoreCase(first) || 'JANE'.equalsIgnoreCase(first)) &&
                dob == null &&
                'UNK'.equalsIgnoreCase(address) &&
                'UNK'.equalsIgnoreCase(city)) {
                    setExclusion()
                    return
                }
                break

            // St. Joe's
            case '2.16.840.1.113883.3.239.16.4056':
                if ('DOE'.equalsIgnoreCase(last) &&
                (('JOHN'.equalsIgnoreCase(first) && 'M'.equalsIgnoreCase(gender)) ||
                ('JANE'.equalsIgnoreCase(first) && 'F'.equalsIgnoreCase(gender))) &&
                '18900101'.equals(dob) &&
                'NFA'.equalsIgnoreCase(address) &&
                '1'.equals(hcn)) {
                    setExclusion()
                    return
                }
                break
        }

        pid_iter++
    }
}

/**
 * Entry point into message morphing
 */

// We need to specify 'QARD' as the pdqry algorithm so the EMPI will only return one 
// PID segment
def messageType = get('/.MSH-9-1')
def triggerEvent = get('/.MSH-9-2')

if ("QBP".equals(messageType) && "Q22".equals(triggerEvent)) {
    def currAlgorithm = get('/.QPD-5')
     if (currAlgorithm == null || "".equals(currAlgorithm)) {
        set('/.QPD-5', "QARD")
    }
}

// Only for ADT feeds, check to see if the patient should be excluded from the matching
// algorithm.
if (messageType.startsWith("ADT")) {
    exclude()
}
