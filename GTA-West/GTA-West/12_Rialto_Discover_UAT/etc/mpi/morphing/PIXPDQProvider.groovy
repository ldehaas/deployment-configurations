// Morphing rules for the TPIX/PDQ Provider

// this script is meant to be an example
// please revise before using this in production

// full documentation
// https://sites.google.com/a/karoshealth.com/products/Home/rialto/end-user-documentation/configuration/services/hl7-proxy#TOC-Tag-Morphing

// We need to specify 'QARD' as the pdqry algorithm so SpiritEHR will only return one PID segment
def messageType = get('/.MSH-9-1')
def triggerEvent = get('/.MSH-9-2')

if ("QBP".equals(messageType) && "Q22".equals(triggerEvent)) {
    def currAlgorithm = get('/.QPD-5')
    if (currAlgorithm == null || "".equals(currAlgorithm)) {
        set('/.QPD-5', "PIX")
    }
}

def setExclusion() {
    set('/.MSH-11-2', 'I');
    set('/.MSH-11-3', 'I');
}

def exclude() {
    // exact name match
    if ('IGNORE_MATCHING' == get('/.PID-5-1')) {
        setExclusion();
        return;
    }

    // all 'Michael' patients from 2.16.840.1.113883.3.239.16.4110
    if (get('/.PID-5-2') != null && !"".equals(get('/.PID-5-2')) && \
        'JOHN' == get('/.PID-5-2').toUpperCase() && \
        get('/.PID-5-1') != null && !"".equals(get('/.PID-5-1')) && \
        'DOE' == get('/.PID-5-1').toUpperCase() && \
        '2.16.840.1.113883.3.239.16.4110' == get('/.PID-3-4-2')) {
        setExclusion();
        return;
    }
}

if (messageType.startsWith("ADT")) {
    exclude()
}

// Handle merges of patients that have the same MRN in both
// the PID segment and MRG segment

if ("ADT".equals(messageType) && "A40".equals(triggerEvent)) {
    def subsumedPat = get('/.MRG-1-1')
    def survivingPat = get('/.PID-3-1')
    def subsumedDom = get('/.MRG-1-4-2')
    def survivingDom = get('/.PID-3-4-2')
    if (subsumedPat == null || survivingPat == null || subsumedDom == null || survivingDom == null) {
        return
    } else if (survivingPat.equals(subsumedPat) && survivingDom.equals(subsumedDom)) {
        set('/.MRG-1-1', "IGNOREME" + subsumedPat + "IGNOREME")
    } else {
        return
    }
}

