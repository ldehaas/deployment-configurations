// Morphing rules for the Imaging Archive

// Get Sending
def sendingFacility = get('/.MSH-4')

// Set Sending
set('/.MSH-3', 'EMPI')
set('/.MSH-4', 'EMPI')

// Set Receiving Application
set('/.MSH-5', 'GE_ENTARCH')

// Set Virtual Archive
def virtualArchiveIDMap = [ 'BGHS':'GTAWUAT_BGHS',
                            'CGMH':'GTAWUAT_CGMH',
                            'CVH':'GTAWUAT_CVH',
                            'SICKKIDS':'GTAWUAT_HSC',
                            'HBKR':'GTAWUAT_HSC',
                            'HHCC':'GTAWUAT_HHCC',
                            'HHS':'GTAWUAT_HHS',
                            'HHSO':'GTAWUAT_HHS',
                            'HHSM':'GTAWUAT_HHS',
                            'HHSG':'GTAWUAT_HHS',
			    'HHSU':'GTAWUAT_HHS',
                            'MAHC':'GTAWUAT_OSMH',
                            'GBGH':'GTAWUAT_OSMH',
                            'OSMH':'GTAWUAT_OSMH',
                            'RVH':'GTAWUAT_RVH',
                            'SJHC':'GTAWUAT_SJHC',
                            'WOHS':'GTAWUAT_WOHS',
                            'THC':'GTAWUAT_THC',
                            'THCM':'GTAWUAT_THC',
                            'THCT':'GTAWUAT_THC',
                            'CAMH':'GTAWUAT_UHN',
                            'TRI':'GTAWUAT_UHN',
                            'UHN':'GTAWUAT_UHN',
                            'WCH':'GTAWUAT_UHN',
                            'MSH':'GTAWUAT_UHN',
                            'WOHS':'GTAWUAT_WOHS',
                            'WOHSC':'GTAWUAT_WOHS',
                            'WOHSE':'GTAWUAT_WOHS',
                            'WOHSR':'GTAWUAT_WOHS',
                            'WPHC':'GTAWUAT_HRRH',
                            'HRRH':'GTAWUAT_HRRH' ]

// Set Receiving Facility
if (virtualArchiveIDMap[sendingFacility] != null) {
    set('/.MSH-6', virtualArchiveIDMap[sendingFacility])
} else {
    set('/.MSH-6', 'UNKNOWN')
}

// Remove the &&ISO from the first PID along with all repeating fields in PID-3
def pid_iter = 0
while ((curr_pid = get('/.PID-3(' + pid_iter + ')-1')) != null ||
     (curr_uid = get('/.PID-3(' + pid_iter + ')-4-2')) != null) {
    if (pid_iter == 0) {
        set('/.PID-3(' + pid_iter + ')-4-1', get('/.PID-3(' + pid_iter + ')-4-2'))
    } else {
        set('/.PID-3(' + pid_iter + ')-1', null)
    }

    set('/.PID-3(' + pid_iter + ')-4-2', null)
    set('/.PID-3(' + pid_iter + ')-4-3', null)
    set('/.PID-3(' + pid_iter + ')-5', null)

    pid_iter++
}

// Remove the &&ISO from the first MRG PID along with all repeating fields in MRG-1
def messageType = get('/.MSH-9-1')
def triggerEvent = get('/.MSH-9-2')

if (!("ADT".equals(messageType) && "A40".equals(triggerEvent))) {
    return
}

def mrg_iter = 0
while ((curr_pid = get('/.MRG-1(' + mrg_iter + ')-1')) != null ||
     (curr_uid = get('/.MRG-1(' + mrg_iter + ')-4-2')) != null) {
    if (mrg_iter == 0) {
        set('/.MRG-1(' + mrg_iter + ')-4-1', get('/.MRG-1(' + mrg_iter + ')-4-2'))
    } else {
        set('/.MRG-1(' + mrg_iter + ')-1', null)
    }

    set('/.MRG-1(' + mrg_iter + ')-4-2', null)
    set('/.MRG-1(' + mrg_iter + ')-4-3', null)
    set('/.MRG-1(' + mrg_iter + ')-5', null)

    mrg_iter++
}
