// Morphing rules for the Imaging Archive

// Get Sending
def sendingFacility = get('/.MSH-4')

// Set Sending
set('/.MSH-3', 'EMPI')
set('/.MSH-4', 'EMPI')

// Set Receiving Application
set('/.MSH-5', 'GE_ENTARCH')

// Set Virtual Archive
def virtualArchiveIDMap = [ 'HSC':'GTAWDEV_HSC',
                            'WOHS':'GTAWDEV_WOHS',
                            'HHCC':'GTAWDEV_HHCC',
                            'CVH':'GTAWDEV_CVH',
                            'THC':'GTAWDEV_THC',
                            'HHS':'GTAWDEV_HHS',
                            'HRRH':'GTAWDEV_HRRH',
                            'OSMH':'GTAWDEV_OSMH',
                            'RVH':'GTAWDEV_RVH',
                            'SJHC':'GTAWDEV_SJHC',
                            'UHN':'GTAWDEV_UHN',
                            'BGHS':'GTAWDEV_BGHS',
                            'CGMH':'GTAWDEV_CGMH',
                            'WOHS':'GTAWDEV_WOHS',
			    'SICKKIDS':'GTAWDEV_SICKKIDS' ]

// Set Receiving Facility
if (virtualArchiveIDMap[sendingFacility] != null) {
    set('/.MSH-6', virtualArchiveIDMap[sendingFacility])
} else {
    set('/.MSH-6', 'UNKNOWN')
}

// Remove the &&ISO from PID-3
set('/.PID-3-4-1', get('/.PID-3-4-2'))
set('/.PID-3-4-2', '')
set('/.PID-3-4-3', '')
set('/.PID-3-5', '')
