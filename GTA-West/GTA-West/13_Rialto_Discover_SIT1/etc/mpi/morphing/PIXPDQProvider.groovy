// Morphing rules for the TPIX/PDQ Provider

// this script is meant to be an example
// please revise before using this in production

// full documentation
// https://sites.google.com/a/karoshealth.com/products/Home/rialto/end-user-documentation/configuration/services/hl7-proxy#TOC-Tag-Morphing

// We need to specify 'QARD' as the pdqry algorithm so SpiritEHR will only return one PID segment
def messageType = get('/.MSH-9-1')
def triggerEvent = get('/.MSH-9-2')

set('/.MSH-14', '')

if ("QBP".equals(messageType) && "Q22".equals(triggerEvent)) {
    set('/.MSH-9-3', 'QBP_Q21')
    set('/.QPD-1', 'IHE PDQ Query')
    def currAlgorithm = get('/.QPD-5')
    if (currAlgorithm == null || "".equals(currAlgorithm)) {
        set('/.QPD-5', "PIX")
    }
}

if (!messageType.startsWith("ADT")) {
    return;
}

def flag() {
    // uncomment below to really flag
    // set('/.MSH-11-1', 'I');
    // apparently Tiani SpiritEHR ignores the ADT message only if MSH-11-1 is 'I'
    // which is a violation of the specification (it should be MSH-11-2)
}

def mrn = get('/.PID-3-1')
// null mrn
if (mrn == null) {
    flag();
    return;
}

// all 0's
if (mrn ==~ /0+/) {
    flag();
    return;
}

// exact name match
if ( 'TARGET^NAME' == get('/.PID-5') ) {
    flag();
    return;
}


