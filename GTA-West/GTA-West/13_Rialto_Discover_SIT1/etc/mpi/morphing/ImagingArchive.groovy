// Morphing rules for the Imaging Archive

// Get Sending
def sendingFacility = get('/.MSH-4')

// Set Sending
set('/.MSH-3', 'EMPI')
set('/.MSH-4', 'EMPI')

// Set Receiving Application
set('/.MSH-5', 'GE_ENTARCH')

// Set Virtual Archive
def virtualArchiveIDMap = [ 'BGHS':'GTAWINT_BGHS',
                            'CGMH':'GTAWINT_CGMH',
                            'CVH':'GTAWINT_CVH',
                            'SICKKIDS':'GTAWINT_SICKKIDS',
                            'HBKR':'GTAWINT_SICKKIDS',
                            'HHCC':'GTAWINT_HHCC',
                            'HHSO':'GTAWINT_HHS',
                            'HHSM':'GTAWINT_HHS',
                            'HHSG':'GTAWINT_HHS',
                            'MAHC':'GTAWINT_OSMH',
                            'GBGH':'GTAWINT_OSMH',
                            'OSMH':'GTAWINT_OSMH',
                            'RVH':'GTAWINT_RVH',
                            'SJHC':'GTAWINT_SJHC',
                            'WOHS':'GTAWINT_WOHS',
                            'THCM':'GTAWINT_THC',
                            'THCT':'GTAWINT_THC',
                            'CAMH':'GTAWINT_UHN',
                            'TRI':'GTAWINT_UHN',
                            'UHN':'GTAWINT_UHN',
                            'WCH':'GTAWINT_UHN',
                            'MSH':'GTAWINT_UHN',
                            'WOHSC':'GTAWINT_WOHS',
                            'WOHSE':'GTAWINT_WOHS',
                            'WOHSR':'GTAWINT_WOHS',
                            'WPHC':'GTAWINT_HRRH',
                            'HRRH':'GTAWINT_HRRH' ]

// Set Receiving Facility
if (virtualArchiveIDMap[sendingFacility] != null) {
    set('/.MSH-6', virtualArchiveIDMap[sendingFacility])
} else {
    set('/.MSH-6', 'UNKNOWN')
}

// Remove the &&ISO from the first PID along with all repeating fields in PID-3
def pid_iter = 0
while ((curr_pid = get('/.PID-3(' + pid_iter + ')-1')) != null ||
     (curr_uid = get('/.PID-3(' + pid_iter + ')-4-2')) != null) {
    if (pid_iter == 0) {
        set('/.PID-3(' + pid_iter + ')-4-1', get('/.PID-3(' + pid_iter + ')-4-2'))
    } else {
        set('/.PID-3(' + pid_iter + ')-1', null)
    }

    set('/.PID-3(' + pid_iter + ')-4-2', null)
    set('/.PID-3(' + pid_iter + ')-4-3', null)
    set('/.PID-3(' + pid_iter + ')-5', null)

    pid_iter++
}

// Remove the &&ISO from the first MRG PID along with all repeating fields in MRG-1
def messageType = get('/.MSH-9-1')
def triggerEvent = get('/.MSH-9-2')

if (!("ADT".equals(messageType) && "A40".equals(triggerEvent))) {
    return
}

def mrg_iter = 0
while ((curr_pid = get('/.MRG-1(' + mrg_iter + ')-1')) != null ||
     (curr_uid = get('/.MRG-1(' + mrg_iter + ')-4-2')) != null) {
    if (mrg_iter == 0) {
        set('/.MRG-1(' + mrg_iter + ')-4-1', get('/.MRG-1(' + mrg_iter + ')-4-2'))
    } else {
        set('/.MRG-1(' + mrg_iter + ')-1', null)
    }

    set('/.MRG-1(' + mrg_iter + ')-4-2', null)
    set('/.MRG-1(' + mrg_iter + ')-4-3', null)
    set('/.MRG-1(' + mrg_iter + ')-5', null)

    mrg_iter++
}
