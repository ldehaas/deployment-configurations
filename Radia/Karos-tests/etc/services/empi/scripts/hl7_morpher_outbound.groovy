import ca.uhn.hl7v2.model.Message;
import ca.uhn.hl7v2.model.Type;
import ca.uhn.hl7v2.util.Terser;

log.debug("Output EMPI HL7 morpher start")

def messageType = get('MSH-9-1')
def triggerEvent = get('MSH-9-2')
log.debug("EMPI HL7 message type = {}", messageType)
log.debug("EMPI HL7 trigger event = {}", triggerEvent)


// We expect this to run against PIX queries by affinity PID. 
// Other types of queries probably won't get morphed properly below, but we don't expect to see them.
// Affinty PID PIX queries will return two patient IDs:
// - One under the RADIAORIGINAL domain. This PID is actually an encode version of the original domain (not pid!) used in the DICOM Study
// - One under the RADIALOCAL domain. This PID is the actual local PID used, but under a single domain.
// We modify the PIX response to have just one PID. For this PID, we take the local PID from RADIALOCAL, and the assigning authority from RADIAORIGINAL.
// All of this is because Radia has many different local domains, that we don't know in advance, and our EMPI cannot register unconfigured domains right now.
// Because the DICOM study is registered under the original domain, we must return the original domain for the subsequent DICOM search to find the patient.
// See image archive PIF morpher for more details

if (messageType == 'RSP' && triggerEvent == "K23") {
    def rawMessage = output.getMessage()
    def terser = new Terser(rawMessage);

    Type[] pids = terser.getSegment("/.PID").getField(3);

    log.debug("Found {} patient IDs in the PIX response.", pids.length)
    log.debug("PIX Response before morphing: {}", output)    

    def localPidToUse = ""
    def localNamespaceIDToUse = ""
    def localUniversalEntityID = ""
    def localUniversalEntityIDType = ""

    for (int i = 0; i < pids.length; i++) {
        String pid = terser.get("/.PID-3(" + i + ")-1");
        String ns = terser.get("/.PID-3(" + i + ")-4-1");
        String aa = terser.get("/.PID-3(" + i + ")-4-2");
        String aatype = terser.get("/.PID-3(" + i + ")-4-3");

        if ("RADIAORIGINAL" == ns) {
            def parts = pid.split("__")

            if (parts.length != 3) {
                log.error("Found unexpected RADIAORIGINAL pid: {}", pid);
                log.error("Returning original PIX response unchanged. Studies for this patient will not be found")
                return true;
            }
            localNamespaceIDToUse = parts[0]
            localUniversalEntityID = parts[1]
            localUniversalEntityIDType = parts[2]

        } else if ("RADIALOCAL" == ns) {
            localPidToUse = pid
        }
    }

    // Now we've iterated through all the pids, we should have enough saved to set the original domain pid
    
    // Clear the PIDs that are there now
    for (int i = 0; i < pids.length; i++) {
	pids[i].clear()   
   }

    def stringVersion = terser.getSegment("/.PID").encode();
    log.debug("Raw String version {}", stringVersion)
    def replaced = stringVersion.replaceAll("~","")
    log.debug("Replaced String version {}", replaced)
    terser.getSegment("/.PID").parse(replaced)

    // Write the new PID
    set("PID-3-1", localPidToUse)
    set("PID-3-4-1", localNamespaceIDToUse)
    set("PID-3-4-2", localUniversalEntityID)
    set("PID-3-4-3", localUniversalEntityIDType)

    log.debug("PIX Response after morphing: {}", output)

} 
