server {
    listen       2525 ssl http2;
    server_name  localhost;

    include /etc/nginx/ssl/default.conf;

    add_header Strict-Transport-Security max-age=15768000;
    add_header X-Frame-Options DENY;
    add_header X-Content-Type-Options nosniff;

    # Enable gzipping of responses.
    gzip on;
    gzip_types text/plain application/xml application/json application/javascript application/x-javascript text/css;

    # Enable checking the existence of precompressed files.
    gzip_static on;

    location ~* ^/(rest|public) {
        proxy_pass http://localhost:2524;
        proxy_set_header Origin $http_origin;
    }

    location ^~ /docs/ {
        root /var/www/;
        index Default.htm;
        add_header X-Frame-Options SAMEORIGIN;
    }

    location ^~ /v2 {
        rewrite /v2/?$ /v2/en/ permanent;
        # Rewrite anything that looks like a page (no file extension) to /v2/{locale} for Angular
        rewrite /v2/([^/]*/)[^.]+$ /v2/$1 break;
        proxy_pass http://localhost:2527;
        proxy_redirect default;
    }

    location / {
        proxy_pass http://localhost:2526;
        proxy_redirect http:// https://;
        proxy_set_header Host $host:$server_port;
    }
}

