/*
 * fps_trigger_morpher.groovy
 *
 * Use this script to determine which messages will kick off the set of tasks
 * that results in the Connect service fetching prior studies.
 *
 * The Connect services expects that the incoming HL7 message fields will be 
 * populated in a certain way. Use this script to modify the incoming messages
 * as required, for example, to update any non-HL7-spec-compliant fields to 
 * compliant fields. If the incoming HL7 messages use any irregular fields, use
 * this script to set or correct any irregularities in the messages from the 
 * site before processing. An irregularity in the message means the message is
 * formatted correctly, but is storing a piece of data in an unexpected field.
 * An example could be data that the HL7 spec specifies should be stored in one
 * PID field, but is actually found in a different PID field in messages at the 
 * site.
 *
 * You can also use this script to filter out any cases where the Connect 
 * service should drop the message and not trigger a prefetch, based on the 
 * message content (for example, for a rogue patient ID). Any message that 
 * causes the script to return false is entirely ignored; the Connect service
 * does not start a prefetch workflow.
 *
 * You can access the fields of this message by using the "get()" method. For
 * example, to access the original PatientId found in the order:
 *
 * def pid = get("PID-3-1")
 */
 log.info("fps_trigger_morpher.groovy: Starting fps_trigger_morpher")

 def msgType = get("MSH-9-1")

 if (msgType != "ORM") {
 	log.info("fps_trigger_morpher.groovy: HL7 message is not an order, do not proceed with prefetching")
 }

 log.info("fps_trigger_morpher.groovy: Ending fps_trigger_morpher")