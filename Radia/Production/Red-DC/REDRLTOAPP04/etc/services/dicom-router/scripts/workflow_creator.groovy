LOAD('router_workflow_builder.groovy')

import com.karos.rialto.workflow.model.Task.TaskPriority;

def builder = new RouterWorkflowBuilder(
    'var/router', 
    getCalledAETitle(),
    getCallingAETitle(),
    get(StudyInstanceUID))

builder.setInboundMorpher("etc/services/dicom-router/scripts/in.groovy")

builder.addDestination(calledAe: getCalledAETitle())

return builder.build()
