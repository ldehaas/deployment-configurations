otherPidsSequence = get(OtherPatientIDsSequence);

ipidQualifierSequence = get(IssuerOfPatientIDQualifiersSequence);

if (ipidQualifierSequence != null) {
  ipidQualifierSequence.each {
    set(OtherPatientIDs,
      get(PatientID)+"&"+
      get(IssuerOfPatientID)+"&"+
      it.get(UniversalEntityID)+"&"+
      it.get(UniversalEntityIDType));
  }
} else {
  set(OtherPatientIDs, get(PatientID)+"^"+get(IssuerOfPatientID));
}

def calledAET = getCalledAETitle()
if (otherPidsSequence != null && calledAET != null && ("RIALTO_TEST".equalsIgnoreCase(calledAET))) {
  otherPidsSequence.each {
    if ("RADIAAFFINITY&EMPI_CROSS_AFFINITY_DOMAIN_ID&ISO".equalsIgnoreCase(it.get(IssuerOfPatientID))) {
      set(PatientID, it.get(PatientID));
      set(IssuerOfPatientID, "RADIAAFFINITY");
      set("IssuerOfPatientIDQualifiersSequence/UniversalEntityID", "EMPI_CROSS_AFFINITY_DOMAIN_ID", VR.LO)
    }
  }
}
