import org.joda.time.LocalDateTime;
import java.time.Duration;
import org.joda.time.DateTime;
import org.joda.time.format.ISODateTimeFormat;

log.info("ilm_policy.groovy START")

//def modalities = study.getModalities()

//log.info("Modalities: {}", modalities)

// NOTE: If the production site contains studies used by the
// health check service, these should never be removed from the system.
// e.g. Check if this is a Test study that is used by HealthCheck service.
// StudyInstanceUIDs below are examples only. Change to match production requirements.
if (study.getStudyInstanceUID() == '1.2.840.114089.1.0.1.3232275478.1532680704.3324.10' || study.getStudyInstanceUID() == '1.2.840.114089.1.0.1.3232275478.1532680704.3324.13' || study.getStudyInstanceUID() == '1.2.840.114089.1.0.1.3232275478.1532680704.3324.16' || study.getStudyInstanceUID() == '1.2.840.114089.1.0.1.3232275478.1532680704.3324.19' || study.getStudyInstanceUID() == '1.2.840.114089.1.0.1.3232275478.1532680704.3324.22' || study.getStudyInstanceUID() == '1.2.840.114089.1.0.1.3232275478.1532680704.3324.25') {
    log.info("ilm_policy.groovy -study with StudyInstanceUID {} is used by HealthCheck service, skipping...", study.getStudyInstanceUID());
    log.debug("ilm_policy.groovy -END - Done looking at study with StudyInstanceUID {}", study.getStudyInstanceUID());
    return;
}

log.debug("ilm_policy.groovy: Check the date of ingestion for study")
//loop and find the first ingestion event for a study
for (event in timeline.setl.events) {
        if (event.type == "IMG") {
                log.info("ilm_policy.groovy: Ingestion Event for study was at: " + event.timestamp)
                def hoursToKeep = 2160
                def eolDate = new org.joda.time.DateTime().minusHours(hoursToKeep)
                def eventTime = org.joda.time.format.ISODateTimeFormat.dateTime()
                eventTime = eventTime.parseDateTime(event.timestamp)
                log.debug ("eventTime and eolDate are = {}, {}", eventTime, eolDate)
                //if the time of first ingestion is older than the eolDate, flag the study for deletion
                if (eventTime < eolDate) {
                        log.debug ("Flagging study {} for deletion from archive as it is older than [{}] number of hours", study.getStudyInstanceUID(), hoursToKeep)
                        ops.scheduleStudyHardDelete();
                }
            break;

        }
}

//log.info("ILM policy for study '{}' - evaluating if older than 2 hours.", study.getStudyInstanceUID());

//twoHoursAgo = new DateTime().minusHours(2);

//if (study.isOlderThan(Duration.standardHours(2))) {
//if (twoHoursAgo.isAfter(study.getLastUpdateTime())) {
 //  log.info("This study '{}' is older than 2 hours. Hard delete requested", study.getStudyInstanceUID());
 //  ops.scheduleStudyHardDelete();
//} else {
//   log.info("This study '{}' is not older than 2 hours. Leaving in image archive", study.getStudyInstanceUID());
//}
//log.info("This study '{}' is flagged as a purged candidate.", study.getStudyInstanceUID());
//ops.flagAsPurgeCandidate();
//if (modality.contains("CT")) {
//    ops.scheduleStudyForward("DESTINATION_AE")
//}



log.info("ilm_policy.groovy END")
