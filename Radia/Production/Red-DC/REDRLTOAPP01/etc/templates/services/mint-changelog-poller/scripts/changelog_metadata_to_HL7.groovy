/*
 * This script constructs an HL7 order message from XML Study metadata.
 * The HL7 order is used to send to Rialto Connect in order to create 
 *
 * The HL7 message is initialized to be an order message at the start.
 * All study metadata is accessible in this script by using the 'input'
 * variable..
 *
 * For example, if the metadata of a study is returned as:
 * <study>
 *  <attributes>
 *   <attr tag="00400032" val="county.general">
 *   <attr tag="00100020" val="TEST123">
 *  </attributes>
 * </study>
 *
 * The way to access the a particular attribute would be:
 *  input.attribute.attr[1] //this would give you the second 'attr'
 *                          //element
 *
 */

import groovy.util.slurpersupport.GPathResult

log.debug("Started creation of HL7 message from MINT metadata document...")

/*
 * We must initialize the type of HL7 message we wish to send. For
 * Connect, we will want to initialize it to be an order message.
 */
 initialize("ORM", "O01", "2.5")

/*
 * Attribute nodes can be searched for particular values.
 * If we have attribute elements that resemble:
 * <attr tag="00100020" val="TEST123">
 * We can access the attributes of that element with the @
 * symbol. The below example searches through all 'attr' elements
 * to find a specific tag. If a value is not found, then it will
 * not be set.
 */
def issuerOfPatientID = input.attributes.attr.'**'.find { node->
    node.name() == 'attr' && node.@tag == "00400032"
}

def patientID = input.attributes.attr.'**'.find { node->
    node.name() == 'attr' && node.@tag == "00100020"
}


/*
 * Here we set the fields of our HL7 message using the values we 
 * retrieved above.
 */
set('PID-3-1', patientID.@val.toString())            // Patient ID
set('PID-3-4-2', issuerOfPatientID.@val.toString() ) // Patient ID Issuer Domain
set('PID-3-4-3', 'ISO')                         // Patient ID Issuer Domain Type

set('ORC-1','NW')

log.debug("... finished creating HL7 message.")

return true
