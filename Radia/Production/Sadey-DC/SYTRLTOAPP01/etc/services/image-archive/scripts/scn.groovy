import org.joda.time.DateTime
import org.joda.time.format.DateTimeFormat
import java.text.SimpleDateFormat

initialize( 'ORM', 'O01', '2.3' );
output.getMessage().addNonstandardSegment('IPC')
log.debug("SCN input is {}", input);
log.debug("Calling AE Title is {}", getCallingAETitle());
//
sdf = new SimpleDateFormat("yyyyMMddHHmmss")
set('MSH-7', sdf.format(new Date()))
//

def patientsId = input.get(0x00100020);
log.debug("scn.groovy: DEBUG: PatientsId: {}", patientsId);
set('PID-3-1', patientsId)

def patientsName = input.get(0x00100010);
log.debug("scn.groovy: DEBUG: PatientsName: {}", patientsName);
/*if (patientsName != null) {
    def patientsNameParts = patientsName.split("\\^")
    for (int i = 1; i <= patientsNameParts.size(); i++) {
        set("PID-5-${i}", patientsNameParts[i-1])
    }
}*/
if (patientsName != null) {
    if (patientsName.contains("^")) {
        log.debug("scn.groovy splitting on ^");
        def patientsNameParts = patientsName.split("\\^")
        for (int i = 1; i <= patientsNameParts.size(); i++) {
            log.debug("Patient name: Part {} has name {}", i, patientsNameParts[i-1])
            set("PID-5-${i}", patientsNameParts[i-1])
        }
    } else if (patientsName.contains(",")) {
        log.debug("scn.groovy splitting on ,");
        def patientsNameParts = patientsName.split(",")
        for (int i = 1; i <= patientsNameParts.size(); i++) {
            log.debug("Patient name: Part {} has name {}", i, patientsNameParts[i-1])
            set("PID-5-${i}", patientsNameParts[i-1])
        }
    } else {
        log.debug("scn.groovy splitting on space by default (no , or ^)");
        def patientsNameParts = patientsName.split(' ')
        for (int i = 1; i <= patientsNameParts.size(); i++) {
            log.debug("Patient name: Part {} has name {}", i, patientsNameParts[i-1])
            set("PID-5-${i}", patientsNameParts[i-1])
        }

    }
} 


def patientsBirthDate = input.get(0x00100030);
log.debug("scn.groovy: DEBUG: PatientsBirthDate: {}", patientsBirthDate);
set('PID-7', patientsBirthDate)

def patientsSex = input.get(0x00100040);
log.debug("scn.groovy: DEBUG: PatientsSex: {}", patientsSex);
set('PID-8', patientsSex)

if (type != null) {
    log.debug("SCN message type = {}", type);
    set('ORC-5', type)
}

set('ORC-1', 'REDRLTOAPP01')

def accessionNumber = input.get(0x00080050);
log.debug("scn.groovy: DEBUG: AccessionNumber: {}", accessionNumber);
set('OBR-3', accessionNumber)
set('OBR-4-2', input.get(StudyDescription))

def studyInstanceUID = input.get(0x0020000D);
log.debug("scn.groovy: DEBUG: StudyInstanceUID: {}", studyInstanceUID);
set('OBX-9', studyInstanceUID)

def studyDate = input.get(0x00080020);
log.debug("scn.groovy: DEBUG: StudyDate: {}", studyDate);
set('ZRI-1', studyDate);

def studyTime = input.get(0x00080030);
log.debug("scn.groovy: DEBUG: StudyTime: {}", studyTime);
set('ZRI-2', studyTime);

def seriesTime = input.get(0x00080031);
log.debug("scn.groovy: DEBUG: SeriesTime: {}", seriesTime);
set('ZRI-3', seriesTime);

def modality = input.get(0x00080060);
log.debug("scn.groovy: DEBUG: Modality: {}", modality);
set('ZRI-4', modality);

def modalitiesInStudy = input.get(0x00080061);
log.debug("scn.groovy: DEBUG: ModalitiesInStudy: {}", modalitiesInStudy);
set('ZRI-5', modalitiesInStudy);

def institutionName = input.get(0x00080080);
log.debug("scn.groovy: DEBUG: InstitutionName: {}", institutionName);
set('ZRI-6', institutionName);

def referringPhysiciansName = input.get(0x00080090);
log.debug("scn.groovy: DEBUG: ReferringPhysiciansName: {}", referringPhysiciansName);
/*if (referringPhysiciansName != null) {
    def referringPhysiciansNameParts = referringPhysiciansName.split("\\^")
    for (int i = 1; i <= referringPhysiciansNameParts.size(); i++) {
        set("ZRI-7-${i}", referringPhysiciansNameParts[i-1])
    }
}*/
if (referringPhysiciansName != null ) {
    if (referringPhysiciansName.contains("^")) {
        log.debug("scn.groovy splitting on ^");
        def referringPhysiciansNameParts = referringPhysiciansName.split("\\^")
        for (int i = 1; i <= referringPhysiciansNameParts.size(); i++) {
        	log.debug("referringPhysiciansName: Part {} has name {}", i, referringPhysiciansNameParts[i-1])
            set("ZRI-7-${i}", referringPhysiciansNameParts[i-1])
        }
    } else if (referringPhysiciansName.contains(",")) {
        log.debug("scn.groovy splitting on ,");
        def referringPhysiciansNameParts = referringPhysiciansName.split(",")
        for (int i = 1; i <= referringPhysiciansNameParts.size(); i++) {
        	log.debug("referringPhysiciansName: Part {} has name {}", i, referringPhysiciansNameParts[i-1])
            set("ZRI-7-${i}", referringPhysiciansNameParts[i-1])
        }
    } else {
        log.debug("scn.groovy splitting on space by default (no , or ^)");
        def referringPhysiciansNameParts = referringPhysiciansName.split(' ')
        for (int i = 1; i <= referringPhysiciansNameParts.size(); i++) {
        	log.debug("referringPhysiciansName: Part {} has name {}", i, referringPhysiciansNameParts[i-1])
            set("ZRI-7-${i}", referringPhysiciansNameParts[i-1])
        }
    }
}

def referringPhysiciansAddress = input.get(0x00080092);
log.debug("scn.groovy: DEBUG: ReferringPhysiciansAddress: {}", referringPhysiciansAddress);
set('ZRI-8', referringPhysiciansAddress);

def referringPhysiciansTelephoneNumbers = input.get(0x00080094);
log.debug("scn.groovy: DEBUG: ReferringPhysiciansTelephoneNumbers: {}", referringPhysiciansTelephoneNumbers);
set('ZRI-9', referringPhysiciansTelephoneNumbers);

def stationName = input.get(0x00081010);
log.debug("scn.groovy: DEBUG: StationName: {}", stationName);
set('ZRI-10', stationName);

def studyDescription = input.get(0x00081030);
log.debug("scn.groovy: DEBUG: StudyDescription: {}", modalitiesInStudy);
set('ZRI-11', studyDescription);

def performingPhysiciansName = input.get(0x00081050);
log.debug("scn.groovy: DEBUG: PerformingPhysiciansName: {}", performingPhysiciansName);
/*if (performingPhysiciansName != null) {
    def performingPhysiciansNameParts = performingPhysiciansName.split("\\^")
    for (int i = 1; i <= performingPhysiciansNameParts.size(); i++) {
        set("ZRI-12-${i}", performingPhysiciansNameParts[i-1])
    }
}*/
if (performingPhysiciansName != null) {
    if (performingPhysiciansName.contains("^")) {
        log.debug("scn.groovy splitting on ^");
        def performingPhysiciansNameParts = performingPhysiciansName.split("\\^")
        for (int i = 1; i <= performingPhysiciansNameParts.size(); i++) {
        	log.debug("performingPhysiciansName: Part {} has name {}", i, performingPhysiciansNameParts[i-1])
            set("ZRI-12-${i}", performingPhysiciansNameParts[i-1])
        }
    } else if (performingPhysiciansName.contains(",")) {
        log.debug("scn.groovy splitting on ,");
        def performingPhysiciansNameParts = performingPhysiciansName.split(",")
        for (int i = 1; i <= performingPhysiciansNameParts.size(); i++) {
        	log.debug("performingPhysiciansName: Part {} has name {}", i, performingPhysiciansNameParts[i-1])
            set("ZRI-12-${i}", performingPhysiciansNameParts[i-1])
        }
    } else {
        log.debug("scn.groovy splitting on space by default (no , or ^)");
        def performingPhysiciansNameParts = performingPhysiciansName.split(' ')
        for (int i = 1; i <= performingPhysiciansNameParts.size(); i++) {
        	log.debug("performingPhysiciansName: Part {} has name {}", i, performingPhysiciansNameParts[i-1])
            set("ZRI-12-${i}", performingPhysiciansNameParts[i-1])
        }
    }
}

def nameOfPhysicianReadingStudy = input.get(0x00081060);
log.debug("scn.groovy: DEBUG: NameOfPhysicianReadingStudy: {}", nameOfPhysicianReadingStudy);
/*if (nameOfPhysicianReadingStudy != null) {
    def nameOfPhysicianReadingStudyParts = nameOfPhysicianReadingStudy.split("\\^")
    for (int i = 1; i <= nameOfPhysicianReadingStudyParts.size(); i++) {
        set("ZRI-13-${i}", nameOfPhysicianReadingStudyParts[i-1])
    }
}*/
if (nameOfPhysicianReadingStudy != null) {
    if (nameOfPhysicianReadingStudy.contains("^")) {
        log.debug("scn.groovy splitting on ^");
        def nameOfPhysicianReadingStudyParts = nameOfPhysicianReadingStudy.split("\\^")
        for (int i = 1; i <= nameOfPhysicianReadingStudyParts.size(); i++) {
        	log.debug("nameOfPhysicianReadingStudy: Part {} has name {}", i, nameOfPhysicianReadingStudyParts[i-1])
            set("ZRI-13-${i}", nameOfPhysicianReadingStudyParts[i-1])
        }
    } else if (nameOfPhysicianReadingStudy.contains(",")) {
        log.debug("scn.groovy splitting on ,");
        def nameOfPhysicianReadingStudyParts = nameOfPhysicianReadingStudy.split(",")
        for (int i = 1; i <= nameOfPhysicianReadingStudyParts.size(); i++) {
        	log.debug("nameOfPhysicianReadingStudy: Part {} has name {}", i, nameOfPhysicianReadingStudyParts[i-1])
            set("ZRI-13-${i}", nameOfPhysicianReadingStudyParts[i-1])
        }
    } else {
        log.debug("scn.groovy splitting on space by default (no , or ^)");
        def nameOfPhysicianReadingStudyParts = nameOfPhysicianReadingStudy.split(' ')
        for (int i = 1; i <= nameOfPhysicianReadingStudyParts.size(); i++) {
        	log.debug("nameOfPhysicianReadingStudy: Part {} has name {}", i, nameOfPhysicianReadingStudyParts[i-1])
            set("ZRI-13-${i}", nameOfPhysicianReadingStudyParts[i-1])
        }
    }
}

def operatorsName = input.get(0x00081070);
log.debug("scn.groovy: DEBUG: OperatorsName: {}", operatorsName);
set('ZRI-14', operatorsName);

def patientsAge = input.get(0x00101010);
log.debug("scn.groovy: DEBUG: PatientsAge: {}", patientsAge);
set('ZRI-15', patientsAge);

def patientsWeight = input.get(0x00101030);
log.debug("scn.groovy: DEBUG: PatientsWeight: {}", patientsWeight);
set('ZRI-16', patientsWeight);

def otherPatientIds = input.get(0x00101000);
log.debug("scn.groovy: DEBUG: OtherPatientIds: {}", otherPatientIds);
set('ZRI-17', otherPatientIds);

def otherPatientNames = input.get(0x00101001);
log.debug("scn.groovy: DEBUG: OtherPatientNames: {}", otherPatientNames);
if (otherPatientNames != null) {
    if (otherPatientNames.contains("^")) {
        log.debug("scn.groovy splitting on ^");
        def otherPatientNamesParts = otherPatientNames.split("\\^")
        for (int i = 1; i <= otherPatientNamesParts.size(); i++) {
        	log.debug("otherPatientNames: Part {} has name {}", i, otherPatientNamesParts[i-1])
            set("ZRI-18-${i}", otherPatientNamesParts[i-1])
        }
    } else if (otherPatientNames.contains(",")) {
        log.debug("scn.groovy splitting on ,");
        def otherPatientNamesParts = otherPatientNames.split(",")
        for (int i = 1; i <= otherPatientNamesParts.size(); i++) {
        	log.debug("otherPatientNames: Part {} has name {}", i, otherPatientNamesParts[i-1])
            set("ZRI-18-${i}", otherPatientNamesParts[i-1])
        }
    } else {
        log.debug("scn.groovy splitting on space by default (no , or ^)");
        def otherPatientNamesParts = otherPatientNames.split(' ')
        for (int i = 1; i <= otherPatientNamesParts.size(); i++) {
        	log.debug("otherPatientNames: Part {} has name {}", i, otherPatientNamesParts[i-1])
            set("ZRI-18-${i}", otherPatientNamesParts[i-1])
        }
    }
}

def smokingStatus = input.get(0x001021A0);
log.debug("scn.groovy: DEBUG: SmokingStatus: {}", smokingStatus);
set('ZRI-19', smokingStatus);

def additionalPatientsHistory = input.get(0x001021B0);
log.debug("scn.groovy: DEBUG: AdditionalPatientsHistory: {}", additionalPatientsHistory);
set('ZRI-20', additionalPatientsHistory);

def pregnancyStatus = input.get(0x001021C0);
log.debug("scn.groovy: DEBUG: PregnancyStatus: {}", pregnancyStatus);
set('ZRI-21', pregnancyStatus);

def contrastBolusAgent = input.get(0x00180010);
log.debug("scn.groovy: DEBUG: ContrastBolusAgent: {}", contrastBolusAgent);
set('ZRI-22', contrastBolusAgent);

def contrastBolusVolume = input.get(0x00181041);
log.debug("scn.groovy: DEBUG: ContrastBolusVolume: {}", contrastBolusVolume);
set('ZRI-23', contrastBolusVolume);

def contrastBolusStartTime = input.get(0x00181042);
log.debug("scn.groovy: DEBUG: ContrastBolusStartTime: {}", contrastBolusStartTime);
set('ZRI-24', contrastBolusStartTime);

def contrastBolusStopTime = input.get(0x00181043);
log.debug("scn.groovy: DEBUG: ContrastBolusStopTime: {}", contrastBolusStopTime);
set('ZRI-25', contrastBolusStopTime);

def contrastBolusTotalDose = input.get(0x00181044);
log.debug("scn.groovy: DEBUG: ContrastBolusTotalDose: {}", contrastBolusTotalDose);
set('ZRI-26', contrastBolusTotalDose);

def contrastFlowRates = input.get(0x00181046);
log.debug("scn.groovy: DEBUG: ContrastFlowRates: {}", contrastFlowRates);
set('ZRI-27', contrastFlowRates);

def contrastDurations = input.get(0x00181047);
log.debug("scn.groovy: DEBUG: ContrastDurations: {}", contrastDurations);
set('ZRI-28', contrastDurations);

def contrastBolusIngredient = input.get(0x00181048);
log.debug("scn.groovy: DEBUG: ContrastBolusIngredient: {}", contrastBolusIngredient);
set('ZRI-29', contrastBolusIngredient);

def contrastBolusIngredientConcentration = input.get(0x00181049);
log.debug("scn.groovy: DEBUG: ContrastBolusIngredientConcentration: {}", contrastBolusIngredientConcentration);
set('ZRI-30', contrastBolusIngredientConcentration);

def currentPatientLocation = input.get(0x00380300);
log.debug("scn.groovy: DEBUG: CurrentPatientLocation: {}", currentPatientLocation);
set('ZRI-31', currentPatientLocation);
