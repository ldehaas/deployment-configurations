<?xml version="1.0"?>
<!--
 The following configuration file is used to define the mint-changelog-poller
 service that is used to query the MINT changelog for recent CREATE 
 operations and the destination device for where relev
 that is used for the fetching of relevent priors, the HL7 listener that 
 Connect will use to receive HL7 messages, and the DICOM AE's we will
 query for relevent priors.
-->
<config>


    <!--
     Define the host and port of the HL7 device that
     will be used to receive orders from the MINT
     changelog poller, if enabled.  By default, we
     use the same host and port as the service
     that Rialto listens on for HL7 orders, which
     is defined immediately below as a server.
     -->
    <device type="hl7v2" id="PrefetchDestinationDevice">
        <host>localhost</host>
        <port>2395</port>
    </device>

    <!--
     Specify the behaviour of the polling service. Do not change
     the type of this service. This service polls for a MINT
     changelog at the specified destination, and then fetches
     the study metadata for each study CREATE operation that
     is returned in the changelog.
    -->

    <service id="poller" type="mint-changelog-poller"> 

        <!--
         Define what device the generated HL7 messages will be sent
         to. The 'idref' must match the source device that was defined above.
         Do not change the name.
         -->
        <device idref="PrefetchDestinationDevice" 
            name="PrefetchDestinationDevice"/>
        <config>
            
            <!--
             This property is required.
             This morpher is used to form the HL7 order message from the
             retrieved MINT metadata that is retrieved from the server. All
             study metadata returned from a query may be accessed in the 
             morpher to form a relevant HL7 message to be sent.
             -->
            <prop name="MetadataToPrefetchOrderMorpher">
                ${rialto.rootdir}/etc/services/mint-changelog-poller/scripts/changelog_metadata_to_HL7.groovy
            </prop>

            <!--
             This property is required.
             This is the endpoint for the changelog that the
             mint-changelog-poller service will query. If this value is
             malformed, the poller service will shutdown and not run. This
             property is required in order for the polling service to start.
            -->
            <prop name="MINTChangelogURL">
                http://localhost:8080/vault/mint/changelog
            </prop>

            <!--
             This property is required.
             This is the endpoint for the study metadata that the
             mint-changelog-poller service will query for based on the retrieved
             CREATE operation SSUID's that are retrieved. The service assumes
             that the TYPE of metadata will be DICOM XML. If this value is
             malformed, the poller service will shutdown and not run. This
             property is required in order for the polling service to start.
            -->
            <prop name="MINTMetadataURL">
                http://localhost:8080/vault/mint/studies
            </prop>

            <!--
             This property is required.
             This is the location of the file that will persist the change time 
             each time an order is generated in the poller service. The name of
             the file in this location is 'changelogSinceStorage' (no file type
             appended). The contents of this file are a single string that 
             matches the date format "yyyyMMdd'T'HHmmss". This represents the 
             structure of the file contents. It cannot be changed with 
             configuration. These values represent:

              yyyy = 4 digit year eg. 2016
              MM   = 2 digit month eg. 03
              dd   = 2 digit day eg. 02
              'T'  = the letter 'T' (the quotes indicate that this value does 
                                    not represent a unit of time or time 
                                    delimeter eg. HH:MM:SS)
              HH   = 2 digit hour eg. 06
              mm   = 2 digit minute eg. 02
              ss   = 2 digit second    

             If the 'changelogSinceStorage' file exists with input, it will 
             fetch all changelog entries since this date. If the file exists or 
             is empty, the current time is used. The 'changelogSinceStorage'
             files contents will be overwritten and updated each time an order 
             is successfully sent.

             For example, if in the 'changelogSinceStorage' file I have a string
             of '20150302T064532' then my first query to the changelog will
             retrieve all entries since March 02, 2015 6:45:32 AM.
            -->
            <prop name="ChangelogSincePath">
                ${rialto.rootdir}/var/connect/poller
            </prop>      

            <!--
             This property is optional.
             How often the poller service will query the changelog for CREATE 
             operations to process. If this value is not set, it will default to
             '60s'.
             The value is in the form of "[number][time unit]" where [number] 
             can be any positive integer and [time unit] can be one of "h", 
             "m" or "s" where those values represent hours, minutes and seconds 
             respectively. For example, "5s" represents 5 seconds, "5m" 
             represents 5 minutes and "5h" represents 5 hours.                
            -->      
            <prop name="PollInterval">60s</prop>
            
        </config>

    </service>
</config>