<?xml version="1.0"?>
<!--
 The following configuration file is used to define the Connect service
 that is used for the fetching of relevant priors, the HL7 listener that 
 Connect will use to receive HL7 messages, and the DICOM AE's we will
 query for relevant priors.
-->
<config>
    <!--
     Define the port of the DICOM receiver that
     will be used to receive adhoc C-STORE requests.
     -->
    <server type="dicom" id="dicom-connect">
        <port>4109</port>
    </server>

    <!--
     Define the port of the HL7 receiver that
     will be used to receive orders and trigger
     prefetching.
     -->
    <server type="hl7v2" id="hl7-connect">
        <port>2395</port>
    </server>

    <!--
     Specify the behaviour of the connect service. Do not change
     the type of this service. This service will trigger 
     prefetching of relevant priors by doing the following:
     1. Receive an HL7 order.
     2. Using the <morpher> configured inside of the <trigger> tag
        within the <prop name="FetchPriors">, check the HL7 message
        type and decide whether or not to initiate prefetching. If
        intiiating prefetching of relevant priors, normalize the HL7
        message (if required).
     2. Perform a PIX query to retrieve other patient identifiers.
     3. Extract details from the HL7 message to create a DICOM
        C-FIND using the <prop name="FetchPriors">/<hl7ToCFindMorpher> 
        tag.
     4. Issue the C-FIND to each DICOM AE inside of the 
        <prop name="Sources"> tag.
         a) You may optionally filter the list of C-FIND responses
            from a DICOM AE by configuring the 
            <cfindResponseMorpher> in each category.
     5. After all C-FIND responses have been received, filter the 
        list of studies using the <cfindResponseFilter> morpher
        script.
     6. For each study remaining, Connect will issue a C-MOVE 
        request to the source AE of each prior study, setting
        the <destinationAE> AE title as the move destination
        for the study.

     The service can also be used to convert a DICOM SR to HL7 ORU and forward
     the message to the move
    -->
    <service type="connect" id="connect">
        <server idref="hl7-connect" name="fps" />
        <server idref="dicom-connect" name="cstore" />


        <!--
         The Connect service can also accept incoming DICOM C-FIND and C-MOVE
         requests. A DICOM C-FIND request to the Connect service will query all 
         configured DICOM sources and forward the results, applying the 
         <cfindRequest> and <cfindResponse> morphers as appropriate. Results of
         studies are cached in the <studyLocationCache> and used when handling
         an adhoc DICOM C-MOVE request. 

         An adhoc DICOM C-MOVE will request that a study be forwarded to the 
         <MoveDestination>. If the study has not been recently located by an 
         adhoc C-FIND cannot be found in the <studyLocationCache>, then the 
         C-MOVE will fail.

        <server idref="dicom-connect" name="cmove" />
        <server idref="dicom-connect" name="cfind" />
        -->

        <!-- 
         Reference to an hl7 device for the Move Destination in the cache and 
         forward workflow. This device is used to send HL7 ORU's when converting
         DICOM SR's. 
        -->
        <device idref="moveDestinationHL7" name="PACSHL7"/>

        <!-- 
         Reference to an EMPI PIX device for translating patient 
         id's to the domains of within each <dicom> AE's <domain>
         tag.
        -->
        <device idref="pix" />

        <config>
            <!-- 
             The DICOM AE's we will configure for each study. 
            -->
            <prop name="Sources">
                <dicom>
                    <!-- 
                     This property is required.

                     The AE title  of a <dicom> source must be referenced 
                     by a <device type="dicom" id="someId">. The device must 
                     have a domain defined. Patient ids in the query will be 
                     translated to the domain of the device. Sources with no 
                     known domains for a given patient will not be queried. 
                    -->
                    <ae>SRC1</ae>
                    <!-- 
                     This property is optional. If not configured the domain 
                     configured under the corresponding 
                     <device type="dicom" id="someId"> will be used to query 
                     the source.
                     The <domain> element can be used multiple times. This is 
                     useful when one source may have multiple domains. Each 
                     domain must contain:

                         namespaceId - The human-readable name of the domain.
                         universalId - The globally unique, HL7-assigned object 
                         identifier (OID) for the domain.
                         universalIdType - The domain type.

                     The domain must be in one of the following formats:

                         a) namespaceId&amp;universalId&amp;universalIdType
                         b)     <namespaceId>ns</namespaceId>
                                <universalId>uid</universalId>
                                <universalIdType>uidt</universalIdType>

                     crossReferencing is an optional property.

                     If set to "true": The Connect service assumes that the 
                     source cross-references patient identifiers across its 
                     multiple domains. The Connect service will query the 
                     source with a Patient ID that corresponds to the domain, 
                     and expects to receive back all possible results for the 
                     source. The Connect service would not query for the other 
                     domains listed for the source.
                    -->
                    <domain crossReferencing="false">
                        PPTH&amp;princeton.plainsboro&amp;ISO
                    </domain>
                    <domain crossReferencing="false">
                        RH&amp;royal.hospital&amp;ISO
                    </domain>
                    <cfindResponseMorpher>${rialto.rootdir}/etc/services/connect/scripts/cfind_response_morpher.groovy</cfindResponseMorpher>
                    <cfindRequestMorpher>${rialto.rootdir}/etc/services/connect/scripts/cfind_request_morpher.groovy</cfindRequestMorpher>
                </dicom>
                <dicom>
                    <ae>SRC2</ae>
                    <domain crossReferencing="false">
                        PPTH&amp;princeton.plainsboro&amp;ISO
                    </domain>
                    <cfindResponseMorpher>${rialto.rootdir}/etc/services/connect/scripts/cfind_response_morpher.groovy</cfindResponseMorpher>
                    <cfindRequestMorpher>${rialto.rootdir}/etc/services/connect/scripts/cfind_request_morpher.groovy</cfindRequestMorpher>
		        </dicom>
            </prop>

            <!--
             The FetchPriors property defines the behaviour of the Fetch Prior
             studies workflow that is executed when an HL7 message triggers.
            -->
            <prop name="FetchPriors">
                <!--
                 The properties under this tag run when an HL7 message is
                 received by the Connect service.
                -->
                <trigger>
                    <!-- 
                     This property is optional. 

                     This morpher can be used to normalize the contents of 
                     inbound hl7 messages. It can also be used to ignor HL7
                     messages and not trigger the prefetching of priors. 
                     -->
                    <morpher>
                        ${rialto.rootdir}/etc/services/connect/scripts/fps_trigger_morpher.groovy
                    </morpher>
                </trigger>

                <!-- 
                 This property is required. 

                 This morpher extracts details of the HL7 message into a DICOM 
                 C-FIND object that is used to search for relevant priors. 
                 -->
                <hl7ToCFindMorpher>
                    ${rialto.rootdir}/etc/services/connect/scripts/hl7_to_cfind_morpher.groovy
                </hl7ToCFindMorpher>

                <!-- 
                 This property is optional. 

                 This morpher extracts details of the HL7 message into a DICOM 
                 C-FIND object that is used to search for relevant priors. 
                 -->
                <cfindResponseFilter>
                    ${rialto.rootdir}/etc/services/connect/scripts/cfind_response_rank_filter.groovy
                </cfindResponseFilter>
            </prop>

           <!--
             This property enables the caching of studies before forwarding to 
             the Move Destination in both the Adhoc C-MOVE and Fetch Prior 
             Studies workflow. With this enabled, the Connect service can 
             localize images as well as localize and convert DICOM SR's to HL7 
             ORU messages.
            -->
            <prop name="CacheAndForward">
                <ImageDelivery>
                  <imageCache>
                    <cacheDir>${rialto.rootdir}/var/connect/images</cacheDir>
                    <maxRetries>4</maxRetries>
                    <retryWaitTime>2m</retryWaitTime>
                    <maxConcurrentCStores>5</maxConcurrentCStores>
                  </imageCache>
                </ImageDelivery>

                <ForeignImageMorpher>
                    ${rialto.rootdir}/etc/services/connect/scripts/foreign_image_localizer.groovy
                </ForeignImageMorpher>

                <PatientCFindToPACSRequestMorpher>
                    ${rialto.rootdir}/etc/services/connect/scripts/patient_cfind_to_pacs.groovy
                </PatientCFindToPACSRequestMorpher>

                <DICOMSRToORUMorpher>
                    ${rialto.rootdir}/etc/services/connect/scripts/sr_to_oru_morpher.groovy
                </DICOMSRToORUMorpher>
            </prop>

            <!-- 
             This property is required.

             The DICOM AE that is used as the Move Destination for outgoing 
             DICOM C-MOVE requests in the fetch relevant priors workflow.
            -->
            <prop name="MoveDestination">
                <destinationAE>${IA-AETitle-PF}</destinationAE>
            </prop>

            <!-- 
             This property is required.

             This is the AE title that identifies the Connect service in DICOM
             transcations.
            -->
            <prop name="LocalAETitle" value="${CONNECT-AETitle}" />
        </config>
    </service>

    <!-- 
     The following device represents the HL7 endpoint of the MoveDestination.
     For the Connect service, this endpoint will receive any HL7 reports 
     retrieved, ORU's converted from DICOM SR's or orders generated in the 
     CacheAndForward workflow.
    -->

    <device type="hl7v2" id="moveDestinationHL7">
        <host>localhost</host>
        <port>9000</port>
    </device>

    <!-- 
     The following device represents the DICOM endpoint of the MoveDestination.
     For the Connect service, this endpoint will receive any DICOM objects 
     originally forwarded to the Connect service. If the 
     <prop name="CacheAndForward"> is configured, the modified dicom objects are
     what will be forwarded to this device.
    -->
     <device type="dicom" id="IA_PF_DEVICE">
        <host>localhost</host>
        <port>11112</port>
       <aetitle>${IA-AETitle-PF}</aetitle>
    </device>

    <!-- 
     The following represents examples of how to configure the Source
     devices that we have identified in the <prop name="Sources"> of the
     Connect service.
    -->

     <device type="dicom" id="SRC1">
        <host>10.0.1.63</host>
        <port>11112</port>
       <aetitle>SRC1</aetitle>
    </device>

    <device type="dicom" id="SRC2">
        <host>10.0.1.83</host>
        <port>11112</port>
        <aetitle>SRC2</aetitle>
    </device>
</config>

