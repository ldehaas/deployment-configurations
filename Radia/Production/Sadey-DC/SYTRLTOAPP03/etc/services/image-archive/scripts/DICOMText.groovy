class DICOMText {

    static replaceEscapedText(text) {
        return text.replaceAll(
            "\\\\X((?:[0-9A-Fa-f]{2}){0,2})\\\\",
            { match, hex -> (char)Integer.parseInt(hex, 16) }
        );
    }

}
