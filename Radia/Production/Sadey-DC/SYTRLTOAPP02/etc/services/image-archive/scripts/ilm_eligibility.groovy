log.info("ilm_eligibility.groovy START")
log.debug("PID={} with domainUUID={} and AccessionNumber={} is local and will be considered for processing by ILM", localPid, localPid.domainUUID, accessionNumber )

// If the given study is from a domain that "belongs" to the local data center,
// the script returns true, and the ILM will operate on the study:
if (localPid != null) { // && localPid.domainUUID == "DC1") {
    log.info("results in true. Local PID is not null")
    return true;
} else {
    log.info("Result is false. Local PID is null")
    return false;
}

log.info("ilm_eligibility.groovy END")
