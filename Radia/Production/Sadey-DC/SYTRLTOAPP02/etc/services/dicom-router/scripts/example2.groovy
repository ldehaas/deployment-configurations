LOAD('router_workflow_builder.groovy')

import com.karos.rialto.workflow.model.Task.TaskPriority;

def builder = new RouterWorkflowBuilder(
    'var/router', 
    getCalledAETitle(),
    getCallingAETitle(),
    get(StudyInstanceUID))

builder.setInboundMorpher("etc/services/dicom-router/scripts/in.groovy")

// Everything goes to Rialto, with high priority for reads
builder.addDestination(calledAe:"RIALTO_SOUTH", transcodeDestinationTs:"1.2.840.10008.1.2.4.80", priority: TaskPriority.HIGH)
// Everything goes to Dell, with low priority because it's slow
builder.addDestination(calledAe:"DELL_INDEX_LINK", priority: TaskPriority.LOW)

// Now based on the called AE, decide where else to route these incoming images:
switch (getCalledAETitle()) {
    case "PACSSCAN":
        builder.addDestination(calledAe:"STENTOR_SCP", lookupAe: "STENTOR_SCP_UNIQ_AE1")
        break

    case "STENTOR_SCP":
        builder.addDestination(calledAe:"GUH_Rad_Rialto")
        break
}

return builder.build()
