/*
 * foreign_image_localizer.groovy
 *
 * This script examines a DICOM header object and modifies the metadata in-place 
 * to localize it into formats that the moveDestination can recognize and 
 * process. By localizing the metadata and modifying the DICOM object, the 
 * Connect service enables the moveDestination to ingest foreign studies and
 * images. This will run on both DICOM images and DICOM Structure Report's (SR).
 *
 * There may be cases when certain images or objects are not recognized by a 
 * particular moveDestination. For these objects, the script can return false to  
 * signal that the particular image should be discarded. Other images in the
 * study may still proceed.
 *
 * In deployments that require the images or objects to have the local patient
 * demographics (rather than the demographics from the retrieved images or 
 * objects themselves), the script can get the local patient demographic data by
 * accessing the list variable "localDemographics". This variable is populated
 * by the results obtained in the DICOM C-FIND to the moveDestination done in
 * the <prop name="CacheAndForward">/<PatientCFindToPACSRequestMorpher> morpher.
 * To access details from this variable, use the "get()" method. For example,
 * to retrieve the PatientName:
 *
 * def patientName = localDemographics.get(0).get(PatientName)
 *
 * To access the DICOM object as it passes through this morpher, use the "set()"
 * method.
 * For example, to set the value of PatientName: 
 * 
 * set("PatientName","John^Smith")
 * OR
 * set(PatientName,"John^Smith")
 */

log.info("foreign_image_localizer.groovy: Starting Localizer Script")

def getLocalDemographicsAttribute(fieldName) {
    if (localDemographics != null && !localDemographics.isEmpty()) {
        log.debug("foreign_image_localizer.groovy: Local Demographics exist at the moveDestination: '{}'", localDemographics[0].get(fieldName) )
        return localDemographics[0].get(fieldName)

    } else {
        log.debug("foreign_image_localizer.groovy: No Local Demographics exist at moveDestination for this study. Falling back to original values.")
        return get(fieldName)
    }
}

set(PatientName,      getLocalDemographicsAttribute(PatientName))
set(PatientBirthDate, getLocalDemographicsAttribute(PatientBirthDate))
set(PatientSex,       getLocalDemographicsAttribute(PatientSex))