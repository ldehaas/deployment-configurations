/*
 * in.groovy
 * 
 * This morpher is applied to all incoming objects from a DICOM C-STORE request.
 * This morpher may be used to generalize or correct any of the data in the 
 * image itself before we store this in the Image Archive and/or forward this
 * to another destination.
 * 
 * The Image Archive service selects which script to use for this based on the 
 * Source AE title when Rialto receives the image. Please see the property
 * <prop name="TagMorphers"> within the image-archive.xml configuration for
 * an example of this.
 *
 * To access values from the DICOM object, use the "get()" method. For 
 * example, to retrieve the PatientId from the object: 
 * 
 * def patientId = get("PatientId")
 * OR
 * def patientId = get(PatientId)
 *
 * This script may optionally return a boolean value (either true or false). If
 * the return value of the script is false then the object will be ignored and
 * not archived or forwarded to another destination (if configured within the
 * Image Lifecycle Management policy script). Other images or objects in the
 * study will still be archived and forwarded (again, if configured). The object
 * will still be acknowledged so that the source system does not attempt to send
 * it again.
 *
 *
 */

import java.time.LocalDateTime
import java.time.temporal.ChronoUnit
import java.time.format.DateTimeFormatter
import java.time.format.DateTimeParseException

log.debug("in.groovy: Start inbound morpher")


/*
 * The following block is used to ensure that the IssuerOfPatientID is complete
 * before we process an image. The domains below only serve as examples of
 * what we have configured within our system. Be changing the calling/called AE
 * titles within the different <prop name="TagMorphers"> configuration, we can
 * perform different actions depending on who the information is coming from.
 * For example, if we know that some studies from a particular source are 
 * missing the IssuerOfPatientID, we can set the value of the required tags
 * to represent which site this has come from.
 */
def issuerOfPatientId = get(IssuerOfPatientID)
def universalEntityID = get("IssuerOfPatientIDQualifiersSequence/UniversalEntityID")
def universalEntityIDType = get("IssuerOfPatientIDQualifiersSequence/UniversalEntityIDType")

if (issuerOfPatientId == null) {
    issuerOfPatientId = 'CG'
    set(IssuerOfPatientID, issuerOfPatientId)
}

log.debug("in.groovy: IssuerOfPatientId is {} ", issuerOfPatientId)

def (namespace, domain, type) = issuerOfPatientId.tokenize("&")
log.debug("in.groovy: namespace = {}, domain = {}, type = {}", namespace, domain, type)

if (namespace != "") {
    set(IssuerOfPatientID, namespace)
}

if (universalEntityID == null && domain == null) {
    if (get(IssuerOfPatientID) == "CG") {
        set("IssuerOfPatientIDQualifiersSequence/UniversalEntityID", 'county.general')
    } else if (get(IssuerOfPatientID) == "TL") {
        set("IssuerOfPatientIDQualifiersSequence/UniversalEntityID", 'toronto.lakeshore')
    } else if (get(IssuerOfPatientID) == "DR") {
        set("IssuerOfPatientIDQualifiersSequence/UniversalEntityID", 'don.river')
    } else if (get(IssuerOfPatientID) == "MASH") {
        set("IssuerOfPatientIDQualifiersSequence/UniversalEntityID", 'mobile.army')
    } else if (get(IssuerOfPatientID) == "PPTH") {
        set("IssuerOfPatientIDQualifiersSequence/UniversalEntityID", 'princeton.plainsboro')
    } else {
        set("IssuerOfPatientIDQualifiersSequence/UniversalEntityID", 'county.general')
    }
} else if (universalEntityID == null && domain != null){
    set("IssuerOfPatientIDQualifiersSequence/UniversalEntityID", domain)
}

if (universalEntityIDType == null && type == null) {
    set("IssuerOfPatientIDQualifiersSequence/UniversalEntityIDType", 'ISO')
} else if (universalEntityIDType == null && type != null) {
    set("IssuerOfPatientIDQualifiersSequence/UniversalEntityIDType", type)
}


/*
 * In some cases we may want to ensure that the age of a patient is calculated
 * using the date/time information provided in the object. We can achieve this 
 * by using the following example. Please note this is only an example.
 */
def patientDateOfBirth = get(PatientBirthDate)
def patientTimeOfBirth = get(PatientBirthTime)
def studyDate = get(StudyDate)
def studyTime = get(StudyTime)
def patientAge = get(PatientAge)

try {
    if (patientAge == null) {
        set("PatientAge", calculateAge(patientDateOfBirth, patientTimeOfBirth, studyDate, studyTime))
    }
} catch (Exception e) {
    log.warn("Failed to compute PatientAge. Proceeding without changing PatientAge. ", e)
}

String calculateAge(String patientDateOfBirth, String patientTimeOfBirth, String studyDate, String studyTime){
    def TIPPING_WEEKS_TO_DAYS = 4
    def TIPPING_MONTHS_TO_WEEKS = 3
    def TIPPING_YEARS_TO_MONTHS = 2
    
    if (patientDateOfBirth == null || studyDate == null) {
        return null
    }

    patientDateOfBirth = patientDateOfBirth.replace(":", "")
    studyDate = studyDate.replace(":", "")

    def patientDateTimeOfBirth
    def studyDateTime

    try {
        def dtf = DateTimeFormatter.ofPattern("yyyyMMddHHmmss")
        patientDateTimeOfBirth = LocalDateTime.parse(patientDateOfBirth + cleanTime(patientTimeOfBirth), dtf)
        studyDateTime = LocalDateTime.parse(studyDate + cleanTime(studyTime), dtf)
    } catch (DateTimeParseException e) {
        log.warn("in.groovy: Failed to parse PatientBirthDate or StudyDate.", e)
        return null
    }

    if (!patientDateTimeOfBirth.isBefore(studyDateTime)) {
        return null
    }

    def years = ChronoUnit.YEARS.between(patientDateTimeOfBirth, studyDateTime)
    def months = ChronoUnit.MONTHS.between(patientDateTimeOfBirth, studyDateTime)
    def weeks = ChronoUnit.WEEKS.between(patientDateTimeOfBirth, studyDateTime)
    def days = ChronoUnit.DAYS.between(patientDateTimeOfBirth, studyDateTime)
    def age = ""

    if (years > 999) {
        return null
    }
    if (years >= TIPPING_YEARS_TO_MONTHS) {
        age = years + "Y"
    } else if (months >= TIPPING_MONTHS_TO_WEEKS) {
        age = months + "M"
    } else if (weeks >= TIPPING_WEEKS_TO_DAYS) {
        age = weeks + "W"
    } else {
        age = days + "D"
    }

    return age.padLeft(4, "0")
}

String cleanTime(String str) {
    if (str == null) {
        return "000000"
    }
    str = str.replace(":", "") 
    if (str.indexOf(".") == 6) {
        str = str.substring(0, 6)
    }
    if (str.contains(".") || str.length() > 6 || str.length()%2 == 1) {
        return "000000"
    }
    return str.padRight(6, "0")
}

log.debug("in.groovy: End inbound morpher")
