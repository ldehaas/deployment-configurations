<?xml version="1.0"?>
<config>

    <server id="dicom-ia-store" type="dicom">
        <port>${IAStorePort}</port>
    </server>

    <server id="hl7-ia-update" type="hl7v2">
        <port>${UpdateStudies-HL7v2Port}</port>
    </server>

    <!-- Image Archive -->
    <!-- Most functionality on the same DICOM port as router, but CStore on secondary -->
    <service id="image-archive" type="image-archive">

        <!-- Usage of spaces is disabled in favour of using standard file system storage -->
        <!--
        <device idref="space-index" name="space" />
        <device idref="elastic" />
        -->

        <server idref="dicom-ia-store"  name="CStoreSCP" />
        <server idref="dicom-ia-store"  name="StgCmtSCP" />
        <server idref="dicom-ia-store"  name="StgCmtSCU" />
        <server idref="dicom-ia-store"  name="CFindSCP" />
        <server idref="dicom-ia-store"  name="CMoveSCP" />
        <server idref="hl7-ia-update"   name="update" />
        <server idref="hl7-ia-update"   name="merge" />
        <server idref="hl7-ia-update"   name="order" />
        <server idref="hl7-ia-update"   name="oru" />

        <server idref="http-rest" name="MINT">
            <url>/vault/mint/*</url>
        </server>

        <server idref="http-rest" name="ILM">
            <url>/vault/ilm/*</url>
        </server>

        <server idref="http-rest" name="QC">
            <url>/vault/qc/*</url>
        </server>

        <server idref="http-rest" name="WADO">
            <url>/vault/wado</url>
        </server>

        <server idref="http-rest" name="StudyManagement">
            <url>${IA-StudyManagement-Path}/*</url>
        </server>

        <server idref="http-rest" name="StowRS">
          <url>/vault/stowrs/*</url>
        </server>

        <!-- Study Validation web service
        <server idref="http-rest" name="StudyValidation">
            <url>/vault/utils/*</url>
        </server>
        -->

        <device idref="xdsrep" />
        <device idref="xdsreg" />
        <device idref="pix" />
        <device idref="pdq" />
        <device idref="pif" />

        <!-- DICOM workflow engine is disabled by default since it requires an -->
        <!-- Elastic 2 instance to function which needs to be installed separately -->
        <!-- <device idref="workflow-engine" /> -->

        <device idref="cassandra-dc1" />

        <config>

            <prop name="StorerType" value="FILE" />
            <prop name="Cache" value="/data/cache" />
            <prop name="Inbox" value="/data/index" />
            <prop name="JobQueue" value="/data/jobs" />
            <prop name="QCWorkDirectory" value="/data/qc" />
            <prop name="CacheSize" value="10" />
            <prop name="AETitle" value="${IA-AETitle}" />

            <!-- DefaultDomain:  For incoming studies, if Issuer of Patient ID (0010,0021) is empty, the
                 study is assumed to be from this domain.  Format: namespace&domainUID&domainUIDType -->
            <prop name="DefaultDomain" value="${System-DefaultLocalFullyQualifiedDomain}" />
            <prop name="AffinityDomain" value="${System-AffinityFullyQualifiedDomain}" />

            <prop name="IndexerType" value="CQL3" />
            <prop name="IndexerRetryTime" value="1m" />

            <!-- An ILM configuration, for archive mode -->
            <prop name="ImagingLifeCycleManagementConfiguration">
                <Mode>ARCHIVE</Mode>
                <!-- if you change data center name ensure that you also add eligibility script -->
                <DataCenter>DC1</DataCenter>
                <Policy>${rialto.rootdir}/etc/services/image-archive/scripts/ilm_policy.groovy</Policy>
                <EligibilityScript>${rialto.rootdir}/etc/services/image-archive/scripts/ilm_eligibility.groovy</EligibilityScript>
            </prop>

            <!-- An alternative ILM configuration, for cache mode -->
            <!-- For details, see: "Policy script" section of Admin Guide -->

	<!--prop name="ImagingLifeCycleManagementConfiguration">
              <Mode>CACHE</Mode>

              <MinASCThresholdRatio>0.5</MinASCThresholdRatio>
              <MinLRRUSCRecoveryRatio>0.1</MinLRRUSCRecoveryRatio>
              <LRRUSCCleanUpMode>HARD_DELETE</LRRUSCCleanUpMode>
              <FCMDSleepTime>5m</FCMDSleepTime>

              <LRRUSCSleepTime>1m</LRRUSCSleepTime>
              <StudyStabilityTime>1m</StudyStabilityTime>

              <StudyRecordBatchRetrieveSize>1000</StudyRecordBatchRetrieveSize>
              <OperationExecutorThreadPoolSize>30</OperationExecutorThreadPoolSize>
              <Policy>${rialto.rootdir}/etc/services/image-archive/scripts/ilm_policy.groovy</Policy>
              <DataCenter>DC1</DataCenter>
              <EligibilityScript>${rialto.rootdir}/etc/services/image-archive/scripts/ilm_eligibility.groovy</EligibilityScript>
              <PrefetchedFilter>
                <callingAE>*</callingAE>
                <calledAE>PF_RIALTO</calledAE>
              </PrefetchedFilter>

              <StgCmtMaxAttemptsToForward>5</StgCmtMaxAttemptsToForward>
            </prop-->


            <prop name="LinkManagerAE" value="RIALTO_MWL" />

            <prop name="CassandraConfiguration">
                <keyspaceName>${CassandraKeyspacePrefix}imagearchive</keyspaceName>
                <keyspaceReplicationStrategy>NetworkTopologyStrategy</keyspaceReplicationStrategy>
                <keyspaceReplicationStrategyOptions>${CassandraReplicationOption}</keyspaceReplicationStrategyOptions>
                <keyspaceCreateEnabled>true</keyspaceCreateEnabled>
                <dynamicConsistencyLevelEnabled>false</dynamicConsistencyLevelEnabled>
                <schemaUpdateEnabled>false</schemaUpdateEnabled>
            </prop>

            <prop name="DirectAccess" value="true" />
            <prop name="MintEnabled" value="true" />

            <prop name="FileSystemStorageConfiguration">
                <FileSystemStorageLocation>
                   <AbsoluteBasePath>/mnt/scality/archive</AbsoluteBasePath>
                   <ReadOnly>false</ReadOnly>
                   <SourceDataCenter>DC1</SourceDataCenter>
                </FileSystemStorageLocation>
            </prop>

            <!-- HCP Configuration
            <prop name="HcpConfiguration">
                <HcpNodeConfiguration>
                    <HcpUrl>${HCPURL}</HcpUrl>
                    <HcpHost>${HCPHost}</HcpHost>
                    <HcpUsername>${HCPUsername}</HcpUsername>
                    <HcpPassword>${HCPPassword}</HcpPassword>
                    <HcpAuthHeader>${HcpAuthHeader}</HcpAuthHeader>
                </HcpNodeConfiguration>
                <HttpConnectionConfiguration>
                    <poolMaxConnectionsPerHost>${HCPPoolMaxConnectionsPerHost}</poolMaxConnectionsPerHost>
                    <poolMaxTotalConnections>${HCPPoolMaxTotalConnections}</poolMaxTotalConnections>
                </HttpConnectionConfiguration>
            </prop>
            -->

            <prop name="MWLReconciliationEnabled">false</prop>
            <prop name="QCToolsReconciliationScript">${rialto.rootdir}/etc/services/image-archive/scripts/qcreconciliation.groovy</prop>
            <prop name="ReconciliationScript">${rialto.rootdir}/etc/services/image-archive/scripts/qcreconciliation.groovy</prop>

            <prop name="PatientNameInHL7Location" value="/.PID-5" />

            <!-- Manifest Publish Configuration -->
            <prop name="PublisherType" value="DISCARD" />
            <prop name="MetadataStudyInstanceUIDKey">studyInstanceUid</prop>
            <prop name="HealthCareFacilityCode" value="HealthCareFacilityCodeValue, HealthCareFaciltiyCodeScheme, HealthCareFaciltiyCodeDisplay" />
            <prop name="PracticeSettingCode" value="PracticeSettingCodeValue, PracticeSettingCodeScheme, PracticeSettingCodeSchemeDisplay" />
            <prop name="ClassCode" value="ClassCodeValue, ClassCodeScheme, ClassCodeDisplay" />
            <prop name="TypeCode" value="TypeCodeValue, TypeCodeScheme,TypeCodeDisplay" />
            <prop name="ContentTypeCode" value="ContentTypeCodeValue, ContentTypeCodeScheme, ContentTypeCodeDisplay" />
            <prop name="ConfidentialityCode" value="ConfidentialityCodeValue, ConfidentialityCodeScheme,ConfidentialityCodeDisplay" />
            <!-- Manifest metadata is now determined from the groovy script -->
            <prop name="DocumentMetadataMorpher">${rialto.rootdir}/etc/services/image-archive/scripts/document_metadata_morpher.groovy</prop>

            <prop name="PatientIdentityFeedMorpher">${rialto.rootdir}/etc/services/image-archive/scripts/pif_morpher.groovy</prop>

            <!--prop name="CrossAffinityDomain">"RADIAAFFINITY&amp;EMPI_CROSS_AFFINITY_DOMAIN_ID&amp;ISO"</prop-->

            <prop name="TagMorphers">
                <script direction="IN" file="${rialto.rootdir}/etc/services/image-archive/scripts/in.groovy" />
                <script direction="OUT" file="${rialto.rootdir}/etc/services/image-archive/scripts/out.groovy" />
            </prop>

            <prop name="MigrateStudyMetadataOnQuery">false</prop>

            <!-- Study Content Notification -->
            <!-- Note: When configuring notification destinations, please ensure the receivingApplication
                 and receivingFacility match up with a configured device -->
            <prop name="StudyContentNotifierType" value="BASIC" />

            <prop name="NotificationDestinations">
                <destination name="hl7-scn">
                    <receivingApplication>SCN-RADIA</receivingApplication>
                    <receivingFacility>SCN-RADIA</receivingFacility>
                    <morphingScript>${rialto.rootdir}/etc/services/image-archive/scripts/scn.groovy</morphingScript>
                </destination>
            </prop>
	<prop name="CreatingUniqueFolderPerFile">true</prop> 
        </config>
    </service>

</config>
