/**
 * Copyright Karos Health 2013
 * This script can be used to query postgres pix database and 
 * send ADTs to a new pix database (potentially with cassandra-backend)
 * 
 * Invoke the script using rtkgrv in cmdline-internal
 */
import java.io.File;

import org.kohsuke.args4j.CmdLineException;
import org.kohsuke.args4j.CmdLineParser;
import org.kohsuke.args4j.Option;
import org.slf4j.Logger
import org.slf4j.LoggerFactory;

import ca.uhn.hl7v2.model.v23.message.ADT_A01;
import ca.uhn.hl7v2.model.v23.segment.*;

import com.karos.rtk.common.Domain
import com.karos.rtk.common.Pid
import com.karos.rtk.common.Terser;
import com.karos.rtk.common.HL7v2Client;
import com.karos.groovy.morph.HL7ToHL7;

import groovy.sql.Sql;

/**
 * 
 * @author Mohammad Derakhshani <mohammad.derakhshani@karoshealth.com>
 * @since Oct 21, 2014
 */

// the minix postgres database url
psqlJdbcUrl = "jdbc:postgresql://10.0.1.109/pix?user=rialto";

// the destination ADT listener to which the ADTs are sent
destinationADTListenerHost = "localhost";
destinationADTListenerPort = 4106;

// whether the script is meant to be used to generate random ADTs for testing
// a value greater than 0 will change the script to a random ADT generator
devTestingFeedWithRandomADTs = -1;

// RAMQ domainUUID
ramqDomain = "2.16.124.10.101.1.60.100";

// PATSEC domainUUID
patsecDomain = null;

// mapping from local MRN domainUUID to sending facility
domainToSendingFacility = ["2.16.124.10.101.1.60.1.1008.1": "sendingFac1"]

// mapping from local MRN domainUUID to sending Application
domainToSendingApp = ["2.16.124.10.101.1.60.1.1008.1": "sendingApp1" ]

logger = LoggerFactory.getLogger("minipix-migrator");

/**
 * Finds all pix relations in postgres database and for each sends ADT to the configured destination
 * @return
 */
def findAndProcessEachPixRelationInPostgres() {

    logger.info("started")
    // setup the connection to postgres database
    def sql = Sql.newInstance(psqlJdbcUrl, "org.postgresql.Driver")

    def queryForAllRAMQs = "SELECT * from pids where domain = $ramqDomain";
    logger.info("query to find ramq: {}", queryForAllRAMQs);
    sql.eachRow(queryForAllRAMQs) { ramqRow ->        
        logger.debug("found ramq row {}", ramqRow);
        def patsecResultRow = null;

        // do patsec query only if patsec domain is provided
        if (patsecDomain != null) {

            def queryMatchingPATSECs = "SELECT * from pids where domain = $patsecDomain AND parent = ${ramqRow.id}";
            logger.debug("query to find matching patsec: {}", queryMatchingPATSECs);

            def patsecResultList = sql.rows(queryMatchingPATSECs);
            logger.debug("found {} patsecs for ramq: {}", patsecResultList.size(), ramqRow);

            if (patsecResultList.size() > 1) {
                logger.warn("there are {} patsecs for ramq: {}, expected to have at most one patsec!", patsecResultList.size(), ramqRow);
                // if there are more than one patsec skip processing this pix relation
                return;
            } else if (patsecResultList.size() == 1){
                patsecResultRow = patsecResultList.get(0);
                logger.debug("found patsecs {}", patsecResultRow);
            }
        }

        def queryMatchingMRNs = "SELECT * from pids where domain != $patsecDomain AND parent = ${ramqRow.id}";                                
        logger.debug("query to find matching mrns: {}", queryMatchingMRNs);

        def mrnList = sql.rows(queryMatchingMRNs);
        logger.debug("found {} mrns for {}", mrnList.size(), ramqRow);
        for (mrnRow in mrnList) {
            def mrn = new Pid(mrnRow.get("pid"), mrnRow.get("domain"));
            def patsec = patsecResultRow == null? null: new Pid(patsecResultRow.get("pid"), patsecResultRow.get("domain"))
            def ramq =  new Pid(ramqRow.pid, ramqRow.domain);
            logger.info("pix found: mrn = " + mrn + ", patsec = " + patsec + ", ramq = " + ramq);

            // send adt for the given mrn, patsec, ramq
            sendADT(mrn, patsec, ramq);
        }
    }
}


/**
 * generates and sends ADT
 * @param mrn
 * @param patsec
 * @param ramq
 * @return
 */
def sendADT(Pid mrn, Pid patsec, Pid ramq) {

    def m = new ADT_A01();
    m.initQuickstart("ADT", "A01", "P");
    Terser t = new Terser(m);
    t.set("PID-2", ramq.pid);
    //t.set("PID-2-4-2", ramq.domainUUID);
    //t.set("PID-2-4-3", ramq.domainUUIDtype);
    t.set("PID-3", mrn.pid);
    //t.set("PID-3-4-2", mrn.domainUUID);
    //t.set("PID-3-4-3", mrn.domainUUIDtype);

    if (patsec != null) {
        t.set("PID-4", patsec.pid);
        //t.set("PID-4-4-2", patsec.domainUUID);
        //t.set("PID-4-4-3", patsec.domainUUIDtype);
    }
    
    t.set("MSH-3", domainToSendingApp.getAt(mrn.domainUUID));
    t.set("MSH-4", domainToSendingFacility.getAt(mrn.domainUUID));
    
    logger.info("sending the message\n{}", m.toString().replaceAll('\r', '\n'))
    rsp = hl7Client.send(m);
    Terser rspt = new Terser(rsp);
    if (rspt.get("MSA-1") != "AA") {
	logger.error("connect failed in processing the message\n{}\nfailed response is\n{}", m.toString().replaceAll('\r', '\n'), rsp.toString().replaceAll('\r', '\n'));
    }
}

// main starts

// parses the command line properties
hl7Client = new HL7v2Client(destinationADTListenerHost, destinationADTListenerPort);

if (devTestingFeedWithRandomADTs > 0) {

    // generate and send random pix relation adts for dev testing
    logger.info("sending {} random ADTs to {}:{}",
                devTestingFeedWithRandomADTs,
                destinationADTListenerHost,
                destinationADTListenerPort);

    for (int i = 0 ; i < devTestingFeedWithRandomADTs;  i++) {
        sendADT(new Pid("testMRN" + UUID.randomUUID(), new Domain()),
                new Pid("testPatsec" + UUID.randomUUID(), new Domain(patsecDomain)),
                new Pid("testRamq" + UUID.randomUUID(), new Domain(ramqDomain)))
    }

    logger.debug("sending {} test messages finished", devTestingFeedWithRandomADTs);

} else {

    // find all the pix relations in postgres database and for each send adt
    findAndProcessEachPixRelationInPostgres()

}

