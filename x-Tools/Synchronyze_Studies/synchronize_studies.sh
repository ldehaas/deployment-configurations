#!/bin/bash

# this script reads a list of StudyIntanceUIDs from a flat file
# queries source and destination systems
# issues a C-Move request to move a study from source to destination if number of SOPs/Series is different (or study not found at destination)

# Date: 2018-02-05
# Author: Alex Bloom


# parameters
domain=GUH
datacenter=SOUTH
DELAY_SEC=3s


case $domain in

    FSH)
      # FSH IMG
      SRC_AET=FSHINSITE02
      SRC_IP=172.23.8.65
      SRC_PORT=14444
      ;;

    GSH)
      # GSH IMG
      SRC_AET=GSHINSITE02
      SRC_IP=172.16.80.66
      SRC_PORT=14444
      ;;

    HHC)
      # HHC IMG
      SRC_AET=indexQuery-P0HS
      SRC_IP=172.16.209.107
      SRC_PORT=14444
      ;;

    UMH)
      # UMH IMG
      SRC_AET=indexQuery-P0H2
      SRC_IP=172.17.80.79
      SRC_PORT=14444
      ;;

    GUH)
      # GUH IMG
      #SRC_AET=indexQuery-P0JG
      #SRC_IP=172.19.1.192
      #SRC_PORT=14444
      ;;

    MGI)
      # MGI IMG
      SRC_AET=indexQuery-P0RA
      SRC_IP=172.18.7.202
      SRC_PORT=14444
      ;;

    MSMH)
      # MSMH IMG
      SRC_AET=indexQuery-P0RB
      SRC_IP=172.30.8.50
      SRC_PORT=14444
      ;;

    WB4)
      # WB4 IMG
      SRC_AET=indexQuery-P005
      SRC_IP=198.50.68.145
      SRC_PORT=14444
      ;;

    *)
      # South eIMG
      SRC_AET=indexQuery-P0WM
      SRC_IP=198.50.68.48
      SRC_PORT=14444

      # South eIMG
      #SRC_AET=indexQuery-P0T7
      #SRC_IP=198.50.67.28
      #SRC_PORT=14444
esac


case $datacenter in

    SOUTH)
      # MedStar South Rialto VIP: 172.27.6.17
      DEST_AET=RIALTO_CACHE
      DEST_IP=172.27.6.17
      DEST_PORT=4104
      ;;

    NORTH)
      # MedStar North Rialto VIP: 172.16.203.17
      DEST_AET=RIALTO_CACHE
      DEST_IP=172.27.6.17
      DEST_PORT=4104

esac

INPUT=cataloged_studies_${domain}_2018-02-05.txt
#INPUT=list_of_studies_to_sync.txt
LOGFILE=cataloged_studies_${domain}_sync_2018-02-09.log

srcSopCount=0
srcSeriesCount=0
destSopCount=0
destSeriesCount=0
destOffline=false


timestamp() {
  date +"%Y-%m-%d %H:%M:%S"
}

check_source () {
    studyInstanceUID=$1

    local result=$(/opt/dcm4che/bin/dcmqr $SRC_AET@$SRC_IP:$SRC_PORT -qStudyInstanceUID=$studyInstanceUID | grep "(0020,1206)\|(0020,1208)" | tail --lines 2)

    srcSopOutput=`echo "${result}" | tail --lines 1 | awk -F " " '{print $4}' | sed 's/[][]//g'`
    srcSeriesOutput=`echo "${result}" | head --lines 1 | awk -F " " '{print $4}' | sed 's/[][]//g'`

    #echo "$(timestamp): INFO: StudyIntanceUID : $siuid, DEBUG: srcSopOutput: $srcSopOutput" | tee -a $LOGFILE
    #echo "$(timestamp): INFO: StudyIntanceUID : $siuid, DEBUG: srcSeriesOutput: $srcSeriesOutput" | tee -a $LOGFILE

    srcSopCount="$srcSopOutput"
    srcSeriesCount="$srcSeriesOutput"
}

check_destination () {
    studyInstanceUID=$1

    #local result=$(/opt/dcm4che/bin/dcmqr $DEST_AET@$DEST_IP:$DEST_PORT -qStudyInstanceUID=$studyInstanceUID | grep "(0020,1206)\|(0020,1208)" | tail --lines 2)

    #destSopOutput=`echo "${result}" | tail --lines 1 | awk -F " " '{print $4}' | sed 's/[][]//g'`
    #destSeriesOutput=`echo "${result}" | head --lines 1 | awk -F " " '{print $4}' | sed 's/[][]//g'`

    local result=$(/opt/dcm4che/bin/dcmqr $DEST_AET@$DEST_IP:$DEST_PORT -qStudyInstanceUID=$studyInstanceUID -r00080056 | grep "(0020,1206)\|(0020,1208)\|(0008,0056)" | tail --lines 3)

    destSopOutput=`echo "${result}" | grep "(0020,1208)" | awk -F " " '{print $4}' | sed 's/[][]//g'`
    destSeriesOutput=`echo "${result}" | grep "(0020,1206)" | awk -F " " '{print $4}' | sed 's/[][]//g'`
    destAvailabilityOutput=`echo "${result}" | grep "(0008,0056)" | awk -F " " '{print $4}' | sed 's/[][]//g'`

    #echo "$(timestamp): INFO: StudyIntanceUID : $siuid, DEBUG: destSopOutput: $destSopOutput" | tee -a $LOGFILE
    #echo "$(timestamp): INFO: StudyIntanceUID : $siuid, DEBUG: destSeriesOutput: $destSeriesOutput" | tee -a $LOGFILE
    #echo "$(timestamp): INFO: StudyIntanceUID : $siuid, DEBUG: destAvailablityOutput: $destAvailabilityOutput" | tee -a $LOGFILE

    destSopCount="$destSopOutput"
    destSeriesCount="$destSeriesOutput"
    if [ "$destAvailabilityOutput" = "UNAVAILABLE" ] ; then
        destOffline=true;
    fi
}

mkdir -p ./logs

[ ! -f $INPUT ] && { echo "$INPUT file not found"; exit 99; }

while read siuid
do
    echo "=======================================================" | tee -a $LOGFILE
    echo "$(timestamp): INFO: StudyIntanceUID : $siuid, about to start processing ..." | tee -a $LOGFILE

    srcSopCount=0
    srcSeriesCount=0
    destSopCount=0
    destSeriesCount=0
    destOffline=false

    check_source $siuid
    #echo "$(timestamp): INFO: StudyIntanceUID : $siuid, found $srcSopCount SOPs and $srcSeriesCount Series at source $SRC_AET" | tee -a $LOGFILE

    check_destination $siuid
    #echo "$(timestamp): INFO: StudyIntanceUID : $siuid, found $destSopCount SOPs and $destSeriesCount Series at destination $DEST_AET" | tee -a $LOGFILE

    if [ -z "$srcSopCount" ] || [ -z "$srcSeriesCount" ]
    then
        echo "$(timestamp): INFO: StudyIntanceUID : $siuid, could not be found at source $SRC_AET" | tee -a $LOGFILE

    elif [ "$destOffline" = true ]
    then
        echo "$(timestamp): INFO: StudyIntanceUID : $siuid, study availability is OFFLINE at destistination, will C-Move this study to $DEST_AET" | tee -a $LOGFILE
        /opt/dcm4che/bin/dcmqr $SRC_AET@$SRC_IP:$SRC_PORT -cmove $DEST_AET -qStudyInstanceUID=$siuid >& ./logs/$siuid.log
        gzip ./logs/$siuid.log

    elif [ -z "$destSopCount" ] || [ -z "$destSeriesCount" ]
    then
        echo "$(timestamp): INFO: StudyIntanceUID : $siuid, study could not be found at destistination, will C-Move this study to $DEST_AET" | tee -a $LOGFILE
        /opt/dcm4che/bin/dcmqr $SRC_AET@$SRC_IP:$SRC_PORT -cmove $DEST_AET -qStudyInstanceUID=$siuid >& ./logs/$siuid.log
        gzip ./logs/$siuid.log

    elif [ ! -z "$destSopCount" ] && [ $srcSopCount -gt $destSopCount ]
    then
        echo "$(timestamp): INFO: StudyIntanceUID : $siuid, number of SOPs is different, Source SOP Count = $srcSopCount, Destination SOP Count = $destSopCount, will C-Move this study to $DEST_AET" | tee -a $LOGFILE
        /opt/dcm4che/bin/dcmqr $SRC_AET@$SRC_IP:$SRC_PORT -cmove $DEST_AET -qStudyInstanceUID=$siuid >& ./logs/$siuid.log
        gzip ./logs/$siuid.log

    elif [ ! -z "$destSeriesCount" ] && [ $srcSeriesCount -gt $destSeriesCount ]
    then
        echo "$(timestamp): INFO: StudyIntanceUID : $siuid, number of Series is different, Source Series Count = $srcSeriesCount, Destination Series Count = $destSeriesCount, will C-Move this study to $DEST_AET" | tee -a $LOGFILE
        /opt/dcm4che/bin/dcmqr $SRC_AET@$SRC_IP:$SRC_PORT -cmove $DEST_AET -qStudyInstanceUID=$siuid >& ./logs/$siuid.log
        gzip ./logs/$siuid.log

    else
        echo "$(timestamp): INFO: StudyIntanceUID : $siuid, both SOP and Series counts match, skipping..." | tee -a $LOGFILE
    fi

    echo "$(timestamp): INFO: StudyIntanceUID : $siuid, done processing." | tee -a $LOGFILE

    sleep $DELAY_SEC

done < $INPUT
