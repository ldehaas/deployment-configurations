#****************************************
#                MedStar                *
#****************************************

#-------------
#    TEST
#-------------

Host medstar
    HostName  198.50.67.50
    User      rialto
    ProxyJump cs_gateway

Host medstar_test_be evtest
    HostName  198.50.67.45
    User      miaccess
    ProxyJump cs_gateway

Host medstar_test_render
    HostName  198.50.67.47
    User      miaccess
    ProxyJump cs_gateway

Host medstar_socks_proxy
    HostName  198.50.67.50
    User      rialto
    ProxyJump cs_gateway
    RemoteForward 58080 127.0.0.1:58080

Host msh_test_router
    HostName  172.16.203.77
    User      rialto
    ProxyJump cs_gateway

Host medstar_tunnels
    HostName  198.50.67.50
    User      rialto
    ProxyJump cs_gateway

    # test cache
    #LocalForward 2522  127.0.0.2:2525
    LocalForward 2522  198.50.67.50:2525
    LocalForward 28080 198.50.67.50:8080

    #Nginx reverse proxy mapped to Kibana 5601
    LocalForward 28088 198.50.67.50:8088

    # Kibana direct access
    LocalForward 25601 127.0.0.1:5601
    LocalForward 29200 127.0.0.1:9200
    LocalForward 29300 127.0.0.1:9300
    LocalForward 29080 127.0.0.1:9080
    LocalForward 20022 198.50.67.50:22

    # Node.js South
    LocalForward 8091 172.27.6.92:8091

    # OpsCenter
    LocalForward 28888 172.27.6.92:8888
    LocalForward 28889 172.16.203.33:8888

    # MariaDB
    LocalForward 23306 172.27.6.92:3306
    LocalForward 23307 172.16.203.33:3306

    # Grafana
    LocalForward 23000 172.27.6.92:3000
    LocalForward 23080 172.27.6.92:8080
    LocalForward 23001 172.16.203.33:3000

    # test router
    LocalForward 2523  172.16.203.77:2525
    LocalForward 2524  172.16.203.78:2525

    # South Cache
    LocalForward 3015  172.27.6.39:2525
    LocalForward 3016  172.27.6.40:2525
    LocalForward 3017  172.27.6.41:2525
    LocalForward 3018  172.27.6.42:2525
    LocalForward 3019  172.27.6.92:2525

    # North Rialto Cache
    LocalForward 4015  172.16.203.61:2525
    LocalForward 4016  172.16.203.62:2525
    LocalForward 4017  172.16.203.63:2525
    LocalForward 4018  172.16.203.64:2525
    LocalForward 4019  172.16.203.33:2525


#-------------
#  South DC
#-------------

Host medstar_s0 msh-south-ilm
    HostName  172.27.6.92
    User      rialto
    ProxyJump cs_gateway

Host medstar_s1 msh-south-app1 medstar_cache
    HostName  172.27.6.39
    User      rialto
    ProxyJump cs_gateway

Host medstar_s2 msh-south-app2
    HostName  172.27.6.40
    User      rialto
    ProxyJump cs_gateway

Host medstar_s3  msh-south-app3
    HostName  172.27.6.41
    User      rialto
    ProxyJump cs_gateway

Host medstar_s4  msh-south-app4
    HostName  172.27.6.42
    User      rialto
    ProxyJump cs_gateway
    #LocalForward 3018  172.27.6.42:2525

Host medstar_s5  msh-south-db1
    HostName  172.27.6.43
    User      rialto
    ProxyJump cs_gateway

Host medstar_s6  msh-south-db2
    HostName  172.27.6.44
    User      rialto
    ProxyJump cs_gateway

Host medstar_s7  msh-south-db3
    HostName  172.27.6.45
    User      rialto
    ProxyJump cs_gateway

Host medstar_s8  msh-south-db4
    HostName  172.27.6.46
    User      rialto
    ProxyJump cs_gateway

Host medstar_s9  msh-south-db5
    HostName  172.27.6.47
    User      rialto
    ProxyJump cs_gateway

Host medstar_s10  msh-south-db6
    HostName  172.27.6.48
    User      rialto
    ProxyJump cs_gateway

Host medstar_s11  msh-south-db7
    HostName  172.27.6.49
    User      rialto
    ProxyJump cs_gateway

Host medstar_s12  msh-south-db8
    HostName  172.27.6.50
    User      rialto
    ProxyJump cs_gateway

Host medstar_s13  msh-south-db-backup
    HostName  172.27.6.51
    User      rialto
    ProxyJump cs_gateway


Host evprod
    HostName  172.27.6.62
    User      miaccess
    ProxyJump cs_gateway



#-------------
#   North DC
#-------------

Host medstar_north_tunnel
    HostName  172.16.203.61
    User      rialto
    ProxyJump cs_gateway
    LocalForward 2529 127.0.0.2:2525

Host medstar_n0  msh-north-ilm
    HostName  172.16.203.33
    User      rialto
    ProxyJump cs_gateway

Host medstar_n1  msh-north-app1
    HostName  172.16.203.61
    User      rialto
    ProxyJump cs_gateway

Host medstar_n2  msh-north-app2
    HostName  172.16.203.62
    User      rialto
    ProxyJump cs_gateway

Host medstar_n3  msh-north-app3
    HostName  172.16.203.63
    User      rialto
    ProxyJump cs_gateway

Host medstar_n4  msh-north-app4
    HostName  172.16.203.64
    User      rialto
    ProxyJump cs_gateway

Host medstar_n5 msh-north-db1
    HostName  172.16.203.65
    User      rialto
    ProxyJump cs_gateway

Host medstar_n6  msh-north-db2
    HostName  172.16.203.66
    User      rialto
    ProxyJump cs_gateway

Host medstar_n7  msh-north-db3
    HostName  172.16.203.67
    User      rialto
    ProxyJump cs_gateway

Host medstar_n8  msh-north-db4
    HostName  172.16.203.68
    User      rialto
    ProxyJump cs_gateway

Host medstar_n9  msh-north-db5
    HostName  172.16.203.69
    User      rialto
    ProxyJump cs_gateway

Host medstar_n10  msh-north-db6
    HostName  172.16.203.70
    User      rialto
    ProxyJump cs_gateway

Host medstar_n11  msh-north-db7
    HostName  172.16.203.71
    User      rialto
    ProxyJump cs_gateway

Host medstar_n12  msh-north-db8
    HostName  172.16.203.72
    User      rialto
    ProxyJump cs_gateway

Host medstar_n13  msh-north-db-backup
    HostName  172.16.203.73
    User      rialto
    ProxyJump cs_gateway



#-------------
#   Routers
#-------------

Host medstar_hhc1 mhhc msh-hhc1
    HostName  172.16.203.20
    User      rialto
    ProxyJump cs_gateway

Host medstar_hhc2 msh-hhc2
    HostName  172.16.203.21
    User      rialto
    ProxyJump cs_gateway

Host medstar_whc1 mwhc msh-whc1
    HostName  172.27.6.53
    User      rialto
    ProxyJump cs_gateway

Host medstar_whc2 msh-whc2
    HostName  172.27.6.54
    User      rialto
    ProxyJump cs_gateway

Host medstar_mmc1 mmmc
    HostName  172.18.7.187
    User      rialto
    ProxyJump cs_gateway
    #LocalForward 2521 127.0.0.1:2525
    #LocalForward 3390 mghdc08.mghad.mgh.com:389
    #LocalForward 3890 127.0.0.1:389 mghdc08.mghad.mgh.com

Host medstar_mmc2
    HostName  172.18.7.191
    User      rialto
    ProxyJump cs_gateway

Host medstar_guh1 mguh
    HostName  172.19.1.13
    User      rialto
    ProxyJump cs_gateway

Host mguh_tunnel
    HostName  172.19.1.13
    User      rialto
    ProxyJump cs_gateway
    LocalForward 2524 127.0.0.1:2525

Host medstar_guh2
    HostName  172.19.1.14
    User      rialto
    ProxyJump cs_gateway

Host medstar_smh1 msmh
    HostName  172.27.6.63
    User      rialto
    ProxyJump cs_gateway

Host medstar_smh2
    HostName  172.27.6.64
    User      rialto
    ProxyJump cs_gateway

Host medstar_smhc1 msmhc
    HostName  10.172.4.139
    User      rialto
    ProxyJump cs_gateway

Host medstar_smhc2
    HostName  10.172.4.158
    User      rialto
    ProxyJump cs_gateway

Host medstar_nrh1 mnrh
    HostName  172.27.6.56
    User      rialto
    ProxyJump cs_gateway

Host medstar_nrh2
    HostName  172.27.6.57
    User      rialto
    ProxyJump cs_gateway

Host medstar_hhc3 msh-hhc3
    HostName  172.16.203.27
    User      rialto
    ProxyJump cs_gateway

Host medstar_hhc4 msh-hhc4
    HostName  172.16.203.28
    User      rialto
    ProxyJump cs_gateway

Host medstar_gsh1 mgsh
    HostName  172.16.80.174
    User      rialto
    ProxyJump cs_gateway

Host medstar_gsh2
    HostName  172.16.80.175
    User      rialto
    ProxyJump cs_gateway

Host medstar_umh1 mumh
    HostName  172.17.81.113
    User      rialto
    ProxyJump cs_gateway

Host medstar_umh2
    HostName  172.17.81.114
    User      rialto
    ProxyJump cs_gateway

Host medstar_fsh1
    HostName  172.23.9.165
    User      rialto
    ProxyJump cs_gateway

Host medstar_fsh2
    HostName  172.23.9.166
    User      rialto
    ProxyJump cs_gateway
