#!/bin/bash

# This script is based on the Outaouais logs as of March 11, 2015


# Modify these variables according to what you need
CONTEXT_NAME="Petite-Nation"
LOGS_PATH="/home/rialto/rialto/log/"
OLDEST_LOG_TO_SEARCH="50"
NEWEST_LOG_TO_SEARCH="1"

FILENAMES=$(eval echo rialto_$CONTEXT_NAME.log.{$OLDEST_LOG_TO_SEARCH..$NEWEST_LOG_TO_SEARCH})


# Clean up temporary files from previous runs
rm /tmp/Rejected_Studies_$CONTEXT_NAME 
rm /tmp/Accession_Numbers_and_Patients_for_Studies_Rejected_at_PACS-$CONTEXT_NAME.txt

# Identify Study Instance UIDs for Studies that failed the Max number of SOP resend retries
cd $LOGS_PATH ; grep WARN $FILENAMES rialto_$CONTEXT_NAME.log | grep -v "Overwriting source" | grep -v "Invalid RAMQ" | grep "permanently failed" | sed -n 's/^.*study/study/p' | sed -n 's/study//p' | sed 's/t.*//' | sed -e 's/^[ \t]*//' -e 's/[ \t]*$//' | sort | uniq > /tmp/Rejected_Studies_$CONTEXT_NAME


# Find Accession Numbers and Patient IDs for Given Study Instance UIDs (using the Rialto Connect logs)
while read siuid;  do

    grep $siuid $FILENAMES rialto_$CONTEXT_NAME.log  | sed -n 's/^.*Accession/Accession/p' | sed 's/Foreign.*//' | sort | uniq | tee -a /tmp/Accession_Numbers_and_Patients_for_Studies_Rejected_at_PACS-$CONTEXT_NAME.txt

done < /tmp/Rejected_Studies_$CONTEXT_NAME 
