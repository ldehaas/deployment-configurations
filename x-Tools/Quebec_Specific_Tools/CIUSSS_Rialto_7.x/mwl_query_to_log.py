import os
import sys
import imp
import getpass

CQLSH_PATH = "/opt/cassandra/bin/cqlsh"
CASSANDRA_NAMESPACE = ""

cqlsh = imp.load_source("cqlsh", "/opt/cassandra/bin/cqlsh")

from cqlsh import sslhandling, read_options, CONFIG_FILE, DEFAULT_PROTOCOL_VERSION, DEFAULT_CQLVER, DEFAULT_CONNECT_TIMEOUT_SECONDS, Cluster, PlainTextAuthProvider, WhiteListRoundRobinPolicy


def get_cluster(argv):
    options, hostname, port = read_options(argv, os.environ)

    username = options.username
    password = options.password
    cqlver = options.cqlversion if options.cqlversion is not None else DEFAULT_CQLVER
    ssl = options.ssl
    connect_timeout = options.connect_timeout if options.connect_timeout is not None else DEFAULT_CONNECT_TIMEOUT_SECONDS

    auth_provider = None
    if username:
        if not password:
            password = getpass.getpass()
        auth_provider = PlainTextAuthProvider(username=username, password=password)

    cluster = Cluster(contact_points=(hostname,), port=port, cql_version=cqlver,
                   protocol_version=DEFAULT_PROTOCOL_VERSION,
                   auth_provider=auth_provider,
                   ssl_options=sslhandling.ssl_settings(hostname, CONFIG_FILE) if ssl else None,
                   load_balancing_policy=WhiteListRoundRobinPolicy([hostname]),
                   connect_timeout=connect_timeout)

    return cluster

# MWL ORDERS

import json
import string
import time
from pprint import pprint
from itertools import product
from datetime import datetime
from httplib import HTTPConnection

cluster = get_cluster(sys.argv[1:])
mwl = cluster.connect(CASSANDRA_NAMESPACE + "mwl")
timestr = time.strftime("%Y%m%d-%H%M%S")

MWL_HOST = "10.241.55.214"
MWL_PORT = 2525

entries = []

entries.extend(list(set(mwl.execute(u"select accession_number, accession_number_universal_id, patient_id, patient_id_universal_id from mwl_date_time_index where index_name = 'procstep_date_time' and month = '06' and date_identifier >= '2018-06-18T00:30:00.000Z' and date_identifier <= '2018-06-18T09:30:00.001Z_7f'"))))

for entry in entries:
    
    records = []

    records.extend(list(set(mwl.execute(u"select requested_procedure_ids from mwl where accession_number='{}' and accession_number_universal_id='{}' and patient_id='{}' and patient_id_universal_id='{}' limit 1".format(entry.accession_number, entry.accession_number_universal_id, entry.patient_id, entry.patient_id_universal_id)))))

    with open('mwl_queryresults_'+str(timestr)+'.log', 'a') as path:
        for record in records:
            path.write(str(entry) + ", " + str(record) + "\n")

cluster.shutdown()
