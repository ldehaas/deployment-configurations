import os
import sys
import imp
import getpass

CQLSH_PATH = "/opt/cassandra/bin/cqlsh"
CASSANDRA_NAMESPACE = ""

cqlsh = imp.load_source("cqlsh", "/opt/cassandra/bin/cqlsh")

from cqlsh import sslhandling, read_options, CONFIG_FILE, DEFAULT_PROTOCOL_VERSION, DEFAULT_CQLVER, DEFAULT_CONNECT_TIMEOUT_SECONDS, Cluster, PlainTextAuthProvider, WhiteListRoundRobinPolicy


def get_cluster(argv):
    options, hostname, port = read_options(argv, os.environ)

    username = options.username
    password = options.password
    cqlver = options.cqlversion if options.cqlversion is not None else DEFAULT_CQLVER
    ssl = options.ssl
    connect_timeout = options.connect_timeout if options.connect_timeout is not None else DEFAULT_CONNECT_TIMEOUT_SECONDS

    auth_provider = None
    if username:
        if not password:
            password = getpass.getpass()
        auth_provider = PlainTextAuthProvider(username=username, password=password)

    cluster = Cluster(contact_points=(hostname,), port=port, cql_version=cqlver,
                   protocol_version=DEFAULT_PROTOCOL_VERSION,
                   auth_provider=auth_provider,
                   ssl_options=sslhandling.ssl_settings(hostname, CONFIG_FILE) if ssl else None,
                   load_balancing_policy=WhiteListRoundRobinPolicy([hostname]),
                   connect_timeout=connect_timeout)

    return cluster

def split_fully_qualified_pid(fully_qualified_pid):
    pid, issuer = fully_qualified_pid.split("^^^")
    namespace, domain_uid, domain_uid_type = issuer.split("&")
    return pid, namespace, domain_uid, domain_uid_type

# MWL ORDERS

import ssl
import json
import string
from pprint import pprint
from itertools import product
from datetime import datetime
from httplib import HTTPSConnection

cluster = get_cluster(sys.argv[1:])
mwl = cluster.connect(CASSANDRA_NAMESPACE + "mwl")

MWL_HOST = "10.241.55.213"
MWL_PORT = 2525

entries = []

entries.extend(list(set(mwl.execute(u"select accession_number, accession_number_universal_id, patient_id, patient_id_universal_id from mwl_date_time_index where index_name = 'procstep_date_time' and month = '01' and date_identifier >= '2019-01-03T10:30:00.000Z' and date_identifier <= '2019-01-03T12:30:00.001Z_7f'"))))

for entry in entries:

   records = []

   records.extend(list(set(mwl.execute(u"select requested_procedure_ids from mwl where accession_number=%s and accession_number_universal_id=%s and patient_id=%s and patient_id_universal_id=%s limit 1", (entry.accession_number, entry.accession_number_universal_id, entry.patient_id, entry.patient_id_universal_id)))))

   for record in records:
       for rpid in record.requested_procedure_ids:
           body = {
               "accessionNumber": entry.accession_number,
               "accessionNumberUniversalId": entry.accession_number_universal_id,
               "patientId": entry.patient_id,
               "patientIdUniversalId": entry.patient_id_universal_id,
               "requestedProcedureId": rpid
           }

           conn = HTTPSConnection(MWL_HOST, MWL_PORT, context=ssl._create_unverified_context())
           conn.request("POST", "/rest/api/mwl/mwl/fetch", headers={"Content-Type": "application/json", "Cookie": "JSESSIONID=3a413962-85ae-4f57-99dc-297416d2d27a"}, body=json.dumps(body))
           resp = conn.getresponse()
           print resp.status, resp.reason
           conn.close()

cluster.shutdown()
