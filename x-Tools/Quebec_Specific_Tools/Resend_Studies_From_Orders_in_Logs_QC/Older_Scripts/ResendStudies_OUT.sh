#! /bin/bash

# First, find all the Studies that were prepared (i.e. localized to send to PACS) on Feb 25:
#   nice -15 grep -r "2014-02-25" ~/rialto/log/ | grep "Preparing" > ./rawSOPsFeb25

# Then, find only the unique PIDS and StudyInstance UIDS 
#   cat rawSOPsFeb25 | awk '{print $16 $25}' | sort | uniq > cleanedUpPIDsAndStudyIUIDs

# Manually clean up the 'cleanedUpPIDsAndStudyIUIDs' file using vim (remove quotations and other symbols)

# NOTE: Manually create additional files for the contexts that cannot be determined automatically

# Use the 'PIDSandSIUIDs' file to seed the resending script:

while read -r PID SIUID
do

    SiteChecker=$PID
    echo $SiteChecker

    SiteChecker=`echo $SiteChecker | cut -c1 `
    echo $SiteChecker

    if [ $SiteChecker == 'G' ] 
        then
            echo "This is deemed a Gatineau Patient. Hitting Rialto on Port 4104."

            # The generic CFind is required to find Hashed SIUIDs
            # i.e. if you search for a hashed SIUID right of the bat, you will get 0 responses
            ~/Tools/dcm4che-2.0.27-SNAPSHOT/bin/dcmqr -cfindrspTO 9999999 -L ALI_SCU rsndOutStds@localhost:4104 -qPatientID=$PID  | less

            ~/Tools/dcm4che-2.0.27-SNAPSHOT/bin/dcmqr -cfindrspTO 9999999 -L ALI_SCU rsndOutStds@localhost:4104 -qPatientID=$PID -qStudyInstanceUID=$SIUID | less

    elif [ $SiteChecker == 'H' ]
        then
            echo "This is deemed a Hull Patient. Hitting Rialto on Port 4106."

            # The generic CFind is required to find Hashed SIUIDs
            # i.e. if you search for a hashed SIUID right of the bat, you will get 0 responses
            ~/Tools/dcm4che-2.0.27-SNAPSHOT/bin/dcmqr -cfindrspTO 9999999 -L ALI_SCU rsndOutStds@localhost:4106 -qPatientID=$PID  | less

            ~/Tools/dcm4che-2.0.27-SNAPSHOT/bin/dcmqr -cfindrspTO 9999999 -L ALI_SCU rsndOutStds@localhost:4106 -qPatientID=$PID -qStudyInstanceUID=$SIUID | less


    elif [ $SiteChecker == 'S' ]
        then
            echo "This is deemed a Hull Patient. Hitting Rialto on Port 4112."

            # The generic CFind is required to find Hashed SIUIDs
            # i.e. if you search for a hashed SIUID right of the bat, you will get 0 responses
            ~/Tools/dcm4che-2.0.27-SNAPSHOT/bin/dcmqr -cfindrspTO 9999999 -L ALI_SCU rsndOutStds@localhost:4112 -qPatientID=$PID  | less

            ~/Tools/dcm4che-2.0.27-SNAPSHOT/bin/dcmqr -cfindrspTO 9999999 -L ALI_SCU rsndOutStds@localhost:4112 -qPatientID=$PID -qStudyInstanceUID=$SIUID | less


    else
        echo "The context for this Patient $PID cannot be determined.  It will be ignored in automatic processing."
    fi

            ~/Tools/dcm4che-2.0.27-SNAPSHOT/bin/dcmqr -cfindrspTO 9999999 -L ALI_SCU rsndOutStds@localhost:4104 -qPatientID=$PID  | less
            ~/Tools/dcm4che-2.0.27-SNAPSHOT/bin/dcmqr -cfindrspTO 9999999 -L ALI_SCU rsndOutStds@localhost:4104 -qPatientID=$PID -qStudyInstanceUID=$SIUID | less

done < ./PIDSandSIUIDs
