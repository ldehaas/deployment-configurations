#! /bin/bash

# First, find all the Studies that were prepared (i.e. localized to send to PACS) on Feb 25:
#   nice -15 grep -r "2014-02-25" ~/rialto/log/ | grep "Preparing" > ./rawSOPsFeb25

# Then, find only the unique PIDS and StudyInstance UIDS 
#   cat rawSOPsFeb25 | awk '{print $16 $25}' | sort | uniq > PIDSandSIUIDs
#   you might not be so lucky, and might have to use vim teach it a pattern, and apply it many times

# Manually clean up the 'PIDSandSIUIDs' file using vim (remove quotations and other symbols)

# Use the 'PIDSandSIUIDs' file to seed the resending script:

TICKET="KHC"
RIALTOPORT="4110"
PACS_CSTORE_AETITLE="ALI_STORE_SCP"


while read -r PID SIUID
do

  ## Remove this first block in a non-multi-context site
    echo "PID is currently $PID, and SIUID is currently $SIUID"
    # First, do just a generic C-Find, as the Study Instance UIDs we have in the log are actually hashed
    # REMOVE this code line if not in a multi-context situation
    dcmqr -cfindrspTO 9999999 -L ALI_SCU $TICKET@localhost:$RIALTOPORT -qPatientID=$PID
  ## End optional block for multi-context sites 

    # Send each individual study as an ad-hoc C-MOVE resulting from a C-Find
    dcmqr -cfindrspTO 9999999 -L ALI_SCU $TICKET@localhost:$RIALTOPORT -qPatientID=$PID -qStudyInstanceUID=$SIUID -cmove $PACS_CSTORE_AETITLE

done < ./PIDSandSIUIDs
