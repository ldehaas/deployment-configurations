#!/bin/sh

RIALTO_PORT=5556

mkdir -p msgs/sent

find msgs/tosend -type f | while read f; do
    echo Sending $f
    hl7snd -f $f -d localhost:$RIALTO_PORT
    mv $f msgs/sent/
done

