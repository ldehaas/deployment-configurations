#!/bin/sh

# Pipe in a rialto log. You may need to examine it to make sure that its log
# messages correspond to what we're expecting here.
# If split complains about running out of files, adjust the -a flag upwards
# until you have enough space.

mkdir -p msgs/tosend
egrep -o 'Inbound message:.*' | cut -c 18- | (cd msgs/tosend; split -l 1 -a 3)

