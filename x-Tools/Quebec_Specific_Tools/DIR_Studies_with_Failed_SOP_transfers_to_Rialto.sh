#! /bin/sh

grep -r "WARN,com.karos.dcm.cmove.CMoveSCU" ~/rialto/log | grep "cmove ended with error" | sed -n 's/^[^:]*://p' | awk '{print $1}' | sort | uniq -c
