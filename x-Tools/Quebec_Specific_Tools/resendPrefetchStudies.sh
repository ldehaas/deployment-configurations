# Re-send prefetched studies for Change Healthcare tool
# Written by: Jason Yip
# Date: 2019-07-02
# Version 1.0

#/bin/sh

#!/usr/bin/env bash
#LOGFILE="/path/to/log.log"

#logFile=/tmp/resentStudies.log
logFile=/home/rialto/rialto/log/resentStudies.log
TIMESTAMP=`date "+%Y-%m-%d %H:%M:%S"`

clear

echo "**************************************************"
echo "Resend prefetch studies for Change Healthcare tool"
echo "**************************************************"
echo ""

declare -a callingAETs=('ALI_SCU','ALI_SCU_CHFC','ALI_SCU_CHGM','ALI_SCU_CHMS','ALI_SCU_CHPN','ALI_SCU_CSPA','ALI_SCU_CSPO','ALI_SCU_CSVG','ALI_SCU_GAT','ALI_SCU_HCB','ALI_SCU_HCIM','ALI_SCU_HDOU','ALI_SCU_HLHL','ALI_SCU_HNDM','ALI_SCU_HULL','ALI_SCU_ICM','ALI_SCU_IPPM','ALI_SCU_IRGLM','ALI_SCU_IUGM','ALI_SCU_PTME','ALI_SCU_SCO','ALI_SCU_SMSL');
declare -a calledAETs=('RIALTO_CHFC','RIALTO_CHGM','RIALTO_CHIS','RIALTO_CHLS','RIALTO_CHMS','RIALTO_CHPN','RIALTO_CHRN','RIALTO_CSL','RIALTO_CSPA','RIALTO_CSPO','RIALTO_CSVG','RIALTO_CSVO','RIALTO_GAT','RIALTO_HCB','RIALTO_HCIM','RIALTO_HDDA','RIALTO_HDLA','RIALTO_HDOU','RIALTO_HGL','RIALTO_HJR','RIALTO_HLHL','RIALTO_HND','RIALTO_HNDM','RIALTO_HULL','RIALTO_ICM','RIALTO_IPPM','RIALTO_IRGLM','RIALTO_IUGM','RIALTO_PAVM','RIALTO_PTME','RIALTO_SCO','RIALTO_SMSL','RIALTO_SOV','RIALTO_TEKI','RIALTO_VALD');

# Validates Calling AE title
read -p "Enter the CALLING AE Title" callingAET

if [[ " ${callingAETs[@]} " =~ $callingAET ]] ; then
    read -p "Enter the Patient ID " patientID
else
    echo "CALLING AE Title is invalid/not found. Cancelling DICOM Query."
    exit
fi

#Validates Called AE title
read -p "Enter the CALLED AE Title" calledAET

if [[ " ${calledAETs[@]} " =~ $calledAET ]] ; then
    read -p "Enter the Study Instance UID " siuid
else
    echo "CALLED AE Title is invalid/not found. Cancelling DICOM Move."
    exit
fi

# Generates dcmqr and dcmmv
dcmQuery="dcmqr -L $callingAET @localhost:4104 -cfindrspTO 999999 -q PatientID=$patientID"
dcmMove="dcmmv -s $calledAET@localhost:4104 -d ALI_STORE_SCP -l $callingAET -q STUDY -u $siuid"

echo $dcmQuery
echo $dcmMove

# Check if the log file exists. If the log file does not exist, will create the new file and write the dcmqr and dcmmv
# You can copy & paste the dcmqr and dcmmv from this file for testing
if [ -f "$logFile" ] ; then
    echo "$TIMESTAMP $dcmQuery" >> $logFile
    echo "$TIMESTAMP $dcmMove" >> $logFile
else
    echo "$TIMESTAMP Starting log file..." >> $logFile
    echo "$TIMESTAMP $dcmQuery" >> $logFile
    echo "$TIMESTAMP $dcmMove" >> $logFile
fi