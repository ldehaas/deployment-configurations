/*
 * Parses an hl7rcv log for patients and searches the registry for
 * all studies for each patient and saves the results to a simplified
 * xml file that can later be easily searched for suitable test data
 * for acceptance tests.
 *
 * Run with rtkgrv and pipe an hl7rcv log (or any file containing hl7
 * messages with the ramq in PID-2 and the local pid in PID-3) to the
 * script's stdin.
 *
 * Copyright Karos Health Inc 2013
 */

import com.karos.rtk.xds.*;
import com.karos.rtk.common.*;
import com.karos.rtk.audit.*;
import com.karos.rialto.dcm.*;
import com.karos.dcm.cfind.*;
import com.karos.rialto.platform.dcm.*;
import org.dcm4che2.data.*;
import org.dcm4che2.net.*;
import java.security.*;
import java.net.*;

// configuration:

REG_URL = "https://xdsprod.dsq.rtss.qc.ca:5000/IBMXDSRegistry/XDSb/SOAP12/Registry"

tls = new TLS(
    keystore:     "/home/rialto/rialto/etc/tls/RialtoKeystore.jks",
    keystorePw:   "rialto",
    keyPw:        "Thinkpad06",
    truststore:   "/home/rialto/rialto/etc/tls/RialtoTruststore.jks",
    truststorePw: "rialto",
)

AUDIT_HOST = "localhost"
AUDIT_PORT = 4000

// 0 or less means to do them all:
maxPatientsToProcess = 5000

RAMQ_DOMAIN = "2.16.124.10.101.1.60.100"

LOCAL_AE = "RIALTO_VALD_TST"

DIRS = new Dirs(devices: [
    // Laval:
    "2.16.124.10.101.1.60.2.50": new DicomDevice(
        "PQDIR1QCSAS",                   // ae
        (String) null,                   // domain
        "10.28.73.205", // host
        11112),                          // port

    // Montreal (needs to be updated for each site):
    "2.16.124.10.101.1.60.2.60": new DicomDevice(
        "XDL_VALD_QRSCP", // ae
        (String) null,   // domain
        "10.241.25.59",  // host
        4005),           // port
], localAe: LOCAL_AE)

// utility classes:

class TLS {
    String keystore
    String keystorePw
    String keyPw
    String truststore
    String truststorePw

    def getEnc() {
        def enc = new EncryptionParameters()
        enc.setKeys(load(keystore, keystorePw), keyPw)
        enc.setTrust(load(truststore, truststorePw))
        return enc
    }

    private load(String file, pw) {
        def ks = KeyStore.getInstance("JKS")
        new File(file).withInputStream { is ->
            ks.load(is, pw == null ? null : pw.toCharArray())
        }
        return ks
    }
}

class Dirs {
    Map<String, DicomDevice> devices;
    String localAe;
    private liveClients = [:];

    public cfind(repouid, suid) {
        def client = liveClients[repouid]
        if (client == null) {
            def device = devices[repouid]
            if (device == null) {
                return "UnknownRepository"
            }

            client = new CFindSCUReuseAssoc(device, localAe)
            liveClients[repouid] = client
        }

        println "Querying ${client.dest.AE} for study $suid"

        def query = new BasicDicomObject()
        query.putString(Tag.QueryRetrieveLevel, null, "STUDY")
        query.putString(Tag.StudyInstanceUID, null, suid)

        def available = false
        client.cfind(query, [
            send: { result, warning ->
                available = true
            },
            valid: { return true }
        ] as CFindCallback)

        return available
    }

    public close() {
        liveClients.each { repouid, client ->
            client.close()
        }

        // the scus are not re-usable:
        liveClients.clear()
    }
}

// parse file containing hl7 messages with patient ids

// maps ramq -> local pid
patients = [:]
System.in.eachLine { line ->
    // this should match any PID segment anywhere in any file
    if ((m = line =~ /PID\|[^|]*\|([A-Z]+[0-9]+)\|([^|]+)/)) {
        // right now we only support one PID segement per line though, if
        // more were required, we could loop through m
        ramq = m[0][1]
        patients[ramq] = m[0][2]
    }
}

println "Found ${patients.size()} unique patients"

// find the first maxPatientsToProcess patients that haven't already been handled:
maxPatientsToProcess = maxPatientsToProcess > 0 ? maxPatientsToProcess : patients.size()
patients = patients.sort().findAll {
    !new File("${it.key}.xml").exists() && maxPatientsToProcess-- > 0
}

println "Processing ${patients.size()} patients: ${patients.keySet()}"

AuditConfiguration.setUDP(AUDIT_HOST, AUDIT_PORT)
EncryptionConfiguration.setDefaultEncryptionParameters(tls.getEnc())
docConsumer = new XDSDocumentConsumer(new URI(REG_URL), [:])

patients.each { ramqPid, localPid ->
    println "Searching registry for studies for $ramqPid"
    qry = new FindDocumentsQuery(new Pid(ramqPid, RAMQ_DOMAIN))
    qry.getFormats().add(new Code("urn:ihe:rad:1.2.840.10008.5.1.4.1.1.88.59", "1.2.840.10008.2.6.1"))

    start = System.currentTimeMillis()
    try {
        documents = docConsumer.findDocuments(qry)
    } catch (XDSServerException e) {
        // log this explicitly because we don't get a good message
        // if we let it fly:
        println "Uh oh!: $e"
        println "Skipping patient $ramqPid"
        return
    }
    time = System.currentTimeMillis() - start
    print "Found ${documents.size()} studies $ramqPid in ${time}ms"
    if (!documents.empty) {
        println " (${time / documents.size()}ms/doc)"
    } else {
        println()
    }

    // utility for safely getting the first element of a list:
    first = { it == null || it.isEmpty() ? null : it[0] }

    // query the dir to see if the studies are available:
    availabilities = [:]
    try {
        documents.each { doc ->
            suid = first(doc.getExtendedProperty("studyInstanceUID"))
            availabilities[suid] = DIRS.cfind(doc.repositoryUID, suid)
        }
    } catch (Exception e) {
        println "Unable to query DIR for study: $e"
        e.printStackTrace()
    } finally {
        // shut down associations after every patient so that we
        // don't have to worry about timeouts:
        DIRS.close()
    }

    // dump the patient to an xml file:
    new File("${ramqPid}.xml").withPrintWriter { out ->
        new groovy.xml.MarkupBuilder(out).patient {
            ramq(ramqPid)
            local(localPid)
            documents.collect { doc ->
                suid = first(doc.getExtendedProperty("studyInstanceUID"))
                study {
                    studyUID(suid)
                    sourcePid(doc.sourcePatientId.pid)
                    sourceDomain(doc.sourcePatientId.domainUUID)
                    doc.eventCodes.findAll({ it.schemeName == "DCM" && !["SR", "KO"].contains(it.codeValue) }).collect {
                        modality(it.codeValue)
                    }
                    doc.eventCodes.findAll({ it.schemeName == "Imagerie Québec-DSQ" }).collect {
                        anatomicRegion(it.codeValue)
                    }
                    doc.getExtendedProperty("accessionNumberList").collect {
                        accession(it)
                    }
                    repositoryUID(doc.repositoryUID)
                    date(doc.serviceStartTime)
                    availableInDIR(availabilities[suid])
                }
            }
        }
    }
}

