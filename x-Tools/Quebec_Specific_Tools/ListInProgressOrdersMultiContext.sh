#! /bin/bash

NUMBER_OF_CONTEXTS=8
COUNTER=1
LIMIT=0

let LIMIT=NUMBER_OF_CONTEXTS+1

echo "Number of In-Progress Orders"

while [ $COUNTER -lt $LIMIT  ] ; do
    echo -n "Site $COUNTER: "
    ls -1 ~/var/connect/Site$COUNTER/orders/inprogress | wc -l
    let COUNTER=COUNTER+1
done

    
let COUNTER=1
echo "Number of SOPs currently on disk to send to Contexts"
    
while [ $COUNTER -lt $LIMIT  ] ; do
    echo -n "Site $COUNTER: "
    find ~/var/connect/Site$COUNTER/images-cache -type f | wc -l
    let COUNTER=COUNTER+1
done
