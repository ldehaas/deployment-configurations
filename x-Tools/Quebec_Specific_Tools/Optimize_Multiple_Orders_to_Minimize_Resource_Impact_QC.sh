# Jeremy Huiskamp (put in here by Rodrigo)
#  run it with something like: ls orders/* | xargs group.sh 

for var in "$@"; do
    accn=$(grep order=MSH $var | sed 's/\\r/\'$'\n/g' | egrep ^OBR | awk -F'|' '{print $4}')
    mkdir -p grouped/$accn
    cp $var grouped/$accn
done
