/*
Create a program to generate delete statement to force cleanup of a study

Input:
- flat file with StudyInstanceUIDs (one SIUID per line)

Output:
- set of detele statements to clean up Cassandra database
- list of files on CIFs share
- report on status of a study
- dcmqr command to move study back into Rialto cache


program should perform the following steps:
- check study metadata
- then check if all files could be located on CIFs share
- if not then it should check study timeline (if study was HDEL or CATaloged)
- determine the DELL IMG the study was archived to and then make sure that study could be found on that IMG (scan study timeline or determine this info based on IssuerOfPatientID)

if study metadata could not be retrieved - generate a list of delete statements
if not all files could be found on CIFs - generate a list of delete statements

if study is not HDEL then:
    for each version:
    - check that all metadata files are present
    - check that Binary Items Description file is present
    - check that DICOM SOP Description file is present

    read Binary Items Description and extract a list of TAR files
    - check that all TAR files are present

    create a SortedMap collection and add TARs from all study version
    check each element of SortedMap and update value to 1 if file is present on CIFS share

initial scan should only identify damaged studies, a separate program can generate the cleanup steps.

Needs the following libraries:
cassandra-driver-core-3.0.0.jar
gson-2.8.0.jar
guava-19.0.jar
metrics-core-3.0.2.jar
netty-all-4.1.1.Final.jar
slf4j-api-1.7.10.jar
slf4j-simple-1.7.10.jar


Usage:
javac -classpath ".:./lib/*" StudyCleanupGen.java
screen
java -classpath ".:./lib/*" StudyCleanupGen SIUIDs_to_check.txt | tee SIUIDs_check.log


*/

import java.io.File;
import java.io.FileReader;
import java.io.FileNotFoundException;
import java.io.BufferedReader;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.StringReader;
import java.util.*;

import java.io.DataOutputStream;
import java.io.StringWriter;
import java.io.PrintWriter;
import java.net.HttpURLConnection;
import java.net.URL;
import javax.net.ssl.HttpsURLConnection;

import java.util.Date;
import java.text.DateFormat;
import java.text.SimpleDateFormat;

import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.xpath.XPath;
import javax.xml.xpath.XPathConstants;
import javax.xml.xpath.XPathExpressionException;
import javax.xml.xpath.XPathFactory;

import org.w3c.dom.Document;
import org.w3c.dom.NodeList;
import org.w3c.dom.Node;
import org.w3c.dom.Element;
import org.xml.sax.SAXException;
import org.xml.sax.InputSource;

import com.datastax.driver.core.Cluster;
import com.datastax.driver.core.ConsistencyLevel;
import com.datastax.driver.core.Session;
import com.datastax.driver.core.PreparedStatement;
import com.datastax.driver.core.BoundStatement;
import com.datastax.driver.core.ResultSet;
import com.datastax.driver.core.Row;

import com.google.gson.Gson;
import com.google.gson.JsonObject;
import com.google.gson.JsonElement;
import com.google.gson.JsonParser;

//import org.json.simple.JSONArray;
//import org.json.simple.JSONObject;
//import org.json.simple.parser.JSONParser;
//import org.json.simple.parser.ParseException;

import org.json.JSONObject;
import org.json.JSONArray;
import org.json.JSONException;

//import net.sf.json.JSONArray;
//import net.sf.json.JSONObject;
//import net.sf.json.JSONSerializer;

import org.apache.commons.io.IOUtils;

public class StudyCleanupGen {

    private static Boolean isDebug = false;
    private static String  environmentVar = "PROD_SOUTH";   // possible values TEST / PROD_NORTH / PROD_SOUTH

    public static class StudyStatus {
        public String   studyInstanceUID;
        public int      numberOfSops;
        public Date     lastReceivedTimestamp;
        public int      numberOfStgCmtAttempts;
        public Boolean  isPreftch;
        public Boolean  isPreftchChecked;
        public String   prefetchTimestamp;
        public String   prefetchSource;
        public Boolean  isForwarded;
        public String   forwardedTimestamp;
        public String   forwardedDestination;
        public int      numberOfForwardedSops;
        public Boolean  isStorageCommitted;
        public String   storageCommittedTimestamp;
        public int      storageCommittedObjects;
        public int      storageCommittedObjectsFailed;
        public String   storageCommitedDestination;
        public Boolean  isCATaloged;
        public Date     catalogTimestamp;
        public Boolean  isLogicalDELeted;
        public Date     logicalDELTimestamp;
        public Boolean  isPhysicalDELeted;
        public Date     physicalDELTimestamp;

        public StudyStatus ( String StudyInstanceUID )
        {
            this.studyInstanceUID              = StudyInstanceUID;
            this.numberOfSops                  = 0;
            this.lastReceivedTimestamp         = null;
            this.numberOfStgCmtAttempts        = 0;
            this.isPreftch                     = false;
            this.isPreftchChecked              = false;
            this.prefetchTimestamp             = "";
            this.prefetchSource                = "";
            this.isForwarded                   = false;
            this.forwardedTimestamp            = "";
            this.forwardedDestination          = "";
            this.numberOfForwardedSops         = 0;
            this.isStorageCommitted            = false;
            this.storageCommittedTimestamp     = "";
            this.storageCommittedObjects       = 0;
            this.storageCommittedObjectsFailed = 0;
            this.storageCommitedDestination    = "";
            this.isCATaloged                   = false;
            this.catalogTimestamp              = null;
            this.isLogicalDELeted              = false;
            this.logicalDELTimestamp           = null;
            this.isPhysicalDELeted             = false;
            this.physicalDELTimestamp          = null;
        }
    }

    public class StudyEvent {
        public int      id;
        public String   timestamp;
        public String   type;
        public String   detail;

        StudyEventAttributes attributes;

    }

    public class StudyEventAttributes {
        public int      NumberOfSops;
        public String   SendingAETitle;
        public String   ReceivingAETitle;
        public String   StgCmtTransactionUID;
        public String   ForwardedToAETitle;
        public String   NumberOfForwardedSops;
        public String   NumberOfStgCmtSops;
        public String   NumberOfFailedToStgCmtSops;
    }

    public class BinaryItem {
        public int     id;
        public int     part;
        public String  location;
        public int     offset;
        public long    size;
    }



    public static class NoStudyMetadataException extends Exception {
        public NoStudyMetadataException(String msg){
            super(msg);
        }
    }



    private static String mapFileToPhysicalLocation2 (String inFileName) throws Exception {

        Map<String,String> dataCenterToMountMap = new HashMap<String,String>();

        String composedFileName = "";

        switch (environmentVar) {

            case "TEST" :
                dataCenterToMountMap.put("DC1", "/mnt/rialto_remote_rw/archive_t/");
                dataCenterToMountMap.put("DC2", "/mnt/rialto_local_rw/archive/");
                break;

            case "PROD_NORTH" :
                dataCenterToMountMap.put("DC2", "/mnt/rialto_local_rw/archive/");
                dataCenterToMountMap.put("DC1", "/mnt/rialto_remote_rw/archive/");
                break;

            case "PROD_SOUTH" :
                dataCenterToMountMap.put("DC1", "/mnt/rialto_local_rw/archive/");
                dataCenterToMountMap.put("DC2", "/mnt/rialto_remote_rw/archive/");
                break;

            default:
                dataCenterToMountMap.put("DC1", "/mnt/rialto_local_rw/archive/");
                dataCenterToMountMap.put("DC2", "/mnt/rialto_remote_rw/archive/");
        }

        String checkDataCenter = inFileName.substring(0,3);

        if ( dataCenterToMountMap.containsKey(checkDataCenter) ) {
            composedFileName = dataCenterToMountMap.get(checkDataCenter) + inFileName.substring(4);
        } else {
            composedFileName = inFileName;
        }

        return composedFileName;

    }



    private static Boolean checkStudyMetadataFiles (String inStudyMetadataLocations) throws Exception {
        
        Boolean returnStatus = true;

        try {

            String[] mdFiles = inStudyMetadataLocations.split(",");

            for (int i = 0; i < mdFiles.length; i++ ) {

                String composedFileName = mapFileToPhysicalLocation2( mdFiles[i] );

                File f = new File(composedFileName);
            
                if ( f.exists() && !f.isDirectory() ) { 
                    if (isDebug) { System.out.println("study metadata file check passed: " + composedFileName); }

                } else {
                    System.out.println("ERROR: Failed to locate study metadata file: " + composedFileName);
                    returnStatus = false;
                }
            }

        } catch (Exception ex) {
            ex.printStackTrace();
            StringWriter errors = new StringWriter();
            ex.printStackTrace(new PrintWriter(errors));
            System.out.println ("ERROR: Failed to locate one or more study metadata files...");
            System.out.println (errors.toString());
            return false;
        }

        return returnStatus;

    }



    private static Boolean checkStudyDicomSopDescFile (String inStudyDicomSopDescFile) throws Exception {

        try {

            String composedFileName = mapFileToPhysicalLocation2( inStudyDicomSopDescFile );

            File f = new File(composedFileName);
            
            if(f.exists() && !f.isDirectory()) { 
                if (isDebug) { System.out.println("DICOM SOP Description file check passed: " + composedFileName); }
                return true;

            } else {
                System.out.println("ERROR: Failed to locate DICOM SOP Description file: " + composedFileName);
                return false;
            }

        } catch (Exception ex) {
            ex.printStackTrace();
            StringWriter errors = new StringWriter();
            ex.printStackTrace(new PrintWriter(errors));
            System.out.println ("ERROR: Failed to locate DICOM SOP Description file...");
            System.out.println (errors.toString());
            return false;
        }
    }



    private static Boolean checkStudyBinaryItemsDescFile (String inStudyBinaryItemsDescription) throws Exception {

        try {

            String composedFileName = mapFileToPhysicalLocation2( inStudyBinaryItemsDescription );

            File f = new File(composedFileName);
            
            if(f.exists() && !f.isDirectory()) { 
                if (isDebug) { System.out.println("Binary Item Description file check passed: " + composedFileName); }
                return true;

            } else {
                System.out.println("ERROR: Failed to locate Binary Item Description file: " + composedFileName);
                return false;
            }

        } catch (Exception ex) {
            ex.printStackTrace();
            StringWriter errors = new StringWriter();
            ex.printStackTrace(new PrintWriter(errors));
            System.out.println ("ERROR: Failed to locate Binary Item Description file....");
            System.out.println (errors.toString());
            return false;
        }
    }


// JSON Libraries

// Library : json-simple
// Docs    : http://juliusdavies.ca/json-simple-1.1.1-javadocs/index.html
//           http://stackoverflow.com/questions/10926353/how-to-read-json-file-into-java-with-simple-json-library

// library : JSON-Java
// Docs    : http://stleary.github.io/JSON-java/index.html
//           https://github.com/stleary/JSON-java
// Download: https://search.maven.org/#search%7Cgav%7C1%7Cg%3A%22org.json%22%20AND%20a%3A%22json%22

// http://www.javacreed.com/gson-deserialiser-example/
//JSONParser parser = new JSONParser();

    private static String[] getListOfTarballsFromBin (String inStudyBinaryItemsDescription) throws Exception {
        // parse study Binary Items Description file and compile a unique list of TAR archives

        Set<String> treeSet = new TreeSet<String>();

        try {

            String composedFileName = mapFileToPhysicalLocation2( inStudyBinaryItemsDescription );

            String jsonTxt = IOUtils.toString(new FileInputStream(composedFileName), "UTF-8");

            JSONObject root = new JSONObject(jsonTxt);

            JSONArray binItems = root.getJSONArray("binaryItems");

            for(int i = 0; i < binItems.length(); i++) {

                JSONObject binItem = binItems.getJSONObject(i);
                String tarFileName = binItem.getString("location");
                if (isDebug) { System.out.println( "DEBUG: tarball name: " + tarFileName ); }
                treeSet.add( tarFileName );
            }

        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        } catch (JSONException e) {
            e.printStackTrace();
        }

        return treeSet.toArray(new String[treeSet.size()]);

    }



    private static String[] getListOfTarballs (String inStudyMetadata) throws Exception {
        
        // parse study metadata XML and compile a unique list of TAR archives
        Set<String> treeSet = new TreeSet<String>();

        try {

            DocumentBuilderFactory dbFactory = DocumentBuilderFactory.newInstance();
            DocumentBuilder dBuilder;

            dBuilder = dbFactory.newDocumentBuilder();

            InputSource is = new InputSource(new StringReader(inStudyMetadata));
            Document doc = dBuilder.parse(is);
            doc.getDocumentElement().normalize();

            XPath xPath =  XPathFactory.newInstance().newXPath();

            String expression = "/study/seriesList/series/instances/instance/attributes/attr[@tag='0040E010']";
            NodeList nodeList = (NodeList) xPath.compile(expression).evaluate(doc, XPathConstants.NODESET);
            for (int i = 0; i < nodeList.getLength(); i++) {
                Node nNode = nodeList.item(i);
                //System.out.println("\nCurrent Element :" + nNode.getNodeName());
                if (nNode.getNodeType() == Node.ELEMENT_NODE) {
                    Element eElement = (Element) nNode;
                    String rawTarballName = eElement.getAttribute("val");
                    if (isDebug) { System.out.println( "tarball name: " + rawTarballName.substring( 0, rawTarballName.indexOf(',')) ); }
                    treeSet.add( rawTarballName.substring( 0, rawTarballName.indexOf(',')) );
                }
            }

        } catch (ParserConfigurationException e) {
            e.printStackTrace();
        } catch (SAXException e) {
            e.printStackTrace();
        } catch (XPathExpressionException e) {
            e.printStackTrace();
        }

        return treeSet.toArray(new String[treeSet.size()]);

    }



    private static Boolean checkTarballs (String[] tarballList) throws Exception {
        // check that each tar ball is present on disk

        Boolean returnStatus = true;

        for (int i = 0; i < tarballList.length; i++ ) {

            if (isDebug) { System.out.println("DEBUG: about to parse tarball: " + tarballList[i]); }
            String composedFileName = mapFileToPhysicalLocation2( tarballList[i] );

            if (isDebug) { System.out.println("DEBUG: about to check file: " + composedFileName); }

            File f = new File(composedFileName);
            
            if(f.exists() && !f.isDirectory()) { 
                //System.out.println("tarball check passed: " + composedFileName);

            } else {
                System.out.println("FAILED TO LOCATE tarball: " + composedFileName);
                returnStatus = false;
            }
        }

        return returnStatus;

    }



    private static void printTarballs (String[] tarballList) throws Exception {

        for (int i = 0; i < tarballList.length; i++ ) {
            System.out.println( mapFileToPhysicalLocation2( tarballList[i] ) );
        }
    }



    private static void printMetadataFiles (String inStudyMetadataLocations) throws Exception {

        String[] mdFiles = inStudyMetadataLocations.split(",");

        for (int i = 0; i < mdFiles.length; i++ ) {
            System.out.println( mapFileToPhysicalLocation2( mdFiles[i] ) );
        }
    }



    private static void printRemoveFilesTreeSet (String[] fileList) throws Exception {

        for (int i = 0; i < fileList.length; i++ ) {
            System.out.println( "rm -rf " + mapFileToPhysicalLocation2( fileList[i] ) );
        }
    }



    private static void GenerateSelectStatements (
         String         inStudyInstanceUID
        ,Set<String>    inLastUpdatedList
        ,String         inStudyID
        ,java.util.UUID inStudyUUID
        ,String         inStudyUUIDStr
        ,String         inStudyDateTime
        ,String         inDomainName
        ,String         inDomainUniversalID
        ,String         inDomainUniversalIDType
        ,String         inPatientID
        ,String         inPatientName
        ,String         inAccessionNumber
    )
    throws Exception {

        String studyTimelineTS = new String("");

        // generate a list of select statement that should be executed against Cassandra database to cleanup all the residual data
        //System.out.println (inStudyInstanceUID + ": generate a list of select statement that should be executed against Cassandra database to cleanup all the residual data ...");

        // select distinct index_name, month from new_date_time_index;
        // study_date_time
        // unverified_study_date_time
        // last_updated
        if (inStudyDateTime != null) {
            System.out.println ("SELECT * FROM new_date_time_index where index_name = 'study_date_time' and month = '" + inStudyDateTime.substring(5,7) + "' and date_study_instance_uid='" + inStudyDateTime + "_" + inStudyInstanceUID + "' and study_instance_uid = '" + inStudyInstanceUID + "';");
            System.out.println ("SELECT * FROM new_date_time_index where index_name = 'unverified_study_date_time' and month = '" + inStudyDateTime.substring(5,7) + "' and date_study_instance_uid='" + inStudyDateTime + "_" + inStudyInstanceUID + "' and study_instance_uid = '" + inStudyInstanceUID + "';");
        } else {
            System.out.println ("WARNING: StudyDateTime is null!!! Will not generate SELECT statements for new_date_time_index....");
        }

        Iterator<String> iterator = inLastUpdatedList.iterator();
        while (iterator.hasNext()) {
            studyTimelineTS = iterator.next();
            System.out.println ("SELECT * FROM new_date_time_index where index_name = 'last_updated' and month = '" + studyTimelineTS.substring(5,7) + "' and date_study_instance_uid='" + studyTimelineTS + "_" + inStudyInstanceUID + "' and study_instance_uid = '" + inStudyInstanceUID + "';");
        }

        System.out.println ("SELECT * FROM new_study_study_uuid_didx where study_uuid = " + inStudyUUID + " and study_instance_uid = '" + inStudyInstanceUID + "';");

        // select distinct index_name, first_char from new_study_text_index;
        if (inAccessionNumber != null ) {
            System.out.println ("SELECT * FROM new_study_text_index where index_name = 'accession_no' and first_char = '" + inAccessionNumber.substring(0,1) + "' and text = '" + inAccessionNumber + "' and study_instance_uid = '" + inStudyInstanceUID + "';");
        }
        System.out.println ("SELECT * FROM new_study_text_index where index_name = 'patient_id' and first_char = '" + inPatientID.substring(0,1) + "' and text = '" + inPatientID + "' and study_instance_uid = '" + inStudyInstanceUID + "';");
        System.out.println ("SELECT * FROM new_study_text_index where index_name = 'patient_name' and first_char = '" + inPatientName.substring(0,1) + "' and text = '" + inPatientName.replaceAll("'","''") + "' and study_instance_uid = '" + inStudyInstanceUID + "';");
        System.out.println ("SELECT * FROM new_study_text_index where index_name = 'patient_name' and first_char = '" + inPatientName.substring(0,1).toUpperCase() + "' and text = '" + inPatientName.replaceAll("'","''").toUpperCase() + "' and study_instance_uid = '" + inStudyInstanceUID + "';");

        if (inStudyID != null) {
            System.out.println ("SELECT * FROM new_study_text_index where index_name = 'study_id' and first_char = '" + inStudyID.substring(0,1) + "' and text = '" + inStudyID + "' and study_instance_uid = '" + inStudyInstanceUID + "';");
        }

        // select distinct index_name, identifier, first_char from new_study_text_index_with_identifier;
        if (inDomainUniversalID != null && inDomainUniversalIDType != null) {
            System.out.println ("SELECT * FROM new_study_text_index_with_identifier where index_name = 'qualified_patient_id' and identifier = '" + inDomainUniversalID + "&" + inDomainUniversalIDType + "' and first_char = '" + inPatientID.substring(0,1) + "' and text = '" + inPatientID + "' and study_instance_uid = '" + inStudyInstanceUID + "';");
        }

        System.out.println ("SELECT * FROM new_study where study_instance_uid = '" + inStudyInstanceUID + "';");

    }



    private static void GenerateDeleteStatements (
         String         inStudyInstanceUID
        ,Set<String>    inLastUpdatedList
        ,String         inStudyID
        ,java.util.UUID inStudyUUID
        ,String         inStudyUUIDStr
        ,String         inStudyDateTime
        ,String         inDomainName
        ,String         inDomainUniversalID
        ,String         inDomainUniversalIDType
        ,String         inPatientID
        ,String         inPatientName
        ,String         inAccessionNumber
    )
    throws Exception {

        String studyTimelineTS = new String("");

        // generate a list of detele statement that should be executed against Cassandra database to cleanup all the residual data
        //System.out.println (inStudyInstanceUID + ": generate a list of detele statement that should be executed against Cassandra database to cleanup all the residual data ...");

        // select distinct index_name, month from new_date_time_index;
        // study_date_time
        // unverified_study_date_time
        // last_updated
        // DELETE FROM new_date_time_index where index_name = 'study_date_time' and month = '11' and date_study_instance_uid='2012-11-23T23:55:15.000Z_1.2.392.200036.9116.2.6.1.48.1211449053.1353664489.527799' and study_instance_uid = '1.2.392.200036.9116.2.6.1.48.1211449053.1353664489.527799';
        if (inStudyDateTime != null) {
            System.out.println ("DELETE FROM new_date_time_index where index_name = 'study_date_time' and month = '" + inStudyDateTime.substring(5,7) + "' and date_study_instance_uid='" + inStudyDateTime + "_" + inStudyInstanceUID + "' and study_instance_uid = '" + inStudyInstanceUID + "';");
            System.out.println ("DELETE FROM new_date_time_index where index_name = 'unverified_study_date_time' and month = '" + inStudyDateTime.substring(5,7) + "' and date_study_instance_uid='" + inStudyDateTime + "_" + inStudyInstanceUID + "' and study_instance_uid = '" + inStudyInstanceUID + "';");
        } else {
            System.out.println ("WARNING: StudyDateTime is null!!! Will not generate DELETE statements for new_date_time_index....");
        }

        Iterator<String> iterator = inLastUpdatedList.iterator();
        while (iterator.hasNext()) {
            studyTimelineTS = iterator.next();
            System.out.println ("DELETE FROM new_date_time_index where index_name = 'last_updated' and month = '" + studyTimelineTS.substring(5,7) + "' and date_study_instance_uid='" + studyTimelineTS + "_" + inStudyInstanceUID + "' and study_instance_uid = '" + inStudyInstanceUID + "';");
        }

        // DELETE FROM new_study_study_uuid_didx where study_uuid = 116ecace-183e-4133-98e8-94bf7ffd21ad and study_instance_uid = '1.2.392.200036.9116.2.6.1.48.1211449053.1353664489.527799';
        System.out.println ("DELETE FROM new_study_study_uuid_didx where study_uuid = " + inStudyUUID + " and study_instance_uid = '" + inStudyInstanceUID + "';");

        // select distinct index_name, first_char from new_study_text_index;
        // DELETE FROM new_study_text_index where index_name = 'patient_name' and first_char = 'O' and text = 'O''DONNELL^CAROLYN^D' and study_instance_uid = '1.2.840.113711.7041813.1.6224.227552347.26.2116281012.110550';
        if (inAccessionNumber != null ) {
            System.out.println ("DELETE FROM new_study_text_index where index_name = 'accession_no' and first_char = '" + inAccessionNumber.substring(0,1) + "' and text = '" + inAccessionNumber + "' and study_instance_uid = '" + inStudyInstanceUID + "';");
        }
        System.out.println ("DELETE FROM new_study_text_index where index_name = 'patient_id' and first_char = '" + inPatientID.substring(0,1) + "' and text = '" + inPatientID + "' and study_instance_uid = '" + inStudyInstanceUID + "';");
        System.out.println ("DELETE FROM new_study_text_index where index_name = 'patient_name' and first_char = '" + inPatientName.substring(0,1) + "' and text = '" + inPatientName.replaceAll("'","''") + "' and study_instance_uid = '" + inStudyInstanceUID + "';");
        System.out.println ("DELETE FROM new_study_text_index where index_name = 'patient_name' and first_char = '" + inPatientName.substring(0,1).toUpperCase() + "' and text = '" + inPatientName.replaceAll("'","''").toUpperCase() + "' and study_instance_uid = '" + inStudyInstanceUID + "';");

        if (inStudyID != null) {
            System.out.println ("DELETE FROM new_study_text_index where index_name = 'study_id' and first_char = '" + inStudyID.substring(0,1) + "' and text = '" + inStudyID + "' and study_instance_uid = '" + inStudyInstanceUID + "';");
        }

        // select distinct index_name, identifier, first_char from new_study_text_index_with_identifier;
        // DELETE FROM new_study_text_index_with_identifier where index_name = 'qualified_patient_id' and identifier = '2.16.840.1.114107.1.1.16.2.10&ISO' and first_char = '1' and text = '102185' and study_instance_uid = '1.2.392.200036.9116.2.6.1.48.1211449053.1353664489.527799';
        if (inDomainUniversalID != null && inDomainUniversalIDType != null) {
            System.out.println ("DELETE FROM new_study_text_index_with_identifier where index_name = 'qualified_patient_id' and identifier = '" + inDomainUniversalID + "&" + inDomainUniversalIDType + "' and first_char = '" + inPatientID.substring(0,1) + "' and text = '" + inPatientID + "' and study_instance_uid = '" + inStudyInstanceUID + "';");
        }

        // DELETE FROM new_study where study_instance_uid = '1.2.392.200036.9116.2.6.1.48.1211449053.1353664489.527799';
        System.out.println ("DELETE FROM new_study where study_instance_uid = '" + inStudyInstanceUID + "';");

    }



    public static void main(String[] args) {

        int StudyTotalCount = 0;
        int StudyFailedCount = 0;

        if (args[0] == null || args[0].trim().isEmpty()) {
            System.out.println("You need to specify a path!");
            return;
        }

        
        // define a list of IMGs that would qualify studies as prefetch
        HashSet<String> prefetchSources = new HashSet<String>();

        // add elements to the hash set
        prefetchSources.add("indexQuery-P0JG");
        prefetchSources.add("indexQuery-P0RA");
        prefetchSources.add("indexQuery-P0RB");
        prefetchSources.add("indexQuery-P005");
        prefetchSources.add("indexQuery-P0T7");
        prefetchSources.add("indexQuery-P0M5");
        prefetchSources.add("indexQuery-P0HS");
        prefetchSources.add("GSHINSITE02");
        prefetchSources.add("indexQuery-P0H2");
        prefetchSources.add("FSHINSITE02");
        prefetchSources.add("indexQuery-P0M2");

        Map<String,String> domainToPrefetchSouceMap = new HashMap<String,String>();
        domainToPrefetchSouceMap.put("HHC",   "indexQuery-P0HS@172.16.209.107:14444");
        domainToPrefetchSouceMap.put("FSH",   "FSHINSITE02@172.23.8.65:14444");
        domainToPrefetchSouceMap.put("UMH",   "indexQuery-P0H2@172.17.80.79:14444");
        domainToPrefetchSouceMap.put("GSH",   "GSHINSITE02@172.16.80.66:14444");
        domainToPrefetchSouceMap.put("WB4",   "indexQuery-P005@172.25.120.25:14444");
        domainToPrefetchSouceMap.put("6B4",   "indexQuery-P005@172.25.120.25:14444");
        domainToPrefetchSouceMap.put("MGI",   "indexQuery-P0RA@172.18.7.202:14444");
        domainToPrefetchSouceMap.put("MSMH",  "indexQuery-P0RB@172.30.8.50:14444");
        domainToPrefetchSouceMap.put("MSMHC", "indexQuery-P0T7@198.50.67.28:14444");
        domainToPrefetchSouceMap.put("MPP",   "indexQuery-P0T7@198.50.67.28:14444");
        domainToPrefetchSouceMap.put("MSC",   "indexQuery-P0T7@198.50.67.28:14444");
        domainToPrefetchSouceMap.put("SHAH",  "indexQuery-P0T7@198.50.67.28:14444");
        domainToPrefetchSouceMap.put("GUH",   "indexQuery-P005@172.25.120.25:14444");

        Cluster cluster;

        switch (environmentVar) {

            case "TEST" :
                cluster = Cluster.builder().addContactPoint("127.0.0.1").build();
                break;

            case "PROD_NORTH" :
                cluster = Cluster.builder().addContactPoint("172.16.203.65").build();
                break;

            case "PROD_SOUTH" :
                cluster = Cluster.builder().addContactPoint("172.27.6.43").build();
                break;

            default:
                cluster = Cluster.builder().addContactPoint("172.27.6.43").build();
        }

        Session session = cluster.connect("medstar_imagearchive");


        String selectStmt = "SELECT timeline_event, study_date_time, metadata_locations, dicom_sops_desc_location, binary_items_desc_location FROM new_study where study_instance_uid = ?";
        PreparedStatement prepSelectStmt = session.prepare(selectStmt);

        //String updateStmt = "UPDATE new_study SET is_verified = true WHERE study_instance_uid = ? and study_version = ?";
        //PreparedStatement prepUpdateStmt = session.prepare(updateStmt);

        String studyDetailsStmt = "SELECT aa_namespace_id, aa_universal_id, aa_universal_id_type, accession_number, patient_id, patient_name, study_date_time, study_id, study_uuid FROM new_study where study_instance_uid = ? LIMIT 1";
        PreparedStatement prepStudyDetailsStmt = session.prepare(studyDetailsStmt);

        FileInputStream fis = null;
        BufferedReader br = null;

        Gson gson = new Gson();

        // 2016-10-28T14:04:03.660Z
        //SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss");
        //SimpleDateFormat output = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        //Date d = sdf.parse(time);
        //String formattedTime = output.format(d);

        DateFormat sdf = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss.SSS'Z'");

        try {

            fis = new FileInputStream(args[0]);
            br  = new BufferedReader(new InputStreamReader(fis));

            String studyInstanceUID;
            String studyDateTime                  = new String("");
            String studyTimeline                  = new String("");
            String studyMostRecentState           = new String("");

            String studyID                        = new String("");
            java.util.UUID studyUUID              = new UUID(0L, 0L);
            String studyUUIDStr                   = new String("");

            String domainName                     = new String("");
            String domainUniversalID              = new String("");
            String domainUniversalIDType          = new String("");
            String patientID                      = new String("");
            String patientName                    = new String("");
            String accessionNumber                = new String("");

            String studyMetadataXML               = new String("");
            String studyMetadataLocations         = new String("");
            String studyDicomSopDescFile          = new String("");
            String studyBinaryItemsDescFile       = new String("");
            String[] studyTarballs                = new String[0];

            Set<String> treeTarballListTotal        = new TreeSet<String>();
            Set<String> treeStudyMetadataListTotal  = new TreeSet<String>();
            Set<String> treeDicomSopDescListTotal   = new TreeSet<String>();
            Set<String> treeBinItemsDescListTotal   = new TreeSet<String>();

            Set<String> lastUpdatedList           = new TreeSet<String>();

            ArrayList<String> studyLogLinesAll    = new ArrayList<String>();
            ArrayList<String> studyLogLinesTars   = new ArrayList<String>();
            ArrayList<String> studyLogLinesSelect = new ArrayList<String>();
            ArrayList<String> studyLogLinesDelete = new ArrayList<String>();
            ArrayList<String> studyLogLinesDcmqr  = new ArrayList<String>();

            Boolean isCleanupRequired;
            Boolean isFailedToProcess;

            String line = "";
            String cvsSplitBy = ",";

            while ((line = br.readLine()) != null) {

                String[] study_info = line.split(cvsSplitBy);
                studyInstanceUID = study_info[0];

                studyDateTime               = "";
                studyTimeline               = "";
                studyMostRecentState        = "";
                studyID                     = "";
                studyUUID                   = new UUID(0L, 0L);
                studyUUIDStr                = "";
                domainName                  = "";
                domainUniversalID           = "";
                domainUniversalIDType       = "";
                patientID                   = "";
                patientName                 = "";
                accessionNumber             = "";
                studyMetadataXML            = "";
                studyMetadataLocations      = "";
                studyDicomSopDescFile       = "";
                studyBinaryItemsDescFile    = "";
                studyTarballs               = new String[0];

                treeTarballListTotal        = new TreeSet<String>();
                treeStudyMetadataListTotal  = new TreeSet<String>();
                treeDicomSopDescListTotal   = new TreeSet<String>();
                treeBinItemsDescListTotal   = new TreeSet<String>();
                
                lastUpdatedList             = new TreeSet<String>();
                isCleanupRequired           = false;
                isFailedToProcess           = false;

                StudyStatus myStudyStatus = new StudyStatus(studyInstanceUID);
                //myStudyStatus.reset(studyInstanceUID);
                System.out.println ("Processing StudyInstanceUID: " + studyInstanceUID);
                StudyTotalCount++;

                try {
                    //System.out.println ("About to select timeline_event from new_study table...");
                    BoundStatement boundStmt = new BoundStatement(prepSelectStmt);
                    boundStmt.setConsistencyLevel(ConsistencyLevel.LOCAL_QUORUM);
                    boundStmt.bind(studyInstanceUID);

                    ResultSet results = session.execute(boundStmt);

                    while (!results.isExhausted()) {
                        Row row = results.one();
                        studyTimeline               = row.getString("timeline_event");
                        studyDateTime               = row.getString("study_date_time");
                        studyMetadataLocations      = row.getString("metadata_locations");
                        studyDicomSopDescFile       = row.getString("dicom_sops_desc_location");
                        studyBinaryItemsDescFile    = row.getString("binary_items_desc_location");
                        StudyEvent myStudyEvent     = gson.fromJson(studyTimeline, StudyEvent.class);

                        lastUpdatedList.add(myStudyEvent.timestamp);

                        //System.out.println ("DEBUG: Study timeline :" + studyTimeline);
                        //String backtoJSON = gson.toJson(myStudyEvent);
                        //System.out.println ("DEBUG: back to JSON: " + backtoJSON);

                        // proceed with checking metadata and DICOM files only if study was NOT physically DELeted
                        if ( !myStudyStatus.isPhysicalDELeted && !myStudyStatus.isCATaloged ) {

                            if (myStudyEvent.type.equals("STG_CMT_RSP")) {
                                System.out.println (studyInstanceUID + ": " + myStudyEvent.timestamp + ": received Storage Commit Response from " + myStudyEvent.attributes.ForwardedToAETitle + ", stored " + myStudyEvent.attributes.NumberOfStgCmtSops + " images, failed to store " + myStudyEvent.attributes.NumberOfFailedToStgCmtSops);
                                if (!myStudyStatus.isStorageCommitted) {
                                    myStudyStatus.isStorageCommitted = true;
                                    myStudyStatus.storageCommittedTimestamp = myStudyEvent.timestamp;
                                    myStudyStatus.storageCommittedObjects = Integer.parseInt(myStudyEvent.attributes.NumberOfStgCmtSops);
                                    myStudyStatus.storageCommittedObjectsFailed =  Integer.parseInt(myStudyEvent.attributes.NumberOfFailedToStgCmtSops);
                                    myStudyStatus.storageCommitedDestination = myStudyEvent.attributes.ForwardedToAETitle;
                                }
                            }

                            else if (myStudyEvent.type.equals("FWD")) {
                                System.out.println (studyInstanceUID + ": " + myStudyEvent.timestamp + ": forwarded " + myStudyEvent.attributes.NumberOfForwardedSops + " to " + myStudyEvent.attributes.ForwardedToAETitle);
                                if (!myStudyStatus.isForwarded) {
                                    myStudyStatus.isForwarded = true;
                                    myStudyStatus.forwardedTimestamp = myStudyEvent.timestamp;
                                    myStudyStatus.forwardedDestination = myStudyEvent.attributes.ForwardedToAETitle;
                                    myStudyStatus.numberOfForwardedSops = Integer.parseInt(myStudyEvent.attributes.NumberOfForwardedSops);
                                }
                            }

                            else if (myStudyEvent.type.equals("IMG")) {
                            
                                if (studyMostRecentState == "") { studyMostRecentState = "IMG"; }
                                // NOTE: this number might not be a complete number of SOPs that study has
                                myStudyStatus.numberOfSops = myStudyEvent.attributes.NumberOfSops;
                                myStudyStatus.lastReceivedTimestamp = sdf.parse(myStudyEvent.timestamp);

                                if (myStudyEvent.attributes.SendingAETitle != null) {

                                    System.out.println (studyInstanceUID + ": " + myStudyEvent.timestamp + ": received " + myStudyEvent.attributes.NumberOfSops + " image(s) from " + myStudyEvent.attributes.SendingAETitle);

                                    if(!myStudyStatus.isPreftchChecked) {
                                        // check the SendingAETitle and if in list of IMGs mark as a prefetch
                                        if (prefetchSources.contains(myStudyEvent.attributes.SendingAETitle)) {
                                            myStudyStatus.isPreftchChecked = true;
                                            myStudyStatus.isPreftch = true;
                                            myStudyStatus.prefetchTimestamp = myStudyEvent.timestamp;
                                            myStudyStatus.prefetchSource = myStudyEvent.attributes.SendingAETitle;
                                        }
                                    }

                                } else {
                                    System.out.println (studyInstanceUID + ": " + myStudyEvent.timestamp + ": WARNING sending AETitle is null!!!");
                                }
                            }

                            else if (myStudyEvent.type.equals("CAT")) {
                                if (studyMostRecentState == "") { studyMostRecentState = "CAT"; }
                                System.out.println (studyInstanceUID + ": " + myStudyEvent.timestamp + ": study was CATALOGed");
                                if (!myStudyStatus.isCATaloged) {
                                    myStudyStatus.isCATaloged = true;
                                    myStudyStatus.catalogTimestamp = sdf.parse(myStudyEvent.timestamp);
                                }
                            }

                            else if (myStudyEvent.type.equals("DEL")) {
                                if (studyMostRecentState == "") { studyMostRecentState = "DEL"; }
                                System.out.println (studyInstanceUID + ": " + myStudyEvent.timestamp + ": study was logically deleted");
                                if (!myStudyStatus.isLogicalDELeted ) {
                                    myStudyStatus.isLogicalDELeted = true;
                                    myStudyStatus.logicalDELTimestamp = sdf.parse(myStudyEvent.timestamp);
                                }
                            }

                            else if (myStudyEvent.type.equals("HDEL")) {
                                if (studyMostRecentState == "") { studyMostRecentState = "HDEL"; }
                                System.out.println (studyInstanceUID + ": " + myStudyEvent.timestamp + ": study was physically deleted");
                                if (!myStudyStatus.isPhysicalDELeted) {
                                    myStudyStatus.isPhysicalDELeted = true;
                                    myStudyStatus.physicalDELTimestamp = sdf.parse(myStudyEvent.timestamp);
                                }
                            }

                            // proceed with checking metadata and DICOM files only if study was NOT physically DELeted
                            if (!myStudyStatus.isPhysicalDELeted) {

                                if(myStudyStatus.isCATaloged) {
                                
                                    // for CATALOGed studies check study metadata and only if the current version type is CAT
                                    if(myStudyEvent.type.equals("CAT")) {

                                        if (isDebug) { System.out.println ("DEBUG: Study metadata locations: " + studyMetadataLocations); }
                                    
                                        String[] mdFiles = studyMetadataLocations.split(",");
                                        for (int i = 0; i < mdFiles.length; i++ ) {
                                            treeStudyMetadataListTotal.add( mdFiles[i] );
                                        }

                                        // check all metadata files
                                        if (!checkStudyMetadataFiles(studyMetadataLocations)) {
                                            isCleanupRequired = true;
                                        }
                                    }

                                } else {

                                    if (isDebug) { System.out.println ("DEBUG: Study metadata locations: " + studyMetadataLocations); }
                                    if (isDebug) { System.out.println ("DEBUG: Study DICOM SOP Description File: " + studyDicomSopDescFile); }
                                    if (isDebug) { System.out.println ("DEBUG: Study Binary Items Description File: " + studyBinaryItemsDescFile); }

                                    // add files to their corresponding TreeSets
                                    String[] mdFiles = studyMetadataLocations.split(",");
                                    for (int i = 0; i < mdFiles.length; i++ ) {
                                        treeStudyMetadataListTotal.add( mdFiles[i] );
                                    }

                                    treeDicomSopDescListTotal.add(studyDicomSopDescFile);
                                    treeBinItemsDescListTotal.add(studyBinaryItemsDescFile);

                                    for (int i = 0; i < studyTarballs.length; i++ ) {
                                        treeTarballListTotal.add(studyTarballs[i]);
                                    }


                                    // check all metadata files
                                    if (!checkStudyMetadataFiles(studyMetadataLocations)) {
                                        isCleanupRequired = true;
                                    }

                                    // check DICOM SOP Descritpion file
                                    if (!checkStudyDicomSopDescFile(studyDicomSopDescFile)) {
                                        isCleanupRequired = true;
                                    }
                            
                                    // check Binary Item Description file
                                    if (!checkStudyBinaryItemsDescFile(studyBinaryItemsDescFile)) {
                                        isCleanupRequired = true;
                                    }

                                    studyTarballs = getListOfTarballsFromBin(studyBinaryItemsDescFile);

                                    // check all tar balls
                                    if (!checkTarballs(studyTarballs)) {
                                        isCleanupRequired = true;
                                    }
                                }
                            }
                        }
                    }

                } catch (Exception ex) {
                    ex.printStackTrace();
                    StringWriter errors = new StringWriter();
                    System.out.println (errors.toString());
                    System.out.println (studyInstanceUID + ": ERROR: " + "Failed to extract timeline...");
                    ex.printStackTrace(new PrintWriter(errors));
                    isCleanupRequired = false;
                    isFailedToProcess = true;
                }

                //isCleanupRequired = false;
                if (isCleanupRequired) {

                    try {

                        // generate a list of files that should be removed from shared storage (that should include json, metadata, TAR balls)
                        //System.out.println (studyInstanceUID + ": generate list of files that should be removed from shared storage ...");
                        if ( studyTarballs != null && studyTarballs.length != 0) {
                            System.out.println ("==================================================================");
                            System.out.println ("==");
                            System.out.print(studyInstanceUID + ": " + "Following files should be backed up and removed from shared storage: \n");
                            printRemoveFilesTreeSet( treeStudyMetadataListTotal.toArray(new String[treeStudyMetadataListTotal.size()]) ) ;
                            printRemoveFilesTreeSet( treeDicomSopDescListTotal.toArray(new String[treeDicomSopDescListTotal.size()]) ) ;
                            printRemoveFilesTreeSet( treeBinItemsDescListTotal.toArray(new String[treeBinItemsDescListTotal.size()]) ) ;
                            printRemoveFilesTreeSet( treeTarballListTotal.toArray(new String[treeTarballListTotal.size()]) ) ;
                            

                            System.out.println ("==");
                            System.out.println ("==================================================================");
                            System.out.println ("==");
                        } 
                        else {
                            System.out.println (studyInstanceUID + ": WARNING: " + "Unable to find any TAR archives...");
                        }

                        // retrieve study details
                        BoundStatement boundStmt2 = new BoundStatement(prepStudyDetailsStmt);
                        boundStmt2.setConsistencyLevel(ConsistencyLevel.LOCAL_QUORUM);
                        boundStmt2.bind(studyInstanceUID);

                        ResultSet results2 = session.execute(boundStmt2);

                        Iterator<Row> iterator = results2.iterator();
                        if (iterator.hasNext()) {
                            Row row2 = iterator.next();

                            domainName            = row2.getString("aa_namespace_id");
                            domainUniversalID     = row2.getString("aa_universal_id");
                            domainUniversalIDType = row2.getString("aa_universal_id_type");
                            accessionNumber       = row2.getString("accession_number");
                            patientID             = row2.getString("patient_id");
                            patientName           = row2.getString("patient_name");
                            studyDateTime         = row2.getString("study_date_time");
                            studyID               = row2.getString("study_id");
                            studyUUID             = row2.getUUID("study_uuid");
                            studyUUIDStr          = studyUUID.toString();

                            if (studyInstanceUID == null || studyInstanceUID == "")           { System.out.println (studyInstanceUID + ": studyInstanceUID is null or empty..."); }
                            if (domainName == null || domainName == "")                       { System.out.println (studyInstanceUID + ": domainName is null or empty..."); }
                            if (domainUniversalID == null || domainUniversalID == "")         { System.out.println (studyInstanceUID + ": domainUniversalID is null or empty..."); }
                            if (domainUniversalIDType == null || domainUniversalIDType == "") { System.out.println (studyInstanceUID + ": domainUniversalIDType is null or empty..."); }
                            if (accessionNumber == null || accessionNumber == "")             { System.out.println (studyInstanceUID + ": accessionNumber is null or empty..."); }
                            if (patientID == null || patientID == "")                         { System.out.println (studyInstanceUID + ": patientID is null or empty..."); }
                            if (patientName == null || patientName == "")                     { System.out.println (studyInstanceUID + ": patientName is null or empty..."); }
                            if (studyDateTime == null || studyDateTime == "")                 { System.out.println (studyInstanceUID + ": studyDateTime is null or empty..."); }
                            if (studyUUIDStr == null || studyUUIDStr == "")                   { System.out.println (studyInstanceUID + ": studyUUIDStr is null or empty..."); }

                            System.out.println (studyInstanceUID + ": use the following SELECT statements to verify what should be removed from Cassandra DB");
                            System.out.println ("==================================================================");
                            GenerateSelectStatements (
                                 studyInstanceUID
                                ,lastUpdatedList
                                ,studyID
                                ,studyUUID
                                ,studyUUIDStr
                                ,studyDateTime
                                ,domainName
                                ,domainUniversalID
                                ,domainUniversalIDType
                                ,patientID
                                ,patientName
                                ,accessionNumber
                            );
                            System.out.println ("==================================================================");
                            System.out.println ("==");

                            System.out.println (studyInstanceUID + ": execute the following DELETE statements to cleanup Cassandra DB and remove any information about this study");
                            System.out.println ("==================================================================");
                            GenerateDeleteStatements(
                                 studyInstanceUID
                                ,lastUpdatedList
                                ,studyID
                                ,studyUUID
                                ,studyUUIDStr
                                ,studyDateTime
                                ,domainName
                                ,domainUniversalID
                                ,domainUniversalIDType
                                ,patientID
                                ,patientName
                                ,accessionNumber
                            );
                            System.out.println ("==================================================================");
                            System.out.println ("==");
                        }
                        else {
                            System.out.print(studyInstanceUID + ": WARNING: " + "Unable to fetch study details...");
                        }
                    } catch (Exception ex) {
                        ex.printStackTrace();
                        
                        StringWriter errors = new StringWriter();
                        ex.printStackTrace(new PrintWriter(errors));
                        System.out.println (studyInstanceUID + ": ERROR: " + "Failed to generate SELECT/DELETE statements...");
                        System.out.println (errors.toString());
                        isFailedToProcess = true;
                    }

                    // query remote LTA/IMG and generate a C-Find command to check if this study exists
                    //System.out.println (studyInstanceUID + ": about to generate C-Find commands...");
                    System.out.println ("==");
                    try {

                        if ( domainToPrefetchSouceMap.containsKey(domainName) ) {
                            System.out.println("dcmqr " + domainToPrefetchSouceMap.get(domainName) + " -qStudyInstanceUID=" + studyInstanceUID);
                        } else {
                            System.out.println(studyInstanceUID + ": WARNING: " + "unable to determine Prefetch Source for Domain = " + domainName);
                        }

                    } catch (Exception ex) {
                        ex.printStackTrace();

                        StringWriter errors = new StringWriter();
                        ex.printStackTrace(new PrintWriter(errors));
                        System.out.println (studyInstanceUID + ": ERROR: " + "Failed to generate C-Find command for StudyInstanceUID: " + studyInstanceUID);
                        System.out.println (errors.toString());
                        isFailedToProcess = true;
                    }


                    // query remote LTA/IMG and generate a C-Move command to bring this study back into Cache
                    //System.out.println (studyInstanceUID + ": about to generate C-Move commands...");
                    try {

                        if ( domainToPrefetchSouceMap.containsKey(domainName) ) {
                            //dcmqr indexQuery-P0HS@172.16.209.107:14444 -cmove RIALTO_CACHE -qStudyInstanceUID=$siuid
                            System.out.println("dcmqr " + domainToPrefetchSouceMap.get(domainName) + " -cmove RIALTO_CACHE -qStudyInstanceUID=" + studyInstanceUID);
                            
                            // if you need to bring this study to TEST Cache
                            //sleep 60
                            //dcmqr RIALTO_CACHE@172.27.6.17:4104 -cmove RIALTO_CACHE_T -qStudyInstanceUID=$siuid

                            if (environmentVar == "TEST") {
                                System.out.println("sleep 60");
                                System.out.println("dcmqr RIALTO_CACHE@172.27.6.17:4104 -cmove RIALTO_CACHE_T -qStudyInstanceUID=" + studyInstanceUID);
                            }

                        } else {
                            System.out.println(studyInstanceUID + ": WARNING: " + "unable to determine Prefetch Source for Domain = " + domainName);
                        }

                    } catch (Exception ex) {
                        ex.printStackTrace();

                        StringWriter errors = new StringWriter();
                        ex.printStackTrace(new PrintWriter(errors));
                        System.out.println (studyInstanceUID + ": ERROR: " + "Failed to generate C-Move command for StudyInstanceUID: " + studyInstanceUID);
                        System.out.println (errors.toString());
                        isFailedToProcess = true;
                    }
                    System.out.println ("==");
                }

                //String studyReport = "StudyInstanceUID = " + studyInstanceUID + " with StudyDateTime = " + studyDateTime;
                //if (isDebug) { System.out.println (studyReport); }

                System.out.println ("Done processing " + studyInstanceUID);
                System.out.println ("--\n--\n--\n");

                if (isFailedToProcess) {
                    StudyFailedCount++;
                }
            }

            //Close the input stream
            br.close();
            //Close file
            fis.close();

            System.out.println("Processed " + StudyTotalCount + " studies");
            System.out.println("Failed " + StudyFailedCount + " studies");

        } catch (IOException e) {
            e.printStackTrace();
        } finally {
            try {
                if (fis != null)
                    fis.close();
            } catch (IOException ex) {
                ex.printStackTrace();
            }
        }

        System.out.println("About to close session...");
        if (!session.isClosed())
            session.close();

        System.out.println("About to close cluster...");
        if (!cluster.isClosed())
            cluster.close();

    }

}
