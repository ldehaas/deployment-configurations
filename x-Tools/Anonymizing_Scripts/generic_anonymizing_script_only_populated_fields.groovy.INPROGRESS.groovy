// Anonimizing Script to be run in HL7 Heaven

// Canned variables with their actual meaning as values (to modify the script according to your needs)
def affinityPID = "Affinity_Patient_ID"
def affinityDomainNameSpace = "Affinity_Domain_NameSpace"
def affinityDomain = "AffinityDomain"

def mrn = "MRN"
def mnrIssuerOfPatientID = "MRN_Issuer_of_Patient_ID"

def patientLastName = "Patient_LastName"
def patientFirstName = "Patient_First_Name"
def patientMiddleName = "Patient_Middle_Name"
def patientMothersMaidenNameLastName = "Patient's_Mother's_Maiden_Name_Last_Name"
def patientMothersMaidenNameFirstName = "Patient's_Mother's_Maiden_Name_First_Name"
def patientDateOfBirth = "Patient's_Date_of_Birth"
def patientGender = "Patient's_Gender"
def patientAddressStreetAddress = "Patient's_Street_Address"
def patientAddressCity = "Patient's_Address_City"
def patientAddressProvince = "Patient's_Address_Province"
def patientAddressPostalCode = "Patient's_Address_Postal_Code"
def patientAddressCountry = "Patient's_Address_Country"
def patientHomePhoneNum = "Patient's_Home_Phone_Number"
def patientBusinessPhoneNum = "Patient's_Business_Phone_Number"

def attendingDoctorLicense = "Attending_Doctor_License"
def attendingDoctorName = "Attending_Doctor_Name"
def referringPhysicianLicense = "Referring_Physician_License"
def referringPhysicianName = "Referring_Physician's_Name"
def admittingPhysicianLicense = "Admitting_Physician's_License"
def admittingPhysicianName = "Admitting_Physician's_Name"

def orderEnteredByUserName = "Order_Entered_By_User_Name"
def orderEnteredByName = "Order_Entered_By_Name"
def readingPhysicianPrimaryLicense = "Primary_Reading_Physician_License"
def readingPhysicianPrimaryName = "Primary_Reading_Physician_Name"
def readingPhysicianSecondaryLicense = "Secondary_Reading_Physician_License"
def readingPhysicianSecondaryName = "Secondary_Reading_Physician_Name"


// Common variables to preserve the content of the original message
def sendingApplication = get('MSH-3')

def accessionNumber = get('ORC-3')
def studyInstanceUID = get('ZDS-1')

def ormOrderModality = get('OBR-24')
def ormOrderAnatomicRegion = get('OBR-15-4')
def ormOrderStatus = get('ORC-16')


// Default Values for Variables
// Variables with the Cannonical (Standard) Placeholder names that will be used to anonymize

def cannonicalDefaultValuesforVariables = [
    [mrn : "MRN"],
    [mnrIssuerOfPatientID : "MRN_Issuer_of_Patient_ID"],
    [patientLastNam :  "Patient_LastName"],
    [patientFirstName : "Patient_First_Name"],
    [patientMiddleName : "Patient_Middle_Name"],
    [patientMothersMaidenNameLastName : "Patient's_Mother's_Maiden_Name_Last_Name"],
    [patientMothersMaidenNameFirstName : "Pat's_Mosher's_Maiden_Name_First_Name"],
    [patientDateOfBirth : "Patient's_Date_of_Birth"],
    [patientGender : "Patient's_Gender"],
    [patientAddressStreetAddress : "Patient's_Street_Address"],
    [patientAddressCity : "Patient's_Address_City"],
    [patientAddressProvince : "Patient's_Address_Province"],
    [patientAddressPostalCode : "Patient's_Address_Postal_Code"],
    [patientAddressCountry : "Patient's_Address_Country"],
    [patientHomePhoneNum : "Patient's_Home_Phone_Number"],
    [patientBusinessPhoneNum : "Patient's_Business_Phone_Number"],
    [attendingDoctorLicense : "Attending_Doctor_License"],
    [attendingDoctorName : "Attending_Doctor_Name"],
    [referringPhysicianLicense : "Referring_Physician_License"],
    [referringPhysicianName : "Referring_Physician's_Name"],
    [admittingPhysicianLicense : "Admitting_Physician's_License"],
    [admittingPhysicianName : "Admitting_Physician's_Name"],
    [orderEnteredByUserName : "Order_Entered_By_User_Name"],
    [orderEnteredByName : "Order_Entered_By_Name"],
    [readingPhysicianPrimaryLicense : "Primary_Reading_Physician_License"],
    [readingPhysicianPrimaryName : "Primary_Reading_Physician_Name"],
    [readingPhysicianSecondaryLicense : "Secondary_Reading_Physician_License"],
    [readingPhysicianSecondaryName : "Secondary_Reading_Physician_Name"]
]




// Location of the variables in the actual anonymized message
//  The variables in this list are only meant to take a value at all (i.e. not be null)
//  if the analogous placeholder in the original message was populated

def variablesAndInsertionPlaces = [
    ["MSH-3", sendingApplication],
    ["PID-2-1", affinityPID],
    ['PID-2-4', affinityDomainNameSpace],
    ['PID-2-4-2', affinityDomain],
    ['PID-3-1', mrn],
    ['PID-3-4-2', mnrIssuerOfPatientID],
    ['PID-5-1', patientLastName],
    ['PID-5-2', patientFirstName],
    ['PID-5-3', patientMiddleName],
    ['PID-6-1', patientMothersMaidenNameLastName],
    ['PID-6-2', patientMothersMaidenNameFirstName],
    ['PID-7', patientDateOfBirth],
    ['PID-8', patientGender],
    ['PID-11-1', patientAddressStreetAddress],
    ['PID-11-3', patientAddressCity],
    ['PID-11-4', patientAddressProvince],
    ['PID-11-5', patientAddressPostalCode],
    ['PID-11-6', patientAddressCountry],
    ['PID-13', patientHomePhoneNum],
    ['PV1-7-1', attendingDoctorLicense],
    ['PV1-7-2', attendingDoctorName],
    ['PV1-8-1', referringPhysicianLicense],
    ['PV1-8-2', referringPhysicianName],
    ['PV1-17-1', admittingPhysicianLicense],
    ['PV1-17-2', admittingPhysicianName],
    ['ORC-10-1', orderEnteredByUserName],
    ['ORC-10-2', orderEnteredByName],
    ['ORC-12-1', referringPhysicianLicense],
    ['ORC-12-2', referringPhysicianName],
    ['ORC-16', ormOrderStatus],
    ['OBR-3', accessionNumber],
    ['OBR-15-4', ormOrderAnatomicRegion],
    ['OBR-16-1', referringPhysicianLicense],
    ['OBR-16-2', referringPhysicianName],
    ['OBR-24', ormOrderModality],
    ['OBR-32-1', readingPhysicianPrimaryLicense],
    ['OBR-32-2', readingPhysicianPrimaryName],
    ['OBR-33-1', readingPhysicianSecondaryLicense],
    ['OBR-33-2', readingPhysicianSecondaryName],
    ['ZDS-1', studyInstanceUID]
]



/*** Script Body ***/

def variablesAndValuesForAnonymizing = []


// Check if the original message has a given field populated.
//  If so, populate each corresponding stock variable with its Cannonical value

for(eachPopulatedFieldInOriginalMessage in variablesAndInsertionPlaces){

    def specificField = get(eachPopulatedFieldInOriginalMessage[0])

    if(specificField != null) {  // Specific field in original message is populated

        // Populate corresponding stock variable with its Cannonical value
        def stockVariable = eachPopulatedFieldInOriginalMessage[1]
        def stockVariableValue = cannonicalDefaultValuesforVariables[stockVariable]

        // Add the specific field and its stock value to the list of values to be used for anonymizing
        variablesAndValuesForAnonymizing.push([stockVariable, stockVariableValue])


        //  for blank fields, the variables will simply be null
    }

}


// Create anonymized message by using only the fields that were populated in the original message

for(eachField in variablesAndInsertionPlaces) {

    for(eachVariableForAnonymizing in variablesAndValuesForAnonymizing) {

        if(eachField[1] == eachVariableForAnonymizing[0]) {  //The field being evaluated did exist in the original message
            set(eachField[0], eachVariableForAnonymizing[1])
        }
    }
}
