from flask import Flask
from flask import request
from flask import jsonify
from datetime import datetime
from pathlib import Path
import yaml
import os
import io
import json
import re

# to start web service run: python3 dapi_handler.py
app = Flask(__name__)

@app.route('/')
def home():
    return "Default Page."

@app.route('/dapi/checkextract', methods=['GET'])
def checkextract():
    print("checking for existing extract process")
    msg=__psextract()
    print(msg)
    return jsonify(result=msg),200,{'Content-Type': 'application/json'}

@app.route('/dapi/startextract', methods=['GET'])
def startextract():
    args=request.args
    poolid=args.get('poolid') or None
    if poolid is None:
        poolid=""   # no pool involved
        msg=__startextract(poolid)
    else:
        if len(re.findall("^p[4-9]|p1[0-9]$",poolid))>0:
            msg=__startextract(poolid)
        else:
            return jsonify(Result="Invalid Pool ID."),500  # return error if invalid pool id is provided.
    return jsonify(Result=msg),200,{'Content-Type': 'application/json'}

@app.route('/dapi/lsreport', methods=['GET'])
def lsreport():
    msg=__lsreport()
    return jsonify(Result=msg),200,{'Content-Type': 'application/json'}

@app.route('/dapi/sendreport', methods=['GET'])
def sendreport():
    # needs implementation
    return "sendreport"

@app.route('/dapi/rmreport', methods=['GET'])
def rmreport():
    # example: curl -X GET localhost:3999/dapi/rmreport?fn=abc
    fn=request.args['fn']
    rmcmdstr="rm "+report_path.rstrip("/")+"/"+fn
    filepath=os.path.join(report_path,fn)
    if os.path.exists(filepath):
        os.remove(filepath)
    return jsonify(filenam=fn),200,{'Content-Type': 'application/json'}

def __getCurTableWalkerStatus():
    cmd_res=os.popen("ps -ef | grep 'com.karos.cql3.maintenance.tablewalker' | grep TableWalkerRunner | grep -v grep | awk '{print $2, $13}'").read()
    if (cmd_res.count('\n') >= 0):
        if (cmd_res.count('imagearchive_') >= 1):
            return 1    # one or more table walker jobs is running against image_archive keyspace
        else:
            return 0    # no table walker identified to be running against image_archive keyspace
    else:
        return -1    # cannot determine status

def __psextract():
    print("checking for existing extract process")
    CurStatusCode=__getCurTableWalkerStatus()
    if CurStatusCode==1:
        msg_summary="One or mor table Walker process is running against image_archive keyspace.\n"
        cmd_res=os.popen("ps -ef | grep 'com.karos.cql3.maintenance.tablewalker' | grep TableWalkerRunner | grep -v grep | awk '{row=$2; for (i=8;i<=NF;i++) row=row\" \"$i ;print row}'").read()
        print(msg_summary+cmd_res)
        return msg_summary+cmd_res
    elif CurStatusCode==0:
        msg_summary="No table walker identified to be running against image_archive keyspace.\n"
        print(msg_summary)
        return msg_summary
    else:
        msg_summary="Unable to determine status.\n"
        print(msg_summary)
        return msg_summary

def __startextract(pool_id):
    print("__startextract")
    CurStatusCode=__getCurTableWalkerStatus()
    if CurStatusCode==0:
        dt_stamp=datetime.now().strftime("%Y%m%d%H%M%S")
        if not pool_id:
            keyspace='imagearchive'
            twoutput=report_path.rstrip("/")+'/'+dt_stamp+'.etr'
        else:
            keyspace='imagearchive_'+pool_id
            twoutput=report_path.rstrip("/")+'/'+pool_id+'_'+dt_stamp+'.etr'

        twcmdstr=' '.join([tw_bin_path.rstrip("/")+'/cassandra_table_walker',
                        '-cassandraIP',worker_node,
                        '-keyspace',keyspace,
                        '-script',tw_groovy,
                        '-entityClassName','new_study',
                        '-consistencyLevel','LOCAL_QUORUM',
                        '-autoPagination' ,
                        '>',twoutput])
        os.popen(twcmdstr)
        msg_summary="Kicked off extract job. Check status with psextract."
        msg_detail=twcmdstr
    elif CurStatusCode==1:
        msg_summary="One or more table Walker process is already running against image_archive keyspace."
        msg_detail="You cannot run more than one table walker process at a time."
    else:
        msg_summary="Unable to determine current status."
        msg_detail="Please provide detail for Vital to investigate."
    print(msg_summary+"\n"+msg_detail)
    return msg_summary+"\n"+msg_detail

def __lsreport():
    #print("__lsreport")
    lscmdstr="ls -ltr --block-size=k "+report_path.rstrip("/")+"|awk '{if(NR>1) print $5,$6,$7,$8,$9}'"
    cmd_res=os.popen(lscmdstr).read()

    if cmd_res.count('\n')>0:
        msg_summary="Reports available:"
        msg_detail=cmd_res
    else:
        msg_summary="Unable to find reports."
        msg_detail="Please provide detail for Vital to investigate if you expect reports."
    #print(msg_summary+"\n"+msg_detail)
    return msg_summary+"\n"+msg_detail

def __sendreport(filename):
    print("__sendreport")
    
    # sed '/TableWalkerRunner \- Total number of processed entities/,$d;0,/support pagination sleep time/d' p8_20191029161354.etr
    # zip -9 name.zip filenam.etr
    return 0

if __name__ == "__main__":
    cfg_file_path=Path(__file__).with_suffix('.yml').as_posix()
    with open(cfg_file_path,'r') as ymlfile:
        cfg = yaml.load(ymlfile, Loader=yaml.FullLoader)
    # Load up and normalize configurations
    api_port=cfg["service"]["api_port"]
    report_path=cfg["output"]["local_path"]
    tw_bin_path=cfg["tablewalker"]["bin_path"]
    tw_groovy=cfg["tablewalker"]["groovy_path"]
    worker_node=cfg["tablewalker"]["worker_node"]

    app.run(host="0.0.0.0",port=api_port,debug=True)

