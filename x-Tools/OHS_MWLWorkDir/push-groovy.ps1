function Convert-File($path, $source, $dest) {
    $utf8NoBomEncoding = New-Object System.Text.UTF8Encoding($False)
    Write-Host "converting:", $path -ForegroundColor "green"
    $x =[System.IO.File]::ReadAllText($path)
    $content = $x -replace $source, $dest
    [System.IO.File]::WriteAllText($path, $content, $utf8NoBomEncoding)
}

$script = Read-Host 'Please validate the script and specify Which one do you want to push? [M] mwl_mapping.groovy [D]dicomProxy_mapping.groovy'
$s = $script.Substring(0,1).ToUpper()
if ($s -eq "D") { $script = "dicomProxy_mapping.groovy" }
elseif ($s -eq "M") { $script = "mwl_mapping.groovy" }
elseif ($s -eq "T") {$script = "test.groovy"}
else { 
    Write-Host "Invalid Input"
    Exit
}

if (-Not (Test-Path $script)){
    Write-Host "File "+$script+"does not exist!"
    Exit
}

$un = Read-Host 'Username'

$pwe = Read-Host 'Password' -AsSecureString
$dst_ip_list = "147.206.236.166","147.206.236.167","147.206.236.168","147.206.236.169","147.206.236.170","147.206.160.26","147.206.160.27","147.206.160.28","147.206.160.29","147.206.160.30"
$pwc = [Runtime.InteropServices.Marshal]::PtrToStringAuto([Runtime.InteropServices.Marshal]::SecureStringToBSTR($pwe))

$full_path=(Get-ChildItem $script).FullName
Convert-File $full_path "`r`n" "`n"
$success_push=0

try{
    foreach ($dst_ip in $dst_ip_list){
        Write-Host "Trying to push to "+$dst_ip
        $push_cmd="./pscp.exe -pw " + $pwc + " ./" + $script + " " + $un + "@" + $dst_ip + ":/opt/rialto/etc/services/common/" + $script
		# if you need to troubleshoot, print out the command above.
        #Write-Host $push_cmd
        $res_success=Invoke-Expression $push_cmd
        if($res_success){
            Write-Host "Successfully pushed to "+$dst_ip
            $success_push++
        }
        else{Write-Host "Failed to push to "+$dst_ip}
    }
}
catch{
     Write-Host "Script push failed on some servers. Please troubleshoot. "
}

if ($success_push -eq $dst_ip_list.Length){
    Write-Host "Updated script has been pushed to all destination servers. Deleting the script."
    Remove-Item $script
}else{
    Write-Host "Script push failed on some servers. Please troubleshoot. "
}

