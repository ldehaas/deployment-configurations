function Convert-File($path, $source, $dest) {
    $utf8NoBomEncoding = New-Object System.Text.UTF8Encoding($False)
    Write-Host "converting:", $path -ForegroundColor "green"
    $x =[System.IO.File]::ReadAllText($path)
    $content = $x -replace $source, $dest
    [System.IO.File]::WriteAllText($path, $content, $utf8NoBomEncoding)
}

$script = Read-Host 'Which script do you want to pull? [M] mwl_mapping.groovy [D]dicomProxy_mapping.groovy'
$s = $script.Substring(0,1).ToUpper()
if ($s -eq "D") { $script = "dicomProxy_mapping.groovy" }
elseif ($s -eq "M") { $script = "mwl_mapping.groovy" }
elseif ($s -eq "T") { $script = "test.groovy" }
else { 
    Write-Host "Invalid Input"
    Exit
}

$un = Read-Host 'Username'
$pwe = Read-Host 'Password' -AsSecureString
$src_ip = "147.206.236.170"
$pwc = [Runtime.InteropServices.Marshal]::PtrToStringAuto([Runtime.InteropServices.Marshal]::SecureStringToBSTR($pwe))
$pull_cmd="./pscp.exe -pw " + $pwc + " " + $un + "@"+ $src_ip +":/opt/rialto/etc/services/common/" + $script + " ./"
#Write-Host $pull_cmd

$res_success=Invoke-Expression $pull_cmd
Write-Host $res_success

if($res_success){
#   start 'https://groovy-playground.appspot.com/'
   $full_path=(Get-ChildItem $script).FullName
   Write-Host $full_path
   Convert-File $full_path "`n" "`r`n"
   $stamp = $(Get-Date -Format "yyyyMMddHHmmss")
   Copy-Item -Path "$($full_path)" -Destination "$($full_path)\$($stamp).groovy"
   notepad.exe $script
}

