#! /bin/bash

while true; do
    echo "----"; date;  

    #Display the amount of total connections (all states, including time wait) and established connections to HCP
    echo -n "Current Connections to HCP hosts (ALL connections): "; netstat -p --tcp | grep "kimhcp"  | wc -l;
    echo -n "Current Connections to HCP hosts (ESTABLISHED Connections ONLY): "; netstat -p --tcp | grep "kimhcp" | grep ESTABLISHED | wc -l; 
    
    #Display the actual content of the Established connections
    netstat -p --tcp | grep "kimhcp" | grep ESTABLISHED ; sleep 30;

# Write Output to File in the /tmp dir
done >> /tmp/ConnectionChecks
