#!/bin/bash
# Rodrigo Gonzalez

# Check all App Servers at Carillion-Kimball on a specified interval

CHECKINTERVAL="10"
    # Pause between checks, in seconds

while true; do 
    echo "-----"; echo -n "Starting at "; date ; echo "Checking All App Servers:";
    echo -n "App0:" ; curl -k https://kvnaapp0:2525/rialto/status ; 
    echo -n "App1:" ; curl -k https://kvnaapp1:2525/rialto/status ;
    echo -n "App2:" ; curl -k https://kvnaapp2:2525/rialto/status ;
    echo -n "App2:" ; curl -k https://kvnaapp3:2525/rialto/status ; 
    echo -n "Test:" ; curl -k https://avnatst0:2525/rialto/status ; 
    echo "" ; echo -n "Ending at "; date ; 
    sleep $CHECKINTERVAL ;
done
