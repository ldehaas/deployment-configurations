time ansible-playbook -i inventories/lab_cs_bloom vc_install_01_repo_online.yml
time ansible-playbook -i inventories/lab_cs_bloom vc_install_02_prepare.yml
time ansible-playbook -i inventories/lab_cs_bloom vc_install_03_rialto_prep.yml

-- ssh into the box
cd /tmp/rialto-all...
bash install-rialto -d cassandra -d elastic -e kibana -e nginx

time ansible-playbook -i inventories/lab_cs_bloom vc_install_99_postinstall.yml




======================= Testing =============================

time ansible-playbook -i inventories/lab_cs_bloom vc_install_01_repo_online.yml
real	1m50.788s
user	0m8.034s
sys	0m1.795s


time ansible-playbook -i inventories/lab_cs_bloom vc_install_02_prepare.yml
real	1m47.700s
user	0m11.063s
sys	0m4.050s


time ansible-playbook -i inventories/lab_cs_bloom vc_install_03_rialto_prep.yml
real	3m20.265s
user	0m16.498s
sys	0m5.547s



time ansible-playbook -i inventories/lab_cs_bloom vc_install_99_postinstall.yml
real	3m13.212s
user	0m14.209s
sys	0m4.665s
