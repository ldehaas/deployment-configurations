#!/bin/bash

ansible-playbook -i inventories/bh_test vc_config_backup.yml
ansible-playbook -i inventories/bh_prod_pr vc_config_backup.yml --limit vc_app
ansible-playbook -i inventories/bh_prod_dr vc_config_backup.yml --limit vc_app
