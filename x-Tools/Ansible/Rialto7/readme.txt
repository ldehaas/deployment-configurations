Ansible playbooks to install Rialto on bare VMs from scratch.

This set of Ansible playbooks should allow you to install single box All-in-One Rialto as well as large cluster distributed across a number of VMs.
This should allow you to deploy Rialto to local ESXi environment as well as to your private cloud on AWS/Azure/Google.



wget http://download.karoshealth.com/install-rialto/downloads-CentOS7.4-latest-version.txt
wget http://download.karoshealth.com/install-rialto/downloads-CentOS7.4-20180507.zip
wget http://download.karoshealth.com/install-rialto/downloads-CentOS7.4-20180507.zip.md5

vi ./inventories/lab_cs_bloom

[all:vars]

[all_hosts]
bloom1	ansible_host=10.100.76.67 ansible_user=root
bloom2  ansible_host=10.100.76.xx ansible_user=root
bloom3  ansible_host=10.100.76.xx ansible_user=root



# download Rialto repo package directly (NOTE: package name and download URL should come from group_vars)
# deploy local Rialto repo (downloads package) and patched the system
ansible-playbook -i inventories/lab_cs_bloom ansible_rialto_repo_setup_online.yml

# download to local box and then upload to remote hosts
# deploy local Rialto repo (downloads package) and patched the system
ansible-playbook -i inventories/lab_cs_bloom ansible_rialto_repo_setup_local.yml


# install open-vm-tools package, start and enable vmtoolsd.service
# systemctl status vmtoolsd.service
ansible-playbook -i inventories/lab_cs_bloom ansible_vm_tools_install.yml

# check that timesync is disabled between guest VM and host
ansible-playbook -i inventories/lab_cs_bloom ansible_vm_timesync_check.yml
# ansible-playbook -i inventories/lab_cs_bloom ansible_vm_timesync_disable.yml

# disable SELinux
# sed -i 's/SELINUX=enforcing/SELINUX=disabled/g' /etc/selinux/config
# sed -i 's/SELINUX=enforcing/SELINUX=disabled/g' /etc/sysconfig/selinux
ansible-playbook -i inventories/lab_cs_bloom ansible_selinux_disable.yml

# Reboot
ansible-playbook -i inventories/lab_cs_bloom ansible_centos_reboot.yml

# disable cronyd
ansible-playbook -i inventories/lab_cs_bloom ansible_chronyd_disable.yml


# install, configure, start, and enable NTP
ansible-playbook -i inventories/lab_cs_bloom ansible_ntp_install.yml


# capture MAC address, bring it back, generate rialto.lic file and upload to each box
# java -jar license-generator-1.0.0-SNAPSHOT-jar-with-dependencies.jar -f rialto.lic -days 100 00:50:56:01:08:59
# java -jar license-generator-1.0.0-SNAPSHOT-jar-with-dependencies.jar -f rialto.lic 00:50:56:01:09:56
ansible-playbook -i inventories/lab_cs_bloom  ansible_rialto_service_install.yml




# upload Rialto installation package
# rialto-product-x.y.z.bNNN.zip

# upload Rialto license file
# rialto.lic


ansible-playbook -i inventories/vbox_cassandra vc_install_01_repo.yml
ansible-playbook -i inventories/vbox_cassandra vc_install_02_prepare.yml
ansible-playbook -i inventories/vbox_cassandra vc_install_02_prepare.ym
ansible-playbook -i inventories/vbox_cassandra --extra-vars="init_server=true empty_cluster=true" vc_install_03_cassandra.yml
