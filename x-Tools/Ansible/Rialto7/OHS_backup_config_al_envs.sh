#!/bin/bash

ansible-playbook -i inventories/ochsner_test vc_config_backup.yml
ansible-playbook -i inventories/ochsner_prod_dc1 vc_config_backup.yml
ansible-playbook -i inventories/ochsner_prod_dc2 vc_config_backup.yml
