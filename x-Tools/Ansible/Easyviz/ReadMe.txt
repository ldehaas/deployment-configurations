Here are the ansible playbooks available for Easyviz.

1) ev_sync_database.yml
This playbook is used to sync (aka apply) the DB2 database from one DC to the other. Take note that the destination system's DB2 database is replaced with the
copy. There is a check task within this playbook which ensures you are not trying to sync the current active database, if so the playbook will fail and a message will saying "Trying to Sync DB on Active System" will be displayed. The source system is not touched and will not be effected by running this playbook as it only reads the latest backup file. The backup file is automatically copied from the active system to the passive system overnight which means it could be a few hours old and therefore not what you want. If you want to sync up the databases using the most upto date information then you should first run the DB2 backup command on the active system (see /etc/cron.d/evvdb_backup) followed by the copy backup script (see /etc/ev/backupScripts/copyBkup.sh) this will ensure that the very latest active database data is available on the passive side. Then run the sync database playbook.

Here is an example of running this playbook:

ansible-playbook  -i ./inventories/ochsner_prod_dc1 ev_sync_database.yml 



2)  ev_upgrade.yml
This playbook will upgrade all nodes/servers within the provided inventory file. No additional parameters are required, however it will prompt you to provide two pieces of information. First it will ask you to specify the version you want to upgrade to, provide this using a format such as 7.7.4. Next it will ask you to specify the hostname were it can obtain the required packages. These packages will be within a repo on the specified hostname. The upgrade process would be to first upgrade one of the environments, for example Test-B, performing the usual documented steps within the admin guide. Then once this system is upgraded all of the other environments can be upgraded using this playbook and specifying the initial hostname (Test-B in this example).

Here is an example of running this playbook:

ansible-playbook -i inventories/ochsner_prod_dc1 ev_upgrade.yml


3)  ev_config_backup
This playbook is designed to run on your Mac and obtain the easyviz and specific OS files from both the backend and render node servers. It will place these files into a specific location within your local GIT respository on your MAC. Once this playbook is completed you can then push these config files into GIT.

Here is an example of this playbook:

ansible-playbook -i inventories/ochsner_prod_dc1 ev_config_backup.yml

4) ev_clean_rnodes.yml
This playbook will find and optionally terminate hung camloader processes on rendernodes. If run with no extra parameters this playbook will only report if any camloader processes are found on the node which are older than the current date. A  camloader 
process is only active while a user is viewing an image so typically they only last a few minutes, hence any older than the current date are almost certainly hung and need to be terminated. To terminate these hung processes add the optional parameter "terminate=true".

Here is an example of running this playbook to report on any hung camloader processes:

ansible-playbook -i inventories/ochsner_prod_dc1 ev_clean_rnodes.yml


Here is an example of running this playbook to terminate any hung camloader processes:

ansible-playbook -i inventories/ochsner_prod_dc1 ev_clean_rnodes.yml -e terminate=true
