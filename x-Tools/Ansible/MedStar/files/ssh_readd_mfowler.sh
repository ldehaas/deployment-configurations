#!/bin/bash

# check if Matt's key is in /home/rialto/.ssh/authorized_keys

# Matt Fowler - Public Key Putty
#ssh-rsa AAAAB3NzaC1yc2EAAAABJQAAAQEAyxucv1AZbNLfiE33/tV5B4ACMvZwKMFZx7EvUw1M8CMWGvVwn9wCs4E0kRIM4LOaByvyVw2V+KJ0Rd196FnOesf58++xieyBrzzHpPYlfiKflEE7U87wJpomWG1e6qp2lAmPxK4kfJMunW7BmKCMWn/bINevuCuz1NOGCGr3djq+6zfoUbWIfa/RfwwL7478gGTGxEPIqECMJ/bkVpCEoJWpylx21VBfSTgJfII59IvNkf8Zh3Zlr8OWYctq0mA+4ZV9cDgf3hOFZzpZsWG70huBgyd5rm3rpmUDWW/PqSbpWGY0c833R4fbDuStB+DMkt026RxouJBptK+OqXFKjQ== rsa-key-20160602 Putty


if ( ! grep -q 'AAAAB3NzaC1yc2EAAAABJQAAAQEAyxucv1AZbNLfiE33/tV5B4ACMvZwKMFZx7EvUw1M8CMWGvVwn9wCs4E0kRIM4LOaByvyVw2V+KJ0Rd196FnOesf58++xieyBrzzHpPYlfiKflEE7U87wJpomWG1e6qp2lAmPxK4kfJMunW7BmKCMWn/bINevuCuz1NOGCGr3djq+6zfoUbWIfa/RfwwL7478gGTGxEPIqECMJ/bkVpCEoJWpylx21VBfSTgJfII59IvNkf8Zh3Zlr8OWYctq0mA+4ZV9cDgf3hOFZzpZsWG70huBgyd5rm3rpmUDWW/PqSbpWGY0c833R4fbDuStB+DMkt026RxouJBptK+OqXFKjQ' /home/rialto/.ssh/authorized_keys ); then
   echo ' ' >> /home/rialto/.ssh/authorized_keys
   echo '###############################################' >> /home/rialto/.ssh/authorized_keys
   echo '### MedStar Support Personnel' >> /home/rialto/.ssh/authorized_keys
   echo '################################################' >> /home/rialto/.ssh/authorized_keys
   echo ' ' >> /home/rialto/.ssh/authorized_keys
   echo '# Matt Fowler - Public Key Putty' >> /home/rialto/.ssh/authorized_keys
   echo 'ssh-rsa AAAAB3NzaC1yc2EAAAABJQAAAQEAyxucv1AZbNLfiE33/tV5B4ACMvZwKMFZx7EvUw1M8CMWGvVwn9wCs4E0kRIM4LOaByvyVw2V+KJ0Rd196FnOesf58++xieyBrzzHpPYlfiKflEE7U87wJpomWG1e6qp2lAmPxK4kfJMunW7BmKCMWn/bINevuCuz1NOGCGr3djq+6zfoUbWIfa/RfwwL7478gGTGxEPIqECMJ/bkVpCEoJWpylx21VBfSTgJfII59IvNkf8Zh3Zlr8OWYctq0mA+4ZV9cDgf3hOFZzpZsWG70huBgyd5rm3rpmUDWW/PqSbpWGY0c833R4fbDuStB+DMkt026RxouJBptK+OqXFKjQ== rsa-key-20160602 Putty' >> /home/rialto/.ssh/authorized_keys
fi
