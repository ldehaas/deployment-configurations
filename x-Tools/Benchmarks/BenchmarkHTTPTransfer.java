/*
 *   Java program to benchmark transfer speed of a study over MINT/HTTP
 *
 *   javac BenchmarkHTTPTransfer.java
 *
 *   java BenchmarkHTTPTransfer 1.2.840.113696.563627.530.520208.123456789
 */

import java.io.*;
import java.net.URL;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Calendar;

public class BenchmarkHTTPTransfer {

    public static void main(String [] args) {

        if (args[0] == null || args[0].trim().isEmpty()) {
            System.out.println("You need to specify a StudyInstanceUID!");
            return;
        }

        // The name of the file to open.
        //String fileName = args[0];
        String studyInstanceUID = args[0];

        try {

            DateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss.SSS");
            //Calendar cal = Calendar.getInstance();

            //InputStream fis = new FileInputStream(fileName);
            //BufferedInputStream bis = new BufferedInputStream(fis);

            // time curl -i -X GET -H "Accept: application/octet-stream" http://localhost:8080/vault/mint/studies/1.2.840.113696.563627.530.520208.123456789/DICOM/binaryitems/all > /dev/null
            BufferedInputStream bis = new BufferedInputStream(new URL("http://localhost:8080/vault/mint/studies/" + studyInstanceUID + "/DICOM/binaryitems/all").openStream());

            Long startTime = System.nanoTime();
            Long startInterval = startTime;
            Long totalBytesRed = 0L;

            Long endInterval = startTime;
            Long intervalBytesRed = 0L;
            Long intervalTime = 0L;
            double intervalSpeedInBytes = 0.0;

            int bytesRed = 0;
            byte[] buf = new byte[1024];

            while( (bytesRed = bis.read(buf)) != -1) {
                totalBytesRed += bytesRed;
                intervalBytesRed += bytesRed;

                endInterval = System.nanoTime();
                if ( (endInterval - startInterval) > 1000000000) {
                    // calculate and print rate
                    intervalTime = endInterval - startInterval;
                    double intervalTimeinMilliseconds = (double)intervalTime/1000000.0;
                    intervalSpeedInBytes = intervalBytesRed / intervalTimeinMilliseconds * 1000 / 1024D / 1024D;     // convert to MB/s

                    // System.out.println( "DEBUG: intervalTime: " + String.format("%.0f", intervalTimeinMilliseconds) + " ms, bytes: " + intervalBytesRed + ", interval speed: " + String.format("%.2f", intervalSpeedInBytes) + " MB/s");
                    System.out.println( dateFormat.format(System.currentTimeMillis()) + ": intervalTime: " + String.format("%.0f", intervalTimeinMilliseconds) + " ms, bytes: " + intervalBytesRed + ", interval speed: " + String.format("%.2f", intervalSpeedInBytes) + " MB/s");

                    // reset startInterval
                    startInterval = endInterval;
                    intervalBytesRed = 0L;
                }
            }

            bis.close();
            //fis.close();

            Long endTime = System.nanoTime();

            //System.out.println( "DEBUG: startTime: " + startTime );
            //System.out.println( "DEBUG: endTime: " + endTime );

            double totalMBytesRed = totalBytesRed / 1024D /1024D;

            double totalTimeInSecs = (double)(endTime - startTime) / 1000000000.0;  //converting nanoseconds to seconds
            //System.out.println( "DEBUG: timeInSecs: " + String.format("%.2f", totalTimeInSecs) );

            double totalSpeedInBytes = totalBytesRed / totalTimeInSecs / 1024D / 1024D;
            //System.out.println( "DEBUG: speedInBytes: " + String.format("%.2f", totalSpeedInBytes) + " MB/s");

            System.out.println( dateFormat.format(System.currentTimeMillis()) + ": Totals: Red " + String.format("%.0f", totalMBytesRed) + " MB in " + String.format("%.2f", totalTimeInSecs) + " seconds; Average speed is: " + String.format("%.2f", totalSpeedInBytes) + " MB/s" );
        }
        catch(IOException ex) {
            ex.printStackTrace();
        }
    }
}
