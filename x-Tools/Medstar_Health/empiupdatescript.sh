# HL7 message generator script for Medstar EMPI Update tickets
# Written by Jason Yip
# Date 2019-04-29

#!/bin/bash

letters='[a-zA-Z]'
numbers='[0-9]+$'

# Prompts for MSH namespace
read -p "Enter the NAMESPACE:" nameSpace

# This checks that the input is a valid namespace for MSH
while [[ ! "$nameSpace" == "MHHC" ]] && [[ ! "$nameSpace" == "MGUH" ]] && [[ ! "$nameSpace" == "MSMH" ]] && [[ ! "$nameSpace" == "MWHC" ]] && [[ ! "$nameSpace" == "MUMH" ]] && [[ ! "$nameSpace" == "MMMC" ]] && [[ ! "$nameSpace" == "MFMC" ]] && [[ ! "$nameSpace" == "MGSH" ]] && [[ ! "$nameSpace" == "MSMHC" ]] && [[ ! "$nameSpace" == "MAS" ]] && [[ ! "$nameSpace" == "MPP" ]] && [[ ! "$nameSpace" == "MSC" ]] && [[ ! "$nameSpace" == "SHAH" ]] && [[ ! "$nameSpace" == "MHH" ]] && [[ ! "$nameSpace" == "MGSH" ]] && [[ ! "$nameSpace" == "MUMH" ]] ; do
    echo "The namespace you've entered incorrect. Make sure it is in UPPER case and a valid namespace."
    read nameSpace
done

echo "Site Namespace:" $nameSpace

# Prompts for EMPI number proviced by customer to be applied to patient
read -p "Enter EMPI number: " empi

while [[ ! ${empi} =~ $numbers ]] ; do
    echo $empi "is NOT a valid EMPI. It is not a number. Try again."
    read empi
done

echo "EMPI number provided by customer and to be applied:" $empi

# Prompts for Patient ID and checks if it's numeric
read -p "Enter PID: " pid

while [[ ! ${pid} =~ $numbers ]] ; do
    echo $pid "is NOT numeric or is blank. Try again."
    read pid
done

echo "Patient ID/PID: " $pid

# Prompts for Patient Name and checks if it's blank
read -p "Enter PATIENT NAME (copy it from metadata & paste it here): " pName

while [[ "$pName" == '' ]] || [[ "$pName" == null ]] ; do
    echo $pName "is NOT valid. It is null or blank. Try again."
    read pName
done

echo "Patient Name:" $pName

# Prompts for the patient's DOB. It will validate if it's numeric first
read -p "Enter patient's DATE OF BIRTH: " pDoB
lengthDoB=${#pDoB}

while [[ ! ${pDoB} =~ $numbers ]] ; do
    echo $pDoB "is NOT numeric. Try again."
    read pDoB
done

while [[ $lengthDoB -lt 8 ]] || [[ $lengthDoB -ge 9 ]] ; do
    echo $lengthDoB "is NOT a valid Date of Birth. DOB must be 8 digits. Try again."
    read pDoB
    lengthDoB=${#pDoB}
done

echo "Patient's data of birth:" $pDoB

# Prompts for and checks Patient Sex
read -p "Enter patient sex: " pSex

while [[ ! $pSex == 'F' ]] && [[ ! $pSex == 'f' ]] && [[ ! $pSex == 'M' ]] && [[ ! $pSex == 'm' ]] ; do
    echo $pSex "is NOT valid. It must F(f) or M(m). Try again."
    read pSex
done

echo "Patient's sex:" $pSex

# Generates line one of the HL7 message
hl7Line1="MSH|^~\&|INVISION|$nameSpace|RIALTO|$nameSpace|201612311259||ADT^A08|-7256382012533524927|P|2.3|-1"
# Generates line two of the HL7 message
hl7Line2="PID|1|$empi^^^MEDSTAR&EE&ISO|$empi^^^MEDSTAR&EE&ISO~$pid^^^$nameSpace&MR&ISO||$pName||$pDoB|$pSex||||||||||||||2"

# Writes line 1 to the new HL7 message. The HL7 message's name is made of the Namespace provided (in UPPER case) with the PID entered. For example MGUH_12345678.hl7
echo ${hl7Line1^^} > /tmp/${nameSpace}_${pid}.hl7
# Appens line 2 to the new HL7 message
echo ${hl7Line2^^} >> /tmp/${nameSpace}_${pid}.hl7

# After creating the HL7 message, it will send the message
hl7snd -d localhost:2398 -f /tmp/${nameSpace}_${pid}.hl7

# After sending in the HL7 message, cleans up by removing the *.hl7 file
rm /tmp/${nameSpace}_${pid}.hl7
