log.debug("IA QC Morpher - START - Reconciling SOP {} with the matching MWL entry.", get(SOPInstanceUID));

log.debug("IA QC Morpher - Going to work from MWL entry {}", mwlEntry);

LOAD('../common_medstar.groovy')
MedStar ms = new MedStar(log)

set('AccessionNumber', mwlEntry.get('AccessionNumber'))
 
set('ReferringPhysicianName', mwlEntry.get('ReferringPhysicianName'))
 
set('PatientID', mwlEntry.get('PatientID'))
set('IssuerOfPatientID', ms.ns_to_ipid_map[mwlEntry.get('IssuerOfPatientID')])
set('IssuerOfPatientIDQualifiersSequence/UniversalEntityID', mwlEntry.get('IssuerOfPatientIDQualifiersSequence/UniversalEntityID'))
set('IssuerOfPatientIDQualifiersSequence/UniversalEntityIDType', mwlEntry.get('IssuerOfPatientIDQualifiersSequence/UniversalEntityIDType'))
 
set('PatientName', mwlEntry.get('PatientName'))
set('PatientSex', mwlEntry.get('PatientSex'))
set('PatientBirthDate', mwlEntry.get('PatientBirthDate'))
set('PatientWeight', mwlEntry.get('PatientWeight'))
set('PatientState', mwlEntry.get('PatientState'))
 
set('MedicalAlerts', mwlEntry.get('MedicalAlerts'))
set('RequestingPhysician', mwlEntry.get('RequestingPhysician'))
 
set('RequestedProcedureID', mwlEntry.get('RequestedProcedureID'))
set('RequestedProcedurePriority', mwlEntry.get('RequestedProcedurePriority'))
 
set('OrderEnteredBy', mwlEntry.get('OrderEnteredBy'))
set('OrderEntererLocation', mwlEntry.get('OrderEntererLocation'))
set('OrderCallbackPhoneNumber', mwlEntry.get('OrderCallbackPhoneNumber'))
 
set('AdmissionID', mwlEntry.get('AdmissionID'))
set('IssuerOfAdmissionID', mwlEntry.get('IssuerOfAdmissionID'))
 
set('RequestedProcedureDescription', mwlEntry.get('RequestedProcedureDescription'))
set('RequestedProcedureCodeSequence/CodeValue', mwlEntry.get('RequestedProcedureCodeSequence/CodeValue'))
set('RequestedProcedureCodeSequence/CodingSchemeDesignator', mwlEntry.get('RequestedProcedureCodeSequence/CodingSchemeDesignator'))
set('RequestedProcedureCodeSequence/CodeMeaning', mwlEntry.get('RequestedProcedureCodeSequence/CodeMeaning'))
 
log.debug("IA QC Morpher - END - Finished reconciling SOP {}", get(SOPInstanceUID));
