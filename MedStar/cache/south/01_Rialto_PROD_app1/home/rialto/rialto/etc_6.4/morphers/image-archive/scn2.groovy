import org.joda.time.DateTime
import org.joda.time.format.DateTimeFormat
import java.text.SimpleDateFormat

def scriptName = "IA SCN Morpher2 - ";

log.info(scriptName + "START")

LOAD('../common_medstar_dicom.groovy')
MedStar_DICOM ms = new MedStar_DICOM(log, input)
sdf = new SimpleDateFormat("yyyyMMddHHmmss")

log.debug(scriptName + "Calling AE Title is {} for input:\n{}", getCallingAETitle(), input);

def idn = input.get("InstitutionalDepartmentName")
if (idn == null) {
    idn = "OTHER";
}

// Determine what type of study it is
def rad_study = false;
//if ("RADIOLOGY".equalsIgnoreCase(idn)) {
    rad_study = true;
//}

if (!rad_study) {
    log.debug(scriptName + "InstitutionalDepartmentName is {}, will NOT sent SCN message out...", idn);
    log.info(scriptName + "END")
    return false
} else {
    log.debug(scriptName + "InstitutionalDepartmentName is {}, will sent SCN message out...", idn);
}

initialize( 'ORM', 'O01', '2.3' );
output.getMessage().addNonstandardSegment('IPC')

log.info(scriptName + "SCN Replay is disabled")
log.info(scriptName + "END")
return false;

