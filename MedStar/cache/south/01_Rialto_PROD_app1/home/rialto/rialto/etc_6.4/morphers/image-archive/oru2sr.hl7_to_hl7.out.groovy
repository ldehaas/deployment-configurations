def scriptName = "OUR2SR HL7 Outbound Morpher - "

log.info(scriptName + "START...")
log.debug(scriptName + "input:\n{}", input)

log.debug(scriptName + "about to set MSA-1 segment to AA")
set('/.MSA-1', 'AA')

log.debug(scriptName + "output:\n{}", output)
log.info(scriptName + "END...")
