import org.joda.time.DateTime
import org.joda.time.format.DateTimeFormat
import java.text.SimpleDateFormat

def scriptName = "IA SCN Morpher2 - ";

log.info(scriptName + "START")

LOAD('../common_medstar_dicom.groovy')
MedStar_DICOM ms = new MedStar_DICOM(log, input)
sdf = new SimpleDateFormat("yyyyMMddHHmmss")

log.debug(scriptName + "Calling AE Title is {} for input:\n{}", getCallingAETitle(), input);

def siuid = input.get("StudyInstanceUID")
if (siuid == null) {
    siuid = "UNKNOWN";
}

def idn = input.get("InstitutionalDepartmentName")
if (idn == null) {
    idn = "OTHER";
}

// Determine what type of study it is
def rad_study = false;
//if ("RADIOLOGY".equalsIgnoreCase(idn)) {
    rad_study = true;
//}

if (!rad_study) {
    log.debug(scriptName + "SIUID: {}, InstitutionalDepartmentName is {}, will NOT sent SCN message out...", siuid, idn);
    log.info(scriptName + "END")
    return false
} else {
    log.debug(scriptName + "SIUID: {}, InstitutionalDepartmentName is {}, will sent SCN message out...", siuid, idn);
}

initialize( 'ORM', 'O01', '2.3' );
output.getMessage().addNonstandardSegment('IPC')

set('MSH-7', sdf.format(new Date()))
setPersonName('PID-5', input.get(PatientName));
/* set('PID-3-1', input.get(PatientID)); */

def issuerOfPatientID = input.get(IssuerOfPatientID);
def namespace = universal_id = universal_id_type = "";
if (issuerOfPatientID != null && issuerOfPatientID.contains("&")) {
    parts = issuerOfPatientID.split("&");
    if (parts.length == 3) {
        domain = com.karos.rtk.common.Domain.parse(issuerOfPatientID)
        namespace = domain.namespaceID
        universal_id = domain.domainUUID
        universal_id_type = domain.domainUUIDtype
    }
} else {
    namespace = issuerOfPatientID
}

if (namespace != null && !ms.ns_to_ipid_map.keySet().contains(namespace)) {
    def hl7_namespace =  ms.ns_to_ipid_map.find { it.value == namespace }?.key
    if (hl7_namespace != null) {
        log.info(scriptName + "SIUID: {}, Mapping dicom namespace \"{}\" to hl7 namespace \"{}\"", siuid, namespace, hl7_namespace);
        namespace = hl7_namespace
    }
}

def temp_daysAgo = new DateTime().minusDays(2);
def temp_studyDate = input.get(0x00080020);
def temp_studyTime = input.get(0x00080030);

log.debug(scriptName + "DEBUG: SIUID: {}, StudyDate: {}", siuid, temp_studyDate);
log.debug(scriptName + "DEBUG: SIUID: {}, StudyTime: {}", siuid, temp_studyTime);
    
def temp_studyDateTime = new DateTime();
if (temp_studyTime.contains(":")) {
    temp_studyDateTime = DateTime.parse(temp_studyDate + " " + temp_studyTime.substring(0,8), DateTimeFormat.forPattern("yyyyMMdd HH:mm:ss"))
} else {
    if(temp_studyTime.length() > 4) {
        temp_studyDateTime = DateTime.parse(temp_studyDate + " " + temp_studyTime.substring(0,6), DateTimeFormat.forPattern("yyyyMMdd HHmmss"))
    } else {
        temp_studyDateTime = DateTime.parse(temp_studyDate + " " + temp_studyTime.substring(0,4), DateTimeFormat.forPattern("yyyyMMdd HHmm"))
    }
}
    
if (temp_studyDateTime.isAfter(temp_daysAgo)) {
    log.info(scriptName + "SIUID: {}, StudyDateTime = {} , StudyDate is within last 2 days, will NOT send SCN message.", siuid, temp_studyDateTime);
    log.info(scriptName + "END")
    return false;
} else {
     log.info(scriptName + "SIUID: {}, StudyDateTime = {} , it is older than 2 days, will proceed with morphing SCN message.", siuid, temp_studyDateTime);
}

if (namespace != null && namespace != "MSMHC")
{
    log.info(scriptName + "SIUID: {},, namespace \"{}\" is other than MSMHC, will NOT send SCN message.", siuid, namespace);
    log.info(scriptName + "END")
    return false;
}

if (namespace != null && namespace == "SHAH")
{
    // check if SHAH StudyDate is within last 7 days
    def daysAgo = new DateTime().minusDays(7);

    //{ 0x0008, 0x0020, 'DA', "Study Date" },
    def studyDate = input.get(0x00080020);
    log.debug(scriptName + "SIUID: {}, DEBUG: StudyDate: {}", siuid, studyDate);

    //{ 0x0008, 0x0030, 'TM', "Study Time" },
    def studyTime = input.get(0x00080030);
    log.debug(scriptName + "SIUID: {}, DEBUG: StudyTime: {}", siuid, studyTime);
    
    def studyDateTime = new DateTime();
    if (studyTime.contains(":")) {
        studyDateTime = DateTime.parse(studyDate + " " + studyTime.substring(0,8), DateTimeFormat.forPattern("yyyyMMdd HH:mm:ss"))
    } else {
        studyDateTime = DateTime.parse(studyDate + " " + studyTime.substring(0,6), DateTimeFormat.forPattern("yyyyMMdd HHmmss"))
    }

    if (studyDateTime.isBefore(daysAgo)) {
        log.info(scriptName + "SIUID: {}, StudyDate is more than 7 days for SHAH study, will NOT send SCN message.", siuid);
        log.info(scriptName + "END")
        return false;
    } else {
        log.info(scriptName + "SIUID: {}, SHAH StudyDateTime = {} , it is within last 7 days, will proceed with morphing SCN message.", siuid, studyDateTime);
    }

    // add DOB to PID-7-1-1 (0010,0030)
    def patientDOB = input.get(PatientBirthDate)
    if (patientDOB != null) {
        log.debug(scriptName + "DEBUG: SIUID: {}, will set PID-7-1-1 (0010,0030) to have PatientBirthDate: {}", siuid, patientDOB);
        set('PID-7-1-1', patientDOB)
    } else {
        log.debug(scriptName + "DEBUG: SIUID: {}, PatientBirthDate appears to be null, will NOT populate PID-7-1-1 (0010,0030) ...", siuid);
    }
}

def patientID = input.get(PatientID);
if (namespace != null && namespace == "MUMH") {
    if (patientID != null && patientID != patientID.padLeft(9,'0')) { 
        log.info(scriptName + "SIUID: {}, padding PatientID with leading zeros for namespace \"{}\"", siuid, namespace);
        patientID = input.get(PatientID).padLeft(9,'0');
    }
}

set('PID-3-1', patientID);

set('PID-3-4-1', namespace);
if (!"".equals(universal_id) && !"".equals(universal_id_type)) {
    set('PID-3-4-2', universal_id);
    set('PID-3-4-3', universal_id_type);
}
if (type != null && type.toLowerCase().contains("delete")) {
    set('ORC-1', 'DC')
} else {
    set('ORC-1', 'NW')
}
set('OBR-1', '1')
set('OBR-3-1', input.get(AccessionNumber))
set('OBR-3-2', namespace)

if (type != null) {
    if ( input.get("IssuerOfAccessionNumberSequence/LocalNamespaceEntityID") == "MAS" ) {
       set('OBR-3-2-1', 'MAS')
       log.info(scriptName + "SIUID: {}, Setting Issuer of Accession Number to MAS based on IssuerOfAccessionNumberSequence/LocalNamespaceEntityID tag", siuid);
    } else if ( input.get(tag(0x0035, 0x1040)) == 'MedStar Radiology Network Bel Air' ) {
       set('OBR-3-2-1', 'MAS')
       log.info(scriptName + "SIUID: {}, Setting Issuer of Accession Number to MAS based on 0035,1040 tag", siuid);
    } else {
       log.info(scriptName + "SIUID: {}, Not setting OBR-3-2-1 segment to MAS", siuid);
       log.debug(scriptName + "SIUID: {}, DICOM tag(0008,0051) = {}", siuid, input.get(tag(0x0008, 0x0051)) );
       log.debug(scriptName + "SIUID: {}, DICOM tag(0035,1040) = {}", siuid, input.get(tag(0x0035, 0x1040)) );
    }
} else {
    log.info(scriptName + "SIUID: {}, Not setting Issuer of Accession Number - type is null!!!", siuid);
}

set('OBR-4-1', input.get(RequestedProcedureID))
def study_description = input.get(StudyDescription)
if (study_description != null)  {
    study_description = study_description.replaceAll("\\^.*", "")
    set('OBR-4-2', study_description)
}
set('OBR-7', input.get(StudyDate))
set('OBR-25', 'I')
set('OBX-1', '1')
set('OBX-2', 'TX')
set('OBX-3-1-2', 'GDT')
set('OBX-5', 'Y')
set('IPC-3', input.get(StudyInstanceUID))

if (namespace != null && namespace == "SHAH")
{
    def study_desc2 = input.get(StudyDescription)
    if (study_desc2 != null) {
        def modality_type = "";
        if ( study_desc2.indexOf(" ") != null && study_desc2.indexOf(" ") > 0 ) {
            modality_type = study_desc2.substring(0, study_desc2.indexOf(" "));
        } else {
            modality_type = study_desc2;
        }
        log.info( scriptName + "SIUID: {}, setting IPC-5 to Modality Type = {}", siuid, modality_type);
        set('IPC-5', modality_type )
    }
}

log.info(scriptName + "END")
