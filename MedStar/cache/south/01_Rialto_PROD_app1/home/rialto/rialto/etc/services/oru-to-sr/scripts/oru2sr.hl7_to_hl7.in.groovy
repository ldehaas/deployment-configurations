def scriptName = "ORU2SR HL7 Inbound Morpher - "

log.info(scriptName + "START...")
log.debug(scriptName + "input:\n{}", input)

LOAD('/home/rialto/rialto/etc/services/common/common_medstar_hl7.groovy')

MedStar_HL7 ms = new MedStar_HL7(log, input)

ms.groomPids()

ms.moveSecondRepetitionToFirstRepetition()

log.debug(scriptName + "output:\n{}", output)
log.info(scriptName + "END...")
