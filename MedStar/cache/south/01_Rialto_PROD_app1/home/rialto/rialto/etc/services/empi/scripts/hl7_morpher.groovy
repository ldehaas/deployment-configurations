def scriptName="EMPI Inbound HL7 Morpher - "

log.info(scriptName + "START ...")

//log.debug(scriptName + "input: {}", input)

def sendingApplication = get('MSH-3')
def sendingFacility = get('MSH-4')

if (sendingApplication != null && sendingFacility != null) {
    log.debug(scriptName + "Sending Application: {}, Sending Facility: {}", sendingApplication, sendingFacility)

    if ( 'PIF_MORPHER'.equalsIgnoreCase(sendingApplication) ) {
        if ( 'SHAH'.equalsIgnoreCase(sendingFacility) ) {
            log.debug(scriptName + "will populate EMPI for SHAH patient...")
        }else if ( 'MPP'.equalsIgnoreCase(sendingFacility) ) {
            log.debug(scriptName + "will populate EMPI for MPP patient...") 
        }else {
            log.debug(scriptName + "HL7 message from PIF_MORPHER, not a SHAH or MPP patient, will NOT proceed")
            log.info(scriptName + "END ...")
            return false
        }
     if ( 'PIF_MORPHER'.equalsIgnoreCase(sendingApplication) ) {
        if ( 'MOF'.equalsIgnoreCase(sendingFacility) ) {
            log.debug(scriptName + "will populate EMPI for MOF patient...")
        }else if ( 'MPP'.equalsIgnoreCase(sendingFacility) ) {
            log.debug(scriptName + "will populate EMPI for MPP patient...")
        }else {
            log.debug(scriptName + "HL7 message from PIF_MORPHER, not a MOF or MPP patient, will NOT proceed")
            log.info(scriptName + "END ...")
            return false
        }

    } else {
        log.debug(scriptName + "HL7 message NOT from PIF_MORPHER...")
    }
    log.debug(scriptName + "input: \n{}", input)
} else {
    if ( sendingApplication == null ) {
        log.debug(scriptName + "Sending Application is null, will NOT proceed!!!")
        log.info(scriptName + "END ...")
        return false
    }
    if ( sendingFacility == null ) {
        log.debug(scriptName + "Sending Facility is null, will NOT proceed!!!")
        log.info(scriptName + "END ...")
        return false
    }
}

log.info(scriptName + "END ...")

