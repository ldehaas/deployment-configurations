import org.joda.time.LocalDateTime;
import org.joda.time.DateTime;

def scriptName = "ILM Policy (no-op) - ";

def sleepDuration = 5000;
def sleepDurationHR = sleepDuration.intdiv(1000);

log.debug(scriptName + "SIUID: {}, AccN: {}, about to start processing", study.getStudyInstanceUID(), study.get("AccessionNumber") );

log.debug(scriptName + "SIUID: {}, AccN: {}, sleep for {} seconds...", study.getStudyInstanceUID(), study.get("AccessionNumber"), sleepDurationHR);
sleep(sleepDuration);

log.debug(scriptName + "SIUID: {}, AccN: {}, done processing", study.getStudyInstanceUID(), study.get("AccessionNumber") );

