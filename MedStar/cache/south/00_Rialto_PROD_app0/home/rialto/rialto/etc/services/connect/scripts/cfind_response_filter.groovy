import org.joda.time.LocalDateTime;
import org.joda.time.DateTime;
import org.joda.time.format.DateTimeFormat;
import org.joda.time.format.DateTimeFormatter;

def scriptName = "Connect CFind Response Filter - ";
def validPriorList = [];
def suidList = [];
def deDuplicated = [];
def sendingApplication = order.get("MSH-3")
DateTime currentDateTime = new DateTime();
def earliestStudyDate = currentDateTime.minusMonths(63);
def latestStudyDate = currentDateTime.minusDays(7);
def modalitiesInStudy = order.get("OBR-24")
def studiesToFetch = [];
log.info(scriptName + "START");
if (modalitiesInStudy != null) {
   modalitiesInStudy = new ArrayList<String>(Arrays.asList(modalitiesInStudy.split("\\\\")));
   modalitiesInStudy.remove("SR")
   modalitiesInStudy.remove("PR")
   modalitiesInStudy.remove("OT")
   modalitiesInStudy.remove("KO")
} else {
   // If we don't have a list of modalities then don't fetch anything
   return []
}
if (modalitiesInStudy.size() == 0) {
   // If all the modalities were filtered out then don't fetch anything
   return []
}
log.debug(scriptName + "sendingApplication is: {}", sendingApplication)
if ("RIALTO_IA".equalsIgnoreCase(sendingApplication)) {
   log.debug(scriptName + "Modalities from SCN from Image Archive in OBR-24 = {}", modalitiesInStudy);
}
log.debug(scriptName + "CurrentDatetime {}, earliestStudyDate {}", currentDateTime, earliestStudyDate);
log.debug(scriptName + "Complete list of studies being examined:\n{}",
      inputs.collect({ it.get(StudyInstanceUID) + ": " + it.getDate(StudyDate, StudyTime) }).join("\n"));
// Add only studies within the valid date range to validPriorList
inputs.each {
  if (it.getDate(StudyDate, StudyTime) < earliestStudyDate) {
       log.debug(scriptName + "Prior {} is older than '{}'. This study has a date and time of '{}' and order modality {}.  Not going to prefetch study.", it.get(StudyInstanceUID), earliestStudyDate, it.getDate(StudyDate, StudyTime), modalitiesInStudy);
  } else if (it.getDate(StudyDate, StudyTime) > latestStudyDate) {
        log.debug(scriptName + "Prior {} is younger than '{}' and younger than 7 days. This study has a date and time of '{}'. Not going to prefetch study.", it.get(StudyInstanceUID), latestStudyDate, it.getDate(StudyDate, StudyTime));
  } else {
        log.debug(scriptName + "Prior {} is younger than '{}' and older than '{}'. This study has a date and time of '{}'. Going to prefetch study.", it.get(StudyInstanceUID), earliestStudyDate, latestStudyDate, it.getDate(StudyDate, StudyTime));
        validPriorList.add(it);
  }
}
// For each modality in the SCN, fetch the three most recent studies of the same type
for (modality in modalitiesInStudy) {
    def studiesThisModality = [];
    // Check each study in the filtered list to see if it contains the desired modality
    for (study in validPriorList) {
    def modalitiesInThisStudy = study.get(ModalitiesInStudy)
    if (modalitiesInThisStudy == null) {
        break
    }
    modalitiesInThisStudy = Arrays.asList(modalitiesInThisStudy.split("\\\\"))
    
    if (modalitiesInThisStudy.contains(modality)) {
        studiesThisModality.add(study)
        log.debug(scriptName + "Modality of prefetched study is {}", modality)
    }
    }
    
    // Sort the matching studies by date
    studiesThisModality.sort { a, b -> a.getDate(StudyDate, StudyTime) == b.getDate(StudyDate, StudyTime) ? 0 :
                  a.getDate(StudyDate, StudyTime) < b.getDate(StudyDate, StudyTime) ? -1 : 1 }
    // Throw away all but the three most recent
    for (i=0; i<3; i++) {
    if (i < studiesThisModality.size()) {
        studiesToFetch.add(studiesThisModality[i])
    }
    }
}
// Filter out duplicates
studiesToFetch.each {
   if (!suidList.contains(it.get(StudyInstanceUID))) {
       suidList.add(it.get(StudyInstanceUID));
       deDuplicated.add(it);
   }
}
log.debug(scriptName + "Complete list of studies that will be retrieved:\n{}",
      deDuplicated.collect({ it.get(StudyInstanceUID) + ": " + it.getDate(StudyDate, StudyTime) }).join("\n"));
log.info(scriptName + "END");
return deDuplicated;
