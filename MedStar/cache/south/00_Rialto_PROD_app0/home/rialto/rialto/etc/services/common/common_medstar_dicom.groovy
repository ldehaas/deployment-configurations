/**
 * DICOM related configuration specific to the MedStar environment
 */

LOAD('/home/rialto/rialto/etc/services/common/common_medstar.groovy')
LOAD('/home/rialto/rialto/etc/services/common/common_dicom.groovy')

import java.security.MessageDigest;
import javax.xml.bind.DatatypeConverter;
import com.karos.groovy.morph.dcm.DicomScriptingAPI;

class MedStar_DICOM extends MedStar {

    def sop = null
    def dc = null

    def MedStar_DICOM(log, sop) {
        super(log)
        this.sop = sop
        dc = new DICOM_Common(log, sop)
    }

    def fixSOPForHangingProtocol() {
        dc.processSOPStudyTime()
        dc.processSOPBodyPart()
        dc.processSOPViewPosition()

        //The line that follows this comment was commented out on July 20, 2016 by Steve V
        //The reason was that the information within the data was not correct enough to make
        //decisions for Laterality so it was removed to mitigate any further incorrectness. 
        //
        //dc.processSOPLaterality()
        //

        dc.processSOPStudyDescription()
        dc.processSOPSeriesDescription()
    }

    def stampUniversalEntityId() {
        def currIPID = sop.get(IssuerOfPatientID)
        def currUEID = sop.get("IssuerOfPatientIDQualifiersSequence/UniversalEntityID") 
        def currUEIDType = sop.get("IssuerOfPatientIDQualifiersSequence/UniversalEntityIDType")
        if (currIPID != null && currUEID != null && currUEIDType != null) {
            log.info("MedStar DICOM Morpher - The DICOM object (SOPInstanceUID, if set, {}) has all important patient identifier qualifier values set ({}&{}&{}). Not stamping the univeral id", sop.get("SOPInstanceUID"), currIPID, currUEID, currUEIDType);
            return
        }

        if (currIPID != null && ns_to_ipid_map.keySet().contains(currIPID))  {
            def newNS = ns_to_ipid_map[currIPID]
            log.info("MedStar DICOM Morpher - The DICOM object (SOPInstanceUID, if set, {}) has an IPID value \"{}\" that will be changed to \"{}\"", sop.get("SOPInstanceUID"), currIPID, newNS)
            currIPID = newNS
        }

        if (currIPID == null || !ipid_to_universalid_map.keySet().contains(currIPID)) {
            log.warn("MedStar DICOM Morpher - The DICOM object (SOPInstanceUID, if set, {}) has an unknown IPID value \"{}\"!", sop.get("SOPInstanceUID"), currIPID)
            currIPID = UNKNOWN_NS 
        }

        currUEID = ipid_to_universalid_map[currIPID]
        if (currUEID == null) {
            currUEID = ipid_to_universalid_map[UNKNOWN_NS]
        }

        updateIssuerOfPatientID(currIPID, currUEID, 'ISO')
    }

    def updateIssuerOfPatientID(namespace, universalId, universalIdType) {
        log.info("MedStar DICOM Morpher - The DICOM object (SOPInstanceUID, if set, {}), setting namespace to {}, setting universal id to {}, setting universal id type to {}", sop.get("SOPInstanceUID"), namespace, universalId, universalIdType)

        sop.set(IssuerOfPatientID, namespace)
        sop.set("IssuerOfPatientIDQualifiersSequence/UniversalEntityID", universalId, VR.LO)
        sop.set("IssuerOfPatientIDQualifiersSequence/UniversalEntityIDType", universalIdType, VR.LO)
    }

    def stampInstitutionalDepartmentName(calledAET) {
        def newIDN = null
        if ("ERS_RAD_NORTH".equalsIgnoreCase(calledAET) || "ERS_RAD_SOUTH".equalsIgnoreCase(calledAET) || "RIALTO_CACHE".equalsIgnoreCase(calledAET)) {
            newIDN = "RADIOLOGY"
        } else if ("ERS_CAR_NORTH".equalsIgnoreCase(calledAET) || "ERS_CAR_SOUTH".equalsIgnoreCase(calledAET)) {
            newIDN = "CARDIOLOGY"
        } else if ("ERS_OTHER_NORTH".equalsIgnoreCase(calledAET) || "ERS_OTHER_SOUTH".equalsIgnoreCase(calledAET)) {
            newIDN = "OTHER"
        }

        if (newIDN == null) {
            log.warn("MedStar DICOM Morpher - The DICOM object (SOPInstanceUID, if set, {}), not setting institutional department name because the calledAET used does not indicate the classification of study", sop.get("SOPInstanceUID"))
            return
        }
 
        if (sop.get(InstitutionalDepartmentName) != null) {
            def curr_idn = sop.get(InstitutionalDepartmentName)

            log.info("MedStar DICOM Morpher - The DICOM object (SOPInstanceUID, if set, {}), going to move existing institutional department name: {} to a Karos specific private tag", sop.get("SOPInstanceUID"), curr_idn)

            dc.setPrivate(sop, 0x0035, 0x0030, "KAROS PRESERVED INFO 1.0", curr_idn, VR.LO)
        }


        log.info("MedStar DICOM Morpher - The DICOM object (SOPInstanceUID, if set, {}), setting institutional department name to {}", sop.get("SOPInstanceUID"), newIDN);
        sop.set("InstitutionalDepartmentName", newIDN, VR.LO)

        //if ( newIDN != null && !"RADIOLOGY".equalsIgnoreCase(newIDN) ) {
        //    sop.set("StudyStatusID", "VER", VR.CS);
        //}
        if ( newIDN != null )
        {
            if ( "RADIOLOGY".equalsIgnoreCase(newIDN) ) {
                log.info("MedStar DICOM Morpher - NOT setting Study Status ID to VER since Institutional Department Name is RADIOLOGY")
            } else if ( "CARDIOLOGY".equalsIgnoreCase(newIDN) ) {
                sop.set("StudyStatusID", "VER", VR.CS);
                log.info("MedStar DICOM Morpher - Setting Study Status ID to VER since Institutional Department Name is CARDIOLOGY")
            } else if ( "OTHER".equalsIgnoreCase(newIDN) ) {
                sop.set("StudyStatusID", "VER", VR.CS);
                log.info("MedStar DICOM Morpher - Setting Study Status ID to VER since Institutional Department Name is OTHER")
            } else {
                log.info("MedStar DICOM Morpher - NOT setting Study Status ID to VER since Institutional Department Name is {}", newIDN)
            }
        } else {
            log.info("MedStar DICOM Morpher - Institutional Department Name is null...")
        }
    }

    def stampInstitutionalName() {

        def NRH_check = sop.get("0040A0A1")
        if (NRH_check == "NRH") {
            log.info("Study is from the NRH router so not updating the institution name - as per KHC6058")
            return
        }

        def curr_ipid = sop.get(IssuerOfPatientID)
        if (curr_ipid == null) {
            log.info("MedStar DICOM Morpher - The DICOM object (SOPInstanceUID, if set, {}), has no issuer of patient id set! Not going to update the institution name", sop.get("SOPInstanceUID"))
            return
        }

        def curr_namespace =  ns_to_ipid_map.find { it.value == curr_ipid }?.key
        if (curr_namespace == null) {
            log.info("MedStar DICOM Morpher - The DICOM object (SOPInstanceUID, if set, {}), has no namespace that can be looked up! Not going to update the institution name", sop.get("SOPInstanceUID"))
            return
        }

        def curr_in = sop.get(InstitutionName)
        if (curr_in != null) {
            log.info("MedStar DICOM Morpher - The DICOM object (SOPInstanceUID, if set, {}), going to move existing institution name: {} to a Karos specific private tag", sop.get("SOPInstanceUID"), curr_in)
            dc.setPrivate(sop, 0x0035, 0x0040, "KAROS PRESERVED INFO 1.0", curr_in, VR.LO)
        }

        log.info("MedStar DICOM Morpher - The DICOM object (SOPInstanceUID, if set, {}), setting institution name to {}", sop.get("SOPInstanceUID"), curr_namespace);
        sop.set("InstitutionName", curr_namespace, VR.LO)
    }

    def copyProcedureCodeToPrivateTags() {
        def currProcedureCodeSequenceCodeValue = sop.get("ProcedureCodeSequence/CodeValue")
        def currProcedureCodeSequenceCodeSchemeDesignator = sop.get("ProcedureCodeSequence/CodingSchemeDesignator")
        def currProcedureCodeSequenceCodeMeaning = sop.get("ProcedureCodeSequence/CodeMeaning")

        if (currProcedureCodeSequenceCodeValue != null ||
            currProcedureCodeSequenceCodeSchemeDesignator != null ||
            currProcedureCodeSequenceCodeMeaning != null) {

            log.info("MedStar DICOM Morpher - The DICOM object (SOPInstanceUID, if set, {}), going to move existing item #1 of procedure code sequence (value: {}, scheme: {}, meaning: {}) to a Karos specific private tag", sop.get("SOPInstanceUID"), currProcedureCodeSequenceCodeValue, currProcedureCodeSequenceCodeSchemeDesignator, currProcedureCodeSequenceCodeMeaning)

            dc.setPrivate(sop, 0x0035, 0x0050, "KAROS PRESERVED INFO 1.0", currProcedureCodeSequenceCodeValue, VR.LO)
            dc.setPrivate(sop, 0x0035, 0x0051, "KAROS PRESERVED INFO 1.0", currProcedureCodeSequenceCodeSchemeDesignator, VR.LO)
            dc.setPrivate(sop, 0x0035, 0x0052, "KAROS PRESERVED INFO 1.0", currProcedureCodeSequenceCodeMeaning, VR.LO)
        }
    }

    def uniqueTemporaryId() {
        def curr_patientid = sop.get(PatientID)
        def new_patientid = curr_patientid
        if (curr_patientid != null && curr_patientid == 'MSH-TEMP_ID') {
            def siuid = sop.get(StudyInstanceUID)
            def md = MessageDigest.getInstance('SHA1')
            new_patientid = curr_patientid + "-" + DatatypeConverter.printHexBinary(md.digest(siuid.getBytes()))

            sop.set("PatientID", new_patientid, VR.LO)
        }
    }
}

