log.info("Connect HL7 to CFind Morpher - START")

def messageType = get('/.MSH-9-1')
def triggerEvent = get('/.MSH-9-2')

if (!"ORM".equalsIgnoreCase(messageType) && !"SIU".equalsIgnoreCase(messageType)) {
    log.warn('Connect HL7 to CFind Morpher - An unactionable message was provided, taking no action on message {}^{}', messageType, triggerEvent)
    return false
}

LOAD('/home/rialto/rialto/etc/services/common/common_medstar_dicom.groovy')
MedStar_DICOM ms = new MedStar_DICOM(log, null)

try {
    patientIds = input.getList('/.PID-3(*)-1')
} catch (Exception e) {
    log.error('Connect HL7 to CFind Morpher - A problem occurred when getting the list of PIDs: ' + e)
    return false
}

def patientId = null
def namespace = null
for (int i = 0; i < patientIds.size(); i++) {
    if (namespace != null) {
        break
    }

    def currNamespace = input.get('/.PID-3(' + i + ')-4-1')
    switch (currNamespace) {
        case 'MFSMC':
        case 'MGSH':
        case 'MHH':
        case 'MUMH':
        case 'MGUH':
        case 'MWHC':
        case 'MNRH':
        case 'MMMC':
        case 'MSMH':
        case 'MAS':
        case 'MAS2':
        case 'MPP':
        case 'MSC':
        case 'MSMHC':
        case 'OUT':
        case 'SHAH':
        case 'MOF':
            patientId = input.get('/.PID-3(' + i + ')-1')
            namespace = currNamespace
            break
    }
}

if (patientId == null || namespace == null) {
    log.error('Connect HL7 to CFind Morpher - No PID could be found in the trigger message from a site we know about')
    return false
}

def ipid = ms.ns_to_ipid_map[namespace]
if (ipid == null) {
    log.error("Connect HL7 to CFind Morpher - Could not identify the IssuerOfPatientID from the namespace: {}", namespace)
}

set(PatientID, patientId);

if (namespace != null && namespace == "MUMH") {
    if (patientId != patientId.replaceFirst("^0+(?!\$)", "")) {
        log.info("Connect HL7 to CFind Morpher - stripping leading zeros for MUMH patient, new PID = {}", patientId.replaceFirst("^0+(?!\$)", ""));
        set(PatientID, patientId.replaceFirst("^0+(?!\$)", ""));
    }
}

set(ModalitiesInStudy, null)

set(IssuerOfPatientID, ipid);

set(NumberOfStudyRelatedSeries, null)

log.info("Connect HL7 to CFind Morpher - Going to issue CFind: {}", output)

log.info("Connect HL7 to CFind Morpher - END")
