/**
 *    ILM policy for MedStar - PROD
 */
import org.joda.time.LocalDateTime;
import org.joda.time.DateTime;

log.debug("ILM policies START - Executing against study with StudyInstanceUID {}", study.getStudyInstanceUID());

// Constants
// North IMGs
//FSH IMG 172.23.8.65/10104/FSHINSITE01
//GSH IMG 172.16.80.66/10104/GSHINSITE01
//HHC IMG 172.16.209.107/10104/indexStore-P0HS
//UMH IMG 172.17.80.79/10104/indexStore-P0H2
LOCAL_FSH_IMG_AET = "FSHINSITE01";
LOCAL_GSH_IMG_AET = "GSHINSITE01";
LOCAL_HHC_IMG_AET = "indexStore-P0HS";
LOCAL_UMH_IMG_AET = "indexStore-P0H2";

// South IMGs
//South eIMG 198.50.67.28/10104/indexStore-P0T7
//GUH IMG 172.19.1.192/10104/indexStore-P0JG
//MMMC IMG 172.18.7.202/10104/indexStore-P0RA
//SMH IMG 172.30.8.50/10104/indexStore-P0RB
//WHC IMG 172.25.120.25/10104/indexStore-P005
SOUTH_INDEX_LINK_AET = "indexStore-P0M5";
SOUTH_eIMG_AET = "indexStore-P0T7";
LOCAL_GUH_IMG_AET = "indexStore-P0JG";
LOCAL_MMC_IMG_AET = "indexStore-P0RA";
LOCAL_SMH_IMG_AET = "indexStore-P0RB";
LOCAL_WHC_IMG_AET = "indexStore-P005";

//Added fot KHC13618
//LOCAL_MOF_IMG_AET = "indexQuery-P0LY";

def idn = study.get("InstitutionalDepartmentName");
if (idn == null) {
  idn = "OTHER";
}

// Determine what type of study it is
def rad_study = false;
if ("RADIOLOGY".equalsIgnoreCase(idn)) {
  rad_study = true;
}

// If the study was prefetched, mark it as a purge candidate and move on
if (timeline.isPrefetched()) {
  log.info("ILM policies - {} study with StudyInstanceUID {} can be marked as a candidate for purging since it was prefetched", idn, study.getStudyInstanceUID());
  ops.flagAsPurgeCandidate();
  log.debug("ILM policies END - Done looking at study with StudyInstanceUID {}", study.getStudyInstanceUID());
  return;
}

def studyInstanceAvailability = study.get(0x00080056);
log.info("ILM policies - DEBUG: {} study with StudyInstanceUID {} has studyInstanceAvailability = {}", idn, study.getStudyInstanceUID(), studyInstanceAvailability);
if ("UNAVAILABLE".equalsIgnoreCase(studyInstanceAvailability)) {
  log.info("ILM policies - {} study with StudyInstanceUID {} is not available or not ONLINE, skipping...", idn, study.getStudyInstanceUID());
  log.debug("ILM policies END - Done looking at study with StudyInstanceUID {}", study.getStudyInstanceUID());
  return;
}

//
// The rest of the logic deals with studies that must be forwarded before being marked as purge candidates
//

// Define where to store long-term
def lta = null;
def ipid = study.get("IssuerOfPatientID");

switch (ipid.toUpperCase()) {
    case "MEDSTAR":
        lta = SOUTH_eIMG_AET;
        break;

    case "FSH":
        lta = LOCAL_FSH_IMG_AET;
        break;

    case "GSH":
        lta = LOCAL_GSH_IMG_AET;
        break;

    case "HHC":
        lta = LOCAL_HHC_IMG_AET;
        break;

    case "UMH":
        lta = LOCAL_UMH_IMG_AET;
        break;

    case "GUH":
        lta = LOCAL_GUH_IMG_AET;
        break;

    case "MGI":
        lta = LOCAL_MMC_IMG_AET;
        break;

    case "MSMHC":
        lta = SOUTH_eIMG_AET;
        break;

    case "MSMH":
        lta = LOCAL_SMH_IMG_AET;
        break;

    case "WB4":
        lta = LOCAL_WHC_IMG_AET;
        break;

    case "6B4":
        lta = LOCAL_WHC_IMG_AET;
        break;

    case "MAS":
        lta = SOUTH_eIMG_AET;
        break;

    case "MPP":
        lta = SOUTH_eIMG_AET;
        break;

    case "MSC":
        lta = SOUTH_eIMG_AET;
        break;

    case "SHAH":
        lta = SOUTH_eIMG_AET;
        break;

    default:
        lta = SOUTH_eIMG_AET;
}

//Added fot KHC13617
if ("MOF".equalsIgnoreCase(institutionName)) && if (study.isBefore(oneYearAgo)) {
    log.info("ILM policies - Study with StudyInstanceUID {} can be marked as a candidate for purging since the study date is set to be {}, and we will mark any studies being ready for purging that are older than one year from today (ie, {})", study.getStudyInstanceUID(), study.get("StudyDate"), oneYearAgo);
    ops.flagAsPurgeCandidate();
  }


// Deal with studies with an unknown long-term archive destination
// this is an extra 'catch all' block and we should never end up in side this block 
if (lta == null) {
  log.info("ILM policies - {} study with StudyInstanceUID {} is not a prefetched study, nor is the long-term archive system known because the issuer of patient id is {}", idn, study.getStudyInstanceUID(), ipid);

  if (timeline.wasFullyForwardedWithStorageCommitTo(SOUTH_eIMG_AET)) {
    log.info("ILM policies - {} study with StudyInstanceUID {} and unknown IPID {} is not a prefetch study and can be marked as a candidate for purging since he study has already been forwarded to {}", idn, study.getStudyInstanceUID(), ipid, SOUTH_eIMG_AET);
    ops.flagAsPurgeCandidate();
    log.debug("ILM policies END - Done looking at study with StudyInstanceUID {}", study.getStudyInstanceUID());
    return;
  } else {
    log.info("ILM policies - {} study with StudyInstanceUID {} and IPID {} can NOT be marked as a candidate for purging since the study has NOT been forwarded to {}", idn, study.getStudyInstanceUID(), ipid, SOUTH_eIMG_AET);
  }

  log.debug("ILM policies END - Done looking at study with StudyInstanceUID {}", study.getStudyInstanceUID());
  return;
}

// Check if the study can be purged (purging will only occur when space needs to be reclaimed)
if (!timeline.wasFullyForwardedWithStorageCommitTo(lta)) {
  log.info("ILM policies - {} study with StudyInstanceUID {} and IPID {} can NOT be marked as a candidate for purging since it has NOT been forwarded to {}", idn, study.getStudyInstanceUID(), ipid, lta);
  if (!rad_study) {
    // Other ologies - forward to long-term archive immediately
    log.info("ILM policies - {} study with StudyInstanceUID {} and IPID {} can be forwarded to {} since we forward non-Radiology studies immediately", idn, study.getStudyInstanceUID(), ipid, lta);
    ops.scheduleStudyForwardWithStorageCommit(lta);
  } else {
    // Radiology - release on ORU to eIMG
    if (timeline.isFinalReported()) {
      log.info("ILM policies - {} study with StudyInstanceUID {} and IPID {} can be forwarded to {} since there was a FINAL ORU observed for this study", idn, study.getStudyInstanceUID(), ipid, lta);
      ops.scheduleStudyForwardWithStorageCommit(lta);
    } else {
      minOruIdleDays = 21;
      gracePeriod = new DateTime().minusDays(minOruIdleDays);
      //log.info("ILM policies - DEBUG: {} study with StudyInstanceUID {} and IPID {} should not be updated since {} to qualify ...", idn, study.getStudyInstanceUID(), ipid, gracePeriod);
      if (gracePeriod.isAfter(study.getLastUpdateTime())) {
        log.info("ILM policies - {} study with StudyInstanceUID {} and IPID {} can be forwarded to {} since the study was last used {}, and we  forward studies for which no ORU was observed for {} days", idn, study.getStudyInstanceUID(), ipid, lta, study.getLastUpdateTime(), minOruIdleDays);
        ops.scheduleStudyForwardWithStorageCommit(lta);
      } else {
        log.info("ILM policies - {} study with StudyInstanceUID {} and IPID {} does NOT have a FINAL ORU and was last used {}, skipping...", idn, study.getStudyInstanceUID(), ipid, study.getLastUpdateTime());
      }
    }
  }
} else {
  log.info("ILM policies - {} study with StudyInstanceUID {} and IPID {} can be marked as a candidate for purging since it has been forwarded to {}", idn, study.getStudyInstanceUID(), ipid, lta);
  ops.flagAsPurgeCandidate();
}

log.debug("ILM policies END - Done looking at study with StudyInstanceUID {}", study.getStudyInstanceUID());

