if (get("SeriesNumber") == "9999") {
    log.warn("As a part of KHC4368, not sending SOP {} having a Series Number of 9999", get("StudyInstanceUID"));
    return false;
}
