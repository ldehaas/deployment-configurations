import java.time.LocalDateTime
import java.time.format.DateTimeFormatter
import java.time.format.DateTimeParseException
import java.time.temporal.ChronoUnit

log.debug("Reconciling SOP {} with the matching MWL entry.", get(SOPInstanceUID));
set('AccessionNumber', mwlEntry.get('AccessionNumber'))
set('IssuerOfAccessionNumberSequence/LocalNamespaceEntityID', mwlEntry.get('IssuerOfAccessionNumberSequence/LocalNamespaceEntityID'))
set('IssuerOfAccessionNumberSequence/UniversalEntityID', mwlEntry.get('IssuerOfAccessionNumberSequence/UniversalEntityID'))
set('IssuerOfAccessionNumberSequence/UniversalEntityIDType', mwlEntry.get('IssuerOfAccessionNumberSequence/UniversalEntityIDType'))

set('ReferringPhysicianName', mwlEntry.get('ReferringPhysicianName'))

set('PatientID', mwlEntry.get('PatientID'))
set('IssuerOfPatientID', mwlEntry.get('IssuerOfPatientID'))
set('IssuerOfPatientIDQualifiersSequence/UniversalEntityID', mwlEntry.get('IssuerOfPatientIDQualifiersSequence/UniversalEntityID'))
set('IssuerOfPatientIDQualifiersSequence/UniversalEntityIDType', mwlEntry.get('IssuerOfPatientIDQualifiersSequence/UniversalEntityIDType'))
//set('StudyDescription', mwlEntry.get('StudyDescription'))

set('PatientName', mwlEntry.get('PatientName'))
set('PatientSex', mwlEntry.get('PatientSex'))
set('PatientBirthDate', mwlEntry.get('PatientBirthDate'))
set('PatientWeight', mwlEntry.get('PatientWeight'))
set('PatientState', mwlEntry.get('PatientState'))

def patientDateOfBirth = get(PatientBirthDate)
def patientTimeOfBirth = get(PatientBirthTime)
def studyDate = get(StudyDate)
def studyTime = get(StudyTime)
def patientAge = get(PatientAge)

set('StudyDate', studyDate)
set('StudyTime', studyTime)

try {
    //if (patientAge == null) {
        log.warn("Failed during catch operation")
        set('PatientAge', calculateAge(patientDateOfBirth, patientTimeOfBirth, studyDate, studyTime))
    //}
} catch (Exception e) {
    log.warn("Failed to compute PatientAge. Proceeding without changing PatientAge. ", e)
}

set('MedicalAlerts', mwlEntry.get('MedicalAlerts'))
set('RequestingPhysician', mwlEntry.get('RequestingPhysician'))

set('RequestedProcedureID', mwlEntry.get('RequestedProcedureID'))
set('RequestedProcedurePriority', mwlEntry.get('RequestedProcedurePriority'))

set('OrderEnteredBy', mwlEntry.get('OrderEnteredBy'))
set('OrderEntererLocation', mwlEntry.get('OrderEntererLocation'))
set('OrderCallbackPhoneNumber', mwlEntry.get('OrderCallbackPhoneNumber'))

set('AdmissionID', mwlEntry.get('AdmissionID'))
set('IssuerOfAdmissionID', mwlEntry.get('IssuerOfAdmissionID'))

set('RequestedProcedureDescription', mwlEntry.get('RequestedProcedureDescription'))
set('RequestedProcedureCodeSequence/CodeValue', mwlEntry.get('RequestedProcedureCodeSequence/CodeValue'))
set('RequestedProcedureCodeSequence/CodingSchemeDesignator', mwlEntry.get('RequestedProcedureCodeSequence/CodingSchemeDesignator'))
set('RequestedProcedureCodeSequence/CodeMeaning', mwlEntry.get('RequestedProcedureCodeSequence/CodeMeaning'))

log.debug("Finished reconciling SOP {}", get(SOPInstanceUID));

// KHC14358 - Added St. Mary's (MSMH) and Southern Maryland (MSMHC) to change StudyDescription to RequestedProcedureDescription during QC
if (mwlEntry.get('IssuerOfAdmissionID') == 'MMMC' || mwlEntry.get('IssuerOfAdmissionID') == 'MSMH' || mwlEntry.get('IssuerOfAdmissionID') == 'MSMHC') {
   set('StudyDescription', mwlEntry.get('RequestedProcedureDescription'))
   }

String calculateAge(String patientDateOfBirth, String patientTimeOfBirth, String studyDate, String studyTime){
    def TIPPING_WEEKS_TO_DAYS = 4
    def TIPPING_MONTHS_TO_WEEKS = 3
    def TIPPING_YEARS_TO_MONTHS = 2

    if (patientDateOfBirth == null || studyDate == null) {
        return null
    }

    patientDateOfBirth = patientDateOfBirth.replace(":", "")
    studyDate = studyDate.replace(":", "")

    def patientDateTimeOfBirth
    def studyDateTime

    try {
        def dtf = DateTimeFormatter.ofPattern("yyyyMMddHHmmss")
        patientDateTimeOfBirth = LocalDateTime.parse(patientDateOfBirth + cleanTime(patientTimeOfBirth), dtf)
        studyDateTime = LocalDateTime.parse(studyDate + cleanTime(studyTime), dtf)
    } catch (DateTimeParseException e) {
        log.warn("in.groovy: Failed to parse PatientBirthDate or StudyDate.", e)
        return null
    }

    if (!patientDateTimeOfBirth.isBefore(studyDateTime)) {
        return null
    }

    def years = ChronoUnit.YEARS.between(patientDateTimeOfBirth, studyDateTime)
    def months = ChronoUnit.MONTHS.between(patientDateTimeOfBirth, studyDateTime)
    def weeks = ChronoUnit.WEEKS.between(patientDateTimeOfBirth, studyDateTime)
    def days = ChronoUnit.DAYS.between(patientDateTimeOfBirth, studyDateTime)
    def age = ""

    if (years > 999) {
        return null
    }
    if (years >= TIPPING_YEARS_TO_MONTHS) {
        age = years + "Y"
    } else if (months >= TIPPING_MONTHS_TO_WEEKS) {
        age = months + "M"
    } else if (weeks >= TIPPING_WEEKS_TO_DAYS) {
        age = weeks + "W"
    } else {
        age = days + "D"
    }

    return age.padLeft(4, "0")
}

String cleanTime(String str) {
    if (str == null) {
        return "000000"
    }
    str = str.replace(":", "")
    if (str.indexOf(".") == 6) {
        str = str.substring(0, 6)
    }
    if (str.contains(".") || str.length() > 6 || str.length()%2 == 1) {
        return "000000"
    }
    return str.padRight(6, "0")
}
