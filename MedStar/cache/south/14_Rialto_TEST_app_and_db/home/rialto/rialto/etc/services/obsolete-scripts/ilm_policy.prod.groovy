/**
 *    ILM policy for MedStar - PROD
 */
import org.joda.time.LocalDateTime;
import org.joda.time.DateTime;

log.debug("ILM policies START - Executing against study with StudyInstanceUID {}", study.getStudyInstanceUID());

// Constants
SOUTH_INDEX_LINK_AET = "indexStore-P0M5";
LOCAL_MMMC_IMG_AET = "indexStore-P0RA";
LOCAL_MGUH_IMG_AET = "indexStore-P0JG";
LOCAL_GUH_IMG_Mammo_AET = "indexStore-P0UY";

idn = study.get("InstitutionalDepartmentName")
if (idn == null) {
  idn = "OTHER";
}

// Determine what type of study it is
rad_study = false;
if ("RADIOLOGY".equalsIgnoreCase(idn)) {
  rad_study = true;
}

// If the study was prefetched, mark it as a purge candidate and move on
if (timeline.isPrefetched()) {
  log.info("ILM policies - {} study with StudyInstanceUID {} can be marked as a candidate for purging since it was prefetched", idn, study.getStudyInstanceUID());
  ops.flagAsPurgeCandidate();
  return;
}

//
// The rest of the logic deals with studies that must be forwarded before being marked as purge candidates
//

// All studies are forwarded to the South Index Link immediately
//if (!timeline.wasFullyForwardedTo(SOUTH_INDEX_LINK_AET)) {
//  log.info("ILM policies - {} study with StudyInstanceUID {} can be forwarded to {} since we forward immediately", idn, study.getStudyInstanceUID(), SOUTH_INDEX_LINK_AET);
//  ops.scheduleStudyForward(SOUTH_INDEX_LINK_AET);
//}

// Define where to store long-term
def modality = study.get("Modality");

lta = null;
ipid = study.get("IssuerOfPatientID")
if ("MGI".equalsIgnoreCase(ipid)) {
  lta = LOCAL_MMMC_IMG_AET;
} else if ("GUH".equalsIgnoreCase(ipid)) {
  lta = LOCAL_MGUH_IMG_AET;
  if ("MG".equalsIgnoreCase(modality)) {
    lta=LOCAL_GUH_IMG_Mammo_AET;
  }
}

// Deal with studies with an unknown long-term archive destination
if (lta == null) {
  log.info("ILM policies - {} study with StudyInstanceUID {} is not a prefetched study, nor is the long-term archive system known because the issuer of patient id is {}", idn, study.getStudyInstanceUID(), ipid);

  if (timeline.wasFullyForwardedTo(SOUTH_INDEX_LINK_AET)) {
    log.info("ILM policies - {} study with StudyInstanceUID {} can be marked as a candidate for purging. Even though it is not a prefetched study, the long-term archive system is unknown because the issuer of patient id is {} and the study has already been forwarded to {}", idn, study.getStudyInstanceUID(), ipid, SOUTH_INDEX_LINK_AET);
    ops.flagAsPurgeCandidate();
    return;
  }

  return;
}

if (!rad_study) {
  // Other ologies - forward to long-term archive immediately
  if (!timeline.wasFullyForwardedWithStorageCommitTo(lta)) {
    log.info("ILM policies - {} study with StudyInstanceUID {} can be forwarded to {} since we forward non-Radiology studies immediately", idn, study.getStudyInstanceUID(), lta);
    ops.scheduleStudyForwardWithStorageCommit(lta);
  }
} else {
  // Radiology - release on ORU to eIMG
  if (timeline.isFinalReported()) {
    if (!timeline.wasFullyForwardedWithStorageCommitTo(lta)) {
      log.info("ILM policies - {} study with StudyInstanceUID {} can be forwarded to {} since there was a FINAL ORU observed for this study", idn, study.getStudyInstanceUID(), lta);
      ops.scheduleStudyForwardWithStorageCommit(lta);
    }
  } else {
    oneMonthAgo = new DateTime().minusMonths(1);
    if (oneMonthAgo.isAfter(study.getLastUpdateTime())) {
      log.info("ILM policies - {} study with StudyInstanceUID {} can be forwarded to {} since the study was last used {}, and we will forward studies for which no ORU was observed for an entire month (ie, {})", idn, study.getStudyInstanceUID(), lta, study.getLastUpdateTime(), oneMonthAgo);
      ops.scheduleStudyForwardWithStorageCommit(lta);
    }
  }
}

// Check if the study can be purged (purging will only occur when space needs to be reclaimed)
if (timeline.wasFullyForwardedTo(SOUTH_INDEX_LINK_AET) && timeline.wasFullyForwardedWithStorageCommitTo(lta)) {
  log.info("ILM policies - {} study with StudyInstanceUID {} can be marked as a candidate for purging since it has been forwarded to {} and {}", idn, study.getStudyInstanceUID(), SOUTH_INDEX_LINK_AET, lta);
  ops.flagAsPurgeCandidate();
}

log.debug("ILM policies END - Done looking at study with StudyInstanceUID {}", study.getStudyInstanceUID());
