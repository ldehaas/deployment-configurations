// CFIND response morpher for modality worklist system tests
// NOTE: Updating an MWL service morpher will update immediately without a Rialto restart. Use caution when updating a morpher on a live system.

// NOTE: this script should NOT filter anything if the callingAET = RIALTO (this is needed for Auto-reconciliation)


import java.text.SimpleDateFormat
import org.joda.time.LocalDateTime;
import org.joda.time.DateTime;
import org.joda.time.format.DateTimeFormat;
import org.joda.time.format.DateTimeFormatter;

def scriptName = "MWL CFind Response Morpher - "

callingAET = getCallingAETitle()

log.debug( scriptName + "Calling AET: {}", getCallingAETitle() );
log.debug( scriptName + "input: {}\n", input );

def MMMC_AET_list = [
     'MMC_PAPER'
    ,'MMC_FUJI_GO'
    ,'MMC_IU22_VAS'
    ,'MMC_IU22_GEN'
    ,'AE_REVIEWER'
    ,'FUJI_IIP2_SCU'
    ,'FUJI_IIP4_SCU'
    ,'guhrad3d01-AQNET'
    ,'MMC_FRONTDESK'
    ,'MMC_PAPER_CT'
    ,'MMC_PAPER_FDESK'
    ,'MMC_PAPER_US1'
    ,'MMC_PAPER_VAS'
    ,'MMC_PAPER_XRAY2'
    ,'MMMC_CTPAPER'
    ,'MMMC_CTPAPER_SCU'
    ,'MMMC_FRONTDESK'
    ,'MMMC_MRIPAPER_SC'
    ,'MMMC_NM_PAPER'
    ,'MMMC_RAD2_PAPER'
    ,'MMMC_US1_PAPER'
    ,'MMMC_US2_PAPER'
    ,'MMMC_XRAY2'
    ,'MMMCADMIN'
    ,'MRC25905'
    ,'PHILIPSIU22_VAS'
    ,'SYMBIAT1'
    ,'SYNGO_P1'
    ,'TM_CT_CMW_V3.00'
    ,'US2_SCU'
    ,'MMC_FUJI_IIP3'
]

def MGUH_AET_list = [
     'GUH_AET1'
    ,'GUH_AET2'
    ,'GUH_AET3'
]

def MHH_AET_list = [
     'MHH_AET1'
    ,'MHH_AET2'
    ,'MHH_AET3'
    ,'DCMMWL'
]

def MGSH_AET_list = [
     'MGSH_AET1'
    ,'MGSH_AET2'
    ,'MGSH_AET3'
]

def InstitutionName = "UNKNOWN"

if ( MMMC_AET_list.any { it == callingAET } ) {
    InstitutionName = 'MMMC'
} else if ( MGUH_AET_list.any { it == callingAET } ) {
    InstitutionName = 'MGUH'
} else if ( MHH_AET_list.any { it == callingAET } ) {
    InstitutionName = 'MHH'
} else if ( MGSH_AET_list.any { it == callingAET } ) {
    InstitutionName = 'MGSH'
}

if ("MMC_".equalsIgnoreCase(callingAET.substring(0,4)))
{
    InstitutionName = 'MMMC'
}

if ("MMMC_".equalsIgnoreCase(callingAET.substring(0,5)))
{
    InstitutionName = 'MMMC'
}


log.debug( scriptName + "DEBUG: Scheduled Procedure Step Start Time: {}", get("ScheduledProcedureStepSequence/ScheduledProcedureStepStartTime"));
//adjust getScheduledProcedureStepStartDateTime
//Note: this is a manual workaround that is  not needed anymore after the fix in RIALTO-7798

//def hoursToAdd = 4;

//if (get("ScheduledProcedureStepSequence/ScheduledProcedureStepStartTime") != null) {

//    log.debug( scriptName + "DEBUG: original Scheduled Procedure Step Start Time: {}", get("ScheduledProcedureStepSequence/ScheduledProcedureStepStartTime"));

//    DateTimeFormatter formatter = DateTimeFormat.forPattern("HHmmss.SSS");
//    DateTime dt = formatter.parseDateTime( get("ScheduledProcedureStepSequence/ScheduledProcedureStepStartTime") );

//    set("ScheduledProcedureStepSequence/ScheduledProcedureStepStartTime", formatter.print(dt.plusHours(hoursToAdd)) );

//    log.debug( scriptName + "DEBUG: morphed Scheduled Procedure Step Start Time: {}", get("ScheduledProcedureStepSequence/ScheduledProcedureStepStartTime"));

//} else {
//    log.debug( scriptName + "DEBUG: original Scheduled Procedure Step Start Time is null.." );
//}


//get Issuer of Admission ID
log.debug( scriptName + "Issuer of Admission ID: {}", input.get(IssuerOfAdmissionID) )

if (InstitutionName != "UNKNOWN") {
    if ( input.get(IssuerOfAdmissionID) != InstitutionName ) {
        log.info( scriptName + "IssuerOfAdmissionID {} does NOT match the InstitutionName ({}) for CallingAET {}....", input.get(IssuerOfAdmissionID), InstitutionName, callingAET )
        log.info( scriptName + "END" );
        return false
    }
}


// Possible ScheduledProcedureStepStatus values:
//   DISCONTINUED
//   COMPLETED
//   INPROGRESS
//   SC
//   SCHEDULED

def scheduledProcedureStepStatus = get("ScheduledProcedureStepSequence/ScheduledProcedureStepStatus");
//log.debug( scriptName + "DEBUG: ScheduledProcedureStepStatus = {}", scheduledProcedureStepStatus);

if (InstitutionName != "UNKNOWN") {

    if ( scheduledProcedureStepStatus != null && scheduledProcedureStepStatus == 'COMPLETED' ) {
        log.info( scriptName + "ScheduledProcedureStepStatus = {}, will NOT include this order in DMWL C-Find response...", scheduledProcedureStepStatus );
        log.info( scriptName + "END" );
        return false
    }

    if ( scheduledProcedureStepStatus != null && scheduledProcedureStepStatus == 'DISCONTINUED' ) {
        log.info( scriptName + "ScheduledProcedureStepStatus = {}, will NOT include this order in DMWL C-Find response...", scheduledProcedureStepStatus );
        log.info( scriptName + "END" );
        return false
    }

}


log.debug( scriptName + "output: {}\n", output );

log.info( scriptName + "END" );
