//Updating this script will update immediately without a Rialto restart. Use caution when updating a script on a live system.
//
// If the script returns true, automatic reconciliation will be skipped for the study.
// If the script returns false or has no return value, automatic reconciliation will proceed for the study.


def scriptName = "IA ReconciliationSkipMorpher - ";

log.info(scriptName + "START");

patientID = get('PatientID');
log.debug(scriptName + "PatientID: {}", patientID);

issuerOfPatientID = get('IssuerOfPatientID');
log.debug(scriptName + "IssuerOfPatientID: {}", issuerOfPatientID);

studyInstanceUID = get('StudyInstanceUID');
log.debug(scriptName + "StudyInstanceUID: {}", studyInstanceUID);

institution = get('InstitutionalDepartmentName');
log.debug(scriptName + "InstitutionalDepartmentName: {}", institution);

institutionName = get('InstitutionName');
log.debug(scriptName + "InstitutionName: {}", institutionName);

//log.debug(scriptName + "CallingAETitle: {}", CallingAETitle);
//log.debug(scriptName + "CalledAETitle: {}", CalledAETitle);

accessionNumber = get('AccessionNumber');
if (accessionNumber != null && accessionNumber != "") {
    log.debug(scriptName + "AccessionNumber = {}", accessionNumber);
} else {
    log.info(scriptName + "AccessionNumber is null or empty, will NOT autoreconcile StudyInstanceUID = {}, InstitutionName = {}, PatientID = {}, IssuerOfPatientID = {}", studyInstanceUID, institutionName, patientID, issuerOfPatientID);
    log.info(scriptName + "END");
    return true;
}

/*
if (institutionName != null && institutionName == "MMMC") {
   log.info(scriptName + "will attempt auto reconciliation.");
   return false;
}

if (institutionName != null && institutionName == "MGUH") {
   log.info(scriptName + "will attempt auto reconciliation.");
   return false;
}
*/

if (issuerOfPatientID != null && issuerOfPatientID == "MPP") {
   log.info(scriptName + "will attempt auto reconciliation for StudyInstanceUID = {}, InstitutionName = {}, PatientID = {}, IssuerOfPatientID = {}, AccessionNumber = {}", studyInstanceUID, institutionName, patientID, issuerOfPatientID, accessionNumber);
   log.info(scriptName + "END");
   return false
}

if (issuerOfPatientID != null && issuerOfPatientID == "MGI") {
   log.info(scriptName + "will attempt auto reconciliation for StudyInstanceUID = {}, InstitutionName = {}, PatientID = {}, IssuerOfPatientID = {}, AccessionNumber = {}", studyInstanceUID, institutionName, patientID, issuerOfPatientID, accessionNumber);
   log.info(scriptName + "END");
   return false
}

if (issuerOfPatientID != null && issuerOfPatientID == "MSMH") {
   log.info(scriptName + "will attempt auto reconciliation for StudyInstanceUID = {}, InstitutionName = {}, PatientID = {}, IssuerOfPatientID = {}, AccessionNumber = {}", studyInstanceUID, institutionName, patientID, issuerOfPatientID, accessionNumber);
   log.info(scriptName + "END");
   return false
}

if (issuerOfPatientID != null && issuerOfPatientID == "WB4") {
   log.info(scriptName + "will attempt auto reconciliation for StudyInstanceUID = {}, InstitutionName = {}, PatientID = {}, IssuerOfPatientID = {}, AccessionNumber = {}", studyInstanceUID, institutionName, patientID, issuerOfPatientID, accessionNumber);
   log.info(scriptName + "END");
   return false
}

// skip auto reconciliation for all studies
log.info(scriptName + "auto reconciliation will be skipped for StudyInstanceUID = {}, InstitutionName = {}, PatientID = {}, IssuerOfPatientID = {}, AccessionNumber = {}", studyInstanceUID, institutionName, patientID, issuerOfPatientID, accessionNumber);
log.info(scriptName + "END");
return true;
