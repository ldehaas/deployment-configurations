def scriptName = "IA Inbound CFind Morpher - ";

log.info(scriptName + "START")

LOAD('/home/rialto/rialto/etc/services/common/common_medstar_dicom.groovy')
MedStar_DICOM ms = new MedStar_DICOM(log, input)

def todayDate = new Date()
def today = todayDate.format('yyyyMMdd')
def yesterdayDate = todayDate -6
def yesterday = yesterdayDate.format('yyyyMMdd')

def issuerOfPatientID = input.get('IssuerOfPatientID')
if (issuerOfPatientID != null) {
    ms.stampUniversalEntityId();
}

def aeTitle = getCallingAETitle()
log.debug(scriptName + "Calling AET: {}", getCallingAETitle() )

def MWHC_AET_list = [
    'POBCT',
    'SCANPOBPK',
    'POBSCANTECH',
    'BONEDENSITY',
    'CBHTECH_RT',
    'CBHTECH_LT',
    'PACSSCANCICT',
    'CCRCT_LT',
    'CCRCT_RT',
    'PACSSCANCCR',
    'SCANMAG2',
    'SCANTITANMR',
    'SCANMAG1',
    'DXMAIN1',
    'DXMAIN2',
    'SCANFLUORO',
    'PACSSCAN_US',
    'SCANMOR',
    'PACSSCAN_TFO',
    'CT4THFLOOR',
    'CARDMRI'
]

if ( MWHC_AET_list.any { it == aeTitle.toUpperCase() } ) {
    log.debug(scriptName + "Inferring WHC context for CFIND from AE: {}", aeTitle)
    set('IssuerOfPatientID', 'WB4')
    set('IssuerOfPatientIDQualifiersSequence/UniversalEntityID', '2.16.840.1.114107.1.1.16.2.3')
    set('IssuerOfPatientIDQualifiersSequence/UniversalEntityIDType', 'ISO')
}

if (aeTitle != null && aeTitle.startsWith('MMC_PAPER')) {
    log.debug(scriptName + "Inferring MGI context for CFIND from AE: {}", aeTitle)
    set('IssuerOfPatientID', 'MGI')
    set('IssuerOfPatientIDQualifiersSequence/UniversalEntityID', '2.16.840.1.114107.1.1.16.2.10')
    set('IssuerOfPatientIDQualifiersSequence/UniversalEntityIDType', 'ISO')
}

//Limit queries to Issuer/Date range based on calling AE
if (aeTitle != null && (aeTitle == "TEST"||aeTitle == "DCMQR")) {
    log.debug(scriptName + "Limiting queries to WB4 today and yesterday from AE: {}", aeTitle)
    set('IssuerOfPatientID', 'WB4')
    set('IssuerOfPatientIDQualifiersSequence/UniversalEntityID', '2.16.840.1.114107.1.1.16.2.3')
    set('IssuerOfPatientIDQualifiersSequence/UniversalEntityIDType', 'ISO')
    set('Modality', 'CR')
    set('StudyDate', yesterday + '-' +today)
}

log.info(scriptName + "CFind Req: {}", input)

log.info(scriptName + "END")
