// CFIND request morpher for modality worklist system tests

// NOTE: Updating an MWL service morpher will update immediately without a Rialto restart. Use caution when updating a morpher on a live system.

import java.text.SimpleDateFormat

def scriptName = "MWL CFind Request Morpher - "

log.debug(scriptName + "input: {}\n", input)

callingAE = getCallingAETitle()
df = new SimpleDateFormat("yyyyMMdd")
today = df.format(new Date())

log.info(scriptName + "This is the host: {}", host)
log.info(scriptName + "Calling AE: {}", getCallingAETitle())
log.info(scriptName + "Current date: {}", today)

if (get('RequestedProcedureDescription') == null) {
    set('RequestedProcedureDescription', '')
}

if (get('RequestedProcedureCodeSequence/CodeValue') == null) {
    set('RequestedProcedureCodeSequence/CodeValue', '')
}

if (get('RequestedProcedureCodeSequence/CodingSchemeDesignator') == null) {
    set('RequestedProcedureCodeSequence/CodingSchemeDesignator', '')
}

if (get('RequestedProcedureCodeSequence/CodeMeaning') == null) {
    set('RequestedProcedureCodeSequence/CodeMeaning', '')
}

//set('ScheduledProcedureStepSequence/ScheduledProcedureStepStatus', 'SCHEDULED' );

log.debug(scriptName + "output: {}\n", output)

log.info(scriptName  + "END")
