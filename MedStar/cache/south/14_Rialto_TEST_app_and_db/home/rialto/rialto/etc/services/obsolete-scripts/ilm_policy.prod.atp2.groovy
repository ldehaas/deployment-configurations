/**
 *    ILM policy for MedStar - PROD
 */
import org.joda.time.LocalDateTime;
import org.joda.time.DateTime;

log.debug("ILM policies START - Executing against study with StudyInstanceUID {}", study.getStudyInstanceUID());

EIMG_AET = "indexStore-P0T7";
SOUTH_INDEX_LINK_AET = "indexStore-P0M5";

idn = study.get("InstitutionalDepartmentName")
if (idn == null) {
  idn = "OTHER";
}

// Determine what type of study it is
rad_study = false;
if ("RADIOLOGY".equalsIgnoreCase(idn)) {
  rad_study = true;
}

// All studies are forwarded to the South Index Link immediately
if (!timeline.wasFullyForwardedTo(SOUTH_INDEX_LINK_AET)) {
  log.info("ILM policies - {} study with StudyInstanceUID {} can be forwarded to {} since we forward immediately", idn, study.getStudyInstanceUID(), SOUTH_INDEX_LINK_AET);
  ops.scheduleStudyForward(SOUTH_INDEX_LINK_AET);
}
  
if (!rad_study) {
  // Other ologies - forward to eIMG immediately
  if (!timeline.wasFullyForwardedWithStorageCommitTo(EIMG_AET)) {
    log.info("ILM policies - {} study with StudyInstanceUID {} can be forwarded to {} since we forward non-Radiology studies immediately", idn, study.getStudyInstanceUID(), EIMG_AET);
    ops.scheduleStudyForwardWithStorageCommit(EIMG_AET);
  }
} else {
  // Radiology - release on ORU to eIMG
  if (timeline.isFinalReported()) {
    if (!timeline.wasFullyForwardedWithStorageCommitTo(EIMG_AET)) {
      log.info("ILM policies - {} study with StudyInstanceUID {} can be forwarded to {} since there was a FINAL ORU observed for this study", idn, study.getStudyInstanceUID(), EIMG_AET);
      ops.scheduleStudyForwardWithStorageCommit(EIMG_AET);
    }
  } else {
    oneMonthAgo = new DateTime().minusMonths(1);
    if (oneMonthAgo.isAfter(study.getLastUpdateTime())) {
      log.info("ILM policies - {} study with StudyInstanceUID {} can be forwarded to {} since the study was last used {}, and we will forward studies for which no ORU was observed for an entire month (ie, {})", idn, study.getStudyInstanceUID(), EIMG_AET, study.getLastUpdateTime(), oneMonthAgo);
      ops.scheduleStudyForwardWithStorageCommit(EIMG_AET);
    }
  }
}

// Check if the study can be purged (purging will only occur when space needs to be reclaimed)
if (timeline.wasFullyForwardedTo(SOUTH_INDEX_LINK_AET) &&
    timeline.wasFullyForwardedWithStorageCommitTo(EIMG_AET)) {
  log.info("ILM policies - {} study with StudyInstanceUID {} can be marked as a candidate for purging since it has been forwarded to {} and {}", idn, study.getStudyInstanceUID(), SOUTH_INDEX_LINK_AET, EIMG_AET);
  ops.flagAsPurgeCandidate();
}

log.debug("ILM policies END - Done looking at study with StudyInstanceUID {}", study.getStudyInstanceUID());
