def scriptName = "MWL ORDER Morpher - "

log.info(scriptName + "START...")

log.debug(scriptName + "input:\n{}", input)

LOAD('/home/rialto/rialto/etc/services/common/common_medstar_hl7.groovy')
MedStar_HL7 ms = new MedStar_HL7(log, input)

//acn = get('OBR-18')
acn = get('ORC-3')

if (acn == null || ''.equals(acn)) {
    return false
}

ms.groomPids()

ms.moveSecondRepetitionToFirstRepetition()

//ms.groomAccessionNumbersInORC()

log.debug(scriptName + "output:\n{}", output)

log.info(scriptName + "END ...")
