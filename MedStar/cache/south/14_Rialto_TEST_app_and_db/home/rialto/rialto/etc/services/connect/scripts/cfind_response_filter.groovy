import org.joda.time.LocalDateTime;
import org.joda.time.DateTime;
import org.joda.time.format.DateTimeFormat;
import org.joda.time.format.DateTimeFormatter;

def scriptName = "Connect CFind Response Filter - ";
log.info(scriptName + "START");

def validPriorList = [];
def suidList = [];
def deDuplicated = [];
//def fmt = DateTimeFormat.forPattern("d/M/yyyy H:m:s");
//def earliestStudyDate = DateTime.parse("01/01/2009 00:00:00", fmt);
DateTime currentDateTime = new DateTime();
def earliestStudyDate = currentDateTime.minusYears(7);
def latestStudyDate = currentDateTime.minusDays(7);

log.debug(scriptName + "CurrentDatetime {}, earliestStudyDate {}", currentDateTime, earliestStudyDate);


log.debug(scriptName + "Complete list of studies being examined:\n{}", 
    inputs.collect({ it.get(StudyInstanceUID) + ": " + it.getDate(StudyDate, StudyTime) }).join("\n"));

//KHC 
inputs.each {
   if (it.getDate(StudyDate, StudyTime) < earliestStudyDate) {
         log.debug(scriptName + "Prior {} is older than '{}'. This study has a date and time of '{}'. Not going to prefetch study.", it.get(StudyInstanceUID), earliestStudyDate, it.getDate(StudyDate, StudyTime));
    } else if (it.getDate(StudyDate, StudyTime) > latestStudyDate) {
         log.debug(scriptName + "Prior {} is younger than '{}' and younger than 7 days. This study has a date and time of '{}'. Not going to prefetch study.", it.get(StudyInstanceUID), latestStudyDate, it.getDate(StudyDate, StudyTime));
    } else {
         log.debug(scriptName + "Prior {} is younger than '{}' and older than '{}'. This study has a date and time of '{}'. Going to prefetch study.", it.get(StudyInstanceUID), earliestStudyDate, latestStudyDate, it.getDate(StudyDate, StudyTime));
         validPriorList.add(it);
    }
}    
validPriorList.each {
    if (!suidList.contains(it.get(StudyInstanceUID))) {
        suidList.add(it.get(StudyInstanceUID));
        deDuplicated.add(it);
    }
}

log.debug(scriptName + "Complete list of studies that will be retrieved:\n{}", 
    deDuplicated.collect({ it.get(StudyInstanceUID) + ": " + it.getDate(StudyDate, StudyTime) }).join("\n"));

log.info(scriptName + "END");


return deDuplicated;
