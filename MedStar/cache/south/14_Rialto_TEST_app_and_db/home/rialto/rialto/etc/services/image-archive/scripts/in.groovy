log.info("IA Inbound Morpher - START")

LOAD('/home/rialto/rialto/etc/services/common/common_medstar_dicom.groovy')

def ipid = get(IssuerOfPatientID)
def callingAET = getCallingAETitle()
def accn = get(AccessionNumber);

// Stamp unconditionally on local MMMC IMG prefetches
if (!"MGI".equalsIgnoreCase(ipid) && "indexQuery-P0RA".equalsIgnoreCase(callingAET)) {
    set(IssuerOfPatientID, "MGI")
}

// Morph back pid for data received from EasyViz
 manufacturer = get(Manufacturer)
 if (manufacturer != null && manufacturer.equals("Medical-Insight A/S")) {
    other_patient_id = get(OtherPatientIDs)
    if (other_patient_id != null && other_patient_id.contains("^")) {
       def (patient_id, other_patient_id_issuer) = other_patient_id.split("\\^")
       set(PatientID, patient_id)
       if (other_patient_id_issuer != null && other_patient_id_issuer.contains("&")) {
           def (issuer_only, other_patient_id_junk) = other_patient_id_issuer.split("\\&")
           other_patient_id_issuer = issuer_only
       }
     set(IssuerOfPatientID, other_patient_id_issuer)
       remove(OtherPatientIDs)
       remove(OtherPatientIDsSequence)
       log.info("IA Inbound Morpher - Incoming object from EasyViz! Set PatientID to {}, set issuerOfPatientID to {}, and removed the other patient id sequence", other_patient_id, other_patient_id_issuer)
    }
}

MedStar_DICOM ms = new MedStar_DICOM(log, input)

ms.uniqueTemporaryId()
ms.stampUniversalEntityId()
/*
--------------------------------------------------------------------
This code is commented out as a request from KHC#11832
--------------------------------------------------------------------
if (!callingAET.equalsIgnoreCase("LILA") && !callingAET.equalsIgnoreCase("LILAPROD")) {
      if ( accn == null || (accn != null && !accn.startsWith("EX") && !accn.startsWith("MOI")) ) {
          ms.stampInstitutionalName()
    }
}
*/
ms.stampInstitutionalDepartmentName(getCalledAETitle())
ms.copyProcedureCodeToPrivateTags()
ms.fixSOPForHangingProtocol()

log.info("IA Inbound Morpher - End")
