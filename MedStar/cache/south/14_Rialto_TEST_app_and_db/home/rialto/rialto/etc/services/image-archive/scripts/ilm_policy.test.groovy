/**
 *    ILM policy for MedStar TEST environment - TEST
 *
 *    WARNING: Updating this script will update immediately without a Rialto restart. Use caution when updating a script on a live system.
 *
 *    Documentation:
 *       https://karoshealth.atlassian.net/wiki/display/RIALTO64/Policy+script
 *       https://karoshealth.atlassian.net/wiki/display/RIALTO64/ILM+policy+methods
 */

import org.joda.time.LocalDateTime;
import org.joda.time.DateTime;

def scriptName = "ILM Policy - ";

log.debug(scriptName + "START - Executing against study with StudyInstanceUID {}", study.getStudyInstanceUID());

def ipid = study.get("IssuerOfPatientID");

//DateTime cutoffDate = new DateTime(2014, 1, 1, 0, 0, 0, 0);
DateTime currentDateTime = new DateTime();
DateTime cutoffDate = currentDateTime.minusYears(2);
log.debug(scriptName + "currentDateTime {}, cutoffDate {}", currentDateTime, cutoffDate);

DateTime studyDateTime = study.getStudyDateTime();

if (studyDateTime != null ) {
    if ( studyDateTime.isBefore(cutoffDate) )  {
        log.info(scriptName + "SIUID {} with IPID {} can be marked as purge candidate since it has Study Date {}", study.getStudyInstanceUID(), ipid, studyDateTime);
        log.info(scriptName + "4FileBeat, {}, {}, {}, purge candidate", study.getStudyInstanceUID(), ipid, studyDateTime);
        ops.flagAsPurgeCandidate();
    } else {
        log.info(scriptName + "SIUID {} with IPID {} can NOT be marked as purge candidate since it has Study Date {}", study.getStudyInstanceUID(), ipid, studyDateTime);
        log.info(scriptName + "4FileBeat, {}, {}, {}, NOT a purge candidate", study.getStudyInstanceUID(), ipid, studyDateTime);
    }
} else {
    log.info(scriptName + "SIUID {} with IPID {} has NULL Study Date", study.getStudyInstanceUID(), ipid );
    log.info(scriptName + "4FileBeat, {}, {}, {}, has NULL Study Date", study.getStudyInstanceUID(), ipid, null );
}



//ops.flagAsPurgeCandidate();

log.debug(scriptName + "END - Done looking at study with StudyInstanceUID {}", study.getStudyInstanceUID());
