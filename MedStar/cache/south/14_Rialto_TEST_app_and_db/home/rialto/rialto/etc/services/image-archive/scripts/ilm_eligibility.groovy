/* ilm_eligibility.groovy - MedStar TEST */

def scriptName = "ILM Eligibility morpher - ";

log.info( scriptName + "START" )

def localDomainUUIDlist = [
    "2.16.124.113638.1.2.1.1",
    "2.16.840.1.114107.1.1.16.2.5",
    "2.16.840.1.114107.1.1.16.2.7",
    "2.16.840.1.114107.1.1.16.2.2",
    "2.16.840.1.114107.1.1.16.2.4",
    "2.16.840.1.114107.1.1.16.2.8",
    "2.16.840.1.114107.1.1.16.2.6",
    "2.16.840.1.114107.1.1.16.2.3",
    "2.16.840.1.114107.1.1.16.2.9",
    "2.16.840.1.114107.1.1.16.2.10",
    "2.16.840.1.114107.1.1.16.2.11",
    "2.16.840.1.114107.1.1.16.2.12",
    "2.16.840.1.114107.1.1.16.2.13",
    "2.16.840.1.114107.1.1.16.2.14",
    "2.16.840.1.114107.1.1.16.2.15",
    "2.16.840.1.114107.1.1.16.2.16" ];

if (localPid != null && localDomainUUIDlist.contains(localPid.domainUUID) ) {
    log.debug( scriptName + "PID={} with domainUUID={} and AccessionNumber={} is local and will be considered for processing by ILM", localPid, localPid.domainUUID, accessionNumber )
    log.info( scriptName + "END" )
    return true;
} else {
    log.debug( scriptName + "PID={} with domainUUID={} and AccessionNumber={} is NOT local and will NOT be considered for processing by ILM", localPid, localPid.domainUUID, accessionNumber )
    log.info( scriptName + "END" )
    return false;
}
