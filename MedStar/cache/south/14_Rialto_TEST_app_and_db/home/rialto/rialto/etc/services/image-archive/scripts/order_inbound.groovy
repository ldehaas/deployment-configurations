log.info("IA Inbound Order Morpher - START")


LOAD('../../common/common_medstar_hl7.groovy')
MedStar_HL7 ms = new MedStar_HL7(log, input)

set('MSH-9-1', 'OMI')
set('MSH-9-2', 'O23')

ms.groomPids()

def patientIds = []
try {
    patientIds = input.getList('/.PID-3(*)-1')
} catch (Exception e) {
    // do nothing!
}

for (int i = 0; i < patientIds.size(); i++) {
    def currNamespace = input.get('/.PID-3(' + i + ')-4-1')
    def currIPID = ms.ns_to_ipid_map[currNamespace]
    if (currIPID != null) {
        log.info("IA Inbound Order Morpher - Modifying HL7 namespace {} to be DICOM IssuerOfPatientID {}", currNamespace, currIPID)
        input.set('/.PID-3(' + i + ')-4-1', currIPID)
    }
}

// Accession Number
set('OBR-18', get('ORC-3-1'))

// Study Description
set('OBR-15-1-1', get('OBR-4-1'))
set('OBR-15-1-2', get('OBR-4-2'))
set('OBR-15-1-3', 'L')
set('OBR-4-1', null)
set('OBR-4-2', null)


log.info("IA Inbound Order Morpher - END")
