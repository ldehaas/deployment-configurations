import java.text.SimpleDateFormat

log.info("IA SCN Morpher - START")

LOAD('../common_medstar_dicom.groovy')
MedStar_DICOM ms = new MedStar_DICOM(log, input)
sdf = new SimpleDateFormat("yyyyMMddHHmmss")

log.debug("IA SCN Morpher - Calling AE Title is {} for input:\n{}", getCallingAETitle(), input);

initialize( 'ORM', 'O01', '2.3' );
output.getMessage().addNonstandardSegment('IPC')

set('MSH-7', sdf.format(new Date()))
setPersonName('PID-5', input.get(PatientName));
/* set('PID-3-1', input.get(PatientID)); */

def issuerOfPatientID = input.get(IssuerOfPatientID);
def namespace = universal_id = universal_id_type = "";
if (issuerOfPatientID != null && issuerOfPatientID.contains("&")) {
    parts = issuerOfPatientID.split("&");
    if (parts.length == 3) {
        domain = com.karos.rtk.common.Domain.parse(issuerOfPatientID)
        namespace = domain.namespaceID
        universal_id = domain.domainUUID
        universal_id_type = domain.domainUUIDtype
    }
} else {
    namespace = issuerOfPatientID
}

if (namespace != null && !ms.ns_to_ipid_map.keySet().contains(namespace)) {
    def hl7_namespace =  ms.ns_to_ipid_map.find { it.value == namespace }?.key
    if (hl7_namespace != null) {
        log.info("IA SCN Morpher - Mapping dicom namespace \"{}\" to hl7 namespace \"{}\"", namespace, hl7_namespace);
        namespace = hl7_namespace
    }
}

def patientID = input.get(PatientID);
if (namespace != null && namespace == "MUMH") {
    if (patientID != null && patientID != patientID.padLeft(9,'0')) { 
        log.info("IA SCN Morpher - padding PatientID with leading zeros for namespace \"{}\"", namespace);
        patientID = input.get(PatientID).padLeft(9,'0');
    }
}

set('PID-3-1', patientID);

set('PID-3-4-1', namespace);
if (!"".equals(universal_id) && !"".equals(universal_id_type)) {
    set('PID-3-4-2', universal_id);
    set('PID-3-4-3', universal_id_type);
}
if (type != null && type.toLowerCase().contains("delete")) {
    set('ORC-1', 'DC')
} else {
    set('ORC-1', 'NW')
}
set('OBR-1', '1')
set('OBR-3-1', input.get(AccessionNumber))
set('OBR-3-2', namespace)

if (type != null) {
    log.info("IA SCN Morpher - DEBUG: DICOM PatientID = {}", input.get(PatientID) );
    log.info("IA SCN Morpher - DEBUG: DICOM 0x00100020 = {}", input.get(0x00100020) );
    log.info("IA SCN Morpher - DEBUG: DICOM tag(0010,0020) = {}", input.get(tag(0x0010, 0x0020)) );
    
    log.info("IA SCN Morpher - DEBUG: DICOM 0x00080051 = {}", input.get(0x00080051) );
    log.info("IA SCN Morpher - DEBUG: DICOM IssuerOfAccessionNumberSequence = {}", input.get("IssuerOfAccessionNumberSequence") );
   
    log.info("IA SCN Morpher - DEBUG: DICOM tag(0035,1040) = {}", input.get(tag(0x0035, 0x1040)) );
    log.info("IA SCN Morpher - DEBUG: DICOM 0x00351040 = {}", input.get(0x00351040) );

    issuerOfAccessionNumberSeq = get("IssuerOfAccessionNumberSequence")
    issuerOfAccessionNumberSeq.each { issuerOfAccNum ->
        log.info( "Discovered another AccessionNumber {} with IssuerOfAccessionNumber {}", issuerOfAccNum.get(AccessionNumber), issuerOfAccNum.get(IssuerOfAccessionNumber) )
    }

    log.info("IA SCN Morpher - DEBUG: full input object: ", input.dump());

    if ( input.get(tag(0x0008, 0x0051)) == 'MAS' ) {
       set('OBR-3-2-1', 'MAS')
       log.info("IA SCN Morpher - Setting Issuer of Accession Number to MAS based on 0008,0051 tag");
    } else if ( input.get(tag(0x0035, 0x1040)) == 'MedStar Radiology Network Bel Air' ) {
       set('OBR-3-2-1', 'MAS')
       log.info("IA SCN Morpher - Setting Issuer of Accession Number to MAS based on 0035,1040 tag");
    } else {
       log.info("IA SCN Morpher - DICOM tag(0008,0051) = {}", input.get(tag(0x0008, 0x0051)) );
       log.info("IA SCN Morpher - DICOM tag(0035,1040) = {}", input.get(tag(0x0035, 0x1040)) );
    }
} else {
    log.info("IA SCN Morpher - Not setting Issuer of Accession Number - type is null!!!");
}

set('OBR-4-1', input.get(RequestedProcedureID))
def study_description = input.get(StudyDescription)
if (study_description != null)  {
    study_description = study_description.replaceAll("\\^.*", "")
    set('OBR-4-2', study_description)
}
set('OBR-7', input.get(StudyDate))
set('OBR-25', 'I')
set('OBX-1', '1')
set('OBX-2', 'TX')
set('OBX-3-1-2', 'GDT')
set('OBX-5', 'Y')
set('IPC-3', input.get(StudyInstanceUID))

log.info("IA SCN Morpher - END")
