LOAD("/home/rialto/rialto/etc/services/common/common_medstar.groovy")

/**
 * HL7 Patient identifier
 */
class Pid {
    def identifier = null
    def namespace = null
    def universalId = null
    def universalIdType = null

    def Pid(identifier, namespace, universalId, universalIdType) {
        this.identifier = identifier
        this.namespace = namespace
        this.universalId = universalId
        this.universalIdType = universalIdType
    }
}

/**
 * HL7 Accession Number identifier
 */
class AccN {
    def identifier = null
    def namespace = null
    def universalId = null
    def universalIdType = null

    def AccN(identifier, namespace, universalId, universalIdType) {
        this.identifier = identifier
        this.namespace = namespace
        this.universalId = universalId
        this.universalIdType = universalIdType
    }
}

/**
 * HL7 related configuration specific to the MedStar environment
 */
class MedStar_HL7 extends MedStar {

    def message = null

    def MedStar_HL7(log, message) {
        super(log)
        this.message = message
    }

    /**
     * An example MedStar ADT message:
     *
     * MSH|^~\&amp;|INVISION|MWHC|RIALTO|MWHC|201505261010||ADT^A08|6144799655738685787|P|2.3|-1
     * PID|1|3553353^^^MEDSTAR|3553353^^^MEDSTAR&amp;EE&amp;ISO~1042356^^^MWHC&amp;MR&amp;ISO~60083375^^^MWHC&amp;PN&amp;ISO||CARDIOLOGY^MAYSIX||19800305|F
     *
     * This method will:
     * 1) move the enterprise identifier to PID-2
     * 2) fully qualify a patient identifier from any known site
     * 3) empty out any other identifier
     */
    def groomPids() {
        log.info("MEDSTAR Morpher - Updating PID-3 with appropriate UniversalIDs...");
       
        def MRN_IDENTIFIER_TYPE = 'MR'
        def ENTERPRISE_IDENTIFIER_TYPE = 'EE'

        def patientIds = null;
        try {
            patientIds = message.getList('/.PID-3(*)-1')
        } catch (Exception e) {
            // do nothing!
        }
        
        for (int i = 0; i < patientIds.size(); i++) {
            // The UniversalId is being used to denote the type of identifier
            // in the source message (for ADTs).
            // This method will set the correct UniversalIds
            def currPID = message.get('/.PID-3(' + i + ')-1')
            def currNS = message.get('/.PID-3(' + i + ')-4-1')
            def currIdentifier = message.get('/.PID-3(' + i + ')-4-2')
            def currIdentifierType = message.get('/.PID-3(' + i + ')-4-3')

            def currIPID = ns_to_ipid_map[currNS]
            if (currIPID == null) {
                log.info("MEDSTAR Morpher - Removing patient identifier ({}^^^{}&{}&{}) because no known namespace was found.",
                   currPID, currNS, currIdentifier, currIdentifierType)

                setPID(new Pid('', '', '', ''), i)
                continue
            }

            if (currNS != null && currNS == "MUMH") {
                if (currPID != null && currPID != currPID.replaceFirst("^0+(?!\$)", "")) {
                    log.info("MEDSTAR Morpher - stripping leading zeros for MUMH patient, new PID = {}", currPID.replaceFirst("^0+(?!\$)", ""));
                    currPID = currPID.replaceFirst("^0+(?!\$)", "");
                }
            }

            def updatedPID = new Pid(currPID, currNS, ipid_to_universalid_map[currIPID], 'ISO')

            if (currIdentifier == null || (currIdentifier != 'EE' && currIdentifier !='MR' && currIdentifier != 'PN')) {
                currIdentifier = ns_to_uiversalid_code_map[currNS];
            }

            switch (currIdentifier) {
                case ENTERPRISE_IDENTIFIER_TYPE:
                    setPID(updatedPID, i)

                    // Copy to PID-2
                    message.set('/.PID-2-1', updatedPID.identifier)
                    message.set('/.PID-2-4-1', updatedPID.namespace)
                    message.set('/.PID-2-4-2', updatedPID.universalId)
                    message.set('/.PID-2-4-3', updatedPID.universalIdType)

                    log.info("MEDSTAR Morpher - Enterprise identifier was set to be ({}^^^{}&{}&{}).",
                        updatedPID.identifier, updatedPID.namespace, updatedPID.universalId, updatedPID.universalIdType)

                    break
                case MRN_IDENTIFIER_TYPE:
                case null:
                    setPID(updatedPID, i)

                    log.info("MEDSTAR Morpher - MRN was set to be ({}^^^{}&{}&{}).", 
                        updatedPID.identifier, updatedPID.namespace, updatedPID.universalId, updatedPID.universalIdType)

                    break
                default:
                    log.info("MEDSTAR Morpher - Removing patient identifier ({}^^^{}&{}&{}) because no known identifier type (ie, either MR or EE) was found.",
                        currPID, currNS, currIdentifier, currIdentifierType)

                    setPID(new Pid('', '', '', ''), i)
            }

        }

    }

    def getEEFromPID2() {
        return new Pid(message.get('/.PID-2-1'),
            message.get('/.PID-2-4-1'),
            message.get('/.PID-2-4-2'),
            message.get('/.PID-2-4-3'));
    }

    def moveSecondRepetitionToFirstRepetition() {
        log.info("MEDSTAR Morpher - Moving the 2nd repetition in PID-3 to the 1st repetition in PID-3");

        setPID(new Pid(message.get('/.PID-3(1)-1'),
            message.get('/.PID-3(1)-4-1'),
            message.get('/.PID-3(1)-4-2'),
            message.get('/.PID-3(1)-4-3'))
        )
    }
    
    def setPID(pid, repetition='0') {
        if (pid == null) {
            return;
        }

        log.info("MEDSTAR Morpher - Setting PID in PID-3({}) to identifier ({}^^^{}&{}&{}).", 
            repetition,
            pid.identifier,
            pid.namespace,
            pid.universalId,
            pid.universalIdType);

        message.set('/.PID-3(' + repetition + ')-1', pid.identifier)
        message.set('/.PID-3(' + repetition + ')-2', '')
        message.set('/.PID-3(' + repetition + ')-3', '')
        message.set('/.PID-3(' + repetition + ')-4-1', pid.namespace)
        message.set('/.PID-3(' + repetition + ')-4-2', pid.universalId)
        message.set('/.PID-3(' + repetition + ')-4-3', pid.universalIdType)
        message.set('/.PID-3(' + repetition + ')-5', '') 
    }

    def groomAccessionNumbersInORC() {
        log.info("MEDSTAR Morpher - Updating ORC-3 with appropriate UniversalIDs...");
       
        def MRN_IDENTIFIER_TYPE = 'MR'
        def ENTERPRISE_IDENTIFIER_TYPE = 'EE'

        def accessionNumbers = null;
        try {
            accessionNumbers = message.getList('/.ORC-3(*)-1')
        } catch (Exception e) {
            // do nothing!
        }
        
        for (int i = 0; i < accessionNumbers.size(); i++) {
            // The UniversalId is being used to denote the type of identifier
            // in the source message (for ADTs).
            // This method will set the correct UniversalIds
            def currAccessionNumber = message.get('/.ORC-3(' + i + ')-1')
            def currNameSpace = message.get('/.ORC-3(' + i + ')-2')
            def currIdentifier = message.get('/.ORC-3(' + i + ')-3')
            def currIdentifierType = message.get('/.ORC-3(' + i + ')-4')

            def currIssuerOfAccessionNumber = ns_to_ipid_map[currNameSpace]
            if (currIssuerOfAccessionNumber == null) {
                log.info("MEDSTAR Morpher - Removing Accession Number ({}^{}^{}^{}) because no known namespace was found.",
                   currAccessionNumber, currNameSpace, currIdentifier, currIdentifierType)

                setAccessionNumberInORC(new AccN('', '', '', ''), i)
                continue
            }

            def updatedAccessionNumber = new AccN(currAccessionNumber, currNameSpace, ipid_to_universalid_map[currIssuerOfAccessionNumber], 'ISO')

            if (currIdentifier == null || (currIdentifier != 'EE' && currIdentifier !='MR')) {
                currIdentifier = ns_to_uiversalid_code_map[currNameSpace];
            }

            switch (currIdentifier) {
                case ENTERPRISE_IDENTIFIER_TYPE:
                    setAccessionNumberInORC(updatedAccessionNumber, i)

                    log.info("MEDSTAR Morpher - Enterprise IssuerOfAccessionNumber was set to be ({}^{}^{}^{}).",
                        updatedAccessionNumber.identifier, updatedAccessionNumber.namespace, updatedAccessionNumber.universalId, updatedAccessionNumber.universalIdType)

                    break
                case MRN_IDENTIFIER_TYPE:
                case null:
                    setAccessionNumberInORC(updatedAccessionNumber, i)

                    log.info("MEDSTAR Morpher - MRN IssuerOfAccessionNumber was set to be ({}^{}^{}^{}).", 
                        updatedAccessionNumber.identifier, updatedAccessionNumber.namespace, updatedAccessionNumber.universalId, updatedAccessionNumber.universalIdType)

                    break
                default:
                    log.info("MEDSTAR Morpher - Removing AccessionNumber ({}^{}^{}^{}) because no known identifier type (ie, either MR or EE) was found.",
                        currAccessionNumber, currNameSpace, currIdentifier, currIdentifierType)

                    setAccessionNumberInORC(new AccN('', '', '', ''), i)
            }

        }

    }

    def setAccessionNumberInORC(accn, repetition='0') {
        if (accn == null) {
            return;
        }

        log.info("MEDSTAR Morpher - Setting Accession Number in ORC-3({}) to identifier ({}^{}^{}^{}).", 
            repetition,
            accn.identifier,
            accn.namespace,
            accn.universalId,
            accn.universalIdType);

        message.set('/.ORC-3(' + repetition + ')-1', accn.identifier)
        message.set('/.ORC-3(' + repetition + ')-2', accn.namespace)
        message.set('/.ORC-3(' + repetition + ')-3', accn.universalId)
        message.set('/.ORC-3(' + repetition + ')-4', accn.universalIdType)
        message.set('/.ORC-3(' + repetition + ')-5', '') 
    }
}
