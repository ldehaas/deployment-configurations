<config>
    <service type="authentication" id="authentication">
        <server idref="authenticated-http" name="web-api">
            <url>${Navigator-RootPath}</url>
        </server>

        <config>
            <include location="userauth.xml"/>
        </config>
    </service>

    <service type="usermanagement" id="usermanagement">
        <config>
            <prop name="SystemDefaultPermissions">
                <permission name="rialto.arr" value="false"/>
                <permission name="rialto.administration" value="false"/>
                <permission name="rialto.usermanagement.internalrealm" value="false"/>
                <permission name="rialto.events" value="false" />
                <permission name="rialto.quality.management" value="false" />

                <!-- Workflows/Dataflows permissions - should never have both configured at the same time -->
                <permission name="rialto.dataflows" value="false" />
                <!--permission name="rialto.workflows" value="false" /-->
                <permission name="rialto.modality.worklist" value="true" />
                <permission name="rialto.modality.worklist.updates" value="true" />
                <!-- NewRelic Dashboard   -->
                <permission name="rialto.dashboard" value="false" />
            </prop>
            <prop name="web-api-path" value="${Usermanagement-APIBasePath}"/>

            <include location="defaultuserprefs.xml"/>
            <include location="defaultdashboardprefs.xml"/>
        </config>
    </service>

    <!-- PLUGIN: External Viewer (Download) -->
    <var name="DownloadPlugin-Name" value="External Viewer (Download)" />
    <var name="DownloadPlugin-Description" value="External platform-specific viewer or download to local disk" />
    <service type="navigator.plugin.download" id="navigator.plugin.download">
        <device idref="pix" />
        <device idref="pdq" />
        <device idref="xdsrep"/>
        <device idref="xdsreg"/>

        <config>
            <prop name="CrossAffinityDomainID" value="${System-AffinityDomain}"/>
            <prop name="ForcedMimeTypeFileExtension">
                <prop mimeType="application/vnd.ms-excel" fileExtension="xls"/>
            </prop>
            <prop name="CassandraConfiguration">
                <clusterHosts>${CassandraClusterHosts}</clusterHosts>
                <clusterDatacenterName>${CassandraClusterDatacenterName}</clusterDatacenterName>
                <keyspaceName>${CassandraKeyspacePrefix}navigator</keyspaceName>
                <keyspaceReplicationStrategy>NetworkTopologyStrategy</keyspaceReplicationStrategy>
                <keyspaceReplicationStrategyOptions>${CassandraReplicationOption}</keyspaceReplicationStrategyOptions>
                <consistencyLevel>LOCAL_QUORUM</consistencyLevel>
                <serialConsistencyLevel>LOCAL_SERIAL</serialConsistencyLevel>
                <keyspaceCreateEnabled>true</keyspaceCreateEnabled>
                <dynamicConsistencyLevelEnabled>false</dynamicConsistencyLevelEnabled>
                <schemaUpdateEnabled>false</schemaUpdateEnabled>
            </prop>
            <prop name="web-api-path" value="/plugins/download"/>
            <prop name="UserManagementURL" value="${Usermanagement-AuthenticatedURL}"/>

            <include location="patientidentitydomains.xml"/>
            <include location="cacheStorage.xml"/>
        </config>
    </service>

    <!-- PLUGIN: CDA Viewer   -->
    <var name="CDAPlugin-Name" value="CDA Viewer" />
    <var name="CDAPlugin-Description" value="HL7 CDA (Clinical Document Architecture) Viewer" />
    <service type="navigator.plugin.cda" id="navigator.plugin.cda">
        <device idref="pix" />
        <device idref="pdq" />
        <device idref="xdsrep"/>
        <device idref="xdsreg"/>

        <config>
            <prop name="CrossAffinityDomainID" value="${System-AffinityDomain}"/>
            <prop name="CassandraConfiguration">
                <clusterHosts>${CassandraClusterHosts}</clusterHosts>
                <clusterDatacenterName>${CassandraClusterDatacenterName}</clusterDatacenterName>
                <keyspaceName>${CassandraKeyspacePrefix}navigator</keyspaceName>
                <keyspaceReplicationStrategy>NetworkTopologyStrategy</keyspaceReplicationStrategy>
                <keyspaceReplicationStrategyOptions>${CassandraReplicationOption}</keyspaceReplicationStrategyOptions>
                <consistencyLevel>LOCAL_QUORUM</consistencyLevel>
                <serialConsistencyLevel>LOCAL_SERIAL</serialConsistencyLevel>
                <keyspaceCreateEnabled>true</keyspaceCreateEnabled>
                <dynamicConsistencyLevelEnabled>false</dynamicConsistencyLevelEnabled>
                <schemaUpdateEnabled>false</schemaUpdateEnabled>
            </prop>
            <prop name="web-api-path" value="/plugins/cda"/>
            <prop name="DefaultStylesheet" value="${rialto.rootdir}/etc/navigator/plugins/cda/default.xsl"/>
            <prop name="UserManagementURL" value="${Usermanagement-AuthenticatedURL}"/>

            <include location="patientidentitydomains.xml"/>
            <include location="cacheStorage.xml"/>
        </config>
    </service>


    <service type="navigator.server" id="navigator.server">
        <device idref="pix" />
        <device idref="pdq" />
        <device idref="xdsrep"/>
        <device idref="xdsreg"/>

        <config>
            <prop name="CrossAffinityDomainID" value="${System-AffinityDomain}"/>
            <prop name="CassandraConfiguration">
                <clusterHosts>${CassandraClusterHosts}</clusterHosts>
                <clusterDatacenterName>${CassandraClusterDatacenterName}</clusterDatacenterName>
                <keyspaceName>${CassandraKeyspacePrefix}navigator</keyspaceName>
                <keyspaceReplicationStrategy>NetworkTopologyStrategy</keyspaceReplicationStrategy>
                <keyspaceReplicationStrategyOptions>${CassandraReplicationOption}</keyspaceReplicationStrategyOptions>
                <consistencyLevel>LOCAL_QUORUM</consistencyLevel>
                <serialConsistencyLevel>LOCAL_SERIAL</serialConsistencyLevel>
                <keyspaceCreateEnabled>true</keyspaceCreateEnabled>
                <dynamicConsistencyLevelEnabled>false</dynamicConsistencyLevelEnabled>
                <schemaUpdateEnabled>false</schemaUpdateEnabled>
            </prop>
            <prop name="Cluster">
                <name>NavigatorServerClusterGroupId</name>
                <udpstack>
                    <bindAddress>${Navigator-Server-Cluster-Bind-IP}</bindAddress>
                    <multicastAddress>${Navigator-Server-Cluster-Multicast-IP}</multicastAddress>
                    <multicastPort>${Navigator-Server-Cluster-Multicast-PORT}</multicastPort>
                </udpstack>
            </prop>

            <!-- Rialto XDS Registry cannot search by extended metadata -->
            <prop name="ExtendedMetadataAttributes"></prop>
            <prop name="SystemDefaultPermissions">
                <permission name="rialto.navigator.patients.search.internal" value="true"/>
                <permission name="rialto.navigator.patients.search.external" value="false"/>
                <permission name="rialto.navigator.study.metadata.view" value="true" />
                <permission name="rialto.navigator.study.move" value="true" />
                <permission name="rialto.navigator.study.changeavailability" value="false" />
                <permission name="rialto.navigator.study.physical.delete" value="false"/>
                <permission name="rialto.navigator.docs.metadata.view.unknown.internal" value="true"/>
                <permission name="rialto.navigator.docs.metadata.view.unknown.external" value="true"/>
                <permission name="rialto.navigator.docs.metadata.edit.unknown.internal" value="false"/>
                <permission name="rialto.navigator.docs.metadata.edit.unknown.external" value="false"/>
                <permission name="rialto.navigator.docs.move.unknown.internal" value="false" />
                <permission name="rialto.navigator.docs.view.unknown.internal.always" value="false"/>
                <permission name="rialto.navigator.docs.view.unknown.external.always" value="false"/>
                <permission name="rialto.navigator.docs.view.unknown.internal.onbreakglass" value="true"/>
                <permission name="rialto.navigator.docs.view.unknown.external.onbreakglass" value="true"/>
                <permission name="rialto.navigator.docs.upload.internal" value="false" />
                <permission name="rialto.navigator.study.qc" value="false" />
                <permission name="rialto.navigator.study.version.restore" value="false" />
                <permission name="rialto.navigator.study.reconciliation" value="false" />
                <permission name="rialto.vip.access" value="false" />
            </prop>

            <prop name="DefaultConfidentialityCodeSchemeID" value="1.2.3.4.5.6.1" />
            <prop name="DefaultConfidentialityCodeSchemeName" value="Karos Health demo confidentialityCodes" />
            <prop name="MaximumPatientResultSetSize" value="${Navigator-MaxPatientSearchResults}"/>
            <prop name="MaximumDocumentResultSetSize" value="${Navigator-MaxDocumentSearchResults}"/>
            <prop name="AllowMetadataUpdates">${XDSRegistry-MetadataUpdateEnabled}</prop>
            <prop name="ConfidentialityCodeSchemaName">TEST</prop>
            <prop name="web-api-path" value="${Navigator-APIBasePath}"/>
            <prop name="UseTargetDomainForPDQ">true</prop>

            <include location="patientidentitydomains.xml"/>
            <include location="cacheStorage.xml"/>

            <prop name="XDSDocumentCodes">
                <classification name="classCodes">
                    <Code codeValue="DICOM Study Manifest" schemeName="Karos Health demo classCodes" schemeID="1.2.3.4.5.6.7" displayName="DICOM Study Manifest" />
                    <Code codeValue="DR" schemeName="Karos Health demo classCodes" schemeID="1.2.3.4.5.6.7" displayName="Study Report" />
                    <Code codeValue="Continuity" schemeName="Karos Health demo classCodes" schemeID="1.2.3.4.5.6.7" displayName="Continuity of Care" />
                    <Code codeValue="History" schemeName="Karos Health demo classCodes" schemeID="1.2.3.4.5.6.7" displayName="Clinical History and Physical" />
                </classification>
                <classification name="formatCodes">
                    <Code codeValue="PDF" schemeName="Karos Health demo formatCodes" schemeID="1.2.3.4.5.6.9" displayName="PDF" />
                    <Code codeValue="DICOM" schemeName="Karos Health demo formatCodes" schemeID="1.2.3.4.5.6.9" displayName="DICOM" />
                    <Code codeValue="JPEG" schemeName="Karos Health demo formatCodes" schemeID="1.2.3.4.5.6.9" displayName="JPEG" />
                    <Code codeValue="CDA" schemeName="Karos Health demo formatCodes" schemeID="1.2.3.4.5.6.9" displayName="CDA Document" />
                </classification>
                <classification name="healthcareFacilityTypeCodes">
                    <Code codeValue="HCFacility" schemeName="Karos Health demo healthcareFacilityTypeCodes" schemeID="1.2.3.4.5.6.0" displayName="Healthcare Facility" />
                </classification>
                <classification name="practiceSettingCodes">
                    <Code codeValue="PracticeSetting" schemeName="Karos Health demo practiceSettingCodes" schemeID="1.2.3.4" displayName="Practice Setting" />
                </classification>
                <classification name="contentTypeCodes">
                    <Code codeValue="Imaging Study" schemeName="Karos Health demo contentTypeCodes" schemeID="1.2.3.4.5.6.8" displayName="Imaging Study" />
                    <Code codeValue="DR" schemeName="Karos Health demo contentTypeCodes" schemeID="1.2.3.4.5.6.8" displayName="Study Report" />
                    <Code codeValue="Continuity" schemeName="Karos Health demo contentTypeCodes" schemeID="1.2.3.4.5.6.8" displayName="Continuity of Care" />
                    <Code codeValue="History" schemeName="Karos Health demo contentTypeCodes" schemeID="1.2.3.4.5.6.8" displayName="Clinical History and Physical" />
                </classification>
                <classification name="typeCodes">
                    <Code codeValue="TC1" schemeName="Karos Health demo typeCodes" schemeID="1.2.3.4" displayName="Hospital Consultation Note" />
                    <Code codeValue="TC2" schemeName="Karos Health demo typeCodes" schemeID="1.2.3.4" displayName="Study Report" />
                    <Code codeValue="TC3" schemeName="Karos Health demo typeCodes" schemeID="1.2.3.4" displayName="Imaging Study" />
                </classification>
            </prop>
        </config>
    </service>

    <service type="rialto.ui" id="rialto.ui">
        <device idref="pix" />
        <device idref="pdq" />

        <server idref="navigator-http" name="web-ui">
            <url>${Navigator-GUIBasePath}</url>
        </server>

        <server idref="authenticated-http" name="web-api">
            <url>/public/*</url>
        </server>

        <config>
            <prop name="RialtoAsACache" value="true"/>
            <prop name="MaxEventDisplay" value="5000"/>
            <prop name="ApiURL" value="${Navigator-HostProtocol}://${Navigator-Host}:${Navigator-APIPort}${Navigator-RootPath}/api${Navigator-APIBasePath}"/>
            <prop name="ArrURL" value="${Navigator-HostProtocol}://${Navigator-Host}:${ARR-GUIPort}"/>
            <prop name="UserManagementURL" value="${Usermanagement-URL}"/>
            <prop name="PublicURL" value="${Navigator-HostProtocol}://${Navigator-Host}:${Navigator-APIPort}/public"/>
            <prop name="StudyManagementURL" value="http://${HostIP}:8080${IA-StudyManagement-Path}"/>
            <prop name="WadoURL" value="http://${HostIP}:8080/vault/wado" />
            <prop name="DataflowApiURL" value="http://${HostIP}:13337/monitoring" />
            <prop name="DeviceRegistryURL" value="http://${HostIP}:8080/rialto/deviceregistry" />
            <prop name="ModalityWorklistURL" value="http://${HostIP}:8080/mwl" />
            <prop name="MintApiURL" value="http://${HostIP}:8080/vault/mint" />
            <prop name="DicomQcToolsURL" value="http://${HostIP}:8080/vault/qc" />
            <prop name="IlmURL" value="http://${HostIP}:8080/vault/ilm" />
            <prop name="GlobalInactivitySessionTimeout" value="${IdleUserSessionTimeout}" />


            <prop name="SupportedThemes">
                rialto-light
                rialto-dark
            </prop>
            <prop name="DefaultTheme" value="${Navigator-DefaultTheme}"/>

            <prop name="SupportedLocales">
                en
                fr
            </prop>
            <prop name="DefaultLocale" value="${Navigator-DefaultLocale}"/>

            <prop name="BreakTheGlassOptions">
                <BreakTheGlassOption>Emergency access required for patient care.</BreakTheGlassOption>
            </prop>

            <prop name="Plugins">
                <plugin>
                    <name>${CDAPlugin-Name}</name>
                    <description>${CDAPlugin-Description}</description>
                    <mimeTypes>text/xml</mimeTypes>
                    <url>${Navigator-HostProtocol}://${Navigator-Host}:${Navigator-APIPort}${Navigator-RootPath}/api/plugins/cda</url>
                    <embedded>true</embedded>
                </plugin>

                <plugin>
                    <name>${DownloadPlugin-Name}</name>
                    <description>${DownloadPlugin-Description}</description>
                    <mimeTypes>*</mimeTypes>
                    <url>${Navigator-HostProtocol}://${Navigator-Host}:${Navigator-APIPort}${Navigator-RootPath}/api/plugins/download</url>
                    <embedded>true</embedded>
                </plugin>
            </prop>

            <include location="patientidentitydomains.xml"/>

            <!-- prop name="RialtoImageArchiveAETitle" value="${IA-AETitle}" / -->
            <prop name="RialtoImageArchiveAETitle" value="RIALTO_CACHE" />
            <prop name="ConnectQueryRetrieveAETitle" value="RIALTO_CONNECT" />

            <!-- NEWRELIC CONFIGURATION -->
            <prop name="NewRelicInsightsQueryKey" value="${NewRelic-InsightsQueryKey}" />
            <prop name="NewRelicInsightsQueryURL" value="https://insights-api.newrelic.com/v1/accounts/${NewRelic-AccountID}/query" />

            <!-- Configure eventCodeSchemes to support searching for documents by eventCode -->
            <!--prop name="EventCodeSchemes">
                <scheme schemeID="1.2.3" schemeName="Karos Health demo eventCodes" />
            </prop-->

        </config>
    </service>
</config>


