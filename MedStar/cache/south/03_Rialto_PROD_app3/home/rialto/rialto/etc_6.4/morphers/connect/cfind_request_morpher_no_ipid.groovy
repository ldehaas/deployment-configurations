log.info("Connect CFind Request Morpher - START")

log.info("Connect CFind Request Morpher - Removing Issuer of Patient ID \"{}\" in CFind request", input.get(IssuerOfPatientID))
input.set(IssuerOfPatientID, null)

log.info("Connect CFind Request Morpher - END")
