/**
 *    ILM policy for MedStar - PROD
 *
 *    WARNING: Updating this script will update immediately without a Rialto restart. Use caution when updating a script on a live system.
 *
 *    Documentation:
 *       https://karoshealth.atlassian.net/wiki/display/RIALTO64/Policy+script
 *       https://karoshealth.atlassian.net/wiki/display/RIALTO64/ILM+policy+methods
 */


import org.joda.time.LocalDateTime;
import org.joda.time.DateTime;

def scriptName = "ILM Policy - ";

log.debug(scriptName + "START - Executing against study with StudyInstanceUID {} and AccessionNumber {}", study.getStudyInstanceUID(), study.get("AccessionNumber"));
//log.debug(scriptName + "ALEX_CUSTOM: {}, {}, {}, {}", study.getStudyInstanceUID(), study.get("IssuerOfPatientID"), study.getStudyDateTime(), study.get("AccessionNumber"));
log.debug(scriptName + "ALEX_CUSTOM: {}, {}", study.getStudyInstanceUID(), study.get("IssuerOfPatientID"));

// Constants
// North IMGs
//FSH IMG 172.23.8.65/10104/FSHINSITE01
//FSH MAMMO IMG 172.23.8.109/10104/indexStore-P0UX
//GSH IMG 172.16.80.66/10104/GSHINSITE01
//HHC IMG 172.16.209.107/10104/indexStore-P0HS
//UMH IMG 172.17.80.79/10104/indexStore-P0H2
LOCAL_FSH_IMG_AET = "FSHINSITE01";
LOCAL_FSH_IMG_Mammo_AET = "indexStore-P0UX";
LOCAL_GSH_IMG_AET = "GSHINSITE01";
LOCAL_HHC_IMG_AET = "indexStore-P0HS";
LOCAL_UMH_IMG_AET = "indexStore-P0H2";

// South IMGs
//South eIMG 198.50.67.28/10104/indexStore-P0T7
//GUH IMG 172.19.1.192/10104/indexStore-P0JG
//GUH MAMMO IMG 172.19.1.67/10104/indexStore-P0UY
//MMMC IMG 172.18.7.202/10104/indexStore-P0RA
//SMH IMG 172.30.8.50/10104/indexStore-P0RB
//WHC IMG 172.25.120.25/10104/indexStore-P005
//WHC MAMMO IMG 172.25.110.46/10104/indexStore-P0UW
SOUTH_INDEX_LINK_AET = "indexStore-P0M5";
SOUTH_eIMG_AET_WM = "indexStore-P0WM";
SOUTH_eIMG_AET = "indexStore-P0T7";
LOCAL_GUH_IMG_AET = "indexStore-P0JG";
LOCAL_GUH_IMG_Mammo_AET = "indexStore-P0UY";
LOCAL_MMC_IMG_AET = "indexStore-P0RA";
LOCAL_SMH_IMG_AET = "indexStore-P0RB";
LOCAL_WHC_IMG_AET = "indexStore-P005";
LOCAL_WHC_IMG_Mammo_AET = "indexStore-P0UW";
LOCAL_WB4_6B4_IMG_AET= "indexStore-P05V";

//DateTime cutoffDate = new DateTime(2014, 1, 1, 0, 0, 0, 0);

def ipid = study.get("IssuerOfPatientID");

DateTime currentDateTime = new DateTime();
//DateTime cutoffDate = currentDateTime.minusYears(2);
DateTime cutoffDate = currentDateTime.minusMonths(24);
if (ipid != null && "MSMHC".equalsIgnoreCase(ipid) )
{
   cutoffDate = currentDateTime.minusYears(5);
}
log.debug(scriptName + "currentDateTime {}, cutoffDate {}, and IssuerOfPatientID {}", currentDateTime, cutoffDate, ipid);


DateTime studyDateTime = study.getStudyDateTime();

def idn = study.get("InstitutionalDepartmentName");
if (idn == null) {
  idn = "OTHER";
}

// Determine what type of study it is
def rad_study = false;
if ("RADIOLOGY".equalsIgnoreCase(idn)) {
  rad_study = true;
}

// WARNING: DO NOT REMOVE NEXT 6 LINES!!!
// check if this is a Test study that is used by HealthCheck service
if (study.getStudyInstanceUID() == '1.2.392.200036.9125.2.164186219242118163.64781111107.3536133' || study.getStudyInstanceUID() == '1.2.392.200036.9125.2.164186219242118163.64781111107.3536134') {
  log.info(scriptName + "{} study with StudyInstanceUID {} is used by HealthCheck service, skipping...", idn, study.getStudyInstanceUID());
  log.debug(scriptName + "END - Done looking at study with StudyInstanceUID {}", study.getStudyInstanceUID());
  return;
}

// WARNING: DO NOT REMOVE NEXT 9 LINES!!!
// check if this is a Test study that is used by South HealthCheck service
if (    study.getStudyInstanceUID() == '1.2.392.200036.9125.2.164186219242118163.172.27.6.39'
     || study.getStudyInstanceUID() == '1.2.392.200036.9125.2.164186219242118163.172.27.6.40'
     || study.getStudyInstanceUID() == '1.2.392.200036.9125.2.164186219242118163.172.27.6.41'
     || study.getStudyInstanceUID() == '1.2.392.200036.9125.2.164186219242118163.172.27.6.42' ) {
  log.info(scriptName + "{} study with StudyInstanceUID {} is used by South HealthCheck service, skipping...", idn, study.getStudyInstanceUID());
  log.debug(scriptName + "END - Done looking at study with StudyInstanceUID {}", study.getStudyInstanceUID());
  return;
}

// WARNING: DO NOT REMOVE NEXT 9 LINES!!!
// check if this is a Test study that is used by North HealthCheck service
if (    study.getStudyInstanceUID() == '1.2.392.200036.9125.2.164186219242118163.172.16.203.61'
     || study.getStudyInstanceUID() == '1.2.392.200036.9125.2.164186219242118163.172.16.203.62'
     || study.getStudyInstanceUID() == '1.2.392.200036.9125.2.164186219242118163.172.16.203.63'
     || study.getStudyInstanceUID() == '1.2.392.200036.9125.2.164186219242118163.172.16.203.64' ) {
  log.info(scriptName + "{} study with StudyInstanceUID {} is used by North HealthCheck service, skipping...", idn, study.getStudyInstanceUID());
  log.debug(scriptName + "END - Done looking at study with StudyInstanceUID {}", study.getStudyInstanceUID());
  return;
}

// If the study was prefetched, mark it as a purge candidate and move on
if (timeline.isPrefetched()) {
  if ( studyDateTime == null || (studyDateTime != null && studyDateTime.isBefore(cutoffDate)) ) {
    log.info(scriptName + "{} study with StudyInstanceUID {} and StudyDate {} can be marked as a candidate for purging since it was prefetched", idn, study.getStudyInstanceUID(), studyDateTime);
    ops.flagAsPurgeCandidate();
  } else {
    log.info(scriptName + "{} study with StudyInstanceUID {} was prefetched and has StudyDate {} which is more recent than cutoff date {} and hence will NOT be marked as a purge candidate", idn, study.getStudyInstanceUID(), studyDateTime, cutoffDate);
  }
  log.debug(scriptName + "END - Done looking at study with StudyInstanceUID {}", study.getStudyInstanceUID());
  return;
}

def studyInstanceAvailability = study.get(0x00080056);
log.info(scriptName + "DEBUG: {} study with StudyInstanceUID {} has studyInstanceAvailability = {}", idn, study.getStudyInstanceUID(), studyInstanceAvailability);
if ("UNAVAILABLE".equalsIgnoreCase(studyInstanceAvailability)) {
  log.info(scriptName + "{} study with StudyInstanceUID {} is not available or not ONLINE, skipping...", idn, study.getStudyInstanceUID());
  log.debug(scriptName + "END - Done looking at study with StudyInstanceUID {}", study.getStudyInstanceUID());
  return;
}

//
// The rest of the logic deals with studies that must be forwarded before being marked as purge candidates
//

// All studies are forwarded to the South Index Link immediately
//if (!timeline.wasFullyForwardedTo(SOUTH_INDEX_LINK_AET)) {
//  log.info("ILM policies - {} study with StudyInstanceUID {} can be forwarded to {} since we forward immediately", idn, study.getStudyInstanceUID(), SOUTH_INDEX_LINK_AET);
//  ops.scheduleStudyForward(SOUTH_INDEX_LINK_AET);
//}

// Define where to store long-term
def lta = null;
def modality = study.getModalities();

switch (ipid.toUpperCase()) {
    case "MEDSTAR":
        lta = SOUTH_eIMG_AET;
        break;

    case "FSH":
        lta = LOCAL_FSH_IMG_AET;
        if (modality.contains("MG")) {
          lta=LOCAL_FSH_IMG_Mammo_AET;
        }
        break;

    case "GSH":
        lta = LOCAL_GSH_IMG_AET;
        break;

    case "HHC":
        lta = LOCAL_HHC_IMG_AET;
        break;

    case "UMH":
        lta = LOCAL_UMH_IMG_AET;
        break;

    case "GUH":
        lta = LOCAL_GUH_IMG_AET;
        if (modality.contains("MG")) {
          lta=LOCAL_GUH_IMG_Mammo_AET;
        }
        break;

    case "MGI":
        lta = LOCAL_MMC_IMG_AET;
        break;

    case "MSMHC":
        lta = SOUTH_eIMG_AET_WM;
        break;

    case "MSMH":
        lta = LOCAL_SMH_IMG_AET;
        break;

    case "WB4":
        lta = LOCAL_WHC_IMG_AET;
        if (modality.contains("MG")) {
          lta=LOCAL_WHC_IMG_Mammo_AET;
        }
        else if ("CARDIOLOGY".equalsIgnoreCase(idn)) {
          lta= LOCAL_WB4_6B4_IMG_AET;
        }
        break;

    case "6B4":
        lta = LOCAL_WHC_IMG_AET;
        if (modality.contains("MG")) {
          lta=LOCAL_WHC_IMG_Mammo_AET;
        }
        else if ("CARDIOLOGY".equalsIgnoreCase(idn)) {
          lta= LOCAL_WB4_6B4_IMG_AET;
        }
        break;

    case "MAS":
        lta = SOUTH_eIMG_AET;
        break;

    case "MAS2":
        lta = SOUTH_eIMG_AET;
        break;

    case "MPP":
        lta = SOUTH_eIMG_AET;
        break;

    case "MSC":
        lta = SOUTH_eIMG_AET;
        break;

    case "SHAH":
        lta = SOUTH_eIMG_AET;
        break;

    default:
        lta = SOUTH_eIMG_AET;
}

// Deal with studies with an unknown long-term archive destination
// this is an extra 'catch all' block and we should never end up in side this block 
if (lta == null) {
  log.info(scriptName + "{} study with StudyInstanceUID {} is not a prefetched study, nor is the long-term archive system known because the issuer of patient id is {}", idn, study.getStudyInstanceUID(), ipid);

  if (timeline.wasFullyForwardedWithStorageCommitTo(SOUTH_eIMG_AET)) {
    if ( studyDateTime == null || (studyDateTime != null && studyDateTime.isBefore(cutoffDate)) ) {
      log.info(scriptName + "{} study with StudyInstanceUID {} and unknown IPID {} and StudyDate {} and can be marked as a candidate for purging since it has been forwarded to {}", idn, study.getStudyInstanceUID(), ipid, studyDateTime, lta);
      ops.flagAsPurgeCandidate();
    } else {
        log.info(scriptName + "{} study with StudyInstanceUID {} and IPID {} and StudyDate {} which is more recent than cutoff date {} and hence will NOT be marked as a purge candidate", idn, study.getStudyInstanceUID(), ipid, studyDateTime, cutoffDate);
    }
    log.debug(scriptName + "END - Done looking at study with StudyInstanceUID {}", study.getStudyInstanceUID());
    return;
  } else {
    log.info(scriptName + "{} study with StudyInstanceUID {} and IPID {} can NOT be marked as a candidate for purging since the study has NOT been forwarded to {}", idn, study.getStudyInstanceUID(), ipid, SOUTH_eIMG_AET);
  }

  log.debug(scriptName + "END - Done looking at study with StudyInstanceUID {}", study.getStudyInstanceUID());
  return;
}

// Check if the study can be purged (purging will only occur when space needs to be reclaimed)
if (!timeline.wasFullyForwardedWithStorageCommitTo(lta)) {
  log.info(scriptName + "{} study with StudyInstanceUID {} and IPID {} can NOT be marked as a candidate for purging since it has NOT been forwarded to {}", idn, study.getStudyInstanceUID(), ipid, lta);
  if (!rad_study) {
    // Other ologies - forward to long-term archive immediately
    log.info(scriptName + "{} study with StudyInstanceUID {} and IPID {} can be forwarded to {} since we forward non-Radiology studies immediately", idn, study.getStudyInstanceUID(), ipid, lta);
    ops.scheduleStudyForwardWithStorageCommit(lta);
  } else {
    // Radiology - release on ORU to eIMG
    if (timeline.isFinalReported()) {
      log.info(scriptName + "{} study with StudyInstanceUID {} and IPID {} can be forwarded to {} since there was a FINAL ORU observed for this study", idn, study.getStudyInstanceUID(), ipid, lta);
      ops.scheduleStudyForwardWithStorageCommit(lta);
    } else {
      minOruIdleDays = 2;
      gracePeriod = new DateTime().minusDays(minOruIdleDays);
      //log.info("ILM policies - DEBUG: {} study with StudyInstanceUID {} and IPID {} should not be updated since {} to qualify ...", idn, study.getStudyInstanceUID(), ipid, gracePeriod);
      if (gracePeriod.isAfter(study.getLastUpdateTime())) {
        log.info(scriptName + "{} study with StudyInstanceUID {} and IPID {} can be forwarded to {} since the study was last used {}, and we  forward studies for which no ORU was observed for {} days", idn, study.getStudyInstanceUID(), ipid, lta, study.getLastUpdateTime(), minOruIdleDays);
        ops.scheduleStudyForwardWithStorageCommit(lta);
      } else {
        log.info(scriptName + "{} study with StudyInstanceUID {} and IPID {} does NOT have a FINAL ORU and was last used {}, skipping...", idn, study.getStudyInstanceUID(), ipid, study.getLastUpdateTime());
      }
    }
  }
} else {
  if ( studyDateTime == null || (studyDateTime != null && studyDateTime.isBefore(cutoffDate)) ) {
    log.info(scriptName + "{} study with StudyInstanceUID {} and IPID {} and StudyDate {} can be marked as a candidate for purging since it has been forwarded to {}", idn, study.getStudyInstanceUID(), ipid, studyDateTime, lta);
    ops.flagAsPurgeCandidate();
  } else {
    log.info(scriptName + "{} study with StudyInstanceUID {} and IPID {} and was forwarded to {} and has StudyDate {} which is more recent than cutoff date {} and hence will NOT be marked as a purge candidate", idn, study.getStudyInstanceUID(), ipid, lta, studyDateTime, cutoffDate);
  }
}

//Added for MOF : khc 13817

if ("MOF".equalsIgnoreCase(InstitutionName)) {
  if (study.isBefore(cutoffDate)) {
    log.info(scriptName + "SIUID: {}, Dept: {}, IPID: {}, StudyDate: {}, CutOffDate: {}, MOF study can be marked as purge candidate", study.getStudyInstanceUID(), idn, ipid, studyDateTime, cutoffDate);
    ops.flagAsPurgeCandidate();
  } else {
    log.info(scriptName + "SIUID: {}, Dept: {}, IPID: {}, StudyDate: {}, CutOffDate: {}, MOF study will NOT be marked as purge candidate", study.getStudyInstanceUID(), idn, ipid, studyDateTime, cutoffDate)
  }

  log.debug(scriptName + "SIUID: {}, done processing", study.getStudyInstanceUID());
  return;
}



log.debug(scriptName + "END - Done looking at study with StudyInstanceUID {}", study.getStudyInstanceUID());

