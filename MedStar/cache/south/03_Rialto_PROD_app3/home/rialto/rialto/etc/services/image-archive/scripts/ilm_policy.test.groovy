/**
 *    ILM policy for MedStar TEST environment - TEST
 */

log.debug("ILM policies START - Executing against study with StudyInstanceUID {}", study.getStudyInstanceUID());

ops.flagAsPurgeCandidate();

log.debug("ILM policies END - Done looking at study with StudyInstanceUID {}", study.getStudyInstanceUID());
