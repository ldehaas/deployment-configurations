#!/bin/sh
#
# cassandra-new-relic      Start Cassandra monitor plugin
#
# chkconfig: 2345 08 92
# description:  Starts, stops and restarts the Cassandra monitor plugin
#
#

# Source function library.
. /etc/init.d/functions

# only usable for root
[ $EUID = 0 ] || exit 4

SERVER_HOME=/home/rialto/newrelic_3legs_plugin-2.0.0

NAME=cassandra-new-relic
DESC="Cassandra monitor plugin"
PIDFILE=/var/run/$NAME.pid
SCRIPTNAME=/etc/init.d/$NAME

case "$1" in
start)
    printf "%-50s" "Starting $NAME..."
    builtin cd $SERVER_HOME
    nohup java -jar plugin.jar >/dev/null 2>&1 & 
    PID=$!
    if [ -z $PID ]; then
        printf "%s\n" "Fail"
    else
        printf "%s" $PID > $PIDFILE
        printf "%s\n" "Ok"
    fi
    ;;
stop)
    printf "%-50s" "Stopping $NAME..."
    if [ -f "${PIDFILE}" ]; then
        PID="`cat $PIDFILE`"
        kill -TERM "${PID}"
        printf "%s\n" "Ok"
        rm -f "${PIDFILE}"
    else
        printf "%s\n" "Service not running"
    fi
    ;;
status)
    printf "%-50s" "Checking $NAME..."
    if [ -f $PIDFILE ]; then
        PID="`cat $PIDFILE`"
        if [ -z "`ps axf | grep ${PID} | grep -v grep`" ]; then
            printf "%s\n" "Process dead but pidfile exists"
        else
            printf "%s\n" "Running"
        fi
    else
        printf "%s\n" "Service not running"
    fi
    ;;
restart)
    $0 stop
    $0 start
    ;;
*)
    printf "Usage: $0 {start|stop|status|restart}\n"
    RETVAL=2
    ;;
esac
exit $RETVAL
