log.info("EMPI Morpher - START")

def messageType = get('/.MSH-9-1')
def triggerEvent = get('/.MSH-9-2')

log.info("messageType is: {}", messageType)
log.info("triggerEvent is: {}", triggerEvent)

if (!"ADT".equalsIgnoreCase(messageType)) {
    log.warn('EMPI Morpher - An unactionable message was provided, taking no action on message {}^{}', messageType, triggerEvent);
    return false
}

LOAD('/home/rialto/rialto/etc/services/common/common_medstar_hl7.groovy')
MedStar_HL7 ms = new MedStar_HL7(log, input)

ms.groomPids()

log.info("EMPI Morpher - End")
