/**
 *    ILM policy for MedStar - PROD
 *
 *    WARNING: Updating this script will update immediately without a Rialto restart. Use caution when updating a script on a live system.
 *
 *    Documentation:
 *       https://karoshealth.atlassian.net/wiki/display/RIALTO64/Policy+script
 *       https://karoshealth.atlassian.net/wiki/display/RIALTO64/ILM+policy+methods
 */


import org.joda.time.LocalDateTime;
import org.joda.time.DateTime;

def scriptName = "ILM Purge Policy - ";
def retentionPeriodMonths = 18;
def finalORUWaitPeriodDays = 2;

log.debug(scriptName + "SIUID: {}, AccN: {}, about to start processing", study.getStudyInstanceUID(), study.get("AccessionNumber") );

// North IMGs
LOCAL_FSH_IMG_AET       = "FSHINSITE01";        //FSH       IMG 172.23.8.65/10104/FSHINSITE01
LOCAL_FSH_IMG_Mammo_AET = "indexStore-P0UX";    //FSH MAMMO IMG 172.23.8.109/10104/indexStore-P0UX
LOCAL_GSH_IMG_AET       = "GSHINSITE01";        //GSH       IMG 172.16.80.66/10104/GSHINSITE01
LOCAL_HHC_IMG_AET       = "indexStore-P0HS";    //HHC       IMG 172.16.209.107/10104/indexStore-P0HS
LOCAL_UMH_IMG_AET       = "indexStore-P0H2";    //UMH       IMG 172.17.80.79/10104/indexStore-P0H2

// South IMGs
SOUTH_INDEX_LINK_AET    = "indexStore-P0M5";
SOUTH_eIMG_AET_WM       = "indexStore-P0WM";
SOUTH_eIMG_AET          = "indexStore-P0T7";    //South    eIMG 198.50.67.28/10104/indexStore-P0T7
LOCAL_GUH_IMG_AET       = "indexStore-P0JG";    //GUH       IMG 172.19.1.192/10104/indexStore-P0JG
LOCAL_GUH_IMG_Mammo_AET = "indexStore-P0UY";    //GUH MAMMO IMG 172.19.1.67/10104/indexStore-P0UY
LOCAL_MMC_IMG_AET       = "indexStore-P0RA";    //MMMC      IMG 172.18.7.202/10104/indexStore-P0RA
LOCAL_SMH_IMG_AET       = "indexStore-P0RB";    //SMH       IMG 172.30.8.50/10104/indexStore-P0RB
LOCAL_WHC_IMG_AET       = "indexStore-P005";    //WHC       IMG 172.25.120.25/10104/indexStore-P005
LOCAL_WHC_IMG_Mammo_AET = "indexStore-P0UW";    //WHC MAMMO IMG 172.25.110.46/10104/indexStore-P0UW
LOCAL_WB4_6B4_IMG_AET   = "indexStore-P05V";

def ipid = study.get("IssuerOfPatientID");

DateTime currentDateTime = new DateTime();
DateTime cutoffDate = currentDateTime.minusMonths(retentionPeriodMonths);
// log.debug(scriptName + "SIUID: {}, IPID: {}, currentDateTime: {}, cutoffDate: {}", study.getStudyInstanceUID(), ipid, currentDateTime, cutoffDate);


DateTime studyDateTime = study.getStudyDateTime();

def idn = study.get("InstitutionalDepartmentName");
if (idn == null) {
  idn = "OTHER";
}

// Determine what type of study it is
def rad_study = false;
if ("RADIOLOGY".equalsIgnoreCase(idn)) {
  rad_study = true;
}

// WARNING: DO NOT REMOVE NEXT 6 LINES!!!
// check if this is a Test study that is used by HealthCheck service
if (study.getStudyInstanceUID() == '1.2.392.200036.9125.2.164186219242118163.64781111107.3536133' || study.getStudyInstanceUID() == '1.2.392.200036.9125.2.164186219242118163.64781111107.3536134') {
  log.info(scriptName + "SIUID: {}, Dept: {}, is used by HealthCheck service, skipping...", study.getStudyInstanceUID(), idn);
  log.debug(scriptName + "SIUID: {}, Done processing", study.getStudyInstanceUID());
  return;
}

// WARNING: DO NOT REMOVE NEXT 9 LINES!!!
// check if this is a Test study that is used by South HealthCheck service
if (    study.getStudyInstanceUID() == '1.2.392.200036.9125.2.164186219242118163.172.27.6.39'
     || study.getStudyInstanceUID() == '1.2.392.200036.9125.2.164186219242118163.172.27.6.40'
     || study.getStudyInstanceUID() == '1.2.392.200036.9125.2.164186219242118163.172.27.6.41'
     || study.getStudyInstanceUID() == '1.2.392.200036.9125.2.164186219242118163.172.27.6.42' ) {
  log.info(scriptName + "SIUID: {}, Dept: {}, is used by South HealthCheck service, skipping...", study.getStudyInstanceUID(), idn);
  log.debug(scriptName + "SIUID: {}, Done processing", study.getStudyInstanceUID());
  return;
}

// WARNING: DO NOT REMOVE NEXT 9 LINES!!!
// check if this is a Test study that is used by North HealthCheck service
if (    study.getStudyInstanceUID() == '1.2.392.200036.9125.2.164186219242118163.172.16.203.61'
     || study.getStudyInstanceUID() == '1.2.392.200036.9125.2.164186219242118163.172.16.203.62'
     || study.getStudyInstanceUID() == '1.2.392.200036.9125.2.164186219242118163.172.16.203.63'
     || study.getStudyInstanceUID() == '1.2.392.200036.9125.2.164186219242118163.172.16.203.64' ) {
  log.info(scriptName + "SIUID: {}, Dept: {}, is used by North HealthCheck service, skipping...", study.getStudyInstanceUID(), idn);
  log.debug(scriptName + "SIUID: {}, done processing", study.getStudyInstanceUID());
  return;
}

// If the study was prefetched, mark it as a purge candidate and move on
if (timeline.isPrefetched()) {
  if ( studyDateTime == null || (studyDateTime != null && studyDateTime.isBefore(cutoffDate)) ) {
    log.info(scriptName + "SIUID: {}, Dept: {}, StudyDate: {}, prefetched and older than cutoffDate: {}, about to mark as purge candidate", study.getStudyInstanceUID(), idn, studyDateTime, cutoffDate);
    ops.flagAsPurgeCandidate();
  } else {
    log.info(scriptName + "SIUID: {}, Dept: {}, StudyDate: {}, prefetched but more recent than cutoffDate: {}, will NOT mark as purge candidate", study.getStudyInstanceUID(), idn, studyDateTime, cutoffDate);
  }
  log.debug(scriptName + "SIUID: {}, done processing.", study.getStudyInstanceUID());
  return;
}

def studyInstanceAvailability = study.get(0x00080056);
log.info(scriptName + "DEBUG: SIUID: {}, Dept: {}, InstanceAvailability: {}", study.getStudyInstanceUID(), idn,  studyInstanceAvailability);
if ("UNAVAILABLE".equalsIgnoreCase(studyInstanceAvailability)) {
  log.info(scriptName + "SIUID: {}, Dept: {}, is not available or not ONLINE, skipping...", study.getStudyInstanceUID(), idn);
  log.debug(scriptName + "SIUID: {}, done processing", study.getStudyInstanceUID());
  return;
}

//
// The rest of the logic deals with studies that must be forwarded to Long-Term Archive (LTA)
//

// Define LTA destination based on IssuerOfPatientID and Modality type or InstitutionalDepartmentName
def lta = null;
def modality = study.getModalities();

switch (ipid.toUpperCase()) {
    case "MEDSTAR":
        lta = SOUTH_eIMG_AET;
        break;

    case "FSH":
        lta = LOCAL_FSH_IMG_AET;
        if (modality.contains("MG")) {
          lta=LOCAL_FSH_IMG_Mammo_AET;
        }
        break;

    case "GSH":
        lta = LOCAL_GSH_IMG_AET;
        break;

    case "HHC":
        lta = LOCAL_HHC_IMG_AET;
        break;

    case "UMH":
        lta = LOCAL_UMH_IMG_AET;
        break;

    case "GUH":
        lta = LOCAL_GUH_IMG_AET;
        if (modality.contains("MG")) {
          lta=LOCAL_GUH_IMG_Mammo_AET;
        }
        break;

    case "MGI":
        lta = LOCAL_MMC_IMG_AET;
        break;

    case "MSMHC":
        lta = SOUTH_eIMG_AET_WM;
        break;

    case "MSMH":
        lta = LOCAL_SMH_IMG_AET;
        break;

    case "WB4":
        lta = LOCAL_WHC_IMG_AET;
        if (modality.contains("MG")) {
          lta=LOCAL_WHC_IMG_Mammo_AET;
        }
        else if ("CARDIOLOGY".equalsIgnoreCase(idn)) {
          lta= LOCAL_WB4_6B4_IMG_AET;
        }
        break;

    case "6B4":
        lta = LOCAL_WHC_IMG_AET;
        if (modality.contains("MG")) {
          lta=LOCAL_WHC_IMG_Mammo_AET;
        }
        else if ("CARDIOLOGY".equalsIgnoreCase(idn)) {
          lta= LOCAL_WB4_6B4_IMG_AET;
        }
        break;

    case "MAS":
        lta = SOUTH_eIMG_AET;
        break;

    case "MPP":
        lta = SOUTH_eIMG_AET;
        break;

    case "MSC":
        lta = SOUTH_eIMG_AET;
        break;

    case "SHAH":
        lta = SOUTH_eIMG_AET;
        break;

    default:
        // Deal with studies with an unknown long-term archive destination
        lta = SOUTH_eIMG_AET;
}


// Check if the study should be forwarded to LTA
if (!timeline.wasFullyForwardedWithStorageCommitTo(lta)) {

  log.info(scriptName + "SIUID: {}, Dept: {}, IPID: {}, StudyDate: {}, NOT fully forwarded to {}, skipping...", study.getStudyInstanceUID(), idn, ipid, studyDateTime, lta);

} else {

  if ( studyDateTime == null || (studyDateTime != null && studyDateTime.isBefore(cutoffDate)) ) {
    log.info(scriptName + "SIUID: {}, Dept: {}, IPID: {}, StudyDate: {}, fully forwarded to {}, about to mark as purge candidate", study.getStudyInstanceUID(), idn, ipid, studyDateTime, lta);
    ops.flagAsPurgeCandidate();
  } else {
    log.info(scriptName + "SIUID: {}, Dept: {}, IPID: {}, StudyDate: {}, fully forwarded to {} but newer than cutoffDate {}, will NOT mark as purge candidate", idn, study.getStudyInstanceUID(), ipid, lta, studyDateTime, cutoffDate);
  }

}

log.debug(scriptName + "SIUID: {}, done processing", study.getStudyInstanceUID());


