/* instance_availability_eval_morpher.groovy */
/* Connect Instance Availablity Filter Eval Morpher */
/* https://karoshealth.atlassian.net/wiki/display/RIALTO62/Connect+service#Connectservice-<instanceAvailabilityFilter> */

def scriptName = "Connect IAF Eval morpher - ";

log.info( scriptName + "START" )

if (source == null) {
    log.info( scriptName + "Source CFIND result is null..." )
}

if (target == null) {
    log.info( scriptName + "Target CFIND result is null..." )
}

sourceNumSeries = source.get("NumberOfStudyRelatedSeries")
targetNumSeries = target.get("NumberOfStudyRelatedSeries")
studyInstanceUID = source.get("StudyInstanceUID")
instanceAvailability = target.get("InstanceAvailability")

log.info( scriptName + "processing StudyInstanceUID: {}", studyInstanceUID )

if (instanceAvailability != null && !"ONLINE".equalsIgnoreCase(instanceAvailability) ) {
    log.info( scriptName + "SIUID: {}, InstanceAvailability = {}, will prefetch this study", studyInstanceUID, instanceAvailability )
    return true
}

log.info( scriptName + "SIUID: {}, Number of series in source: {}", studyInstanceUID, sourceNumSeries )
log.info( scriptName + "SIUID: {}, Number of series in target: {}", studyInstanceUID, targetNumSeries )

if (sourceNumSeries != null && targetNumSeries != null) {
    if (sourceNumSeries.toInteger() <= targetNumSeries.toInteger()) {
        log.info( scriptName + "SIUID: {}, MoveDestination has all series for this study already.", studyInstanceUID )
        return false;
    }
    else
    {
        log.info( scriptName + "SIUID: {}, will issue a C-Move request for this study.", studyInstanceUID)
    }
}
else
{
    log.info( scriptName + "SIUID: {}, Unable to determine if all series are in cache at MoveDestination, will issue a C-Move request for this study", studyInstanceUID )
}

log.info( scriptName + "END" )
