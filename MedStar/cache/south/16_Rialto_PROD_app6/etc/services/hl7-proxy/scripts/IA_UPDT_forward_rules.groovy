log.info("IA Update Morpher - Start")

def messageType = get('/.MSH-9-1')
def triggerEvent = get('/.MSH-9-2')

log.info("messageType is: {}", messageType)
log.info("triggerEvent is: {}", triggerEvent)

if (messageType != null && triggerEvent != null) {
    if ("ADT".equals(messageType) && ("A08".equals(triggerEvent) || "A40".equals(triggerEvent) || "A31".equals(triggerEvent) || "A39".equals(triggerEvent))) {
        log.info("IA Update Morpher - Will forward message to IA Update")
    } else {
        log.info('IA Update Morpher - An unactionable message was provided, taking no action on message {}^{}', messageType, triggerEvent);
        return false
    }
}

LOAD('/home/rialto/rialto/etc/services/common/common_medstar_hl7.groovy')
MedStar_HL7 ms = new MedStar_HL7(log, input)

ms.groomPids()

log.info("IA Update Morpher - End")
