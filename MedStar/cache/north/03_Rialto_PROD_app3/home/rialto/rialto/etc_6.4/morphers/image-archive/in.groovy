log.info("IA Inbound Morpher - START")

LOAD('../common_medstar_dicom.groovy')

def ipid = get(IssuerOfPatientID)
def callingAET = getCallingAETitle()
def accn = get(AccessionNumber);

def siuid = get(StudyInstanceUID);
if (siuid != null && (    siuid.equals("1.2.840.113711.5542834.2.4876.214114601.26.2116281012.1930")
                       || siuid.equals("1.2.840.113711.8459186.1.9760.525941013.26.2116281012.1280")
                       || siuid.equals("1.2.840.113711.8459186.1.3416.421042191.26.2116281012.1830")
                       || siuid.equals("1.2.840.113711.8459186.1.7788.526005123.26.2116281012.17500")
                       || siuid.equals("1.2.840.113711.8459186.1.9996.516463957.26.2116281012.12150")
                       || siuid.equals("1.2.840.113696.563627.530.2051296.20140910092612")
                       || siuid.equals("1.2.840.113696.563627.530.1919746.2014021409369")
                       || siuid.equals("1.2.840.113696.563627.530.1188610.20101012090846")
                     )
   )
{
    log.info("IA Inbound Morpher - SIUID: {} - detected one of the studies with large number of versions, will ignore this ingestion request...", siuid)
    log.info("IA Inbound Morpher - End")
    return false
}

// Stamp unconditionally on local MMMC IMG prefetches
if (!"MGI".equalsIgnoreCase(ipid) && "indexQuery-P0RA".equalsIgnoreCase(callingAET)) {
    set(IssuerOfPatientID, "MGI")
}

// Morph back pid for data received from EasyViz
manufacturer = get(Manufacturer)                                              
if (manufacturer != null && manufacturer.equals("Medical-Insight A/S")) {   
    other_patient_id = get(OtherPatientIDs)                             
    if (other_patient_id != null && other_patient_id.contains("^")) {                         
       def (patient_id, other_patient_id_issuer) = other_patient_id.split("\\^")
       set(PatientID, patient_id)
       if (other_patient_id_issuer != null && other_patient_id_issuer.contains("&")) {
           def (issuer_only, other_patient_id_junk) = other_patient_id_issuer.split("\\&")
           other_patient_id_issuer = issuer_only
       }
       set(IssuerOfPatientID, other_patient_id_issuer)
       remove(OtherPatientIDs)
       remove(OtherPatientIDsSequence)
       log.info("IA Inbound Morpher - Incoming object from EasyViz! Set PatientID to {}, set issuerOfPatientID to {}, and removed the other patient id sequence", other_patient_id, other_patient_id_issuer)
    }
}

MedStar_DICOM ms = new MedStar_DICOM(log, input)

ms.uniqueTemporaryId()
ms.stampUniversalEntityId()

/*
--------------------------------------------------------------------
This code is commented out as a request from KHC#11832
--------------------------------------------------------------------
if (!callingAET.equalsIgnoreCase("LILA") && !callingAET.equalsIgnoreCase("LILAPROD") && !callingAET.equalsIgnoreCase("CDIMPAZ3") && !callingAET.equalsIgnoreCase("CDIMPORTFL")) {
  if ( accn == null || (accn != null && !accn.startsWith("EX") && !accn.startsWith("MOI")) ) { 
    ms.stampInstitutionalName()
  } 
}*/

ms.stampInstitutionalDepartmentName(getCalledAETitle())
ms.copyProcedureCodeToPrivateTags()
ms.fixSOPForHangingProtocol()

log.info("IA Inbound Morpher - End")


