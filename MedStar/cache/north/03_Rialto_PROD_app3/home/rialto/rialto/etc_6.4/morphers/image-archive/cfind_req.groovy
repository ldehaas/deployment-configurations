log.info("IA Inbound CFind Morpher - START")

LOAD('../common_medstar_dicom.groovy')
MedStar_DICOM ms = new MedStar_DICOM(log, input)

def todayDate = new Date()
def today = todayDate.format('yyyyMMdd')
def yesterdayDate = todayDate -1
def yesterday = yesterdayDate.format('yyyyMMdd')

def issuerOfPatientID = input.get('IssuerOfPatientID')
if (issuerOfPatientID != null) {
    ms.stampUniversalEntityId();
}

def aeTitle = getCallingAETitle()
log.debug( "Calling AET: {}", getCallingAETitle() )

def MWHC_AET_list = [
    'POBCT',
    'SCANPOBPK',
    'POBSCANTECH',
    'BONEDENSITY',
    'CBHTECH_RT',
    'CBHTECH_LT',
    'PACSSCANCICT',
    'CCRCT_LT',
    'CCRCT_RT',
    'PACSSCANCCR',
    'SCANMAG2',
    'SCANTITANMR',
    'SCANMAG1',
    'DXMAIN1',
    'DXMAIN2',
    'SCANFLUORO',
    'PACSSCAN_US',
    'SCANMOR',
    'PACSSCAN_TFO',
    'CT4THFLOOR',
    'CARDMRI'
]

if ( MWHC_AET_list.any { it == aeTitle.toUpperCase() } ) {
    log.debug("Inferring WHC context for CFIND from AE: {}", aeTitle)
    set('IssuerOfPatientID', 'WB4')
    set('IssuerOfPatientIDQualifiersSequence/UniversalEntityID', '2.16.840.1.114107.1.1.16.2.3')
    set('IssuerOfPatientIDQualifiersSequence/UniversalEntityIDType', 'ISO')
}

if (aeTitle != null && aeTitle.startsWith('MMC_PAPER')) {
    log.debug("Inferring MGI context for CFIND from AE: {}", aeTitle)
    set('IssuerOfPatientID', 'MGI')
    set('IssuerOfPatientIDQualifiersSequence/UniversalEntityID', '2.16.840.1.114107.1.1.16.2.10')
    set('IssuerOfPatientIDQualifiersSequence/UniversalEntityIDType', 'ISO')
}

if (aeTitle != null && aeTitle.startsWith('SMC')) {
    log.debug("Inferring MSMHC context for CFIND from AE: {}", aeTitle)
    set('IssuerOfPatientID', 'MSMHC')
    set('IssuerOfPatientIDQualifiersSequence/UniversalEntityID', '2.16.840.1.114107.1.1.16.2.12')
    set('IssuerOfPatientIDQualifiersSequence/UniversalEntityIDType', 'ISO')
}

//Limit queries to Issuer/Date range based on calling AE
if (aeTitle != null && (aeTitle == "PSCANGCT"||aeTitle == "PSCANGCTED")) {
    limitCfindIPIDAndDate(aeTitle, "CT", yesterday, today)
}

if (aeTitle != null && (aeTitle == "PSCANGDRED"||aeTitle == "PSCANGDRMAIN"||aeTitle == "PSCANGDRMAIN1"||aeTitle == "PSCANGDRMOB"||aeTitle == "PSCANGDRPOB")) {
    limitCfindIPIDAndDate(aeTitle, "CR", yesterday, today)
}

if (aeTitle != null && aeTitle == "PSCANGIR") {
    limitCfindIPIDAndDate(aeTitle, "XA", yesterday, today)
}

if (aeTitle != null && aeTitle == "PSCANGMA") {
    limitCfindIPIDAndDate(aeTitle, "MA", yesterday, today)
}

if (aeTitle != null && aeTitle == "PSCANGNM") {
    limitCfindIPIDAndDate(aeTitle, "NM", yesterday, today)
}

if (aeTitle != null && (aeTitle == "PSCANGUS1"||aeTitle == "PSCANGUS2")) {
    limitCfindIPIDAndDate(aeTitle, "US", yesterday, today)
}

if (aeTitle != null && aeTitle == "PSCANGMRI") {
    limitCfindIPIDAndDate(aeTitle, "MR", yesterday, today)
}

//method for limit cfind response to GSH specific study with modality from today and yesterday
def limitCfindIPIDAndDate(aet, modality, p_yesterday, p_today)
{
    log.debug("Limiting queries to GSH and {} today and yesterday from AE: {}",  modality, aet)
    set('IssuerOfPatientID', 'GSH')
    set('IssuerOfPatientIDQualifiersSequence/UniversalEntityID', '2.16.840.1.114107.1.1.16.2.2')
    set('IssuerOfPatientIDQualifiersSequence/UniversalEntityIDType', 'ISO')
    set('Modality', modality)
    set('StudyDate', p_yesterday + "-" + p_today)
}

log.info("CFind Req: {}", input)

log.info("IA Inbound CFind Morpher - End")
