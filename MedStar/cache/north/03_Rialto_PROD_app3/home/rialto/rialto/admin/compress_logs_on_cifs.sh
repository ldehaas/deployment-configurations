#!/bin/bash

LOCKFILE="/tmp/`basename $0`"
LOCKFD=99

# PRIVATE
_lock()             { flock -$1 $LOCKFD; }
_no_more_locking()  { _lock u; _lock xn && rm -f $LOCKFILE; }
_prepare_locking()  { eval "exec $LOCKFD>\"$LOCKFILE\""; trap _no_more_locking EXIT; }

# ON START
_prepare_locking

# PUBLIC
exlock_now()        { _lock xn; }  # obtain an exclusive lock immediately or fail
exlock()            { _lock x; }   # obtain an exclusive lock
shlock()            { _lock s; }   # obtain a shared lock
unlock()            { _lock u; }   # drop a lock

### BEGIN OF SCRIPT ###

# Simplest example is avoiding running multiple instances of script.
exlock_now || exit 1


cd /mnt/rialto_local_rw/logs

echo "$(date +%Y-%m-%d:%H:%M:%S) : About to remove compressed log files older than 62 days ..."
#find /mnt/rialto_local_rw/logs/rialto.HHCERSKCH03V.log*.gz -mtime +62 -exec rm {} \;

find . -type f -iname "rialto.HHCERSKCH03V.log*.gz" -mtime +62 -print0 | while IFS= read -r -d $'\0' old_file; do
    echo "$(date +%Y-%m-%d:%H:%M:%S) : Removing $old_file ..."
    rm $old_file
done

echo "$(date +%Y-%m-%d:%H:%M:%S) : About to compressed Rialto log files on CIFS share ..."
for file in rialto.HHCERSKCH03V.log.20??-??-??
do
    cur_time=$(date +%k%M)

    if [ $cur_time -ge 2200 ] || [ $cur_time -ge 0 -a $cur_time -lt 600 ]; then 
        # calculate maximum run duration in seconds
        max_exec_duration=0
        now_sec=$(date '+%s')
        window_start=$(date -d 'today 22:00:00' '+%s')
        window_stop=$(date -d 'today 6:00:00' '+%s')

        if [ $now_sec -ge $window_start ]; then
            window_stop=$(( window_stop + 86400 ))
        else
            window_start=$(( window_start - 86400 ))
        fi

        #echo "DEBUG: window_start: $(date -d @${window_start} +"%Y-%m-%d:%H:%M:%S")"
        #echo "DEBUG: window_stop: $(date -d @${window_stop} +"%Y-%m-%d:%H:%M:%S")"        
        
        max_exec_duration=$(( window_stop - now_sec ))
        echo "$(date +%Y-%m-%d:%H:%M:%S) : Time window between $(date -d @${window_start} +"%Y-%m-%d:%H:%M:%S") and $(date -d @${window_stop} +"%Y-%m-%d:%H:%M:%S"), max_exec_duration = $max_exec_duration seconds" 

        filesize=$(stat -c%s "$file")
        echo "$(date +%Y-%m-%d:%H:%M:%S) : Compressing $file ($filesize bytes) ..."
        SECONDS=0
        timeout $max_exec_duration gzip $file
        duration=$SECONDS
        echo "$(date +%Y-%m-%d:%H:%M:%S) : Elapsed time : $(($duration / 60)) minutes and $(($duration % 60)) seconds"

        # OPTIONAL: delay between compressing individual log files
        #sleep 60

    else
        echo "$(date +%Y-%m-%d:%H:%M:%S) : Outside of window, exiting ..."
        exit;
    fi
done;

