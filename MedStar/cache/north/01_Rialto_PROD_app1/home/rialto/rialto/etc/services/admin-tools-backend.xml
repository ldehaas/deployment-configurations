<?xml version="1.0"?>
<config>

    <service type="navigator.server" id="navigator.server">
        <device idref="pix" />
        <device idref="pdq" />
        <device idref="xdsrep"/>
        <device idref="xdsreg"/>

        <config>
            <prop name="CrossAffinityDomainID" value="${System-AffinityDomain}"/>
            <prop name="CassandraConfiguration">
                <clusterHosts>${CassandraClusterHosts}</clusterHosts>
                <clusterDatacenterName>${CassandraClusterDatacenterName}</clusterDatacenterName>
                <keyspaceName>${CassandraKeyspacePrefix}navigator</keyspaceName>
                <keyspaceReplicationStrategy>NetworkTopologyStrategy</keyspaceReplicationStrategy>
                <keyspaceReplicationStrategyOptions>${CassandraReplicationOption}</keyspaceReplicationStrategyOptions>
                <consistencyLevel>LOCAL_QUORUM</consistencyLevel>
                <serialConsistencyLevel>LOCAL_SERIAL</serialConsistencyLevel>
                <keyspaceCreateEnabled>true</keyspaceCreateEnabled>
                <dynamicConsistencyLevelEnabled>false</dynamicConsistencyLevelEnabled>
                <schemaUpdateEnabled>false</schemaUpdateEnabled>
            </prop>
            <prop name="Cluster">
                <name>NavigatorServerClusterGroupId</name>
                <udpstack>
                    <bindAddress>${Navigator-Server-Cluster-Bind-IP}</bindAddress>
                    <multicastAddress>${Navigator-Server-Cluster-Multicast-IP}</multicastAddress>
                    <multicastPort>${Navigator-Server-Cluster-Multicast-PORT}</multicastPort>
                </udpstack>
            </prop>

            <!-- Rialto XDS Registry cannot search by extended metadata -->
            <prop name="ExtendedMetadataAttributes"></prop>
            <prop name="SystemDefaultPermissions">
                <permission name="rialto.navigator.patients.search.internal" value="true"/>
                <permission name="rialto.navigator.patients.search.external" value="false"/>
                <permission name="rialto.navigator.study.metadata.view" value="true" />
                <permission name="rialto.navigator.study.move" value="true" />
                <permission name="rialto.navigator.study.changeavailability" value="false" />
                <permission name="rialto.navigator.study.physical.delete" value="false"/>
                <permission name="rialto.navigator.docs.metadata.view.unknown.internal" value="true"/>
                <permission name="rialto.navigator.docs.metadata.view.unknown.external" value="true"/>
                <permission name="rialto.navigator.docs.metadata.edit.unknown.internal" value="false"/>
                <permission name="rialto.navigator.docs.metadata.edit.unknown.external" value="false"/>
                <permission name="rialto.navigator.docs.move.unknown.internal" value="false" />
                <permission name="rialto.navigator.docs.view.unknown.internal.always" value="false"/>
                <permission name="rialto.navigator.docs.view.unknown.external.always" value="false"/>
                <permission name="rialto.navigator.docs.view.unknown.internal.onbreakglass" value="true"/>
                <permission name="rialto.navigator.docs.view.unknown.external.onbreakglass" value="true"/>
                <permission name="rialto.navigator.docs.upload.internal" value="false" />
                <permission name="rialto.navigator.study.qc" value="false" />
                <permission name="rialto.navigator.study.version.restore" value="false" />
                <permission name="rialto.navigator.study.reconciliation" value="false" />
                <permission name="rialto.vip.access" value="false" />
            </prop>

            <prop name="DefaultConfidentialityCodeSchemeID" value="1.2.3.4.5.6.1" />
            <prop name="DefaultConfidentialityCodeSchemeName" value="Karos Health demo confidentialityCodes" />
            <prop name="MaximumPatientResultSetSize" value="${Navigator-MaxPatientSearchResults}"/>
            <prop name="MaximumDocumentResultSetSize" value="${Navigator-MaxDocumentSearchResults}"/>
            <prop name="AllowMetadataUpdates">${XDSRegistry-MetadataUpdateEnabled}</prop>
            <prop name="ConfidentialityCodeSchemaName">TEST</prop>
            <prop name="web-api-path" value="${Navigator-APIBasePath}"/>
            <prop name="UseTargetDomainForPDQ">true</prop>

            <include location="patientidentitydomains.xml"/>
            <include location="admin-tools/cacheStorage.xml"/>

            <prop name="XDSDocumentCodes">
                <classification name="classCodes">
                    <Code codeValue="DICOM Study Manifest" schemeName="Karos Health demo classCodes" schemeID="1.2.3.4.5.6.7" displayName="DICOM Study Manifest" />
                    <Code codeValue="DR" schemeName="Karos Health demo classCodes" schemeID="1.2.3.4.5.6.7" displayName="Study Report" />
                    <Code codeValue="Continuity" schemeName="Karos Health demo classCodes" schemeID="1.2.3.4.5.6.7" displayName="Continuity of Care" />
                    <Code codeValue="History" schemeName="Karos Health demo classCodes" schemeID="1.2.3.4.5.6.7" displayName="Clinical History and Physical" />
                </classification>
                <classification name="formatCodes">
                    <Code codeValue="PDF" schemeName="Karos Health demo formatCodes" schemeID="1.2.3.4.5.6.9" displayName="PDF" />
                    <Code codeValue="DICOM" schemeName="Karos Health demo formatCodes" schemeID="1.2.3.4.5.6.9" displayName="DICOM" />
                    <Code codeValue="JPEG" schemeName="Karos Health demo formatCodes" schemeID="1.2.3.4.5.6.9" displayName="JPEG" />
                    <Code codeValue="CDA" schemeName="Karos Health demo formatCodes" schemeID="1.2.3.4.5.6.9" displayName="CDA Document" />
                </classification>
                <classification name="healthcareFacilityTypeCodes">
                    <Code codeValue="HCFacility" schemeName="Karos Health demo healthcareFacilityTypeCodes" schemeID="1.2.3.4.5.6.0" displayName="Healthcare Facility" />
                </classification>
                <classification name="practiceSettingCodes">
                    <Code codeValue="PracticeSetting" schemeName="Karos Health demo practiceSettingCodes" schemeID="1.2.3.4" displayName="Practice Setting" />
                </classification>
                <classification name="contentTypeCodes">
                    <Code codeValue="Imaging Study" schemeName="Karos Health demo contentTypeCodes" schemeID="1.2.3.4.5.6.8" displayName="Imaging Study" />
                    <Code codeValue="DR" schemeName="Karos Health demo contentTypeCodes" schemeID="1.2.3.4.5.6.8" displayName="Study Report" />
                    <Code codeValue="Continuity" schemeName="Karos Health demo contentTypeCodes" schemeID="1.2.3.4.5.6.8" displayName="Continuity of Care" />
                    <Code codeValue="History" schemeName="Karos Health demo contentTypeCodes" schemeID="1.2.3.4.5.6.8" displayName="Clinical History and Physical" />
                </classification>
                <classification name="typeCodes">
                    <Code codeValue="TC1" schemeName="Karos Health demo typeCodes" schemeID="1.2.3.4" displayName="Hospital Consultation Note" />
                    <Code codeValue="TC2" schemeName="Karos Health demo typeCodes" schemeID="1.2.3.4" displayName="Study Report" />
                    <Code codeValue="TC3" schemeName="Karos Health demo typeCodes" schemeID="1.2.3.4" displayName="Imaging Study" />
                </classification>
            </prop>
        </config>

    </service>

</config>

