// CFIND response morpher for modality worklist system tests
// NOTE: Updating an MWL service morpher will update immediately without a Rialto restart. Use caution when updating a morpher on a live system.

// NOTE: this script should NOT filter anything if the callingAET = RIALTO (this is needed for Auto-reconciliation)


import java.text.SimpleDateFormat
import org.joda.time.LocalDateTime;
import org.joda.time.DateTime;
import org.joda.time.format.DateTimeFormat;
import org.joda.time.format.DateTimeFormatter;

def scriptName = "MWL CFind Response Morpher - "

callingAET = getCallingAETitle()

log.debug( scriptName + "Calling AET: {}", getCallingAETitle() );
log.debug( scriptName + "input: {}\n", input );

def MMMC_AET_list = [
     'MMC_PAPER'
    ,'MMC_FUJI_GO'
    ,'MMC_IU22_VAS'
    ,'MMC_IU22_GEN'
    ,'AE_REVIEWER'
    ,'FUJI_IIP2_SCU'
    ,'FUJI_IIP4_SCU'
    ,'guhrad3d01-AQNET'
    ,'MMC_FRONTDESK'
    ,'MMC_PAPER_CT'
    ,'MMC_PAPER_FDESK'
    ,'MMC_PAPER_US1'
    ,'MMC_PAPER_VAS'
    ,'MMC_PAPER_XRAY2'
    ,'MMMC_CTPAPER'
    ,'MMMC_CTPAPER_SCU'
    ,'MMMC_FRONTDESK'
    ,'MMMC_MRIPAPER_SC'
    ,'MMMC_NM_PAPER'
    ,'MMMC_RAD2_PAPER'
    ,'MMMC_US1_PAPER'
    ,'MMMC_US2_PAPER'
    ,'MMMC_XRAY2'
    ,'MMMCADMIN'
    ,'MRC25905'
    ,'PHILIPSIU22_VAS'
    ,'SYMBIAT1'
    ,'SYNGO_P1'
    ,'TM_CT_CMW_V3.00'
    ,'US2_SCU'
    ,'MMC_FUJI_IIP3'
]

def MGUH_AET_list = [
     'GUH_AET1'
    ,'GUH_AET2'
    ,'GUH_AET3'
]

def MSMHC_AET_list = [
     '301877RF2'
    ,'301877RF8'
    ,'ACR'
    ,'BXSPEC1'
    ,'BXSPEC1_WIFI'
    ,'CARM_OEC_2'
    ,'CRPORT1'
    ,'CRPORT1_WIFI'
    ,'CX50'
    ,'CX501'
    ,'CX50_1'
    ,'CX50_2'
    ,'CYSTORM1'
    ,'EDSONO1_WIRELESS'
    ,'FD20_1'
    ,'GEDXRM3'
    ,'GELUNAR'
    ,'HOLOGIC_1'
    ,'HOLOGIC_2'
    ,'HOLOGIC_3'
    ,'HOLOGIC_4'
    ,'IE332'
    ,'INFINIA'
    ,'ISOL_1'
    ,'LOGICE9_US1'
    ,'LOGICE9_US2'
    ,'LOGICE9_US3'
    ,'Medimage'
    ,'NM3_ACQU'
    ,'OEC_9800A'
    ,'OEC_9900'
    ,'OSIRIX2'
    ,'PHILIPS-GJ1ZRBX'
    ,'PMSRIS'
    ,'RM1GEDX_SMHC'
    ,'RM3GEDX_SMHC'
    ,'RSXVimport'
    ,'SAMSUNGPORT1'
    ,'SAMSUNGPORT2'
    ,'SAMSUNGRM2'
    ,'SAMSUNGRM8'
    ,'SONOSITE'
    ,'SimplyPhysics'
    ,'US303-OP'
    ,'USENDO'
    ,'VOLCANO_1'
    ,'VOLCANO_2'
    ,'WINDOWS-LVD6BOF'
    ,'XTRAVISION_1'
    ,'angio1_aw'
    ,'angio1_dl'
    ,'angio2'
    ,'angio2_dlx'
    ,'aw1'
    ,'cathlab-us1'
    ,'cathlab-us2'
    ,'cathlab-us3'
    ,'ct01'
    ,'ct02_64simag'
    ,'ct02_64swrk'
    ,'ct2_op'
    ,'echo_1'
    ,'ge_logic10_op'
    ,'ge_logic9_op'
    ,'mri2_op'
    ,'nm3_proc'
    ,'r190-2537'
    ,'stereo1_op'
    ,'vidar_Pro'
    ,'xa1'
    ,'xa1_ws'
    ,'xa3'
    ,'xa3_ws'
]


def MHH_AET_list = [
     'angi3d'
    ,'AN_CTAWP96312'
    ,'AN_MEDCOMNT204'
    ,'AWS_STORE160'
    ,'APLIO_2'
    ,'ARTIS109038'
    ,'CT53112'
    ,'DYNACAD_DIAG'
    ,'ELEVA_RF'
    ,'EZ_PIC03'
    ,'GEVOLE10_1'
    ,'GEVOLE10_2'
    ,'H_ICONS'
    ,'HECAM1'
    ,'HECAM8350'
    ,'HERCR_1'
    ,'HESOFTP1'
    ,'HHCCAD_01'
    ,'HHCCAD_02'
    ,'HH_INTERA_01'
    ,'HH_iU22'
    ,'HH_IU22_2'
    ,'HH_IU22_3'
    ,'HHMR_SIEMENS'
    ,'HH_SS'
    ,'HHCMAMMO_DIM'
    ,'HHCMAMMO_SEL'
    ,'HHCT16-1'
    ,'HHEDDR_1_STSCU'
    ,'HMAINCR_1'
    ,'HHOEC_9800_1'
    ,'HHOEC_9800_2'
    ,'HHOEC_9800_3'
    ,'HHOEC_9900_1'
    ,'HHOEC_9900_2'
    ,'LEO12089'
    ,'medison'
    ,'MHH_CTMAIN_PAPER'
    ,'MHH_EDCT_PAPER'  
    ,'MHH_EDXR_PAPER'
    ,'MHH_IR_PAPER'
    ,'MHH_MRMAIN_PAPER'
    ,'MHH_NM_PAPER'
    ,'MHH_US_PAPER'
    ,'MHH_XRMAIN_PAPER'
    ,'MMGR-ROUTER'
    ,'OEM_StoreSCU'
    ,'SAM_TIM_1'
    ,'SAM_TIM_2'
    ,'vf1'
]

def MGSH_AET_list = [
     'MGSH_AET1'
    ,'MGSH_AET2'
    ,'MGSH_AET3'
]

def InstitutionName = "UNKNOWN"

if ( MMMC_AET_list.any { it == callingAET } ) {
    InstitutionName = 'MMMC'
} else if ( MGUH_AET_list.any { it == callingAET } ) {
    InstitutionName = 'MGUH'
} else if ( MSMHC_AET_list.any { it == callingAET } ) {
    InstitutionName = 'MSMHC'
} else if ( MHH_AET_list.any { it == callingAET } ) {
    InstitutionName = 'MHH'
} else if ( MGSH_AET_list.any { it == callingAET } ) {
    InstitutionName = 'MGSH'
}

if ("MMC_".equalsIgnoreCase(callingAET.substring(0,4)))
{
    InstitutionName = 'MMMC'
}

if ("MMMC_".equalsIgnoreCase(callingAET.substring(0,5)))
{
    InstitutionName = 'MMMC'
}


//adjust getScheduledProcedureStepStartDateTime
def hoursToAdd = 4;

if (get("ScheduledProcedureStepSequence/ScheduledProcedureStepStartTime") != null) {

    log.debug( scriptName + "DEBUG: original Scheduled Procedure Step Start Time: {}", get("ScheduledProcedureStepSequence/ScheduledProcedureStepStartTime"));

    DateTimeFormatter formatter = DateTimeFormat.forPattern("HHmmss.SSS");
    DateTime dt = formatter.parseDateTime( get("ScheduledProcedureStepSequence/ScheduledProcedureStepStartTime") );

    set("ScheduledProcedureStepSequence/ScheduledProcedureStepStartTime", formatter.print(dt.plusHours(hoursToAdd)) );

    log.debug( scriptName + "DEBUG: morphed Scheduled Procedure Step Start Time: {}", get("ScheduledProcedureStepSequence/ScheduledProcedureStepStartTime"));

} else {
    log.debug( scriptName + "DEBUG: original Scheduled Procedure Step Start Time is null.." );
}


//get Issuer of Admission ID
log.debug( scriptName + "Issuer of Admission ID: {}", input.get(IssuerOfAdmissionID) )

if (InstitutionName != "UNKNOWN") {
    if ( input.get(IssuerOfAdmissionID) != InstitutionName ) {
        log.info( scriptName + "IssuerOfAdmissionID {} does NOT match the InstitutionName ({}) for CallingAET {}....", input.get(IssuerOfAdmissionID), InstitutionName, callingAET )
        log.info( scriptName + "END" );
        return false
    }
}


// Possible ScheduledProcedureStepStatus values:
//   DISCONTINUED
//   COMPLETED
//   INPROGRESS
//   SC
//   SCHEDULED

def scheduledProcedureStepStatus = get("ScheduledProcedureStepSequence/ScheduledProcedureStepStatus");
//log.debug( scriptName + "DEBUG: ScheduledProcedureStepStatus = {}", scheduledProcedureStepStatus);

if (InstitutionName != "UNKNOWN") {

    if ( scheduledProcedureStepStatus != null && scheduledProcedureStepStatus == 'COMPLETED' ) {
        log.info( scriptName + "ScheduledProcedureStepStatus = {}, will NOT include this order in DMWL C-Find response...", scheduledProcedureStepStatus );
        log.info( scriptName + "END" );
        return false
    }

    if ( scheduledProcedureStepStatus != null && scheduledProcedureStepStatus == 'DISCONTINUED' ) {
        log.info( scriptName + "ScheduledProcedureStepStatus = {}, will NOT include this order in DMWL C-Find response...", scheduledProcedureStepStatus );
        log.info( scriptName + "END" );
        return false
    }

}


log.debug( scriptName + "output: {}\n", output );

log.info( scriptName + "END" );
