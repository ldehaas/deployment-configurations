/* also known as target C-Find morpher */
/* this scripts morphs C-Find requests issued against a target/MoveDestination (usually a local cache) */

def scriptName = "Connect IAF C-Find morpher - ";

log.info(scriptName + 'START')
//log.debug(scriptName + 'C-Find query against local cache: {}', input)

log.info(scriptName + 'About to set NumberOfStudyRelatedSeries tag...')
set(NumberOfStudyRelatedSeries, null)

//log.info(scriptName + 'Going to issue CFind: {}', output)
log.info(scriptName + 'END')
