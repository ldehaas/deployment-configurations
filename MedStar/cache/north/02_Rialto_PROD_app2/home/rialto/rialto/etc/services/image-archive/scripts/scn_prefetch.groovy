import org.joda.time.DateTime
import org.joda.time.format.DateTimeFormat
import java.text.SimpleDateFormat

LOAD('/home/rialto/rialto/etc/services/common/common_medstar_dicom.groovy')
MedStar_DICOM ms = new MedStar_DICOM(log, input)

def scriptName = "SCN Prefetch Morpher - "

log.info(scriptName + "START")

log.debug(scriptName + "processing input:\n{}", input)

def studyDate = input.get(StudyDate)
//log.debug(scriptName + "DEBUG: StudyDate is {}", studyDate)

DateTime studyDateTime;

try {
    studyDateTime = DateTime.parse(studyDate.substring(0,8), DateTimeFormat.forPattern("yyyyMMdd"))
} catch(Exception ex) {
    log.info(scriptName + "failed to parse StudyDate {}...", studyDate)
}
//log.debug(scriptName + "DEBUG: studyDateTime is {}", studyDateTime)

DateTime currentDateTime1 = new DateTime();
//DateTime cutoffDate = new DateTime(2014, 1, 1, 0, 0, 0, 0)
DateTime cutoffDate = currentDateTime1.minusMonths(24);
//log.debug(scriptName + "DEBUG: cutoffDate is {}", cutoffDate)

def proceedWithPrefetch = false

switch (type) {
    case "ARCHIVE":
        proceedWithPrefetch = true
        break
    case "RESTORE":
        proceedWithPrefetch = false
        break
    case "LOGICAL_DELETE":
        proceedWithPrefetch = false
        break
    case "PHYSICAL_DELETE":
        proceedWithPrefetch = false
        break
    case "CATALOG":
        if (studyDateTime != null && studyDateTime.isAfter(cutoffDate)) {
            proceedwithPrefetch = true
        } else {
            proceedWithPrefetch = false
        }
        break
    default:
        proceedWithPrefetch = false
        break
}

if (!proceedWithPrefetch) {
    log.info(scriptName + "archiving status is {}, and studyDate is {}, will NOT prefetch...", type, studyDate)
    log.info(scriptName + "END")
    return false
}

def prefetchSourcesList = [
                               'indexQuery-P005'
                              ,'indexQuery-P0M5'
                              ,'indexQuery-P0RA'
                              ,'indexQuery-P0JG'
                              ,'indexQuery-P0RA'
                              ,'indexQuery-P0RB'
                              ,'indexQuery-P0M5'
                              ,'indexQuery-P0WM'
                              ,'indexQuery-P0T7'
                              ,'indexQuery-P0HS'
                              ,'GSHINSITE02'
                              ,'indexQuery-P0H2'
                              ,'FSHINSITE02'
                              ,'indexQuery-P0TA'
                              ,'indexQuery-P0M2'
                              ,'ALI_SCU'
                              ,'STENTOR_SCU'
                              ,'STENTOR_SEND'
                              ,'ALI_QUERY_SCP'
                          ]

def callingAET = getCallingAETitle()

// check if this study came from LTA (Dell IMG)
if (type != null && "ARCHIVE".equalsIgnoreCase(type)) {
    if (callingAET != null && !"".equalsIgnoreCase(callingAET)) {
        if ( prefetchSourcesList.any { it == callingAET } ) {
            log.info(scriptName + "Calling AE Title {} matches one of the prefetch sources, will NOT issue another prefetch...", getCallingAETitle() )
            log.info(scriptName + "END")
            return false
        }
    } else {
        log.info(scriptName + "Calling AE Title is null or blank, will proceed with prefetch...")
    }
}

log.debug(scriptName + "PatientName is {}", input.get(PatientName))
log.debug(scriptName + "PatientDOB is {}", input.get(PatientBirthDate))
log.debug(scriptName + "PatientID is {}", input.get(PatientID))
log.debug(scriptName + "IssuerOfPatientID is {}", input.get(IssuerOfPatientID))
log.debug(scriptName + "UniversalEntityID is {}", input.get("IssuerOfPatientIDQualifiersSequence/UniversalEntityID"))
log.debug(scriptName + "UniversalEntityType is {}", input.get("IssuerOfPatientIDQualifiersSequence/UniversalEntityIDType"))
log.debug(scriptName + "AccessionNumber is {}", input.get(AccessionNumber))
log.debug(scriptName + "StudyDate is {}", input.get(StudyDate))
log.debug(scriptName + "StudyDescription is {}", input.get(StudyDescription))
log.debug(scriptName + "StudyInstanceUID is {}", input.get(StudyInstanceUID))
log.debug(scriptName + "RequestedProcedureID is {}", input.get(RequestedProcedureID))
log.debug(scriptName + "InstitutionalDepartmentName is {}", input.get(InstitutionalDepartmentName))
log.debug(scriptName + "Calling AE Title is {}", getCallingAETitle())


def institutionalDepartmentName = input.get(InstitutionalDepartmentName);

if (institutionalDepartmentName != null && !"RADIOLOGY".equalsIgnoreCase(institutionalDepartmentName)) {
    log.info(scriptName + "InstitutionalDepartmentName {} does not match RADIOLOGY, will NOT proceed with prefetch...", institutionalDepartmentName )
    log.info(scriptName + "END")
    return false
}

initialize( 'ORM', 'O01', '2.3' )

set('MSH-3', "RIALTO_IA")         //SendingApplication   
set('MSH-4', "MEDSTAR")           //SendingFacility
set('MSH-5', "RIALTO_CONNECT")    //ReceivingApplication   
set('MSH-6', "MEDSTAR")           //ReceivingFacility

def sdf = new SimpleDateFormat("yyyyMMddHHmmss")
def currentDateTime = sdf.format(new Date())
set('MSH-7', currentDateTime)

def issuerOfPatientID = input.get(IssuerOfPatientID)
def namespace = universal_id = universal_id_type = ""
if (issuerOfPatientID != null && issuerOfPatientID.contains("&")) {
    parts = issuerOfPatientID.split("&")
    if (parts.length == 3) {
        domain = com.karos.rtk.common.Domain.parse(issuerOfPatientID)
        namespace = domain.namespaceID
        universal_id = domain.domainUUID
        universal_id_type = domain.domainUUIDtype
    }
} else {
    namespace = issuerOfPatientID
}

if (namespace != null && !ms.ns_to_ipid_map.keySet().contains(namespace)) {
    def hl7_namespace =  ms.ns_to_ipid_map.find { it.value == namespace }?.key
    if (hl7_namespace != null) {
        log.info(scriptName + "Mapping dicom namespace \"{}\" to hl7 namespace \"{}\"", namespace, hl7_namespace)
        namespace = hl7_namespace
    }
}

def patientID = input.get(PatientID)

if (patientID != null) {

    set('PID-2-1', patientID)
    set('PID-2-4-1', namespace)
    set('PID-2-4-2', "MR")
    set('PID-2-4-3', "ISO")

    set('PID-3-1', patientID)
    set('PID-3-4-1', namespace)
    if (!"".equals(universal_id) && !"".equals(universal_id_type)) {
        set('PID-3-4-2', universal_id)
        set('PID-3-4-3', universal_id_type)
    } else {
       log.debug(scriptName + "DEBUG: universal_id or universal_id_type is null...")
    }
} else {
    log.debug(scriptName + "DEBUG: it apperas that PatienID is null, will NOT prefetch...")
}


setPersonName('PID-5', input.get(PatientName))

// add DOB to PID-7-1-1 (0010,0030)
def patientDOB = input.get(PatientBirthDate)
if (patientDOB != null) {
    log.debug(scriptName + "DEBUG: will set PID-7-1-1 (0010,0030) to have PatientBirthDate: {}", patientDOB)
    set('PID-7-1-1', patientDOB)
} else {
    log.debug(scriptName + "DEBUG: PatientBirthDate appears to be null, will NOT populate PID-7-1-1 (0010,0030) ...")
}

log.debug(scriptName + "output is \n{}", output)

log.info(scriptName + "END")

