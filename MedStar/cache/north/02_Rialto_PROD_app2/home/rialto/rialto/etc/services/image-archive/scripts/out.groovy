otherPidsSequence = get(OtherPatientIDsSequence);

ipidQualifierSequence = get(IssuerOfPatientIDQualifiersSequence);

if (ipidQualifierSequence != null) {
  ipidQualifierSequence.each {
    set(OtherPatientIDs, 
      get(PatientID)+"&"+
      get(IssuerOfPatientID)+"&"+
      it.get(UniversalEntityID)+"&"+
      it.get(UniversalEntityIDType));
  }
} else {
  set(OtherPatientIDs, get(PatientID)+"^"+get(IssuerOfPatientID));
}

def calledAET = getCalledAETitle()
if (otherPidsSequence != null && calledAET != null && ("ARCHIVE".equalsIgnoreCase(calledAET) || "ARCHIVE2".equalsIgnoreCase(calledAET) || "ARCHIVE3".equalsIgnoreCase(calledAET) || "ARCHIVE4".equalsIgnoreCase(calledAET) || "PERFAE".equalsIgnoreCase(calledAET) || "PERFAE2".equalsIgnoreCase(calledAET) || "PERFAE3".equalsIgnoreCase(calledAET))) {
  otherPidsSequence.each {
    if ("MEDSTAR&2.16.840.1.114107.1.1.16.2.5&ISO".equalsIgnoreCase(it.get(IssuerOfPatientID))) {
      set(PatientID, it.get(PatientID));
      set(IssuerOfPatientID, "MEDSTAR");
      set("IssuerOfPatientIDQualifiersSequence/UniversalEntityID", "2.16.840.1.114107.1.1.16.2.5", VR.LO)
    } 
  }
}
