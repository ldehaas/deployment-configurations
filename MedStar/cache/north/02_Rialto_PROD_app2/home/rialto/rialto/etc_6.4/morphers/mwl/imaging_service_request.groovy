// Imaging service request morpher for modality worklist system tests

def scriptName = "MWL ISR Morpher - ";

log.info(scriptName + "The order message:\n{}", input)
log.info(scriptName + "The initial Imaging Service Request:\n{}", imagingServiceRequest)

if (imagingServiceRequest.getAccessionNumberUniversalId() == null) {
    imagingServiceRequest.setAccessionNumberNamespaceId("UNKNOWN")
    imagingServiceRequest.setAccessionNumberUniversalId("2.16.124.113638.1.2.1.1")
    imagingServiceRequest.setAccessionNumberUniversalIdType("ISO")
}

if (get("ORC-3") != null) {
   log.debug(scriptName + "about to set Accession Number to {}", get("ORC-3"))
   imagingServiceRequest.setAccessionNumber(get("ORC-3"))
} else {
   log.debug(scriptName + "NOT setting Accession Number as ORC-3 appeards to be NULL")
}

log.debug(scriptName + "about to set PatientID to {}", get("PID-3-1"))

def patientIdentification = imagingServiceRequest.getPatientIdentification()
patientIdentification.setPatientID(get("PID-3-1"))
if (patientIdentification.getPatientIdUniversalId() == null) {
    patientIdentification.setPatientIdNamespaceId(get("PID-3-4-1"))
    patientIdentification.setPatientIdUniversalId(get("PID-3-4-2"))
    patientIdentification.setPatientIdUniversalIdType("ISO")
}
imagingServiceRequest.setPatientIdentification(patientIdentification)

def requestedProcedure = imagingServiceRequest.getRequestedProcedureSequence().get(0)
requestedProcedure.setRequestedProcedureID(imagingServiceRequest.getAccessionNumber())

def scheduledProcedureStep = requestedProcedure.getScheduledProcedureStepSequence().get(0)
scheduledProcedureStep.setScheduledProcedureStepIDString(imagingServiceRequest.getAccessionNumber())

if (scheduledProcedureStep.getScheduledProcedureStepLocation() == null) {
    log.debug(scriptName + "about to set Scheduled Procedure Step Location to UNKNOWN");
    scheduledProcedureStep.setScheduledProcedureStepLocation("UNKNOWN");
}

if (scheduledProcedureStep.getScheduledStationName() == null) {
    log.debug(scriptName + "about to set Scheduled Station Name to UNKNOWN");
    scheduledProcedureStep.setScheduledStationName("UNKNOWN");
}

if (scheduledProcedureStep.getModality() == null) {
    log.debug(scriptName + "about to set Modality to UNKNOWN");
    scheduledProcedureStep.setModality("UNKNOWN");
}

def scheduledProcedureStepStatus = scheduledProcedureStep.getScheduledProcedureStepStatus()
log.debug(scriptName + "ScheduledProcedureStepStatus = {}", scheduledProcedureStepStatus)

def orderControlCodeReason = get("ORC-16")
if (orderControlCodeReason != null) {

    log.debug(scriptName + "ORC-16 segment has orderControlCodeReason = {}", orderControlCodeReason)

    switch (orderControlCodeReason) {
        case 'Ordered':
            log.debug(scriptName + "about to set Scheduled Procedure Step Status to SCHEDULED")
            scheduledProcedureStep.setScheduledProcedureStepStatus('SCHEDULED')
            break
        case 'Exam Started':
            log.debug(scriptName + "about to set Scheduled Procedure Step Status to INPROGRESS")
            scheduledProcedureStep.setScheduledProcedureStepStatus('INPROGRESS')
            break
        case 'Exam Completed':
            log.debug(scriptName + "about to set Scheduled Procedure Step Status to COMPLETED")
            scheduledProcedureStep.setScheduledProcedureStepStatus('COMPLETED')
            break
        case 'Preliminary':
            log.debug(scriptName + "about to set Scheduled Procedure Step Status to COMPLETED (Preliminary Report)")
            scheduledProcedureStep.setScheduledProcedureStepStatus('COMPLETED')
            break
        case 'Final':
            log.debug(scriptName + "about to set Scheduled Procedure Step Status to COMPLETED (Final Report)")
            scheduledProcedureStep.setScheduledProcedureStepStatus('COMPLETED')
            break
        case 'Cancelled':
            log.debug(scriptName + "about to set Scheduled Procedure Step Status to DISCONTINUED")
            scheduledProcedureStep.setScheduledProcedureStepStatus('DISCONTINUED')
            break
        default:
            log.debug(scriptName + "about to set Scheduled Procedure Step Status to SCHEDULED (Default)")
            scheduledProcedureStep.setScheduledProcedureStepStatus('SCHEDULED')
            break
    }

} else {
    log.debug( scriptName + "ORC-16 segment is not set...");
    if ( scheduledProcedureStepStatus != null && "SC".equalsIgnoreCase(scheduledProcedureStepStatus) ) {
        log.debug(scriptName + "about to set Scheduled Procedure Step Status to SCHEDULED (ORC-16 segment is null)")
        scheduledProcedureStep.setScheduledProcedureStepStatus('SCHEDULED')
    }
}


log.debug(scriptName + "about to set ReferringPhysician to {}^{}", get("PV1-8-2"), get("PV1-8-3"))
imagingServiceRequest.setReferringPhysician(get("PV1-8-2") + "^" + get("PV1-8-3"))

log.info(scriptName + "Finished fixing up the Imaging Service Request:\n{}", imagingServiceRequest)
