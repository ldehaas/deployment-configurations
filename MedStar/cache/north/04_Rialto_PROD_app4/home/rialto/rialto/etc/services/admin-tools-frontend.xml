<?xml version="1.0"?>
<config>
    <!-- use the default TLS configuration in etc/variables/tls.xml -->

    <!-- PLUGIN: External Viewer (Download) -->
    <var name="DownloadPlugin-Name" value="External Viewer (Download)" />
    <var name="DownloadPlugin-Description" value="External platform-specific viewer or download to local disk" />

    <service type="navigator.plugin.download" id="navigator.plugin.download">
        <device idref="pix" />
        <device idref="pdq" />
        <device idref="xdsrep"/>
        <device idref="xdsreg"/>

        <config>
            <prop name="CrossAffinityDomainID" value="${System-AffinityDomain}"/>
            <prop name="ForcedMimeTypeFileExtension">
                <prop mimeType="application/vnd.ms-excel" fileExtension="xls"/>
            </prop>
            <prop name="CassandraConfiguration">
                <clusterHosts>${CassandraClusterHosts}</clusterHosts>
                <clusterDatacenterName>${CassandraClusterDatacenterName}</clusterDatacenterName>
                <keyspaceName>${CassandraKeyspacePrefix}navigator</keyspaceName>
                <keyspaceReplicationStrategy>NetworkTopologyStrategy</keyspaceReplicationStrategy>
                <keyspaceReplicationStrategyOptions>${CassandraReplicationOption}</keyspaceReplicationStrategyOptions>
                <consistencyLevel>LOCAL_QUORUM</consistencyLevel>
                <serialConsistencyLevel>LOCAL_SERIAL</serialConsistencyLevel>
                <keyspaceCreateEnabled>true</keyspaceCreateEnabled>
                <dynamicConsistencyLevelEnabled>false</dynamicConsistencyLevelEnabled>
                <schemaUpdateEnabled>false</schemaUpdateEnabled>
            </prop>
            <prop name="web-api-path" value="/plugins/download"/>
            <prop name="UserManagementURL" value="${Usermanagement-AuthenticatedURL}"/>

            <include location="patientidentitydomains.xml"/>
            <include location="admin-tools/cacheStorage.xml"/>
        </config>
    </service>

    <!-- PLUGIN: CDA Viewer   -->
    <var name="CDAPlugin-Name" value="CDA Viewer" />
    <var name="CDAPlugin-Description" value="HL7 CDA (Clinical Document Architecture) Viewer" />
    <service type="navigator.plugin.cda" id="navigator.plugin.cda">
        <device idref="pix" />
        <device idref="pdq" />
        <device idref="xdsrep"/>
        <device idref="xdsreg"/>

        <config>
            <prop name="CrossAffinityDomainID" value="${System-AffinityDomain}"/>
            <prop name="CassandraConfiguration">
                <clusterHosts>${CassandraClusterHosts}</clusterHosts>
                <clusterDatacenterName>${CassandraClusterDatacenterName}</clusterDatacenterName>
                <keyspaceName>${CassandraKeyspacePrefix}navigator</keyspaceName>
                <keyspaceReplicationStrategy>NetworkTopologyStrategy</keyspaceReplicationStrategy>
                <keyspaceReplicationStrategyOptions>${CassandraReplicationOption}</keyspaceReplicationStrategyOptions>
                <consistencyLevel>LOCAL_QUORUM</consistencyLevel>
                <serialConsistencyLevel>LOCAL_SERIAL</serialConsistencyLevel>
                <keyspaceCreateEnabled>true</keyspaceCreateEnabled>
                <dynamicConsistencyLevelEnabled>false</dynamicConsistencyLevelEnabled>
                <schemaUpdateEnabled>false</schemaUpdateEnabled>
            </prop>
            <prop name="web-api-path" value="/plugins/cda"/>
            <prop name="DefaultStylesheet" value="${rialto.rootdir}/etc/navigator/plugins/cda/default.xsl"/>
            <prop name="UserManagementURL" value="${Usermanagement-AuthenticatedURL}"/>

            <include location="patientidentitydomains.xml"/>
            <include location="admin-tools/cacheStorage.xml"/>
        </config>
    </service>


    <service type="rialto.ui" id="rialto.ui">
        <device idref="pix" />
        <device idref="pdq" />

        <server idref="navigator-http" name="web-ui">
            <url>${Navigator-GUIBasePath}</url>
        </server>

        <server idref="authenticated-http" name="web-api">
            <url>/public/*</url>
        </server>

        <config>
            <prop name="RialtoAsACache" value="true"/>
            <prop name="MaxEventDisplay" value="5000"/>
            <prop name="ApiURL" value="${Navigator-HostProtocol}://${Navigator-Host}:${Navigator-APIPort}${Navigator-RootPath}/api${Navigator-APIBasePath}"/>
            <prop name="ArrURL" value="${Navigator-HostProtocol}://${Navigator-Host}:${ARR-GUIPort}"/>
            <prop name="UserManagementURL" value="${Usermanagement-URL}"/>
            <prop name="PublicURL" value="${Navigator-HostProtocol}://${Navigator-Host}:${Navigator-APIPort}/public"/>
            <prop name="StudyManagementURL" value="http://${HostIP}:8080${IA-StudyManagement-Path}"/>
            <prop name="WadoURL" value="http://${HostIP}:8080/vault/wado" />
            <prop name="DataflowApiURL" value="http://${HostIP}:13337/monitoring" />
            <prop name="DeviceRegistryURL" value="http://${HostIP}:8080/rialto/deviceregistry" />
            <prop name="ModalityWorklistURL" value="http://${HostIP}:8080/mwl" />
            <prop name="MintApiURL" value="http://${HostIP}:8080/vault/mint" />
            <prop name="DicomQcToolsURL" value="http://${HostIP}:8080/vault/qc" />
            <prop name="IlmURL" value="http://${HostIP}:8080/vault/ilm" />
            <prop name="GlobalInactivitySessionTimeout" value="${IdleUserSessionTimeout}" />
            <!-- KHC9958 double the default size -->
            <prop name="StudyMetadataSizeLimit" value="41943040" />
            <!-- workflow configuration -->
            <prop name="WorkflowApiURL" value="http://${HostIP}:2556/workflows" />
            <prop name="ElasticsearchRestApiURL" value="http://172.16.203.65:9200" />

            <!-- NEWRELIC CONFIGURATION -->
            <prop name="NewRelicInsightsQueryKey" value="${NewRelic-InsightsQueryKey}" />
            <prop name="NewRelicInsightsQueryURL" value="https://insights-api.newrelic.com/v1/accounts/${NewRelic-AccountID}/query" />

            <!-- prop name="RialtoImageArchiveAETitle" value="${IA-AETitle}" / -->
            <prop name="RialtoImageArchiveAETitle" value="RIALTO_CACHE" />
            <prop name="ConnectQueryRetrieveAETitle" value="RIALTO_CONNECT" />

            <prop name="SupportedThemes">
                rialto-light
                rialto-dark
            </prop>
            <prop name="DefaultTheme" value="${Navigator-DefaultTheme}"/>

            <prop name="SupportedLocales">
                en
                fr
            </prop>
            <prop name="DefaultLocale" value="${Navigator-DefaultLocale}"/>

            <prop name="BreakTheGlassOptions">
                <BreakTheGlassOption>Emergency access required for patient care.</BreakTheGlassOption>
            </prop>

            <!--Preloading and caching of SOP thumbnails introduced in 6.6-->
            <ImageCache>true</ImageCache>

            <prop name="Plugins">
                <plugin>
                    <name>${CDAPlugin-Name}</name>
                    <description>${CDAPlugin-Description}</description>
                    <mimeTypes>text/xml</mimeTypes>
                    <url>${Navigator-HostProtocol}://${Navigator-Host}:${Navigator-APIPort}${Navigator-RootPath}/api/plugins/cda</url>
                    <embedded>true</embedded>
                </plugin>

                <plugin>
                    <name>${DownloadPlugin-Name}</name>
                    <description>${DownloadPlugin-Description}</description>
                    <mimeTypes>*</mimeTypes>
                    <url>${Navigator-HostProtocol}://${Navigator-Host}:${Navigator-APIPort}${Navigator-RootPath}/api/plugins/download</url>
                    <embedded>true</embedded>
                </plugin>
            </prop>

            <include location="patientidentitydomains.xml"/>

            <!-- Configure eventCodeSchemes to support searching for documents by eventCode -->
            <!--prop name="EventCodeSchemes">
                <scheme schemeID="1.2.3" schemeName="Karos Health demo eventCodes" />
            </prop-->

        </config>
    </service>
</config>

