LOAD('/home/rialto/rialto/etc/services/common/dicom_mappings.groovy')

class DICOM_Common {

    def log = null
    def sop = null
    def mapper = null

    def DICOM_Common(log, sop) {
        this.log = log
        this.sop = sop
        this.mapper = new DICOM_Mappings()
    }

    def getPrivateCreatorMask(dcmSOP, group, creator) {
        def element
        def pcElement
        def existingCreator

        for (element = 0x0010; element <= 0x00ff; element++) {
            pcElement = (group << 16) | element
            existingCreator = dcmSOP.get(pcElement)
            if (existingCreator == null || existingCreator.equals(creator)) {
                dcmSOP.set(pcElement, creator, VR.LO)
                return element << 8
            }
        }

        return null
    }

    /*
     * Set a private tag value.  Element to set should have most significant byte == 00.
     */
    def setPrivate(dcmSOP, groupToSet, elementToSet, privateCreator, value, vr) {
        def mask

        mask = getPrivateCreatorMask(dcmSOP, groupToSet, privateCreator)
        if (mask == null) {
            log.warn("All private creator slots for group {} are already taken", String.format("%04x", groupToSet))
            return
        }

        dcmSOP.set((groupToSet << 16) | mask | elementToSet, value, vr)
    }

    //Function that will process the DICOM value specified by "tag" in pm
    //and evaluate it according to the corresponding RegEx command specified in processingExpression in pm
    //to find the final value that is to be used as indicated by resultString in pm.
    //If no match is found in the array list, null will be returned from this function

    def evalSOP(pm) {
        def sourceValue
        def foundValue
        def sequenceItems

        //iterate over the pattern matching array list
        for (def i = 0; i < pm.size(); i++) {
            if ((sop.get(Modality) == pm[i].modality) || (pm[i].modality == null)) {
                if (pm[i].sequenceTag == 0) { //0 is the root of the object.
                    log.info("looking in the root of the object for tag " + pm[i].tag)
                    sourceValue = sop.get(pm[i].tag)
                    foundValue = evaluateValue(sourceValue, pm[i].processingExpression, pm[i].resultString)
                    if (foundValue != null) {
                        log.info("found a match (" + foundValue + "), getting out.")
                        return foundValue
                    } 
                }
                else {
                    log.info("the value we are to look for is within a sequence. cracking opening the sequence...")
                    sequenceItems = sop.get(pm[i].sequenceTag)
                    if (sequenceItems != null) {
                        log.info("got all the items within the sequence. starting to iterate...")
                        for (def k = 0; k < sequenceItems.size(); k++) {
                            foundValue = evaluateValue(sequenceItems[k].get(pm[i].tag), pm[i].processingExpression, pm[i].resultString)
                            if (foundValue != null) {
                                log.info("found a match (" + foundValue + "), getting out.")
                                return foundValue
                            }
                            else {
                                log.info("not a match, moving to the next item within the sequence if there is one.")
                            } 
                        }
                    } else {
                        log.info("did not get any items within the sequence...")
                    }
                }
            }
            else {
                //log.info("mapping doesn't apply for this modality...skipping.")
            }
        }
    }

    def evaluateValue(valueIn, expressionIn, toReturn) {
        def f

        log.info("looking at '" + valueIn + "' using " + expressionIn)

        //perform RegEx evaluation on the sourceValue
        f = valueIn =~ expressionIn

        //ensure it returned a value Matcher data type
        //assert f instanceof java.util.regex.Matcher
        if (f) {
            if (f.matches()) {
                log.info("match found, returning '" + toReturn +"'.")
                return toReturn
            }
        }
    }

    def processSOPSeriesDescription() {
        def seriesDescription
        def bp
        def l
        def vp
        def built

        seriesDescription = sop.get(SeriesDescription)

       if ((seriesDescription == "") || (seriesDescription == null)) {
            log.info("seriesDescription is empty...populating with body part and laterality values in object.")

            bp = sop.get(BodyPartExamined)
            vp = sop.get(ViewPosition)
            l = sop.get(Laterality)

            built = ""

            if (bp != null) {
                built += bp
            }

            if (vp != null) {
                built += " " + vp
            }

            if (l != null) {
                built += " " + l
            }

            built = built.trim()

            sop.set(SeriesDescription, built)
        }
    }

    def processSOPStudyDescription() {
        def studyDescription
        def newStudyDescription

        def newStudyDescriptionSources = ["PerformedProcedureStepDescription", 
                                          "RequestedProcedureDescription",
                                          "ProcedureCodeSequence/CodeMeaning"]

        studyDescription = sop.get(StudyDescription)

        if ((studyDescription == "") || (studyDescription == null)) {
            log.info("studyDescription is empty, so we would like to fix this up")

            for (def j = 0; j < newStudyDescriptionSources.size(); j++) {
                newStudyDescription = sop.get(newStudyDescriptionSources[j])

                if ((newStudyDescription != "") && (newStudyDescription != null)) {
                    log.info("updating empty study description with '" + newStudyDescription + "' which was found at " + newStudyDescriptionSources[j])
                    sop.set(StudyDescription, newStudyDescription)
                    return
                }
               else {
                    log.info("The value at '" + newStudyDescriptionSources[j] + "' is also empty, so we will move onto the next source location for a study desc.")
                }
            }
        }
        else {
            if (studyDescription.length() >= 3) {
                if (studyDescription[0..2].toUpperCase() == "XRY") {
                    log.info("studyDescription is one of the useless ones that starts with XRY.")

                    for (def j = 0; j < newStudyDescriptionSources.size(); j++) {
                        log.info("Checking to see if we have a value at '" + newStudyDescriptionSources[j] + "'...")
                        newStudyDescription = sop.get(newStudyDescriptionSources[j])

                        if ((newStudyDescription != "") && (newStudyDescription != null)) {
                            log.info("we found a non-blank value present ('" + newStudyDescription + "'), so we will back up original studyDescription to KAROS PRESERVED INFO 1.0 privates.")
                            setPrivate(sop, 0x0035, 0x0060, "KAROS PRESERVED INFO 1.0", studyDescription, VR.LO)

                            log.info("setting '" + newStudyDescription + "' into study description...")
                            sop.set(StudyDescription, newStudyDescription)
                            return
                        }
                        else {
                            log.info("value at '" + newStudyDescriptionSources[j] + "' is also empty, so we will move onto the next source location for a study desc.")
                        }
                    }
                }
            }
        }
    }

    def processSOPStudyTime() {
    
        log.info("starting to fix study time for this sop...")

        if ((sop.get(StudyTime) == null) || (sop.get(StudyTime) == "")) {

            if ((sop.get(SeriesTime) != null) && (sop.get(SeriesTime) != "")) {
                log.info("setting StudyTime to be equal to SeriesTime.")
                sop.set(StudyTime, sop.get(SeriesTime))
            }

            else if ((sop.get(AcquisitionTime) != null) && (sop.get(AcquisitionTime) != "")) {
                log.info("setting StudyTime to be equal to AcquisitionTime.")
                sop.set(StudyTime, sop.get(AcquisitionTime))
            }

            else if ((sop.get(ContentTime) != null) && (sop.get(ContentTime) != "")) {
                log.info("setting StudyTime to be equal to ContentTime.")
                sop.set(StudyTime, sop.get(ContentTime))
            }

            else if ((sop.get(InstanceCreationTime) != null) && (sop.get(InstanceCreationTime) != "")) {
                log.info("setting StudyTime to be equal to InstanceCreationTime.")
                sop.set(StudyTime, sop.get(InstanceCreationTime))
            }

            else {
                log.error("No suitable time stamps found in this object. Unable to set StudyTime to anything reasonable.")
            }
        }

        else {
            log.info("skipping SOP since it already has a study time filled in.")
        }
    }

    def processSOPBodyPart() {
        def foundBodyPart

        log.info("starting to fix body part for this sop with modality '" + sop.get(Modality) + "'...")

        //
        // if the body part is missing or it's empty
        // OR
        // BodyPart is either LOW_EXM or UP_EXM and is modality CR
        //
        if (((sop.get(BodyPartExamined) == null) || (sop.get(BodyPartExamined) == "")) || (((sop.get(BodyPartExamined) == "UP_EXM") || (sop.get(BodyPartExamined) == "LOW_EXM")) && (sop.get(Modality) == "CR"))) {
            foundBodyPart = evalSOP(mapper.bodyParts)

            if (foundBodyPart != null) {
                log.info("setting body part '" + foundBodyPart + "' into object.")
                sop.set(BodyPartExamined, foundBodyPart)
            }
            else {
                log.info("not setting body part into object since no suitable mappings for this object could be found.")
            }
        }
        else {
            log.info("skipping look up of body part since one already exists in the object that isn't blank and isn't UP_EXM or LOW_EXM.")
        }
    }

    def processSOPViewPosition() {
        def foundViewPosition

        log.info("starting to fix view position for this sop with modality '" + sop.get(Modality) + "'...")
        if ((sop.get(ViewPosition) == null) || (sop.get(ViewPosition) == "")) {
            foundViewPosition = evalSOP(mapper.viewPositions)
            if (foundViewPosition != null) {
                log.info("setting view position '" + foundViewPosition + "' into object.")
                sop.set(ViewPosition, foundViewPosition)
            }
            else {
                log.info("not setting view position into object since no suitable mappings for this object could be found.")
            }
        }
        else {
            log.info("skipping look up of view position since one already exists in the object.")
        }
    }

    def processSOPLaterality() {
        def foundLaterality

        log.info("starting to fix laterality for this sop with modality '" + sop.get(Modality) + "'...")
        if ((sop.get(Laterality) == null) || (sop.get(Laterality) == "")) {
            foundLaterality = evalSOP(mapper.lateralities)

            if (foundLaterality != null) {
                log.info("setting laterality '" + foundLaterality + "' into object.")
                sop.set(Laterality, foundLaterality)
            }
            else {
                log.info("not setting laterality into object since no suitable mappings for this object could be found.")
            }
        }
        else {
            log.info("skipping look up of laterality since one already exists in the object.")
        }
    }
}

