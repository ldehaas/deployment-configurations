/**
 *    ILM policy for MedStar
 */
import org.joda.time.LocalDateTime;
import org.joda.time.DateTime;


log.debug("ILM policies START - Executing against study with StudyInstanceUID {}", study.getStudyInstanceUID());

if (timeline.wasFullyForwardedWithStorageCommitTo("indexStore-P0T7")) {

  oneYearAgo = new DateTime().minusYears(1);
  if (study.isBefore(oneYearAgo)) {
    log.info("ILM policies - Study with StudyInstanceUID {} can be marked as a candidate for purging since the study date is set to be {}, and we will mark any studies being ready for purging that are older than one year from today (ie, {})", study.getStudyInstanceUID(), study.get("StudyDate"), oneYearAgo);
    ops.flagAsPurgeCandidate();
  }

  sixMonthsAgo = new DateTime().minusMonths(6);
  if (sixMonthsAgo.isAfter(study.getLastUpdateTime())) {
    log.info("ILM policies - Study with StudyInstanceUID {} can be marked as a candidate for purging since the study was last used {}, and we will mark any studies being ready for purging that haven't been updated in the previous 6 months from today (ie, {})", study.getStudyInstanceUID(), study.getLastUpdateTime(), sixMonthsAgo);
    ops.flagAsPurgeCandidate();
  }

} else {

  if  (!timeline.isPrefetched()) {
    log.info("ILM policies - Schedule forwarding study with StudyInstanceUID {} to indexStore-P0T7", study.getStudyInstanceUID());
    ops.scheduleStudyForwardWithStorageCommit("indexStore-P0T7");
  }

}

log.debug("ILM policies END - Done looking at study with StudyInstanceUID {}", study.getStudyInstanceUID());
