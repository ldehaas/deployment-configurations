log.info("Begin instanceAvailabilityFilter morpher")

log.info("Source CFIND result: {}", source)
log.info("Target CFIND result: {}", target)

source_num_series = source.get("NumberOfStudyRelatedSeries")
target_num_series = target.get("NumberOfStudyRelatedSeries")

log.info("Number of series in source: {}", source_num_series)
log.info("Number of series in target: {}", target_num_series)

if (source_num_series <= target_num_series) {
   return false
}

