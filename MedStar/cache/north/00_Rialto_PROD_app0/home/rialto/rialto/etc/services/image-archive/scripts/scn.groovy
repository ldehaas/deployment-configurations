import org.joda.time.DateTime
import org.joda.time.format.DateTimeFormat
import java.text.SimpleDateFormat

def scriptName = "IA SCN Morpher - ";

log.info(scriptName + "START")

LOAD('/home/rialto/rialto/etc/services/common/common_medstar_dicom.groovy')
MedStar_DICOM ms = new MedStar_DICOM(log, input)
sdf = new SimpleDateFormat("yyyyMMddHHmmss")

log.debug(scriptName + "Calling AE Title is {} for input:\n{}", getCallingAETitle(), input);

def idn = input.get("InstitutionalDepartmentName")
if (idn == null) {
    idn = "OTHER";
}

// Determine what type of study it is
def rad_study = false;
if ("RADIOLOGY".equalsIgnoreCase(idn)) {
    rad_study = true;
}

if (!rad_study) {
    log.debug(scriptName + "InstitutionalDepartmentName is {}, will NOT sent SCN message out...", idn);
    return false
} else {
    log.debug(scriptName + "InstitutionalDepartmentName is {}, will sent SCN message out...", idn);
}

initialize( 'ORM', 'O01', '2.3' );
output.getMessage().addNonstandardSegment('IPC')

set('MSH-7', sdf.format(new Date()))
setPersonName('PID-5', input.get(PatientName));
/* set('PID-3-1', input.get(PatientID)); */

def issuerOfPatientID = input.get(IssuerOfPatientID);
def namespace = universal_id = universal_id_type = "";
if (issuerOfPatientID != null && issuerOfPatientID.contains("&")) {
    parts = issuerOfPatientID.split("&");
    if (parts.length == 3) {
        domain = com.karos.rtk.common.Domain.parse(issuerOfPatientID)
        namespace = domain.namespaceID
        universal_id = domain.domainUUID
        universal_id_type = domain.domainUUIDtype
    }
} else {
    namespace = issuerOfPatientID
}

if (namespace != null && !ms.ns_to_ipid_map.keySet().contains(namespace)) {
    def hl7_namespace =  ms.ns_to_ipid_map.find { it.value == namespace }?.key
    if (hl7_namespace != null) {
        log.info(scriptName + "Mapping dicom namespace \"{}\" to hl7 namespace \"{}\"", namespace, hl7_namespace);
        namespace = hl7_namespace
    }
}

if (namespace != null && namespace == "SHAH")
{
    // check if SHAH StudyDate is within last 7 days
    def daysAgo = new DateTime().minusDays(7);

    //{ 0x0008, 0x0020, 'DA', "Study Date" },
    def studyDate = input.get(0x00080020);
    log.debug(scriptName + "DEBUG: StudyDate: {}", studyDate);

    //{ 0x0008, 0x0030, 'TM', "Study Time" },
    def studyTime = input.get(0x00080030);
    log.debug(scriptName + "DEBUG: StudyTime: {}", studyTime);
    
    def studyDateTime = new DateTime();
    if (studyTime.contains(":")) {
        studyDateTime = DateTime.parse(studyDate + " " + studyTime.substring(0,8), DateTimeFormat.forPattern("yyyyMMdd HH:mm:ss"))
    } else {
        studyDateTime = DateTime.parse(studyDate + " " + studyTime.substring(0,6), DateTimeFormat.forPattern("yyyyMMdd HHmmss"))
    }

    if (studyDateTime.isBefore(daysAgo)) {
        log.info(scriptName + "StudyDate is more than 7 days for SHAH study, will NOT send SCN message.");
        return false;
    } else {
        log.info(scriptName + "SHAH StudyDateTime = {} , it is within last 7 days, will proceed with morphing SCN message.", studyDateTime);
    }

    // add DOB to PID-7-1-1 (0010,0030)
    def patientDOB = input.get(PatientBirthDate)
    if (patientDOB != null) {
        log.debug(scriptName + "DEBUG: will set PID-7-1-1 (0010,0030) to have PatientBirthDate: {}", patientDOB);
        set('PID-7-1-1', patientDOB)
    } else {
        log.debug(scriptName + "DEBUG: PatientBirthDate appears to be null, will NOT populate PID-7-1-1 (0010,0030) ...");
    }
}

def patientID = input.get(PatientID);
if (namespace != null && namespace == "MUMH") {
    if (patientID != null && patientID != patientID.padLeft(9,'0')) { 
        log.info(scriptName + "padding PatientID with leading zeros for namespace \"{}\"", namespace);
        patientID = input.get(PatientID).padLeft(9,'0');
    }
}

set('PID-3-1', patientID);

set('PID-3-4-1', namespace);
if (!"".equals(universal_id) && !"".equals(universal_id_type)) {
    set('PID-3-4-2', universal_id);
    set('PID-3-4-3', universal_id_type);
}
if (type != null && (type.toLowerCase().contains("delete") || type.toLowerCase().contains("cataloged"))) {
    set('ORC-1', 'DC');
} else {
    set('ORC-1', 'NW');
}
set('OBR-1', '1')
set('OBR-3-1', input.get(AccessionNumber))
set('OBR-3-2', namespace)

if (type != null) {
    if ( input.get("IssuerOfAccessionNumberSequence/LocalNamespaceEntityID") == "MAS" ) {
       set('OBR-3-2-1', 'MAS')
       log.info(scriptName + "Setting Issuer of Accession Number to MAS based on IssuerOfAccessionNumberSequence/LocalNamespaceEntityID tag");
    } else if ( input.get(tag(0x0035, 0x1040)) == 'MedStar Radiology Network Bel Air' ) {
       set('OBR-3-2-1', 'MAS')
       log.info(scriptName + "Setting Issuer of Accession Number to MAS based on 0035,1040 tag");
    } else {
       log.info(scriptName + "Not setting OBR-3-2-1 segment to MAS");
       log.debug(scriptName + "DICOM tag(0008,0051) = {}", input.get(tag(0x0008, 0x0051)) );
       log.debug(scriptName + "DICOM tag(0035,1040) = {}", input.get(tag(0x0035, 0x1040)) );
    }
    if ( input.get("IssuerOfAccessionNumberSequence/LocalNamespaceEntityID") == "MAS2" ) {
        set('OBR-3-2-1', 'MAS2')
        log.info(scriptName + "Setting Issuer of Accession Number to MAS2 based on IssuerOfAccessionNumberSequence/LocalNamespaceEntityID tag");
        } 
        else {
               log.info(scriptName + "Not setting OBR-3-2-1 segment to MAS2");
               log.debug(scriptName + "DICOM tag(0008,0051) = {}", input.get(tag(0x0008, 0x0051)) );
               log.debug(scriptName + "DICOM tag(0035,1040) = {}", input.get(tag(0x0035, 0x1040)) );
             }
} else {
    log.info(scriptName + "Not setting Issuer of Accession Number - type is null!!!");
}

set('OBR-4-1', input.get(RequestedProcedureID))
def study_description = input.get(StudyDescription)
if (study_description != null)  {
    study_description = study_description.replaceAll("\\^.*", "")
    set('OBR-4-2', study_description)
}
set('OBR-7', input.get(StudyDate))
set('OBR-25', 'I')
set('OBX-1', '1')
set('OBX-2', 'TX')
set('OBX-3-1-2', 'GDT')
set('OBX-5', 'Y')
set('IPC-3', input.get(StudyInstanceUID))

if (namespace != null && namespace == "SHAH")
{
    def study_desc2 = input.get(StudyDescription)
    if (study_desc2 != null) {
        def modality_type = "";
        if ( study_desc2.indexOf(" ") != null && study_desc2.indexOf(" ") > 0 ) {
            modality_type = study_desc2.substring(0, study_desc2.indexOf(" "));
        } else {
            modality_type = study_desc2;
        }
        log.info( scriptName + "setting IPC-5 to Modality Type = {}", modality_type);
        set('IPC-5', modality_type )
    }
}

log.info(scriptName + "END")
