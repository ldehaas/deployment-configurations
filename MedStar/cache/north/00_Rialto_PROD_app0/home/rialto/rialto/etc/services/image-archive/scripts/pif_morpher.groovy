def scriptName="IA PIF Morpher - ";

log.debug(scriptName + "Morphing PIF")
log.debug(scriptName + "input: {}", input)

// The key is the domain and the value is a list holding the values for [/.MSH-3, /.MSH-4, /.MSH-5, /.MSH-6]
def sendingAndRecievingInformation = [
    "MEDSTAR":["PIF_MORPHER", "MEDSTAR", "RIALTO_EMPI", "MEDSTAR"],
    "HELIX":["PIF_MORPHER", "MEDSTAR", "RIALTO_EMPI", "MEDSTAR"],
    "FSH":["PIF_MORPHER", "MFSMC", "RIALTO_EMPI", "MEDSTAR"],
    "GSH":["PIF_MORPHER", "MGSH", "RIALTO_EMPI", "MEDSTAR"],
    "HHC":["PIF_MORPHER", "MHH", "RIALTO_EMPI", "MEDSTAR"],
    "UMH":["PIF_MORPHER", "MUMH", "RIALTO_EMPI", "MEDSTAR"],
    "GUH":["PIF_MORPHER", "MGUH", "RIALTO_EMPI", "MEDSTAR"],
    "WB4":["PIF_MORPHER", "MWHC", "RIALTO_EMPI", "MEDSTAR"],
    "6B4":["PIF_MORPHER", "MNRH", "RIALTO_EMPI", "MEDSTAR"],
    "MGI":["PIF_MORPHER", "MMMC", "RIALTO_EMPI", "MEDSTAR"],
    "MAS":["PIF_MORPHER", "MAS", "RIALTO_EMPI", "MEDSTAR"],
    "MAS2":["PIF_MORPHER", "MAS2", "RIALTO_EMPI", "MEDSTAR"],
    "MPP":["PIF_MORPHER", "MPP", "RIALTO_EMPI", "MEDSTAR"],
    "MSC":["PIF_MORPHER", "MSC", "RIALTO_EMPI", "MEDSTAR"],
    "SMH":["PIF_MORPHER", "MSMH", "RIALTO_EMPI", "MEDSTAR"],
    "SMHC":["PIF_MORPHER", "MSMHC", "RIALTO_EMPI", "MEDSTAR"],
    "SHAH":["PIF_MORPHER", "SHAH", "RIALTO_EMPI", "MEDSTAR"]
    ]

LOAD('/home/rialto/rialto/etc/services/common/common_medstar_dicom.groovy')
MedStar_DICOM ms = new MedStar_DICOM(log, input)

def issuerOfPatientID = input.get(IssuerOfPatientID);
log.info(scriptName + "IssuerOfPatientID is " + issuerOfPatientID)

def dicomNamespace = universal_id = universal_id_type = "";
if (issuerOfPatientID != null) {
    if (issuerOfPatientID.contains("&")) {
        parts = issuerOfPatientID.split("&");
        if (parts.length == 3) {
            domain = com.karos.rtk.common.Domain.parse(issuerOfPatientID)
            dicomNamespace = domain.namespaceID
            universal_id = domain.domainUUID
            universal_id_type = domain.domainUUIDtype
        }
    } else if (issuerOfPatientID == "UNKNOWN") {
        //try to exctract IssuerOfAccessionNumber
        log.debug(scriptName + "DEBUG: DICOM 0x00080051 = {}", input.get(0x00080051) );
        log.debug(scriptName + "DEBUG: DICOM IssuerOfAccessionNumberSequence = {}", input.get("IssuerOfAccessionNumberSequence") );

        def issuerOfAccessionNumber = input.get(0x00080051)

        if (issuerOfAccessionNumber != null) {
            dicomNamespace = issuerOfAccessionNumber
        }
    } else {
        dicomNamespace = issuerOfPatientID
    }
} else {
    //try to exctract IssuerOfAccessionNumber
    log.debug(scriptName + "DEBUG: DICOM 0x00080051 = {}", input.get(0x00080051) );
    log.debug(scriptName + "DEBUG: DICOM IssuerOfAccessionNumberSequence = {}", input.get("IssuerOfAccessionNumberSequence") );

    def issuerOfAccessionNumber = input.get(0x00080051)

    if (issuerOfAccessionNumber != null) {
        dicomNamespace = issuerOfAccessionNumber
    }
}

log.info(scriptName + "DICOM Namespace is " + dicomNamespace)

def hl7Namespace = dicomNamespace

if (dicomNamespace != null && !ms.ns_to_ipid_map.keySet().contains(dicomNamespace)) {
    hl7Namespace =  ms.ns_to_ipid_map.find { it.value == dicomNamespace }?.key
    if (hl7Namespace != null) {
        log.info(scriptName + "Mapping dicom namespace \"{}\" to hl7 namespace \"{}\"", dicomNamespace, hl7Namespace);
    }
}

log.info(scriptName + "HL7 Namespace is " + hl7Namespace)

def info = sendingAndRecievingInformation.get(dicomNamespace);

if (info == null) {
    log.warn(scriptName + "PIF Morphing script does not know about the given IssuerOfPatientID. Will not update MSH-3,4,5,6 based on IssuerOfPatientID.")
} else {
    set("/.MSH-3", info.get(0))
    set("/.MSH-4", info.get(1))
    set("/.MSH-5", info.get(2))
    set("/.MSH-6", info.get(3))
}

/**
 * Copies up to three components of a name from the SR into the hl7 message.
 * DICOM person name elements are supposed to be of the form
 * family^given^middle^prefix^suffix.  HL7 names are sometimes of the form
 * family^given^middle^suffix^prefix but some components are actually 
 * id^family^given^...
 * 
 * In the case where the id is there, pass startingComponent=2 to offset where
 * we put the name.  However, in our sample SRs, the values of the person names
 * actually include the id (they are in DICOM format instead of HL7).  In this
 * case, just leave the default startingComponent.
 * 
 * @param dicomTag place to get name from in SR
 * @param hl7prefix place to put name in hl7
 * @param startingComponent if name doesn't line up at beginning of 
 * @return
 */
def setName(dicomTag, hl7prefix, startingComponent=1) {
    nameParts = split(dicomTag)
    if (nameParts == null) {
        return
    }

    // loop far enough to get the id if present, plus the family and given names
    int maxPartsToCopy = 3
    
    for (int i = 0; i < maxPartsToCopy && i < nameParts.size(); i++) {
        set(hl7prefix + "-" + (startingComponent++), nameParts[i])
    }
}

//set( "PID-2-1", input.get(0x00101000) )
set( "PID-2-1", "MSH-TEMP-EMPI-" + get(PatientID) )
set( "PID-2-4-1", "MEDSTAR" )
set( "PID-2-4-2", "EE" )
set( "PID-2-4-3", "ISO" )

//set( "PID-3(0)-1", input.get(0x00101000) )
set( "PID-3(0)-1", "MSH-TEMP-EMPI-" + get(PatientID) )
set( "PID-3(0)-4-1", "MEDSTAR" )
set( "PID-3(0)-4-2", "EE" )
set( "PID-3(0)-4-3", "ISO" )

set( "PID-3(1)-1",  get(PatientID) )
set( "PID-3(1)-4-1", hl7Namespace )
set( "PID-3(1)-4-2", "MR" )
set( "PID-3(1)-4-3", "ISO" )

// Set the Patient Name with up to 3 components from DICOM
setName(PatientName, "PID-5")
setName(PatientMotherBirthName, "PID-6")

set("PID-7", get(PatientBirthDate))
set("PID-8", get(PatientSex))
set("PID-11", get(PatientAddress))
set("PID-13", get(PatientTelephoneNumbers))

// print generated HL7 message
log.debug(scriptName + "generated the following HL7 message: {}", output);

if (dicomNamespace != null && dicomNamespace == "SHAH")
{
    log.info(scriptName + "IssuerofPatientID = {}, will proceed with updating EMPI", dicomNamespace);
} else if ( dicomNamespace != null && dicomNamespace == "MPP" ){
    log.info(scriptName + "IssuerofPatientID = {}, will proceed with updating EMPI", dicomNamespace);
} else {
    log.info(scriptName + "IssuerOfPatienID is not SHAH or MPP, will skip updating EMPI...")
    // zero out SendingFacitily and SendingApplication
    set("/.MSH-3", "")
    set("/.MSH-4", "")
    return false
}

log.debug(scriptName + "End Morphing PIF")
