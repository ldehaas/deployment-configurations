/* ilm_eligibility.groovy - MedStar North
 *
 * WARNING: Updating this script will update immediately without a Rialto restart. Use caution when updating a script on a live system.
 *
 * Documentation: https://karoshealth.atlassian.net/wiki/display/RIALTO64/EligibilityScript+script
 *
 * Script variables:
 *     localPid        - local PatientID of a given study.
 *     accessionNumber - AccessionNumber of a given study.
 *
 */

def scriptName = "ILM Eligibility morpher (wide) - ";

// log.info( scriptName + "START" )

/*
=== North ===
     "2.16.840.1.114107.1.1.16.2.5"   - MEDSTAR
    ,"2.16.840.1.114107.1.1.16.2.7"   - FSH
    ,"2.16.840.1.114107.1.1.16.2.2"   - GSH
    ,"2.16.840.1.114107.1.1.16.2.4"   - HHC
    ,"2.16.840.1.114107.1.1.16.2.8"   - UMH

=== South ===
     "2.16.124.113638.1.2.1.1"        - UNKNOWN
    ,"2.16.840.1.114107.1.1.16.2.5"   - MEDSTAR
    ,"2.16.840.1.114107.1.1.16.2.3"   - WHC
    ,"2.16.840.1.114107.1.1.16.2.6"   - GUH
    ,"2.16.840.1.114107.1.1.16.2.9"   - NRH
    ,"2.16.840.1.114107.1.1.16.2.10"  - MMC
    ,"2.16.840.1.114107.1.1.16.2.11"  - SMH
    ,"2.16.840.1.114107.1.1.16.2.12"  - SMHC
    ,"2.16.840.1.114107.1.1.16.2.13"  - MAS
    ,"2.16.840.1.114107.1.1.16.2.14"  - MPP
    ,"2.16.840.1.114107.1.1.16.2.15"  - MSC
    ,"2.16.840.1.114107.1.1.16.2.16"  - SHAH
*/

// affected domains are - MEDSTAR, FSH, GSH, HHC, UMH
def localDomainUUIDlist = [
     "2.16.840.1.114107.1.1.16.2.5"
    ,"2.16.840.1.114107.1.1.16.2.7"
    ,"2.16.840.1.114107.1.1.16.2.2"
    ,"2.16.840.1.114107.1.1.16.2.4"
    ,"2.16.840.1.114107.1.1.16.2.8"
    ,"2.16.124.113638.1.2.1.1"
    ,"2.16.840.1.114107.1.1.16.2.3"
    ,"2.16.840.1.114107.1.1.16.2.6"
    ,"2.16.840.1.114107.1.1.16.2.9"
    ,"2.16.840.1.114107.1.1.16.2.10"
    ,"2.16.840.1.114107.1.1.16.2.11"
    ,"2.16.840.1.114107.1.1.16.2.12"
    ,"2.16.840.1.114107.1.1.16.2.13"
    ,"2.16.840.1.114107.1.1.16.2.14"
    ,"2.16.840.1.114107.1.1.16.2.15"
    ,"2.16.840.1.114107.1.1.16.2.16"
];

if (localPid != null && localDomainUUIDlist.contains(localPid.domainUUID) ) {
    log.debug( scriptName + "PID={} with domainUUID={} and AccessionNumber={} is local, will process.", localPid, localPid.domainUUID, accessionNumber )
    log.info( scriptName + "END" )
    return true;
} else {
    log.debug( scriptName + "PID={} with domainUUID={} and AccessionNumber={} is NOT local, skipping...", localPid, localPid.domainUUID, accessionNumber )
    log.info( scriptName + "END" )
    return false;
}

