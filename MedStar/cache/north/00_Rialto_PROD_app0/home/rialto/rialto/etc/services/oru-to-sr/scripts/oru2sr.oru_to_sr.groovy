/**
 * converts HL7 ORU to a DICOM SR object for Medseek
 * draft according to the SAD v1.5
 *
 * many of the HL7 locations are potentially different in the field. this 
 * script is simply a starting point.
 */

import org.dcm4che2.util.UIDUtils
import org.joda.time.DateTime

def scriptName = "ORU2SR ORU Morpher - "

log.info(scriptName + "START...")

log.debug(scriptName + "HL7 input:\n{}", input)

//log.debug("Structure of input message:\n{}", input.getMessage().printStructure(true))

LOAD("oru2sr.common.groovy")

def namespace_map = [
             'HELIX': 'MEDSTAR',
             'MFSMC': 'FSH',
             'MGSH': 'GSH',
             'MHH': 'HHC',
             'MUMH': 'UMH',
             'MGUH': 'GUH',
             'MWHC': 'WB4',
             'MNRH': '6B4',
             'MMMC': 'MGI',
             'MSMH': 'MSMH',
             'MAS': 'MAS',
             'MAS2': 'MAS2',
             'MPP': 'MPP',
             'MSC': 'MSC',
             'SHAH': 'SHAH',
             'MSMHC': 'MSMHC'
            ]

def ipid = get('PID-3-4-1')
def lookup_ipid = namespace_map[ipid]
if (lookup_ipid != null) {
    ipid = lookup_ipid
}

set(SpecificCharacterSet, 'ISO_IR 100')
set(InstanceCreationDate, new DateTime())
set(InstanceCreationDate, new DateTime())
set(InstanceCreatorUID, '1.2.3.4.5.6.7.8.9')
set(Modality, 'SR')
set(SOPClassUID, '1.2.840.10008.5.1.4.1.1.88.11') // basic SR
set(ValueType, 'CONTAINER')
set(ContinuityOfContent, 'CONTINUOUS')
set('ConceptNameCodeSequence/CodeValue', '18748-4')
set('ConceptNameCodeSequence/CodingSchemeDesignator', 'LN')
set('ConceptNameCodeSequence/CodeMeaning', 'Diagnostic Imaging Report')
set(InstanceNumber, '1')
set(SeriesNumber, '1')
set(Manufacturer, 'Karos Health')

/* PID SEGMENT */
setPersonName(PatientName, 'PID-5') //
setPersonName('PatientMotherBirthName', 'PID-6') 
set(PatientBirthDate, get('PID-7')) //
set(PatientSex, get('PID-8')) //
set('PatientAddress', get('PID-11'))
set('PatientTelephoneNumbers', get('PID-13'))
set(PatientID, get('PID-3-1'))
set(IssuerOfPatientID, ipid) // or inferred


/* OBR SEGMENT */
set(AccessionNumber, get('OBR-3')) //
//4-1, 4-2 present, 4-4, 4-6 missing
set(StudyDescription, get('OBR-4-1')+'^'+get('OBR-4-2')) //+'^'+get('OBR-4-4')+'^'+get('OBR-4-6'))

def studyDate = DICOMTime.parseDate(get('OBR-7'))
def studyTime = DICOMTime.parseTime(get('OBR-7'))
set(StudyDate, studyDate)
set(StudyTime, studyTime)
set(SeriesDate, studyDate)
set(SeriesTime, studyTime)

def contentDate = get('OBR-22') != null ? DICOMTime.parseDate(get('OBR-22')) : studyDate
def contentTime = get('OBR-22') != null ? DICOMTime.parseTime(get('OBR-22')) : studyTime

set(ContentDate, contentDate)
set(ContentTime, contentTime)

set('AnatomicRegionSequence/CodeValue', get('OBR-15-1-1'))
set('AnatomicRegionSequence/CodeMeaning', get('OBR-15-1-2'))
set('AnatomicRegionSequence/CodingSchemeDesignator', get('OBR-15-1-3'))
setPersonName(ReferringPhysicianName, 'OBR-16') //
set(StudyID, get('OBR-20'))

set('VerifyingObserverSequence/VerifyingOrganization', get('MSH-4'))
set('VerifyingObserverSequence/VerificationDateTime', get('OBR-7'))
set('VerifyingObserverSequence/VerifyingObserverName', get('OBR-32-2')+ '^' + get('OBR-32-3'))
seq = 'VerifyingObserverSequence/VerifyingObserverIdentificationCodeSequence'
set(seq+'/CodeValue', get('OBR-32-1'))
set(seq+'/CodingSchemeDesignator', get('MSH-4'))
set(seq+'/CodeMeaning', get('OBR-32-2')+ '^' + get('OBR-32-3'))


set('PerformedProcedureCodeSequence/CodeValue', get('OBR-15-1-1'))

def completionFlag = get('OBR-25')
if( completionFlag != null ) {
    if( completionFlag.equals('F') ) {
        //supposedly this is invalid
        //set('PerformedProcedureCodeSequence/CompletionFlag', 'COMPLETE')
        set('CompletionFlag', 'COMPLETE')
    } else if( completionFlag.equals('P') ) {
        //supposedly this is invalid
        //set('PerformedProcedureCodeSequence/CompletionFlag', 'PARTIAL')
        set('CompletionFlag', 'PARTIAL')
    } else {
        log.warn('Completion flag unrecognized')
    }
}

set('VerifyingObserverIdentificationCodeSequence/CodeValue', get('OBR-32-1')) //
setPersonName('VerifyingObserverIdentificationCodeSequence/VerifyingObserverName', 'OBR-32-2') //Looks like this should join across OBR-32-2+


set('ReferencedPerformedProcedureStepSequence', [])

/* OBX SEGMENT */
def verificationFlag = get('OBX-11')
if( verificationFlag != null ) {
    if( verificationFlag.equals('F') ) {
        set('VerificationFlag', 'VERIFIED')
    } else if( verificationFlag.equals('P') ) {
        set('VerificationFlag', 'UNVERIFIED')
    } else {
        log.warn('Verification flag unrecognized');
    }
}
set('ContentSequence[1]/RelationshipType', 'HAS CONCEPT MOD')
set('ContentSequence[1]/ValueType', 'CODE')
set('ContentSequence[1]/ConceptNameCodeSequence/CodeValue', '121058')
set('ContentSequence[1]/ConceptNameCodeSequence/CodingSchemeDesignator', 'DCM')
set('ContentSequence[1]/ConceptNameCodeSequence/CodeMeaning', 'Procedure Reported')

set('ContentSequence[1]/ConceptCodeSequence/CodeValue', get('OBR-4-1'))
set('ContentSequence[1]/ConceptCodeSequence/CodingSchemeDesignator', 'RP')
set('ContentSequence[1]/ConceptCodeSequence/CodeMeaning', get('OBR-4-2'))

set('ContentSequence[2]/RelationshipType', 'HAS CONCEPT MOD')
set('ContentSequence[2]/ValueType', 'CODE')
set('ContentSequence[2]/ConceptNameCodeSequence/CodeValue', '121049')
set('ContentSequence[2]/ConceptNameCodeSequence/CodingSchemeDesignator', 'DCM')
set('ContentSequence[2]/ConceptNameCodeSequence/CodeMeaning', 'Language of Content Item and Descendants')

set('ContentSequence[2]/ConceptCodeSequence/CodeValue', 'en') // or fr?
set('ContentSequence[2]/ConceptCodeSequence/CodingSchemeDesignator', 'RFC3066')
set('ContentSequence[2]/ConceptCodeSequence/CodeMeaning', 'English') // or french


set('ContentSequence[3]/RelationshipType', 'HAS OBS CONTEXT')
set('ContentSequence[3]/ValueType', 'CODE')
set('ContentSequence[3]/ConceptNameCodeSequence/CodeValue', '121005')
set('ContentSequence[3]/ConceptNameCodeSequence/CodingSchemeDesignator', 'DCM')
set('ContentSequence[3]/ConceptNameCodeSequence/CodeMeaning', 'Observer Type')

set('ContentSequence[3]/ConceptCodeSequence/CodeValue', '121006')
set('ContentSequence[3]/ConceptCodeSequence/CodingSchemeDesignator', 'DCM')
set('ContentSequence[3]/ConceptCodeSequence/CodeMeaning', 'Person')

set('ContentSequence[4]/RelationshipType', 'HAS OBS CONTEXT')
set('ContentSequence[4]/ValueType', 'CODE')
set('ContentSequence[4]/ConceptNameCodeSequence/CodeValue', '121011')
set('ContentSequence[4]/ConceptNameCodeSequence/CodingSchemeDesignator', 'DCM')
set('ContentSequence[4]/ConceptNameCodeSequence/CodeMeaning', 'Person Observer\'s Role in this Procedure')

set('ContentSequence[4]/ConceptCodeSequence/CodeValue', '121097')
set('ContentSequence[4]/ConceptCodeSequence/CodingSchemeDesignator', 'DCM')
set('ContentSequence[4]/ConceptCodeSequence/CodeMeaning', 'Recording')


set('ContentSequence[5]/RelationshipType', 'CONTAINS')
set('ContentSequence[5]/ValueType', 'CONTAINER')
set('ContentSequence[5]/ConceptNameCodeSequence/CodeValue', '121064')
set('ContentSequence[5]/ConceptNameCodeSequence/CodingSchemeDesignator', 'DCM')
set('ContentSequence[5]/ConceptNameCodeSequence/CodeMeaning', 'Current Procedure Description')
set('ContentSequence[5]/ContinuityOfContent', 'CONTINUOUS')

seq = 'ContentSequence[5]/ContentSequence'

set(seq+'/RelationshipType', 'CONTAINS')
set(seq+'/ValueType', 'TEXT')
set(seq+'/ConceptNameCodeSequence/CodeValue', '121065')
set(seq+'/ConceptNameCodeSequence/CodingSchemeDesignator', 'DCM')
set(seq+'/ConceptNameCodeSequence/CodeMeaning', 'Procedure Description')
set(seq+'/TextValue', get('OBR-4-1') + '^' + get('OBR-4-2'))

set('ContentSequence[6]/RelationshipType', 'CONTAINS')
set('ContentSequence[6]/ValueType', 'CONTAINER')
set('ContentSequence[6]/ConceptNameCodeSequence/CodeValue', '121070')
set('ContentSequence[6]/ConceptNameCodeSequence/CodingSchemeDesignator', 'DCM')
set('ContentSequence[6]/ConceptNameCodeSequence/CodeMeaning', 'Findings')
set('ContentSequence[6]/ContinuityOfContent', 'CONTINUOUS')

seq = 'ContentSequence[6]/ContentSequence'
set(seq+'/ValueType', 'TEXT')
set(seq+'/RelationshipType', 'CONTAINS')
set(seq+'/ConceptNameCodeSequence/CodeValue', '121071')
set(seq+'/ConceptNameCodeSequence/CodingSchemeDesignator', 'DCM')
set(seq+'/ConceptNameCodeSequence/CodeMeaning', 'Finding')
set(seq+'/ContinuityOfContent', 'CONTINUOUS')

//java.util.ArrayList
def reportTextSegments = getList('ORDER_OBSERVATION/OBSERVATION(*)/OBX-5(*)')

if (reportTextSegments != null) {
    log.debug(scriptName + "found {} OBX-5 segments", reportTextSegments.size() )
    for (int i = 0; i < reportTextSegments.size(); i++)
    {
        if (reportTextSegments[i] == null)
        {
           reportTextSegments[i] = ""
        }
    }
}
else {
    log.debug( scriptName + "could not find any OBX-5 segments" )
}

def reportText = reportTextSegments.join('\n')
//reportText = reportText.replaceAll("\\\\x0d\\\\\\\\x0a\\\\", "\r\n")
//reportText = reportText.replaceAll("\\\\x0D\\\\\\\\x0A\\\\", "\r\n")
log.debug(scriptName + "DEBUG: reportText: {}", reportText)
set(seq+'/TextValue', reportText)


log.debug(scriptName + "output:\n {}", output)

log.info(scriptName + "END...")
