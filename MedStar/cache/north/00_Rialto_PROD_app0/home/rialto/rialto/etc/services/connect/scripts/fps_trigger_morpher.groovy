def scriptName = "Connect FPS Morpher - "

log.info(scriptName + "START")

def messageType = get('/.MSH-9-1')
def triggerEvent = get('/.MSH-9-2')

log.info(scriptName + "messageType is: {}", messageType)
log.info(scriptName + "triggerEvent is: {}", triggerEvent)

if (!"ORM".equalsIgnoreCase(messageType) && !"SIU".equalsIgnoreCase(messageType)) {
    log.warn(scriptName + "An unactionable message was provided, taking no action on message {}^{}", messageType, triggerEvent);
    return false
}

LOAD('/home/rialto/rialto/etc/services/common/common_medstar_hl7.groovy')
MedStar_HL7 ms = new MedStar_HL7(log, input)

ms.groomPids()

log.debug(scriptName + "replacing ORC-2 with ORC-3...")
input.set("ORC-2", input.get("ORC-3"))

log.info(scriptName  + "END")
