log.debug("Processing patient demographics update script.\n {}", input)
def publishManifestRequired = false

def updateValueInManifestIfRequired = { tag, originalValue, newValue ->
    if (originalValue != newValue) {
        set(tag, newValue)
        publishManifestRequired = true
    }
}

updateValueInManifestIfRequired(
    PatientName,
    output.get(PatientName),
    get('PID-5-1') + '^' + get('PID-5-2'))

updateValueInManifestIfRequired(
    PatientBirthDate,
    output.get(PatientBirthDate),
    get('PID-7'))

updateValueInManifestIfRequired(
    PatientSex,
    output.get(PatientSex),
    get('PID-8'))

updateValueInManifestIfRequired(
    IssuerOfPatientID,
    output.get(IssuerOfPatientID),
    get('PID-3-4-2'))

log.debug("Done processing patient demographics update script. Current Manifest:\n {}", output)

if (publishManifestRequired && output.get(NumberOfStudyRelatedInstances) != null) {
    log.debug("Patient demographics processing script indicating that publish is required")
    return true

} else if (publishManifestRequired && output.get(NumberOfStudyRelatedInstances) == null) {
    log.debug("Patient demographics processing script updated the manifest, but there are no images so publishing not required.")
    return false

} else {
    log.debug("Patient demographics processing script indicating that publish is not required")
    return false

}