class Context_Mapping {
    

    static  knownIssuersOfPID = [

    '1.1.1':'Context 1',
    '1.1.2':'Context 2',
    '1.1.3':'Context 3',
    '2.16.124.10.101.1.60.1.1007.1':'Hôpital général du Lakeshore',
    '2.16.124.10.101.1.60.1.1004.1':'CHU Sainte-Justine',

    // UNKNOWN is to handle in-house SRs whose Context is not known
    'UNKNOWN':'UNKNOWN'
    ]

    static Resolve_Context_by_AETitle (callingAE, calledAE) {
    
        def issuerOfPID = null
        def institutionName = null
        def mapvalues = null

        mapvalues = AETitles_to_Issuer_and_Institution_Map[callingAE, calledAE] 

        if (mapvalues != null ) {
            issuerOfPID = mapvalues[0]
            institutionName = mapvalues[1]
        }

        return [issuerOfPID, institutionName]
    }


    private static final AETitles_to_Issuer_and_Institution_Map = [
        ["Context1_SCU","VAULT"] : ["1.1.1","Context 1"],
        ["Context2_SCU","VAULT"] : ["1.1.2","Context 2"],
        ["Context3_SCU","VAULT"] : ["1.1.3","Context 3"],
        ["C1_AETITLE_SCU","C1_AETITLE_SCP"] : ["1.1.1","Context 1"],
        ["C2_AETITLE_SCU","C2_AETITLE_SCP"] : ["1.1.2","Context 2"],
        ["C3_AETITLE_SCU","C3_AETITLE_SCP"] : ["1.1.3","Context 3"],

        // Called and Calling AE Titles that HMI EIR uses when moving in Studies with a Lakeshore Context
        ["H_LKSHR_SCU", "VLT_H_LKSHR"] : ["2.16.124.10.101.1.60.1.1007.1", "Lakeshore"],

        // Called and Calling AE Titles that HMI EIR uses when moving in Studies with a Ste-Justine Context
        ["H_STJSTN_SCU","VLT_H_STJSTN"] : ["2.16.124.10.101.1.60.1.1004.1","Ste-Justine"],

        // Called and Calling AE Titles that we use when testing manually for a Lakeshore Context
        ["ALI_QUERY_SCU","RIALTO_TEST"] : ["2.16.124.10.101.1.60.1.1007.1","Lakeshore"]
    ]


    static Issuer_from_Institution_Name = [
        "LATOURIX" : "2.16.124.10.101.1.60.1.1007.1"
    ]

} // End of Context_Mapping Class
