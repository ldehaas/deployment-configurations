/*
* This script will return false if the instance is of a specific class type (various SR and PR classes).  Returning false
* will prevent this instance from being routed. 
*/
def noRouteList = ['1.2.840.10008.5.1.4.1.1.11.1','1.2.840.10008.5.1.4.1.1.11.2','1.2.840.10008.5.1.4.1.1.11.3','1.2.840.10008.5.1.4.1.1.11.4','1.2.840.10008.5.1.4.1.1.88.11','1.2.840.10008.5.1.4.1.1.88.22','1.2.840.10008.5.1.4.1.1.88.33','1.2.840.10008.5.1.4.1.1.88.50','1.2.840.10008.5.1.4.1.1.88.65','1.2.840.10008.5.1.4.1.1.88.67','1.3.12.2.1107.5.9.1'];
def sopClassUid = get(SOPClassUID);

if (sopClassUid in noRouteList) {

   log.info("Not forwarding instance SOP {} to AET GUH_Rad_Rialto because it has SOP Class UID {}", get(SOPInstanceUID), sopClassUid);
   return false;
}

