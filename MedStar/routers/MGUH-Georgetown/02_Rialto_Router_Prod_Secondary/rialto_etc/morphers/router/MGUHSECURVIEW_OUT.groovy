/*
* This script will return false if the instance is of a specific class type (various SR and PR classes).  Returning false
* will prevent this instance from being routed. 
*/
def noRouteList = ['1.2.840.10008.5.1.4.1.1.11.1','1.2.840.10008.5.1.4.1.1.6.1','1.2.840.10008.5.1.4.1.1.88.50','1.2.840.10008.5.1.4.1.1.66'];
def sopClassUid = get(SOPClassUID);

if (sopClassUid in noRouteList) {

   log.info("Not forwarding instance SOP {} to AET SCWMGR because it has SOP Class UID {}", get(SOPInstanceUID), sopClassUid);
   return false;
}

