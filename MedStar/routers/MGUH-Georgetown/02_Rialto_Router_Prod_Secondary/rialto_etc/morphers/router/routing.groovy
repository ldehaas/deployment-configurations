/**
* This script will decide which dataflow template name to return.
*/
def called_AET = getCalledAETitle().toUpperCase();
def Amalga_AET = "GUH_RAD_RIALTO";
def SecurView_AET = "MGUHSECURVIEW";

if (called_AET.equals(Amalga_AET)) {

        log.info("Called AET is Amalga_AET will send only to Amalga");
        return "etc/dataflows/GUH_Rad_Rialto.xml";
}
if (called_AET.equals(SecurView_AET)) {

        log.info("Called AET is Amalga_AET will send only to Amalga");
        return "etc/dataflows/MGUHSECURVIEW.xml";
}
if  (get(Modality).equalsIgnoreCase("MG")) {
		log.info("Mammo studies sent through Tomo capable IMG indexStore-P0UY");
        return "etc/dataflows/mammotomo.xml";
 } 

return "etc/dataflows/default.xml"

