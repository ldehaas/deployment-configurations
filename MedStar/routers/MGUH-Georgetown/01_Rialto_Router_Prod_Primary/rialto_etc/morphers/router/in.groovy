/**
 * This script handles the in-morphing for MGUH.
 *
 *  All objects sent will be stamped with the
 *  IPID value and various other values within the study.
 *  All objects will have the PatientID value checked, if null/empty
 *  set value. 
 */
import java.security.MessageDigest;

IPID ipid = new IPID(log, input)

ipid.checkIPID();

class IPID {
    def log = null;
    def sop = null;
    def defaultPID = "MSH-TEMP_ID";
    //KHC6121 location is a list of all Medstar IPID
    def sopIPID = sop.get(IssuerOfPatientID);
    def locationMap = ['MEDSTAR' : '2.16.840.1.114107.1.1.16.2.5', 
                        'GSH' : '2.16.840.1.114107.1.1.16.2.2',
                        'WB4' : '2.16.840.1.114107.1.1.16.2.3',
                        'HHC' : '2.16.840.1.114107.1.1.16.2.4',
                        'GUH' : '2.16.840.1.114107.1.1.16.2.6',
                        'FSH' : '2.16.840.1.114107.1.1.16.2.7',
                        'UMH' : '2.16.840.1.114107.1.1.16.2.8',
                        '6B4' : '2.16.840.1.114107.1.1.16.2.9',
                        'MGI' : '2.16.840.1.114107.1.1.16.2.10',
                        'MSMH' : '2.16.840.1.114107.1.1.16.2.11',
                        'MSMHC' : '2.16.840.1.114107.1.1.16.2.12',
                        'MAS' : '2.16.840.1.114107.1.1.16.2.13',
                        'MPP' : '2.16.840.1.114107.1.1.16.2.14',
                        'MSC' : '2.16.840.1.114107.1.1.16.2.15',
                        'SHAH' : '2.16.840.1.114107.1.1.16.2.16']   //no comma at the last one

    def IPID(log, sop) {
        this.log = log;
        this.sop = sop;
    }

    //KHC6121 check to see if IPID belongs to Medstar or not
    def checkIPID(){
        log.info("Original IPID is " + sopIPID + ", original UID is " + sop.get("IssuerOfPatientIDQualifiersSequence/UniversalEntityID") + ", and original UID type is " + sop.get("IssuerOfPatientIDQualifiersSequence/UniversalEntityIDType"))
        if (sopIPID != null && locationMap.containsKey(sopIPID)) {
            stampUID(sopIPID);
        }else{
            set("GUH","2.16.840.1.114107.1.1.16.2.6","ISO")
            setDefaultPatientID()
        }
        if (sop.get(AccessionNumber) == null || sop.get(AccessionNumber).isEmpty()) {
        setAccessionNumber();
        }
    }
    //KHC6121 stamp UID and UID type anyways in case they are null
    def stampUID(ipidString)
    {
        log.info("It's a Medstar IPID, setting UID and UID type to match.");
        def domain = locationMap.get(ipidString);

        log.info("SOP: " + sop.get("SOPInstanceUID") + ", setting universal id to: " + domain);
        sop.set("IssuerOfPatientIDQualifiersSequence/UniversalEntityID", domain, VR.LO)
        log.info("Now UID is " + sop.get("IssuerOfPatientIDQualifiersSequence/UniversalEntityID"));
        sop.set("IssuerOfPatientIDQualifiersSequence/UniversalEntityIDType", "ISO", VR.LO)
        log.info("Now UID type is " + sop.get("IssuerOfPatientIDQualifiersSequence/UniversalEntityIDType"));
    }

    def set(namespace, universalid, universalidtype) {

        if (sop.get(IssuerOfPatientID) != null || sop.get("IssuerOfPatientIDQualifiersSequence/UniversalEntityID") != null || sop.get("IssuerOfPatientIDQualifiersSequence/UniversalEntityIDType") != null) {
            def curr_i = sop.get(IssuerOfPatientID)
            def curr_u = sop.get("IssuerOfPatientIDQualifiersSequence/UniversalEntityID")
            def curr_ut = sop.get("IssuerOfPatientIDQualifiersSequence/UniversalEntityIDType")

            log.info("SOP: " + sop.get("SOPInstanceUID") + ", : Going to move existing issuer information [IussuerOfPatientID:  " + curr_i + ", UniversalEntityID: " + curr_u + ", UniversalEntityIDType: " + curr_ut + "] to a Karos specific private tag")

            sop.set(0x00350010, "KAROS PRESERVED INFO 1.0", VR.LO)
            sop.set(0x00351010, curr_i, VR.LO)
            sop.set(0x00351011, curr_u, VR.LO)
            sop.set(0x00351012, curr_ut, VR.LO)
        }

        log.info("SOP: " + sop.get("SOPInstanceUID") + ", setting namespace to: " + namespace);
        sop.set(IssuerOfPatientID, namespace)

        log.info("SOP: " + sop.get("SOPInstanceUID") + ", setting universal id to: " + universalid);
        sop.set("IssuerOfPatientIDQualifiersSequence/UniversalEntityID", universalid, VR.LO)

        log.info("SOP: " + sop.get("SOPInstanceUID") + ", setting universal id type to: " + universalidtype);
        sop.set("IssuerOfPatientIDQualifiersSequence/UniversalEntityIDType", universalidtype, VR.LO)
    }

    /*
    * This method will check to see if the PatientID is either NULL or empty. If so then set the value 
    * of PatientID to the customer supplied string.
    */
    def setDefaultPatientID() {
        // if the PatientID is either NULL or empty enter if block
        if (!sop.get("PatientID") ) {
            log.info("SOP: " + sop.get("SOPInstanceUID") + ", empty PatientID. Setting PatientID to: " + defaultPID);
            sop.set(PatientID, defaultPID);
        }
    }
    def setAccessionNumber() {
        def hashedSUID = MessageDigest.getInstance("MD5").digest(sop.get("StudyInstanceUID").getBytes()).encodeBase64().toString()
        //prefix GUH+ hash of studyinstance UID
        def accessionNumber = "GUH" + sop.get("PatientID").padLeft(7, '0').reverse().take(7).reverse() + hashedSUID.replaceAll("[^a-zA-Z0-9]", "").toUpperCase().reverse().take(6).reverse();

        log.info("SOP: " + sop.get("SOPInstanceUID") + ", has blank AccessionNumber, setting AccessionNumber to {}", accessionNumber);
        sop.set(0x00080050, accessionNumber, VR.SH);
    }
}


