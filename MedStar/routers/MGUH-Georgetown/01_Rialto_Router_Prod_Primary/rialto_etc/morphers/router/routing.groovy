/**
* This script will decide which dataflow template name to return.
*/

def Phillips_Pacs_AET = "STENTOR_SCP";
def Phillips_Pacs_SCUAET = "STENTOR_SCU";
def Msmhmamo_AET = "MSMHMAMO";
def Pacsscan_AET = "PACSSCAN";
def Pacsgear_AET = "PACSGEAR";
def Local_IMG_AET = "INDEXQUERY-P0JG";
def Amalga_AET = "GUH_RAD_RIALTO";
def GUHSYNGOVIA01_L_AET = "GUHSYNGOVIA01_L";
def DYNACAD_SRV_L_AET = "DYNACAD_SRV_L";
def DYNACAD_INT_L_AET = "DYNACAD_INT_L";
def DYNACAD_NEURO_L_AET = "DYNACAD_NEURO_L";
def DYNACAD_DIAG3_L_AET = "DYNACAD_DIAG3_L";
def MGUHSECURVIEW_AET = "MGUHSECURVIEW";
def Rialto_Cache_Prod = "RIALTO_CACHE";
def Rialto_Cache_Test = "RIALTO_CACHE_T"; 
def Rialto_Router_Test = "RIALTO_ROUTER_T";
def calling_AET = getCallingAETitle().toUpperCase();
def called_AET = getCalledAETitle().toUpperCase();
def todayDate = new Date().format('EE');
def dfName = null;


if (called_AET.equals(Amalga_AET)) {
    log.info("Called AET is Amalga_AET will send only to Amalga");
    return "etc/dataflows/GUH_Rad_Rialto.xml";
}

if (called_AET.equals(MGUHSECURVIEW_AET)) {
    log.info("Called AET is MGUHSECURVIEW will send only to SecurView");
    return "etc/dataflows/MGUHSECURVIEW.xml";
}

if (called_AET.equals(GUHSYNGOVIA01_L_AET)) {
    log.info("Called AET is GUHSYNGOVIA01_L will send LEI if possible");
    return "etc/dataflows/guhsyngovia01.xml";
}

if (called_AET.equals(DYNACAD_SRV_L_AET)) {
    log.info("Called AET is DYNACAD_SRV_L will send LEI if possible");
    return "etc/dataflows/guhdynacad_srv_01.xml";
}

if (called_AET.equals(DYNACAD_INT_L_AET)) {
    log.info("Called AET is DYNACAD_INT_L will send LEI if possible");
    return "etc/dataflows/guhdynacad_int_01.xml";
}

if (called_AET.equals(DYNACAD_NEURO_L_AET)) {
    log.info("Called AET is DYNACAD_NEURO_L will send LEI if possible");
    return "etc/dataflows/guhdynacad_neuro_01.xml";
}

if (called_AET.equals(DYNACAD_DIAG3_L_AET)) {
    log.info("Called AET is DYNACAD_DIAG3_L will send LEI if possible");
    return "etc/dataflows/guhdynacad_diag3_01.xml";
}

/*  Special dataflow,  re khc 8147  */
if (called_AET.equals("RIALTO_MGUH") && get(SOPClassUID) != null && get(SOPClassUID) == '1.3.12.2.1107.5.9.1') {
    log.info("Called AET is Amalga_AET will not be sent to Amalga");
    return "etc/dataflows/default-noamalga.xml";
}

if ((calling_AET.startsWith(Pacsscan_AET)) || (calling_AET.startsWith(Pacsgear_AET)) ) {
    if (get(StudyDescription) != null ) {
        if ((get(StudyDescription).contains("SCRN")) || (get(StudyDescription).toLowerCase().contains("screen"))) {
            log.info("Screening Mammo Scanned Doc, using pacsscanscreeningMG.xml");
            return "etc/dataflows/pacsscanscreeningMG.xml";
        }
    }
    log.info("Calling AET starts with pacsscan or pacsgear, using dataflow route pacsscan.xml");
    return "etc/dataflows/pacsscan.xml";
}

/*  Special dataflow if mod type is NM ,  re khc 5666  */
if  (get(Modality).equalsIgnoreCase("NM")) {
        return "etc/dataflows/nm_studies_with_amalga.xml";
} 
        
switch (calling_AET) {
        case Phillips_Pacs_AET:
                dfName = "etc/dataflows/stentor-scp.xml";
                break
        case Phillips_Pacs_SCUAET:
                dfName = "etc/dataflows/stentor-scu.xml";
                break
        case Msmhmamo_AET:
                dfName = "etc/dataflows/msmhmamo.xml";
                break
        case Local_IMG_AET:

                switch (called_AET) {

                    case Rialto_Cache_Prod:
                        dfName = "etc/dataflows/only-rialto-prod-and-compress.xml";
                        break

                    case Rialto_Cache_Test:
                        dfName = "etc/dataflows/only-rialto-test-and-compress.xml";
                        break

                    case Rialto_Router_Test:
                        dfName = "etc/dataflows/only-rialto-test-router-and-no-compress.xml";
                        break

                    default:
                        dfName = "etc/dataflows/default_central_rialto_only.xml";

                }
                break
        default:
                dfName = "etc/dataflows/default.xml";

}
/*  Special dataflow for mammography,  re khc 6609  */
if (get(StudyDescription) != null && get(Modality) != null) {
    if ( (get(Modality).equalsIgnoreCase("MG")) && ((get(StudyDescription).toLowerCase().contains("scrn")) || (get(StudyDescription).toLowerCase().contains("screen"))) ) {
	    log.info("routing.groovy: Using alternate workflow for modality: MG Description: SCRN/screen");
        dfName = "etc/dataflows/GUHScreeningMammo.xml"
    }
}

if (calling_AET.startsWith("MAMMCAD")) {
        log.info("Mammo CAD routing to Lafayette, using dataflow route GUHMammoCADtoLaf.xml");
        return "etc/dataflows/GUHMammoCADtoLaf.xml";
}


return dfName;


