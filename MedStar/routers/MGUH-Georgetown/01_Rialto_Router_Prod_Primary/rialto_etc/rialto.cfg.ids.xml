<?xml version="1.0" encoding="UTF-8"?>
<config>
    <var name="HostIP" value="${env.IP_ADDR_ETH0}" />
    <var name="DatabaseURL" value="jdbc:oracle:thin:tmed/tmed@10.0.1.62:1521:tmed"/>
    
    <!-- XDS REPOSITORY VARIABLES -->
    <var name="XDSRepository-Host" value="localhost" />
    <var name="XDSRepository-Port" value="8082" />
    <var name="XDSRepository-Path" value="/vault/repo" />
    <var name="XDSRepository-ID" value="1.2.3.4.5"/>
    <var name="XDSRepository-URL" value="http://${XDSRepository-Host}:${XDSRepository-Port}${XDSRepository-Path}"/>
    
    <!-- receive images to generate manfiests -->
    <server id="dicom4104" type="dicom">
        <port>4104</port>
    </server>

    <server id="http13337" type="http">
        <port>13337</port>
    </server>
    
    <!-- receive ORU reports -->
    <server id="hl75555" type="hl7v2">
        <port>5555</port>
    </server>

    <server type="http" id="repo-http">
        <port>${XDSRepository-Port}</port>
    </server> 
    
    <!-- this has to be declared as the first service -->
    <service type="txn-records" id="txn-records">
        <config>
            <prop name="StorageDir">${rialto.rootdir}/var/txn_records</prop>
        </config>
    </service>

    <!-- CLUSTER CONFIGURATION -->
    <!-- Single Sign On in a Cluster -->
    <var name="SSO-Cluster-Bind-IP" value="127.0.0.1"/>
    <var name="SSO-Cluster-Multicast-IP" value="232.10.10.201"/>
    <var name="SSO-Cluster-Multicast-PORT" value="45201"/>

    <!-- Navigator Cache Manager in a cluster -->
    <var name="Navigator-Cluster-Bind-IP" value="127.0.0.1"/>
    <var name="Navigator-Cluster-Multicast-IP" value="232.10.10.202"/>
    <var name="Navigator-Cluster-Multicast-PORT" value="45202"/>

    <!-- Navigator Server access controller in a cluster -->
    <var name="Navigator-Server-Cluster-Bind-IP" value="127.0.0.1"/>
    <var name="Navigator-Server-Cluster-Multicast-IP" value="232.10.10.203"/>
    <var name="Navigator-Server-Cluster-Multicast-PORT" value="45203"/>


    <!-- NAVIGATOR VARIABLES -->
    <var name="Navigator-Host" value="${HostIP}"/>
    <var name="Navigator-HostProtocol" value="http" />
    <var name="Navigator-GUIPort" value="2525" />
    <var name="Navigator-APIPort" value="2524" />
    <var name="Navigator-RootPath" value="/rest" />
    <var name="Navigator-APIBasePath" value="/rialto" />
    <var name="Navigator-GUIBasePath" value="/" />
    <var name="Navigator-DefaultLocale" value="en" />
    <var name="Navigator-DefaultTheme" value="rialto-light" />
    <var name="Navigator-MaxPatientSearchResults" value="1000" />
    <var name="Navigator-MaxDocumentSearchResults" value="1000" />
    <var name="IdleUserSessionTimeout" value="30m"/>
    <var name="System-DefaultLocalDomain" value="mobile.army"/>

    <!-- USERMANAGEMENT VARIABLES -->
    <var name="Usermanagement-APIBasePath" value="/usermanagement" />
    <var name="Usermanagement-URL" value="${Navigator-HostProtocol}://${Navigator-Host}:${Navigator-APIPort}${Navigator-RootPath}" />
    <var name="Usermanagement-AuthenticatedURL" value="${Navigator-HostProtocol}://${Navigator-Host}:${Navigator-APIPort}${Navigator-RootPath}/api${Usermanagement-APIBasePath}" />

    <server type="http" id="authenticated-http">
        <port>${Navigator-APIPort}</port>
    </server>

    <server type="http" id="navigator-http">
        <port>${Navigator-GUIPort}</port>
    </server>

    <server type="http" id="audit-http">
        <port>${ARR-GUIPort}</port>
    </server>

    <service type="authentication" id="authentication">
        <server idref="authenticated-http" name="web-api">
            <url>${Navigator-RootPath}</url>
        </server>

        <config>
            <prop name="UserAuthConfig">

                <DatabaseURL>${DatabaseURL}</DatabaseURL>
                <systemDefaultDomainId>${System-DefaultLocalDomain}</systemDefaultDomainId>
                <globalInactivitySessionTimeout>${IdleUserSessionTimeout}</globalInactivitySessionTimeout>

                <singleSignOn>

                    <CacheManager>

                        <uid>ShiroSingleSignOnCacheManager</uid>
                        <diskStore path="${rialto.rootdir}/var/shiro-sessions-navigator"/>
                        <cache name="shiro-activeSessionCache" clusteringIsEnabled="false"/>

                    </CacheManager>

                </singleSignOn>

            </prop>
        </config>
    </service>

    <service type="usermanagement" id="usermanagement">
        <config>
            <prop name="SystemDefaultPermissions">
                <permission name="rialto.arr" value="false"/>
                <permission name="rialto.administration" value="false"/>
                <permission name="rialto.usermanagement.internalrealm" value="false"/>
                <permission name="rialto.events" value="false" />
                <permission name="rialto.workflows" value="true" />

                <!-- Doc access permissions here allow access to manifest documents when viewing workflows -->
                <permission name="rialto.navigator.docs.view.unknown.internal.always" value="true"/>
                <permission name="rialto.navigator.docs.view.unknown.external.always" value="true"/>
                <permission name="rialto.navigator.docs.view.unknown.internal.onbreakglass" value="false"/>
                <permission name="rialto.navigator.docs.view.unknown.external.onbreakglass" value="false"/>
            </prop>
            <prop name="web-api-path" value="${Usermanagement-APIBasePath}"/>
            <prop name="DatabaseURL" value="${DatabaseURL}"/>

            <include location="navigator/defaultuserprefs.ids.xml"/>
        </config>
    </service>

    <!-- PLUGIN: External Viewer (Download) -->
    <!-- Needed for downloading and viewing Manifest documents -->
    <var name="DownloadPlugin-Name" value="External Viewer (Download)" />
    <var name="DownloadPlugin-Description" value="External platform-specific viewer or download to local disk" />
    <service type="navigator.plugin.download" id="navigator.plugin.download">
        <device idref="xdsrep"/>
        <device idref="xdsreg"/>

        <config>
            <prop name="ForcedMimeTypeFileExtension">
                <prop mimeType="application/vnd.ms-excel" fileExtension="xls"/>
            </prop>
            <prop name="DatabaseURL" value="${DatabaseURL}"/>
            <prop name="web-api-path" value="/plugins/download"/>
            <prop name="UserManagementURL" value="${Usermanagement-AuthenticatedURL}"/>

            <include location="navigator/patientidentitydomains.xml"/>
            <include location="navigator/cacheStorage.xml"/>
        </config>
    </service>

    <service type="rialto.ui" id="rialto.ui">
        <server idref="authenticated-http" name="web-api">
            <url>/public/*</url>
        </server>

        <server idref="navigator-http" name="web-ui">
            <url>${Navigator-GUIBasePath}</url>
        </server>

        <config>
            <prop name="ApiURL" value="${Navigator-HostProtocol}://${Navigator-Host}:${Navigator-APIPort}${Navigator-RootPath}/api${Navigator-APIBasePath}"/>
            <prop name="ArrURL" value="${Navigator-HostProtocol}://${Navigator-Host}:${ARR-GUIPort}"/>
            <prop name="UserManagementURL" value="${Usermanagement-URL}"/>
            <prop name="PublicURL" value="${Navigator-HostProtocol}://${Navigator-Host}:${Navigator-APIPort}/public"/>
            <prop name="StudyManagementURL" value="http://${HostIP}:8080${IA-StudyManagement-Path}"/>
            <prop name="WadoURL" value="http://${HostIP}:13337/wado" />
            <prop name="WorkflowApiURL" value="http://${HostIP}:13337/monitoring" />
            <prop name="GlobalInactivitySessionTimeout" value="${IdleUserSessionTimeout}" />

            <prop name="SupportedThemes">
                rialto-light
                rialto-dark
            </prop>
            <prop name="DefaultTheme" value="${Navigator-DefaultTheme}"/>

            <prop name="SupportedLocales">
                en
                fr
            </prop>
            <prop name="DefaultLocale" value="${Navigator-DefaultLocale}"/>

            <prop name="Plugins">
                <plugin>
                    <name>${DownloadPlugin-Name}</name>
                    <description>${DownloadPlugin-Description}</description>
                    <mimeTypes>*</mimeTypes>
                    <url>${Navigator-HostProtocol}://${Navigator-Host}:${Navigator-APIPort}${Navigator-RootPath}/api/plugins/download</url>
                    <embedded>true</embedded>
                </plugin>
            </prop>
        </config>
    </service>


    <!-- VAULT - XDS REPOSITORY -->
    <service id="xdsrepo" type="xdsrepo">
        <device idref="xdsreg" />

        <server idref="repo-http" name="repository">
            <url>${XDSRepository-Path}</url>
        </server>

        <config>
            <prop name="IndexType" value="jpa" />
            <prop name="IndexerRetryTime" value="1m" />
            <prop name="UniqueId" value="${XDSRepository-ID}"/>
            <prop name="StorageType" value="file"/>
            <prop name="TempDir" value="${rialto.rootdir}/var/xdsrepo/temp" />
            <prop name="FileStorage" value="${rialto.rootdir}/var/xdsrepo/storage" />
            <prop name="IndexerDatabaseURL" value="${DatabaseURL}" />    
        </config>
    </service>
    
    <service id="ids" type="ids">
        <device idref="xdsreg" />
        <device idref="xdsrep" />
        <device idref="DicomSrDestination" name="DicomSrDestination" />
        <device idref="ReportHl7Destination" name="ReportHl7Destination" />
        <!-- The HL7FeedDestination is used to forward incoming HL7, ADT, or ORM messages
            to the EIR. Rialto will not process the message until the EIR receives and 
            positively acknowledges the message. If the device is not present, Rialto does
            not forward the message to any destinations. The device configuration is near
            the bottom of this file. -->
        <!--device idref="Hl7FeedDestination" name="Hl7FeedDestination" /-->

        <!-- dependencies for generating manifests -->
        <server idref="dicom4104" name="publishStudy" />
        
        <!-- dependencies to WADO enable the IA -->
        <server idref="http13337" name="WADO">
            <url>/wado/*</url>
        </server>

        <server idref="http13337" name="MonitoringApi">
            <url>/monitoring/*</url>
        </server>

        <server idref="http13337" name="DomainsApi">
            <url>/domains/*</url>
        </server>
 
        <server idref="http13337" name="DeprecateStudyApi">
            <url>/deprecate/*</url>
        </server>
                
        <!-- dependencies for receiving / storing ORUs -->
        <server idref="hl75555" name="OrderEndpoint" />
        <server idref="hl75555" name="ReportEndpoint" />
        <server idref="hl75555" name="HL7PatientFeed" />
        <server idref="hl75555" name="HL7PatientMerge" />

        <config>
            <prop name="DatabaseURL">${DatabaseURL}</prop>

            <prop name="CallingAEToIssuerOfPatientIDMapping">
                <mapping>
                    <CallingAE>DCMSND</CallingAE>
                    <IssuerOfPatientID>2.16.124.113638.1.1.1.1</IssuerOfPatientID>
                </mapping>
                <mapping>
                    <CallingAE>DCMSND2</CallingAE>
                    <IssuerOfPatientID>princeton.plainsoboro</IssuerOfPatientID>
                </mapping>
                <mapping>
                    <CallingAE>EIR</CallingAE>
                    <IssuerOfPatientID>6.7.8.9</IssuerOfPatientID>
                </mapping>
                <mapping>
                    <CallingAE>RialtoSend</CallingAE>
                    <IssuerOfPatientID>mobile.army</IssuerOfPatientID>
                </mapping>
            </prop>

            <prop name="ContextMappings">

                <ContextMapping context="CA" issuerOfPatientId="mobile.army" />
                <ContextMapping context="EI" issuerOfPatientId="2.16.124.113638.1.1.1.1" />

            </prop>
           
            <!-- local AE title used when sending cmove and to move images to -->
            <prop name="LocalAE">RIALTO</prop>
            <prop name="RAMQDomain">2.16.124.10.101.1.60.100</prop>
            <prop name="NIUDomain">NIU-Usager&amp;2.16.124.10.101.1.60.222&amp;ISO</prop>

            <prop name="WADOCacheFolder">
                ${rialto.rootdir}/var/ids/wadocache
            </prop>
            
            <prop name="ManifestContentTypeCode">manifest^^test</prop>

            <prop name="ORUToSRMorpher">
                ${rialto.rootdir}/etc/morphers/ids/oru_to_sr.groovy
            </prop>
            
            <prop name="SRToTextMorpher">
                ${rialto.rootdir}/etc/morphers/ids/sr_to_text.groovy
            </prop>
            
            <prop name="OrderUpdateScript">
                ${rialto.rootdir}/etc/morphers/ids/order_update_script.groovy
            </prop>
            
            <prop name="PatientDemographicsUpdateScript">
                ${rialto.rootdir}/etc/morphers/ids/patient_demographics_update_script.groovy
            </prop>

            <prop name="PatientMergeScript">
                ${rialto.rootdir}/etc/morphers/ids/patient_merge_script.groovy
            </prop>
            
            <prop name="ManifestMorpher">
                ${rialto.rootdir}/etc/morphers/ids/manifest.groovy
            </prop>

            <prop name="DocumentMetadataMorpher">
                ${rialto.rootdir}/etc/morphers/ids/metadata.groovy
            </prop>

            <prop name="NumberOfManifestProcessors">3</prop>
            <prop name="NumberOfReportProcessors">3</prop>
                    
            <prop name="RetrieveLocationUID">${XDSRepository-ID}</prop>
            <prop name="RetrieveAE">MCKESSON_EIR</prop>

            <prop name="ManifestBaseDirectory">${rialto.rootdir}/var/ids/manifests/</prop>
            
            <prop name="PublishStudyCStoreStorageLocation">
                ${rialto.rootdir}/var/ids/publishStudy
            </prop>
            
            <prop name="WorkflowDocumentStorageLocation">
                ${rialto.rootdir}/var/ids/workflowDocuments
            </prop>

            <prop name="PublishStudyCStoreGroupingTimeout">30s</prop>

            <include location="navigator/patientidentitydomains.xml" />
        </config>
    </service>
    
    <!-- xds registry and repository for publishing manifests -->
    <device id="xdsreg" type="xdsreg">
        <url>http://10.0.1.66:8081/registry</url>
        <affinitydomain>2.16.124.10.101.1.60.100</affinitydomain>
    </device>
    
    <device id="xdsrep" type="xdsrep">
        <url>${XDSRepository-URL}</url>
        <uniqueid>${XDSRepository-ID}</uniqueid>
    </device>
    
    <device id="DicomSrDestination" type="dicom">
        <host>localhost</host>
        <port>11112</port>
        <aetitle>DCMRCV</aetitle>
    </device>

    <device id="ReportHl7Destination" type="hl7v2">
        <host>localhost</host>
        <port>2775</port>
        <sendingApplication>sendApp</sendingApplication>
        <sendingFacility>sendFac</sendingFacility>
        <receivingApplication>RIALTO_VAULT_CA</receivingApplication>
        <receivingFacility>KAROS_HEALTH</receivingFacility>
        <timeout>20s</timeout>
    </device>

    <!-- The device configuration for the HL7FeedDestination.
        See the device reference in the ids service for usage. -->
    <!--device id="Hl7FeedDestination" type="hl7v2">
        <host>localhost</host>
        <port>5678</port>
        <sendingApplication>sendApp</sendingApplication>
        <sendingFacility>sendFac</sendingFacility>
        <receivingApplication>recApp</receivingApplication>
        <receivingFacility>recFac</receivingFacility>
        <persistentConnection>true</persistentConnection>
    </device-->

</config>
