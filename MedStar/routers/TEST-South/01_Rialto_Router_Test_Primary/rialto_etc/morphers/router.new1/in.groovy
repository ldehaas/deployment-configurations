/**
 * This script handles the in-morphing for for Hydration MUMH and MGSH..
 *
 *  All objects sent will be stamped with the
 *  IPID value and various other values within the study.
 *  All objects will have the PatientID value checked, if null/empty
 *  set value. 
 */

IPID ipid = new IPID(log, input)

ipid.setIPID(getCallingAETitle())

class IPID {
    def log = null;
    def sop = null;
    def defaultPID = "MSH-TEMP_ID";

    def IPID(log, sop) {
        this.log = log;
        this.sop = sop;
    }

    def setIPID(setHydrationSite) {

        if (setHydrationSite.equalsIgnoreCase("GSHINSITE02")) {
                set("GSH","2.16.840.1.114107.1.1.16.2.2","ISO")
        } else if (setHydrationSite.equalsIgnoreCase("indexQuery-P0H2")) {
                set("UMH","2.16.840.1.114107.1.1.16.2.8","ISO") 
        }
    }

    def set(namespace, universalid, universalidtype) {

        if (sop.get(IssuerOfPatientID) != null || sop.get("IssuerOfPatientIDQualifiersSequence/UniversalEntityID") != null || sop.get("IssuerOfPatientIDQualifiersSequence/UniversalEntityIDType") != null) {
            def curr_i = sop.get(IssuerOfPatientID)
            def curr_u = sop.get("IssuerOfPatientIDQualifiersSequence/UniversalEntityID")
            def curr_ut = sop.get("IssuerOfPatientIDQualifiersSequence/UniversalEntityIDType")

            log.info("SOP: " + sop.get("SOPInstanceUID") + ", : Going to move existing issuer information [IussuerOfPatientID:  " + curr_i + ", UniversalEntityID: " + curr_u + ", UniversalEntityIDType: " + curr_ut + "] to a Karos specific private tag")

            sop.set(0x00350010, "KAROS PRESERVED INFO 1.0", VR.LO)
            sop.set(0x00351010, curr_i, VR.LO)
            sop.set(0x00351011, curr_u, VR.LO)
            sop.set(0x00351012, curr_ut, VR.LO)
        }

        log.info("SOP: " + sop.get("SOPInstanceUID") + ", setting namespace to: " + namespace);
        sop.set(IssuerOfPatientID, namespace)

        log.info("SOP: " + sop.get("SOPInstanceUID") + ", setting universal id to: " + universalid);
        sop.set("IssuerOfPatientIDQualifiersSequence/UniversalEntityID", universalid, VR.LO)

        log.info("SOP: " + sop.get("SOPInstanceUID") + ", setting universal id type to: " + universalidtype);
        sop.set("IssuerOfPatientIDQualifiersSequence/UniversalEntityIDType", universalidtype, VR.LO)
    }

    /*
    * This method will check to see if the PatientID is either NULL or empty. If so then set the value 
    * of PatientID to the customer supplied string.
    */
    def setDefaultPatientID() {
        // if the PatientID is either NULL or empty enter if block
        if (!sop.get("PatientID") ) {
            log.info("SOP: " + sop.get("SOPInstanceUID") + ", empty PatientID. Setting PatientID to: " + defaultPID);
            sop.set(PatientID, defaultPID);
        }
    }
}
