IPID ipid = new IPID(log, input)

ipid.set("MGI","2.16.840.1.114107.1.1.16.2.10", "ISO")
// ipid.setInstitutionName(getCalledAETitle() )
//ipid.setIPID(getCallingAETitle())

class IPID {
    def log = null;
    def sop = null;
    //def instName = "MedStar Radiology Network – Bel Air";
    //def newCalledAE  = "RIALTO_MRN_BA";
    def defaultPID = "MSH-TEMP_ID";

    def IPID(log, sop) {
        this.log = log;
        this.sop = sop;
    }

    //def setInstitutionName(theCalledAET) {

    //log.info("darren - the calledAE is " + theCalledAET   );
    //   if (theCalledAET.equalsIgnoreCase(newCalledAE) ) {
    //      sop.set(0x00080080, instName, VR.LO)
    //      log.info("SOP: " + sop.get("SOPInstanceUID") + ", Setting Institution Name to: " + instName);
    //   }
   // }

    def setIPID(setHydrationSite) {

        if (setHydrationSite.equalsIgnoreCase("GHOPKINS2")) {
                set("GSH","2.16.840.1.114107.1.1.16.2.2","ISO")
        } else if (setHydrationSite.equalsIgnoreCase("indexQuery-P0JG")) {
                set("GSH","2.16.840.1.114107.1.1.16.2.2","ISO")
        } else if (setHydrationSite.equalsIgnoreCase("USKY")) {
                set("UMH","2.16.840.1.114107.1.1.16.2.8","ISO")
        } else if (setHydrationSite.equalsIgnoreCase("STENTOR_SCU")) {
                set("FSH","2.16.840.1.114107.1.1.16.2.7","ISO")
        } else if (setHydrationSite.equalsIgnoreCase("STENTOR_SCP")) {
                set("FSH","2.16.840.1.114107.1.1.16.2.7","ISO")
        } else if (setHydrationSite.equalsIgnoreCase("MFS01PLT")) {
                set("FSH","2.16.840.1.114107.1.1.16.2.7","ISO")
        } else if (setHydrationSite.equalsIgnoreCase("ALI_STORE_SCP")) {
                set("HHC","2.16.840.1.114107.1.1.16.2.4","ISO")
        } else if (setHydrationSite.equalsIgnoreCase("ALI_SCU")) {
                set("HHC","2.16.840.1.114107.1.1.16.2.4","ISO")
        } else if (setHydrationSite.equalsIgnoreCase("KPServerV")) {
                set("GUH","2.16.840.1.114107.1.1.16.2.6","ISO")
        } else if (setHydrationSite.equalsIgnoreCase("KPServerP")) {
                set("GUH","2.16.840.1.114107.1.1.16.2.6","ISO")
        } else if (setHydrationSite.equalsIgnoreCase("KPServer")) {
                set("GUH","2.16.840.1.114107.1.1.16.2.6","ISO")
        } else if (setHydrationSite.equalsIgnoreCase("GSMV30001")) {
                set("GSH","2.16.840.1.114107.1.1.16.2.2","ISO")
        }
    }


    def set(namespace, universalid, universalidtype) {

        if (sop.get(IssuerOfPatientID) != null || sop.get("IssuerOfPatientIDQualifiersSequence/UniversalEntityID") != null || sop.get("IssuerOfPatientIDQualifiersSequence/UniversalEntityIDType") != null) {
            def curr_i = sop.get(IssuerOfPatientID)
            def curr_u = sop.get("IssuerOfPatientIDQualifiersSequence/UniversalEntityID")
            def curr_ut = sop.get("IssuerOfPatientIDQualifiersSequence/UniversalEntityIDType")

            log.info("SOP: " + sop.get("SOPInstanceUID") + ", : Going to move existing issuer information [IussuerOfPatientID:  " + curr_i + ", UniversalEntityID: " + curr_u + ", UniversalEntityIDType: " + curr_ut + "] to a Karos specific private tag")

            sop.set(0x00350010, "KAROS PRESERVED INFO 1.0", VR.LO)
            sop.set(0x00351010, curr_i, VR.LO)
            sop.set(0x00351011, curr_u, VR.LO)
            sop.set(0x00351012, curr_ut, VR.LO)
        }

        log.info("SOP: " + sop.get("SOPInstanceUID") + ", setting namespace to: " + namespace);
        sop.set(IssuerOfPatientID, namespace)

        log.info("SOP: " + sop.get("SOPInstanceUID") + ", setting universal id to: " + universalid);
        sop.set("IssuerOfPatientIDQualifiersSequence/UniversalEntityID", universalid, VR.LO)

        log.info("SOP: " + sop.get("SOPInstanceUID") + ", setting universal id type to: " + universalidtype);
        sop.set("IssuerOfPatientIDQualifiersSequence/UniversalEntityIDType", universalidtype, VR.LO)
    }

    /*
    * This method will check to see if the PatientID is either NULL or empty. If so then set the value 
    * of PatientID to the customer supplied string.
    */
    def setDefaultPatientID() {
        // if the PatientID is either NULL or empty enter if block
        if (!sop.get("PatientID") ) {
            log.info("SOP: " + sop.get("SOPInstanceUID") + ", empty PatientID. Setting PatientID to: " + defaultPID);
            sop.set(PatientID, defaultPID);
        }
    }
}
