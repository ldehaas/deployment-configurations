def dfName = null;

def SHAHrt = "RIALTO_SHAH"
def Rialto_Cache_Test = "RIALTO_CACHE_T"; 
def TestDownAE = "TEST-DOWN";

def calledAET = getCalledAETitle().toUpperCase();


/* This method will check called AE and route the studies to the correct destinations. 
 */

switch (calledAET) {

                case Rialto_Cache_Test:
                        dfName = "etc/dataflows/rialto_cache_test.xml";
                        break

                case SHAHrt:
                        dfName = "etc/dataflows/rialto_shah_test.xml";
                        break

                case TestDownAE:
                        dfName = "etc/dataflows/test-down.xml";
                        break

                default:
                    dfName = "etc/dataflows/only-rialto-and-compress.xml";
}

log.info("Dataflow template found for modality " + getCallingAETitle()  + " with destination set to " + getCalledAETitle() + " is " + dfName);

/* return the dataflow template xml file name */
return dfName;
