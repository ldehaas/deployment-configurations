/**
*  This common class is used to provide access to the static map which holds the calling AET to
*  dataflow template xml file name entry.
*/
class DataflowModalityMapping {
    

    /**
     * This static method will return the file name of the dataflow template for a particular calling AET
     */
    static GetDataflowTemplete (callingAE) {
    
        def dfValue = null
        def pathPrefix = "etc/dataflows/"

        dfValue  = DataflowModalityMap[callingAE] 

        return pathPrefix.concat(dfValue); 
    }

    /**
     * This static variable holds the Map with the modality AETitles
     * as the key, and the dataflow template xml file name as the value.
     *
     * When new entries are added ensure proper syntax for a map. Format should be:
     *  'ae_title of modality'   colon   'name_of_dataflow_xml_file'  comma
     *
     * New entries should be added in alphabetic order.
     */
    private static final DataflowModalityMap = [
    'ACUS2000'         :  'pass-through-radiology.xml',
    'Amalga_Sender_1'  :  'pass-through-non-radiology.xml',
	'ANT115969'        :  'pass-through-radiology.xml',
    'ARCADISOR22056'   :  'pass-through-radiology.xml',
    'ARCHIVE'          :  'rialto_mgsh_rad.xml',
    'ARISTOS38'        :  'pass-through-radiology.xml',
    'AW-01'            :  'pass-through-radiology.xml',
    'AXIS01842'        :  'pass-through-radiology.xml',
    'CT64783'          :  'pass-through-radiology.xml',
    'ECAM'             :  'pass-through-radiology.xml',
    'EZ_PIC03'         :  'pass-through-non-radiology.xml',
	'GELUNAR'		   :  'rialto_mgsh_oth.xml', 
    'GHOPKINS2'        :  'pass-through-radiology.xml',
    'GIU221'           :  'pass-through-non-radiology.xml',
    'GIU222'           :  'pass-through-non-radiology.xml',
    'GOEC_9800'        :  'pass-through-radiology.xml',
    'GR2CAD'           :  'pass-through-radiology.xml',
    'GS_SIREST'        :  'pass-through-radiology.xml',
    'GSCR01'           :  'pass-through-radiology.xml',
    'GSCR02'           :  'pass-through-radiology.xml',
    'GSCR03'           :  'pass-through-radiology.xml',
    'GSCR04'           :  'pass-through-radiology.xml',
    'GSCR06'           :  'pass-through-radiology.xml',
    'GSCR07'           :  'pass-through-radiology.xml',
    'GSDR01'           :  'pass-through-radiology.xml',
	'GSLOGIQ1'         :  'rialto_mgsh_rad.xml', 
    'GSELENIA1'        :  'pass-through-radiology.xml',
    'GSEQ63764'        :  'pass-through-radiology.xml',
    'GSH_Apollo'       :  'pass-through-radiology.xml',
    'GSHARTIS01'       :  'pass-through-non-radiology.xml',
    'GSHINSITE01'      :  'pass-through-non-radiology-north-only.xml',
    'GSHINSITE02'      :  'pass-through-non-radiology-north-only.xml',
    'GSIDM01'          :  'pass-through-radiology.xml',
    'GSMRI'            :  'pass-through-radiology.xml',
    'GSMV30001'        :  'pass-through-radiology.xml',
    'GSMV30002'        :  'pass-through-radiology.xml',
    'GSN50547'         :  'pass-through-radiology.xml',
    'GSW50547'         :  'pass-through-radiology.xml',
    'GXRRM2'           :  'pass-through-radiology.xml',
    'MDROR'            :  'pass-through-radiology.xml',
    'MGIR'             :  'pass-through-radiology.xml',
    'MMWP46574'        :  'pass-through-radiology.xml',
    'OEM_StoreSCU'     :  'rialto_mgsh_oth.xml',
    'PSCANGCT'         :  'rialto_mgsh_rad.xml',
    'PSCANGCTED'       :  'rialto_mgsh_rad.xml',
    'PSCANGDRED'       :  'rialto_mgsh_rad.xml',
    'PSCANGDRMAIN'     :  'rialto_mgsh_rad.xml',
    'PSCANGDRMAIN1'    :  'rialto_mgsh_rad.xml',
    'PSCANGDRMOB'      :  'rialto_mgsh_rad.xml',
    'PSCANGDRPOB'      :  'rialto_mgsh_rad.xml',
    'PSCANGIR'         :  'rialto_mgsh_rad.xml',
    'PSCANGMA'         :  'rialto_mgsh_rad.xml',
    'PSCANGMRI'        :  'rialto_mgsh_rad.xml',
    'PSCANGNM'         :  'rialto_mgsh_rad.xml',
    'PSCANGNMFD'       :  'rialto_mgsh_rad.xml',
    'PSCANGUS1'        :  'rialto_mgsh_rad.xml',
    'PSCANGUS2'        :  'rialto_mgsh_rad.xml',
    'RIALTO_MGSH_CAR'  :  'rialto_mgsh.xml',
    'RIALTO_MGSH_OTH'  :  'rialto_mgsh_oth.xml',
    'RIALTO_CACHE'     :  'rialto_mgsh_rad.xml',
    'RIALTO'           :  'rialto_mgsh_rad.xml',
    'SAMSUNG'          :  'rialto_mgsh_rad.xml',
    'SonovaE'          :  'pass-through-non-radiology.xml',
    'US2003'           :  'pass-through-radiology.xml',
    'UMH_XCHANG'       :  'rialto_mgsh_rad.xml',
    'VIVID7-011277'    :  'pass-through-non-radiology.xml',
    'VIVID7-011716'    :  'pass-through-non-radiology.xml'    
    'BIOPTICS_DR'      :  'KHC-8965_gsh_faxitron.xml',          //I am going to add the last two calling AE tiltles in the prod.
    'RIALTO_FAXITRON'  :  'KHC-8965_gsh.xml'    /* last entry in map take note no comma at end */
    ]




}
