// This destination (McKesson's PACS) doesn't support SOPs of type 1.2.840.10008.5.1.4.1.1.88.33 (Comprehensive SR).
// We use this script to drop those SOP types, but forward the rest.
def sopClassUid = get(SOPClassUID)

if (sopClassUid == '1.2.840.10008.5.1.4.1.1.88.33') {
    log.debug("Not forwarding SOP {} because it has SOP Class UID {}", get(SOPInstanceUID), sopClassUid)
    return false
}

return true