/**
* This script will decide which dataflow template name to return.
* If the calling AET is McKesson Pacs then we will return the dataflow
* template file name that will route only to Rialto Cache.
* If however, its is not McKesson Pacs then we will return the dataflow
* template file name that will route to both Rialto Cache(compress)
* and McKesson Pacs (no compress).  
*/
/* This variable defines the name of the calling  McKesson AET */
def McKesson_Pacs_AET = "ALI_SCU"
def df_name = null;

if (getCallingAETitle().equalsIgnoreCase(McKesson_Pacs_AET)) {
    df_name = "etc/dataflows/only-rialto-and-compress.xml";
} else {
    df_name = "etc/dataflows/multiple-pass-through-and-compress.xml";
}

return df_name
