def df_name = "etc/dataflows/pass-through.xml"

def todayDate = new Date().format('EE')

if ((get(Modality).equalsIgnoreCase("MG")) && (get(StudyDescription).contains("SCRN")) || (get(StudyDescription).toLowerCase().contains("screen")) && (todayDate == "Mon" || todayDate == "Wed" || todayDate == "Fri") ) {
    df_name = "etc/dataflows/mg.xml"
log.info("routing.groovy: Using alternate workflow for modality: MG Description: SCRN/screen and day of week: '{}'", todayDate)
}

//khc5666, NM compression testing
def calling_AET = getCallingAETitle().toUpperCase();
if (calling_AET.equals("RIALTO_ROUTER_T2")) {
	log.info("This is a NM study from GUH01 router.")
	df_name = "etc/dataflows/nm.xml"
}

return df_name

