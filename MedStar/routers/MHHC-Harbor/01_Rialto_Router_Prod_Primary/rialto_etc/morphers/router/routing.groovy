/**
* This script will decide which dataflow template name to return.
*/

String dfName = "etc/dataflows/pass-through-and-compress.xml";

def Rialto_Cache_Prod = "RIALTO_CACHE";
def Rialto_Cache_Test = "RIALTO_CACHE_T"; 
def Rialto_Router_Test = "RIALTO_ROUTER_T";

def calledAET = getCalledAETitle().toUpperCase();

   /* This method will check called AE and route the studies to the correct destinations. 
    */

switch (calledAET) {

                case Rialto_Cache_Prod:
                        dfName = "etc/dataflows/rialto_cache_prod.xml";
                        break

                case Rialto_Cache_Test:
                        dfName = "etc/dataflows/rialto_cache_test.xml";
                        break

                case Rialto_Router_Test:
                        dfName = "etc/dataflows/rialto_router_test.xml";
                        break
                       
}

log.info("Dataflow template found for modality " + getCallingAETitle()  + " with destination set to " + getCalledAETitle() + " is " + dfName);

/* return the dataflow template xml file name */
return dfName;

