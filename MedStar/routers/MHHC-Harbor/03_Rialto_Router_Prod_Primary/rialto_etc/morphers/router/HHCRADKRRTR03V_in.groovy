s script handles the in-morphing for MHHC.
 *
 *  All objects sent will be stamped with the
 *  IPID value and various other values within the study.
 *  All objects will have the PatientID value checked, if null/empty
 *  set value. 
 */

/* load in the common class that loads the calling AET to dataflow template map  */
LOAD("dataflow_modality_mapping_common.groovy")

/* using the common class and the Get_Dataflow_templete method lookup the calling AET */
/* check to see if the lookup resulted in a null, if so then the calling aet was not found in the map hence return false. This will result in the instances being discarded */
if (DataflowModalityMapping.GetDataflowTemplete(getCallingAETitle()) == null) {
    log.info("in.groovy: Calling AET " + getCallingAETitle() +" not defined. SOP instances for this study will be discarded");
    return false
}


IPID ipid = new IPID(log, input)

ipid.set("HHC","2.16.840.1.114107.1.1.16.2.4","ISO")
ipid.setDefaultPatientID()

class IPID {
    def log = null;
    def sop = null;
    def defaultPID = "MSH-TEMP_ID";

    def IPID(log, sop) {
        this.log = log;
        this.sop = sop;
    }

    def set(namespace, universalid, universalidtype) {
        //location is a list of all Medstar IPID
        def location =["MEDSTAR","FSH","GSH","HHC","UMH","GUH","MGI","MSMHC","MSMH","WB4","6B4","MAS",”MPP”,”MSC”,”SHAH”];
        /*
        * It will check if the IPID is already stamped, if it is stamped by the following locations, 
        * do not re-stamp.
        */
        if(!location.contains(sop.get(IssuerOfPatientID))){
                if (sop.get(IssuerOfPatientID) != null || sop.get("IssuerOfPatientIDQualifiersSequence/UniversalEntityID") != null || sop.get("IssuerOfPatientIDQualifiersSequence/UniversalEntityIDType") != null) {
                        def curr_i = sop.get(IssuerOfPatientID)
                        def curr_u = sop.get("IssuerOfPatientIDQualifiersSequence/UniversalEntityID")
                        def curr_ut = sop.get("IssuerOfPatientIDQualifiersSequence/UniversalEntityIDType")
        
                        log.info("SOP: " + sop.get("SOPInstanceUID") + ", : Going to move existing issuer information [IussuerOfPatientID:  " + curr_i + ", UniversalEntityID: " + curr_u + ", UniversalEntityIDType: " + curr_ut + "] to a Karos specific private tag")

                        sop.set(0x00350010, "KAROS PRESERVED INFO 1.0", VR.LO)
                        sop.set(0x00351010, curr_i, VR.LO)
                        sop.set(0x00351011, curr_u, VR.LO)
                        sop.set(0x00351012, curr_ut, VR.LO)
                }

                log.info("SOP: " + sop.get("SOPInstanceUID") + ", setting namespace to: " + namespace);
                sop.set(IssuerOfPatientID, namespace)

                log.info("SOP: " + sop.get("SOPInstanceUID") + ", setting universal id to: " + universalid);
                sop.set("IssuerOfPatientIDQualifiersSequence/UniversalEntityID", universalid, VR.LO)

                log.info("SOP: " + sop.get("SOPInstanceUID") + ", setting universal id type to: " + universalidtype);
                sop.set("IssuerOfPatientIDQualifiersSequence/UniversalEntityIDType", universalidtype, VR.LO)

        }
        else{
                log.info("SOP: " + sop.get("SOPInstanceUID") + ", namespace: " + namespace + “ has not changed”);
                log.info("SOP: " + sop.get("SOPInstanceUID") + ", universal id: " + universalid + “ has not changed”);
                log.info("SOP: " + sop.get("SOPInstanceUID") + ", universal id type: " + universalidtype “ has not changed);
        }
    }


    /*
    * This method will check to see if the PatientID is either NULL or empty. If so then set the value 
    * of PatientID to the customer supplied string.
    */
    def setDefaultPatientID() {
        // if the PatientID is either NULL or empty enter if block
        if (!sop.get("PatientID") ) {
            log.info("SOP: " + sop.get("SOPInstanceUID") + ", empty PatientID. Setting PatientID to: " + defaultPID);
            sop.set(PatientID, defaultPID);
        }
    }
}

