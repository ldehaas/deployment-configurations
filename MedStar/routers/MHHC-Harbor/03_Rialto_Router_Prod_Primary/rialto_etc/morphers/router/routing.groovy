/**
*This groovy script is used to return the correct dataflow xml file name for a particular modality
* (calling ae Title).  
*
*
*
*
*/

/* load in the common class that loads the calling AET to dataflow template map  */
LOAD("dataflow_modality_mapping_common.groovy")

/* use the Get_Dataflow_templete method to lookup the calling AET */
String dfName = DataflowModalityMapping.GetDataflowTemplete(getCallingAETitle())

def MHHCAR = "RIALTO_MHH_CAR";
def MHHOTH = "RIALTO_MHH_OTH";
def MHHRAD = "RIALTO_MHH_RAD";
def MHHORTHO = "ORTHO_NORTH";

def Rialto_Cache_Prod = "RIALTO_CACHE";
def Rialto_Cache_Test = "RIALTO_CACHE_T"; 
def Rialto_Router_Test = "RIALTO_ROUTER_T";

def calledAET = getCalledAETitle().toUpperCase();
def modality = get(Modality);
def physicianName = get(0x00080090);
//def callingAET = getCallingAETitle().toUpperCase();
def pid = get(PatientID)
def physicianList = [
                     'CARBONE^JOHN JOSEPH',
                     'GRIFFITH^MARY E.',
                     'CURL^LEIGH ANN',
                     'MCGUIGAN^LAWRENCE A.',
                     'STANIEWSKI^STEPHEN J.',
                     'SIMMONS III^SHELTON',
                     'KAMINSKI^ELIZABETH M.']

    /* This method will check called AE and route the studies to the correct destinations. 
    */

switch (calledAET) {
                case MHHCAR:
                        dfName = "etc/dataflows/rialto_mhh_car.xml";
                        if  (modality.equalsIgnoreCase("NM")) {
                              dfName = "etc/dataflows/rialto_mhh_car_nm.xml";
                        }
                        break

                case MHHOTH:
                        dfName = "etc/dataflows/rialto_mhh_oth.xml";
                        if  (modality.equalsIgnoreCase("NM")) {
                              dfName = "etc/dataflows/rialto_mhh_oth_nm.xml";
                        }
                        break

                case MHHRAD:
                        dfName = DataflowModalityMapping.GetDataflowTemplete(getCallingAETitle());
                        break
                case Rialto_Cache_Prod:
                        dfName = "etc/dataflows/rialto_cache_prod.xml";
                        if  (modality.equalsIgnoreCase("NM")) {
                              dfName = "etc/dataflows/rialto_cache_prod_nm.xml";
                        }
                        break

                case Rialto_Cache_Test:
                        dfName = "etc/dataflows/rialto_cache_test.xml";
                        if  (modality.equalsIgnoreCase("NM")) {
                              dfName = "etc/dataflows/rialto_cache_test_nm.xml";
                        }
                        break

                case Rialto_Router_Test:
                        dfName = "etc/dataflows/rialto_router_test.xml";
                        break

                case MHHORTHO:
                        log.debug("in.groovy: processing called AET  {}. physicianName value is {} ", MHHORTHO, physicianName);
                        if (physicianList.contains(physicianName)){
                            dfName = "etc/dataflows/KHC8949_medstrat.xml";
                        }
                        else if (pid.startsWith("MPP")) {
                            dfName = "etc/dataflows/rialto_cache_test.xml";
                        } 
                        else{
                            dfName = "etc/dataflows/KHC8949_default.xml";
                        }
                        break 
}

log.info("Dataflow template found for modality " + getCallingAETitle()  + " is " + dfName);

/* return the dataflow template xml filename */
return dfName;

