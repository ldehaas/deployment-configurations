// This destination (ALI_STORE_SCP) doesn't support SOPs of type 1.2.840.10008.5.1.4.1.1.88.33 (Comprehensive SR) or 1.2.840.10008.5.1.4.1.1.88.3/Detail SR Storage - Trial (Retired).
// We use this script to drop those SOP types, but forward the rest.
def sopClassUid = get(SOPClassUID)

if (sopClassUid == '1.2.840.10008.5.1.4.1.1.88.33' || sopClassUid == '1.2.840.10008.5.1.4.1.1.88.3' || sopClassUid == '1.2.840.10008.5.1.4.1.1.88.22' ||  sopClassUid == '1.2.840.10008.5.1.4.1.1.88.67' || sopClassUid == '1.3.12.2.1107.5.9.1') {
    log.debug("Not forwarding SOP {} because it has SOP Class UID {}", get(SOPInstanceUID), sopClassUid)
    return false
}

return true