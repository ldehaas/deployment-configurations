medstar_amalga_devices.xml/**
*  This common class is used to provide access to the static map which holds the calling AET to
*  dataflow template xml file name entry.
*/
class DataflowModalityMapping {
    

    /**
     * This static method will return the file name of the dataflow template for a particular calling AET
     */
    static GetDataflowTemplete (callingAE) {
    
        def dfValue = null
        def pathPrefix = "etc/dataflows/"

        dfValue  = DataflowModalityMap[callingAE] 

        return pathPrefix.concat(dfValue); 
    }

    /**
     * This static variable holds the Map with the modality AETitles
     * as the key, and the dataflow template xml file name as the value.
     *
     * When new entries are added ensure proper syntax for a map. Format should be:
     *  'ae_title of modality'   colon   'name_of_dataflow_xml_file'  comma
     *
     * New entries should be added in alphabetic order.
     */
    private static final DataflowModalityMap = [
    'angi3d'          :  'rialto_mhh_rad.xml',
	'ALI_SCU'		  :  'rialto_mhh_radfromMcKesson.xml',
	'ALI_QUERY_SCU'	  :  'rialto_mhh_radfromMcKesson.xml',
	'ALI_QUERY_SCP'	  :  'rialto_mhh_radfromMcKesson.xml',
	'AN_CTAWP96312'   :  'rialto_mhh_rad.xml',
	'AN_MEDCOMNT204'  :  'rialto_mhh_rad.xml',
	'AWS_STORE160'	  :  'rialto_mhh_rad.xml',
	'APLIO_2'         :  'rialto_mhh_oth.xml',
	'ARTIS109038'     :  'rialto_mhh_rad.xml',
	'CT53112'         :  'rialto_mhh_rad.xml',
	'DYNACAD_DIAG'    :  'rialto_mhh_rad.xml',
	'ELEVA_RF'        :  'rialto_mhh_rad.xml',
	'EZ_PIC03'        :  'rialto_mhh_rad.xml',
	'GEVOLE10_1'      :  'rialto_mhh_oth.xml',	
	'GEVOLE10_2'      :  'rialto_mhh_oth.xml',
	'H_ICONS'         :  'rialto_mhh_rad_nm.xml',
	'HECAM1'          :  'rialto_mhh_rad_nm.xml',	
	'HECAM8350'       :  'rialto_mhh_rad_nm.xml',
	'HERCR_1'         :  'rialto_mhh_rad.xml',
	'HESOFTP1'        :  'rialto_mhh_rad_nm.xml',
	'HHCCAD_01'		  :  'rialto_mhh_rad.xml',
	'HHCCAD_02'		  :  'rialto_mhh_rad.xml',
	'HH_INTERA_01'    :  'rialto_mhh_rad.xml',
	'HH_iU22'         :  'rialto_mhh_rad.xml',
	'HH_IU22_2'       :  'rialto_mhh_rad.xml',
	'HH_IU22_3'       :  'rialto_mhh_rad.xml',
	'HHMR_SIEMENS'    :  'rialto_mhh_rad.xml',
	'HH_SS'           :  'rialto_mhh_rad.xml',
	'HHCMAMMO_DIM'    :  'rialto_mhh_rad.xml',
	'HHCMAMMO_SEL'    :  'rialto_mhh_rad.xml',
	'HHCT16-1'        :  'rialto_mhh_rad_ct.xml',
	'HHEDDR_1_STSCU'  :  'rialto_mhh_rad.xml',
	'HMAINCR_1'       :  'rialto_mhh_rad.xml',
	'HHOEC_9800_1'    :  'rialto_mhh_rad.xml',
	'HHOEC_9800_2'    :  'rialto_mhh_rad.xml',
	'HHOEC_9800_3'    :  'rialto_mhh_rad.xml',
	'HHOEC_9900_1'    :  'rialto_mhh_rad.xml',
	'HHOEC_9900_2'    :  'rialto_mhh_rad.xml',	
	'LEO12089'        :  'rialto_mhh_rad.xml',
	'medison'         :  'rialto_mhh_oth.xml',
	'MHH_CTMAIN_PAPER':  'rialto_mhh_rad_noamalga.xml',
	'MHH_EDCT_PAPER'   :  'rialto_mhh_rad_noamalga.xml',
	'MHH_EDXR_PAPER'   :  'rialto_mhh_rad_noamalga.xml',
	'MHH_IR_PAPER'     :  'rialto_mhh_rad_noamalga.xml',
	'MHH_MRMAIN_PAPER' :  'rialto_mhh_rad_noamalga.xml',
	'MHH_NM_PAPER'     :  'rialto_mhh_rad_noamalga.xml',
	'MHH_US_PAPER'     :  'rialto_mhh_rad_noamalga.xml',
	'MHH_XRMAIN_PAPER' :  'rialto_mhh_rad_noamalga.xml',
	'MMGR-ROUTER'      :  'rialto_mhh_radDCM4CHE.xml',
	'OEM_StoreSCU'    :  'rialto_mhh_rad.xml',
	'vf1'             :  'rialto_mhh_rad.xml'     /* last entry in map take note no comma at end */
    ]




}
