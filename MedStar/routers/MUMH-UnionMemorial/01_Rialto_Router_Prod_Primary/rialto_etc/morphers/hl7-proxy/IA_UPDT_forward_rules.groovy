log.info("Start IA Update Morpher")

def messageType = get('/.MSH-9-1')
def triggerEvent = get('/.MSH-9-2')

log.info("messageType is: {}", messageType)
log.info("triggerEvent is: {}", triggerEvent)

if (messageType != null && triggerEvent != null) {
    if ("ADT".equals(messageType) && ("A08".equals(triggerEvent) || "A40".equals(triggerEvent) || "A31".equals(triggerEvent) || "A39".equals(triggerEvent))) {
        log.info("will forward message to IA Update")
    } else {
        log.info("will not forward to IA Update")
        return false
    }
}

log.info("End IA Update Morpher")