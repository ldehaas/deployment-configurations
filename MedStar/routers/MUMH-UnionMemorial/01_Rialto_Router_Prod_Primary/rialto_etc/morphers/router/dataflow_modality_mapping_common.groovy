/**
*  This common class is used to provide access to the static map which holds the calling AET to
*  dataflow template xml file name entry.
*/
class DataflowModalityMapping {


    /**
     * This static method will return the file name of the dataflow template for a particular calling AET
     */
    static GetDataflowTemplete (callingAE) {

        def dfValue = null
        def pathPrefix = "etc/dataflows/"

        dfValue  = DataflowModalityMap[callingAE]
        
        /* check to make sure dfValue is not null before we use concat */
        if (dfValue) {
           dfValue = pathPrefix.concat(dfValue);
        }

        return dfValue;
    }

    /**
     * This static variable holds the Map with the modality AETitles
     * as the key, and the dataflow template xml file name as the value.
     *
     * When new entries are added ensure proper syntax for a map. Format should be:
     *  'ae_title of modality'   colon   'name_of_dataflow_xml_file'  comma
     *
     * New entries should be added in alphabetic order.
     */
    private static final DataflowModalityMap = [
    'AN_ESOFT7092'      :  'pass-through-radiology.xml',
    'AN_MRC22753'       :  'pass-through-radiology.xml',
    'ANT115969'         :  'pass-through-radiology.xml',
    'AXIS05843'         :  'rialto_mumh_rad.xml',
    'BIOPTICS_DR'       :  'rialto_mumh_rad.xml',
    'BSR07002'          :  'rialto_mumh_rad.xml',
    'CT66952'           :  'pass-through-radiology.xml',
    'DRX_PORT_1'        :  'pass-through-radiology.xml',
    'DRX_PORT3'         :  'pass-through-radiology.xml',
    'DRX_RM1'           :  'pass-through-radiology.xml',
    'DRXCLIN4'          :  'pass-through-radiology.xml',
    'ESOFTP'            :  'pass-through-radiology.xml',
    'GELUNAR'           :  'rialto_mumh_rad.xml',
    'ICON1'             :  'pass-through-radiology.xml',
    'IDM2'              :  'pass-through-radiology.xml',
    'IE3302X3QJ'        :  'rialto_mumh_car.xml',
    'indexQuery-P0H2'   :   'pass-through-radiology-north-only.xml',
    'INJECTOR_1'        :  'rialto_mumh_rad_wIDM.xml',
    'INJECTOR_2'        :  'rialto_mumh_rad_wIDM.xml',
    'LEO6905'           :  'pass-through-radiology.xml',
    'MDRGAR1'           :  'rialto_mumh_rad.xml',
    'MMWP7631'          :  'pass-through-radiology.xml',
    'OEC_985'           :  'rialto_mumh_rad.xml',
    'OEC_9900_1'        :  'rialto_mumh_rad.xml',
    'OEC_9900_2'        :  'rialto_mumh_rad.xml',
    'OEC_9900_3'        :  'rialto_mumh_rad.xml',
    'OEC_9900_4'        :  'rialto_mumh_rad.xml',
    'OEC_9900_5'        :  'rialto_mumh_rad.xml',
    'OH_IE33_1'         :  'rialto_mumh_car.xml',
    'PHILIU22_US1'      :  'pass-through-radiology.xml',
    'PHILIU22_US2'      :  'pass-through-radiology.xml',
    'PHILIU22_US3'      :  'pass-through-radiology.xml',
    'PR_NAV100258'      :  'pass-through-radiology.xml',
    'PSCANUCT'          :  'rialto_mumh_rad.xml',
    'PSCANUCT2'         :  'rialto_mumh_rad.xml',
    'PSCANUDR1'         :  'rialto_mumh_rad.xml',
    'PSCANUDR2'         :  'rialto_mumh_rad.xml',
    'PSCANUDR3'         :  'rialto_mumh_rad.xml',
    'PSCANUDR4'         :  'rialto_mumh_rad.xml',
    'PSCANUDR5'         :  'rialto_mumh_rad.xml',
    'PSCANUDR6'         :  'pass-through-radiology.xml',
    'PSCANUIR'          :  'rialto_mumh_rad.xml',
    'PSCANUMA'          :  'rialto_mumh_rad.xml',
    'PSCANUMR'          :  'rialto_mumh_rad.xml',
    'PSCANUNM'          :  'rialto_mumh_rad.xml',
    'PSCANURF'          :  'rialto_mumh_rad.xml',
    'PSCANUUS'          :  'rialto_mumh_rad.xml',
    'RIALTO_MUMH_OTH'   :  'rialto_mumh_oth.xml',
    'RIALTO_MUMH_CAR'   :  'rialto_mumh_car.xml',
    'RIALTO_MUMH_RAD'   :  'rialto_mumh_rad.xml',
    'THIN1'             :  'rialto_mumh_rad.xml',
    'THIN2'             :  'rialto_mumh_rad.xml',
    'U975_1'            :  'pass-through-radiology.xml',
    'UAEGISLAPTOP'      :  'rialto_mumh_rad.xml',
    'UCYSTO'            :  'rialto_mumh_rad.xml',
    'UDYNACAD_SRV'      :  'rialto_mumh_rad.xml',
    'UFLMV'             :  'pass-through-radiology.xml',
    'UM_JPB_SP'         :  'pass-through-radiology.xml',
    'UM_RM2'            :  'pass-through-radiology.xml',
    'UM_RM3_SCU'        :  'pass-through-radiology.xml',
    'UM_RM4_STSCU'      :  'pass-through-radiology.xml',
    'UMAM850_2'         :  'pass-through-radiology.xml',
    'UMH_CAD01'         :  'pass-through-radiology.xml',
    'UMH_DIGI01'        :  'pass-through-radiology.xml',
    'UMH_DM01'          :  'pass-through-radiology.xml',
    'UMH_DM02'          :  'pass-through-radiology.xml',
    'UMH_DWS01'         :  'pass-through-radiology.xml',
	'UMH_ED_Sono01'     :  'pass-through-radiology.xml',
	'UMH_ED_Sono02'     :  'pass-through-radiology.xml',
    'UMH_STEREO'        :  'pass-through-radiology.xml',
    'UMH_XCHANG'        :  'pass-through-radiology.xml',
    'UMHINNOVA4'        :  'pass-through-radiology.xml',
    'UMHJET1'           :  'pass-through-radiology.xml',
    'UMHJET2'           :  'pass-through-radiology.xml',
    'UMHJET3'           :  'pass-through-radiology.xml',
    'UMHLILA'           :  'pass-through-radiology.xml',
    'USKY'              :  'pass-through-radiology.xml',
    'Venue40_01'        :  'rialto_mumh_oth.xml',
    'Venue40_02'        :  'rialto_mumh_oth.xml',
    'Venue50_03'        :  'rialto_mumh_oth.xml'      /* last entry in map take note no comma at end */
    ]

}

