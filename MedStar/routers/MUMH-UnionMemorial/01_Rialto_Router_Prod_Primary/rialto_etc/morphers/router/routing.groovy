/**
*This groovy script is used to return the correct dataflow xml file name for a particular modality
* (calling ae Title).
*
*
*
*
*/

/* load in the common class that loads the calling AET to dataflow template map  */
LOAD("dataflow_modality_mapping_common.groovy")

/* use the Get_Dataflow_templete method to lookup the calling AET */
String dfName = DataflowModalityMapping.GetDataflowTemplete(getCallingAETitle())

/* KHC5171 - change from Sandy to ensure ones from 'indexQuery-P0H2' only goes to 'ERS_RAD_NORTH' */
/*  need to ensure dfName is not null if so then do compare operation */
if ( dfName && dfName.equals("etc/dataflows/pass-through-radiology-north-only.xml") ) {
    return dfName;
}

def MUMHCAR = "RIALTO_MUMH_CAR";
def MUMHOTH = "RIALTO_MUMH_OTH";
def MUMHRAD = "RIALTO_MUMH_RAD";
def RIALTOPACS = "RIALTO_PACS";
def RIALTOnoAMALGA = "RIALTO_NOAMALGA";
def RIALTOFAXITRON = "RIALTO_FAXITRON";
def RIALTO3DR = "RIALTO_3DR";

def Rialto_Cache_Prod = "RIALTO_CACHE";
def Rialto_Cache_Test = "RIALTO_CACHE_T"; 
def Rialto_Router_Test = "RIALTO_ROUTER_T";

def calledAET = getCalledAETitle().toUpperCase();
def modality = get(Modality);

    /*
    * This method will check called AE and route the studies to the correct destinations. 
    */

switch (calledAET) {
                case MUMHCAR:
                        dfName = "etc/dataflows/rialto_mumh_car.xml";
                        if  (modality.equalsIgnoreCase("NM")) { 
                              dfName = "etc/dataflows/rialto_mumh_car_nm.xml";
                        }      
                        break

                case MUMHOTH:
                        dfName = "etc/dataflows/rialto_mumh_oth.xml";
                        if  (modality.equalsIgnoreCase("NM")) {    
                              dfName = "etc/dataflows/rialto_mumh_oth_nm.xml";
                        }
                        break

                case MUMHRAD:
                        dfName = "etc/dataflows/rialto_mumh_rad.xml";
                        if  (modality.equalsIgnoreCase("NM")) {
                              dfName = "etc/dataflows/rialto_mumh_rad_nm.xml";
                        }
                        break

                case RIALTOPACS:
                        dfName = "etc/dataflows/rialto_mumh_rad_wIDM.xml";
                        if  (modality.equalsIgnoreCase("NM")) {
                              dfName = "etc/dataflows/rialto_mumh_rad_wIDM_nm.xml";
                        }
                        break

                case RIALTOnoAMALGA:
                        dfName = "etc/dataflows/rialto_noamalga.xml";
                        if  (modality.equalsIgnoreCase("NM")) {
                              dfName = "etc/dataflows/rialto_noamalga_nm.xml";
                        }
                        break

                case RIALTOFAXITRON:
                        dfName = "etc/dataflows/rialto_faxitron.xml";
                        if  (modality.equalsIgnoreCase("NM")) {
                              dfName = "etc/dataflows/rialto_faxitron_nm.xml";
                        }
                        break
                //add as KHC7548
                case "RIALTO_MAMMO":
                        dfName = "etc/dataflows/rialto_mammo.xml";
                        if  (modality.equalsIgnoreCase("NM")) {
                              dfName = "etc/dataflows/rialto_mammo_nm.xml";
                        }
                        break

                case Rialto_Cache_Prod:
                        dfName = "etc/dataflows/rialto_cache_prod.xml";
                        if  (modality.equalsIgnoreCase("NM")) {
                              dfName = "etc/dataflows/rialto_cache_prod_nm.xml";
                        }
                        break

                case Rialto_Cache_Test:
                        dfName = "etc/dataflows/rialto_cache_test.xml";
                        if  (modality.equalsIgnoreCase("NM")) {
                              dfName = "etc/dataflows/rialto_cache_test_nm.xml";
                        }
                        break

                case Rialto_Router_Test:
                        dfName = "etc/dataflows/rialto_router_test.xml";
                        break
}

log.info("Dataflow template found for modality " + getCallingAETitle()  + " is " + dfName);

/* return the dataflow template xml file name */
return dfName;

