import org.dcm4che2.data.BasicDicomObject;
import com.karos.groovy.morph.dcm.DicomScriptingAPI;

log.debug("Processing order update script.\n {}", input)
def publishManifestRequired = false

def updateValueInManifestIfRequired = { tag, originalValue, newValue ->
    if (originalValue != newValue) {
        set(tag, newValue)
        publishManifestRequired = true
    }
}

if (output.get(SpecificCharacterSet) == null) {
   set(SpecificCharacterSet, "ISO_IR 100")
}

updateValueInManifestIfRequired(
    ReferringPhysicianName,
    output.get(ReferringPhysicianName),
    get('ORC-12-2') + '^' + get('ORC-12-3'))

updateValueInManifestIfRequired(
    "ReferringPhysicianIdentificationSequence/PersonIdentificationCodeSequence/CodeValue", 
    output.get("ReferringPhysicianIdentificationSequence/PersonIdentificationCodeSequence/CodeValue"), 
    get('ORC-12-1'))

updateValueInManifestIfRequired(
    StudyDescription, 
    output.get(StudyDescription), 
    get('OBR-4-2'))

updateValueInManifestIfRequired("AnatomicRegionSequence/CodeValue", 
    output.get("AnatomicRegionSequence/CodeValue"), 
    get("ORC-15-4"))

updateValueInManifestIfRequired(
    "AnatomicRegionSequence/CodingSchemeDesignator", 
    output.get("AnatomicRegionSequence/CodingSchemeDesignator"), 
    "Imagerie Qu\u00e9bec-DSQ")

updateValueInManifestIfRequired(
    "AnatomicRegionSequence/CodeMeaning", 
    output.get("AnatomicRegionSequence/CodeMeaning"), 
    "Code de r\u00e9gion anatomique")

updateValueInManifestIfRequired(
    ReasonForStudy, 
    output.get(ReasonForStudy), 
    get("OBR-31"))

updateValueInManifestIfRequired(
    "RequestedProcedureCodeSequence/CodeValue",
    output.get("RequestedProcedureCodeSequence/CodeValue"),
    get('OBR-4-1'))

updateValueInManifestIfRequired(
    "RequestedProcedureCodeSequence/CodingSchemeDesignator",
    output.get("RequestedProcedureCodeSequence/CodingSchemeDesignator"),
    get('OBR-4-1'))

updateValueInManifestIfRequired(
    "RequestedProcedureCodeSequence/CodeMeaning", 
    output.get("RequestedProcedureCodeSequence/CodeMeaning"), 
    get('OBR-4-2'))

updateValueInManifestIfRequired(
    "RequestedProcedureCodeSequence/CodeMeaning", 
    output.get("RequestedProcedureCodeSequence/CodeMeaning"), 
    get('OBR-4-2'))

updateValueInManifestIfRequired(
    "CurrentRequestedProcedureEvidenceSequence/ReferringPhysicianName", 
    output.get("CurrentRequestedProcedureEvidenceSequence/ReferringPhysicianName"), 
    get('ORC-12-2') + '^' + get('ORC-12-3'))

updateValueInManifestIfRequired(
    "CurrentRequestedProcedureEvidenceSequence/StudyDescription", 
    output.get("CurrentRequestedProcedureEvidenceSequence/StudyDescription"), 
    get('OBR-4-2'))

log.debug("Done processing order update script. Current Manifest:\n {}", output)

if (publishManifestRequired && output.get(NumberOfStudyRelatedInstances) != null) {
    log.debug("Order processing script indicating that publish is required")
    return true

} else if (publishManifestRequired && output.get(NumberOfStudyRelatedInstances) == null) {
    log.debug("Order processing script updated the manifest, but there are no images so publishing not required.")
    return false

} else {
    log.debug("Order processing script indicating that publish is not required")
    return false

}