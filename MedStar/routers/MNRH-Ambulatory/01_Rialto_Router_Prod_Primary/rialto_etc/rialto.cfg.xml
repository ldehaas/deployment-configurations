<?xml version="1.0"?>
<config>

    <!-- ################################################################## -->
    <!--                                                                    -->
    <!--  Rialto Configuration                                              -->
    <!--                                                                    -->
    <!-- ################################################################## -->

    <!-- ################################################################## -->
    <!--  VARIABLES                                                         -->
    <!-- ################################################################## -->

    <var name="HostIP" value="172.27.6.56" />

    <!-- AUDIT RECORD REPOSITORY VARIABLES -->
    <var name="ARR-Protocol" value="udp" />
    <var name="ARR-GUIPort" value="7070" />
    <var name="ARR-Port" value="4000" />
    <var name="ARR-Host" value="localhost" />

    <!-- Rialto Cache VARIABLES -->
    <var name="RialtoCache-AETitle" value="ERS_RAD_SOUTH" />
    <var name="RialtoCache-HostIP" value="172.27.6.17" />
    <var name="RialtoCache-HostPort" value="4104" />

    <!-- SYSTEM COMMON VARIABLES -->
    <var name="System-AffinityDomain" value="EMPI_CROSS_AFFINITY_DOMAIN_ID" />
    <var name="System-AffinityNamespace" value="RIALTO" />
    <var name="System-DefaultLocalDomain" value="county.general" />
    <var name="System-DefaultNamespace" value="CG" />
    <var name="System-UniversalIDType" value="ISO" />
    <var name="System-DefaultLocalFullyQualifiedDomain" value="${System-DefaultNamespace}&amp;${System-DefaultLocalDomain}&amp;${System-UniversalIDType}" />
    <var name="System-AffinityFullyQualifiedDomain" value="${System-AffinityNamespace}&amp;${System-AffinityDomain}&amp;${System-UniversalIDType}" />

    <!-- DICOM Router -->
    <var name="DicomRouter-DicomPort" value="4104" />
    <var name="DicomRouter-DatabaseURL" value="jdbc:postgresql://localhost/dicomrouter?user=rialto"/>
    <var name="UserManagement-DatabaseURL" value="jdbc:postgresql://localhost/navigatordb?user=rialto"/>

    <!-- DATABASE URLs: "DatabaseURL" parameter name is used by the navigator-security script, do not change the parameter name  -->
    <var name="DatabaseURL" value="jdbc:postgresql://localhost/navigatordb?user=rialto"/>

    <!-- NAVIGATOR VARIABLES -->
    <var name="Navigator-Host" value="${HostIP}"/>
    <var name="Navigator-HostProtocol" value="https" />
    <var name="Navigator-GUIPort" value="2525" />
    <var name="Navigator-APIPort" value="2524" />
    <var name="Navigator-RootPath" value="/rest" />
    <var name="Navigator-APIBasePath" value="/rialto" />
    <var name="Navigator-GUIBasePath" value="/" />
    <var name="Navigator-DefaultLocale" value="en" />
    <var name="Navigator-DefaultTheme" value="rialto-light" />
    <var name="Navigator-MaxPatientSearchResults" value="1000" />
    <var name="Navigator-MaxDocumentSearchResults" value="1000" />

    <!-- USERMANAGEMENT VARIABLES -->
    <var name="Usermanagement-APIBasePath" value="/usermanagement" />
    <var name="Usermanagement-URL" value="${Navigator-HostProtocol}://${Navigator-Host}:${Navigator-APIPort}${Navigator-RootPath}" />
    <var name="Usermanagement-AuthenticatedURL" value="${Navigator-HostProtocol}://${Navigator-Host}:${Navigator-APIPort}${Navigator-RootPath}/api${Usermanagement-APIBasePath}" />

    <!-- CLUSTER CONFIGURATION -->
    <!-- Single Sign On in a Cluster -->
    <var name="SSO-Cluster-Bind-IP" value="127.0.0.1"/>
    <var name="SSO-Cluster-Multicast-IP" value="232.10.10.201"/>
    <var name="SSO-Cluster-Multicast-PORT" value="45201"/>

    <!-- Navigator Cache Manager in a cluster -->
    <var name="Navigator-Cluster-Bind-IP" value="127.0.0.1"/>
    <var name="Navigator-Cluster-Multicast-IP" value="232.10.10.202"/>
    <var name="Navigator-Cluster-Multicast-PORT" value="45202"/>

    <!-- Navigator Server access controller in a cluster -->
    <var name="Navigator-Server-Cluster-Bind-IP" value="127.0.0.1"/>
    <var name="Navigator-Server-Cluster-Multicast-IP" value="232.10.10.203"/>
    <var name="Navigator-Server-Cluster-Multicast-PORT" value="45203"/>

    <!-- TLS CONFIGURATION -->
    <var name="TLSKeyStore" value="sample.jks" />
    <var name="TLSKeyStorePass" value="password" />
    <var name="TLSKeyPass" value="password" />
    <var name="TLSTrustStore" value="sample.jks" />
    <var name="TLSTrustStorePass" value="password" />
    <var name="TLSClientAuth" value="false" />
    <var name="TLSV2Hello" value="true" />

    <!-- User Sessions -->
    <var name="IdleUserSessionTimeout" value="30m" />

    <!-- Database Backup -->
    <var name="BackupRemotePath" value="/home/rialto/rialto/var/database-backups" />
    <var name="BackupMaxSnapshots" value="5" />

    <!-- ################################################################## -->
    <!--  SERVERS                                                           -->
    <!-- ################################################################## -->
    <server type="http" id="authenticated-http">
        <port>${Navigator-APIPort}</port>
        <tls>
            <keystore>${TLSKeyStore}</keystore>
            <keystorepass>${TLSKeyStorePass}</keystorepass>
            <keypass>${TLSKeyPass}</keypass>
            <truststore>${TLSTrustStore}</truststore>
            <truststorepass>${TLSTrustStorePass}</truststorepass>
            <clientauth>${TLSClientAuth}</clientauth>
            <v2hello>${TLSV2Hello}</v2hello>
        </tls>
    </server>

     <server type="http" id="audit-http">
            <port>${ARR-GUIPort}</port>
            <tls>
                <keystore>${TLSKeyStore}</keystore>
                <keystorepass>${TLSKeyStorePass}</keystorepass>
                <keypass>${TLSKeyPass}</keypass>
                <truststore>${TLSTrustStore}</truststore>
                <truststorepass>${TLSTrustStorePass}</truststorepass>
                <clientauth>${TLSClientAuth}</clientauth>
                <v2hello>${TLSV2Hello}</v2hello>
           </tls>
    </server>

    <server type="http" id="navigator-http">
        <port>${Navigator-GUIPort}</port>
        <tls>
            <keystore>${TLSKeyStore}</keystore>
            <keystorepass>${TLSKeyStorePass}</keystorepass>
            <keypass>${TLSKeyPass}</keypass>
            <truststore>${TLSTrustStore}</truststore>
            <truststorepass>${TLSTrustStorePass}</truststorepass>
            <clientauth>${TLSClientAuth}</clientauth>
            <v2hello>${TLSV2Hello}</v2hello>
        </tls>
    </server>

    <server type="http" id="router-monitoring" >
        <port>13337</port>
    </server>
    
    <server type="dicom" id="router-dicom">
        <port>${DicomRouter-DicomPort}</port>
    </server>

    <!-- ################################################################## -->
    <!--  SERVICES                                                          -->
    <!-- ################################################################## -->

    <!-- AUDIT RECORD REPOSITORY SERVICE -->

   <service id="arr" type="arr">


        <server idref="audit-http" name="arr-api">
            <url>/auditRecords/*</url>
        </server>

        <server idref="audit-http" name="arr-index-backup-api">
            <url>/auditRecordsIndexBackup/*</url>
        </server>

        <config>
            <prop name="AuthEnabled" value="true" />
            <prop name="IndexDir" value="${rialto.rootdir}/var/arr/index" />
            <prop name="IndexBackupDir" value="${rialto.rootdir}/var/arr/index-backup" />
            <prop name="ArchiveDir" value="${rialto.rootdir}/var/arr/archive" />
            <prop name="TempDir" value="${rialto.rootdir}/var/arr/temp" />
            <prop name="Listener" value="tcp:4000" />
            <prop name="Protocol" value="tcp" />
            <prop name="Port" value="${ARR-GUIPort}" />
        </config>
    </service>

    <!-- NAVIGATOR (Including Usermanagement and plugins) -->
    <include location="navigator/navigator.xml"/>

   <service id="dicom-router" type="dicom-router">
        <server idref="router-dicom" name="incoming" />
        <server idref="router-monitoring" name="MonitoringApi">
            <url>/monitoring/*</url>
        </server>

        <config>
            <prop name="DatabaseURL">
                ${DicomRouter-DatabaseURL}
            </prop>

            <prop name="InboxStorageLocation">
                ${rialto.rootdir}/var/router/
            </prop>

            <prop name="RoutingRulesScript">
                ${rialto.rootdir}/etc/morphers/router/routing.groovy
            </prop>

            <prop name="InboundMorphingScript">
                ${rialto.rootdir}/etc/morphers/router/in.groovy
            </prop>

            <prop name="NumberOfProcessors">10</prop>

           <!--Fix for CAPA-19 per KHC-8389 -->
            <prop name="CStoreSCPTransferCapabilities">
               <disable>
                   <transferSyntax uid="1.2.840.10008.1.2.2" />
               </disable>
            </prop>

        </config>
    </service>

    <!-- ################################################################## -->
    <!--  DEVICES                                                           -->
    <!-- ################################################################## -->

    <device type="dicom" id="rialto-cache">
        <host>${RialtoCache-HostIP}</host>
        <port>${RialtoCache-HostPort}</port>
        <aetitle>${RialtoCache-AETitle}</aetitle>
        <domain>1.2.3</domain>
    </device>
    <device id="rialto-cache-prod" type="dicom">
        <host>172.27.6.17</host>
        <port>4104</port>
        <aetitle>RIALTO_CACHE</aetitle>
        <domain>1.2.3</domain>
    </device>
    <device type="dicom" id="test-receiver">
         <host>localhost</host>
         <port>6104</port>
         <aetitle>TEST-RECEIVER</aetitle>
         <domain>1.2.3</domain>
    </device>
    <device id="auditDevice" type="audit">
       <transport>${ARR-Protocol}</transport>
       <host>${ARR-Host}</host>
       <port>${ARR-Port}</port>
    </device>
    <device id="rialto-cache-test" type="dicom">
         <host>198.50.67.50</host>
         <port>4104</port>
         <aetitle>RIALTO_CACHE_T</aetitle>
    </device>
    <device id="rialto-router-test" type="dicom">
         <host>172.16.203.79</host>
         <port>4104</port>
         <aetitle>RIALTO_ROUTER_T</aetitle>
    </device>
	<device id="DYNACAD_MRN" type="dicom">
         <host>10.125.129.102</host>
         <port>1602</port>
         <aetitle>DYNACAD_MRN</aetitle>
    </device>
    <!-- Include Medstar Amalga devices --> 
    <include location="medstar_amalga_devices.xml" />

</config>
