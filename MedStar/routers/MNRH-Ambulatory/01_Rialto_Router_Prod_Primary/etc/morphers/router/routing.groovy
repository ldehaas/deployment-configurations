//return "etc/dataflows/pass-through-and-compress.xml"
//return "etc/dataflows/pass-through.xml"
//return "etc/dataflows/multiple-destinations.xml"
//return "etc/dataflows/decompress.xml"

/**
*This groovy script is used to return the correct dataflow xml file name for a particular modality
* (calling ae Title).  
*
*
*
*
*/

String dfName = "etc/dataflows/ERSrad.xml";

def Rialto_Cache_Prod = "RIALTO_CACHE";
def Rialto_Cache_Test = "RIALTO_CACHE_T"; 
def Rialto_Router_Test = "RIALTO_ROUTER_T";
def GMOIrt = "RIALTO_GMOI_BRDY";
def GMOILTrt = "RIALTO_GMOI_LORT";
def GMOIWBrt = "RIALTO_GMOI_WOOD";
def GMOIALrt = "RIALTO_GMOI_ALEX";
def GMOIALPCrt = "RIALTO_GMOI_ALPC";
def GMOIWDrt = "RIALTO_GMOI_WALD";
def SHAHrt = "RIALTO_SHAH";
def OIAdynacad = "OIADYNACAD_AMBRT"
def MRNHYATTPACS = "MRN-HYATT-PACS"
def MRNLAFPACS = "MRN-LAF-PACS"
def MRNBELAIRPACS = "MRN-BELAIR-PACS"
def LAFAYETTEHOLOGIC = "MSLFAW01"
def iPIDrt = get(0x00100021);
def calledAET = getCalledAETitle().toUpperCase();
def callingAET = getCallingAETitle().toUpperCase();
def modality = get(Modality);
def sopclassUID = get(0x00080016);

def UNKrt = "UNKNOWN";
def GSHrt = "GSH";
def WHCrt = "WHC";
def WBrt = "WB4";
def NBrt = "6B4";
def HHCrt = "HHC";
def GUHrt = "GUH";
def FSHrt = "FSH";
def UMHrt = "UMH";
def MMCrt = "MMC";
def SMHrt = "SMH";
def SMCrt = "SMC";
def MPPrt = "MPP";
def MSCrt = "MSC";
def MSMHCrt = "MSHMC";
def MSMHrt = "MSMH";

   /* Check if CallingAE is Lafayette Hologic, if so route to ERS South and to GUH and WHC Hologic. 
    */

if (callingAET.equals(LAFAYETTEHOLOGIC)) {
	dfName = "etc/dataflows/ERSrad_whchologic_guhhologic.xml";
	}	

if (calledAET.contains(GMOI)) {
	dfName = "etc/dataflows/ERSoth.xml";
	}	
	
if (get(BodyPartExamined) != null && get(BodyPartExamined).contains("Abdomen") && modality.equalsIgnoreCase("MR")) {
	if (modality.equalsIgnoreCase("MR")) {
                            dfName = "etc/dataflows/ERSrad_TERARECON.xml";
                            }
						if (get(StudyDescription) != null ) {
							if ((get(StudyDescription).contains("MR HEPATOBILIARY"))) {
							dfName = "etc/dataflows/ERSrad_Aquarius.xml";
							}
						}
	}
	
   /* This method will check called AE and route the studies to the correct destinations. 
    */

switch (calledAET) {

                case Rialto_Cache_Prod:
                        dfName = "etc/dataflows/rialto_cache_prod.xml";
                        break

                case Rialto_Cache_Test:
                        dfName = "etc/dataflows/rialto_cache_test.xml";
                        break

                case Rialto_Router_Test:
                        dfName = "etc/dataflows/rialto_router_test.xml";
                        break
                       						
                case SHAHrt:
                        dfName = "etc/dataflows/ERSrad.xml";
                        if (modality.equalsIgnoreCase("CT")) {
                            dfName = "etc/dataflows/ERSrad_TERARECON.xml";
                            }
						if (get(StudyDescription) != null ) {
							if ((get(StudyDescription).contains("MR HEPATOBILIARY"))) {
							dfName = "etc/dataflows/ERSrad_Aquarius.xml";
							}
						}
                        break
						
				case OIAdynacad:
						dfName = "etc/dataflows/OIAdynacad.xml";
						break

				case MRNHYATTPACS:
						dfName = "etc/dataflows/rialto_MRN-HYATT-PACSonly.xml";
						break
						
				case MRNLAFPACS:
						dfName = "etc/dataflows/rialto_MRN-LAF-PACSonly.xml";
						break
						
				case MRNBELAIRPACS:
						dfName = "etc/dataflows/rialto_MRN-BELAIR-PACSonly.xml";
						break
}

log.info("Dataflow template found for modality " + getCallingAETitle()  + " with destination set to " + getCalledAETitle() + " is " + dfName);

/* return the dataflow template xml file name */
return dfName;


