/**
 * This script handles the in-morphing for Bel Air 
 *
 * This site handles studies for various sites, so the IPID is the first 3 characters
 * of the patientID.  We will inspect these characters to determine what to stamp for
 * the IPID values.
 *
 */

import java.security.MessageDigest;

IPID ipid = new IPID(log, input)

ipid.checkIPID(getCalledAETitle());

class IPID {

    def log = null;
    def sop = null;
    def defaultPID = "MSH-TEMP_ID";
    def mrnBA = "RIALTO_MRN_BA";
    def mrnHY = "RIALTO_MRN_HYAT"; 
    def mrnBR = "RIALTO_MRN_BRDY";
    def mrnLF1 = "RIALTO_MRN_LFTE1";
    def mrnLF2 = "RIALTO_MRN_LFTE2";
    def mrnMI = "RIALTO_MRN_MITCH";
    def mrnTI = "RIALTO_MRN_TIMO";
    def mrnCC = "RIALTO_MRN_CHEVY";
    def gmoiBR = "RIALTO_GMOI_BRDY";
    def gmoiLT = "RIALTO_GMOI_LORT";
    def gmoiWB = "RIALTO_GMOI_WOOD";
    def gmoiAL = "RIALTO_GMOI_ALEX";
    def gmoiWD = "RIALTO_GMOI_WALD";
    def masOT = "RIALTO_MAS_OTH";
    def mrnSHAH = "RIALTO_SHAH";
    //KHC6121 location is a list of all Medstar IPID
    def sopIPID = sop.get(IssuerOfPatientID);
    def locationMap = ['MEDSTAR' : '2.16.840.1.114107.1.1.16.2.5', 
                        'GSH' : '2.16.840.1.114107.1.1.16.2.2',
                        'WB4' : '2.16.840.1.114107.1.1.16.2.3',
                        'HHC' : '2.16.840.1.114107.1.1.16.2.4',
                        'GUH' : '2.16.840.1.114107.1.1.16.2.6',
                        'FSH' : '2.16.840.1.114107.1.1.16.2.7',
                        'UMH' : '2.16.840.1.114107.1.1.16.2.8',
                        '6B4' : '2.16.840.1.114107.1.1.16.2.9',
                        'MGI' : '2.16.840.1.114107.1.1.16.2.10',
                        'MSMH' : '2.16.840.1.114107.1.1.16.2.11',
                        'MSMHC' : '2.16.840.1.114107.1.1.16.2.12',
                        'MAS' : '2.16.840.1.114107.1.1.16.2.13',
                        'MPP' : '2.16.840.1.114107.1.1.16.2.14',
                        'MSC' : '2.16.840.1.114107.1.1.16.2.15',
                        'SHAH' : '2.16.840.1.114107.1.1.16.2.16']   //no comma at the last one
    def IPID(log, sop) {
        this.log = log;
        this.sop = sop;
    }

    //KHC6121 check to see if IPID belongs to Medstar or not
    def checkIPID(calledAET)
    {
        log.info("Original IPID is " + sopIPID + ", original UID is " + sop.get("IssuerOfPatientIDQualifiersSequence/UniversalEntityID") + ", and original UID type is " + sop.get("IssuerOfPatientIDQualifiersSequence/UniversalEntityIDType"))
        if(sopIPID != null && locationMap.containsKey(sopIPID)){
            stampUID(sopIPID);
         //KHC 8888 if GUH,then put InstitutionName
               if (sopIPID.equalsIgnoreCase("GUH")){
                 setInstitutionName(calledAET);
            }
        }else{
            setDefaultPatientID();
            setIPID(calledAET);
            setInstitutionName(calledAET);
        }
        if (sop.get(AccessionNumber) == null || sop.get(AccessionNumber).isEmpty()) {
            setAccessionNumber();
        }
    }

    //KHC6121 stamp UID and UID type anyways in case they are null
    def stampUID(ipidString)
    {
        log.info("It's a Medstar IPID, setting UID and UID type to match.");
        def domain = locationMap.get(ipidString);

        log.info("SOP: " + sop.get("SOPInstanceUID") + ", setting universal id to: " + domain);
        sop.set("IssuerOfPatientIDQualifiersSequence/UniversalEntityID", domain, VR.LO)
        log.info("Now UID is " + sop.get("IssuerOfPatientIDQualifiersSequence/UniversalEntityID"));
        sop.set("IssuerOfPatientIDQualifiersSequence/UniversalEntityIDType", "ISO", VR.LO)
        log.info("Now UID type is " + sop.get("IssuerOfPatientIDQualifiersSequence/UniversalEntityIDType"));
    }

    def setIPID(theCalledAET) {
        def prefix = getPatientIDPrefix();
        
        if (prefix.equalsIgnoreCase("FSH")) {
            set("FSH","2.16.840.1.114107.1.1.16.2.7", "ISO",sop.get("PatientID").drop(3));

        } else if (prefix.equalsIgnoreCase("GSH")) {
            set("GSH","2.16.840.1.114107.1.1.16.2.2", "ISO",sop.get("PatientID").drop(3));

        } else if (prefix.equalsIgnoreCase("HHC")) {
            set("HHC","2.16.840.1.114107.1.1.16.2.4", "ISO",sop.get("PatientID").drop(3));

        } else if (prefix.equalsIgnoreCase("UMH")) {
            set("UMH","2.16.840.1.114107.1.1.16.2.8", "ISO",sop.get("PatientID").drop(3));

        } else if (prefix.equalsIgnoreCase("GUH")) {
            set("GUH","2.16.840.1.114107.1.1.16.2.6", "ISO",sop.get("PatientID").drop(3));

        } else if (prefix.equalsIgnoreCase("WB4")) {
            set("WB4","2.16.840.1.114107.1.1.16.2.3", "ISO",sop.get("PatientID").drop(3));

        } else if (prefix.equalsIgnoreCase("WHC")) {
            set("WB4","2.16.840.1.114107.1.1.16.2.3", "ISO",sop.get("PatientID").drop(3));

        } else if (prefix.equalsIgnoreCase("SMH")) {
            set("MSMH","2.16.840.1.114107.1.1.16.2.11", "ISO",sop.get("PatientID").drop(3));

        } else if (prefix.equalsIgnoreCase("SMC")) {
            set("MSMHC","2.16.840.1.114107.1.1.16.2.12", "ISO",sop.get("PatientID").drop(3));

        } else if (prefix.equalsIgnoreCase("NRH")) {
            set("6B4","2.16.840.1.114107.1.1.16.2.9", "ISO",sop.get("PatientID").drop(3));

        } else if (prefix.equalsIgnoreCase("MMC")) {
            set("MGI","2.16.840.1.114107.1.1.16.2.10", "ISO",sop.get("PatientID").drop(3));

        } else if (prefix.equalsIgnoreCase("MPP")) {
            set("MPP","2.16.840.1.114107.1.1.16.2.14", "ISO",sop.get("PatientID").drop(3));

        } else if (prefix.equalsIgnoreCase("MSC")) {
            set("MSC","2.16.840.1.114107.1.1.16.2.15", "ISO",sop.get("PatientID").drop(3));

        } else if (theCalledAET.equalsIgnoreCase(mrnSHAH) ) {           
            set("SHAH","2.16.840.1.114107.1.1.16.2.16", "ISO", sop.get("PatientID"));

            // set OtherPatientIDs to "MSH-TEMP-EMPI" + LocalMRN
            sop.set(0x00101000, "MSH-TEMP-EMPI-" + sop.get("PatientID"), VR.LO);
            setAccessionNumber(theCalledAET);
            setStudyDescriptionWithModality();
        } else {
            set("UNKNOWN","2.16.124.113638.1.2.1.1", "ISO", sop.get("PatientID"));

        }
   }

    /*
    * This is the method that sets the IPID values for the study. 
    *
    */
    def set(namespace, universalid, universalidtype, patientID) {

        if (sop.get(IssuerOfPatientID) != null || sop.get("IssuerOfPatientIDQualifiersSequence/UniversalEntityID") != null || sop.get("IssuerOfPatientIDQualifiersSequence/UniversalEntityIDType") != null) {
            def curr_i = sop.get(IssuerOfPatientID)
            def curr_u = sop.get("IssuerOfPatientIDQualifiersSequence/UniversalEntityID")
            def curr_ut = sop.get("IssuerOfPatientIDQualifiersSequence/UniversalEntityIDType")

            log.info("SOP: " + sop.get("SOPInstanceUID") + ", : Going to move existing issuer information [IussuerOfPatientID:  " + curr_i + ", UniversalEntityID: " + curr_u + ", UniversalEntityIDType: " + curr_ut + "] to a Karos specific private tag")

            sop.set(0x00350010, "KAROS PRESERVED INFO 1.0", VR.LO)
            sop.set(0x00351010, curr_i, VR.LO)
            sop.set(0x00351011, curr_u, VR.LO)
            sop.set(0x00351012, curr_ut, VR.LO)
        }

        log.info("SOP: " + sop.get("SOPInstanceUID") + ", setting namespace to: " + namespace);
        sop.set(IssuerOfPatientID, namespace)

        log.info("SOP: " + sop.get("SOPInstanceUID") + ", setting universal id to: " + universalid);
        sop.set("IssuerOfPatientIDQualifiersSequence/UniversalEntityID", universalid, VR.LO)

        log.info("SOP: " + sop.get("SOPInstanceUID") + ", setting universal id type to: " + universalidtype);
        sop.set("IssuerOfPatientIDQualifiersSequence/UniversalEntityIDType", universalidtype, VR.LO)

        if (namespace != null && namespace == "UMH") {
            log.info("SOP: " + sop.get("SOPInstanceUID") + ", stripping leading zeros and setting patient id to: " + patientID.replaceFirst("^0+(?!\$)", ""));
            sop.set(PatientID, patientID.replaceFirst("^0+(?!\$)", ""));
        } else {
            log.info("SOP: " + sop.get("SOPInstanceUID") + ", setting patient id to: " + patientID);
            sop.set(PatientID, patientID);
        }
    }

    /*
    * This method will check the called AE and stamp the Institution Name according to the values provided.
    *
    * KHC6252 - do not modify the Institution Name if the Accession Number starts with 'EX'.
    *
    */
    def setInstitutionName(theCalledAET) {
       def accessionNumber = sop.get("AccessionNumber")
       if (accessionNumber != null && accessionNumber.startsWith('EX')) {
       }
       else {
          if (theCalledAET.equalsIgnoreCase(mrnBA) ) {
             sop.set(0x00080080, "MedStar Radiology Bel Air", VR.LO)
             log.info("SOP: " + sop.get("SOPInstanceUID") + ", Setting Institution Name to: MedStar Radiology Bel Air");
             log.info("SOP: " + sop.get("SOPInstanceUID") + ", setting IssuerOfAccessionNumber to MAS");
             setIssuerOfAccessionNumber("MAS", "2.16.840.1.114107.1.1.16.2.13", "ISO");
          } else if (theCalledAET.equalsIgnoreCase(mrnHY) ) {
             sop.set(0x00080080, "MedStar Radiology Hyattsville", VR.LO)
             log.info("SOP: " + sop.get("SOPInstanceUID") + ", Setting Institution Name to: MedStar Radiology Hyattsville");
             log.info("SOP: " + sop.get("SOPInstanceUID") + ", setting IssuerOfAccessionNumber to MAS");
             setIssuerOfAccessionNumber("MAS", "2.16.840.1.114107.1.1.16.2.13", "ISO");
          } else if (theCalledAET.equalsIgnoreCase(mrnBR) ) {
             sop.set(0x00080080, "MedStar Radiology Brandywine", VR.LO)
             log.info("SOP: " + sop.get("SOPInstanceUID") + ", Setting Institution Name to: MedStar Radiology Brandywine");
             log.info("SOP: " + sop.get("SOPInstanceUID") + ", setting IssuerOfAccessionNumber to MAS");
             setIssuerOfAccessionNumber("MAS", "2.16.840.1.114107.1.1.16.2.13", "ISO");
          } else if (theCalledAET.equalsIgnoreCase(mrnLF1) ) {
             sop.set(0x00080080, "MedStar Radiology Lafayette I", VR.LO)
             log.info("SOP: " + sop.get("SOPInstanceUID") + ", Setting Institution Name to: MedStar Radiology Lafayette I");
             log.info("SOP: " + sop.get("SOPInstanceUID") + ", setting IssuerOfAccessionNumber to MAS");
             setIssuerOfAccessionNumber("MAS", "2.16.840.1.114107.1.1.16.2.13", "ISO");
          } else if (theCalledAET.equalsIgnoreCase(mrnLF2) ) {
             sop.set(0x00080080, "MedStar Radiology Lafayette II", VR.LO)
             log.info("SOP: " + sop.get("SOPInstanceUID") + ", Setting Institution Name to: MedStar Radiology Lafayette II");
             log.info("SOP: " + sop.get("SOPInstanceUID") + ", setting IssuerOfAccessionNumber to MAS");
             setIssuerOfAccessionNumber("MAS", "2.16.840.1.114107.1.1.16.2.13", "ISO");
          } else if (theCalledAET.equalsIgnoreCase(mrnMI) ) {
             sop.set(0x00080080, "MedStar Radiology Mitchellville", VR.LO)
             log.info("SOP: " + sop.get("SOPInstanceUID") + ", Setting Institution Name to: MedStar Radiology Mitchellville");
             log.info("SOP: " + sop.get("SOPInstanceUID") + ", setting IssuerOfAccessionNumber to MAS");
             setIssuerOfAccessionNumber("MAS", "2.16.840.1.114107.1.1.16.2.13", "ISO");
          } else if (theCalledAET.equalsIgnoreCase(mrnTI) ) {
             sop.set(0x00080080, "MedStar Radiology Timonium", VR.LO)
             log.info("SOP: " + sop.get("SOPInstanceUID") + ", Setting Institution Name to: MedStar Radiology Timonium");
             log.info("SOP: " + sop.get("SOPInstanceUID") + ", setting IssuerOfAccessionNumber to MAS");
             setIssuerOfAccessionNumber("MAS", "2.16.840.1.114107.1.1.16.2.13", "ISO"); 
          } else if (theCalledAET.equalsIgnoreCase(mrnCC) ) {
             sop.set(0x00080080, "MedStar Radiology Chevy Chase", VR.LO)
             log.info("SOP: " + sop.get("SOPInstanceUID") + ", Setting Institution Name to: MedStar Radiology Chevy Chase");
             log.info("SOP: " + sop.get("SOPInstanceUID") + ", setting IssuerOfAccessionNumber to MAS");
             setIssuerOfAccessionNumber("MAS", "2.16.840.1.114107.1.1.16.2.13", "ISO");
          } else if (theCalledAET.equalsIgnoreCase(gmoiBR) ) {
             sop.set(0x00080080, "MedStar Orthopaedic Institute at Brandywine", VR.LO)
             log.info("SOP: " + sop.get("SOPInstanceUID") + ", Setting Institution Name to: MedStar Orthopaedic Institute at Brandywine");
             set("GUH","2.16.840.1.114107.1.1.16.2.6", "ISO",sop.get("PatientID"));
             //def accn=sop.get(0x00080050)
             //sop.set(0x00080050, "MOI" + accn, VR.LO);
             //log.info("Adding MOI Prefix to accession number based on called AET.");
             setAccessionNumber(theCalledAET);
          } else if (theCalledAET.equalsIgnoreCase(gmoiLT) ) {
             sop.set(0x00080080, "MedStar Orthopaedic Institute at Lorton", VR.LO)
             log.info("SOP: " + sop.get("SOPInstanceUID") + ", Setting Institution Name to: MedStar Orthopaedic Institute at Lorton");
             set("GUH","2.16.840.1.114107.1.1.16.2.6", "ISO",sop.get("PatientID"));
             //def accn=sop.get(0x00080050)
             //sop.set(0x00080050, "MOI" + accn, VR.LO);
             //log.info("Adding MOI Prefix to accession number based on called AET.");
             setAccessionNumber(theCalledAET);
          }else if (theCalledAET.equalsIgnoreCase(gmoiWB) ) {
             sop.set(0x00080080, "MedStar Orthopaedic Institute at Woodbridge", VR.LO)
             log.info("SOP: " + sop.get("SOPInstanceUID") + ", Setting Institution Name to: MedStar Orthopaedic Institute at Woodbridge");
             set("GUH","2.16.840.1.114107.1.1.16.2.6", "ISO",sop.get("PatientID"));
             //def accn=sop.get(0x00080050)
             //sop.set(0x00080050, "MOI" + accn, VR.LO);
             //log.info("Adding MOI Prefix to accession number based on called AET.");
             setAccessionNumber(theCalledAET);
          }else if (theCalledAET.equalsIgnoreCase(gmoiAL) ) {
             sop.set(0x00080080, "MedStar Orthopaedic Institute at Alexandria", VR.LO)
             log.info("SOP: " + sop.get("SOPInstanceUID") + ", Setting Institution Name to: MedStar Orthopaedic Institute at Alexandria");
             set("GUH","2.16.840.1.114107.1.1.16.2.6", "ISO",sop.get("PatientID"));
             //def accn=sop.get(0x00080050)
             //sop.set(0x00080050, "MOI" + accn, VR.LO);
             //log.info("Adding MOI Prefix to accession number based on called AET.");
             setAccessionNumber(theCalledAET);
          }else if (theCalledAET.equalsIgnoreCase(gmoiWD) ) {
             sop.set(0x00080080, "MedStar Orthopaedic Institute at Waldorf", VR.LO)
             log.info("SOP: " + sop.get("SOPInstanceUID") + ", Setting Institution Name to: MedStar Orthopaedic Institute at Waldorf");
             set("GUH","2.16.840.1.114107.1.1.16.2.6", "ISO",sop.get("PatientID"));
             //def accn=sop.get(0x00080050)
             //sop.set(0x00080050, "MOI" + accn, VR.LO);
             //log.info("Adding MOI Prefix to accession number based on called AET.");
             setAccessionNumber(theCalledAET);
          }else if (theCalledAET.equalsIgnoreCase(mrnSHAH) ) {
             log.info("SOP: " + sop.get("SOPInstanceUID") + ", Setting Institution Name to: MedStar Shah Medical Group");
             sop.set(0x00080080, "MedStar Shah Medical Group", VR.LO)
             log.info("SOP: " + sop.get("SOPInstanceUID") + ", setting IssuerOfAccessionNumber to SHAH");
             setIssuerOfAccessionNumber("SHAH", "2.16.840.1.114107.1.1.16.2.16", "ISO");
          }
       }
    }

    /*
     * This method will obtain the first three characters of the patientID.
     * This is used to determine the IPID values.
     */
    def getPatientIDPrefix() {
        def prefix = "TST";
        def tempID = sop.get("PatientID");
        // no need to check patientID again for null we checked it previously
        prefix = tempID.take(3)
        return prefix; 
    }

    /*
    * This method will check the called AE and autogenerate Accession Number
    *
    * KHC-6012 - limit 16 characters, prefix with SA + LocalMRN + substring(MD5(StudyInstanceUID), max-7, max).
    *
    */

    def setAccessionNumber(theCalledAET) {
        if (theCalledAET.equalsIgnoreCase(mrnSHAH) ) {
            def hashedSUID = MessageDigest.getInstance("MD5").digest(sop.get("StudyInstanceUID").getBytes()).encodeBase64().toString()

            //def AccessionNumber = "SA" + sop.get("PatientID").padLeft(7, '0') + sop.get("StudyInstanceUID").replaceAll("[^0-9]", "").reverse().take(7).reverse();
            //def AccessionNumber = "SA" + sop.get("PatientID").padLeft(7, '0') + hashedSUID.reverse().take(7).reverse();
            def AccessionNumber = "SA" + sop.get("PatientID").padLeft(7, '0') + hashedSUID.replaceAll("[^a-zA-Z0-9]", "").toUpperCase().reverse().take(7).reverse();

            log.info("SOP: " + sop.get("SOPInstanceUID") + ", SHAH study, setting AccessionNumber to {}", AccessionNumber);
            sop.set(0x00080050, AccessionNumber, VR.SH);
        }

        if (theCalledAET.equalsIgnoreCase(gmoiBR) ) {
            def hashedSUID = MessageDigest.getInstance("MD5").digest(sop.get("StudyInstanceUID").getBytes()).encodeBase64().toString()
            def AccessionNumber = "MOI" + sop.get("PatientID").padLeft(7, '0') + hashedSUID.replaceAll("[^a-zA-Z0-9]", "").toUpperCase().reverse().take(6).reverse();
            log.info("SOP: " + sop.get("SOPInstanceUID") + ", BrandyWine Ortho study, setting AccessionNumber to {}", AccessionNumber);
            sop.set(0x00080050, AccessionNumber, VR.SH);
        }
		
		if (theCalledAET.equalsIgnoreCase(gmoiLT) ) {
            def hashedSUID = MessageDigest.getInstance("MD5").digest(sop.get("StudyInstanceUID").getBytes()).encodeBase64().toString()
            def AccessionNumber = "MOI" + sop.get("PatientID").padLeft(7, '0') + hashedSUID.replaceAll("[^a-zA-Z0-9]", "").toUpperCase().reverse().take(6).reverse();
            log.info("SOP: " + sop.get("SOPInstanceUID") + ", Lorton Ortho study, setting AccessionNumber to {}", AccessionNumber);
            sop.set(0x00080050, AccessionNumber, VR.SH);
        }
		
		if (theCalledAET.equalsIgnoreCase(gmoiWB) ) {
            def hashedSUID = MessageDigest.getInstance("MD5").digest(sop.get("StudyInstanceUID").getBytes()).encodeBase64().toString()
            def AccessionNumber = "MOI" + sop.get("PatientID").padLeft(7, '0') + hashedSUID.replaceAll("[^a-zA-Z0-9]", "").toUpperCase().reverse().take(6).reverse();
            log.info("SOP: " + sop.get("SOPInstanceUID") + ", Woodbridge Ortho study, setting AccessionNumber to {}", AccessionNumber);
            sop.set(0x00080050, AccessionNumber, VR.SH);
        }
		
		if (theCalledAET.equalsIgnoreCase(gmoiAL) ) {
            def hashedSUID = MessageDigest.getInstance("MD5").digest(sop.get("StudyInstanceUID").getBytes()).encodeBase64().toString()
            def AccessionNumber = "MOI" + sop.get("PatientID").padLeft(7, '0') + hashedSUID.replaceAll("[^a-zA-Z0-9]", "").toUpperCase().reverse().take(6).reverse();
            log.info("SOP: " + sop.get("SOPInstanceUID") + ", Alexandria Ortho study, setting AccessionNumber to {}", AccessionNumber);
            sop.set(0x00080050, AccessionNumber, VR.SH);
        }
		
		if (theCalledAET.equalsIgnoreCase(gmoiWD) ) {
            def hashedSUID = MessageDigest.getInstance("MD5").digest(sop.get("StudyInstanceUID").getBytes()).encodeBase64().toString()
            def AccessionNumber = "MOI" + sop.get("PatientID").padLeft(7, '0') + hashedSUID.replaceAll("[^a-zA-Z0-9]", "").toUpperCase().reverse().take(6).reverse();
            log.info("SOP: " + sop.get("SOPInstanceUID") + ", Waldorf Ortho study, setting AccessionNumber to {}", AccessionNumber);
            sop.set(0x00080050, AccessionNumber, VR.SH);
        }
    }

    /*
    * This method will stamp IssuerOfAccessionNumber sequence
    *
    * KHC-7310 - Issuer of Accession Number is not stamped correctly
    *
    */

    def setIssuerOfAccessionNumber(namespace, universalID, universalIDType) {
        sop.set('IssuerOfAccessionNumberSequence/LocalNamespaceEntityID', namespace, VR.LO)
        sop.set('IssuerOfAccessionNumberSequence/UniversalEntityID', universalID, VR.LO)
        sop.set('IssuerOfAccessionNumberSequence/UniversalEntityIDType', universalIDType, VR.LO)
    }

    /*
    * This method will check to see if the PatientID is either NULL or empty. If so then 
    * set the value of PatientID to the customer supplied string.
    */
    def setDefaultPatientID() {
        /* KHC6058 tagging dicom field so cache knows study is from NRH router */
        sop.set(0x0040A0A1, "NRH", VR.LO);
        
        // if the PatientID is either NULL or empty enter if block
        if (!sop.get("PatientID") ) {
            log.info("SOP: " + sop.get("SOPInstanceUID") + ", empty PatientID. Setting PatientID to: " + defaultPID);
            sop.set(PatientID, defaultPID);
        }
    }

    /*
     * This method will read DICOM 0008,0060 tag and prefix Study Description with Modality Type
     */
    def setStudyDescriptionWithModality() {
        def modalityType = sop.get("Modality");
        def studyDescription = sop.get("StudyDescription");
                sop.set(StudyDescription,studyDescription.replace("^"," "))
                studyDescription = studyDescription.replace("^"," ")

        if (modalityType != null && modalityType.equalsIgnoreCase('OT')) {
            log.info("SOP: " + sop.get("SOPInstanceUID") + ", Modality type is OT, do not prefix...");
        }
        else if (modalityType != null) {
            if (studyDescription == null) {
                                log.info("SOP: " + sop.get("SOPInstanceUID") + ", StudyDescription is null, will update StudyDescrition with Modality: " + modalityType);
                sop.set(StudyDescription, modalityType.toUpperCase())                                           
            }
                        else if ( !studyDescription.startsWith(modalityType.toUpperCase()) ) {
                log.info("SOP: " + sop.get("SOPInstanceUID") + ", StudyDescription is NOT prefixed with Modality type, will fix this ridiculous inconsisteency by prefixing StudyDescription with Modality: " + modalityType);
                sop.set(StudyDescription, modalityType.toUpperCase() + " " + studyDescription)
            }
            else {
                log.info("SOP: " + sop.get("SOPInstanceUID") + ", StudyDescription appears to be prefixed with Modality type...");
            }
        }
        else{
            log.info("SOP: " + sop.get("SOPInstanceUID") + ", empty Modality type, will not update StudyDescription...");
        }
    }
    def setAccessionNumber() {
        def hashedSUID = MessageDigest.getInstance("MD5").digest(sop.get("StudyInstanceUID").getBytes()).encodeBase64().toString()
        //prefix NRH+ hash of studyinstance UID
        def accessionNumber = "NRH" + sop.get("PatientID").padLeft(7, '0').reverse().take(7).reverse() + hashedSUID.replaceAll("[^a-zA-Z0-9]", "").toUpperCase().reverse().take(6).reverse();

        log.info("SOP: " + sop.get("SOPInstanceUID") + ", has blank AccessionNumber, setting AccessionNumber to {}", accessionNumber);
        sop.set(0x00080050, accessionNumber, VR.SH);
    }
}

