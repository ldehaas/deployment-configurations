<config>

    <!-- ################################################################## -->
    <!--  VARIABLES                                                         -->
    <!-- ################################################################## -->

    <var name="dicomServerForManifestsPort" value="4105" />
    <var name="dicomServerForWADOImagesPort" value="5104" />
    <var name="hl7ProxyReceiverServerPort" value="5555" />
    <var name="internalHL7ServerForPIXadtsPort" value="5554" />
    <var name="internalHL7ServerForORUreportsPort" value="5557" />
    <var name= "defaultCharacterSet" value="8859/1" />

    <!-- The Orders Server is currently turned off - ->
    <var name ="internalHL7ServerForORMordersPort">5556</var>
    <!- - Receiving Application and Sending Facility are used by the HL7 Proxy to identify devices - ->
    <var name ="internalHL7ServerForORMordersReceivingApplication">VAULT-ORM-RCV</var>
    <var name ="internalHL7ServerForORMordersReceivingFacility">TCR06-RUISMM</var>
    -->

    <var name="xdsRepoAndWADORequestsPort" value="8082" />
    <var name="WADOBaseURI" value="/vault/wado" />
    <var name="MorpherScriptLocation" value="${rialto.rootdir}/etc/morphers/ids" />

       <!-- AETitle to use for:
        1. Calling a C-Move SCP (WADO conversion is currently the only IDS workflow that does this) (i.e. use as Calling AETitle)
        2. Requesting a destination AETitle for the ensuing C-Store (generated from the C-Move).
        (For our own WADO Server SCP they can call us anything they like)
       -->
    <var name="dicom-aetitle-for-wado-server-and-scu" value="VAULT_WADO" />

    <!-- DIR imaging archive (e.g. to send cmove request to for WADO conversion) -->
    <var name="DIRdicomHost" value="localhost" />
    <var name="DIRdicomPort" value="5000" />
    <var name="DIRdicomAETitle" value="ALI_QUERY_EIRSCP" />
    <!-- <var name ="DIRdicomAETitle" value="ALI_QUERY_SCP" /> -->

    <!-- ################################################################## -->
    <!--  SERVERS                                                           -->
    <!-- ################################################################## -->

    <!-- receive dicom images to generate manfiests -->
    <server id="dicomForManifestFeed" type="dicom">
        <port>${dicomServerForManifestsPort}</port>
    </server>

    <!-- receive dicom images for WADO  *CURRENTLY NOT USED IN THE POC* -->
    <server id="dicomImageReceiveForWado" type="dicom">
        <port>${dicomServerForWADOImagesPort}</port>
    </server>

    <!-- receive WADO requests: Right now using same port as XDS Repository -->
    <!-- server id="httpXDSRepoAndWadoRequests" type="http">
        <port>${xdsRepoAndWADORequestsPort}</port>
    </server -->

    <!-- internal receive HL7 ADT feeds for PIX functionality -->
    <server id="intHl7AdtforPIX" type="hl7v2">
        <port>${internalHL7ServerForPIXadtsPort}</port>
        <!-- Limit this Server for only locahost traffic -->
        <!--host>127.0.0.1</host-->
        <defaultEncoding>${defaultCharacterSet}</defaultEncoding> 
    </server>

    <!-- receive HL7 ORU reports -->
    <server id="intHl7Oru" type="hl7v2">
        <port>${internalHL7ServerForORUreportsPort}</port>
        <!-- Limit this Server for only locahost traffic -->
        <!--host>127.0.0.1</host-->
        <defaultEncoding>${defaultCharacterSet}</defaultEncoding>
    </server>


    <!-- ################################################################## -->
    <!--  SERVICE                                                           -->
    <!-- ################################################################## -->

    <service id="VaultIds" type="ids">
        <device idref="xdsreg" />
        <device idref="xdsrep" />
        <device idref="DIR-dicom" name="WADO" />

        <server idref="intHl7AdtforPIX" name="PIXFeed" />          <!-- dependencies for receiving HL7 ADT feeds for the MiniPIX functionality -->
        <server idref="intHl7Oru" name="ORU" />                    <!-- dependencies for receiving / storing ORUs -->
        <server idref="dicomForManifestFeed" name="manifest" />    <!-- dependencies for generating manifests -->
        <server idref="dicomImageReceiveForWado" name="WADO" />    <!-- dependencies to WADO enable the IA -->
        <server idref="vault-http" name="WADO">
            <url>${WADOBaseURI}</url>
        </server>

        <!-- NOTE: Sample WADO query  (change the **THIS** items):
            http:localhost:**PORT**/${WADOBaseURI}?requestType=WADO&studyUID=**STUDY_INSTANCE_UID**&seriesUID=**SERIES_INSTANCE_UID**&objectUID=**SOP_INSTANCE_UID**&contentType=text/plain
        -->

        <config>
            <prop name="PIXDatabaseURL">${Vault-DatabaseURL}</prop>
            <prop name="AffinityDomain">${System-AffinityDomain}</prop>

            <prop name="LocalAE">${dicom-aetitle-for-wado-server-and-scu}</prop>
            <prop name="WADOCacheFolder">${rialto.rootdir}/var/ids/wadocache</prop>
            <prop name="MetadataStudyInstanceUIDKey">studyInstanceUID</prop>
            <prop name="ManifestContentTypeCode">examen imagerie^KOS étude simple^Imagerie Québec-DSQ^junk</prop>


            <!-- MORPHERS -->
            <!-- Notes:
                PIX Morpher: Modifies the HL7 ADT Feeds as they are coming in

                Manifest Morpher: Runs once, after all the images for a study have been received.  Sets ad-hoc tags in the Manifest
                    Gotchas are things like local IssuerOfPatientID so that Vault IDS can properly publish the manifest to the XDS Registry

                Document Metadata Morpher: Generates and modifies the metadada that is published to the registry as part of the "Register" in "Provide and Register Document Set"
                    If you want IDS to generate and store the Manifest in the local XDS Repo without publishing to the XDS Registry, use:
                            set(XDSExtendedMetadata('karosDontRegister'), 'true')    at the top of the script

                ORU to SR Morpher: Converts an ORU Report (received from RISs via the HL7 Server) into a DICOM SR

                SR to Text Morpher: Runs when the WADO interface is called to provide a report.  Takes the stored corresponding DICOM SR and extracts the text to serve it back to caller
            -->
            <prop name="PIXMorpher">${MorpherScriptLocation}/pix_morpher.groovy</prop>
            <prop name="ManifestMorpher">${MorpherScriptLocation}/manifest_morpher.groovy</prop>
            <prop name="DocumentMetadataMorpher">${MorpherScriptLocation}/document_metadata_morpher.groovy</prop>
            <prop name="ORUToSRMorpher">${MorpherScriptLocation}/oru_to_sr.groovy</prop>
            <prop name="SRToTextMorpher">${MorpherScriptLocation}/sr_to_text.groovy</prop>

        </config>
    </service>


    <!-- ################################################################## -->
    <!--  DEVICES                                                           -->
    <!-- ################################################################## -->

    <!-- DIR imaging archive (e.g. to send cmove request to for WADO conversion) -->
    <device id="DIR-dicom" type="dicom">
        <host>${DIRdicomHost}</host>
        <port>${DIRdicomPort}</port>
        <aetitle>${DIRdicomAETitle}</aetitle>
    </device>
</config>