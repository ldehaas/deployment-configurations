//return "etc/dataflows/pass-through-and-compress.xml"
//return "etc/dataflows/pass-through.xml"
//return "etc/dataflows/multiple-destinations.xml"
//return "etc/dataflows/decompress.xml"

/**
*This groovy script is used to return the correct dataflow xml file name for a particular modality
* (calling ae Title).  
*
*
*
*
*/

String dfName = "etc/dataflows/pass-through-and-compress.xml";

def Rialto_Cache_Prod = "RIALTO_CACHE";
def Rialto_Cache_Test = "RIALTO_CACHE_T"; 
def Rialto_Router_Test = "RIALTO_ROUTER_T";
def GMOIrt = "RIALTO_GMOI_BRDY";
def GMOILTrt = "RIALTO_GMOI_LORT";
def GMOIWBrt = "RIALTO_GMOI_WOOD";
def GMOIALrt = "RIALTO_GMOI_ALEX";
def GMOIWDrt = "RIALTO_GMOI_WALD";
def SHAHrt = "RIALTO_SHAH";
def OIAdynacad = "OIADYNACAD_AMBRT"
def MRNHYATTPACS = "MRN-HYATT-PACS"
def iPIDrt = get(0x00100021);
def calledAET = getCalledAETitle().toUpperCase();
def modality = get(Modality);
def sopclassUID = get(0x00080016);

def UNKrt = "UNKNOWN";
def GSHrt = "GSH";
def WHCrt = "WHC";
def WBrt = "WB4";
def NBrt = "6B4";
def HHCrt = "HHC";
def GUHrt = "GUH";
def FSHrt = "FSH";
def UMHrt = "UMH";
def MMCrt = "MMC";
def SMHrt = "SMH";
def SMCrt = "SMC";
def MPPrt = "MPP";
def MSCrt = "MSC";
def MSHMCrt = "MSHMC";
def MSMHrt = "MSMH";

switch (iPIDrt) {
                case UNKrt:
                        dfName = "etc/dataflows/rialto_SMH.xml";
                        if (modality.equalsIgnoreCase("NM")) {
                            dfName = "etc/dataflows/rialto_SMH_nm.xml";
                            }
                        break
                case GSHrt:
                        dfName = "etc/dataflows/rialto_GSH.xml";
                        if (modality.equalsIgnoreCase("NM")) {
                            dfName = "etc/dataflows/rialto_GSH_nm.xml";
                            }
                        break
                case WHCrt:
                        dfName = "etc/dataflows/rialto_WHC.xml";
                        if (modality.equalsIgnoreCase("NM")) {
                            dfName = "etc/dataflows/rialto_WHC_nm.xml";
                            }
                        break
                case WBrt:
                        dfName = "etc/dataflows/rialto_WHC.xml";
                        if (modality.equalsIgnoreCase("NM")) {
                            dfName = "etc/dataflows/rialto_WHC_nm.xml";
                            }
                        break
                case NBrt:
                        dfName = "etc/dataflows/rialto_WHC.xml";
                        if (modality.equalsIgnoreCase("NM")) {
                            dfName = "etc/dataflows/rialto_WHC_nm.xml";
                            }
                        break
                case HHCrt:
                        dfName = "etc/dataflows/rialto_HHC.xml";
                        if (modality.equalsIgnoreCase("NM")) {
                            dfName = "etc/dataflows/rialto_HHC_nm.xml";
                            }
                        break
                case GUHrt:
                        dfName = "etc/dataflows/rialto_GUH.xml";
                        if (modality.equalsIgnoreCase("NM")) {
                            dfName = "etc/dataflows/rialto_GUH_nm.xml";
                            }
                        break
                case FSHrt:
                        dfName = "etc/dataflows/rialto_FSH.xml";
                        if (modality.equalsIgnoreCase("NM")) {
                            dfName = "etc/dataflows/rialto_FSH_nm.xml";
                            }
                        if (modality.equalsIgnoreCase("MG") || sopclassUID == "1.2.840.10008.5.1.4.1.1.88.50") {
                            dfName = "etc/dataflows/rialto_FSH_mg.xml";
                            }
                        break
                case UMHrt:
                        dfName = "etc/dataflows/rialto_UMH.xml";
                        if (modality.equalsIgnoreCase("NM")) {
                            dfName = "etc/dataflows/rialto_UMH_nm.xml";
                            }
                        break
                case MMCrt:
                        dfName = "etc/dataflows/rialto_MMC.xml";
                        if (modality.equalsIgnoreCase("NM")) {
                            dfName = "etc/dataflows/rialto_MMC_nm.xml";
                            }
                        break
                case SMHrt:
                        dfName = "etc/dataflows/rialto_SMH.xml";
                        if (modality.equalsIgnoreCase("NM")) {
                            dfName = "etc/dataflows/rialto_SMH_nm.xml";
                            }
                        break
                                case MSMHrt:
                        dfName = "etc/dataflows/rialto_SMH.xml";
                        if (modality.equalsIgnoreCase("NM")) {
                            dfName = "etc/dataflows/rialto_SMH_nm.xml";
                            }
                        break
                case SMCrt:
                        dfName = "etc/dataflows/rialto_SMC.xml";
                        if (modality.equalsIgnoreCase("NM")) {
                            dfName = "etc/dataflows/rialto_SMC_nm.xml";
                            }
                        break
                case MPPrt:
                        dfName = "etc/dataflows/rialto_MPP.xml";
                        if (modality.equalsIgnoreCase("NM")) {
                            dfName = "etc/dataflows/rialto_MPP_nm.xml";
                            }
                        break
                case MSCrt:
                        dfName = "etc/dataflows/rialto_MPP.xml";
                        if (modality.equalsIgnoreCase("NM")) {
                            dfName = "etc/dataflows/rialto_MPP_nm.xml";
                            }
                        break
                default:
                        dfName = "etc/dataflows/rialto_cache_prod.xml";
                        if (modality.equalsIgnoreCase("NM")) {
                            dfName = "etc/dataflows/rialto_cache_prod_nm.xml";
                            }
                        break
}

   /* This method will check called AE and route the studies to the correct destinations. 
    */

switch (calledAET) {

                case Rialto_Cache_Prod:
                        dfName = "etc/dataflows/rialto_cache_prod.xml";
                        if (modality.equalsIgnoreCase("NM")) {
                            dfName = "etc/dataflows/rialto_cache_prod.xml_nm";
                            }
                        break

                case Rialto_Cache_Test:
                        dfName = "etc/dataflows/rialto_cache_test.xml";
                        break

                case Rialto_Router_Test:
                        dfName = "etc/dataflows/rialto_router_test.xml";
                        break
                       
                case GMOIrt:
                        dfName = "etc/dataflows/rialto_GMOI.xml";
                        if (modality.equalsIgnoreCase("NM")) {
                            dfName = "etc/dataflows/rialto_GMOI_nm.xml";
                            }
                        break
						
				case GMOILTrt:
                        dfName = "etc/dataflows/rialto_GMOI.xml";
                        if (modality.equalsIgnoreCase("NM")) {
                            dfName = "etc/dataflows/rialto_GMOI_nm.xml";
                            }
                        break
						
				case GMOIWBrt:
                        dfName = "etc/dataflows/rialto_GMOI.xml";
                        if (modality.equalsIgnoreCase("NM")) {
                            dfName = "etc/dataflows/rialto_GMOI_nm.xml";
                            }
                        break
						
				case GMOIALrt:
                        dfName = "etc/dataflows/rialto_GMOI.xml";
                        if (modality.equalsIgnoreCase("NM")) {
                            dfName = "etc/dataflows/rialto_GMOI_nm.xml";
                            }
                        break
						
				case GMOIWDrt:
                        dfName = "etc/dataflows/rialto_GMOI.xml";
                        if (modality.equalsIgnoreCase("NM")) {
                            dfName = "etc/dataflows/rialto_GMOI_nm.xml";
                            }
                        break
						
                case SHAHrt:
                        //dfName = "etc/dataflows/rialto_router_test.xml";
                        //dfName = "etc/dataflows/rialto_cache_test.xml";
                        dfName = "etc/dataflows/rialto_SHAH.xml";
                                                if (modality.equalsIgnoreCase("CT")) {
                            dfName = "etc/dataflows/rialto_SHAHandTERARECON.xml";
                            }
                        break
						
				case OIAdynacad:
						dfName = "etc/dataflows/OIAdynacad.xml";
						break

				case MRNHYATTPACS:
						dfName = "etc/dataflows/rialto_MRN-HYATT-PACSonly";
						break
}

log.info("Dataflow template found for modality " + getCallingAETitle()  + " with destination set to " + getCalledAETitle() + " is " + dfName);

/* return the dataflow template xml file name */
return dfName;


