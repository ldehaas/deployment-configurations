/**
 * This script handles the in-morphing for Bel Air 
 *
 * This site handles studies for various sites, so the IPID is the first 3 characters
 * of the patientID.  We will inspect these characters to determine what to stamp for
 * the IPID values.
 *
 */

IPID ipid = new IPID(log, input)

ipid.setDefaultPatientID()
ipid.setIPID()
ipid.setInstitutionName(getCalledAETitle() )

class IPID {
    def log = null;
    def sop = null;
    def defaultPID = "MSH-TEMP_ID";
    def mrnBA = "RIALTO_MRN_BA";
    def mrnHY = "RIALTO_MRN_HYAT"; 
    def mrnBR = "RIALTO_MRN_BRDY";
    def mrnLF = "RIALTO_MRN_LFTE";
    def gmoiBR = "RIALTO_GMOI_BRDY"
    def masOT = "RIALTO_MAS_OTH"


    def IPID(log, sop) {
        this.log = log;
        this.sop = sop;
    }

    def setIPID() {

        def prefix = getPatientIDPrefix();

        if (prefix.equalsIgnoreCase("FSH")) {
            set("FSH","2.16.840.1.114107.1.1.16.2.7", "ISO",sop.get("PatientID").drop(3));

        } else if (prefix.equalsIgnoreCase("GSH")) {
            set("GSH","2.16.840.1.114107.1.1.16.2.2", "ISO",sop.get("PatientID").drop(3));

        } else if (prefix.equalsIgnoreCase("HHC")) {
            set("HHC","2.16.840.1.114107.1.1.16.2.4", "ISO",sop.get("PatientID").drop(3));

        } else if (prefix.equalsIgnoreCase("UMH")) {
            set("UMH","2.16.840.1.114107.1.1.16.2.8", "ISO",sop.get("PatientID").drop(3));

        } else if (prefix.equalsIgnoreCase("GUH")) {
            set("GUH","2.16.840.1.114107.1.1.16.2.6", "ISO",sop.get("PatientID").drop(3));

        } else if (prefix.equalsIgnoreCase("WB4")) {
            set("WB4","2.16.840.1.114107.1.1.16.2.3", "ISO",sop.get("PatientID").drop(3));

        } else if (prefix.equalsIgnoreCase("WHC")) {
            set("WB4","2.16.840.1.114107.1.1.16.2.3", "ISO",sop.get("PatientID").drop(3));

        } else if (prefix.equalsIgnoreCase("SMH")) {
            set("MSMH","2.16.840.1.114107.1.1.16.2.11", "ISO",sop.get("PatientID").drop(3));

        } else if (prefix.equalsIgnoreCase("SMC")) {
            set("MSMHC","2.16.840.1.114107.1.1.16.2.12", "ISO",sop.get("PatientID").drop(3));

        } else if (prefix.equalsIgnoreCase("NRH")) {
            set("6B4","2.16.840.1.114107.1.1.16.2.9", "ISO",sop.get("PatientID").drop(3));

        } else if (prefix.equalsIgnoreCase("MMC")) {
            set("MGI","2.16.840.1.114107.1.1.16.2.10", "ISO",sop.get("PatientID").drop(3));

        } else if (prefix.equalsIgnoreCase("MPP")) {
            set("MPP","2.16.840.1.114107.1.1.16.2.14", "ISO",sop.get("PatientID").drop(3));

        } else if (prefix.equalsIgnoreCase("MSC")) {
            set("MSC","2.16.840.1.114107.1.1.16.2.15", "ISO",sop.get("PatientID").drop(3));

        } else {
            set("UNKNOWN","2.16.124.113638.1.2.1.1", "ISO", sop.get("PatientID"));

        }


   }

    /*
    * This is the method that sets the IPID values for the study. 
    *
    */
    def set(namespace, universalid, universalidtype, patientID) {

        if (sop.get(IssuerOfPatientID) != null || sop.get("IssuerOfPatientIDQualifiersSequence/UniversalEntityID") != null || sop.get("IssuerOfPatientIDQualifiersSequence/UniversalEntityIDType") != null) {
            def curr_i = sop.get(IssuerOfPatientID)
            def curr_u = sop.get("IssuerOfPatientIDQualifiersSequence/UniversalEntityID")
            def curr_ut = sop.get("IssuerOfPatientIDQualifiersSequence/UniversalEntityIDType")

            log.info("SOP: " + sop.get("SOPInstanceUID") + ", : Going to move existing issuer information [IussuerOfPatientID:  " + curr_i + ", UniversalEntityID: " + curr_u + ", UniversalEntityIDType: " + curr_ut + "] to a Karos specific private tag")

            sop.set(0x00350010, "KAROS PRESERVED INFO 1.0", VR.LO)
            sop.set(0x00351010, curr_i, VR.LO)
            sop.set(0x00351011, curr_u, VR.LO)
            sop.set(0x00351012, curr_ut, VR.LO)
        }

        log.info("SOP: " + sop.get("SOPInstanceUID") + ", setting namespace to: " + namespace);
        sop.set(IssuerOfPatientID, namespace)

        log.info("SOP: " + sop.get("SOPInstanceUID") + ", setting universal id to: " + universalid);
        sop.set("IssuerOfPatientIDQualifiersSequence/UniversalEntityID", universalid, VR.LO)

        log.info("SOP: " + sop.get("SOPInstanceUID") + ", setting universal id type to: " + universalidtype);
        sop.set("IssuerOfPatientIDQualifiersSequence/UniversalEntityIDType", universalidtype, VR.LO)
        
        if (namespace != null && namespace == "UMH") {
            log.info("SOP: " + sop.get("SOPInstanceUID") + ", stripping leading zeros and setting patient id to: " + patientID.replaceFirst("^0+(?!\$)", ""));
            sop.set(PatientID, patientID.replaceFirst("^0+(?!\$)", ""));
        } else {
            log.info("SOP: " + sop.get("SOPInstanceUID") + ", setting patient id to: " + patientID);
            sop.set(PatientID, patientID);
        }
    }


    /*
    * This method will check the called AE and stamp the Institution Name according to the values provided. 
    */
    def setInstitutionName(theCalledAET) {
       if (theCalledAET.equalsIgnoreCase(mrnBA) ) {
          sop.set(0x00080080, "MedStar Radiology Network - Bel Air", VR.LO)
          log.info("SOP: " + sop.get("SOPInstanceUID") + ", Setting Institution Name to: MedStar Radiology Network - Bel Air");
       } else if (theCalledAET.equalsIgnoreCase(mrnHY) ) {
          sop.set(0x00080080, "MedStar Radiology Network - Hyattsville", VR.LO)
          log.info("SOP: " + sop.get("SOPInstanceUID") + ", Setting Institution Name to: MedStar Radiology Network - Hyattsville");
       } else if (theCalledAET.equalsIgnoreCase(mrnBR) ) {
          sop.set(0x00080080, "MedStar Radiology Network - Brandywine", VR.LO)
          log.info("SOP: " + sop.get("SOPInstanceUID") + ", Setting Institution Name to: MedStar Radiology Network - Brandywine");
       } else if (theCalledAET.equalsIgnoreCase(mrnLF) ) {
          sop.set(0x00080080, "MedStar Radiology Network - Lafayette", VR.LO)
          log.info("SOP: " + sop.get("SOPInstanceUID") + ", Setting Institution Name to: MedStar Radiology Network - Lafayette");
       } else if (theCalledAET.equalsIgnoreCase(gmoiBR||masOT) ) {
          sop.set(0x00080080, "MedStar Orthopaedic Institute at Brandywine", VR.LO)
          ipid.set("GUH","2.16.840.1.114107.1.1.16.2.6", "ISO",sop.get("PatientID").drop(3));
          log.info("SOP: " + sop.get("SOPInstanceUID") + ", Setting Institution Name to: MedStar Orthopaedic Institute at Brandywine");
          prepend(0x00080050, "MOI");
          log.info("Adding MOI Prefix to accession number based on called AET.");
       }
    }

    /*
    * This method will check to see if the PatientID is either NULL or empty. If so then 
    * set the value of PatientID to the customer supplied string.
    */
    def setDefaultPatientID() {
        // if the PatientID is either NULL or empty enter if block
        if (!sop.get("PatientID") ) {
            log.info("SOP: " + sop.get("SOPInstanceUID") + ", empty PatientID. Setting PatientID to: " + defaultPID);
            sop.set(PatientID, defaultPID);
        }
    }

    /*
     * This method will obtain the first three characters of the patientID.
     * This is used to determine the IPID values.
     */
    def getPatientIDPrefix() {

        def prefix = "TST";
        def tempID = sop.get("PatientID");
        // no need to check patientID again for null we checked it previously
        prefix = tempID.take(3)
        return prefix; 
    }

}
