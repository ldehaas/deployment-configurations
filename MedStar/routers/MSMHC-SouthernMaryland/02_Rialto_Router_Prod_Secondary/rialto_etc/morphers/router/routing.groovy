def called_AET = getCalledAETitle().toUpperCase();
if (called_AET.equals("RIALTO_SMHC_RAD")) {
        return "etc/dataflows/ERSRAD_MCKESSON_P0T7.xml";
}

else if (called_AET.equals("RIALTO_SMHC_CAR")) {
        return "etc/dataflows/ERSCAR_MCKESSON_P0T7.xml";
}

else if (called_AET.equals("RIALTO_SMHC_OTH")) {
        return "etc/dataflows/ERSOTH_MCKESSON_P0T7.xml";
}
else if (called_AET.equals("MSMHC_PACS")) {
		return "etc/dataflows/MCKESSON_only.xml";
}
else
return "etc/dataflows/multiple-destinations.xml"
