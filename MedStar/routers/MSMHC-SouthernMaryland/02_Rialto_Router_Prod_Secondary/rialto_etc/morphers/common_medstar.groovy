/**
 * Configuration specific to the MedStar environment
 */
class MedStar {

    def MedStar(log) {
        this.log = log;
    }

    def UNKNOWN_NS = 'UNKNOWN'
    def ENTERPRISE = 'MEDSTAR'

    def log = null

    def ns_to_ipid_map = [
        (ENTERPRISE): (ENTERPRISE),
        'HELIX': (ENTERPRISE),
        'MFSMC': 'FSH',
        'MGSH': 'GSH',
        'MHH': 'HHC',
        'MUMH': 'UMH',
        'MGUH': 'GUH',
        'MWHC': 'WB4',
        'MNRH': '6B4',
        'MMMC': 'MGI',
        'MSMH': 'MSMH',
        'MSMHC': 'MSMHC',
        (UNKNOWN_NS): (UNKNOWN_NS)
    ]

    /**
     * Ensure there is a key in this map for each value found in 'ns_to_ipid_map'.
     * This is important to understand how the HL7 namespace maps into the universal id.
     */
    def ipid_to_universalid_map = [
        (ENTERPRISE): '2.16.840.1.114107.1.1.16.2.5',
        'FSH': '2.16.840.1.114107.1.1.16.2.7',
        'GSH': '2.16.840.1.114107.1.1.16.2.2',
        'HHC': '2.16.840.1.114107.1.1.16.2.4',
        'UMH': '2.16.840.1.114107.1.1.16.2.8',
        'GUH': '2.16.840.1.114107.1.1.16.2.6',
        'WB4': '2.16.840.1.114107.1.1.16.2.3',
        '6B4': '2.16.840.1.114107.1.1.16.2.9',
        'MGI': '2.16.840.1.114107.1.1.16.2.10',
        'MSMH': '2.16.840.1.114107.1.1.16.2.11',
        'MSMHC': '2.16.840.1.114107.1.1.16.2.12',
        (UNKNOWN_NS): '2.16.124.113638.1.2.1.1'
    ]
}
