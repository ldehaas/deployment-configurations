<?xml version="1.0"?>
<config>
    <prop name="DefaultUserPreferences">

        <!-- Configuration and customization settings included here are for all  -->
        <!-- possible UI pages. The pages available in your UI will depend on    -->
        <!-- your deployment type and system configuration.                      -->
        <!--                                                                     -->
        <!-- Index of all possible UI pages:                                     -->
        <!--     Patients & Documents                                            -->
        <!--        Patients                                                     -->
        <!--           - Search criteria:    patient.search.criteria.*           -->
        <!--           - Table config:       patient.list.column.*               -->
        <!--           - Avail. page sizes:  patient.list.pagesize.set           -->
        <!--        Documents                                                    -->
        <!--           - Search criteria:    doc.search.criteria.*               -->
        <!--           - Table config:       doc.list.column.*                   -->
        <!--           - Avail. page sizes:  doc.list.pagesize.set               -->
        <!--                                                                     -->
        <!--     Audit Records                                                   -->
        <!--        - Search criteria:       arr.search.criteria.*               -->
        <!--        - Table config:          arr.list.column.*                   -->
        <!--        - Avail. page sizes:     arr.list.pagesize.set               -->
        <!--                                                                     -->
        <!--     Dataflow Tracking                                               -->
        <!--        - Search criteria:       dataflows.search.criteria.*         -->
        <!--        - Table config:          dataflows.list.column.*             -->
        <!--        - Default sort column:   dataflows.list.sortbycolumn.column  -->
        <!--                                                                     -->
        <!--     Users & Groups                                                  -->
        <!--        Users                                                        -->
        <!--           - Search criteria:    user.search.criteria.*              -->
        <!--           - Table config:       user.list.column.*                  -->
        <!--        Groups                                                       -->
        <!--           - Search criteria:    group.search.criteria.*             -->
        <!--           - Table config:       group.list.column.*                 -->
        <!--        Realms                                                       -->
        <!--           -Table config:        realms.list.column.*                -->
        <!--                                                                     -->
        <!--     DICOM Studies                                                   -->
        <!--        Studies                                                      -->
        <!--           - Search criteria:    studies.search.criteria.*           -->
        <!--           - Table config:       studies.list.column.*               -->
        <!--           - Avail. page sizes:  studies.list.pagesize.set           -->
        <!--        Study Send Jobs                                              -->
        <!--           - Search criteria:    study_jobs.search.criteria.*        -->
        <!--           - Table config:       study_jobs.list.column.*            -->
        <!--                                                                     -->
        <!--     Quality Management                                              -->
        <!--        - Search criteria:       xdsQuality.search.criteria.*        -->
        <!--        - Table config:          xdsQuality.list.column.*            -->
        <!--                                                                     -->
        <!-- Default theme config:           user.theme                          -->
        <!-- Default language config:        user.language                       -->
        <!--                                                                     -->
        <!-- Default DICOM viewer:     document.application.dicom.default.viewer -->
        <!--                                                                     -->
        <!-- Information previews                                                -->
        <!--    Document metadata preview:   document.metadata.preview.*         -->
        <!--    Patient info preview:        patient.metadata.doc.index.*        -->
        <!--                                                                     -->

        <!-- =================================================================== -->


        <!-- =================================================================== -->
        <!-- ================ Search Criteria Configuration ==================== -->
        <!-- =================================================================== -->

        <!-- Patient search -->

        <preference>
            <label>patient.search.criteria.patientID</label>
            <value>true</value>
        </preference>
        <preference>
            <label>patient.search.criteria.givenName</label>
            <value>false</value>
        </preference>
        <preference>
            <label>patient.search.criteria.familyName</label>
            <value>true</value>
        </preference>
        <preference>
            <label>patient.search.criteria.domainID</label>
            <value>true</value>
        </preference>


        <!-- Document search -->

        <preference>
            <label>doc.search.criteria.classCode</label>
            <value>false</value>
        </preference>
        <preference>
            <label>doc.search.criteria.creationTime</label>
            <value>true</value>
        </preference>
        <preference>
            <label>doc.search.criteria.availabilityStatus</label>
            <value>false</value>
        </preference>
        <!--preference>
            <label>doc.search.criteria.eventCodes</label>
            <value>true</value>
        </preference-->
        <preference>
            <label>doc.search.criteria.formatCode</label>
            <value>true</value>
        </preference>
        <!--preference>
            <label>doc.search.criteria.extendedMetadata</label>
            <value>false</value>
        </preference-->

        <!-- Audit record search -->

        <!--  preference>
            <label>arr.search.criteria.patientID</label>
            <value>true</value>
        </preference-->
        <!--  preference>
            <label>arr.search.criteria.domainID</label>
            <value>true</value>
        </preference-->
        <preference>
            <label>arr.search.criteria.creationTime</label>
            <value>true</value>
        </preference>
        <preference>
            <label>arr.search.criteria.requesterUserID</label>
            <value>true</value>
        </preference>
        <preference>
            <label>arr.search.criteria.realmID</label>
            <value>true</value>
        </preference>

        <!-- User search -->

        <preference>
            <label>user.search.criteria.userID</label>
            <value>true</value>
        </preference>
        <preference>
            <label>user.search.criteria.realmID</label>
            <value>true</value>
        </preference>
        <preference>
            <label>user.search.criteria.firstName</label>
            <value>true</value>
        </preference>
        <preference>
            <label>user.search.criteria.lastName</label>
            <value>true</value>
        </preference>
        <preference>
            <label>user.search.criteria.groupName</label>
            <value>true</value>
        </preference>
        <preference>
            <label>user.search.criteria.status</label>
            <value>true</value>
        </preference>


        <!-- Group search -->

        <preference>
            <label>group.search.criteria.realmID</label>
            <value>true</value>
        </preference>

        <!-- Confidentiality code search -->

        <preference>
            <label>confidentialityCodes.search.criteria.code</label>
            <value>true</value>
        </preference>
        <preference>
            <label>confidentialityCodes.search.criteria.displayName</label>
            <value>false</value>
        </preference>
        <preference>
            <label>confidentialityCodes.search.criteria.description</label>
            <value>false</value>
        </preference>

        <!-- DICOM Studies search -->

        <preference>
            <label>studies.search.criteria.aeTitle</label>
            <value>true</value>
        </preference>
        <preference>
            <label>studies.search.criteria.patientID</label>
            <value>false</value>
        </preference>
        <preference>
            <label>studies.search.criteria.issuerOfPatientID</label>
            <value>false</value>
        </preference>
        <preference>
            <label>studies.search.criteria.accessionNumber</label>
            <value>true</value>
        </preference>
        <preference>
            <label>studies.search.criteria.studyDate</label>
            <value>false</value>
        </preference>
        <preference>
            <label>studies.search.criteria.studyDate.timeRanges</label>
            <value>1,4,6,12,24</value>
        </preference>
        <preference>
            <label>studies.search.criteria.givenName</label>
            <value>false</value>
        </preference>
        <preference>
            <label>studies.search.criteria.familyName</label>
            <value>false</value>
        </preference>


        <!-- "Send Study" jobs code search -->

        <preference>
            <label>study_jobs.search.criteria.sourceAETitle</label>
            <value>true</value>
        </preference>
        <preference>
            <label>study_jobs.search.criteria.destinationAETitle</label>
            <value>true</value>
        </preference>
        <preference>
            <label>study_jobs.search.criteria.creationTime</label>
            <value>false</value>
        </preference>
        <preference>
            <label>study_jobs.search.criteria.taskStatus</label>
            <value>false</value>
        </preference>

        <!-- Quality Control search -->

        <preference>
            <label>xdsQuality.search.criteria.patientID</label>
            <value>true</value>
        </preference>
        <preference>
            <label>xdsQuality.search.criteria.issuerOfPatientID</label>
            <value>true</value>
        </preference>
        <preference>
            <label>xdsQuality.search.criteria.creationTime</label>
            <value>true</value>
        </preference>

        <!-- Dataflows search -->

        <preference>
            <label>dataflows.search.criteria.createdTime</label>
            <value>true</value>
        </preference>
        <preference>
            <label>dataflows.search.criteria.modifiedTime</label>
            <value>true</value>
        </preference>
        <preference>
            <label>dataflows.search.criteria.studyDate</label>
            <value>true</value>
        </preference>
        <preference>
            <label>dataflows.search.criteria.dataflowState</label>
            <value>true</value>
        </preference>
        <preference>
            <label>dataflows.search.criteria.accessionNumber</label>
            <value>true</value>
        </preference>
        <preference>
            <label>dataflows.search.criteria.studyInstanceUID</label>
            <value>true</value>
        </preference>
        <preference>
            <label>dataflows.search.criteria.familyName</label>
            <value>true</value>
        </preference>
        <preference>
            <label>dataflows.search.criteria.givenName</label>
            <value>true</value>
        </preference>
        <preference>
            <label>dataflows.search.criteria.enableAutoRefresh</label>
            <value>true</value>
        </preference>
        <preference>
            <label>dataflows.search.criteria.refreshInterval</label>
            <value>true</value>
        </preference>
        <preference>
           <label>dataflows.search.criteria.patientID</label>
           <value>true</value>
        </preference>


        <!-- =================================================================== -->
        <!-- ================== Table Columns Configuration ==================== -->
        <!-- =================================================================== -->

        <!-- List of Documents Table -->

        <preference>
            <label>doc.list.column.title</label>
            <value>1,1</value>
        </preference>
        <preference>
            <label>doc.list.column.extendedMetadata.accessionNumber</label>
            <value>1,2</value>
        </preference>
        <preference>
            <label>doc.list.column.author</label>
            <value>1,3</value>
        </preference>
        <preference>
            <label>doc.list.column.availabilityStatus</label>
            <value>-1,4</value>
        </preference>
        <preference>
            <label>doc.list.column.classCodeDisplayName</label>
            <value>1,5</value>
        </preference>
        <preference>
            <label>doc.list.column.confidentialityCode</label>
            <value>1,6</value>
        </preference>
        <preference>
            <label>doc.list.column.creationTime</label>
            <value>1,7</value>
        </preference>
        <preference>
            <label>doc.list.column.sourceOrganization</label>
            <value>1,8</value>
        </preference>
        <preference>
            <label>doc.list.column.extendedMetadata.referringPhysician</label>
            <value>-1,9</value>
        </preference>


        <!-- List of Patients Table -->

        <preference>
            <label>patient.list.column.patientName</label>
            <value>1,1</value>
        </preference>
        <preference>
            <label>patient.list.column.gender</label>
            <value>1,2</value>
        </preference>
        <preference>
            <label>patient.list.column.birthDate</label>
            <value>1,3</value>
        </preference>
        <preference>
            <label>patient.list.column.patientID</label>
            <value>1,4</value>
        </preference>
        <preference>
            <label>patient.list.column.domainName</label>
            <value>1,5</value>
        </preference>
        <preference>
            <label>patient.list.column.address</label>
            <value>1,6</value>
        </preference>
        <preference>
            <label>patient.list.column.phoneNumber</label>
            <value>-1,7</value>
        </preference>


        <!-- List of Audit Records Table -->

        <preference>
            <label>arr.list.column.timestamp</label>
            <value>1,1</value>
        </preference>
        <preference>
            <label>arr.list.column.patientID</label>
            <value>1,2</value>
        </preference>
        <preference>
            <label>arr.list.column.action</label>
            <value>-1,3</value>
        </preference>
        <preference>
            <label>arr.list.column.eventType</label>
            <value>1,4</value>
        </preference>
        <preference>
            <label>arr.list.column.sourceID</label>
            <value>1,5</value>
        </preference>
        <preference>
            <label>arr.list.column.requesterUserID</label>
            <value>1,6</value>
        </preference>
        <preference>
            <label>arr.list.column.requesterIP</label>
            <value>1,7</value>
        </preference>

        <!-- List of Groups Table -->

        <preference>
            <label>group.list.column.name</label>
            <value>1,1</value>
        </preference>
        <preference>
            <label>group.list.column.domain</label>
            <value>1,2</value>
        </preference>
        <preference>
            <label>group.list.column.description</label>
            <value>1,3</value>
        </preference>


        <!-- List of Users Table -->

        <preference>
            <label>user.list.column.name</label>
            <value>1,1</value>
        </preference>
        <preference>
            <label>user.list.column.id</label>
            <value>1,2</value>
        </preference>
    <preference>
            <label>user.list.column.isActive</label>
            <value>1,3</value>
        </preference>
        <preference>
            <label>user.list.column.group</label>
            <value>1,4</value>
        </preference>
        <preference>
            <label>user.list.column.realm</label>
            <value>1,5</value>
        </preference>


        <!-- List of Realms Table -->

        <preference>
            <label>realms.list.column.name</label>
            <value>1,1</value>
        </preference>
        <preference>
            <label>realms.list.column.realmType</label>
            <value>1,2</value>
        </preference>
        <preference>
            <label>realms.list.column.isEnabled</label>
            <value>1,3</value>
        </preference>
        <preference>
            <label>realms.list.column.description</label>
            <value>1,4</value>
        </preference>


        <!-- Confidentiality codes table -->

        <preference>
            <label>confidentialityCodes.list.column.code</label>
            <value>1,1</value>
        </preference>
        <preference>
            <label>confidentialityCodes.list.column.displayName</label>
            <value>1,2</value>
        </preference>
        <preference>
            <label>confidentialityCodes.list.column.description</label>
            <value>1,3</value>
        </preference>


        <!-- Document type codes table -->

        <preference>
            <label>classCode.list.column.displayName</label>
            <value>1,1</value>
        </preference>
        <preference>
            <label>classCode.list.column.codeValue</label>
            <value>1,2</value>
        </preference>
        <preference>
            <label>classCode.list.column.schemeName</label>
            <value>1,3</value>
        </preference>
        <preference>
            <label>classCode.list.column.schemeId</label>
            <value>1,4</value>
        </preference>


        <!-- List of Studies Table -->

        <preference>
            <label>studies.list.column.accessionNumber</label>
            <value>1,1</value>
        </preference>
        <preference>
            <label>studies.list.column.patientName</label>
            <value>1,2</value>
        </preference>
        <preference>
            <label>studies.list.column.modalitiesInStudy</label>
            <value>1,3</value>
        </preference>
        <preference>
            <label>studies.list.column.numberOfStudyRelatedInstances</label>
            <value>1,4</value>
        </preference>
        <preference>
            <label>studies.list.column.studyDate</label>
            <value>1,5</value>
        </preference>
        <preference>
            <label>studies.list.column.studyTime</label>
            <value>1,6</value>
        </preference>

        <!-- List of Jobs Table -->

        <preference>
            <label>study_jobs.list.column.transactionID</label>
            <value>1,1</value>
        </preference>
        <preference>
            <label>study_jobs.list.column.issuerOfPatientID</label>
            <value>1,2</value>
        </preference>
        <preference>
            <label>study_jobs.list.column.accessionNumber</label>
            <value>1,3</value>
        </preference>
        <preference>
            <label>study_jobs.list.column.sourceAETitle</label>
            <value>1,5</value>   
        </preference>
        <preference>
            <label>study_jobs.list.column.destinationAETitle</label>
            <value>1,6</value>
        </preference>
        <preference>
            <label>study_jobs.list.column.patientID</label>
            <value>1,7</value>
        </preference>
        <preference>
            <label>study_jobs.list.column.createdDateTime</label>
            <value>1,8</value>
        </preference>
        <preference>
            <label>study_jobs.list.column.taskStatus</label>
            <value>1,9</value>
        </preference>

        <!-- List of Quality Control - XDS Errors -->

        <preference>
            <label>xdsQuality.list.column.id</label>
            <value>1,1</value>
        </preference>
        <preference>
            <label>xdsQuality.list.column.pid</label>
            <value>1,2</value>
        </preference>
        <preference>
            <label>xdsQuality.list.column.createdDateTime</label>
            <value>1,3</value>
        </preference>

        <!-- List of Dataflows Table -->

        <preference>
            <label>dataflows.list.column.patientID</label>
            <value>1,1</value>
        </preference>
        <preference>
            <label>dataflows.list.column.patientName</label>
            <value>1,2</value>
        </preference>
        <preference>
            <label>dataflows.list.column.issuerOfPatientID</label>
            <value>1,3</value>
        </preference>
        <preference>
            <label>dataflows.list.column.callingAe</label>
            <value>1,4</value>
        </preference>
        <preference>
            <label>dataflows.list.column.accessionNumber</label>
            <value>1,5</value>
        </preference>
        <preference>
            <label>dataflows.list.column.studyDateTime</label>
            <value>1,6</value>
        </preference>
        <preference>
            <label>dataflows.list.column.creationDateTime</label>
            <value>1,7</value>
        </preference>
        <preference>
            <label>dataflows.list.column.modifiedDateTime</label>
            <value>1,8</value>
        </preference>
        <!--preference>
            <label>dataflows.list.column.issuerOfPatientID_universalID</label>
            <value>1,9</value>
        </preference-->


        <!-- =================================================================== -->
        <!-- ==================== User Customizations ========================== -->
        <!-- =================================================================== -->

        <!-- Themes and Language Preference -->

        <preference>
            <label>user.theme</label>
            <value>${Navigator-DefaultTheme}</value>
        </preference>
        <preference>
            <label>user.language</label>
            <value>${Navigator-DefaultLocale}</value>
        </preference>

        <!-- Paging result size configurations -->

        <preference>
            <label>patient.list.pagesize.set</label>
            <value>10,20,50,100</value>
        </preference>
        <preference>
            <label>doc.list.pagesize.set</label>
            <value>10,20,50,100</value>
        </preference>
        <preference>
            <label>arr.list.pagesize.set</label>
            <value>10,20,50,100</value>
        </preference>
        <preference>
            <label>studies.list.pagesize.set</label>
            <value>10,20,50,100</value>
        </preference>

        <!-- Default document viewers -->   

        <!--preference>
            <label>document.application.dicom.default.viewer</label>
            <value></value>
        </preference-->

        <!-- Sort-by column preferences -->

        <preference>
            <label>dataflows.list.sort.column</label>
            <value>modifiedDateTime</value>
        </preference>
        <preference>
            <label>dataflows.list.sort.column.direction</label>
            <value>0</value>
        </preference>


        <!-- =================================================================== -->
        <!-- ================ Information Preview Configuration ================ -->
        <!-- =================================================================== -->


        <!-- Document metadata preview details -->

        <preference>
            <label>document.metadata.preview.sourcePatient.name</label>
            <value>1,1</value>
        </preference>
        <preference>
            <label>document.metadata.preview.sourcePatient.gender</label>
            <value>1,2</value>
        </preference>
        <preference>
            <label>document.metadata.preview.sourcePatient.birthDate</label>
            <value>1,3</value>
        </preference>
        <!--preference>
            <label>document.metadata.preview.extendedMetadata.accessionNumber</label>
            <value>1,4</value>
        </preference>
        <preference>
            <label>document.metadata.preview.extendedMetadata.referringPhysician</label>
            <value>1,5</value>
        </preference-->

        <!-- Patient info shown on list of documents page -->

        <preference>
            <label>patient.metadata.doc.index.name</label>
            <value>1,1</value>
        </preference>
        <preference>
            <label>patient.metadata.doc.index.gender</label>
            <value>1,2</value>
        </preference>
        <preference>
            <label>patient.metadata.doc.index.birthDate</label>
            <value>1,3</value>
        </preference>

    </prop>

</config>
