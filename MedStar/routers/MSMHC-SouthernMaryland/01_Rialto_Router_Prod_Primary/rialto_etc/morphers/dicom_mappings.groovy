class DICOM_Mappings {

    class PatternMatchers {
        def processingExpression
        def resultString
        def tag
        def modality
        def sequenceTag

        public PatternMatchers(appliesToModality, regExpression, dicomSequenceTag, dicomTag, resultantValue) {
            processingExpression = regExpression
            resultString = resultantValue
            tag = dicomTag
            modality = appliesToModality
            sequenceTag = dicomSequenceTag
        }

    }

    def bodyParts
    def lateralities
    def viewPositions

    //DICOM tag constants
    final TAG_ACQUISITION_DEVICE_PROCESSING_DESCRIPTION = 0x00181400
    final TAG_BODY_PART_EXAMINED = 0x00180015
    final TAG_PERFORMED_PROCEDURE_STEP_DESCRIPTION = 0x00400254
    final TAG_STUDY_DESCRIPTION = 0x00081030
    final TAG_SERIES_DESCRIPTION = 0x0008103e
    final TAG_REQUESTED_PROCEDURE_DESCRIPTION = 0x00321060
    final TAG_PROTOCOL_NAME = 0x00181030
    final TAG_LATERALITY = 0x00200060
    final TAG_VIEW_POSITION = 0x00185101
    final TAG_PATIENT_ORIENTATION = 0x00200020
    final TAG_REQUEST_ATTRIBUTE_SEQUENCE = 0x00400275
    final TAG_PROCEDURE_CODE_SEQUENCE = 0x00081032
    final TAG_CODE_MEANING = 0x00080104
    final TAG_ROOT_OF_OBJECT = 0x00000000
    final TAG_SCHEDULED_PROCEDURE_STEP_DESCRIPTION = 0x00400007

    def DICOM_Mappings() {
        /* DICOM specifies a set of body parts that can be one of:
            HEAD
            HEART
            NECK
            LEG
            ARM
            JAW
            SKULL
            CSPINE
            TSPINE
            LSPINE
            SSPINE
            COCCYX
            CHEST
            CLAVICLE
            BREAST
            ABDOMEN
            PELVIS
            HIP
            SHOULDER
            ELBOW
            KNEE
            ANKLE
            HAND
            FOOT
            EXTREMITY

        Additional body parts that we would like to add would be
            UP_EXM  (upper extremity)
            LOW_EXM (lower extremity)
            LYMPH NODES
            SPINE
        here we define the rules for how body parts will be evaluated.
        priority order for where to find the most accurate information is as follows
         0018,0015 Body Part Examined
         0040,0254 Performed Procedure Step Description
         0018,1400 Acquisition Device Procedure Description
         0008,1030 Study Description
         0008,103e Series Description
         0032,1060 Requested Procedure Description
         0018,1030 Protocol Name
        we may add more places to look in case these are not sufficient for body part determination, but these are pretty good fields to examine.
    */


        //when specifying sequences for PatternMatchers, you must put the name in quotes because the API for fetching 
        //sequences doesn't support passing numbers for some reason.
        bodyParts = [
            new PatternMatchers('CR', /(?i).*chest.*/, TAG_ROOT_OF_OBJECT, TAG_PERFORMED_PROCEDURE_STEP_DESCRIPTION, 'CHEST'),
            new PatternMatchers('CR', /(?i).*chest.*/, TAG_ROOT_OF_OBJECT, TAG_ACQUISITION_DEVICE_PROCESSING_DESCRIPTION, 'CHEST'),
            new PatternMatchers('CR', /(?i).*chest.*/, TAG_ROOT_OF_OBJECT, TAG_STUDY_DESCRIPTION, 'CHEST'),
            new PatternMatchers('CR', /(?i).*chest.*/, "ProcedureCodeSequence", TAG_CODE_MEANING, 'CHEST'),
            new PatternMatchers('CR', /(?i).*chest.*/, TAG_ROOT_OF_OBJECT, TAG_SERIES_DESCRIPTION, 'CHEST'),
            new PatternMatchers('CR', /(?i).*chest.*/, TAG_ROOT_OF_OBJECT, TAG_REQUESTED_PROCEDURE_DESCRIPTION, 'CHEST'),
            new PatternMatchers('CR', /(?i).*chest.*/, "RequestAttributesSequence", TAG_SCHEDULED_PROCEDURE_STEP_DESCRIPTION, 'CHEST'),
            new PatternMatchers('CR', /(?i).*chest.*/, TAG_ROOT_OF_OBJECT, TAG_PROTOCOL_NAME, 'CHEST'),
            new PatternMatchers('CR', /(?i).*foot.*/, TAG_ROOT_OF_OBJECT, TAG_PERFORMED_PROCEDURE_STEP_DESCRIPTION, 'FOOT'),
            new PatternMatchers('CR', /(?i).*foot.*/, TAG_ROOT_OF_OBJECT, TAG_ACQUISITION_DEVICE_PROCESSING_DESCRIPTION, 'FOOT'),
            new PatternMatchers('CR', /(?i).*foot.*/, TAG_ROOT_OF_OBJECT, TAG_STUDY_DESCRIPTION, 'FOOT'),
            new PatternMatchers('CR', /(?i).*foot.*/, "ProcedureCodeSequence", TAG_CODE_MEANING, 'FOOT'),
            new PatternMatchers('CR', /(?i).*foot.*/, TAG_ROOT_OF_OBJECT, TAG_SERIES_DESCRIPTION, 'FOOT'),
            new PatternMatchers('CR', /(?i).*foot.*/, TAG_ROOT_OF_OBJECT, TAG_REQUESTED_PROCEDURE_DESCRIPTION, 'FOOT'),
            new PatternMatchers('CR', /(?i).*foot.*/, "RequestAttributesSequence", TAG_SCHEDULED_PROCEDURE_STEP_DESCRIPTION, 'FOOT'),
            new PatternMatchers('CR', /(?i).*foot.*/, TAG_ROOT_OF_OBJECT, TAG_PROTOCOL_NAME, 'FOOT'),
            new PatternMatchers('CR', /(?i).*l-spine.*/, TAG_ROOT_OF_OBJECT, TAG_PERFORMED_PROCEDURE_STEP_DESCRIPTION, 'LSPINE'),
            new PatternMatchers('CR', /(?i).*l-spine.*/, TAG_ROOT_OF_OBJECT, TAG_ACQUISITION_DEVICE_PROCESSING_DESCRIPTION, 'LSPINE'),
            new PatternMatchers('CR', /(?i).*l-spine.*/, TAG_ROOT_OF_OBJECT, TAG_STUDY_DESCRIPTION, 'LSPINE'),
            new PatternMatchers('CR', /(?i).*l-spine.*/, "ProcedureCodeSequence", TAG_CODE_MEANING, 'LSPINE'),
            new PatternMatchers('CR', /(?i).*l-spine.*/, TAG_ROOT_OF_OBJECT, TAG_SERIES_DESCRIPTION, 'LSPINE'),
            new PatternMatchers('CR', /(?i).*l-spine.*/, TAG_ROOT_OF_OBJECT, TAG_REQUESTED_PROCEDURE_DESCRIPTION, 'LSPINE'),
            new PatternMatchers('CR', /(?i).*l-spine.*/, "RequestAttributesSequence", TAG_SCHEDULED_PROCEDURE_STEP_DESCRIPTION, 'LSPINE'),
            new PatternMatchers('CR', /(?i).*l-spine.*/, TAG_ROOT_OF_OBJECT, TAG_PROTOCOL_NAME, 'LSPINE'),
            new PatternMatchers('CR', /(?i).*finger.*/, TAG_ROOT_OF_OBJECT, TAG_STUDY_DESCRIPTION, 'HAND'),
            new PatternMatchers('CR', /(?i).*neck.*/, TAG_ROOT_OF_OBJECT, TAG_STUDY_DESCRIPTION, 'NECK'),
            new PatternMatchers('CR', /(?i).*wrist.*/, TAG_ROOT_OF_OBJECT, TAG_STUDY_DESCRIPTION, 'HAND'),
            new PatternMatchers('CR', /(?i).*hip.*/, TAG_ROOT_OF_OBJECT, TAG_STUDY_DESCRIPTION, 'HIP'),
            new PatternMatchers('CR', /(?i).*ankle.*/, TAG_ROOT_OF_OBJECT, TAG_STUDY_DESCRIPTION, 'ANKLE'),

            new PatternMatchers('CT', /(?i).*head.*/, TAG_ROOT_OF_OBJECT, TAG_STUDY_DESCRIPTION, 'HEAD'),
            new PatternMatchers('CT', /(?i).*head.*/, "ProcedureCodeSequence", TAG_CODE_MEANING, 'HEAD'),
            new PatternMatchers('CT', /(?i).*head.*/, "RequestAttributesSequence", TAG_SCHEDULED_PROCEDURE_STEP_DESCRIPTION, 'HEAD'),
            new PatternMatchers('CT', /(?i).*brain.*/, TAG_ROOT_OF_OBJECT, TAG_PROTOCOL_NAME, 'HEAD'),
            new PatternMatchers('CT', /(?i).*chest.*abd.*pel.*/, TAG_ROOT_OF_OBJECT, TAG_STUDY_DESCRIPTION, 'CHEST ABD PELV'),
            new PatternMatchers('CT', /(?i).*abd.*pel.*/, TAG_ROOT_OF_OBJECT, TAG_STUDY_DESCRIPTION, 'ABD PELV'),

            new PatternMatchers('NM', /(?i).*lung.*/, "ProcedureCodeSequence", TAG_CODE_MEANING, 'LUNG'),
            new PatternMatchers('NM', /(?i).*bone.*/, "ProcedureCodeSequence", TAG_CODE_MEANING, 'BONE'),
            new PatternMatchers('NM', /(?i).*lymph.*/, "ProcedureCodeSequence", TAG_CODE_MEANING, 'LYMPH NODES'),
            new PatternMatchers('NM', /(?i).*lung.*/, TAG_ROOT_OF_OBJECT, TAG_PERFORMED_PROCEDURE_STEP_DESCRIPTION, 'LUNG'),
            new PatternMatchers('NM', /(?i).*lymph.*/, TAG_ROOT_OF_OBJECT, TAG_PERFORMED_PROCEDURE_STEP_DESCRIPTION, 'LYMPH NODES'),
            new PatternMatchers('NM', /(?i).*bone.*/, TAG_ROOT_OF_OBJECT, TAG_PERFORMED_PROCEDURE_STEP_DESCRIPTION, 'BONE'),

            new PatternMatchers('MR', /(?i).*pelvis.*/, TAG_ROOT_OF_OBJECT, TAG_PERFORMED_PROCEDURE_STEP_DESCRIPTION, 'PELVIS'),
            new PatternMatchers('MR', /(?i).*pelvis.*/, TAG_ROOT_OF_OBJECT, TAG_REQUESTED_PROCEDURE_DESCRIPTION, 'PELVIS'),
            new PatternMatchers('MR', /(?i).*neck.*/, TAG_ROOT_OF_OBJECT, TAG_PERFORMED_PROCEDURE_STEP_DESCRIPTION, 'NECK'),
            new PatternMatchers('MR', /(?i).*neck.*/, TAG_ROOT_OF_OBJECT, TAG_REQUESTED_PROCEDURE_DESCRIPTION, 'NECK'),
            new PatternMatchers('MR', /(?i).*brain.*/, TAG_ROOT_OF_OBJECT, TAG_PERFORMED_PROCEDURE_STEP_DESCRIPTION, 'BRAIN'),
            new PatternMatchers('MR', /(?i).*brain.*/, TAG_ROOT_OF_OBJECT, TAG_REQUESTED_PROCEDURE_DESCRIPTION, 'BRAIN'),
            new PatternMatchers('MR', /(?i).*shoulder.*/, TAG_ROOT_OF_OBJECT, TAG_PERFORMED_PROCEDURE_STEP_DESCRIPTION, 'SHOULDER'),
            new PatternMatchers('MR', /(?i).*spine.*lumbar.*/, TAG_ROOT_OF_OBJECT, TAG_PERFORMED_PROCEDURE_STEP_DESCRIPTION, 'LSPINE'),
            new PatternMatchers('MR', /(?i).*lumbar.*spine.*/, TAG_ROOT_OF_OBJECT, TAG_PERFORMED_PROCEDURE_STEP_DESCRIPTION, 'LSPINE'),
            new PatternMatchers('MR', /(?i).*extrem.*up.*/, TAG_ROOT_OF_OBJECT, TAG_PERFORMED_PROCEDURE_STEP_DESCRIPTION, 'UP_EXM'),
            new PatternMatchers('MR', /(?i).*hip.*/, TAG_ROOT_OF_OBJECT, TAG_PERFORMED_PROCEDURE_STEP_DESCRIPTION, 'HIP'),
            new PatternMatchers('MR', /(?i).*knee.*/, TAG_ROOT_OF_OBJECT, TAG_PERFORMED_PROCEDURE_STEP_DESCRIPTION, 'KNEE'),
            new PatternMatchers('MR', /(?i).*spine.*cervical.*/, TAG_ROOT_OF_OBJECT, TAG_PERFORMED_PROCEDURE_STEP_DESCRIPTION, 'CSPINE'),
            new PatternMatchers('MR', /(?i).*cervical.*spine.*/, TAG_ROOT_OF_OBJECT, TAG_PERFORMED_PROCEDURE_STEP_DESCRIPTION, 'CSPINE'),
            new PatternMatchers('MR', /(?i).*spine.*thoracic.*/, TAG_ROOT_OF_OBJECT, TAG_PERFORMED_PROCEDURE_STEP_DESCRIPTION, 'TSPINE'),
            new PatternMatchers('MR', /(?i).*thoracic.*spine.*/, TAG_ROOT_OF_OBJECT, TAG_PERFORMED_PROCEDURE_STEP_DESCRIPTION, 'TSPINE'),
            new PatternMatchers('MR', /(?i).*foot.*/, TAG_ROOT_OF_OBJECT, TAG_PERFORMED_PROCEDURE_STEP_DESCRIPTION, 'FOOT'),
            new PatternMatchers('MR', /(?i).*neck.*/, "RequestedProcedureCodeSequence", TAG_CODE_MEANING, 'NECK'),
            new PatternMatchers('MR', /(?i).*pelvis.*/, "RequestedProcedureCodeSequence", TAG_CODE_MEANING, 'PELVIS'),
            new PatternMatchers('MR', /(?i).*brain.*/, "RequestedProcedureCodeSequence", TAG_CODE_MEANING, 'BRAIN'),
            new PatternMatchers('MR', /(?i).*shoulder.*/, "RequestedProcedureCodeSequence", TAG_CODE_MEANING, 'SHOULDER'),
            new PatternMatchers('MR', /(?i).*foot.*/, "RequestedProcedureCodeSequence", TAG_CODE_MEANING, 'FOOT'),
            new PatternMatchers('MR', /(?i).*brain.*/, "RequestedProcedureCodeSequence", TAG_CODE_MEANING, 'BRAIN'),
            new PatternMatchers('MR', /(?i).*shoulder.*/, "RequestedProcedureCodeSequence", TAG_CODE_MEANING, 'SHOULDER'),
            new PatternMatchers('MR', /(?i).*spine.*lumbar.*/, "RequestedProcedureCodeSequence", TAG_CODE_MEANING, 'LSPINE'),
            new PatternMatchers('MR', /(?i).*lumbar.*spine.*/, "RequestedProcedureCodeSequence", TAG_CODE_MEANING, 'LSPINE'),
            new PatternMatchers('MR', /(?i).*extrem.*upper.*/, "RequestedProcedureCodeSequence", TAG_CODE_MEANING, 'UP_EXM'),
            new PatternMatchers('MR', /(?i).*hip.*/, "RequestedProcedureCodeSequence", TAG_CODE_MEANING, 'HIP'),
            new PatternMatchers('MR', /(?i).*knee.*/, "RequestedProcedureCodeSequence", TAG_CODE_MEANING, 'KNEE'),
            new PatternMatchers('MR', /(?i).*spine.*cervical.*/, "RequestedProcedureCodeSequence", TAG_CODE_MEANING, 'CSPINE'),
            new PatternMatchers('MR', /(?i).*cervical.*spine.*/, "RequestedProcedureCodeSequence", TAG_CODE_MEANING, 'CSPINE'),
            new PatternMatchers('MR', /(?i).*spine.*thoracic.*/, "RequestedProcedureCodeSequence", TAG_CODE_MEANING, 'TSPINE'),
            new PatternMatchers('MR', /(?i).*thoracic.*spine.*/, "RequestedProcedureCodeSequence", TAG_CODE_MEANING, 'TSPINE'),
            new PatternMatchers('MR', /(?i).*extrem.*low.*/, "RequestAttributesSequence", TAG_SCHEDULED_PROCEDURE_STEP_DESCRIPTION, 'LOW_EXM'),
            new PatternMatchers('MR', /(?i).*extrem.*up.*/, "RequestAttributesSequence", TAG_SCHEDULED_PROCEDURE_STEP_DESCRIPTION, 'UP_EXM'),

            new PatternMatchers('XA', /(?i).*upper.*ext.*/, TAG_ROOT_OF_OBJECT, TAG_PERFORMED_PROCEDURE_STEP_DESCRIPTION, 'UP_EXM'),
            new PatternMatchers('XA', /(?i).*cerebral.*/, TAG_ROOT_OF_OBJECT, TAG_PERFORMED_PROCEDURE_STEP_DESCRIPTION, 'HEAD'),

            new PatternMatchers('US', /(?i).*paracentesis.*/, TAG_ROOT_OF_OBJECT, TAG_PERFORMED_PROCEDURE_STEP_DESCRIPTION, 'ABDOMEN'),
            new PatternMatchers('US', /(?i).*renal.*/, TAG_ROOT_OF_OBJECT, TAG_PERFORMED_PROCEDURE_STEP_DESCRIPTION, 'KIDNEY'),
            new PatternMatchers('US', /(?i).*abdomen.*/, TAG_ROOT_OF_OBJECT, TAG_PERFORMED_PROCEDURE_STEP_DESCRIPTION, 'ABDOMEN'),
            new PatternMatchers('US', /(?i).*abd.*/, TAG_ROOT_OF_OBJECT, TAG_PERFORMED_PROCEDURE_STEP_DESCRIPTION, 'ABDOMEN'),
            new PatternMatchers('US', /(?i).*upper.*extrem.*/, TAG_ROOT_OF_OBJECT, TAG_SERIES_DESCRIPTION, 'UP_EXM'),
            new PatternMatchers('US', /(?i).*low.*extrem.*/, TAG_ROOT_OF_OBJECT, TAG_SERIES_DESCRIPTION, 'LOW_EXM'),
            new PatternMatchers('US', /(?i).*thoracentesis.*/, TAG_ROOT_OF_OBJECT, TAG_PERFORMED_PROCEDURE_STEP_DESCRIPTION, 'LUNG'),
            new PatternMatchers('US', /(?i).*ob.*less.*than.*/, TAG_ROOT_OF_OBJECT, TAG_STUDY_DESCRIPTION, 'ABDOMEN'),
            new PatternMatchers('US', /(?i).*trans.*vag.*/, TAG_ROOT_OF_OBJECT, TAG_STUDY_DESCRIPTION, 'ABDOMEN'),
            new PatternMatchers('US', /(?i).*ble .*/, TAG_ROOT_OF_OBJECT, TAG_PERFORMED_PROCEDURE_STEP_DESCRIPTION, 'LOW_EXM'),  //trailing space intentional
            new PatternMatchers('US', /(?i).*obstetrical.*/, TAG_ROOT_OF_OBJECT, TAG_STUDY_DESCRIPTION, 'ABDOMEN'),
            new PatternMatchers('US', /(?i).*low.*extrem.*/, TAG_ROOT_OF_OBJECT, TAG_STUDY_DESCRIPTION, 'LOW_EXM'),
            new PatternMatchers('US', /(?i).*bpp.*nst.*/, TAG_ROOT_OF_OBJECT, TAG_PERFORMED_PROCEDURE_STEP_DESCRIPTION, 'ABDOMEN')
        ]

    /*    DICOM specifies a set of lateralities that can be one of:
            R = right
            L = left
            U = unpaired
            B = both left and right

        here we define the rules for how body parts will be evaluated.
        priority order for where to find the most accurate information is as follows
         0020,0060 Laterality
         0008,1030 Study Description
         0018,1400 Acquisition Device Procedure Description
         0040,0254 Performed Procedure Step Description
         0008,103e Series Description
        we may add more places to look in case these are not sufficient for some laterality determination, but these are pretty good fields to examine.
    */

        lateralities = [
            //rules for CR
            new PatternMatchers('CR', /(?i)xry.*-l/, TAG_ROOT_OF_OBJECT, TAG_STUDY_DESCRIPTION, 'L'), //starts with xry and ends in -l
            new PatternMatchers('CR', /(?i).*left.*/, TAG_ROOT_OF_OBJECT, TAG_ACQUISITION_DEVICE_PROCESSING_DESCRIPTION, 'L'),
            new PatternMatchers('CR', /(?i).*left.*/, TAG_ROOT_OF_OBJECT, TAG_PERFORMED_PROCEDURE_STEP_DESCRIPTION, 'L'),
            new PatternMatchers('CR', /(?i)xry.*-r/, TAG_ROOT_OF_OBJECT, TAG_STUDY_DESCRIPTION, 'R'), //starts with xry and ends in -r
            new PatternMatchers('CR', /(?i).*right.*/, TAG_ROOT_OF_OBJECT, TAG_ACQUISITION_DEVICE_PROCESSING_DESCRIPTION, 'R'),
            new PatternMatchers('CR', /(?i).*right.*/, TAG_ROOT_OF_OBJECT, TAG_PERFORMED_PROCEDURE_STEP_DESCRIPTION, 'R'),

            new PatternMatchers('US', /(?i).*right.*/, TAG_ROOT_OF_OBJECT, TAG_PERFORMED_PROCEDURE_STEP_DESCRIPTION, 'R'),
            new PatternMatchers('US', /(?i).*left.*/, TAG_ROOT_OF_OBJECT, TAG_PERFORMED_PROCEDURE_STEP_DESCRIPTION, 'L'),
            new PatternMatchers('US', /(?i).*right.*/, TAG_ROOT_OF_OBJECT, TAG_STUDY_DESCRIPTION, 'R'),
            new PatternMatchers('US', /(?i).*left.*/, TAG_ROOT_OF_OBJECT, TAG_STUDY_DESCRIPTION, 'L'),

            new PatternMatchers('MR', /(?i).*right.*/, TAG_ROOT_OF_OBJECT, TAG_PERFORMED_PROCEDURE_STEP_DESCRIPTION, 'R'),
            new PatternMatchers('MR', /(?i).*left.*/, TAG_ROOT_OF_OBJECT, TAG_PERFORMED_PROCEDURE_STEP_DESCRIPTION, 'L')
        ]

    /*
        DICOM specifies a set of view positions that can be one of:
                AP = Anterior/Posterior
                PA = Posterior/Anterior
                LL = Left Lateral
                RL = Right Lateral
                RLD = Right Lateral Decubitus
                LLD = Left Lateral Decubitus
                RLO = Right Lateral Oblique
                LLO = Left Lateral Oblique

        here we define the rules for how view positions will be evaluated.
        priority order for where to find the most accurate information is as follows
         0018,5101 View Position
         0020,0020 Patient Orientation
         0018,1400 Acquisition Device Procedure Description
        we may add more places to look in case these are not sufficient for view position determination, but these are pretty good fields to examine.

        near the bottom of these mappings are some additional view positions that are outside the DICOM standard. They are included for when we cannot
        determine the full view position.
    */

        viewPositions = [
            //rules for CR
            new PatternMatchers('CR', /(?i).*ap.*/, TAG_ROOT_OF_OBJECT, TAG_ACQUISITION_DEVICE_PROCESSING_DESCRIPTION, 'AP'),
            new PatternMatchers('CR', /(?i).*pa.*/, TAG_ROOT_OF_OBJECT, TAG_ACQUISITION_DEVICE_PROCESSING_DESCRIPTION, 'PA'),
            new PatternMatchers('CR', /(?i).*lateral.*/, TAG_ROOT_OF_OBJECT, TAG_ACQUISITION_DEVICE_PROCESSING_DESCRIPTION, 'LAT'),
            new PatternMatchers('CR', /(?i).*lat.*/, TAG_ROOT_OF_OBJECT, TAG_ACQUISITION_DEVICE_PROCESSING_DESCRIPTION, 'LAT'),

            new PatternMatchers('CR', /(?i).*ap/, TAG_ROOT_OF_OBJECT, TAG_STUDY_DESCRIPTION, 'AP'), //ends in ap
            new PatternMatchers('CR', /(?i).*ap.*/, TAG_ROOT_OF_OBJECT, TAG_STUDY_DESCRIPTION, 'AP'), //contains ap

            new PatternMatchers('CR', /(?i).*pa/, TAG_ROOT_OF_OBJECT, TAG_STUDY_DESCRIPTION, 'PA'), //ends in pa
            new PatternMatchers('CR', /(?i).*pa.*/, TAG_ROOT_OF_OBJECT, TAG_STUDY_DESCRIPTION, 'PA'), //contains pa

            new PatternMatchers('CR', /(?i).*obl.*/, TAG_ROOT_OF_OBJECT, TAG_ACQUISITION_DEVICE_PROCESSING_DESCRIPTION, 'OBL')
        ]
    }
}
