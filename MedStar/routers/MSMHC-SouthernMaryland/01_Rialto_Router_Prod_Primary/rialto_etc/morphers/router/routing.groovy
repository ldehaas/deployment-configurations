def called_AET = getCalledAETitle().toUpperCase();
def calling_AET = getCallingAETitle().toUpperCase();

if (calling_AET.equals("FD20_1")) {
        return "etc/dataflows/ERSRAD_MCKESSON_P0T7.xml";
}

if (calling_AET.equals("FD20_2")) {
        return "etc/dataflows/ERSRAD_MCKESSON_P0T7.xml";
}

if (calling_AET.equals("TERRA")) {
        return "etc/dataflows/ERSRAD_MCKESSON_P0T7.xml";
}

if (calling_AET.equals("PHILIPS_LAB3")) {
        return "etc/dataflows/ERSRAD_MCKESSON_P0T7.xml";
}

if (called_AET.equals("RIALTO_SMHC_RAD")) {
        return "etc/dataflows/ERSRAD_MCKESSON_P0T7.xml";
}

else if (called_AET.equals("RIALTO_SMHC_CAR")) {
        return "etc/dataflows/ERSCAR_MCKESSON_P0T7.xml";
}

else if (called_AET.equals("RIALTO_SMHC_OTH")) {
        return "etc/dataflows/ERSOTH_MCKESSON_P0T7.xml";
}
else if (called_AET.equals("MSMHC_PACS")) {
		return "etc/dataflows/MCKESSON_only.xml";
}
else if (called_AET.equals("STATRAD")) {
		return "etc/dataflows/STATRAD.xml";
}
else
return "etc/dataflows/multiple-destinations.xml"
