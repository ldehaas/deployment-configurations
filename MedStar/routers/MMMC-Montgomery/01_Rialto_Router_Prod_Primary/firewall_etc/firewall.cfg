#
# Rialto Firewall Configuration
#
# This configuration file is read by the Rialto configure-rialto-firewall
# script.  The script implements a standardized Linux firewall set up for
# Rialto but supplements the firewall rules with the configuration supplied
# by this script.
#
# Please consult the Rialto product documentation for more information.
#

#################################
# Accept TCP Traffic by Port & IP
#################################
# Syntax:
#     open port [ip]
# Where:
#     port can be a port or port range
#         e.g. 443
#         e.g. 8080:8085
#     ip can be an IP address, subnet, or comma separated list of the same
#         e.g. 10.242.22.47
#         e.g. 10.242.22.0/24
#         e.g. 10.242.22.0/255.255.255.0
#         e.g. 10.242.22.47,10.242.23.0/24,10.242.250.0/24
#
# Please note that the firewall configuration script automatically:
# - Opens port 22 for SSH from any IP address
# - Automatically allows inter-node packets for Cassandra
# - Automatically configures firewall for Corosync if installed
##########################################################################

# EXAMPLES:

# Accept packets on this port from this IP address
# open 11112 10.242.43.44

# Accept packets on this port from any IP address (wide open)
# open 443

# Accept packets from all devices on a subnet
# open 5900:5932 10.242.43.0/24
# -or-
# open 11112 10.242.43.0/255.255.255.0

# ***********************
# *** Cassandra Nodes ***
# ***********************
# Configure this on all Cassandra nodes in the cluster.  Accept database
# client connections from all Rialto App nodes on these two ports:
# open 9042 10.242.43.44,10.242.43.47,10.242.43.50
# open 9160 10.242.43.44,10.242.43.47,10.242.43.50


#####################
# TCP Port Forwarding
#####################
# Syntax:
#     forward source-port destination-port
# Where
#     source-port is an external port to accept traffic on
#     destination-port is an *open* port to forward traffic to
#
# Note that the destination port must be open.
#################################################################

# EXAMPLES:

# Forward packets received on port 443 to Rialto Navigator on port 2525.
# open 2525
# forward 443 2525

# Forward packets received on port 80 to Rialto Navigator on port 8080.
# open 8080
# forward 80 8080

# Accept UDP packets for Audit Record Repository on ports 4514 and 2514
# openUDP 4514
# forwardUDP 2514 4514


##########################
# Manually specified rules
##########################
# Syntax:
#     iptables ...
# See man iptables(8)
##########################

# EXAMPLES:

# Accept packets on port 8080 only on network interface "eth0"
# iptables -A INPUT -p tcp --dport 8080 -i eth0 -j ACCEPT

# Accept NTP queries (uses UDP, not TCP)
# iptables -A INPUT -m state --state NEW -p udp --dport 123 -j ACCEPT

open 2525
open 2524
open 4104
open 2390 
open 2399
open 2299
open 8090
open 2099
open 5432
# open port 4444 as requested in KHC 4665.
open 4444
forward 2382 2390
forward 2383 2390
forward 2384 2390
forward 2385 2390
forward 2386 2390
forward 2387 2390
forward 2388 2390
forward 2389 2390
forward 2380 2390
forward 2381 2390
forward 2400 2399
forward 2401 2399
forward 2402 2399
forward 2403 2399
forward 2404 2399
forward 2405 2399
forward 2406 2399
forward 2407 2399
forward 2408 2399
forward 2409 2399
forward 2297 2299
forward 2298 2299
forward 443 2525
forward 8090 8080
forward 2100 2099
forward 2101 2099
forward 2102 2099
forward 2103 2099
forward 2104 2099
forward 2105 2099
forward 2106 2099
forward 2107 2099
forward 2108 2099
forward 2109 2099

