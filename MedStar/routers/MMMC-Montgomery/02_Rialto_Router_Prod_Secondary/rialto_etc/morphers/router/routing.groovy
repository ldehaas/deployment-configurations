/**
* This script will decide which dataflow template name to return.
*
* A switch statement is used to test the calling AET
*/

def McKesson_Pacs_AET = "ALI_SCU"; /* This variable defines the name of the calling  McKesson AET */
def Dell_IMG_AET = "INDEXQUERY-P0RA"; /* This variable defines the name of the calling Dell Image Archive KHC4967 */

/* Called AETitles */
def ERS_RAD_SOUTH = "ERS_RAD_SOUTH";
def Rialto_Cache_Prod = "RIALTO_CACHE";
def Rialto_Cache_Test = "RIALTO_CACHE_T"; 
def Rialto_Router_Test = "RIALTO_ROUTER_T";
def Rialto_McKesson = "RIALTO_MCK";

def df_name = null;

def calling_AET = getCallingAETitle().toUpperCase();
def called_AET  = getCalledAETitle().toUpperCase();
def modality = get(Modality);

switch (calling_AET) {

    case McKesson_Pacs_AET:

        df_name = "etc/dataflows/McKesson_Pacs.xml";
        if  (modality.equalsIgnoreCase("NM")) {
                dfName = "etc/dataflows/McKesson_Pacs_nm.xml";
        }
        break

    case Dell_IMG_AET:

        switch (called_AET) {

            case ERS_RAD_SOUTH:
                df_name = "etc/dataflows/only-rialto-and-compress.xml";
                if  (modality.equalsIgnoreCase("NM")) {
                      dfName = "etc/dataflows/only-rialto-and-compress_nm.xml";
                }
                break

            case Rialto_Cache_Prod:
                df_name = "etc/dataflows/only-rialto-prod-and-compress.xml";
                if  (modality.equalsIgnoreCase("NM")) {
                      dfName = "etc/dataflows/only-rialto-prod-and-compress_nm.xml";
                }
                break

            case Rialto_Cache_Test:
                df_name = "etc/dataflows/only-rialto-test-and-compress.xml";
                break

            case Rialto_Router_Test:
                df_name = "etc/dataflows/only-rialto-test-router-and-no-compress.xml";
                break

            case Rialto_McKesson:
                df_name = "etc/dataflows/mckesson-only.xml";
                break

            default:
                df_name = "etc/dataflows/only-rialto-and-compress.xml";
                if  (modality.equalsIgnoreCase("NM")) {
                      dfName = "etc/dataflows/only-rialto-and-compress_nm.xml";
                }
                break

        }

        default:

                df_name = "etc/dataflows/default.xml";
                if  (modality.equalsIgnoreCase("NM")) {
                      dfName = "etc/dataflows/default_nm.xml";
                }
                break

}

return df_name

