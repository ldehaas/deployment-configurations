
import org.dcm4che2.data.Tag

/*
 * helpers
 */
  
write_tags = { tags, format='%30s: %s\n' ->
    def fmt = new Formatter(output)
        
    for (def tag : tags) {
        def val = get(tag)
        if (val == null) {
            val = ''
        }
        
        def name = '?'
        
        if (tag instanceof String) {
            def path = Tag.toTagPath(tag)
            name = input.getDicomObject().nameOf(path[-1])
        } else {
            name = input.getDicomObject().nameOf(tag)
        }
         
        fmt.format(format, name, val)
    }
}

/**
 * find DicomElements that has ValueType == 'TEXT'
 * @param dcm dcm4che's dicom object (not the scripting api wrapper!)
 */
find_text_elements = { dcm ->
    def ret = []
    
    if (dcm.getString(ValueType) == 'TEXT') {
        ret.add(dcm)
    }
    
    def iter = dcm.iterator()
    while (iter.hasNext()) {
        def elem = iter.next()
        if (elem.hasDicomObjects()) {
            for (int i=0; i<elem.countItems(); i++) {
                ret.addAll( find_text_elements( elem.getDicomObject(i) ) )
            }
        }
    }
    
    return ret
}

/*
 * main body
 */
 
write_tags([
    PatientID,
    PatientName,
    PatientBirthDate,
])
writeln()

write_tags([
    AccessionNumber,
    StudyDescription,
    StudyDate,
    ReferringPhysicianName,
    CompletionFlag,
    VerificationFlag,
    'VerifyingObserverSequence/VerifyingObserverName',
])
writeln()

writeln('------------------------------------------------------------')
writeln()
def title = get('ContentSequence[1]/TextValue')
if (title == null) {
    title = 'Diagnostic Imaging Report'
}
writeln(title)
writeln()

def text_elems = find_text_elements( input.getDicomObject() )
def path_meaning = Tag.toTagPath(
    'ConceptNameCodeSequence/CodeMeaning')
def prev_meaning = null

for (def elem : text_elems) {
    def meaning = elem.getString(path_meaning)
    if (meaning != prev_meaning) {
        writeln()
        write(meaning == null ? '?' : meaning)
        writeln(":")
        prev_meaning = meaning
    }
    
    def val = elem.getString(TextValue)
    if (val) {
        writeln(elem.getString(TextValue))
    }
}

writeln()
writeln('------------------------------------------------------------')
writeln('This page is generated from dicom SR object by Rialto Vault')
