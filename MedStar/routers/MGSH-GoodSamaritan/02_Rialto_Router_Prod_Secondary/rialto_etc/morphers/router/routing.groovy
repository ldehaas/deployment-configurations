/**
*This groovy script is used to return the correct dataflow xml file name for a particular modality
* (calling ae Title).  
*
*
*
*
*/

/* load in the common class that loads the calling AET to dataflow template map  */
LOAD("dataflow_modality_mapping_common.groovy")

/* use the Get_Dataflow_templete method to lookup the calling AET */
String dfName = DataflowModalityMapping.GetDataflowTemplete(getCallingAETitle())

def MGSHCAR = "RIALTO_MGSH_CAR";
def MGSHOTH = "RIALTO_MGSH_OTH";
def MGSHRAD = "RIALTO_MGSH_RAD";

def Rialto_Cache_Prod = "RIALTO_CACHE";
def Rialto_Cache_Test = "RIALTO_CACHE_T"; 
def Rialto_Router_Test = "RIALTO_ROUTER_T";

def calledAET = getCalledAETitle().toUpperCase();
def modality = get(Modality);

    /* This method will check called AE and route the studies to the correct destinations. 
    */

switch (calledAET) {
                case MGSHCAR:
                        dfName = "etc/dataflows/rialto_mgsh.xml";
                        if  (modality.equalsIgnoreCase("NM")) {
                              dfName = "etc/dataflows/rialto_mgsh_nm.xml";
                        }
                        break

                case MGSHOTH:
                        dfName = "etc/dataflows/rialto_mgsh_oth.xml";
                        if  (modality.equalsIgnoreCase("NM")) {
                              dfName = "etc/dataflows/rialto_mgsh_oth_nm.xml";
                        }
                        break

                case MGSHRAD:
                        dfName = "etc/dataflows/rialto_mgsh_rad.xml";
                        if  (modality.equalsIgnoreCase("NM")) {
                              dfName = "etc/dataflows/rialto_mgsh_rad_nm.xml";
                        }
                        break

                case Rialto_Cache_Prod:
                        dfName = "etc/dataflows/rialto_cache_prod.xml";
                        if  (modality.equalsIgnoreCase("NM")) {
                              dfName = "etc/dataflows/rialto_cache_prod_nm.xml";
                        }
                        break

                case Rialto_Cache_Test:
                        dfName = "etc/dataflows/rialto_cache_test.xml";
                        if  (modality.equalsIgnoreCase("NM")) {
                              dfName = "etc/dataflows/rialto_cache_test_nm.xml";
                        }
                        break

                case Rialto_Router_Test:
                        dfName = "etc/dataflows/rialto_router_test.xml";
                        break
}

log.info("Dataflow template found for modality " + getCallingAETitle()  + " is " + dfName);

/* return the dataflow template xml file name */
return dfName;

