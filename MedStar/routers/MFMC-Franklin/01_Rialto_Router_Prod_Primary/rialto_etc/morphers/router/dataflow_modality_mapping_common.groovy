/**
*  This common class is used to provide access to the static map which holds the calling AET to
*  dataflow template xml file name entry.
*/
class DataflowModalityMapping {


    /**
     * This static method will return the file name of the dataflow template for a particular calling AET
     */
    static GetDataflowTemplete (callingAE) {

        def dfValue = null
        def pathPrefix = "etc/dataflows/"

        dfValue  = DataflowModalityMap[callingAE]

        return pathPrefix.concat(dfValue);
    }

    /**
     * This static variable holds the Map with the modality AETitles
     * as the key, and the dataflow template xml file name as the value.
     *
     * When new entries are added ensure proper syntax for a map. Format should be:
     *  'ae_title of modality'   colon   'name_of_dataflow_xml_file'  comma
     *
     * New entries should be added in alphabetic order.
     */
    private static final DataflowModalityMap = [
    'ACUS2000'             :  'pass-through-radiology.xml',
    'Amalga_Sender_1'    :  'pass-through-non-radiology.xml',
    'SonovaE'          :  'pass-through-non-radiology.xml',
    'VIVID7-011277'    : 'pass-through-non-radiology.xml',
    'VIVID7-011716'    :  'pass-through-non-radiology.xml'     /* last entry in map take note no comma at end */
    ]




}

