/**
*This groovy script is used to return the correct dataflow xml file name for a particular modality
* (calling ae Title).  
*
*
*
*
*/

/* load in the common class that loads the calling AET to dataflow template map  */
/* LOAD("dataflow_modality_mapping_common.groovy") */

/* use the Get_Dataflow_templete method to lookup the calling AET */
/* String dfName = DataflowModalityMapping.GetDataflowTemplete(getCallingAETitle()) */

String dfName = "etc/dataflows/pass-through.xml"

def MFSHCAR = "RIALTO_MFSMC_CAR";
def MFSHOTH = "RIALTO_MFSMC_OTH";
def MFSHRAD = "RIALTO_MFSMC_RAD";
def calledAET = getCalledAETitle().toUpperCase();


    /* This method will check called AE and route the studies to the correct destinations. 
    */

switch (calledAET) {
                case MFSHCAR:
                        dfName = "etc/dataflows/rialto_mfsh_car.xml";
                        break
                        
                case MFSHOTH:
                        dfName = "etc/dataflows/rialto_mfsh_oth.xml";
                        break
                
                case MFSHRAD:
                        dfName = "etc/dataflows/rialto_mfsh_rad.xml";
                        break
}

log.info("Dataflow template found for modality " + getCallingAETitle()  + " is " + dfName);

/* return the dataflow template xml file name */
return dfName;

