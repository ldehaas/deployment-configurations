/**
*This groovy script is used to return the correct dataflow xml file name for a particular modality
* (calling ae Title).  
*
*
*
*
*/

/* load in the common class that loads the calling AET to dataflow template map  */
LOAD("dataflow_modality_mapping_common.groovy")

/* use the Get_Dataflow_templete method to lookup the calling AET */
String dfName = DataflowModalityMapping.GetDataflowTemplete(getCallingAETitle())

log.info("Dataflow template found for modality " + getCallingAETitle()  + " is " + dfName);

def todayDate = new Date().format('EE')

if (get(StudyDescription) != null && (get(Modality) != null) ) {
    if ((get(Modality).equalsIgnoreCase("MG")) && (get(StudyDescription).contains("SCRN")) || (get(StudyDescription).toLowerCase().contains("screen")) && (todayDate == "Tue" || todayDate == "Thu") ) {
        dfName = "etc/dataflows/WHCScreeningMammo.xml"
        log.info("routing.groovy: Using alternate workflow for modality: MG Description: SCRN/screen and day of week: '{}'", todayDate)
    }
}

/* return the dataflow template xml file name */
return dfName;
