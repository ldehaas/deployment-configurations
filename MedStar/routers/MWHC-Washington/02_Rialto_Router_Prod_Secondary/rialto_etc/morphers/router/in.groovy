/**
 * This script handles the in-morphing for MWHC.
 *
 *  All objects sent will be stamped with the
 *  IPID value and various other values within the study.
 *  All objects will have the PatientID value checked, if null or empty
 *  set value. 
 */

if (get(IssuerOfPatientID) ==  null  && getCallingAETitle() == "indexQuery-P005") {
    set(IssuerOfPatientID, 'WB4')
    set("IssuerOfPatientIDQualifiersSequence/UniversalEntityID", '2.16.840.1.114107.1.1.16.2.3')
    set("IssuerOfPatientIDQualifiersSequence/UniversalEntityIDType", 'ISO')
}

