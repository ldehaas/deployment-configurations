/**
*This groovy script is used to return the correct dataflow xml file name for a particular modality
* (calling ae Title).  
*
*
*
*
*/

/* load in the common class that loads the calling AET to dataflow template map  */
LOAD("dataflow_modality_mapping_common.groovy")

/* use the Get_Dataflow_templete method to lookup the calling AET */
String dfName = DataflowModalityMapping.GetDataflowTemplete(getCallingAETitle())

log.info("Dataflow template found for modality " + getCallingAETitle()  + " is " + dfName);

/* return the dataflow template xml file name */
return dfName; 
