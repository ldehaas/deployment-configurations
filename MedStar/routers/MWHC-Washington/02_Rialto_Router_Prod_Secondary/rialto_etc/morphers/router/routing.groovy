/**
* This script will decide which dataflow template name to return.
*/
def called_AET = getCalledAETitle().toUpperCase();
def CardioIMG_AET = "INDEXSTORE-P05V";
def LAFSecurView_AET = "MSLFMV01";

if (called_AET.equals(CardioIMG_AET)) {
        log.info("Cardio studies sent through Cardio IMG indexStore-P05V");
        return "etc/dataflows/indexStoreP05V.xml";
}
if (called_AET.equals(LAFSecurView_AET)) {

        log.info("Called AET is Amalga_AET will send only to Lafayette SecurView");
        return "etc/dataflows/LAFSECURVIEW.xml";
}
if (get(Modality) != null) {
        if  (get(Modality).equalsIgnoreCase("MG")) {
        log.info("Mammo studies sent through Tomo capable IMG indexStore-P0UY");
        return "etc/dataflows/mammotomo.xml";
        }
}

return "etc/dataflows/default.xml"
