// This destination (Amalga) doesn't support PR.
// We use this script to drop those SOP types, but forward the rest.
def sopClassUid = get(SOPClassUID)

if (sopClassUid == '1.2.840.10008.5.1.4.1.1.11.1' || sopClassUid == '1.2.840.10008.5.1.4.1.1.11.2' || sopClassUid == '1.2.840.10008.5.1.4.1.1.11.3' || sopClassUid == '1.2.840.10008.5.1.4.1.1.11.4' || sopClassUid == '1.2.840.10008.5.1.4.1.1.11.5') {
    log.debug("Not forwarding SOP {} because it has SOP Class UID {}", get(SOPInstanceUID), sopClassUid)
    return false
}

return true
