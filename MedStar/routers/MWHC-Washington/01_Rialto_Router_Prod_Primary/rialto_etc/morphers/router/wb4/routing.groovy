/**
*This groovy script is used to return the correct dataflow xml file name for a particular modality
* (calling ae Title).  
*/

/* load in the common class that loads the calling AET to dataflow template map  */
LOAD("dataflow_modality_mapping_common.groovy")

def todayDate = new Date().format('EE')
def called_AET = getCalledAETitle().toUpperCase();
def calling_AET = getCallingAETitle().toUpperCase();

if (called_AET.equals("MWHCPACS")) {
    log.info("Called AET is MWHCPACS will send only to MWHC iSite");
    return "etc/dataflows/STENTOR_SCP_172_25_240_48.xml";
}

if (called_AET.equals("MWHCDYNACAD")) {
    log.info("Called AET is MWHCDYNACAD will send only to MWHC Dynacad, Decompressed");
        return "etc/dataflows/MWHCDYNACAD.xml";
}

if (called_AET.equals("MSLFMV01")) {
    log.info("Called AET is MSLFMV01 will send only to MWHC Dynacad, Decompressed");
        return "etc/dataflows/MSLFMV01.xml";
}


/* use the Get_Dataflow_templete method to lookup the calling AET */
String dfName = DataflowModalityMapping.GetDataflowTemplete(getCallingAETitle())


if (get(StudyDescription) != null && get(Modality) != null) {
    if ((getCallingAETitle().contains("CBHTECH")) && ((get(StudyDescription).contains("SCRN")) || (get(StudyDescription).toLowerCase().contains("screen"))) ) {
        return "etc/dataflows/WHCScreeningMammoScanDocs.xml";
    }
        else if ( (get(Modality).equalsIgnoreCase("MG")) && ((get(StudyDescription).contains("SCRN")) || (get(StudyDescription).toLowerCase().contains("screen"))) ) {
        return "etc/dataflows/WHCScreeningMammo.xml";
    }
    else if ( (get(Modality).equalsIgnoreCase("OT")) && ((get(StudyDescription).contains("SCRN")) || (get(StudyDescription).toLowerCase().contains("screen"))) ) {
        return "etc/dataflows/WHCScreeningMammoScanDocs.xml";
    }
    else if ((get(Modality).equalsIgnoreCase("SR")) && ((get(StudyDescription).contains("SCRN")) || (get(StudyDescription).toLowerCase().contains("screen")))) {
        return "etc/dataflows/WHCMammoCADtoLaf.xml";
    }
}

if (get(Modality) != null && get(Modality).equalsIgnoreCase("NM")){
    switch (calling_AET) {
        case 'D_SPECT':
            return "etc/dataflows/AMALGA_WHC_1_NM.xml";
        case 'DICOMlink':
            return "etc/dataflows/AMALGA_WHC_1_NM.xml";
        case 'PET-CT_HOST':
            return "etc/dataflows/AMALGA_WHC_12_NM.xml";
        case 'EVMPLS':
            return "etc/dataflows/AM_WHC_RISO_8_NM.xml";
        case 'INTEGCD':
            return "etc/dataflows/AMALGA_WHC_5_NM.xml";
        case 'ECFILM01':
            return "etc/dataflows/AMALGA_WHC_3_NM.xml"; 
        case 'LILAPROD':
            return "etc/dataflows/AMALGA_WHC_SVDX_NM.xml";                                      
    }
}

if (calling_AET.equals("CANCT01") && get(SOPClassUID) != null && get(SOPClassUID) == '1.2.840.10008.5.1.4.1.1.88.67') {
    return "etc/dataflows/ERS_P005_AMALGA.xml";
}


log.info("Dataflow template found for modality " + getCallingAETitle()  + " is " + dfName);
/* return the dataflow template xml file name */
return dfName; 


