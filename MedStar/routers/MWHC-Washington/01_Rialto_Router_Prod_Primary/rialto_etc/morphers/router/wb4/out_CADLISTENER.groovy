// This destination (CADLISTENER does not support BTO)
// We use this script to drop those SOP types, but forward the rest.

def noRouteList = ['1.2.840.10008.5.1.4.1.1.13.1.3'];
def sopClassUid = get(SOPClassUID);
def transferSyntax = get(TransferSyntaxUID);
//notSupportSyntaxMap maps like sopClassUID : transferSyntax
def notSupportSyntaxMap = ['1.2.840.10008.5.1.4.1.1.7' : '1.2.840.10008.1.2.4.50'];

if (sopClassUid in noRouteList) {
   log.info("Not forwarding instance SOP {} to AET CADLISTENER because it has SOP Class UID {}", get(SOPInstanceUID), sopClassUid);
   return false;
}
else if (sopClassUid !=null && notSupportSyntaxMap.containsKey(sopClassUid)) {
	if (notSupportSyntaxMap.get(sopClassUid) == transferSyntax) {
		return false;
	}
}

return true
