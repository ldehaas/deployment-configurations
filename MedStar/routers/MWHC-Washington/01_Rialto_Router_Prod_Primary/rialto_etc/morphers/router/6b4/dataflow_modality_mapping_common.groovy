/**
*  This common class is used to provide access to the static map which holds the calling AET to
*  dataflow template xml file name entry.
*/
class DataflowModalityMapping {
    

    /**
     * This static method will return the file name of the dataflow template for a particular calling AET
     */
    static GetDataflowTemplete (callingAE) {
    
        def dfValue = null
        def pathPrefix = "etc/dataflows/"

        dfValue  = DataflowModalityMap[callingAE] 

        return pathPrefix.concat(dfValue); 
    }

    /**
     * This static variable holds the Map with the modality AETitles
     * as the key, and the dataflow template xml file name as the value.
     *
     * When new entries are added ensure proper syntax for a map. Format should be:
     *  'ae_title of modality'   colon   'name_of_dataflow_xml_file'  comma
     *
     * New entries should be added in alphabetic order.
     */
    private static final DataflowModalityMap = [
        'ACUSON'      :     'NRH_AZISO_1.xml',
        'CARMNRH'     :     'AMALGA_NRH_1.xml',
    'CHEVYCHASE'  :     'AMALGA_NRH_1.xml',
        'CS3-0285'    :     'NRH_AZISO_1.xml',
        'GELUNAR1'    :     'AMALGA_NRH_1.xml',
        'NRHMCL_TE3'  :     'AMALGA_NRH_1.xml',
        'NRHMMMC_TE2' :     'AMALGA_NRH_1.xml',
        'NRHOPC_SO1'  :     'AMALGA_NRH_1.xml',
        'NRHOPC_TE1'  :     'AMALGA_NRH_1.xml',
        'SCAN_NRH'    :     'NRH_AZISO_1.xml',
        'V2DR'        :     'AMALGA_WHC_1.xml',
        'WHCEV'       :     'NRH_AZISO_3.xml',
    'OECCARMMPC'  :     'AMALGA_WHC_11.xml' /* last entry in map take note no comma at end */
        ]
}

