/**
 * This script handles the routing for WHC (from St Marys).
 *  First it checks if the calling AET is STMAGT1 (St Marys PACS) AND 
 *  if the study_desc contains various values we route only to Cache,
 * All others will route to both Cache and WHC iSite PACS
 */

def modality_AET = "STMAGT1"
def studyDesc = get(StudyDescription);

// check if studyDesc is not null and this is the correct AET
if ( studyDesc && getCallingAETitle().equalsIgnoreCase(modality_AET)) {

    studyDesc = studyDesc.trim().toUpperCase();
    if (studyDesc.startsWith('MA DIGITAL BREAST TOMO') || studyDesc.startsWith('NM NUCLEAR')  || studyDesc.contains('ECHO')|| studyDesc.contains('CARDIAC')) {
       log.debug("Instance {} from Study {} has been excluded from routing because it is from calling AET {} and has a study description of {}",  get(SOPInstanceUID), get(StudyInstanceUID), modality_AET, studyDesc);
       return "etc/dataflows/STMARYS-CACHEONLY.xml";
    }
}
return "etc/dataflows/STMARYS-CACHEandWHCiSite.xml"
