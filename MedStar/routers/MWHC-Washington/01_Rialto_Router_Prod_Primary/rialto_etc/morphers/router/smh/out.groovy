// This destination (STENTOR_SCP_172_25_240_46) doesn't support SOPs of type 1.2.840.10008.5.1.4.1.1.11.1 (Greyscale PS) or 1.2.840.10008.5.1.4.1.1.88.22 (Enhanced SR) or 1.2.840.10008.5.1.4.1.1.88.67 (X-Ray Radiation Dose SR Storage).
// We use this script to drop those SOP types, but forward the rest.
def sopClassUid = get(SOPClassUID)

if (sopClassUid == '1.2.840.10008.5.1.4.1.1.11.1' || sopClassUid == '1.2.840.10008.5.1.4.1.1.88.22' || sopClassUid == '1.2.840.10008.5.1.4.1.1.88.67' || sopClassUid == '1.2.840.10008.5.1.4.1.1.13.1.3') {
    log.debug("Not forwarding SOP {} because it has SOP Class UID {}", get(SOPInstanceUID), sopClassUid)
    return false
}

return true
