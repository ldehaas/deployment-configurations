/**
*This groovy script is used to return the correct dataflow xml file name for a particular modality
* (calling ae Title).  
*
*
*
*
*/

String dfName = "etc/dataflows/pass-through-and-compress.xml"

def Rialto_Cache_Prod = "RIALTO_CACHE";
def Rialto_Cache_Test = "RIALTO_CACHE_T"; 
def Rialto_Router_Test = "RIALTO_ROUTER_T";
def SMHC_Rialto_Cache_Prod = "SMC_RIALTO_CACHE";

def calledAET = getCalledAETitle().toUpperCase();
def sopClassUid = get(SOPClassUID)

   /* This method will check called AE and route the studies to the correct destinations. 
    */

if (sopClassUid == '1.2.840.10008.5.1.4.90' || sopClassUid == '1.2.840.10008.5.1.4.91') {
    log.debug("Using alternate dataflow for SOP {} to transcode JPEG2000 {}", get(SOPInstanceUID), sopClassUid)
        dfname = "etc/dataflows/JPEG2000.xml";
    }

switch (calledAET) {

                case Rialto_Cache_Prod:
                        dfName = "etc/dataflows/rialto_cache_prod.xml";
                        // Problems with JPEG2000 images during transcoding so we will use alternate transcoding.
                        if (sopClassUid == '1.2.840.10008.5.1.4.90' || sopClassUid == '1.2.840.10008.5.1.4.91') {
                            log.debug("Using alternate dataflow for SOP {} to transcode JPEG2000 {}", get(SOPInstanceUID), sopClassUid)
                            dfname = "etc/dataflows/JPEG2000.xml";
                        }
                        break
				
				case SMHC_Rialto_Cache_Prod:
                        dfName = "etc/dataflows/smhc_rialto_cache_prod.xml";
                        // Problems with JPEG2000 images during transcoding so we will use alternate transcoding.
                        if (sopClassUid == '1.2.840.10008.5.1.4.90' || sopClassUid == '1.2.840.10008.5.1.4.91') {
                            log.debug("Using alternate dataflow for SOP {} to transcode JPEG2000 {}", get(SOPInstanceUID), sopClassUid)
                            dfname = "etc/dataflows/smhc_JPEG2000.xml";
                        }
                        break

                case Rialto_Cache_Test:
                        dfName = "etc/dataflows/rialto_cache_test.xml";
                        break

                case Rialto_Router_Test:
                        dfName = "etc/dataflows/rialto_router_test.xml";
                        break
                       
}

if (get(Modality).equalsIgnoreCase("NM")) {
      return "etc/dataflows/rialto_cache_prod_nm.xml";
}

log.info("Dataflow template found for modality " + getCallingAETitle()  + " with destination set to " + getCalledAETitle() + " is " + dfName);

/* return the dataflow template xml file name */
return dfName;

