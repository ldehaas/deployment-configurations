LOAD('router_workflow_builder.groovy')

import org.dcm4che2.data.DicomObject
import org.dcm4che2.data.Tag.*;

import com.karos.rialto.workflow.model.*;
import com.karos.rialto.workflow.common.tasks.*;
import com.karos.rialto.storage.file.*;
import java.time.*;
import com.karos.groovy.morph.dcm.*;

import com.karos.rialto.workflow.model.Task.Status;
import org.apache.commons.lang.StringUtils;
import groovy.transform.InheritConstructors

@InheritConstructors class SubRouterWorkflowBuilder extends RouterWorkflowBuilder {      
    def setInboundMorpher(script) {
        super.setInboundMorpher(script);
    }

    @Override
    def addDestination(params) {
    	def calledAe = params.calledAe
        def lookupAe = params.lookupAe
        def supportedSourceTs = params.supportedSourceTs
        def transcodeDestinationTs = params.transcodeDestinationTs
        def priority = params.priority
        def transcodePriority = params.transcodePriority
        def morpher = params.morpher
        def maxRetry = params.maxRetry
        def retryDelays = params.retryDelays

        def baseForSend = baseTask

        if (morpher != null) {
            def morpherBuilder = new MorphStudyTask.Builder()
            morpherBuilder.addDataDependency(baseForSend, Status.SUCCESS)
            morpherBuilder.setMorpherScriptName(morpher)

            baseForSend = morpherBuilder

            cleanStorageLocationTaskBuilder.addDataDependency(morpherBuilder, Status.SUCCESS)
            wfb.addTaskBuilder(morpherBuilder)
        }

        def sendStudyTaskBuilder = new SendStudyTask.Builder()
        
        if (transcodeDestinationTs == null) {
            sendStudyTaskBuilder.addDataDependency(baseForSend, Status.SUCCESS)
        } 
        else {
            // let's make a transcode task!
            transcodeBuilder = new TranscodeInstancesTask.Builder()
            transcodeBuilder.addDataDependency(baseForSend, Status.SUCCESS)
            transcodeBuilder.setSupportedSourceTs(supportedSourceTs)
            transcodeBuilder.setDestinationTs(transcodeDestinationTs)
            if (transcodePriority != null) {
            transcodeBuilder.setPriority(transcodePriority.getPriorityValue())
            }
            cleanStorageLocationTaskBuilder.addDataDependency(transcodeBuilder, Status.SUCCESS, Status.FAILURE)
            wfb.addTaskBuilder(transcodeBuilder)
            sendStudyTaskBuilder.addDataDependency(transcodeBuilder, Status.SUCCESS, Status.FAILURE)
        }
        sendStudyTaskBuilder.setCallingAe(callingAe)
        sendStudyTaskBuilder.setDestination(calledAe)
        if (lookupAe != null) {
            sendStudyTaskBuilder.setLookupAe(lookupAe)
        }
        if (priority != null) {
            sendStudyTaskBuilder.setPriority(priority.getPriorityValue())
        }
        if (maxRetry != null) {
            sendStudyTaskBuilder.setMaxRetries(maxRetry.toInteger())
        }
        if (retryDelays != null) {
            sendStudyTaskBuilder.setRetryDelays(retryDelays)
        } 
        cleanStorageLocationTaskBuilder.addDataDependency(sendStudyTaskBuilder, Status.SUCCESS)
        wfb.addTaskBuilder(sendStudyTaskBuilder)
    }
    def build() {
        super.build()
    }
}