LOAD('sub_router_workflow_builder.groovy')

import com.karos.rialto.workflow.model.Task.TaskPriority;

builder = new SubRouterWorkflowBuilder(
    'var/router',
    getCalledAETitle(),
    getCallingAETitle(),
    get(StudyInstanceUID))

builder.setInboundMorpher("etc/morphers/router/in.groovy")

def callingAET = getCallingAETitle();
def calledAET = getCalledAETitle().toUpperCase();
modality = get(Modality).toUpperCase();
destName = "";

switch(calledAET) {
    case 'ERS_RAD_SOUTH':
    case 'RIALTO_CACHE':
        route_RIALTO_CACHE();
        return builder.build();
    case 'RIALTO_CACHE_T':
        route_RIALTO_CACHE_T();
        return builder.build();
    case 'RIALTO_ROUTER_T':
        builder.addDestination(calledAe:"RIALTO_ROUTER_T");
        destName += "RIALTO_ROUTER_T ";
        return builder.build();
    default:
        route_RIALTO_CACHE();
        return builder.build();
}

log.info("Study from modality " + callingAET + " is sending to destination " + destName);


def route_RIALTO_CACHE(){
    switch(modality){
            case 'NM':
                builder.addDestination(calledAe:"ERS_RAD_SOUTH", priority: TaskPriority.HIGH, transcodePriority: TaskPriority.LOW, supportedSourceTs:"[1.2.840.10008.1.2, 1.2.840.10008.1.2.1, 1.2.840.10008.1.2.2]" ,transcodeDestinationTs:"1.2.840.10008.1.2.4.70");
            default:
                builder.addDestination(calledAe:"ERS_RAD_SOUTH", priority: TaskPriority.HIGH, transcodePriority: TaskPriority.LOW, supportedSourceTs:"[1.2.840.10008.1.2, 1.2.840.10008.1.2.1, 1.2.840.10008.1.2.2]" ,transcodeDestinationTs:"1.2.840.10008.1.2.4.80");
    }
    destName += "ERS_RAD_SOUTH ";
}

def route_RIALTO_CACHE_T(){
    builder.addDestination(calledAe:"RIALTO_CACHE_T", supportedSourceTs:"[1.2.840.10008.1.2, 1.2.840.10008.1.2.1, 1.2.840.10008.1.2.2]" ,transcodeDestinationTs:"1.2.840.10008.1.2.4.80");
    destName += "RIALTO_CACHE_T ";
}