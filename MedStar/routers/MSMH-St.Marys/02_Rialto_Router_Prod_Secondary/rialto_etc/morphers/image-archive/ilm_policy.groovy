import org.joda.time.LocalDateTime;

if (state.isStudyCompleted()) {

  if (!timeline.wasFullyForwardedTo("LTE_ARCHIVE1") && study.getPatientDOB().isBefore(new LocalDateTime(2016, 6, 30, 12, 00))) {
     log.debug("schedules forwarding study {} to LTE_ARCHIVE1", study.getStudyInstanceUID());
     ops.scheduleStudyForward("LTE_ARCHIVE1");
  }

//  if (!timeline.wasFullyForwardedTo("LTE_ARCHIVE2")) {
//
//     log.debug("schedules forwarding study {} to LTE_ARCHIVE2", study.getStudyInstanceUID());
//     ops.scheduleStudyForward("LTE_ARCHIVE2");
//  }
// 
//  if (timeline.wasFullyForwardedTo("LTE_ARCHIVE1") && timeline.wasFullyForwardedTo("LTE_ARCHIVE2")) {
//     ops.scheduleStudyCatalog();
//  }
}

log.trace("configurable policy DONE with study {}", study.getStudyInstanceUID());
