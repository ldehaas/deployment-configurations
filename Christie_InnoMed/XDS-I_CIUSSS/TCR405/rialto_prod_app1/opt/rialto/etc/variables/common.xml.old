<?xml version="1.0"?>
<config>

    <!-- ################################################################## -->
    <!--  VARIABLES                                                         -->
    <!-- ################################################################## -->

    <var name="HostIP" value="10.241.55.211" />

    <!-- AUDIT RECORD REPOSITORY VARIABLES -->
    <var name="ARR-Protocol" value="udp" />
    <var name="ARR-GUIPort" value="7070" />
    <var name="ARR-Port" value="514" />
    <var name="ARR-Host" value="xdsprod.dsq.rtss.qc.ca" />

    <!-- SYSTEM COMMON VARIABLES -->
    <var name="System-AffinityDomain" value="EMPI_CROSS_AFFINITY_DOMAIN_ID" />
    <var name="System-AffinityNamespace" value="RIALTO" />
    <var name="System-DefaultLocalDomain" value="county.general" />
    <var name="System-DefaultNamespace" value="CG" />
    <var name="System-UniversalIDType" value="ISO" />
    <var name="System-DefaultLocalFullyQualifiedDomain" value="${System-DefaultNamespace}&amp;${System-DefaultLocalDomain}&amp;${System-UniversalIDType}" />
    <var name="System-AffinityFullyQualifiedDomain" value="${System-AffinityNamespace}&amp;${System-AffinityDomain}&amp;${System-UniversalIDType}" />

    <!-- XDS REGISTRY VARIABLES -->
    <var name="XDSRegistry-Host" value="localhost"/>
    <var name="XDSRegistry-Port" value="8081"/>
    <var name="XDSRegistry-Path" value="/registry"/>
    <var name="XDSRegistry-URL" value="http://${XDSRegistry-Host}:${XDSRegistry-Port}${XDSRegistry-Path}"/>
    <var name="XDSRegistry-HL7v2Port" value="2397"/>
    <var name="XDSRegistry-MetadataUpdateEnabled" value="true"/>

    <!-- XDS REPOSITORY VARIABLES -->
    <var name="XDSRepository-Host" value="localhost" />
    <var name="XDSRepository-Port" value="8082" />
    <var name="XDSRepository-Path" value="/vault/repo" />
    <var name="XDSRepository-ID" value="2.16.840.1.113883.1.2.3.40001"/>
    <var name="XDSRepository-URL" value="http://${XDSRepository-Host}:${XDSRepository-Port}${XDSRepository-Path}"/>

    <!-- VAULT - IMAGE ARCHIVE VARIABLES -->
    <!-- 11112 is the default DICOM port according to IANA -->
    <!--
    <var name="MainDicomPort" value="11112" />
    <var name="IAStorePort" value="11113" />
    <var name="IA-AETitle" value="RIALTO" />
    <var name="IA-StudyManagement-Path" value="/vault/studymanagement" />
    -->
    <!-- Port which image archive listens on for study update messages -->
    <!--
    <var name="UpdateStudies-HL7v2Port" value="2455" />
    -->

    <!-- DICOM Router -->
    <var name="Router-AETitle" value="ROUTER" />

    <!-- EMPI VARIABLES -->
    <var name="EMPI-Host" value="localhost" />
    <var name="EMPI-HL7v2Port" value="2399" />
    <var name="EMPI-SendingApplication" value="RIALTO_EMPI"/>
    <var name="EMPI-SendingFacility" value="KAROS_HEALTH"/>

    <!-- DATABASE URLs: "DatabaseURL" parameter name is used by the navigator-security script, do not change the parameter name  -->
    <var name="DatabaseURL" value="jdbc:postgresql://localhost/navigatordb?user=rialto"/>

    <!-- NAVIGATOR VARIABLES -->
    <var name="Navigator-Host" value="${HostIP}"/>
    <var name="Navigator-HostProtocol" value="https" />
    <var name="Navigator-GUIPort" value="2525" />
    <var name="Navigator-APIPort" value="2525" />
    <var name="Navigator-DefaultLocale" value="en" />
    <var name="Navigator-DefaultTheme" value="rialto-light" />
    <var name="Navigator-MaxPatientSearchResults" value="1000" />
    <var name="Navigator-MaxDocumentSearchResults" value="1000" />

    <!-- USERMANAGEMENT VARIABLES -->
    <var name="Usermanagement-URL" value="${Navigator-HostProtocol}://${Navigator-Host}:${Navigator-APIPort}/rest" />
    <var name="Usermanagement-AuthenticatedURL" value="${Navigator-HostProtocol}://${Navigator-Host}:${Navigator-APIPort}/rest/api/usermanagement" />

    <!-- CLUSTER CONFIGURATION -->
    <!-- Single Sign On in a Cluster -->
    <var name="SSO-Cluster-Bind-IP" value="127.0.0.1"/>
    <var name="SSO-Cluster-Multicast-IP" value="232.10.10.201"/>
    <var name="SSO-Cluster-Multicast-PORT" value="45201"/>

    <!-- Navigator Cache Manager in a cluster -->
    <var name="Navigator-Cluster-Bind-IP" value="127.0.0.1"/>
    <var name="Navigator-Cluster-Multicast-IP" value="232.10.10.202"/>
    <var name="Navigator-Cluster-Multicast-PORT" value="45202"/>

    <!-- Navigator Server access controller in a cluster -->
    <var name="Navigator-Server-Cluster-Bind-IP" value="127.0.0.1"/>
    <var name="Navigator-Server-Cluster-Multicast-IP" value="232.10.10.203"/>
    <var name="Navigator-Server-Cluster-Multicast-PORT" value="45203"/>

    <!-- User Sessions -->
    <var name="IdleUserSessionTimeout" value="30m" />

    <!-- Database Backup -->
    <var name="BackupRemotePath" value="/opt/rialto/var/database-backups" />
    <var name="BackupMaxSnapshots" value="5" />

</config>
