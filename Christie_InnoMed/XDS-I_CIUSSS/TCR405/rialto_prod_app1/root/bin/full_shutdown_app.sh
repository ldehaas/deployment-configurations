#!/bin/bash
# full shutdown app server

# disable app services
echo 'Disabling services'
systemctl disable nifi
systemctl disable rialto
systemctl disable nginx.service
systemctl disable filebeat-events
systemctl disable filebeat-error

# stop app services
echo 'Stopping services'
service nifi stop
service rialto stop
service nginx.service stop
service filebeat-events stop
service filebeat-error stop

sleep 10
# validate services are stopped
systemctl status nifi rialto nginx filebeat-events filebeat-error

echo 'Checking for services which are not yet shutdown...'
ps -ef | grep 'rialto\|nifi\|filebeat-events\|nginx' | grep -v grep

# stamp /data mount to indicated shutdown time
echo 'remove and touch /data/full_shutdown file for NetApp sync identifier'
cd /data && rm -rf full_shutdown && touch full_shutdown
cd /data && ls -la | grep full_shutdown > /tmp/data.txt && cat /tmp/data.txt

#unmount /data drive and comment out in /etc/fstab
echo 'unmount /data'
cd / && umount /data && sed -i '/[/]data/ s/^/#/' /etc/fstab && cat /etc/fstab
