#!/bin/bash

SNAPSHOT=`date +%Y%m%d-%H%M%S`

curl -XPUT "localhost:9200/_snapshot/rialto_prod_backups/$SNAPSHOT?wait_for_completion=true&pretty"

