#delete order-schedule entries scheduled more than two years in the future

adapter=$(netstat -tulpn | grep 9200 | grep -v 127.0.0.1 | awk '{ print $4 }' | sed 's/:.*//')
master=$(curl -s localhost:9200/_cat/master | awk ' {print $3}')
namespace=rialto-es-prod-order-schedule-read
futureyear=$(date --date='+2 year' '+%Y')
futuremonth=$(date --date='+1 month' '+%m')
echo "Delete orders with scheduledProcedureStepStartDateTime greater or equal to YYYY-MM:" $futureyear"-"$futuremonth

QUERY()
{
cat <<EOF
{
    "query": {
        "range" : {
            "scheduledProcedureStepStartDateTime": {
                "gte": "$futureyear-$futuremonth",
                "format": "yyyy-mm"
            }
        }
    }
}
EOF
}

#only run if elasticsearch instance is the master node
   if [ "$adapter" == "$master" ]; then
      echo Master node "$master" matches adapter address "$adapter", proceeding
      logger rialto-es-prod-order-schedule query cron: master node "$master" matches adapter address "$adapter", proceeding
      curl -XPOST "http://localhost:9200/$namespace/_delete_by_query" -H 'Content-Type: application/json' -d "$(QUERY)"
      sleep 10
   else
      echo "$adapter" does not match master node address "$master", exiting
      logger rialto-es-prod-order-schedule query: "$adapter" does not match master node address "$master" exiting
   fi
