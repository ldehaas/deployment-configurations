#!/bin/bash
# full shutdown db server

# disable database services
echo 'Disabling services'
systemctl disable cassandra
systemctl disable elasticsearch.service
systemctl disable kibana

# stop database services
echo 'Stopping services'
service cassandra stop
service elasticsearch stop
service kibana stop

sleep 10
# validate services are stopped
systemctl status cassandra elasticsearch.service kibana

echo 'Checking for services which are not yet shutdown...'
ps -ef | grep 'cassandra\|elastic\|kibanas' | grep -v grep

# stamp /data mount to indicated shutdown time
echo 'remove and touch /data/full_shutdown file for NetApp sync identifier'
cd /data && rm -rf full_shutdown && touch full_shutdown
cd /data && ls -la | grep full_shutdown > /tmp/data.txt && cat /tmp/data.txt

#unmount /data and /log drives and comment out in /etc/fstab
echo 'unmount /data'
cd / && umount /data
sed -i '/[/]data/ s/^/#/' /etc/fstab 
cat /etc/fstab
