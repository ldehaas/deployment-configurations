#!/bin/bash
# Step-1 This is needed to be run before shutting down VM to disable shard location.

curl -X PUT "localhost:9200/_cluster/settings" -H 'Content-Type: application/json' -d'{"transient": {"cluster.routing.allocation.enable": "none"}}'


read -p "flush indexes" -n 1 -r

if [[ $REPLY =~ ^[Yy]$ ]]
then
curl -X POST "localhost:9200/_flush/synced"
fi



