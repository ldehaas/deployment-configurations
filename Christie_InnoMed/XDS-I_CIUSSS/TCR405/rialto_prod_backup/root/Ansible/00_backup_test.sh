#!/bin/bash

cd ~/deployment-configurations/x-Tools/Ansible/Quebec

# backup /etc/ntp.conf
ansible-playbook -i ./inventories/xdsi-test ansible_ntp_config_backup.yml --limit test-all

# backup /etc/fstab
ansible-playbook -i ./inventories/xdsi-test ansible_fstab_config_backup.yml --limit test-all

# backup /etc/smb_mount_tcr400.txt
ansible-playbook -i ./inventories/xdsi-test ansible_fstab_pass_config_backup.yml --limit test-cassandra-db


# backup /etc/hosts
# /etc/hostname
# /etc/resolv.conf
# /etc/sysconfig/network
# /etc/sysconfig/network-scripts/ifcfg*
# /etc/sysconfig/network-scripts/route*
# /etc/sysconfig/network-scripts/rule*
ansible-playbook -i ./inventories/xdsi-test ansible_network_config_backup.yml --limit test-all


# /etc/sysconfig/iptables
# /etc/sysconfig/firewalld
ansible-playbook -i ./inventories/xdsi-test ansible_network_firewall_config_backup.yml --limit test-all


# backup system and kernel configuration
# /etc/sysctl.conf
# /etc/sysconfig/kernel
# /etc/security/limits.conf
# /etc/security/limits.d/*
ansible-playbook -i ./inventories/xdsi-test ansible_system_config_backup.yml --limit test-all


# backup SSH configuration
# /etc/ssh/*
ansible-playbook -i ./inventories/xdsi-test ansible_ssh_config_backup.yml --limit test-all


# backup SE Linux
# /etc/selinux/config
# /etc/sysconfig/selinux
# new selinux: sed -i 's/SELINUX=enforcing/SELINUX=disabled/g' /etc/sysconfig/selinux
ansible-playbook -i ./inventories/xdsi-test ansible_selinux_config_backup.yml --limit test-all


# vmware tools?


# backup root config
# /root/.bashrc
# /root/.bash_profile
# /root/.ssh/*
# /root/scripts/*
# /root/*.sh
# crontab -l > /root/crontab.txt
ansible-playbook -i ./inventories/xdsi-test ansible_root_config_backup.yml --limit test-all


# Apache NiFi
# /opt/nifi/conf/*
# /data/nifi/flow.xml.gz
# /etc/init.d/nifi
# /etc/rc.d/init.d/nifi
ansible-playbook -i ./inventories/xdsi-test ansible_nifi_config_backup.yml --limit test-rialto-app


# Rialto configuration
# /opt/rialto/etc/*
# /etc/init.d/rialto
ansible-playbook -i ./inventories/xdsi-test ansible_rialto_config_backup.yml --limit test-rialto-app


# Filebeat config
# /etc/filebeat/*
# /etc/init.d/filebeat*
ansible-playbook -i ./inventories/xdsi-test ansible_filebeat_config_backup_v2.yml --limit test-all


# Metricbeat config
# /etc/metricbeat/*
# /etc/init.d/metribeat*
ansible-playbook -i ./inventories/xdsi-test ansible_metricbeat_config_backup_v2.yml --limit test-all


# Heartbeat config
# /etc/heartbeat/*
# /etc/init.d/heartbeat*
ansible-playbook -i ./inventories/xdsi-test ansible_heartbeat_config_backup_v2.yml --limit test-all


# Nginx
# /etc/nginx/nginx.conf
# /etc/sysconfig/nginx*
# # /etc/init.d/nginx*
# /usr/lib/systemd/system/nginx.service
ansible-playbook -i ./inventories/xdsi-test ansible_nginx_config_backup.yml --limit test-rialto-app


# Cassandra
# /etc/init.d/cassandra
# /opt/cassandra/conf/*
ansible-playbook -i ./inventories/xdsi-test ansible_cassandra_config_backup.yml --limit test-cassandra-db


# Elastic
# /etc/elasticsearch/elasticsearch.yml
# /usr/lib/systemd/system/elasticsearch.service
# /etc/init.d/elastcisearch
# /opt/rialto/entity/rialto-es-prod-context
# or
# /data/rialto/rialto-es-prod-context
ansible-playbook -i ./inventories/xdsi-test ansible_elastic_config_backup.yml --limit test-cassandra-db
ansible-playbook -i ./inventories/xdsi-test ansible_elastic_config_backup.yml --limit test-elastic-db


# Kibana
# /etc/init.d/kibana
# /etc/kibana/kibana.yml
ansible-playbook -i ./inventories/xdsi-test ansible_kibana_config_backup.yml --limit test-cassandra-db
ansible-playbook -i ./inventories/xdsi-test ansible_kibana_config_backup.yml --limit test-elastic-db
