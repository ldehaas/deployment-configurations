#!/bin/bash

cd ~/deployment-configurations/x-Tools/Ansible/Quebec

# backup /etc/ntp.conf
ansible-playbook -i ./inventories/xdsi-prod ansible_ntp_config_backup.yml --limit tcr400

# backup /etc/fstab
ansible-playbook -i ./inventories/xdsi-prod ansible_fstab_config_backup.yml --limit tcr400

# backup /etc/smb_mount_tcr400.txt
ansible-playbook -i ./inventories/xdsi-prod ansible_fstab_pass_config_backup.yml --limit tcr400-cassandra-db


# backup /etc/hosts
# /etc/hostname
# /etc/iproute2/rt_tables
# /etc/resolv.conf
# /etc/sysconfig/network
# /etc/sysconfig/network-scripts/ifcfg*
# /etc/sysconfig/network-scripts/route*
# /etc/sysconfig/network-scripts/rule*
ansible-playbook -i ./inventories/xdsi-prod ansible_network_config_backup.yml --limit tcr400


# /etc/sysconfig/iptables
# /etc/sysconfig/firewalld
# /usr/local/etc/firewall.cfg
ansible-playbook -i ./inventories/xdsi-prod ansible_network_firewall_config_backup.yml --limit tcr400

# fatal: [tcrsvr2327]: FAILED! => {"changed": false, "msg": "file not found: /etc/sysconfig/iptables"}


# backup system and kernel configuration
# /etc/sysctl.conf
# /etc/sysconfig/kernel
# /etc/security/limits.conf
# /etc/security/limits.d/*
ansible-playbook -i ./inventories/xdsi-prod ansible_system_config_backup.yml --limit tcr400

# fatal: [tcrsvr2327]: FAILED! => {"changed": false, "msg": "file not found: /etc/yum.repos.d/Rialto.repo"}


# backup SSH configuration
# /etc/ssh/*
ansible-playbook -i ./inventories/xdsi-prod ansible_ssh_config_backup.yml --limit tcr400


# backup SE Linux
# /etc/selinux/config
# /etc/sysconfig/selinux
# new selinux: sed -i 's/SELINUX=enforcing/SELINUX=disabled/g' /etc/sysconfig/selinux
ansible-playbook -i ./inventories/xdsi-prod ansible_selinux_config_backup.yml --limit tcr400


# vmware tools?


# backup root config
# /root/.bashrc
# /root/.bash_profile
# /root/.ssh/*
# /root/scripts/*
# /root/*.sh
# crontab -l > /root/crontab.txt
ansible-playbook -i ./inventories/xdsi-prod ansible_root_config_backup.yml --limit tcr400


# Apache NiFi
# /opt/nifi/conf/*
# /data/nifi/flow.xml.gz
# /etc/init.d/nifi
# /etc/rc.d/init.d/nifi
ansible-playbook -i ./inventories/xdsi-prod ansible_nifi_config_backup.yml --limit tcr400-rialto-app


# Rialto configuration
# /data/tls/*
# /opt/rialto/etc/*
# /etc/init.d/rialto
ansible-playbook -i ./inventories/xdsi-prod ansible_rialto_config_backup.yml --limit tcr400-rialto-app


# Filebeat config
# /data/filebeat-events/*
# /etc/filebeat/*
# /etc/init.d/filebeat*
# /usr/lib/systemd/system/filebeat*
# /var/lib/filebeat*
ansible-playbook -i ./inventories/xdsi-prod ansible_filebeat_config_backup_v2.yml --limit tcr400


# Metricbeat config
# /etc/metricbeat/*
# /etc/init.d/metribeat*
# /usr/lib/systemd/system/metricbeat*
ansible-playbook -i ./inventories/xdsi-prod ansible_metricbeat_config_backup_v2.yml --limit tcr400


# Heartbeat config
# /etc/heartbeat/*
# /etc/init.d/heartbeat*
# /usr/lib/systemd/system/heartbeat*
ansible-playbook -i ./inventories/xdsi-prod ansible_heartbeat_config_backup_v2.yml --limit tcr400


# Nginx
# /etc/nginx/nginx.conf
# /etc/nginx/ssl/default.conf
# /etc/sysconfig/nginx*
# # /etc/init.d/nginx*
# /usr/lib/systemd/system/nginx.service
ansible-playbook -i ./inventories/xdsi-prod ansible_nginx_config_backup.yml --limit tcr400-rialto-app


# Cassandra
# /etc/init.d/cassandra
# /opt/cassandra/conf/*
ansible-playbook -i ./inventories/xdsi-prod ansible_cassandra_config_backup.yml --limit tcr400-cassandra-db


# Elastic
# /etc/elasticsearch/elasticsearch.yml
# /etc/elasticsearch/checkmount.sh
# /usr/lib/systemd/system/elasticsearch.service
# /etc/systemd/system/elasticsearch.service.d/override.conf
# /etc/init.d/elasticsearch
# /opt/rialto/entity/rialto-es-prod-context
# or
# /data/rialto/rialto-es-prod-context
ansible-playbook -i ./inventories/xdsi-prod ansible_elastic_config_backup.yml --limit tcr400-cassandra-db
ansible-playbook -i ./inventories/xdsi-prod ansible_elastic_config_backup.yml --limit tcr400-elastic-db


# Kibana
# /etc/init.d/kibana
# /etc/kibana/kibana.yml
ansible-playbook -i ./inventories/xdsi-prod ansible_kibana_config_backup.yml --limit tcr400-cassandra-db
ansible-playbook -i ./inventories/xdsi-prod ansible_kibana_config_backup.yml --limit tcr400-elastic-db

# Backup
# SMB

# Health Monitoring
# /opt/cato/*.yml
# /opt/cato/.htpasswd
# /opt/cato/nginx.conf
# /opt/cato/pipeline/logstash.conf
