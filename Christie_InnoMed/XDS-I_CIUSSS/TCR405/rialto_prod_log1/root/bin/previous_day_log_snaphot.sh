#snapshot previous days event index using UTC time

adapter=$(netstat -tulpn | grep 9200 | grep -v 127.0.0.1 | awk '{ print $4 }' | sed 's/:.*//')
master=$(curl -s localhost:9200/_cat/master | awk ' {print $3}')
namespace=rialto-es-log-event
index=$(date --date='-1 day' '+%F' '--utc')
snapshot_location=rialto_log_backups

SNAPSHOT()
{
cat <<EOF
{
"indices": "$namespace-$index",
"ignore_unavailable": true,
"include_global_state": false
}
EOF
}

#only run if elasticsearch instance is the master node
   if [ "$adapter" == "$master" ]; then
      echo Master node "$master" matches adapter address "$adapter", proceeding
      logger rialto-es-log snapshot cron: master node "$master" matches adapter address "$adapter", proceeding
      curl -XPUT "http://localhost:9200/_snapshot/$snapshot_location/$namespace-$index?wait_for_completion=true" -H 'Content-Type: application/json' -d "$(SNAPSHOT)"
      sleep 10
      snapshot_state=$(curl -s localhost:9200/_snapshot/$snapshot_location/$namespace-$index | /root/bin/jq-linux64 '.snapshots[] | [.state] |@text' | tr -d '[]\\"')
         if [ "$snapshot_state" == "SUCCESS" ]; then
            echo Snapshot result is "$snapshot_state", completed successfully
            logger rialto-es-log snapshot cron: "$snapshot_state" snapshot completed successfully
         else
            echo Snapshot result is "$snapshot_state", failed
            logger rialto-es-log snapshot cron: "$snapshot_state" is not SUCCESS, snapshot failed
         fi
   else
      echo "$adapter" does not match master node address "$master", exiting
      logger rialto-es-log snapshot cron: "$adapter" does not match master node address "$master" exiting
   fi
