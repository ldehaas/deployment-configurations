namespace=rialto-es-log-event-2019-07
for index in {01..13}
do
 
SNAPSHOT()
{
cat <<EOF
{
"indices": "$namespace-$index",
"ignore_unavailable": true,
"include_global_state": false
}
EOF
}
curl -XPUT "http://localhost:9200/_snapshot/rialto_log_backups/$namespace-$index?wait_for_completion=true" -H 'Content-Type: application/json' -d "$(SNAPSHOT)"
sleep 1
done
