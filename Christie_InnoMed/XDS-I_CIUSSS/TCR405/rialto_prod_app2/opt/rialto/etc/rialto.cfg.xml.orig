<?xml version="1.0"?>
<config>

    <!-- ################################################################## -->
    <!--  VARIABLES                                                         -->
    <!-- ################################################################## -->
    <include location="variables/common.xml" />
    <include location="variables/tls.xml" />
    <include location="variables/newrelic.xml" />
    <include location="variables/cassandra.xml" />
    <include location="variables/elastic.xml" />

    <!-- ################################################################## -->
    <!--  SERVERS                                                           -->
    <!-- ################################################################## -->
    <include location="servers/common.xml" />

    <!-- ################################################################## -->
    <!--  DEVICES                                                           -->
    <!-- ################################################################## -->
    <include location="devices/common.xml" />
    <include location="devices/cassandra.xml" />
    <include location="devices/dicom-external.xml" />
    <include location="devices/audit-repository.xml" />
    <include location="devices/empi.xml" />
    <include location="devices/xds-registry.xml" />
    <include location="devices/xds-repository.xml" />

    <!-- ################################################################## -->
    <!--  SERVICES                                                          -->
    <!-- ################################################################## -->

    <!-- These four core Rialto services provide a platform for other services. -->
    <!-- They must start first, in the order specified. Do not change order or remove!! -->
    <include location="services/cassandra-startup.xml" />
    <include location="services/device-registry.xml" />

    <!-- Rialto entity managers -->
    <include location="services/authentication.xml" />
    <include location="services/usermanagement.xml" />
    <include location="services/context-manager.xml"/>
    <include location="services/device-manager.xml"/>
    <include location="services/script-manager.xml"/>
    <include location="services/event-manager.xml"/>
    <include location="services/prior-rules-manager.xml"/>

    <!-- Rialto services -->

    <!-- Web-based administration UI - requires authentication, user management and REST backend services -->
    <include location="services/admin-tools-backend.xml" />
    <include location="services/admin-tools-frontend.xml" />

    <!-- modality worklist service is disabled by default, since it requires administrative configuration -->
    <include location="services/modality-worklist.xml" />

</config>
