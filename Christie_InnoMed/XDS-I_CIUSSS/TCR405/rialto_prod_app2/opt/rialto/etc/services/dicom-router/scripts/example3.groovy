LOAD('router_workflow_builder.groovy')

import com.karos.rialto.workflow.model.Task.TaskPriority;

def builder = new RouterWorkflowBuilder(
    'var/router', 
    getCalledAETitle(),
    getCallingAETitle(),
    get(StudyInstanceUID))

builder.setInboundMorpher("etc/services/dicom-router/scripts/in.groovy")

// If it's a NM modality, transcode to JPEG Lossless (.70)
// For everything else, use JPEG-LS (.80)
if (get(Modality).equalsIgnoreCase("NM")) {
    builder.addDestination(calledAe:"RIALTO_SOUTH", transcodeDestinationTs:"1.2.840.10008.1.2.4.70", priority: TaskPriority.HIGH)
} else {
    builder.addDestination(calledAe:"RIALTO_SOUTH", transcodeDestinationTs:"1.2.840.10008.1.2.4.80", priority: TaskPriority.HIGH)
}

builder.addDestination(calledAe:"DELL_INDEX_LINK", priority: TaskPriority.LOW)

return builder.build()
