log.info("Start XDSRegistry Morpher")

if (get('/.MSH-9-2') != null && get('/.MSH-9-2') == "A39")
    set('/.MSH-9-2', 'A40')

def messageType = get('/.MSH-9-1')
def triggerEvent = get('/.MSH-9-2')

log.info("messageType is: {}", messageType)
log.info("triggerEvent is: {}", triggerEvent)

if (messageType != null && triggerEvent != null) {
    if ("ADT".equals(messageType) && ("A40".equals(triggerEvent))) {
        log.info("will forward message to XDSRegistry")
    } else {
        log.info("will not forward message to the XDSRegistry")
        return false
    }
} else {
    log.info("will not forward message to the XDSRegistry")
    return false
}

log.info("End XDSRegistry Morpher")
