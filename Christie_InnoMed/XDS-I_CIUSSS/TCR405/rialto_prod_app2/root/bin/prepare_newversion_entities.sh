cd /opt/rialto/entity
mkdir -p rialto-es-prod-context && chown -R rialto:rialto rialto-es-prod-context
mkdir -p rialto-es-prod-device && chown -R rialto:rialto rialto-es-prod-device
mkdir -p rialto-es-prod-prior-rules && chown -R rialto:rialto rialto-es-prod-prior-rules
mkdir -p rialto-es-prod-script && chown -R rialto:rialto rialto-es-prod-script
