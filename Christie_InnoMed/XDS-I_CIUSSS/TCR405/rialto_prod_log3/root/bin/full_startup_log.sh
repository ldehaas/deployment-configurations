#!/bin/bash
# full startup log server

#mount /data and /log drive and enable in /etc/fstab
sed -i '/^#.*[/]data/s/^#//' /etc/fstab && cat /etc/fstab && mount -a

read -p "Continue with startup?" -n 1 -r
if [[ $REPLY =~ ^[Yy]$ ]]
then
# enable database services
systemctl enable elasticsearch.service
systemctl enable kibana

# start database services
service elasticsearch start
service kibana start

sleep 10
systemctl status elasticsearch.service kibana

# touch file
cd /data && rm -rf full_startup && touch full_startup
fi
