<?xml version="1.0"?>
<config>

    <server type="http" id="http-navigator">
        <port>2526</port> <!-- Note this port is hardcoded now -->
    </server>

    <!-- This server is new, and should be added -->
    <server type="http" id="http-admin-tools">
        <port>2527</port> <!-- Note this port is also hardcoded -->
    </server>

    <!-- PLUGIN: External Viewer (Download) -->
    <var name="DownloadPlugin-Name" value="External Viewer (Download)" />
    <var name="DownloadPlugin-Description" value="External platform-specific viewer or download to local disk" />
    <service id="navigator-plugin-download" type="navigator.plugin.download">

        <device idref="pix" />
        <device idref="pdq" />
        <device idref="xdsrep"/>
        <device idref="xdsreg"/>
        <device idref="cassandra-dc1" />

        <config>
            <prop name="CrossAffinityDomainID" value="${System-AffinityDomain}"/>
            <prop name="ForcedMimeTypeFileExtension">
                <prop mimeType="application/vnd.ms-excel" fileExtension="xls"/>
            </prop>
            <prop name="CassandraConfiguration">
                <keyspaceName>${CassandraKeyspacePrefix}navigator</keyspaceName>
                <keyspaceReplicationStrategy>NetworkTopologyStrategy</keyspaceReplicationStrategy>
                <keyspaceReplicationStrategyOptions>${CassandraReplicationOption}</keyspaceReplicationStrategyOptions>
                <keyspaceCreateEnabled>true</keyspaceCreateEnabled>
                <dynamicConsistencyLevelEnabled>false</dynamicConsistencyLevelEnabled>
                <schemaUpdateEnabled>false</schemaUpdateEnabled>
            </prop>
            <prop name="web-api-path" value="/plugins/download"/>
            <prop name="UserManagementURL" value="${Usermanagement-AuthenticatedURL}"/>

            <include location="admin-tools/patientidentitydomains.xml"/>
            <include location="admin-tools/cacheStorage.xml"/>
        </config>
    </service>

    <!-- PLUGIN: CDA Viewer   -->
    <var name="CDAPlugin-Name" value="CDA Viewer" />
    <var name="CDAPlugin-Description" value="HL7 CDA (Clinical Document Architecture) Viewer" />
    <service id="navigator-plugin-cda" type="navigator.plugin.cda">
        <device idref="pix" />
        <device idref="pdq" />
        <device idref="xdsrep"/>
        <device idref="xdsreg"/>
        <device idref="cassandra-dc1" />

        <config>
            <prop name="CrossAffinityDomainID" value="${System-AffinityDomain}"/>
            <prop name="CassandraConfiguration">
                <keyspaceName>${CassandraKeyspacePrefix}navigator</keyspaceName>
                <keyspaceReplicationStrategy>NetworkTopologyStrategy</keyspaceReplicationStrategy>
                <keyspaceReplicationStrategyOptions>${CassandraReplicationOption}</keyspaceReplicationStrategyOptions>
                <keyspaceCreateEnabled>true</keyspaceCreateEnabled>
                <dynamicConsistencyLevelEnabled>false</dynamicConsistencyLevelEnabled>
                <schemaUpdateEnabled>false</schemaUpdateEnabled>
            </prop>
            <prop name="web-api-path" value="/plugins/cda"/>
            <prop name="DefaultStylesheet" value="${rialto.rootdir}/etc/services/admin-tools/plugins/cda/default.xsl"/>
            <prop name="UserManagementURL" value="${Usermanagement-AuthenticatedURL}"/>

            <include location="admin-tools/patientidentitydomains.xml"/>
            <include location="admin-tools/cacheStorage.xml"/>
        </config>
    </service>

    <service id="rialto-ui" type="rialto.ui">
        <device idref="pix" />
        <device idref="pdq" />

        <server idref="http-navigator" name="web-ui">
            <url>/</url>
        </server>

        <server idref="http-admin-tools" name="admin-tools">
            <url>/v2/</url>
        </server>

        <server idref="http-authenticated" name="web-api">
            <!-- Not configurable at this time -->
            <url>/public/*</url>
        </server>

        <config>
            <prop name="ApiURL" value="${Navigator-HostProtocol}://${Navigator-Host}:${Navigator-APIPort}${Navigator-RootPath}/api${Navigator-APIBasePath}"/>
            <prop name="ArrURL" value="${Navigator-HostProtocol}://${Navigator-Host}:${ARR-GUIPort}"/>
            <prop name="UserManagementURL" value="${Navigator-HostProtocol}://${HostIP}:${Navigator-APIPort}${Navigator-RootPath}"/>
            <prop name="PublicURL" value="${Navigator-HostProtocol}://${Navigator-Host}:${Navigator-APIPort}/public"/>
            <prop name="StudyManagementURL" value="http://${HostIP}:8080${IA-StudyManagement-Path}"/>
            <prop name="WadoURL" value="http://${HostIP}:8080/vault/wado" />
            <prop name="MintApiURL" value="http://${HostIP}:8080/vault/mint" />
            <prop name="WorkflowApiURL" value="http://${HostIP}:8080/workflows" />
            <prop name="ElasticsearchRestApiURL" value="http://192.168.56.101:9200" />
            <prop name="DeviceManagerURL" value="${Navigator-HostProtocol}://${Navigator-Host}:${Navigator-APIPort}/rest/api/devices" />
            <prop name="ContextManagerURL" value="${Navigator-HostProtocol}://${Navigator-Host}:${Navigator-APIPort}/rest/api/contexts" />
            <prop name="PriorRulesManagerURL" value="${Navigator-HostProtocol}://${Navigator-Host}:${Navigator-APIPort}/rest/api/priorrules" />
            <prop name="ScriptManagerURL" value="${Navigator-HostProtocol}://${Navigator-Host}:${Navigator-APIPort}/rest/api/scripts" />
            <prop name="EventViewerURL" value="${Navigator-HostProtocol}://${Navigator-Host}:${Navigator-APIPort}/rest/api/events" />

            <prop name="ModalityWorklistURL" value="${Navigator-HostProtocol}://${HostIP}:${Navigator-APIPort}/rest/api/mwl" />
            <prop name="DicomQcToolsURL" value="http://${HostIP}:8080/vault/qc" />
            <prop name="IlmURL" value="http://${HostIP}:8080/vault/ilm" />
            <prop name="EMPIManagementURL" value="http://${HostIP}:8080/rialto/empi" />
            <prop name="GlobalInactivitySessionTimeout" value="${IdleUserSessionTimeout}" />
            <prop name="ExternalPDQConfigured" value="false" />
            <prop name="RialtoAsACache" value="false" />
            <!--prop name="VIPConfidentialityCode" value="VIP" /-->

            <prop name="RialtoImageArchiveAETitle" value="${Router-AETitle}" />

            <prop name="ElasticsearchSchemaPrefix" value="${WorkflowSchemaPrefix}" />

            <!-- ConnectQueryRetrieveAETitle is used for query/retrieve of catalogued studies
                 This property should be enabled if you want to enable the import of catalogued studies in Rialto Admin tools
                 It's value should match a configured Connect device -->
            <!-- <prop name="ConnectQueryRetrieveAETitle" value="CONNECT" /> -->

            <!-- NEWRELIC CONFIGURATION -->
            <!-- Enable the following properties in order to enable the NewRelic Insights dashboards in the Rialto Admin tools -->
            <prop name="NewRelicInsightsQueryKey" value="${NewRelic-InsightsQueryKey}" />
            <prop name="NewRelicInsightsQueryURL" value="https://insights-api.newrelic.com/v1/accounts/${NewRelic-AccountID}/query" />
            <!-- Enable the following properties in order to enable the Server, Applications and Alerts dashboards in the Rialto Admin tools -->
            <prop name="NewRelicRestApiKey" value="${NewRelic-RestApiKey}" />
            <prop name="NewRelicRestApiURL" value="https://api.newrelic.com/v2/" />

            <prop name="SupportedThemes">
                rialto-light
                rialto-dark
            </prop>
            <prop name="DefaultTheme" value="${Navigator-DefaultTheme}"/>

            <prop name="SupportedLocales">
                en
                fr
            </prop>
            <prop name="DefaultLocale" value="${Navigator-DefaultLocale}"/>

            <prop name="BreakTheGlassOptions">
                <BreakTheGlassOption>Emergency access required for patient care.</BreakTheGlassOption>
            </prop>

            <include location="admin-tools/patientidentitydomains.xml"/>

            <prop name="Plugins">
                <plugin>
                    <name>${CDAPlugin-Name}</name>
                    <description>${CDAPlugin-Description}</description>
                    <mimeTypes>text/xml</mimeTypes>
                    <url>${Navigator-HostProtocol}://${Navigator-Host}:${Navigator-APIPort}${Navigator-RootPath}/api/plugins/cda</url>
                    <embedded>true</embedded>
                </plugin>

                <plugin>
                    <name>${DownloadPlugin-Name}</name>
                    <description>${DownloadPlugin-Description}</description>
                    <mimeTypes>*</mimeTypes>
                    <url>${Navigator-HostProtocol}://${Navigator-Host}:${Navigator-APIPort}${Navigator-RootPath}/api/plugins/download</url>
                    <embedded>true</embedded>
                </plugin>
            </prop>

            <!-- Configure server name to display in UI -->
            <!-- prop name="ServerName" value="server" /-->

            <prop name="IOCMEnabled" value="true" />

            <prop name="StudyMetadataSizeLimit" value="1000000" />

            <!-- Configure image caching for studies in the UI       -->
            <!-- The value can be one of:                            -->
            <!-- "true": enable the cache with default size (32 MBs) -->
            <!-- "false": disabled caching of images                 -->
            <!-- "number": The store size in megabytes               -->
            <prop name="ImageCache">
                <size>64</size>
            </prop>

            <!-- Configure eventCodeSchemes to support searching for documents by eventCode -->
            <prop name="EventCodeSchemes">
                <scheme schemeID="" schemeName="Karos Health demo eventCodes" />
                <scheme schemeID="" schemeName="CareGiverScheme" />
            </prop>

        </config>
    </service>
</config>
