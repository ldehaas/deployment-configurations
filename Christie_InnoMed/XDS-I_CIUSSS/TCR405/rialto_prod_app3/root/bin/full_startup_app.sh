#!/bin/bash
# full startup app server

#mount /data drive and enable in /etc/fstab
sed -i '/^#.*[/]data/s/^#//' /etc/fstab && cat /etc/fstab && mount -a

read -p "Continue with startup?" -n 1 -r
if [[ $REPLY =~ ^[Yy]$ ]]
then
# enable app services
systemctl enable nifi
systemctl enable rialto
systemctl enable nginx.service
systemctl enable filebeat-events
systemctl enable filebeat-error

# start app services
service filebeat-events start
service filebeat-error start
service nginx.service start
service rialto start
service nifi start

sleep 10
systemctl status nifi rialto nginx filebeat-events filebeat-error

# touch file
cd /data && rm -rf full_startup && touch full_startup
fi
