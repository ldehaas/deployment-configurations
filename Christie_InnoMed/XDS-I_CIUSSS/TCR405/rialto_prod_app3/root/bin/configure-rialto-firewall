#!/bin/bash
#
# Configure Linux Firewall for Rialto
#
# Replace the IPv4 iptables rule set with a set designed for Rialto.
# Custom rules are configured from a simple syntax config file.
#
# Copyright Karos Health Incorporated 2018
#

ConfigFile=/usr/local/etc/firewall.cfg

##############
# Subroutines
##############

usage() {
    cat <<EOF

Configure the Iptables (IPV4) Firewall for Rialto

Usage: $0 [options]

Options:
   -r        Restore firewall rules to OS default.
   -f <file> Use config from specified file.
             Default: /usr/local/etc/firewall.cfg

Type "iptables -L" to display current firewall rules

EOF
}

# Display error message and exit
die() {
    printf "ERROR: $*\r\n"
    exit 1
}

# Display error message and exit (and restore previous firewall config)
croak() {
    echo
    printf "ERROR: $*\r\n"
    echo
    echo "Abandon new rule set. Die die die."
    echo "Reloading previous firewall config"
    service iptables reload
    service ip6tables reload
    echo
    echo "If there's a problem with the previous firewall config, the -r"
    echo "option can be used to restore the firewall rules to OS default."
    exit 1
}

# Preserve original version of a file if we haven't already
preserveOriginal() {
    local file="$1"

    if [ ! -s "$file" ]; then
        die "preserveOriginal: $file missing or zero length"
    elif [ ! -f "$file.orig" ]; then
        echo "Backing up $file to $file.orig"
        cp -p "$file" "$file.orig"
    fi
}

# Restore original version of a file if it was preserved
restoreOriginal() {
    local file="$1"

    if [ -s "$file.orig" ]; then
        echo "Restoring $file from $file.orig"
        cp -p "$file.orig" "$file"
    else
        die "restoreOriginal: $file: no original version to restore"
    fi
}

# Check if a package is installed
isInstalled() {
    rpm -q "$1" &>/dev/null
}

# Return comma separated list of arguments
commaList() {
    local list=''
    local item
    for item in $* ; do
        if [ "$list" ]; then
            list+=",$item"
        else
            list="$item"
        fi
    done
    echo "$list"
}

# Install default firewall rules for Red Hat (which doesn't have any)
setDefaultRules() {
    local file="$1"

    # These default rules are similar to those for CentOS 6.
    echo "Install default firewall rules: $file"
    sed 's/^ *//' <<END >$file
    # Default firewall configuration written by configure-rialto-firewall
    # Manual customization of this file is not recommended.
    *filter
    :INPUT ACCEPT [0:0]
    :FORWARD ACCEPT [0:0]
    :OUTPUT ACCEPT [0:0]
    -A INPUT -m state --state ESTABLISHED,RELATED -j ACCEPT
    -A INPUT -p icmp -j ACCEPT
    -A INPUT -i lo -j ACCEPT
    -A INPUT -m state --state NEW -m tcp -p tcp --dport 22 -j ACCEPT
    -A INPUT -j REJECT
    -A FORWARD -j REJECT
    COMMIT
END
}

# Add a firewall rule to "open a port", i.e. accept traffic on that port
open() {
    local protocol="$1"
    local fromPort="$2"
    local ip="$3"

    local cmd="iptables -A INPUT -m state --state NEW -m $protocol -p $protocol"

    if [ "$ip" ]; then
        echo "Config file: Accept $protocol packets on port $fromPort from $ip"
        cmd+=" -s $ip"
    else
        echo "Config file: Accept $protocol packets on port $fromPort from any IP"
    fi

    cmd+=" --dport $fromPort -j ACCEPT"

    $cmd
    if [ $? -ne 0 ]; then
        croak "$cmd"
    fi
}

# Add a firewall rule to forward incoming traffic from one port to another
forward() {
    local protocol="$1"
    local fromPort="$2"
    local toPort="$3"

    echo "Config file: Forward $protocol packets from port $fromPort to port $toPort"

    # Forwarding from external interfaces
    local cmd="iptables -t nat -A PREROUTING -p $protocol -m $protocol "
    cmd+="--dport $fromPort -j REDIRECT --to-port $toPort"
    $cmd
    if [ $? -ne 0 ]; then
        croak "$cmd"
    fi

    # Forwarding from local interfaces
    local cmd="iptables -t nat -A OUTPUT -o lo -p $protocol -m $protocol "
    cmd+="--dport $fromPort -j REDIRECT --to-port $toPort"
    $cmd
    if [ $? -ne 0 ]; then
        croak "$cmd"
    fi
}

function __isCentos6() {
    grep -Pq 'release \K6\.[0-9]' ${__releaseFile:-/etc/redhat-release}
}
function __isCentos7() {
    grep -Pq 'release \K7\.[0-9]' ${__releaseFile:-/etc/redhat-release}
}


########################
# Execution starts here
########################

# Must be root.
if [ $EUID -ne 0 ]; then
    die 'Must be root'
fi

# Process command line options
while getopts 'f:r?' flag; do
    case $flag in
        f)  # Read config from specified file
            ConfigFile="$OPTARG"
            ;;
        r)  # Restore firewall rules to OS default
            restoreOriginal /etc/sysconfig/iptables
            restoreOriginal /etc/sysconfig/ip6tables
            service iptables reload
            service ip6tables reload
            exit 0
            ;;
        \?)
            usage
            exit 1
            ;;
    esac
done

if [ ! -f "$ConfigFile" ]; then
    die "Config file not found: $ConfigFile"
fi

echo 'Ensure firewall packages are installed'
if ! isInstalled "iptables" ; then
    die "Package iptables is not installed"
fi

__isCentos6 && { ip6Package=iptables-ipv6 ; }
__isCentos7 && { ip6Package=iptables-services ; }
if ! isInstalled "${ip6Package}" ; then
    die "Package iptables-services is not installed"
fi

echo 'Enable firewall autostart on boot'
if ! chkconfig iptables on ; then
    die "Error enabling iptables autostart"
fi
if ! chkconfig ip6tables on ; then
    die "Error enabling ip6tables autostart"
fi

# Red Hat doesn't ship with default firewall rules and iptables
# won't start if there are no rules.
file=/etc/sysconfig/iptables
if [ ! -f $file ]; then
    setDefaultRules $file
fi
file=/etc/sysconfig/ip6tables
if [ ! -f $file ]; then
    setDefaultRules $file
fi

echo 'Ensure firewall is running'
if ! service iptables start ; then
    die "Error starting IPv4 firewall (iptables)"
fi
if ! service ip6tables start ; then
    die "Error starting IPv6 firewall (ip6tables)"
fi

# Preserve OS default firewall config
preserveOriginal /etc/sysconfig/iptables
preserveOriginal /etc/sysconfig/ip6tables

# Temporarily change default policy on INPUT chain so we don't
# lock ourselves out when we flush the current rules.
echo 'Temporarily disable incoming packet policy'
iptables -P INPUT ACCEPT

# Flush and re-add basic rules.
echo 'Wiping filter and nat chains'
echo 'Flush all rules'
iptables -F
iptables -t nat -F
echo 'Add rule: Accept packets for established connections'
iptables -A INPUT -m state --state ESTABLISHED,RELATED -j ACCEPT
echo 'Add rule: Allow ping'
iptables -A INPUT -p icmp -j ACCEPT
echo 'Add rule: Accept incoming packets for localhost (loopback)'
iptables -A INPUT -i lo -j ACCEPT

# Accept packets for known services.
echo 'Add rule: Allow SSH connections'
iptables -A INPUT -m state --state NEW -m tcp -p tcp --dport 22 -j ACCEPT

########################################################
########################################################
###            ATTENTION CUSTOMER SERVICES
###           Do not customize this script.
### This script is overwritten when Rialto is upgraded.
###       Put custom rules in the config file.
###
### Please send feedback to john.lammers@karoshealth.com
########################################################
########################################################


# Rules for Cassandra database nodes
# See http://docs.datastax.com/en/
#   cassandra/2.1/cassandra/security/secureFireWall_r.html
# Rialto App nodes must also be added to firewall config file.
file=/opt/cassandra/conf/cassandra-topology.properties
if [ -f $file ]; then

    # Extract IPv4 addresses from topology file.
    ipAddresses=$(grep "^[[:space:]]*[1-9]" $file \
        | sed -e 's/^[[:space:]]*//' -e 's/[:space:]*=.*//' \
        | sort \
        | uniq)

    echo "Cassandra nodes (from cassandra-topology.properties):"
    echo "$ipAddresses"

    echo "Add rules for Cassandra:  Allow inter-node TCP packets"
    ports='7000:7001 7199 9042 9160'
    ipList=$(commaList $ipAddresses)
    for p in $ports ; do
        iptables -A INPUT -s $ipList -m state --state NEW -m tcp -p tcp \
            --dport $p -j ACCEPT
    done
fi

# Rules for Elasticsearch nodes
#TODO: consider limiting access to rialto nodes only

if isInstalled elasticsearch ; then
    file=/etc/elasticsearch/elasticsearch.yml
    httpPort=$(egrep -vh '^[[:space:]]*#' $file | grep 'http\.port' \
        | head -1 | awk '{print $2}')
    httpPort=${httpPort:-9200}
    open tcp $httpPort

    # The port that other nodes in the cluster should use when communicating with this node.
    # See: https://www.elastic.co/guide/en/elasticsearch/reference/current/modules-transport.html
    publishPort=$(egrep -vh '^[[:space:]]*#' $file | grep 'transport\.publish_port' \
        | head -1 | awk '{print $2}')
    publishPort=${publishPort:-9300}
    open tcp $publishPort
fi

# Rules for kibana
#TODO: consider limiting access to rialto nodes only

if isInstalled kibana ; then
    file=/etc/kibana/kibana.yml
    kibanaPort=$(egrep -vh '^[[:space:]]*#' $file | grep 'server\.port' \
        | head -1 | awk '{print $2}')
    kibanaPort=${kibanaPort:-5601}
    open tcp $kibanaPort
fi

# Rules for NiFi
nifiProperties=/opt/nifi/conf/nifi.properties
if [ -f $nifiProperties ] ; then
    nifiWebPort=$(egrep -vh '^[[:space:]]*#' $nifiProperties |
        grep nifi\.web\.http\.port | head -1 | awk -F= '{print $2}')
    nifiWebPort=${nifiWebPort:-8081}
    open tcp $nifiWebPort
fi

# Rules for PostgreSQL replication nodes
file=/var/lib/pgsql/9.4/data/postgresql.conf
if [ -f $file ] && grep -q "^ *wal_level *= *'hot_standby'" $file ; then

    # Extract IPv4 addresses from PostgreSQL client authentication file.
    file=/var/lib/pgsql/9.4/data/pg_hba.conf
    pattern='^[[:space:]]*host'
    pattern+='[[:space:]]*replication'
    pattern+='[[:space:]]*replicator'
    pattern+='[[:space:]]*[1-9]'
    ipAddresses=$(grep $pattern $file | awk -F '[/ ]+' '{print $4}')
    if [ -z "$ipAddresses" ]; then
        die "Failed to extract IP addresses from $file"
    fi

    echo "Add rule for PostgreSQL replication:  Allow inter-node TCP packets"
    ipList=$(commaList $ipAddresses)
    iptables -A INPUT -s $ipList -m state --state NEW -m tcp -p tcp \
        --dport 5432 -j ACCEPT
fi


# Add rules from config file
echo "Begin processing config from $ConfigFile"
declare -i lineNumber=0
while read line ; do
    ((++lineNumber))

    # Ignore comments and empty lines
    if [[ "$line" =~ ^\#|^$ ]]; then
        continue
    fi

    # Delete any trailing comment
    line=$(sed 's/#.*//' <<<$line)

    # Parse the line
    read command arg1 arg2 remainder <<<"$line"

    # Accept a manually specified rule
    if [ "$command" == 'iptables' ]; then
        echo "Config file: Adding manually specified rule"
        echo "    $line"
        if ! $line ; then
            croak "Command failed, line number $lineNumber"
        fi
        continue
    fi

    # Die if there's extra arguments.  Probably due to a typo.
    if [ "$remainder" ]; then
        croak "Extra argument '$remainder' in $ConfigFile" \
            "line $lineNumber: '$line'"
    fi

    # Perform the command
    case $command in
        open|openTCP) # Open a TCP port
            open tcp $arg1 $arg2
            ;;
        openUDP) # Open a UDP port
            open udp $arg1 $arg2
            ;;
        forward|forwardTCP) # Forward a TCP port
            forward tcp $arg1 $arg2
            ;;
        forwardUDP) # Forward a UDP port
            forward udp $arg1 $arg2
            ;;
        *)
            croak "Unrecognized command '$command' in $ConfigFile" \
                "line $lineNumber: '$line'"
            ;;
    esac
done <"$ConfigFile"
echo "Finished processing config from $ConfigFile"


# The following rules were present in the default for CentOS 6.7.
# I'm not sure why they are needed, but we'll keep them around.
iptables -A INPUT -j REJECT --reject-with icmp-host-prohibited
iptables -A FORWARD -j REJECT --reject-with icmp-host-prohibited

# Restore policies
echo 'Restore incoming packet policy: disallow'
iptables -P INPUT DROP
ip6tables -P INPUT DROP
echo 'Set default OUTPUT packet policy: allow'
iptables -P OUTPUT ACCEPT
ip6tables -P OUTPUT ACCEPT
echo 'Set default FORWARD packet policy: disallow'
iptables -P FORWARD DROP
ip6tables -P FORWARD DROP

# Save configuration (preserve across reboot)
service iptables save
service ip6tables save
echo "Firewall configuration complete"
