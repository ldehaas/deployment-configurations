#!/bin/bash
#
# Force an HA failover
#
# Run on either host, irregardless of which node is currently active.
#
# Copyright Karos Health Incorporated 2018
#

CRM_MON=/usr/sbin/crm_mon
PCS=/usr/sbin/pcs

die() {
    echo $*
    exit 1
}

# Must be root.
if [ $EUID -ne 0 ]; then
    die 'Must be root'
fi

if [ ! -f $CRM_MON ]; then
    die "Command $CRM_MON not found"
fi

if [ ! -f $PCS ]; then
    die "Command $PCS not found"
fi

# TODO:
# - Add usage message
# - Add prompt "Enter y to failover to blah: "
# - Add -y option to not prompt

# Get the list of online hosts
if ! $CRM_MON -1 >/tmp/ha_status; then
    die "Failed to query HA status"
fi

if ! grep '^Online' /tmp/ha_status >/tmp/online; then
    die "Failed to find any Online hosts"
fi

brace1=$(cat /tmp/online | awk '{print $2}')
host1=$(cat /tmp/online | awk '{print $3}')
host2=$(cat /tmp/online | awk '{print $4}')
brace2=$(cat /tmp/online | awk '{print $5}')
rm -f /tmp/online

if [ "$brace1" != "[" ]; then
    die "Error parsing list of online hosts"
fi

if [ "$brace2" != "]" ]; then
    die "Error parsing list of online hosts"
fi
echo "Online hosts: $host1 $host2"

# Get the currently active host
if ! grep 'Started' /tmp/ha_status >/tmp/active; then
    die "Failed to find currently active host"
fi
rm -f /tmp/ha_status

active=$(cat /tmp/active | sed 's/.*Started //' | awk '{print $1}')
echo "Active host = $active"

# Get the currently inactive host
if [ "$active" == "$host1" ]; then
    inactive="$host2"
else
    inactive="$host1"
fi
echo "Inactive host = $inactive"

# Trigger failover
echo "Forcing failover to inactive host $inactive"
if ! $PCS resource move virtualIP $inactive; then
    echo "Error moving virtual IP resource to host $inactive"
else
    sleep 2
fi

echo "Releasing failover control back to Pacemaker"
if ! $PCS resource clear virtualIP; then
    echo "Error returning virtual IP resource control to Pacemaker"
fi

echo
$CRM_MON -1
