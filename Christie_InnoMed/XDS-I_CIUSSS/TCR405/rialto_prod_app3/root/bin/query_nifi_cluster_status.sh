#query NiFi api to get cluster status

adapter=$(netstat -tulpn | grep 8088 | grep -v 127.0.0.1 | awk '{ print $4 }' | sed 's/:.*//')

curl -s http://$adapter:8088/nifi-api/controller/cluster | /root/bin/jq-linux64 '.cluster.nodes[] | [ .address, .status, .roles]|tostring' | tr -d '\\{}[]"'
