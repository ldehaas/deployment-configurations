#!/bin/bash
logger Checking for iSCSI mount
mount -a
until [ -f /data/iSCSImounted ]; do sleep 5; done
logger iSCSI mount is available

