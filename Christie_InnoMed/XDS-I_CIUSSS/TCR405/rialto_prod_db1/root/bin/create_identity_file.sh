while IFS=',' read -ra CONTEXT; do 
cat -v <<EOF >> identitysources.xml

        <identitySourceConfig>
            <namespaceId>${CONTEXT[1]}</namespaceId>
            <universalId>${CONTEXT[0]}</universalId>
            <universalIdType>ISO</universalIdType>
            <sendingApplication>CIUSSS</sendingApplication>
            <sendingFacility>${CONTEXT[1]}</sendingFacility>
        </identitySourceConfig>
EOF
done < /root/bin/CIUSSS_OID_Facility.txt
