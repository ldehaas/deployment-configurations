# delete prior files
rm -rf /tmp/orders-schedule-indices.txt
rm -rf /tmp/orders-schedule-indices2.txt

# collect a list of all order schedule indices present in the cluster
curl -s localhost:9200/_cat/indices | grep order-schedule | sort | awk '{ print $3 }' > /tmp/orders-schedule-indices.txt

# generate comma delimited output of future remaining scheduled orders based on the scheduledProcedureStepStartDateTime remaining in each index
while read indices; do
  count=$(curl -s "http://localhost:9200/$indices/_count" -H 'Content-Type: application/json' -d '{"query": {"range" : {"scheduledProcedureStepStartDateTime": {"gte": "now"}}}}' | jq-linux64 -j '[. | {count}, {successful: ._shards.successful}, {failed: ._shards.failed}]|tostring' | tr -d ':a-zA-Z{}[]"')
  echo "$count,$indices" >> /tmp/orders-schedule-indices2.txt
done </tmp/orders-schedule-indices.txt

# evaluate and delete indices with no future orders
# ADDR0 future orders remaining
# ADDR1 successful shard count (should be 5)
# ADDR2 errors (should be 0)
# ADDR3 order schedule index

while IFS=',' read -ra ADDR; do      
  if [ "${ADDR[0]}" -eq "0" ] && [ "${ADDR[1]}" -eq "5" ] && [ "${ADDR[2]}" -eq "0" ]; then 
    echo "${ADDR[0]}","${ADDR[1]}","${ADDR[2]}" "${ADDR[3]} no longer has future orders";
  fi
 done < /tmp/orders-schedule-indices2.txt
