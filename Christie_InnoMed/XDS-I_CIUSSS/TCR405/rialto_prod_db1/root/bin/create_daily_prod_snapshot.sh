namespace=rialto-es-prod
snapshotdate=$(date +"%m-%d-%Y")

SNAPSHOT()
{
cat <<EOF
{
"indices": "$namespace-context,$namespace-device,$namespace-script,$namespace-prior-rules",
"ignore_unavailable": true,
"include_global_state": false
}
EOF
}
curl -XPUT "http://localhost:9200/_snapshot/rialto_prod_backups/$namespace-$snapshotdate?wait_for_completion=true" -H 'Content-Type: application/json' -d "$(SNAPSHOT)"
