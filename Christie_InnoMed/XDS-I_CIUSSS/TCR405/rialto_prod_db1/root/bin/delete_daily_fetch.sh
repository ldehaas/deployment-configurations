# delete prior files
rm -rf /tmp/daily-fetch-indices.txt

# collect a list of all daily fetch indices present in the cluster
curl -s localhost:9200/_cat/indices | grep daily-fetch | sort | awk '{ print $3 }' > /tmp/daily-fetch-indices.txt

currentday=$(date --utc +%F)
previousday=$(date --date='-1 day' --utc +%F)

while read indices; do
  echo $indices
  if [ $indices != "rialto-es-prod-daily-fetch-$currentday" ] && [ $indices != "rialto-es-prod-daily-fetch-$previousday" ]; then
    echo "$indices is not a protected index, OK to delete";
    curl -s -XDELETE "http://localhost:9200/$indices"
  fi
done </tmp/daily-fetch-indices.txt
