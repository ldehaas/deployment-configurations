import com.karos.rtk.common.Pid

log.info("Here's how the order message looks:]\n{}", input)
log.info("And here's how the initial Imaging Service Request looks like\n{}", imagingServiceRequest)

if (imagingServiceRequest.getAccessionNumberUniversalId() == null) {
    imagingServiceRequest.setAccessionNumberNamespaceId(get("PID-3-4-1"))
    imagingServiceRequest.setAccessionNumberUniversalId(get("PID-3-4-2"))
    imagingServiceRequest.setAccessionNumberUniversalIdType(get("PID-3-4-3"))
}

def otherPatientIds = imagingServiceRequest.getPatientIdentification().getOtherPatientIds()
pid2 = new Pid(input.get("PID-2-1"),
               input.get("PID-2-4-1"),
               input.get("PID-2-4-2"),
               input.get("PID-2-4-3"))
otherPatientIds.add(pid2)
imagingServiceRequest.getPatientIdentification().setOtherPatientIds(otherPatientIds)

def patientDemographics = imagingServiceRequest.getPatientDemographics()
//patientDemographics.setPatientBirthDate(get("PID-6"))
imagingServiceRequest.setPatientDemographics(patientDemographics)

def requestedProcedure = imagingServiceRequest.getRequestedProcedureSequence().get(0)
requestedProcedure.setRequestedProcedureDescription(get("OBR-5"))
//requestedProcedure.setStudyInstanceUID(get("ZDS-1"))

def scheduledProcedureStep = requestedProcedure.getScheduledProcedureStepSequence().get(0)
//scheduledProcedureStep.setScheduledProcedureStepStatus(get("OBR-1"))
scheduledProcedureStep.setScheduledProcedureStepLocation(get("OBR-2"))
scheduledProcedureStep.setScheduledStationName(get("OBR-4"))
if (scheduledProcedureStep.getScheduledProcedureStepLocation() == null) {
    scheduledProcedureStep.setScheduledProcedureStepLocation("DEFAULT_LOCATION_CT");
}
if (get("ORC-1") == "CA") {
  scheduledProcedureStep.setScheduledProcedureStepStatus("CA")
}

scheduledProcedureStep.setModality(get("OBR-24"))
if (scheduledProcedureStep.getModality() == null) {
    scheduledProcedureStep.setModality("CT");
}

log.info("Finished fixing up the Imaging Service Request...Here it is\n{}", imagingServiceRequest)
