#!/bin/bash
# full shutdown db server

# disable database services
systemctl disable cassandra
systemctl disable elasticsearch.service
systemctl disable kibana

# stop database services
service cassandra stop
service elasticsearch stop
service kibana stop

sleep 10

# touch file
cd /data && rm -rf full_shutdown && touch full_shutdown

#unmount /data and /log drives and comment out in /etc/fstab
cd / && umount /data
sed -i '/[/]data/ s/^/#/' /etc/fstab 
cd / && umount /log
sed -i '/[/]log/ s/^/#/' /etc/fstab
cat /etc/fstab