#
# Rialto Firewall Configuration
#
# This configuration file is read by the Rialto configure-rialto-firewall
# script.  The script implements a standardized Linux firewall set up for
# Rialto but supplements the firewall rules with the configuration supplied
# by this script.
#
# Please consult the Rialto product documentation for more information.
#

#################################
# Accept TCP Traffic by Port & IP
#################################
# Syntax:
#     open port [ip]
# Where:
#     port can be a port or port range
#         e.g. 443
#         e.g. 8080:8082
#     ip can be an IP address, subnet, or comma separated list of the same
#         e.g. 10.242.22.47
#         e.g. 10.242.22.0/24
#         e.g. 10.242.22.0/255.255.255.0
#         e.g. 10.242.22.47,10.242.23.0/24,10.242.250.0/24
#
# Please note that the firewall configuration script automatically:
# - Opens port 22 for SSH from any IP address
# - Automatically allows inter-node packets for Cassandra
# - Automatically configures firewall for Corosync if installed
##########################################################################

# EXAMPLES:

# Accept packets on this port from this IP address
# open 11112 10.242.43.44

# Accept packets on this port from any IP address (wide open)
# open 443

# Accept packets from all devices on a subnet
# open 5900:5932 10.242.43.0/24
# -or-
# open 11112 10.242.43.0/255.255.255.0

# ***********************
# *** Cassandra Nodes ***
# ***********************
# Configure this on all Cassandra nodes in the cluster.  Accept database
# client connections from all Rialto App nodes on these two ports:
# open 9042 10.242.43.44,10.242.43.47,10.242.43.50
# open 9160 10.242.43.44,10.242.43.47,10.242.43.50


#####################
# TCP Port Forwarding
#####################
# Syntax:
#     forward source-port destination-port
# Where
#     source-port is an external port to accept traffic on
#     destination-port is an *open* port to forward traffic to
#
# Note that the destination port must be open.
#################################################################

# EXAMPLES:

# Forward packets received on port 443 to Rialto Navigator on port 2525.
# open 2525
# forward 443 2525

# Forward packets received on port 80 to Rialto Navigator on port 8080.
# open 8080
# forward 80 8080

# Accept UDP packets for Audit Record Repository on ports 4514 and 2514
# openUDP 4514
# forwardUDP 2514 4514


##########################
# Manually specified rules
##########################
# Syntax:
#     iptables ...
# See man iptables(8)
##########################

# EXAMPLES:

# Accept packets on port 8080 only on network interface "eth0"
# iptables -A INPUT -p tcp --dport 8080 -i eth0 -j ACCEPT

# Accept NTP queries (uses UDP, not TCP)
# iptables -A INPUT -m state --state NEW -p udp --dport 123 -j ACCEPT

##########################
# DEFAULT PORTS FOR RIALTO
##########################
# These Ports are the defaults for Rialto to work "out of the box".
# These ports are only meant as default. They are read in from the install-script and
# configured right away upon install. If a port is removed before installing Rialto,
# it may manually be configured at a later time by editing '/usr/local/etc/firewall.cfg'
# as root and running 'configure-rialto-firewall'
##########################

# *** GUI ports   ***
open 2525 #UI Port
#open 7070 #ARR-GUI Port

# *** EMPI Port  ***
#open 2399 #hl7port

# *** xdsRegistry Ports  ***
#open 8081 #registry port
open 2395 #hl7Port

# *** xdsRepository Ports  ***
#open 8082

# *** imageArchive Ports  ***
open 11112 #cFind Port
open 11113 #cStore Port
#open 2455 #Update Studies Port

# *** HTTP Port  ***
open 8080 #default http device port

# *** MWL Port ***
#open 2576 #hl7port
#open 4106 #DICOM Port

# *** Workflow Engine Port ***
#open 13337 #WorkflowEngine port
#open 2575 #Workflow Destination port

# *** Nifi GUI ***
open 8088 10.128.52.174,10.128.52.175,10.128.52.176,10.128.52.214 

# *** Nifi Zookeeper Service ***
open 2181 10.128.52.174,10.128.52.175,10.128.52.176,10.128.52.214 #Client Port
open 2888 10.128.52.174,10.128.52.175,10.128.52.176,10.128.52.214 #Peer Port
open 3888 10.128.52.174,10.128.52.175,10.128.52.176,10.128.52.214 #Leader Port
open 4040 10.128.52.174,10.128.52.175,10.128.52.176,10.128.52.214 #Cluster Coordination Port
