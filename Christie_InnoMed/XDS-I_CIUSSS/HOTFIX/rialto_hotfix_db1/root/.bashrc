# .bashrc

# User specific aliases and functions

alias rm='rm -i'
alias cp='cp -i'
alias mv='mv -i'

alias c=clear

# Source global definitions
if [ -f /etc/bashrc ]; then
	. /etc/bashrc
fi
# BEGIN RIALTO (replaced on upgrade)
alias l='ls -la'
alias r='su - rialto'
alias rs="service rialto restart ; sleep 2 ; less -i +F /var/log/rialto/rialto.log"
alias tl="less -i +F /var/log/rialto/rialto.log"
# END RIALTO
