#!/bin/bash
#
# Start and Stop Cassandra with Automatic Restart
#
# This is the "init" script for Cassandra, a No-SQL storage and
# database system.
#
# Configuration for chkconfig(8):
# chkconfig: 2345 90 10
# description: Cassandra
#
# To enable automatic start and stop at boot and shutdown enter
# the command "chkconfig <app_instance_name> reset"
# e.g. chkconfig cassandra reset
#
# To disable automatic start and stop at boot and shutdown enter
# the command "chkconfig --del <app_instance_name>"
# e.g. chkconfig --del cassandra
#
# Copyright Karos Health Incorporated 2018
#

. /etc/rc.d/init.d/functions
. /etc/profile.d/cassandra.sh

APP_NAME='Cassandra'
APP_USER='rialto'
APP_BIN=$CASSANDRA_HOME/bin/cassandra

LOG_DIR=/var/log/cassandra
mkdir -m 0755 -p $LOG_DIR ; chown $APP_USER $LOG_DIR
STDOUT_FILE=$LOG_DIR/cassandra.out
LOG_FILE=$LOG_DIR/start_stop.log
 
RIALTO_HOME=/opt/rialto
NEWRELIC_AGENT_JAR=$RIALTO_HOME/lib/jar/newrelic-agent-3.33.0.jar

PID_DIR=/var/run/cassandra
mkdir -m 0755 -p $PID_DIR ; chown $APP_USER $PID_DIR
APP_PID=$PID_DIR/cassandra.pid
RESTART_PID=$PID_DIR/cassandra_restart.pid


log() {
    echo $(date) $* >>$LOG_FILE
}

die() {
    echo "$*" 1>&2
    log "$*"
    exit 1
}

isAppAlive() {
    [ -f $APP_PID ] && checkpid `cat $APP_PID`
}

isRestartAlive() {
    [ -f $RESTART_PID ] && checkpid `cat $RESTART_PID`
}

quitIfNotRoot()
{
    if [ $UID -ne 0 ]; then
        echo 'Sorry, must be root'
        exit 1
    fi
}

doStart() {
    # Prevent starting two copies of the application.
    if isRestartAlive; then
        echo "$APP_NAME auto restart shell is already running"
    fi
    if isAppAlive; then
        echo "$APP_NAME is already running"
        exit 0
    fi
    if isRestartAlive; then
        echo "$APP_NAME is not running but will restart automatically"
        exit 0
    fi

    # Cassandra doesn't produce a diagnostic if there's not enough memory
    # for it to start, so we do it here.
    declare -i memory=$(grep '^MemTotal:' /proc/meminfo | awk '{print $2}')
    if [ $memory -lt 1922860 ]; then
        die 'Sorry, Cassandra cannot start with less than 2 GB of memory'
    fi

    # Increase readahead sectors on Cassandra's data file system.
    # See http://www.datastax.com/docs/1.2/install/recommended_settings
    # System default for CentOS 6.x is 256.
    fsys=$(df -h /var/lib/cassandra/data | \
        grep -v '^Filesystem' | \
        grep '^/' | \
        awk '{print $1}')
    blockdev --setra 512 $fsys

    # Cassandra 2.0.3 spews errors to console if hostname not in /etc/hosts
    # Do this every time in case the host name has changed.
    #sed -i -e '/Autoadded by Cassandra init.d/d' /etc/hosts
    #sed -i -e '$a\127.0.0.1 '$(hostname)' # Autoadded by Cassandra init.d' /etc/hosts

    # New Relic agent requires that we pass several options to the jvm
    export JVM_OPTS="
    -XX:+HeapDumpOnOutOfMemoryError
    -Dnewrelic.config.file=$RIALTO_HOME/etc/newrelic-java-agent.yml
    -Dnewrelic.config.log_file_path=$RIALTO_HOME/log
    -Dnewrelic.environment=production
    -javaagent:$NEWRELIC_AGENT_JAR
    "

    # Put a shell into background that will automatically restart
    # the application in case it should hit an error and die.
    log 'Starting auto restart shell'
    echo -n "Starting $APP_NAME auto restart shell: "
    (
        echo $BASHPID >$RESTART_PID

        while [ 'true' ]; do
            log "Starting $APP_NAME"
            echo '--- Starting ---' >>$STDOUT_FILE
            daemon --user $APP_USER $APP_BIN -p $APP_PID 2>&1 >>$STDOUT_FILE
            usleep 500000

            # Poll the application until it dies
            PID=$(cat $APP_PID)
            while [ -e /proc/$PID ]; do
                sleep 30
            done
            log "$APP_NAME stopped unexpectedly, will restart"
            echo '--- Stopped unexpectedly ---' >>$STDOUT_FILE

            # Don't be in a rush to restart.  If the application cannot restart
            # we don't want to fill the hard drive with the log file.
            sleep 30
        done
    ) &
    if [ $? -eq 0 ]; then
        echo_success
    else
        echo_failure
    fi
    echo

    # Report whether the application started.
    echo -n "Starting $APP_NAME: "
    sleep 2
    if isAppAlive; then
        Exit_value=0
        echo_success
    else
        Exit_value=1
        echo_failure
    fi
    echo
}


doStop() {

    # Stop the auto restart shell
    if isRestartAlive; then
        log 'Stopping auto restart shell'
        echo -n 'Stopping auto restart shell'
        if kill `cat $RESTART_PID`; then
            Exit_value=0
            echo_success
        else
            Exit_value=1
            echo_failure
        fi
        echo
    else
        echo 'Auto restart shell is already stopped'
    fi

    # Make sure it's really stopped
    while isRestartAlive; do
        echo 'Waiting for auto restart shell to stop ...'
        sleep 2
    done
    rm -f $RESTART_PID

    # Stop the application
    if isAppAlive; then
        log "Stopping $APP_NAME"
        echo -n "Stopping $APP_NAME: "
        if kill `cat $APP_PID`; then
            sleep 2 # Give application a chance to shut down
            Exit_value=0
            echo_success
        else
            Exit_value=1
            echo_failure
        fi
        echo
    else
        echo "$APP_NAME is already stopped"
    fi

    # Make sure it's really stopped
    while isAppAlive; do
        echo "Waiting for $APP_NAME to stop ..."
        sleep 2
    done
    rm -f $APP_PID
}


status_fn() {
    if isRestartAlive; then
        echo "$APP_NAME auto restart shell is running"
    else
        echo "$APP_NAME auto restart shell is not running"
    fi

    if isAppAlive; then
        echo "$APP_NAME is running"
        exit 0
    elif isRestartAlive; then
        echo "$APP_NAME is not running (but will restart automatically)"
    else
        echo "$APP_NAME is not running"
    fi
}


# Sanity check
if [ ! -x $APP_BIN ]; then
    die "Application executable file not found: $APP_BIN"
fi

# Ensure log file exists and is owned by rialto userid.
if [ $UID -eq 0 ]; then
    touch $LOG_FILE
    chown rialto: $LOG_FILE
fi


Exit_value=0
case "$1" in
    start)
        quitIfNotRoot
        doStart
        ;;
    stop)
        quitIfNotRoot
        doStop
        ;;
    status)
        status_fn
        ;;
    restart)
        quitIfNotRoot
        doStop
        doStart
        ;;
    *)
    echo 'Usage: $0 {start|stop|restart|status}'
    Exit_value=3
esac

exit $Exit_value
