#!/bin/bash
#
# heartbeat-elastic          heartbeat shipper
#
# chkconfig: 2345 98 02
# description: Starts and stops a single heartbeat instance on this system
#

### BEGIN INIT INFO
# Provides:          heartbeat-elastic
# Required-Start:    $local_fs $network $syslog
# Required-Stop:     $local_fs $network $syslog
# Default-Start:     2 3 4 5
# Default-Stop:      0 1 6
# Short-Description: Ping remote services for availability and log results to Elasticsearch or send to Logstash.
# Description:       heartbeat is a shipper part of the Elastic Beats
#					 family. Please see: https://www.elastic.co/products/beats
### END INIT INFO



PATH=/usr/bin:/sbin:/bin:/usr/sbin
export PATH

[ -f /etc/sysconfig/heartbeat-elastic ] && . /etc/sysconfig/heartbeat-elastic
pidfile=${PIDFILE-/var/run/heartbeat-elastic.pid}
agent=${BEATS_AGENT-/usr/share/heartbeat/bin/heartbeat}
args="-c /etc/heartbeat/heartbeat.yml -path.home /usr/share/heartbeat -path.config /etc/heartbeat -path.data /var/lib/heartbeat -path.logs /var/log/heartbeat"
test_args="-e test config"
beat_user="${BEAT_USER:-root}"
wrapper="/usr/share/heartbeat/bin/heartbeat-god"
wrapperopts="-r / -n -p $pidfile"
user_wrapper="su"
user_wrapperopts="$beat_user -c"
RETVAL=0

# Source function library.
. /etc/rc.d/init.d/functions

# Determine if we can use the -p option to daemon, killproc, and status.
# RHEL < 5 can't.
if status | grep -q -- '-p' 2>/dev/null; then
    daemonopts="--pidfile $pidfile"
    pidopts="-p $pidfile"
fi

if command -v runuser >/dev/null 2>&1; then
    user_wrapper="runuser"
fi

[ "$beat_user" != "root" ] && wrapperopts="$wrapperopts -u $beat_user"

test() {
	$user_wrapper $user_wrapperopts "$agent $args $test_args"
}

start() {
    echo -n $"Starting heartbeat: "
	test
	if [ $? -ne 0 ]; then
		echo
		exit 1
	fi
    daemon $daemonopts $wrapper $wrapperopts -- $agent $args
    RETVAL=$?
    echo
    return $RETVAL
}

stop() {
    echo -n $"Stopping heartbeat: "
    killproc $pidopts $wrapper
    RETVAL=$?
    echo
    [ $RETVAL = 0 ] && rm -f ${pidfile}
}

restart() {
	test
	if [ $? -ne 0 ]; then
		return 1
	fi
    stop
    start
}

rh_status() {
    status $pidopts $wrapper
    RETVAL=$?
    return $RETVAL
}

rh_status_q() {
    rh_status >/dev/null 2>&1
}

case "$1" in
    start)
        start
    ;;
    stop)
        stop
    ;;
    restart)
        restart
    ;;
    condrestart|try-restart)
        rh_status_q || exit 0
        restart
    ;;
    status)
        rh_status
    ;;
    *)
        echo $"Usage: $0 {start|stop|status|restart|condrestart}"
        exit 1
esac

exit $RETVAL
