#!/bin/bash
# full startup db server

#mount /data and /log drive and enable in /etc/fstab
sed -i '/^#.*[/]data/s/^#//' /etc/fstab 
#sed -i '/^#.*[/]log/s/^#//' /etc/fstab
cat /etc/fstab && mount -a

# enable database services
#systemctl enable cassandra
systemctl enable elasticsearch.service
systemctl enable kibana

# start database services
#service cassandra start
service elasticsearch start
service kibana start

# touch file
cd /data && rm -rf full_startup && touch full_startup
