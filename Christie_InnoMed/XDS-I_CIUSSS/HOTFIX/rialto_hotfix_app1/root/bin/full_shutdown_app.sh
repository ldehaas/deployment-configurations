#!/bin/bash
# full shutdown app server

# disable app services
systemctl disable nifi
systemctl disable rialto
systemctl disable nginx.service
systemctl disable filebeat-events
systemctl disable filebeat-error

# stop app services
service nifi stop
service rialto stop
service nginx.service stop
service filebeat-events stop
service filebeat-error stop

sleep 10

# touch file
cd /data && rm -rf full_shutdown && touch full_shutdown

#unmount /data drive and comment out in /etc/fstab
cd / && umount /data && sed -i '/[/]data/ s/^/#/' /etc/fstab && cat /etc/fstab
