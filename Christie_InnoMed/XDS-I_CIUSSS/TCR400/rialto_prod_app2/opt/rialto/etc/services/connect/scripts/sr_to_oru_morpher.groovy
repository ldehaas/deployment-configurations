/**
 * This script creates an ORU report message from a DICOM Structured Report.
 *
 * It is optional.  If not configured, SRs will be passed directly to the PACS.
 * If configured, the SRs will not be delivered, but the generated ORU will be.
 *
 * The input Structured Report will have already been localized by the
 * foreign image localizer script.
 *
 * This script can return false to signal that the SR should be dropped.
 */

initialize("ORU", "R01", "2.3")

set("PID-2", get(PatientID))
// TODO: find the local pid for PID-3

setPersonName("PID-5", get(PatientName))

set("PID-7", get(PatientBirthDate))
set("PID-8", get(PatientSex))

set("ORC-1", "RE")
set("ORC-3", get(AccessionNumber))

set("OBR-1", "1")
set("OBR-3", get(AccessionNumber))
set("OBR-4", get(StudyDescription))
//set("OBR-6", requested date time ??)
set("OBR-7", get(StudyDate) + get(StudyTime))

anatomicRegionSeq = get(AnatomicRegionSequence)
if (!anatomicRegionSeq.isEmpty()) {
    anatomicRegion = anatomicRegionSeq.first()
    set("OBR-15-1-1", anatomicRegion.get(CodeValue))
    set("OBR-15-1-2", anatomicRegion.get(CodeMeaning))
    set("OBR-15-1-3", anatomicRegion.get(CodingSchemeDesignator))
}

// our sample SR includes the physician id (non-standard)
setPersonName("OBR-16", get(ReferringPhysicianName))

set("OBR-19", get(AccessionNumber))
set("OBR-20", get(StudyID))
set("OBR-22", get(ContentDate) + get(ContentTime))
set("OBR-24", get(Modality))

completionFlag = null
performedProcedureSeq = get(PerformedProcedureCodeSequence)
if (!performedProcedureSeq.isEmpty()) {
    performedProcedure = performedProcedureSeq.first()
    completionFlag = performedProcedure.get(CompletionFlag)
}

set("OBR-25", completionFlag)

verifyingObserverName = null
verifyingObserverSeq = get(VerifyingObserverSequence)
if (!verifyingObserverSeq.isEmpty()) {
    verifyingObserver = verifyingObserverSeq.first()
    verifyingObserverName = verifyingObserver.get(VerifyingObserverName)
}

setPersonName("OBR-32-1", verifyingObserver.get(VerifyingObserverName),
    startingPosition=2)

def gatherTextContent(contentSeq, output) {
    contentSeq.each { content ->
        if ("TEXT".equals(content.get(ValueType))) {
            meaning = null
            conceptNameCodeSeq = content.get(ConceptNameCodeSequence)
            if (!conceptNameCodeSeq.isEmpty()) {
                meaning = conceptNameCodeSeq.first().get(CodeMeaning)
            }

            output.add([meaning, content.get(TextValue)])

        } else if ("CONTAINER".equals(content.get(ValueType))) {
            gatherTextContent(content.get(ContentSequence), output)
        }
    }
}

/**
 * Some SRs that we have seen in production have invalid escape sequences in
 * their report content.  For example, the string "\X09\" instead of a tab
 * character.  This causes visual problems in the reports as displayed by
 * PACSs so we should replace them with what is likely the correct character...
 */
def replaceInvalidEscapeSequences(content) {
    // It appears that the escape sequence is \X??\ where ?? is a hex number
    // representing the actual ascii code for the character.  So we parse that
    // to a number and cast it to a char to get the intended string value.
    return content.replaceAll("\\\\X(\\p{XDigit}{2})\\\\") {
        (char) Integer.parseInt(it[1], 16)
    }
}

reportTextItems = []
gatherTextContent(get(ContentSequence), reportTextItems)

set("OBX-1", "1")
set("OBX-2", "TX")
textRepetition = 0
reportTextItems.each { reportTextItem ->
    title = reportTextItem[0] == null ? "___" : reportTextItem[0].toUpperCase()
    text = replaceInvalidEscapeSequences(reportTextItem[1])

    set("OBX-5(" + textRepetition++ + ")", title)
    // blank line after title
    set("OBX-5(" + textRepetition++ + ")", "")
    set("OBX-5(" + textRepetition++ + ")", text)
    // 2 blank lines after each section
    set("OBX-5(" + textRepetition++ + ")", "")
    set("OBX-5(" + textRepetition++ + ")", "")
}

set("OBX-11", completionFlag)
set("OBX-14", get(ContentDate) + get(ContentTime))
// TODO: split name and use subcomponents of OBX-16, starting at OBX-16-2
//set("OBX-16", verifyingObserverName)

set("ZDS-1", get(StudyInstanceUID))