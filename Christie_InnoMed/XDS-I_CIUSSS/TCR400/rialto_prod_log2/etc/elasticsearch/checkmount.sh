#!/bin/bash
logger Checking for iSCSI mount
until [ -f /data/iSCSImounted ]; do sleep 5; done
logger iSCSI mount is available
