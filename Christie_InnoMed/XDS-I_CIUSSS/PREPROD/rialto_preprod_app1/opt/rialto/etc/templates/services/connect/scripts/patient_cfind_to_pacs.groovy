/*
 * patient_cfind_to_pacs.groovy
 *
 * This script constructs a DICOM C-FIND that will be used to fetch the local
 * demographics for a patient. By default, only the PatientId is used in the
 * query.
 *
 * To access the DICOM C-FIND object, use the "set()" method. In order to set a 
 * query parameter value in the DICOM C-FIND object set the value to what you 
 * wish to query by.
 * For example, to set the value of PatientId: 
 * 
 * set("PatientId","12345")
 * OR
 * set(PatientId,"12345")
 *
 * In order to request information be returned in the C-FIND response from 
 * the source, set the value to be blank or null.
 * For example, to request StudyDate from the source:
 * 
 * set("StudyDate",null)
 * OR
 * set(StudyDate,"")
 *
 * If an HL7 order is associated with prefetching of the DICOM object we are 
 * retrieving the demographics for, then this can be accessed with the
 * "prefetchorder" variable. If not, the variable will still be available (but 
 * set to null).To retrieve values from the HL7 message, use the "get()" method.
 * For example, to retrieve the patientID from the HL7 message to a variable:
 *
 * if (prefetchorder != null) {
 * 	def pid = prefetchorder.get("PID-3-1")
 * }
 *
 * The variable "originalInput" can be used to access the the DICOM object 
 * header. For example, to get the patientId of the cached DICOM object:
 *
 * def originalPID = originalInput.get(PatientId)
 */

 /*
  * If this workflow was initiated using a prefetch order, we may wish to use
  * the patient details contained within the order itself and not the original
  * image that is cached within the Connect service.
  */
log.info("ilm_policy_hard_delete_only.groovy: Starting Policy Script")

if (prefetchorder != null) {
	def pid = prefetchorder.get("PID-3-1")
	def namespace = prefetchorder.get("PID-3-4-1")
	def universalID = prefetchorder.get("PID-3-4-2")

	/*
	 * If the Issuer of Patient ID details from the HL7 message are not complete,
	 * then we want to set the value to empty strings so that we do not throw
	 * a null pointer exception when accessing the variable.
	 */
	if (namespace == null) {
	    namespace = ""
	}
	if (universalID == null) {
	    universalID = ""
	}

	if (pid != null) {
		set(PatientId,pid)
	}

	set(IssuerOfPatientID, namespace)
	set("IssuerOfPatientIDQualifiersSequence/UniversalEntityID", universalID);
	set("IssuerOfPatientIDQualifiersSequence/UniversalEntityIDType", "ISO");

} else {
	def pid = originalInput.get(PatientId)
	def namespace = originalInput.get(IssuerOfPatientID)
	def universalID = originalInput.get("IssuerOfPatientIDQualifiersSequence/UniversalEntityID")

	/*
	 * If the Issuer of Patient ID details from the original image are not 
	 * complete, then we want to set the value to empty strings so that we do 
	 * not throw a null pointer exception when accessing the variable.
	 */
	if (namespace == null) {
	    namespace = ""
	}
	if (universalID == null) {
	    universalID = ""
	}

	if (pid != null) {
		set(PatientId,pid)
	}

	set(IssuerOfPatientID, namespace)
	set("IssuerOfPatientIDQualifiersSequence/UniversalEntityID", universalID);
	set("IssuerOfPatientIDQualifiersSequence/UniversalEntityIDType", "ISO");
}