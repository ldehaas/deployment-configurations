import com.karos.rtk.common.Pid
import org.joda.time.DateTime
import org.joda.time.DateTimeZone
import com.karos.rtk.common.HL7v2Date

def scriptName = "MWL Order Morpher - ";

log.info("Here's how the order message looks:]\n{}", input)
log.info("And here's how the initial Imaging Service Request looks like\n{}", imagingServiceRequest)

if (imagingServiceRequest.getAccessionNumberUniversalId() == null) {
    imagingServiceRequest.setAccessionNumberNamespaceId(get("PID-3-4-1"))
    imagingServiceRequest.setAccessionNumberUniversalId(get("PID-3-4-2"))
    imagingServiceRequest.setAccessionNumberUniversalIdType(get("PID-3-4-3"))
}

def otherPatientIds = imagingServiceRequest.getPatientIdentification().getOtherPatientIds()
pid2 = new Pid(input.get("PID-2-1"),
               input.get("PID-2-4-1"),
               input.get("PID-2-4-2"),
               input.get("PID-2-4-3"))
otherPatientIds.add(pid2)
imagingServiceRequest.getPatientIdentification().setOtherPatientIds(otherPatientIds)

def patientDemographics = imagingServiceRequest.getPatientDemographics()
imagingServiceRequest.setPatientDemographics(patientDemographics)

def requestedProcedure = imagingServiceRequest.getRequestedProcedureSequence().get(0)

def scheduledProcedureStep = requestedProcedure.getScheduledProcedureStepSequence().get(0)
scheduledProcedureStep.setScheduledProcedureStepIDString(requestedProcedure.getRequestedProcedureID())

if (get("OBR-27-4") != null){
   scheduledProcedureStep.setScheduledProcedureStepStartDateTime(HL7v2Date.parse(get("OBR-27-4").toString(), DateTimeZone.getDefault()))
} else {
   log.warn(scriptName + "OBR-27-4 is null. Unknown scheduled date time.")
}

def scheduledProcedureStepStatus = scheduledProcedureStep.getScheduledProcedureStepStatus()

def orderControl = get("ORC-1")
def orderStatus = get("ORC-5")
if (orderStatus != null) {

    log.debug(scriptName + "ORC-1 segment has Order Control = {}", orderControl)
    log.debug(scriptName + "ORC-5 segment has Order Status = {}", orderStatus)

    switch (orderStatus) {
        case 'CM':
            log.debug(scriptName + "about to set Scheduled Procedure Step Status to COMPLETED")
            scheduledProcedureStep.setScheduledProcedureStepStatus('COMPLETED')
            break
        case 'CA':
            log.debug(scriptName + "about to set Scheduled Procedure Step Status to DISCONTINUED")
            scheduledProcedureStep.setScheduledProcedureStepStatus('DISCONTINUED')
            break
        default:
            if (orderControl == "XO" && orderStatus == "SC") {
            log.debug(scriptName + "about to set Scheduled Procedure Step Status to INPROGRESS")
            scheduledProcedureStep.setScheduledProcedureStepStatus('INPROGRESS')
            } else {
            log.debug(scriptName + "about to set Scheduled Procedure Step Status to SCHEDULED")
            scheduledProcedureStep.setScheduledProcedureStepStatus('SCHEDULED')
            }
            break
    }

} else {
    log.debug( scriptName + "ORC-5 segment is not set...");
    }

log.info("Finished fixing up the Imaging Service Request...Here it is\n{}", imagingServiceRequest)
