// This file is used to modify incoming messages

def messageType = get('MSH-9-1')
def triggerEvent = get('MSH-9-2')

if (messageType.equalsIgnoreCase("ADT")) {
	def ramq = get('PID-2-1')

	// The EMPI service only checks PID-3 for patient identifiers, so copy the RAMQ into a PID-3 repitition.

	set('PID-3(1)-1', ramq)

	// RAMQ Domain is static: NAM&2.16.124.10.101.1.60.100&ISO
	set('PID-3(1)-4-1', 'NAM')
	set('PID-3(1)-4-2', '2.16.124.10.101.1.60.100')
	set('PID-3(1)-4-3', 'ISO')

	log.debug("Modified HL7 {}", output)
}
