import com.karos.rtk.common.HL7v2Date;
import org.joda.time.DateTimeZone;

log.info("Here's how the order message looks:\n{}", input)
log.info("And here's how the initial Imaging Service Request looks like\n{}", imagingServiceRequest)

def patientIdentification = imagingServiceRequest.getPatientIdentification()

if (imagingServiceRequest.getAccessionNumberUniversalId() == null) {
    imagingServiceRequest.setAccessionNumberNamespaceId(patientIdentification.getPatientIdNamespaceId())
    imagingServiceRequest.setAccessionNumberUniversalId(patientIdentification.getPatientIdUniversalId())
    imagingServiceRequest.setAccessionNumberUniversalIdType("ISO")
}

def requestedProcedure = imagingServiceRequest.getRequestedProcedureSequence().get(0)
requestedProcedure.setStudyInstanceUID(get("ZDS-1"))

def scheduledProcedureStep = requestedProcedure.getScheduledProcedureStepSequence().get(0)
scheduledProcedureStep.setScheduledProcedureStepLocation(get("OBR-2"))
scheduledProcedureStep.setScheduledStationName(get("OBR-4"))

def scheduledDate = get("OBR-27-4")

scheduledProcedureStep.setScheduledProcedureStepStartDateTime(HL7v2Date.parse(scheduledDate, DateTimeZone.getDefault()))

scheduledProcedureStep.setScheduledProcedureStepIDString(requestedProcedure.getRequestedProcedureID())

if (scheduledProcedureStep.getScheduledProcedureStepLocation() == null) {
    scheduledProcedureStep.setScheduledProcedureStepLocation(get("MSH-4"));
}

if (get("ORC-1") == "CA") {
  scheduledProcedureStep.setScheduledProcedureStepStatus("DISCONTINUED")
} else {
  scheduledProcedureStep.setScheduledProcedureStepStatus("SCHEDULED")
}

scheduledProcedureStep.setModality(get("OBR-24"))

log.info("Finished fixing up the Imaging Service Request...Here it is\n{}", imagingServiceRequest)

