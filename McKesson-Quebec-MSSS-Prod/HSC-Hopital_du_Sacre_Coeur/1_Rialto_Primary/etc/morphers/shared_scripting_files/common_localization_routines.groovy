
/**
 * Encapsulates a dicom object to provide easy access to
 * the patient identifiers.
 */
class Pids {
    /**
     * Searches through the supplied pids and finds the one in the local domain.
     * If no local pid is found, a new one is created by prefixing the source
     * patient id with the source issuer.
     * <p>
     * TODO: use an instance of the class instead of this static method
     * 
     * @return a two element array containing the localized pid and issuer
     * @param qualifiedSourcePid a two element array containing the qualified source
     *      pid and source issuer
     * @param qualifiedOtherPids an array of qualified pids
     */
    static localize(qualifiedSourcePid, qualifiedOtherPids, localIssuer) {
        def sourcePid = qualifiedSourcePid[0]
        def sourceIssuer = qualifiedSourcePid[1]
    
        if (localIssuer == sourceIssuer) {
            // this image originated from the local PACS
            return qualifiedSourcePid
        }
    
        for (def pid : qualifiedOtherPids) {
            if (localIssuer == pid[1]) {
                // finds the local PID if available
                return pid
            }
        }
    
        // no linked pid found, create one by prefixing
        return [sourceIssuer+"_"+sourcePid, localIssuer]
    }

    private issuerToPid = [:]
    private sourceDomain
    private sop

    public Pids(sop, order=null) {
        this.sop = sop

        sourceDomain = sop.get(IssuerOfPatientID)
        issuerToPid[sourceDomain] = sop.get(PatientID)

        sop.get(OtherPatientIDsSequence).each {
            issuerToPid[it.get(IssuerOfPatientID)] = it.get(PatientID)
        }

        if (order != null) {
            SharedHl7.PIDConfiguration.each { pidspec ->
                try {
                    // assumes order has already been normalized:
                    def domain = order.get(pidspec.field + "-4-2")
                    def pid = order.get(pidspec.field)
                    if (domain != null && pid != null) {
                        issuerToPid[domain] = pid
                    }
                } catch (Exception e) {
                    // eg, looking for a MRG segment in a non-merge message
                    // nothing to worry about
                }
            }
        }
    }

    /**
     * Find a patient identifier in the local domain.
     */
    public getLocalPid(localDomain) {
        def localPid = issuerToPid[localDomain]
        if (localPid != null) {
            return [localPid, issuer]
        }

        // no linked pid found, create one by prefixing
        return [sourceDomain + "_" + issuerToPid[sourceDomain], localIssuer]
    }

    public getPid(domain) {
        issuerToPid[domain]
    }

    /**
     * Return the source domain, or null if it is not known.
     */
    public getSourceDomain() {
        return sourceDomain
    }
}

class Prefixes {

    static getPrefix(assigningAuthority, localIssuer) {
        if (localIssuer == assigningAuthority) {
            // Study belongs to the local PACS.  No prefix needed
            return ""

        } else {
            return sourceIssuerToPrefixAndInstitutionName[assigningAuthority][0]
        }
    }

    static getInstitutionName(assigningAuthority) {
        return sourceIssuerToPrefixAndInstitutionName[assigningAuthority][1]
    }

    private static final sourceIssuerToPrefixAndInstitutionName = [
        "2.16.124.10.101.1.60.1.3000.102.10.1045.1" : ["AD", "Centre Comtois"],
        "2.16.124.10.101.1.60.1.3000.102.10.1060.0" : ["AE", "CRSSS de la Baie-James"],
        "2.16.124.10.101.1.60.1.3000.102.10.1061.1" : ["BC", "CSSS d'Argenteuil"],
        "2.16.124.10.101.1.60.1.3000.101.10.1030.1" : ["BD", "CSSS de la Basse-Côte-Nord"],
        "2.16.124.10.101.1.60.1.3000.101.10.1028.1" : ["BE", "CSSS de la Minganie"],
        "2.16.124.10.101.1.60.1.3000.101.10.1003.1" : ["BF", "CSSS de la Mitis"],
        "2.16.124.10.101.1.60.1.3000.103.10.1005.1" : ["BG", "CSSS de la MRC-de-Coaticook"],
        "2.16.124.10.101.1.60.1.3000.101.10.1029.1" : ["BH", "CSSS de l'Hématite"],
        "2.16.124.10.101.1.60.1.3000.103.10.1003.1" : ["BJ", "CSSS de Memphrémagog"],
        "2.16.124.10.101.1.60.1.3000.101.10.1027.1" : ["BK", "CSSS de Port-Cartier"],
        "2.16.124.10.101.1.60.1.3000.103.10.1006.1" : ["BL", "CSSS du Granit"],
        "2.16.124.10.101.1.60.1.3000.102.10.1043.1" : ["BM", "CSSS du Haut-Saint-Maurice"],
        "2.16.124.10.101.1.60.1.3000.102.10.1069.0" : ["BN", "Centre de santé Inuulitsivik"],
        "2.16.124.10.101.1.60.1.3000.102.10.1070.0" : ["BT", "Centre de santé Tulattavik de l'Ungava"],
        "2.16.124.10.101.1.60.1.1016.1" :             ["BW", "Centre de soins de courte durée La Sarre"],
        "2.16.124.10.101.1.60.1.3000.103.10.1007.1" : ["BX", "CLSC - Centre d'hébergement de Windsor"],
        "2.16.124.10.101.1.60.1.3000.102.10.1044.2" : ["BZ", "Centre multi. en santé et en services sociaux Christ-Roi"],
        "2.16.124.10.101.1.60.1.3000.102.10.1041.2" : ["CB", "Centre d'hébergement Laflèche"],
        "2.16.124.10.101.1.60.1.1036.1" :             ["CC", "Centre d'hébergement Notre-Dame-de-la-Merci"],
        "2.16.124.10.101.1.60.1.3000.101.10.1040.1" : ["CD", "Hôtel-Dieu de Lévis"],
        "2.16.124.10.101.1.60.1.3000.102.10.1065.1" : ["CE", "Centre hospitalier Anna-Laberge"],
        "2.16.124.10.101.1.60.1.1000.1" :             ["CF", "Centre hospitalier de St. Mary"],
        "2.16.124.10.101.1.60.1.3000.101.10.1001.1" : ["CJ", "Centre hospitalier régional du Grand-Portage"],
        "2.16.124.10.101.1.60.1.3000.101.10.1006.1" : ["CK", "Centre hospitalier Trois-Pistoles"],
        "2.16.124.10.101.1.60.1.3000.103.10.1001.0" : ["CQ", "Centre hospitalier universitaire de Sherbrooke"],
        "2.16.124.10.101.1.60.1.1004.1" :             ["CR", "CHU Sainte-Justine"],
        "2.16.124.10.101.1.60.1.3000.102.10.1055.0" : ["CS", "CHUM"],
        "2.16.124.10.101.1.60.1.3000.102.10.1044.1" : ["CT", "Centre multi. en santé et en services sociaux de Fortierville"],
        "2.16.124.10.101.1.60.1.1027.1" :             ["CU", "CLSC de Pointe-aux-Trembles-Montréal-Est"],
        "2.16.124.10.101.1.60.1.1012.1" :             ["CV", "CLSC de Senneterre"],
        "2.16.124.10.101.1.60.1.3000.101.10.1022.1" : ["CW", "CLSC St-Marc-des-Carrières"],
        "2.16.124.10.101.1.60.1.1019.1" :             ["CX", "CLSC et centre d'hébergement la Petite-Nation"],
        "2.16.124.10.101.1.60.1.1021.1" :             ["CY", "CLSC de Fort-Coulonge"],
        "2.16.124.10.101.1.60.1.3000.101.10.1041.0" : ["CZ", "CLSC Naskapi"],
        "2.16.124.10.101.1.60.1.3000.102.10.1042.1" : ["DD", "CLSC Sainte-Geneviève-de-Batiscan"],
        "2.16.124.10.101.1.60.1.3000.102.10.1042.2" : ["DF", "CLSC Saint-Tite"],
        "2.16.124.10.101.1.60.1.1037.1" :             ["DG", "CLSC de Saint-Léonard et Saint-Michel"],
        "2.16.124.10.101.1.60.1.3000.102.10.1046.1" : ["DJ", "CLSC, centre d'hébergement et hôpital Cloutier-du-Rivage"],
        "2.16.124.10.101.1.60.1.3000.101.10.1021.1" : ["DK", "CLSC, hôpital et centre d'hébergement Christ-Roi"],
        "2.16.124.10.101.1.60.1.3000.102.10.1071.0" : ["DL", "Conseil Cri de la Santé et des Services sociaux de la Baie-James"],
        "2.16.124.10.101.1.60.1.3000.102.10.1052.0" : ["DS", "Hôpital de Mont-Laurier / Centre de services de Rivière-Rouge"],
        "2.16.124.10.101.1.60.1.3000.102.10.1047.0" : ["FJ", "Centre hospitalier régional de Trois-Rivières"],
        "2.16.124.10.101.1.60.1.3000.102.10.1050.0" : ["FW", "CSSS du Nord de Lanaudière"],
        "2.16.124.10.101.1.60.1.3000.102.10.1051.0" : ["FY", "Hôpital Pierre-Le Gardeur"],
        "2.16.124.10.101.1.60.1.3000.102.10.1057.0" : ["GK", "CUSM"],
        "2.16.124.10.101.1.60.1.3000.102.10.1068.1" : ["GQ", "Hôpital Barrie Memorial"],
        "2.16.124.10.101.1.60.1.3000.103.10.1012.1" : ["GR", "Hôpital Brome-Missisquoi-Perkins"],
        "2.16.124.10.101.1.60.1.3000.102.10.1054.1" : ["GS", "Hôpital Charles LeMoyne"],
        "2.16.124.10.101.1.60.1.3000.101.10.1020.2" : ["GT", "Hôpital Chauveau"],
        "2.16.124.10.101.1.60.1.3000.102.10.1048.1" : ["GU", "CSSS Laval"],
        "2.16.124.10.101.1.60.1.3000.101.10.1010.1" : ["GV", "Hôpital d'Alma"],
        "2.16.124.10.101.1.60.1.1023.1" :             ["GW", "Hôpital d'Amos"],
        "2.16.124.10.101.1.60.1.3000.101.10.1008.1" : ["GX", "Hôpital d'Amqui"],
        "2.16.124.10.101.1.60.1.3000.101.10.1019.1" : ["GY", "Hôpital de Baie-Saint-Paul"],
        "2.16.124.10.101.1.60.1.3000.101.10.1036.1" : ["GZ", "Hôpital de Chandler"],
        "2.16.124.10.101.1.60.1.3000.101.10.1009.1" : ["HA", "Hôpital de Chicoutimi"],
        "2.16.124.10.101.1.60.1.3000.101.10.1012.1" : ["HB", "Hôpital de Dolbeau-Mistassini"],
        "2.16.124.10.101.1.60.1.1008.1" :             ["HC", "Hôpital de Gatineau"],
        "2.16.124.10.101.1.60.1.3000.103.10.1010.1" : ["HD", "Hôpital de Granby"],
        "2.16.124.10.101.1.60.1.1009.1" :             ["HE", "Hôpital de Hull"],
        "2.16.124.10.101.1.60.1.3000.101.10.1014.1" : ["HF", "Hôpital de la Baie"],
        "2.16.124.10.101.1.60.1.3000.101.10.1019.2" : ["HG", "Hôpital de la Malbaie"],
        "2.16.124.10.101.1.60.1.3000.101.10.1035.1" : ["HH", "Hôpital de l'Archipel"],
        "2.16.124.10.101.1.60.1.3000.102.10.1059.1" : ["HJ", "Hôpital de LaSalle"],
        "2.16.124.10.101.1.60.1.3000.101.10.1016.1" : ["HK", "Hôpital de l'Enfant-Jésus"],
        "2.16.124.10.101.1.60.1.1010.1" :             ["HL", "Hôpital de Maniwaki"],
        "2.16.124.10.101.1.60.1.3000.101.10.1034.1" : ["HM", "Hôpital de Maria"],
        "2.16.124.10.101.1.60.1.3000.101.10.1004.1" : ["HN", "Hôpital de Matane"],
        "2.16.124.10.101.1.60.1.3000.101.10.1039.1" : ["HQ", "Hôpital de Montmagny"],
        "2.16.124.10.101.1.60.1.3000.101.10.1005.1" : ["HR", "Hôpital de Notre-Dame-du-Lac / CLSC Pohénégamook"],
        "2.16.124.10.101.1.60.1.1020.1" :             ["HS", "Hôpital de Papineau"],
        "2.16.124.10.101.1.60.1.1015.1" :             ["HT", "CLSC et hôpital de Rouyn-Noranda"],
        "2.16.124.10.101.1.60.1.3000.101.10.1020.1" : ["HU", "Hôpital de Sainte-Anne-de-Beaupré"],
        "2.16.124.10.101.1.60.1.3000.101.10.1033.1" : ["HV", "Hôpital de Sainte-Anne-des-Monts"],
        "2.16.124.10.101.1.60.1.3000.102.10.1063.1" : ["HW", "Hôpital de Saint-Eustache"],
        "2.16.124.10.101.1.60.1.3000.101.10.1037.1" : ["HX", "Hôpital de St-Georges/CLSC, centre d’hébergement de Lac-Etchemin"],
        "2.16.124.10.101.1.60.1.3000.101.10.1038.1" : ["HY", "Hôpital de Thetford Mines"],
        "2.16.124.10.101.1.60.1.1006.1" :             ["HZ", "Hôpital de Verdun"],
        "2.16.124.10.101.1.60.1.3000.102.10.1074.1" : ["JA", "Hôpital Douglas"],
        "2.16.124.10.101.1.60.1.3000.102.10.1041.1" : ["JB", "Hôpital du Centre-de-la-Mauricie"],
        "2.16.124.10.101.1.60.1.3000.103.10.1013.1" : ["JC", "Hôpital du Haut-Richelieu"],
        "2.16.124.10.101.1.60.1.1022.1" :             ["JD", "Hôpital du Pontiac"],
        "2.16.124.10.101.1.60.1.1001.1" :             ["JE", "Hôpital du Sacré-Cœur de Montréal"],
        "2.16.124.10.101.1.60.1.3000.101.10.1016.2" : ["JF", "Hôpital du Saint-Sacrement"],
        "2.16.124.10.101.1.60.1.3000.102.10.1066.1" : ["JG", "Hôpital du Suroît"],
        "2.16.124.10.101.1.60.1.3000.101.10.1013.1" : ["JH", "Hôpital et centre de réadaptation de Jonquière"],
        "2.16.124.10.101.1.60.1.3000.101.10.1031.1" : ["JJ", "Hôpital et centre d'hébergement de Sept-Îles"],
        "2.16.124.10.101.1.60.1.3000.103.10.1002.1" : ["JK", "Institut universitaire de gériatrie de Sherbrooke"],
        "2.16.124.10.101.1.60.1.1013.1" :             ["JL", "Hôpital et CLSC de Val-d'Or"],
        "2.16.124.10.101.1.60.1.3000.102.10.1058.1" : ["JN", "Hôpital Fleury"],
        "2.16.124.10.101.1.60.1.1007.1" :             ["JQ", "Hôpital général du Lakeshore"],
        "2.16.124.10.101.1.60.1.1002.1" :             ["JR", "Hôpital général juif"],
        "2.16.124.10.101.1.60.1.3000.103.10.1011.1" : ["JS", "Hôpital Honoré-Mercier"],
        "2.16.124.10.101.1.60.1.3000.101.10.1032.1" : ["JT", "Hôpital Hôtel-Dieu de Gaspé"],
        "2.16.124.10.101.1.60.1.1005.1" :             ["JU", "Hôpital Jean-Talon"],
        "2.16.124.10.101.1.60.1.3000.101.10.1023.1" : ["JV", "Hôpital Jeffery Hale"],
        "2.16.124.10.101.1.60.1.3000.102.10.1049.1" : ["JY", "Hôpital juif de réadaptation"],
        "2.16.124.10.101.1.60.1.3000.102.10.1062.1" : ["KA", "Hôpital Laurentien"],
        "2.16.124.10.101.1.60.1.3000.101.10.1025.1" : ["KB", "Hôpital Le Royer"],
        "2.16.124.10.101.1.60.1.1030.1" :             ["KC", "Institut universitaire en santé mentale de Montréal"],
        "2.16.124.10.101.1.60.1.1017.1" :             ["KG", "Hôpital mémorial de Wakefield"],
        "2.16.124.10.101.1.60.1.1026.1" :             ["KH", "Hôpital Mont-Sinaï"],
        "2.16.124.10.101.1.60.1.3000.101.10.1002.1" : ["KL", "Hôpital Notre-Dame-de-Fatima"],
        "2.16.124.10.101.1.60.1.3000.102.10.1053.1" : ["KM", "Hôpital Pierre-Boucher"],
        "2.16.124.10.101.1.60.1.3000.101.10.1022.2" : ["KQ", "Hôpital régional de Portneuf/CLSC de Saint-Raymond"],
        "2.16.124.10.101.1.60.1.3000.101.10.1007.1" : ["KR", "Hôpital régional de Rimouski"],
        "2.16.124.10.101.1.60.1.3000.102.10.1064.1" : ["KS", "Hôpital régional de Saint-Jérôme"],
        "2.16.124.10.101.1.60.1.1031.1" :             ["KT", "Hôpital Rivière-des-Prairies"],
        "2.16.124.10.101.1.60.1.1003.1" :             ["KV", "Hôpital Santa Cabrini"],
        "2.16.124.10.101.1.60.1.3000.103.10.1009.1" : ["KZ", "Hôpital Sainte-Croix"],
        "2.16.124.10.101.1.60.1.3000.103.10.1004.1" : ["LB", "Hôpital, CLSC et centre d'hébergement d'Asbestos"],
        "2.16.124.10.101.1.60.1.3000.101.10.1011.1" : ["LC", "Hôpital, CLSC et centre d'hébergement de Roberval"],
        "2.16.124.10.101.1.60.1.1035.1" :             ["LD", "Hôpital Catherine-Booth"],
        "2.16.124.10.101.1.60.1.3000.103.10.1008.1" : ["LE", "Hôtel-Dieu-d'Arthabaska"],
        "2.16.124.10.101.1.60.1.3000.102.10.1067.1" : ["LG", "Hôtel-Dieu de Sorel"],
        "2.16.124.10.101.1.60.1.3000.102.10.1078.1" : ["LK", "Institut de cardiologie de Montréal"],
        "2.16.124.10.101.1.60.1.1032.1" :             ["LL", "Institut de réadaptation de Montréal"],
        "2.16.124.10.101.1.60.1.3000.101.10.1017.0" : ["LM", "Institut de réadaptation en déficience physique de Québec"],
        "2.16.124.10.101.1.60.1.1033.1" :             ["LP", "Institut Philippe-Pinel de Montréal"],
        "2.16.124.10.101.1.60.1.3000.101.10.1024.0" : ["LR", "IUCPQ"],
        "2.16.124.10.101.1.60.1.3000.101.10.1018.0" : ["LU", "Institut universitaire en santé mentale de Québec"],
        "2.16.124.10.101.1.60.1.1028.1" :             ["LX", "L'hôpital chinois de Montréal (1963)"],
        "2.16.124.10.101.1.60.1.3000.102.10.1056.1" : ["LZ", "Hôpital Maisonneuve-Rosemont"],
        "2.16.124.10.101.1.60.1.3000.101.10.1015.3" : ["MA", "Pavillon centre hospitalier de l'Université Laval"],
        "2.16.124.10.101.1.60.1.1034.1" :             ["MB", "Institut universitaire de gériatrie de Montréal"],
        "2.16.124.10.101.1.60.1.3000.101.10.1026.1" : ["MC", "CSSS de la Haute-Côte-Nord (Pavillon Escoumins)"],
        "2.16.124.10.101.1.60.1.3000.101.10.1026.2" : ["MD", "CSSS de la Haute-Côte-Nord (Pavillon Forestville)"],
        "2.16.124.10.101.1.60.1.3000.101.10.1015.2" : ["ME", "Pavillon l'Hôtel-Dieu de Québec"],
        "2.16.124.10.101.1.60.1.1018.1" :             ["MF", "Pavillon Sainte-Famille"],
        "2.16.124.10.101.1.60.1.3000.101.10.1015.1" : ["MH", "Pavillon Saint-François d'Assise"],
        "2.16.124.10.101.1.60.1.1024.1" :             ["MK", "Point de service de Témiscaming-et-de-Kipawa"],
        "2.16.124.10.101.1.60.1.3000.201.1000.0" :    ["OA", "Imagix"],
        "2.16.124.10.101.1.60.1.3000.201.1003.0" :    ["OB", "Radiologix"],
        "2.16.124.10.101.1.60.1.3000.201.1006.0" :    ["OC", "Radiologie Varad"],
        "2.16.124.10.101.1.60.1.3000.201.1014.0" :    ["OD", "Clinique radiologique du Haut-Richelieu"],
        "2.16.124.10.101.1.60.1.3000.201.1015.0" :    ["OF", "Radiologie PB Longueuil (chemin Chambly)"],
        "2.16.124.10.101.1.60.1.3000.201.1016.0" :    ["SF", "Clinique radiologique Audet"],
        "2.16.124.10.101.1.60.1.3000.201.1017.0" :    ["SK", "Clinique de radiologie du Saguenay"],
        "2.16.124.10.101.1.60.1.3000.201.1010.0" :    ["OG", "Radiologie St-Martin & Bois-de-Boulogne"],
        "2.16.124.10.101.1.60.1.3000.201.1011.0" :    ["PA", "RésoScan CLM"],
        "2.16.124.10.101.1.60.1.3000.201.1002.0" :    ["PE", "Centre Radiologique Sherbrooke"],
        "2.16.124.10.101.1.60.1.3000.201.1004.0" :    ["PF", "Centre d'imagerie médicale Clarke"],
        "2.16.124.10.101.1.60.1.3000.201.1012.0" :    ["PG", "Radimed"],
        "2.16.124.10.101.1.60.1.3000.201.1007.0" :    ["PH", "Imagerie des Pionniers"],
        "2.16.124.10.101.1.60.1.3000.201.1005.0" :    ["SA", "Clinique Léger"],
        "2.16.124.10.101.1.60.1.3000.201.1009.0" :    ["SB", "Services radiologiques de Joliette Inc."],
        "2.16.124.10.101.1.60.1.3000.201.1008.0" :    ["SC", "Imagerie Terrebonne"],
        "2.16.124.10.101.1.60.1.3000.201.1013.0" :    ["SD", "SORAD - Clinique Maisonneuve"],
        "2.16.124.10.101.1.60.1.3000.201.1001.0" :    ["SG", "Radiologie et Imagerie Medicale de la Capitale"],

        // won't be able to determine the institution for a blank Assigning Authority
        "" : ["ZZ", "Institution Inconnue"],

        // Same with null Assigning Authorities
        null : ["ZZ", "Institution Inconnue"]

    ].withDefault{["AU", "Autre Institution Inconnue"]}

} // end of Prefixes class


class Demographics {

    /** @return an array with [0]=family name; [1]=given name  */
    public static parseName(String name) {

        // assume "family, given middle etc"
        if (name.contains(",")) {
            // get rid of extra splitters left in by some dicom systems:
            // returns middle name as part of given name
            name = name.replaceAll("\\^", "")

            // only split on first comma:
            return name.split(",", 2).collect{it.trim()}
       }

        // assume dicom standard "family^given^middle^etc"
        if (name.contains("^")) {
            // split on all ^s but only take the first two
            // doesn't return middle name, should we?
            return name.split("\\^")[0..1]
        }

        // assume "given family" where family might be multiple parts
        // too difficult to tell difference between middle names and
        // multi-part last names
        return name.split("\\s+", 2).reverse()
    }
}

class UIDs {
    private static localizeUID(sop, context, log, hash, tag) {
        def uids = sop.getList(tag)
        log.debug("{} is {}", tag, uids)

        // only set if there is at least one, in case we're touching an
        // object with no ids (eg, a cfind request might have no
        // series or sop instance uids)
        if (!uids.isEmpty()) {
            uids = uids.collect { context.localizeUID(it, hash) }
            sop.set(tag, uids, null)
            log.debug("Setting hashed {} to {}", tag, uids)
        }

        if (uids.isEmpty()) {
            // wasn't present, we effectively leave it null
            return null
        } else if (uids.size() == 1) {
            // common case of only one uid, they only want one
            // back...
            return uids[0]
        } else {
            // if we localized multiple uids, make sure they
            // get them all
            return uids
        }
    }

    /**
     * Localize study, series and sop instances uids.
     * Supports lists of uids as well.
     *
     * @param sop the dicom object with the uids
     * @param context the current context from contexts.groovy
     * @param hash the hash closure provided by connect
     * @param log the logger
     * @return a list of the new study, series and sop uids in that order.
     *         If any had multiple values, a nested list is returned.
     */
    public static localizeUIDs(sop, context, hash, log) {
        return [
            localizeUID(sop, context, log, hash, 'StudyInstanceUID'),
            localizeUID(sop, context, log, hash, 'SeriesInstanceUID'),
            localizeUID(sop, context, log, hash, 'SOPInstanceUID')
        ]
    }
}

