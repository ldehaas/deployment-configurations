14:38:58 INFO  com.karos.rtk.audit.AuditConfiguration - Audit logging disabled
Found 1 document:
14:39:00 INFO  com.karos.rtk.cmdline.xds.storedquery.GetDocuments - 
Logical Document: urn:uuid:7bc0f98d-1618-4b57-a5a0-d0577cfeaa88
   Patient ID: 1027338753^^^&2.16.840.1.113883.4.56&ISO
   Title: IRM PIED DROIT SANS INFUSION
   Status: Deprecated
   UID: 2.25.187386950831370685676229617981691765129
   Repository UID: 2.16.124.10.101.1.60.2.80
   Entry UID: urn:uuid:7bc0f98d-1618-4b57-a5a0-d0577cfeaa88
   Logical ID: urn:uuid:7bc0f98d-1618-4b57-a5a0-d0577cfeaa88
   MimeType: application/dicom
   Hash: 56697ae7d7366a0b9c21ecc84f55fe6d01573db2
   Size: 93566
   Version: 1
   Creation time: 2017-02-26T18:15:00.000Z
   Service Start Time: 2017-02-26T18:15:00.000Z
   Class Code: Code display=examen imagerie, codeValue=examen imagerie, schemeID=null, schemeName=Imagerie Québec-DSQ
   Format Code: Code display=1.2.840.10008.5.1.4.1.1.88.59, codeValue=urn:ihe:rad:1.2.840.10008.5.1.4.1.1.88.59, schemeID=null, schemeName=1.2.840.10008.2.6.1
   Event Codes: [Code display=null, codeValue=MR, schemeID=null, schemeName=DCM, Code display=null, codeValue=SR, schemeID=null, schemeName=DCM, Code display=null, codeValue=EI, schemeID=null, schemeName=Imagerie Québec-DSQ]
   Healthcare Facility: Code display=healthcareFacilityTypeCodeDisplayName, codeValue=hopital, schemeID=null, schemeName=Imagerie Québec-DSQ
   Confidentiality Code: []
   Type Code: Code display=IRM PIED DROIT SANS INFUSION, codeValue=8575PID, schemeID=null, schemeName=Imagerie Québec-DSQ
   Language Code: fr-CA
   Source Patient ID: 29702^^^&2.16.124.10.101.1.60.1.1023.1&ISO
   Extended Metadata:
     accessionNumberList: [20170004135602]
     aeTitle: [RID-2-0-PROD]
     submissionTS: [20170301123602]
   


