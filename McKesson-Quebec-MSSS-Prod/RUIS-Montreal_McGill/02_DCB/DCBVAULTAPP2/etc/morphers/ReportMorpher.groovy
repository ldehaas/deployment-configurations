// Add the default Character set that can handle French Characters, which is often not explicit in the RIS Feed
def characterEncoding = get("MSH-18")
if (characterEncoding == null || characterEncoding.isEmpty()){
    log.trace("No character set specified.  Hard-coding to 8859/1")
    set('MSH-18', '8859/1')
}

def PID21 = get('PID-2-1')
def PID241 = get('PID-2-4-1')
def PID242 = get('PID-2-4-2')
def PID243 = get('PID-2-4-3')
def PID25 = get('PID-2-5')

set('PID-2-1', get('PID-4-1'))
set('PID-2-4-1', get('PID-4-4-1'))
set('PID-2-4-2', get('PID-4-4-2'))
set('PID-2-4-3', get('PID-4-4-3'))
set('PID-2-5', get('PID-4-5'))

set('PID-4-1', PID21)
set('PID-4-4-1', PID241)
set('PID-4-4-2', PID242)
set('PID-4-4-3', PID243)
set('PID-4-5', PID25)
