#!/bin/bash

# Configuration parameters for chkconfig(8):
# chkconfig: 5 99 01
# description: Rialto - a product of Karos Health

#
# Start and stop Rialto
#
# To enable automatic start and stop at boot and shutdown enter
# the command "chkconfig <app_instance_name> reset"
# e.g. chkconfig rialto reset
#
# To disable automatic start and stop at boot and shutdown enter
# the command "chkconfig --del <app_instance_name>"
# e.g. chkconfig --del rialto
#
###############################################################
#
# Written by John Lammers January 2010.
# Updated February 2011
# Copyright (c) 2010-2013 Karos Health Inc., Waterloo, Ontario, Canada
#

# Choose Java maximum heap size
chooseHeapSize() {

    # Determine how much memory is available (in MB)
    local -i reserveMemory=384 # Reserve for OS
    if [ -f /etc/init.d/cassandra ]; then
        let reserveMemory+=3800 # Reserve for Cassandra
    fi

    local -i systemMemory=$(free -m | awk '/Mem:/ {print $2}')
    local -i availableMemory=$((systemMemory - reserveMemory))

    # Choose heap size (in MB)
    local -i MAX_JVM_HEAP=8192

    if [[ $availableMemory -lt 1024 ]]; then
        HeapSize=1024
    elif [[ $availableMemory -lt $MAX_JVM_HEAP ]]; then
        HeapSize=$availableMemory
    else
        HeapSize=$MAX_JVM_HEAP
    fi
}

APP_NAME=Rialto
APP_INSTANCE_NAME=rialto # e.g. rialto, rialto2, rialto3, ...
APP_USER=rialto
USER_HOME=/home/$APP_USER

APP_HOME=$USER_HOME/$APP_INSTANCE_NAME
APP_JAR=$APP_HOME/lib/jar/jruby-complete-1.7.8.jar
APP_RUBY_SCRIPT=$APP_HOME/lib/rubybin/runrialto.rb
APP_TEMP_DIR=$APP_HOME/var/tmp

NEWRELIC_AGENT_JAR=$APP_HOME/lib/jar/newrelic-agent-3.29.0.jar

LOCK_FILE=/var/lock/subsys/$APP_INSTANCE_NAME
LOG_DIR=$APP_HOME/var/log
LOG_FILE=$LOG_DIR/start_stop.log

# Location of PID file is also known to the install-rialto script
PID_FILE=/var/run/$APP_INSTANCE_NAME/$APP_INSTANCE_NAME.pid
export RIALTO_HOME=$APP_HOME # Needed by Rialto JRuby script

# Get definitions for JAVA and JAVA_HOME
. /etc/profile.d/rialto.sh

# Automatically increase Java maximum heap size according to size
# of system memory.
declare -i HeapSize
chooseHeapSize

JAVA_OPTS="
-Xms512M
-Xmx${HeapSize}M
-XX:MaxPermSize=512M
-XX:PermSize=512M
-XX:+HeapDumpOnOutOfMemoryError
-Dfile.encoding=UTF-8
-Djava.net.preferIPv4Stack=true
-Djava.io.tmpdir=$APP_TEMP_DIR
-Dnewrelic.config.file=$APP_HOME/etc/newrelic.yml
-javaagent:$NEWRELIC_AGENT_JAR
-server
"
# to debug ssl handshakesa, add the following to JAVA_OPTS
# -Djavax.net.debug=all

# to monitor rialto via jvisualvm, add the following to JAVA_OPTS
# -Dcom.sun.management.jmxremote

# to remotely attach a debugger, add the following to JAVA_OPTS
# -Xdebug -Xrunjdwp:transport=dt_socket,server=y,suspend=n,address=1044
# and then in eclipse, debug configurations, add a remote java application
# to this host on port 1044

IP_ADDR_ETH0=$(ifconfig eth0 2>/dev/null | awk -F '[: ]+' '/inet addr/{print $4}')
IP_ADDR_ETH1=$(ifconfig eth1 2>/dev/null | awk -F '[: ]+' '/inet addr/{print $4}')



log() {
    echo `date` "[$APP_INSTANCE_NAME]" "$*" >>$LOG_FILE
}

isAliveByPid() {
    if [ ! -f $PID_FILE ]; then
        return 1 # not running
    fi
    pid=`cat $PID_FILE`
    if [ -d "/proc/$pid" ]; then
        return 0 # running
    else
        # The pid file is removed only when it is stale
        rm -f $PID_FILE
        return 1 # not running
    fi
}

quitIfNotRoot()
{
    if [ $UID -ne 0 ]; then
        echo 'Sorry, must be root'
        exit 1
    fi
}

NODETOOL='/opt/cassandra/bin/nodetool'
cassandraReady() {

    local output=$($NODETOOL -h localhost status 2>/dev/null)
    if [ $? -ne 0 ]; then
        return 1 # error, not ready
    fi

    if echo "$output" | grep -q '^UN'; then
        return 0 # ready
    else
        return 1 # not ready
    fi
}

doStop() {
    #
    # If application is not running, there's nothing to do.
    #
    log "Request to shut down $APP_NAME"
    rm -f $LOCK_FILE
    if ! isAliveByPid ; then
        log "$APP_NAME was not running"
        return
    fi

    #
    # Send shutdown signal to application.
    #
    log "Signalling $APP_NAME to stop"
    kill `cat $PID_FILE` 2>/dev/null

    #
    # Wait for application to complete shutdown operations.
    #
    local -i seconds=0
    while [ $seconds -lt 300 ]; do
        sleep 1
        let ++seconds
        if isAliveByPid ; then
            log "Waiting for $APP_NAME to stop ... $seconds seconds"
        else
            log "$APP_NAME stopped normally after $seconds seconds"
            return
        fi
    done
    log "$APP_NAME failed to stop after $seconds seconds"

    #
    # Application won't shut down, so kill it.
    #
    log "Terminating $APP_NAME with extreme prejudice"
    kill -9 `cat $PID_FILE` 2>/dev/null
    sleep 3
    if isAliveByPid ; then
        log "ERROR: Unable to kill $APP_NAME"
    fi
}

doStart() {
    #
    # Prevent starting two copies with the same instance name.
    #
    if [ -f $LOCK_FILE ]; then
        if isAliveByPid ; then
            echo "$APP_NAME instance [$APP_INSTANCE_NAME] already running, " \
                "pid=" `cat $PID_FILE`
            log "ERROR: Requested to start $APP_NAME when already running"
            return
        else
            echo "$APP_NAME instance [$APP_INSTANCE_NAME] is starting up" \
                "(no pid yet)"
            log "ERROR: Requested to start $APP_NAME when already starting"
            return
        fi
    fi
    touch $LOCK_FILE

    #
    # If application is already running, there's nothing to do.
    #
    if isAliveByPid ; then
        echo "$APP_NAME instance [$APP_INSTANCE_NAME] already running, " \
            "pid=" `cat $PID_FILE`
        log "ERROR: Requested to start $APP_NAME when already running"
        return
    fi

    #
    # Sanity check.
    #
    if [ ! -r "$APP_JAR" ]; then
        echo "Abort: Cannot access $APP_NAME JAR file $APP_JAR"
        log "ERROR: Cannot access $APP_NAME JAR file $APP_JAR"
        rm -f $LOCK_FILE
        exit 1
    fi

    #
    # Ensure local instance of Cassandra database is ready.
    #
    if [ -f $NODETOOL ]; then
        if ! cassandraReady; then
            echo "Waiting for local Cassandra"
            local -i seconds=0
            local ready=false
            while [ $seconds -lt 300 ]; do
                sleep 1
                let ++seconds
                if cassandraReady; then
                    ready=true
                    break
                else
                    log "Waiting for local Cassandra to be ready ... $seconds seconds"
                fi
            done
            if [ "$ready" == true ]; then
                echo "Cassandra is ready ... starting"
                log "Cassandra is ready after waiting $seconds seconds"
            else
                echo "Gave up waiting for Cassandra ... startup failed"
                log "Cassandra not ready after waiting $seconds seconds"
                log "Giving up"
                rm -f $LOCK_FILE
                return
            fi
        fi
    fi

    opts=$(echo $JAVA_OPTS) # Remove line breaks
    (
        log "Starting $APP_NAME"
        su $APP_USER -c "
            cd $APP_HOME
            export IP_ADDR_ETH0='$IP_ADDR_ETH0'
            export IP_ADDR_ETH1='$IP_ADDR_ETH1'
            export LD_LIBRARY_PATH='$APP_HOME/lib/native'
            exec $JAVA $opts \\
                -jar $APP_JAR \\
                --1.9 \\
                $APP_RUBY_SCRIPT \\
                >>$LOG_DIR/rialto_stdout.log \\
                2>>$LOG_DIR/rialto_stderr.log &
            pid=\$!
            echo \$pid >$PID_FILE
        " >>$LOG_DIR/java_stdout.log 2>>$LOG_DIR/java_stderr.log
    ) &
    sleep 1
    log "$APP_NAME startup complete, pid=" `cat $PID_FILE`
}

doStatus() {
    # Only human beings would use this
    if [ -f $LOCK_FILE ]; then
        if isAliveByPid ; then
            echo "$APP_NAME instance [$APP_INSTANCE_NAME] is running, " \
                "pid=" `cat $PID_FILE`
        else
            echo "$APP_NAME instance [$APP_INSTANCE_NAME] is starting up" \
                "(no pid yet)"
        fi
    else
        if isAliveByPid ; then
            echo "$APP_NAME instance [$APP_INSTANCE_NAME] is shutting down, " \
                "pid=" `cat $PID_FILE`
        else
            echo "$APP_NAME instance [$APP_INSTANCE_NAME] is not running"
        fi
    fi
}



#
# Ensure log file exists and is owned by rialto userid.
#
if [ $UID -eq 0 ]; then
    touch $LOG_FILE
    chown rialto: $LOG_FILE
fi

#
# Execute specified command.
#
log "-----> Command '$1' invoked by userid $USER, script '$0'"
case "$1" in
start)
    quitIfNotRoot
    doStart
    ;;
stop)
    quitIfNotRoot
    doStop
    ;;
restart)
    quitIfNotRoot
    echo "Shutting down $APP_NAME instance [$APP_INSTANCE_NAME]"
    doStop
    if isAliveByPid ; then
        echo "Timeout waiting for $APP_NAME instance [$APP_INSTANCE_NAME]" \
            " to shut down"
        exit 1
    fi
    echo "Starting $APP_NAME instance [$APP_INSTANCE_NAME]"
    doStart
    doStatus
    echo "See $LOG_FILE for more details"
    ;;
status)
    doStatus
    echo "See $LOG_FILE for more details"
    ;;
*)
    echo "$0: {start|stop|restart|status}"
    echo "See $LOG_FILE for more details"
    exit 1
    ;;
esac

exit 0
