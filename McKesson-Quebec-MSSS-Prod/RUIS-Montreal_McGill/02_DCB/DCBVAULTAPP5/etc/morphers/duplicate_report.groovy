log.info("Begin DuplicateReportScript")
 
def getReportText = {
    for (item in it.get(ContentSequence)) {
        code = item.get(ConceptNameCodeSequence)[0].get(CodeValue)
         if (code == "121070") {
             return item.get(ContentSequence)[0].get(TextValue)
         }
    }
}
 
inputText = getReportText(input)
otherText = getReportText(other)
log.info("Input report text:\n{}", inputText)
log.info("Other report text:\n{}", otherText)
if (inputText != otherText) {
   log.info("Report text does not match.  Will generate new report.")
   return false
}
 
inputIpid = get(IssuerOfPatientID)
otherIpid = other.get(IssuerOfPatientID)
log.info("Input issuer: {}", inputIpid)
log.info("Other issuer: {}", otherIpid)
if (inputIpid != otherIpid) {
   log.info("Issuers do not match.  Will generate new report.")
   return false
}
 
inputObserverName = get(VerifyingObserverSequence)[0].get(VerifyingObserverName)
otherObserverName = other.get(VerifyingObserverSequence)[0].get(VerifyingObserverName)
log.info("Input observer name: {}", inputObserverName)
log.info("Other observer name: {}", otherObserverName)
if (inputObserverName != otherObserverName) {
   log.info("Observer names do not match.  Will generate new report.")
   return false
}
 
inputObserverNumber = get(VerifyingObserverSequence)[0].get(VerifyingObserverIdentificationCodeSequence)[0].get(CodeValue)
otherObserverNumber = other.get(VerifyingObserverSequence)[0].get(VerifyingObserverIdentificationCodeSequence)[0].get(CodeValue)
log.info("Input observer number: {}", inputObserverNumber)
log.info("Other observer number: {}", otherObserverNumber)
if (inputObserverNumber != otherObserverNumber) {
   log.info("Observer numbers do not match.  Will generate new report.")
   return false
}
 
inputVerificationDatetime = get(VerifyingObserverSequence)[0].get(VerificationDateTime)
otherVerificationDatetime = other.get(VerifyingObserverSequence)[0].get(VerificationDateTime)
log.info("Input verification datetime: {}", inputVerificationDatetime)
log.info("Other verification datetime: {}", otherVerificationDatetime)
if (inputVerificationDatetime != otherVerificationDatetime) {
   log.info("Verification datetime values do not match. Will generate new report.")
   return false
}

inputVerificationFlag = get(VerificationFlag)
otherVerificationFlag = other.get(VerificationFlag)
log.info("Input Verification Flag: {}", inputVerificationFlag)
log.info("Other Verification Flag: {}", otherVerificationFlag)
if (inputVerificationFlag != otherVerificationFlag) {
   log.info("Verification Flag does not match.  Will generate new report.")
   return false
}

log.debug("Reports duplicate each other.  Will not generate new report.")
return true
