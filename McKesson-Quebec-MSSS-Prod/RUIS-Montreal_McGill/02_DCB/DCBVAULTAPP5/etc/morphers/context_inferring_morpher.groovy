log.info("Start ContextInferringMorpher script")

//Site is not ready yet to use DICOM tags to specify the domain universal ID.  Until then, we will map from the CallingAETitle

//universalID = get("IssuerOfPatientIDQualifiersSequence/UniversalEntityID")
//issuerID = get(IssuerOfPatientID)

//log.info("Universal entity ID is: {}", universalID)
//log.info("Issuer of patient ID is: {}", issuerID)

// Get the context from the universal entity ID, if it exists
//if (universalID) {
//    return universalID
//}

// Get the context from the issuer
//if (issuerID == "TEST&test.issuer&ISO") {
//    return "test.issuer"
//}

issuerID = get(IssuerOfPatientID)

if (issuerID != null && issuerID != '') {
    log.debug("Issuer of patient ID is: {}", issuerID)
    return issuerID

} else {
switch (getCallingAETitle()) {

case "DCMSND":
    return "2.16.124.113638.1.1.1.1"

case "ALI_HSM":
    return "2.16.124.10.101.1.60.1.1000.1"
case "ALI_HSCM":
    return "2.16.124.10.101.1.60.1.1001.1"
case "ALI_JGH":
    return "2.16.124.10.101.1.60.1.1002.1"
case "ALI_SCO":
    return "2.16.124.10.101.1.60.1.1003.1"
case "ALI_STJU":
    return "2.16.124.10.101.1.60.1.1004.1"

case "ALI_HJT":
    return "2.16.124.10.101.1.60.1.1005.1"
case "ALI_SOV":
    return "2.16.124.10.101.1.60.1.1006.1"
case "ALI_ODI":
    return "2.16.124.10.101.1.60.1.1007.1"
case "ALI_GATI":
    return "2.16.124.10.101.1.60.1.1008.1"
case "ALI_HULL":
    return "2.16.124.10.101.1.60.1.1009.1"

case "ALI_MANI":
    return "2.16.124.10.101.1.60.1.1010.1"
case "ALI_CSVO":
    return "2.16.124.10.101.1.60.1.1012.1"
case "ALI_VALD":
    return "2.16.124.10.101.1.60.1.1013.1"
case "ALI_CHRN":
    return "2.16.124.10.101.1.60.1.1015.1"
case "ALI_CHLS":
    return "2.16.124.10.101.1.60.1.1016.1"

case "ALI_GAME":
    return "2.16.124.10.101.1.60.1.1017.1"
case "ALI_PAVM":
    return "2.16.124.10.101.1.60.1.1018.1"
case "ALI_PETI":
    return "2.16.124.10.101.1.60.1.1019.1"
case "ALI_PAPI":
    return "2.16.124.10.101.1.60.1.1020.1"
case "ALI_FORT":
    return "2.16.124.10.101.1.60.1.1021.1"

case "ALI_SHAW":
    return "2.16.124.10.101.1.60.1.1022.1"
case "ALI_HDDA":
    return "2.16.124.10.101.1.60.1.1023.1"
case "ALI_TEKI":
    return "2.16.124.10.101.1.60.1.1024.1"
case "ALI_CHMS":
    return "2.16.124.10.101.1.60.1.1026.1"
case "ALI_PTME":
    return "2.16.124.10.101.1.60.1.1027.1"

case "ALI_HCIM":
    return "2.16.124.10.101.1.60.1.1028.1"
case "ALI_HLHL":
    return "2.16.124.10.101.1.60.1.1030.1"
case "ALI_CHRP":
    return "2.16.124.10.101.1.60.1.1031.1"
case "ALI_IRGLM":
    return "2.16.124.10.101.1.60.1.1032.1"
case "ALI_IPPM":
    return "2.16.124.10.101.1.60.1.1033.1"

case "ALI_IUGM":
    return "2.16.124.10.101.1.60.1.1034.1"
case "ALI_HCB":
    return "2.16.124.10.101.1.60.1.1035.1"
case "ALI_HNDM":
    return "2.16.124.10.101.1.60.1.1036.1"
case "ALI_SMSL":
    return "2.16.124.10.101.1.60.1.1037.1"

case "ALI_CHCM":
    return "2.16.124.10.101.1.60.1.3000.102.10.1041.1"
case "ALI_HLAF":
    return "2.16.124.10.101.1.60.1.3000.102.10.1041.2"
case "ALI_BATI":
    return "2.16.124.10.101.1.60.1.3000.102.10.1042.1"
case "ALI_STTI":
    return "2.16.124.10.101.1.60.1.3000.102.10.1042.2"
case "ALI_CHLT":
    return "2.16.124.10.101.1.60.1.3000.102.10.1043.1"

case "ALI_FOVI":
    return "2.16.124.10.101.1.60.1.3000.102.10.1044.1"
case "ALI_HCRN":
    return "2.16.124.10.101.1.60.1.3000.102.10.1044.2"
case "ALI_CHCO":
    return "2.16.124.10.101.1.60.1.3000.102.10.1045.1"
case "ALI_HCLO":
    return "2.16.124.10.101.1.60.1.3000.102.10.1046.1"
case "ALI_CHSM":
    return "2.16.124.10.101.1.60.1.3000.102.10.1047.0"

case "ALI_HCSL":
    return "2.16.124.10.101.1.60.1.3000.102.10.1048.1"
case "ALI_HJRL":
    return "2.16.124.10.101.1.60.1.3000.102.10.1049.1"
case "ALI_CRNL":
    return "2.16.124.10.101.1.60.1.3000.102.10.1050.0"
case "ALI_CRSL":
    return "2.16.124.10.101.1.60.1.3000.102.10.1051.0"
case "ALI_CHML":
    return "2.16.124.10.101.1.60.1.3000.102.10.1052.0"

case "ALI_CHPB":
    return "2.16.124.10.101.1.60.1.3000.102.10.1053.1"
case "ALI_HCLM":
    return "2.16.124.10.101.1.60.1.3000.102.10.1054.1"
case "ALI_CHUM":
    return "2.16.124.10.101.1.60.1.3000.102.10.1055.0"
case "ALI_HMR":
    return "2.16.124.10.101.1.60.1.3000.102.10.1056.1"
case "ALI_CUSM":
    return "2.16.124.10.101.1.60.1.3000.102.10.1057.0"

case "ALI_HOFL":
    return "2.16.124.10.101.1.60.1.3000.102.10.1058.1"
case "ALI_HDLA":
    return "2.16.124.10.101.1.60.1.3000.102.10.1059.1"
case "ALI_CSCI":
    return "2.16.124.10.101.1.60.1.3000.102.10.1060.0"
case "ALI_CHLA":
    return "2.16.124.10.101.1.60.1.3000.102.10.1061.1"
case "ALI_CHDS":
    return "2.16.124.10.101.1.60.1.3000.102.10.1062.1"

case "ALI_CHSE":
    return "2.16.124.10.101.1.60.1.3000.102.10.1063.1"
case "ALI_HDSJ":
    return "2.16.124.10.101.1.60.1.3000.102.10.1064.1"
case "ALI_CHAL":
    return "2.16.124.10.101.1.60.1.3000.102.10.1065.1"
case "ALI_CHRS":
    return "2.16.124.10.101.1.60.1.3000.102.10.1066.1"
case "ALI_HDSO":
    return "2.16.124.10.101.1.60.1.3000.102.10.1067.1"

case "ALI_HBBM":
    return "2.16.124.10.101.1.60.1.3000.102.10.1068.1"
case "ALI_PUVI":
    return "2.16.124.10.101.1.60.1.3000.102.10.1069.0"
case "ALI_KUUJ":
    return "2.16.124.10.101.1.60.1.3000.102.10.1070.0"
case "ALI_CHIS":
    return "2.16.124.10.101.1.60.1.3000.102.10.1071.0"
case "ALI_HDOU":
    return "2.16.124.10.101.1.60.1.3000.102.10.1074.1"

case "ALI_ICM":
    return "2.16.124.10.101.1.60.1.3000.102.10.1078.1"
case "ALI_HND":
    return "2.16.124.10.101.1.60.1.3000.102.10.1080.1"

case "ALI_VICTO":
    return "2.16.124.10.101.1.60.1.3000.103.10.1008.1"
case "ALI_DRUM":
    return "2.16.124.10.101.1.60.1.3000.103.10.1009.1"
case "ALI_YAMA":
    return "2.16.124.10.101.1.60.1.3000.103.10.1011.1"
case "ALI_ROUV":
    return "2.16.124.10.101.1.60.1.3000.103.10.1013.1"

case "ALI_IMGX":
    return "2.16.124.10.101.1.60.1.3000.201.1000.0"
case "ALI_RIMC":
    return "2.16.124.10.101.1.60.1.3000.201.1001.0"
case "ALI_CRSH":
    return "2.16.124.10.101.1.60.1.3000.201.1002.0"
case "ALI_RDGX":
    return "2.16.124.10.101.1.60.1.3000.201.1003.0"
case "ALI_CIMC":
    return "2.16.124.10.101.1.60.1.3000.201.1004.0"
case "ALI_CLLE":
    return "2.16.124.10.101.1.60.1.3000.201.1005.0"
case "ALI_RAVA":
    return "2.16.124.10.101.1.60.1.3000.201.1006.0"
case "ALI_IMPI":
    return "2.16.124.10.101.1.60.1.3000.201.1007.0"
case "ALI_IMTE":
    return "2.16.124.10.101.1.60.1.3000.201.1008.0"
case "ALI_SRJO":
    return "2.16.124.10.101.1.60.1.3000.201.1009.0"
case "ALI_SMBB":
    return "2.16.124.10.101.1.60.1.3000.201.1010.0"
case "ALI_RCLM":
    return "2.16.124.10.101.1.60.1.3000.201.1011.0"
case "ALI_RAMD":
    return "2.16.124.10.101.1.60.1.3000.201.1012.0"
case "ALI_SOCM":
    return "2.16.124.10.101.1.60.1.3000.201.1013.0"
case "ALI_CRHR":
    return "2.16.124.10.101.1.60.1.3000.201.1014.0"
case "ALI_RPBL":
    return "2.16.124.10.101.1.60.1.3000.201.1015.0"
case "ALI_CRAU":
    return "2.16.124.10.101.1.60.1.3000.201.1016.0"
case "ALI_CRSA":
    return "2.16.124.10.101.1.60.1.3000.201.1017.0"
case "ALI_RMAI":
    return "2.16.124.10.101.1.60.1.3000.201.1018.0"
case "ALI_CRLE":
    return "2.16.124.10.101.1.60.1.3000.201.1019.0"
case "ALI_CLIX":
    return "2.16.124.10.101.1.60.1.3000.201.1020.0"
case "ALI_CIRE":
    return "2.16.124.10.101.1.60.1.3000.201.1021.0"
case "ALI_CLSL":
    return "2.16.124.10.101.1.60.1.3000.201.1022.0"
case "ALI_RDTR":
    return "2.16.124.10.101.1.60.1.3000.201.1023.0"
case "ALI_CONC":
    return "2.16.124.10.101.1.60.1.3000.201.1024.0"

default:
    log.warn("WARNING - IssuerOfPatientID is null or blank and Calling AE Title does not match anything in our list - returning null")
    return null
    }
}
