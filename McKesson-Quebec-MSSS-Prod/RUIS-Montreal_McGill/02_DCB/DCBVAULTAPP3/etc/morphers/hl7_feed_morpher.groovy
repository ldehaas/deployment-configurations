log.debug("HL7 Feed before morphing: \n {}", input)

def messageType = get('/.MSH-9-1')
def triggerEvent = get('/.MSH-9-2')

if (messageType == "ADT" && triggerEvent == "A40") {
	log.debug("Removing fully qualified pid from MRG segment.")
	set('MRG-1-4-1', "")
	set('MRG-1-4-2', "")
	set('MRG-1-4-3', "")
}

def PID21 = get('PID-2-1')
def PID241 = get('PID-2-4-1')
def PID242 = get('PID-2-4-2')
def PID243 = get('PID-2-4-3')
def PID25 = get('PID-2-5')

set('PID-2-1', get('PID-4-1'))
set('PID-2-4-1', get('PID-4-4-1'))
set('PID-2-4-2', get('PID-4-4-2'))
set('PID-2-4-3', get('PID-4-4-3'))
set('PID-2-5', get('PID-4-5'))

set('PID-4-1', PID21)
set('PID-4-4-1', PID241)
set('PID-4-4-2', PID242)
set('PID-4-4-3', PID243)
set('PID-4-5', PID25)

log.debug("HL7 Feed after morphing: \n {}", input)
return true
