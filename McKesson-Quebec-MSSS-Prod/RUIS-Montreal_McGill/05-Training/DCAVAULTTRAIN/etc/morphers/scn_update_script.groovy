import org.dcm4che2.data.BasicDicomObject
import com.karos.groovy.morph.dcm.DicomScriptingAPI
import org.dcm4che2.util.UIDUtils
import org.joda.time.DateTime
import org.apache.commons.lang.StringUtils

LOAD("common.groovy")

log.info("Start SCNUpdateScript")

def publishManifestRequired = false
def updateValueInManifestIfRequired = { tag, originalValue, newValue ->
    if (originalValue != newValue) {
        log.info("Will change {} from {} to {}", tag, originalValue, newValue)
        set(tag, newValue)
        publishManifestRequired = true
    }
}
def updateValueInManifestIfRequired2 = { tag, originalValue, newValue ->
    if (originalValue == null) {
        log.info("Manifest has no studyDate and studyTime value, will update to {}", newValue)
        set(tag, newValue)
        publishManifestRequired = true
    } else {
        log.info("Manifest already has studyDate and studyTime values: {}, no update will be made", originalValue)
    }
}

def sdt = get ('OBR-7')
if (StringUtils.isNotBlank(sdt) ) {
    log.debug("OBR-7 in SCN has a value, checking against manifest value")
    def studyDate = DICOMTime.parseDate(get('OBR-7'))
    def studyTime = DICOMTime.parseTime(get('OBR-7'))

    if (StringUtils.isNotBlank(studyDate) && StringUtils.isNotBlank(studyTime)) {
        updateValueInManifestIfRequired2(
            StudyDate,
            output.get(StudyDate),
            studyDate)
    
        updateValueInManifestIfRequired2(
            StudyTime, 
            output.get(StudyTime),
            studyTime)
    } else {
        log.debug("Either the studyDate or studyTime has a blank or null value, no update will be made")
    }

} else {
    log.debug("OBR-7 in SCN is either blank of null")
}

updateValueInManifestIfRequired(
    AdditionalPatientHistory,
    output.get(AdditionalPatientHistory),
    get("PV2-3"))
 
updateValueInManifestIfRequired(
    StudyPriorityID,
    output.get(StudyPriorityID),
    get("OBR-5"))
 
updateValueInManifestIfRequired(
    CurrentPatientLocation,
    output.get(CurrentPatientLocation),
    get("PV1-3"))

updateValueInManifestIfRequired(
    PatientID,
    output.get(PatientID),
    get("PID-3-1"))

updateValueInManifestIfRequired(
    StudyID,
    output.get(StudyID),
    get("OBR-18"))

updateValueInManifestIfRequired(
    AccessionNumber,
    output.get(AccessionNumber),
    get("OBR-19"))

updateValueInManifestIfRequired(
    Modality,
    output.get(Modality),
    get("OBR-24"))

return publishManifestRequired
