/**
 * converts HL7 ORU to a DICOM SR object
 * draft according to the SAD v0.5
 *
 * many of the HL7 locations are potentially different in the field. this 
 * script is simply a starting point.
 */
import org.dcm4che2.util.UIDUtils
import org.joda.time.DateTime
LOAD("common.groovy")

set(SpecificCharacterSet, 'ISO_IR 100')
set(InstanceCreationDate, new DateTime())
set(InstanceCreationDate, new DateTime())
set(InstanceCreatorUID, '1.2.3.4.5.6.7.8.9')

set(Modality, 'SR')

set(SOPClassUID, '1.2.840.10008.5.1.4.1.1.88.11') // basic SR

set(StudyInstanceUID, get('ZDS-1'))
set(SeriesInstanceUID, UIDUtils.createUID())
set(SOPInstanceUID, UIDUtils.createUID())

if (get('OBR-7') != null && get('OBR-7') != '') {
    def studyDate = DICOMTime.parseDate(get('OBR-7'))
    def studyTime = DICOMTime.parseTime(get('OBR-7'))

    set(StudyDate, studyDate)
    set(StudyTime, studyTime)
    set(SeriesDate, studyDate)
    set(SeriesTime, studyTime)
}

if (get('OBR-22') != null && get('OBR-22') != '') {
    def contentDate = DICOMTime.parseDate(get('OBR-22'))
    def contentTime = DICOMTime.parseTime(get('OBR-22'))

    set(ContentDate, contentDate)
    set(ContentTime, contentTime)
}

def acc_num = get('OBR-19') // Technically this is the Requested Procedure ID, but most sites want us to treat this as the Accession Number
def sending_facility = get('MSH-4')
// Intelrad sites use Order ID (OBR-18) as the Accession Number. 
// All the other sites use the Requested Procedure ID (OBR-19).
def intelerad_sending_facilities = ["LZ", "GK"]
if ( ! intelerad_sending_facilities.contains(sending_facility) ) {
    set(AccessionNumber, acc_num)
} else {
    // Special treatment for the Intelerad sites.
    def order_id = get('OBR-18')
    set(AccessionNumber, order_id)
}

set(StudyID, acc_num)

setPersonName(ReferringPhysicianName, 'OBR-16', startingPosition=2)
set('ReferringPhysicianIdentificationSequence/InstitutionName', get('MSH-4'))

def seq = 'ReferringPhysicianIdentificationSequence/PersonIdentificationCodeSequence';
set(seq+'/CodeValue', get('OBR-16-1'))
set(seq+'/CodingSchemeDesignator', get('MSH-4'))
set(seq+'/CodeMeaning', 'Referring Physician ID')

set(StationName, get('ORC-13'))

//set(NameOfPhysiciansReadingStudy, get('OBR-32-2')+ '^' + get('OBR-32-3'))
//Changed Name Of Physicians Reading Study as per khc3118
set(NameOfPhysiciansReadingStudy, get('OBR-32-1-2')+ '^' + get('OBR-32-1-3'))

set('AnatomicRegionSequence/CodeValue', get('OBR-15-4-1'))
set('AnatomicRegionSequence/CodingSchemeDesignator', get('OBR-15-1-3'))
set('AnatomicRegionSequence/CodeValue', get('OBR-15-4-1'))

setPersonName(PatientName, 'PID-5')
set(PatientID, get('PID-3-1'))
set(IssuerOfPatientID, get('PID-3-4-2')) // or inferred

set(PatientBirthDate, get('PID-7'))
set(PatientSex, get('PID-8'))

set(InstanceNumber, '1')
set(StudyStatusID, get('OBR-25'))
set(ReasonForStudy, get('ORC-16'))
set(RequestingPhysician, get('ORC-12'))
set(RequestingService, get('MSH-4'))

set('RequestedProcedureCodeSequence/CodeValue', get('OBR-4-1'))
set('RequestedProcedureCodeSequence/CodingSchemeDesignator', 'Imagerie Qu\u00e9bec-DSQ')
set('RequestedProcedureCodeSequence/CodeMeaning', get('OBR-4-2'))

set(RequestedProcedureID, acc_num)
set(ValueType, 'CONTAINER')

set('ConceptNameCodeSequence/CodeValue', '18748-4')
set('ConceptNameCodeSequence/CodingSchemeDesignator', 'LN')
set('ConceptNameCodeSequence/CodeMeaning', 'Diagnostic Imaging Report')

set(ContinuityOfContent, 'CONTINUOUS')

set('VerifyingObserverSequence/VerifyingOrganization', get('MSH-4'))
set('VerifyingObserverSequence/VerificationDateTime', get('OBR-22'))
set('VerifyingObserverSequence/VerifyingObserverName', get('OBR-32-1-2')+ '^' + get('OBR-32-1-3'))
seq = 'VerifyingObserverSequence/VerifyingObserverIdentificationCodeSequence'
set(seq+'/CodeValue', get('OBR-32-1'))
set(seq+'/CodingSchemeDesignator', get('MSH-4'))
set(seq+'/CodeMeaning', get('OBR-32-2')+ '^' + get('OBR-32-3'))

set('ReferencedRequestSequence/AccessionNumber', acc_num)
set('ReferencedRequestSequence/StudyInstanceUID', get('ZDS-1'))
set('ReferencedRequestSequence/RequestedProcedureDescription', get('OBR-4-2'))
set('ReferencedRequestSequence/RequestedProcedureID', get('ORC-3'))
set('ReferencedRequestSequence/PlacerOrderNumberProcedure', get('ORC-2'))
set('ReferencedRequestSequence/FillerOrderNumberProcedure', get('ORC-3'))

seq = 'ReferencedRequestSequence/RequestedProcedureCodeSequence'
set(seq+'/CodeValue', get('OBR-4-1'))
set(seq+'/CodeMeaning', get('OBR-4-2'))

set(CompletionFlag, 'COMPLETE')
if ((get('OBX-11') == 'F') || (get('OBX-11') == 'C')) {
    set(VerificationFlag, 'VERIFIED')
} else {
    set(VerificationFlag, 'UNVERIFIED')
}

set('ContentSequence[1]/RelationshipType', 'HAS CONCEPT MOD')
set('ContentSequence[1]/ValueType', 'CODE')
set('ContentSequence[1]/ConceptNameCodeSequence/CodeValue', '121058')
set('ContentSequence[1]/ConceptNameCodeSequence/CodingSchemeDesignator', 'DCM')
set('ContentSequence[1]/ConceptNameCodeSequence/CodeMeaning', 'Procedure Reported')

set('ContentSequence[1]/ConceptCodeSequence/CodeValue', get('OBR-4-1'))
set('ContentSequence[1]/ConceptCodeSequence/CodingSchemeDesignator', 'RP')
set('ContentSequence[1]/ConceptCodeSequence/CodeMeaning', get('OBR-4-2'))

set('ContentSequence[2]/RelationshipType', 'HAS CONCEPT MOD')
set('ContentSequence[2]/ValueType', 'CODE')
set('ContentSequence[2]/ConceptNameCodeSequence/CodeValue', '121049')
set('ContentSequence[2]/ConceptNameCodeSequence/CodingSchemeDesignator', 'DCM')
set('ContentSequence[2]/ConceptNameCodeSequence/CodeMeaning', 'Language of Content Item and Descendants')

set('ContentSequence[2]/ConceptCodeSequence/CodeValue', 'en') // or fr?
set('ContentSequence[2]/ConceptCodeSequence/CodingSchemeDesignator', 'RFC3066')
set('ContentSequence[2]/ConceptCodeSequence/CodeMeaning', 'English') // or french


set('ContentSequence[3]/RelationshipType', 'HAS OBS CONTEXT')
set('ContentSequence[3]/ValueType', 'CODE')
set('ContentSequence[3]/ConceptNameCodeSequence/CodeValue', '121005')
set('ContentSequence[3]/ConceptNameCodeSequence/CodingSchemeDesignator', 'DCM')
set('ContentSequence[3]/ConceptNameCodeSequence/CodeMeaning', 'Observer Type')

set('ContentSequence[3]/ConceptCodeSequence/CodeValue', '121006')
set('ContentSequence[3]/ConceptCodeSequence/CodingSchemeDesignator', 'DCM')
set('ContentSequence[3]/ConceptCodeSequence/CodeMeaning', 'Person')

set('ContentSequence[4]/RelationshipType', 'HAS OBS CONTEXT')
set('ContentSequence[4]/ValueType', 'CODE')
set('ContentSequence[4]/ConceptNameCodeSequence/CodeValue', '121011')
set('ContentSequence[4]/ConceptNameCodeSequence/CodingSchemeDesignator', 'DCM')
set('ContentSequence[4]/ConceptNameCodeSequence/CodeMeaning', 'Person Observer\'s Role in this Procedure')

set('ContentSequence[4]/ConceptCodeSequence/CodeValue', '121097')
set('ContentSequence[4]/ConceptCodeSequence/CodingSchemeDesignator', 'DCM')
set('ContentSequence[4]/ConceptCodeSequence/CodeMeaning', 'Recording')


set('ContentSequence[5]/RelationshipType', 'CONTAINS')
set('ContentSequence[5]/ValueType', 'CONTAINER')
set('ContentSequence[5]/ConceptNameCodeSequence/CodeValue', '121064')
set('ContentSequence[5]/ConceptNameCodeSequence/CodingSchemeDesignator', 'DCM')
set('ContentSequence[5]/ConceptNameCodeSequence/CodeMeaning', 'Current Procedure Description')
set('ContentSequence[5]/ContinuityOfContent', 'CONTINUOUS')

seq = 'ContentSequence[5]/ContentSequence'

set(seq+'/RelationshipType', 'CONTAINS')
set(seq+'/ValueType', 'TEXT')
set(seq+'/ConceptNameCodeSequence/CodeValue', '121065')
set(seq+'/ConceptNameCodeSequence/CodingSchemeDesignator', 'DCM')
set(seq+'/ConceptNameCodeSequence/CodeMeaning', 'Procedure Description')
set(seq+'/TextValue', get('OBR-4-1') + '^' + get('OBR-4-2'))

set('ContentSequence[6]/RelationshipType', 'CONTAINS')
set('ContentSequence[6]/ValueType', 'CONTAINER')
set('ContentSequence[6]/ConceptNameCodeSequence/CodeValue', '121070')
set('ContentSequence[6]/ConceptNameCodeSequence/CodingSchemeDesignator', 'DCM')
set('ContentSequence[6]/ConceptNameCodeSequence/CodeMeaning', 'Findings')

seq = 'ContentSequence[6]/ContentSequence'
set(seq+'/ValueType', 'TEXT')
set(seq+'/ConceptNameCodeSequence/CodeValue', '121071')
set(seq+'/ConceptNameCodeSequence/CodingSchemeDesignator', 'DCM')
set(seq+'/ConceptNameCodeSequence/CodeMeaning', 'Finding')
def reportTextSegments = getList("OBX-5(*)")
for (int i = 0 ; i < reportTextSegments.size() ; ++i) {
        if (reportTextSegments[i] == null)
                reportTextSegments[i] = ''
}
def reportText = reportTextSegments.join('\n')
reportText = reportText.replaceAll("\\\\X0D\\\\\\\\X0A\\\\", "\r\n")
set(seq+'/TextValue', reportText)

// don't yet know how to distinguish impression for regular findings
/*
set('ContentSequence[7]/RelationshipType', 'CONTAINS')
set('ContentSequence[7]/ValueType', 'CONTAINER')
set('ContentSequence[7]/ConceptNameCodeSequence/CodeValue', '121072')
set('ContentSequence[7]/ConceptNameCodeSequence/CodingSchemeDesignator', 'DCM')
set('ContentSequence[7]/ConceptNameCodeSequence/CodeMeaning', 'Impressions')

seq = 'ContentSequence[7]/ContentSequence'
set(seq+'/RelationshipType', 'CONTAINS')
set(seq+'/ValueType', 'TEXT')
set(seq+'/ConceptNameCodeSequence/CodeValue', '121073')
set(seq+'/ConceptNameCodeSequence/CodingSchemeDesignator', 'DCM')
set(seq+'/ConceptNameCodeSequence/CodeMeaning', 'Impression')
set(seq+'/TextValue', '');
*/

