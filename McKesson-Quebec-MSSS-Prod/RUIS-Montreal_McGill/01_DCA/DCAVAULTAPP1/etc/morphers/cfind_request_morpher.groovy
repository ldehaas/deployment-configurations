/**
 * Copyright Karos Health Incorporated ${year}
 * @author Victor Lauria <vlauria@vitalimages.com> 
 * @since ${date}
 */

// ==========================================================================================
// Project:         Integration Activities for Phase 2 deployment of Vault
// Task:            CR25 - Drop 2 tags from CFinds
// JIRA ticket:     CS-4586
// Written by:      Victor Lauria <vlauria@vitalimages.com>
// Version:         1.1
// Created on:      July 9, 2018
// Last Update:     July 11, 2018
// Description:     CHC has found that there are 2 tags in the CFinds from Vault that, 
//                  depending on CIR's cache status for a given study, can cause CIR 
//                  to go to disk which slows down the CFind response.
//                  CHC has also said that the two tags in question are unnecessary for CIR.
//                  The DICOM tags that are to be dropped are:
//                  0040,A491 (CompletionFlag) and 0040,A493 (VerificationFlag)
// ==========================================================================================

// Variables
def scriptName = "CFind Request Morpher - "

// Log information before doing anything
log.info(scriptName + "STARTED")
log.info(scriptName + "Calling AE: {}", getCallingAETitle())

// Log input
log.debug(scriptName + "Input: {}", input)

// Remove tag 0040,A491
log.debug(scriptName + "Removing CompletionFlag")
remove(CompletionFlag)

// Remove tag 0040,A493
log.debug(scriptName + "Removing VerificationFlag")
remove(VerificationFlag)

// Log output
log.debug(scriptName + "Output: {}", output)

// Log end of script
log.info(scriptName  + "END of script")

