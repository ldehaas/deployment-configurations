def setName(hl7Location, dicomTag) {

    def lastName = get(hl7Location + '-1')
    def firstName = get(hl7Location + '-2')

    def fullName = lastName + '^' + firstName;
    set(dicomTag, fullName)
}

log.debug("Processing patient merge script.")
log.debug("Received HL7 ADT: {}", input)

setName('PID-5', PatientName)
set(PatientSex, get('PID-8'))
set(PatientBirthDate, get('PID-7'))
// set surviving local pid
localSurvivingPid = get('PID-3-1')
localSurvivingDomainId = get('PID-3-4-2')
 
localSubsumedPid = get('MRG-1-1')
localSubsumedDomainId = get('MRG-1-4-2')

set(PatientID, localSurvivingPid)
set(IssuerOfPatientID, localSurvivingDomainId)

def publishManifest = true
if (output.get(NumberOfStudyRelatedInstances) == null) {
    publishManifest = false
}

log.debug("Done processing patient merge script.")
log.debug("Manifest:\n {}", output)

return publishManifest
