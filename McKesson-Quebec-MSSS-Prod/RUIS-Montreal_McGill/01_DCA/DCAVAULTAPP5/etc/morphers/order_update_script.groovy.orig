import org.dcm4che2.data.BasicDicomObject;
import com.karos.groovy.morph.dcm.DicomScriptingAPI;

log.debug("Processing order update script.\n {}", input)
def publishManifestRequired = false

def updateValueInManifestIfRequired = { tag, originalValue, newValue ->
    if (originalValue != newValue) {
        set(tag, newValue)
        publishManifestRequired = true
    }
}

updateValueInManifestIfRequired(
    ReferringPhysicianName,
    output.get(ReferringPhysicianName),
    get('OBR-16-2') + '^' + get('OBR-16-3'))

updateValueInManifestIfRequired(
    "ReferringPhysicianIdentificationSequence/PersonIdentificationCodeSequence/CodeValue", 
    output.get("ReferringPhysicianIdentificationSequence/PersonIdentificationCodeSequence/CodeValue"), 
    get('ORC-12-1'))

def studyDescription = get('OBR-4-2')
studyDescription = studyDescription.replaceAll("\\\\E\\\\", "\\")

updateValueInManifestIfRequired(
    StudyDescription, 
    output.get(StudyDescription), 
    studyDescription)


updateValueInManifestIfRequired("AnatomicRegionSequence/CodeValue", 
    output.get("AnatomicRegionSequence/CodeValue"), 
    get("OBR-15-4"))

updateValueInManifestIfRequired(
    "AnatomicRegionSequence/CodingSchemeDesignator", 
    output.get("AnatomicRegionSequence/CodingSchemeDesignator"), 
    "Imagerie Qu\u00e9bec-DSQ")

updateValueInManifestIfRequired(
    "AnatomicRegionSequence/CodeMeaning", 
    output.get("AnatomicRegionSequence/CodeMeaning"), 
    "Code de r\u00e9gion anatomique")

updateValueInManifestIfRequired(
    ReasonForStudy, 
    output.get(ReasonForStudy), 
    get("OBR-31"))

updateValueInManifestIfRequired(
    "RequestedProcedureCodeSequence/CodeValue",
    output.get("RequestedProcedureCodeSequence/CodeValue"),
    get('OBR-4-1'))


updateValueInManifestIfRequired(
    "RequestedProcedureCodeSequence/CodingSchemeDesignator",
    output.get("RequestedProcedureCodeSequence/CodingSchemeDesignator"),
    get('OBR-4-1'))

updateValueInManifestIfRequired(
    "RequestedProcedureCodeSequence/CodeMeaning", 
    output.get("RequestedProcedureCodeSequence/CodeMeaning"), 
    get('OBR-4-2'))

updateValueInManifestIfRequired(
    "RequestedProcedureCodeSequence/CodeMeaning", 
    output.get("RequestedProcedureCodeSequence/CodeMeaning"), 
    get('OBR-4-2'))

updateValueInManifestIfRequired(
    NameOfPhysiciansReadingStudy,
    output.get(NameOfPhysiciansReadingStudy),
    get("OBR-32-1") + "^" + get("OBR-32-2") + "^" + get("OBR-32-3") + "^" + get("OBR-32-4") + "^" + get("OBR-32-5"))
 
 
updateValueInManifestIfRequired(
    StudyStatusID,
    output.get(StudyStatusID),
    get("OBR-25"))

updateValueInManifestIfRequired(
    RequestingService,
    output.get(RequestingService),
    get("ORC-13"))

if (output.get(SpecificCharacterSet) == null) {
   set(SpecificCharacterSet, "ISO_IR 100")
   }

log.debug("Done processing order update script. Current Manifest:\n {}", output)

if (publishManifestRequired && output.get(NumberOfStudyRelatedInstances) != null) {
    log.debug("Order processing script indicating that publish is required")
    return true

} else if (publishManifestRequired && output.get(NumberOfStudyRelatedInstances) == null) {
    log.debug("Order processing script updated the manifest, but there are no images so publishing not required.")
    return false

} else {
    log.debug("Order processing script indicating that publish is not required")
    return false

}
