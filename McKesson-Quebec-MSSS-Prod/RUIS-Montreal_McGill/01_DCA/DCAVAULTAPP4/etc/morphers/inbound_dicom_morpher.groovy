if (output.get(SpecificCharacterSet) == null) {
    set(SpecificCharacterSet, "ISO_IR 100")
}

def accessionNumber = get("AccessionNumber")
if (accessionNumber != null) {
    set("RequestedProcedureID", accessionNumber)
}
//if (accessionNumber != null && get("RequestedProcedureID") == null) {
//    set("RequestedProcedureID", accessionNumber)
//}
return true
