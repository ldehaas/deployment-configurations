log.info("Start SCNUpdateScript")
def publishManifestRequired = false

def updateValueInManifestIfRequired = { tag, originalValue, newValue ->
    if (originalValue != newValue) {
        log.info("Will change {} from {} to {}", tag, originalValue, newValue)
        set(tag, newValue)
        publishManifestRequired = true
    }
}

updateValueInManifestIfRequired(
    NameOfPhysiciansReadingStudy,
    output.get(NameOfPhysiciansReadingStudy),
    get("OBR-32-1") + "^" + get("OBR-32-2") + "^" + get("OBR-32-3") + "^" + get("OBR-32-4") + "^" + get("OBR-32-5"))

updateValueInManifestIfRequired(
    AdditionalPatientHistory,
    output.get(AdditionalPatientHistory),
    get("PV2-3"))

updateValueInManifestIfRequired(
    StudyStatusID,
    output.get(StudyStatusID),
    get("OBR-25"))

updateValueInManifestIfRequired(
    StudyPriorityID,
    output.get(StudyPriorityID),
    get("OBR-5"))

updateValueInManifestIfRequired(
    RequestingService,
    output.get(RequestingService),
    get("ORC-13"))

updateValueInManifestIfRequired(
    CurrentPatientLocation,
    output.get(CurrentPatientLocation),
    get("PV1-3"))

return publishManifestRequired