if (output.get(SpecificCharacterSet) == null) {
    set(SpecificCharacterSet, "ISO_IR 100")
}

def accessionNumber = get("AccessionNumber")

if (accessionNumber != null) {
    set("RequestedProcedureID", accessionNumber)
}


// SFDC 00436753 - Exclude SOP 1.3.12.2.1107.5.9.1 from publishing

def sopClassUID = get("SOPClassUID")

if (sopClassUID != null && sopClassUID == "1.3.12.2.1107.5.9.1") {
    log.info("SOP {} from study {} will not be forwarded as it is a Siemens Private CSA.", get("SOPInstanceUID"), get("StudyInstanceUID"))
    return false;
} else {
    return true;
}
