log.debug("Processing manifest update script")

def updateValueInManifestIfRequired = { tag, originalValue, newValue ->
    if (originalValue != newValue) {
        set(tag, newValue)
    }
}

if (output.get(SpecificCharacterSet) == null) {
    set(SpecificCharacterSet, "ISO_IR 100")
}

updateValueInManifestIfRequired(
    "ReferringPhysicianIdentificationSequence/InstitutionName",
    output.get("ReferringPhysicianIdentificationSequence/InstitutionName"),
    get("ReferringPhysicianIdentificationSequence/InstitutionName"))

updateValueInManifestIfRequired(
    "CurrentRequestedProcedureEvidenceSequence/AccessionNumber",
    output.get("CurrentRequestedProcedureEvidenceSequence/AccessionNumber"),
    get("AccessionNumber"))


