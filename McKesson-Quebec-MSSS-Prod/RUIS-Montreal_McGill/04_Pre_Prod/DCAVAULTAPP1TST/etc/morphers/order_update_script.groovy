import org.dcm4che2.data.BasicDicomObject
import com.karos.groovy.morph.dcm.DicomScriptingAPI
import org.dcm4che2.util.UIDUtils
import org.joda.time.DateTime
import org.apache.commons.lang.StringUtils

LOAD("common.groovy")

log.debug("Processing order update script.\n {}", input)
def publishManifestRequired = false

def updateValueInManifestIfRequired = { tag, originalValue, newValue ->
    if (originalValue != newValue) {
        set(tag, newValue)
        publishManifestRequired = true
    }
}

def sdt = get ('OBR-7')
if (StringUtils.isNotBlank(sdt) ) {
    log.debug("OBR-7 has a value, processing")
    def studyDate = DICOMTime.parseDate(get('OBR-7'))
    def studyTime = DICOMTime.parseTime(get('OBR-7'))

    if (StringUtils.isNotBlank(studyDate) && StringUtils.isNotBlank(studyTime)) {
        updateValueInManifestIfRequired(
            StudyDate,
            output.get(StudyDate),
            studyDate)
    
        updateValueInManifestIfRequired(
            StudyTime, 
            output.get(StudyTime),
            studyTime)
        log.debug("The studyDate and studyTime has been updated")
    } else {
        log.debug("Either studyDate or studyTime has a blank or null value, no update will be made")
    }

} else {
    log.debug("OBR-7 is either blank of null, no update will be made")
}

def rpn = get('OBR-16-3')
if (StringUtils.isNotBlank(rpn) ) {
    log.debug("OBR-16-3 has a value, adding to the Referring Physician name")
    updateValueInManifestIfRequired(
        ReferringPhysicianName,
        output.get(ReferringPhysicianName),
        get('OBR-16-2') + '^' + get('OBR-16-3'))
    } else {
    log.debug("OBR-16-3 is blank, only using OBR-16-2 as the Referring Physician value")
    updateValueInManifestIfRequired(
        ReferringPhysicianName,
        output.get(ReferringPhysicianName),
        get('OBR-16-2'))
} 

updateValueInManifestIfRequired(
    "ReferringPhysicianIdentificationSequence/PersonIdentificationCodeSequence/CodeValue", 
    output.get("ReferringPhysicianIdentificationSequence/PersonIdentificationCodeSequence/CodeValue"), 
    get('OBR-16-1'))

def studyDescription = get('OBR-4-2')
log.debug("THIS IS THE INPUT FOR Order Morpher SCRIPT, ACCENT TESTING.\n {}", studyDescription)
if (studyDescription != null) {
    studyDescription = studyDescription.replaceAll("\\\\E\\\\", "\\\\")

    updateValueInManifestIfRequired(
      StudyDescription, 
      output.get(StudyDescription), 
      studyDescription)
log.debug("THIS IS THE OUTPUT FOR Order Morpher SCRIPT, ACCENT TESTING.\n {}", studyDescription)
}

updateValueInManifestIfRequired("AnatomicRegionSequence/CodeValue", 
    output.get("AnatomicRegionSequence/CodeValue"), 
    get("OBR-15-4"))

updateValueInManifestIfRequired(
    "AnatomicRegionSequence/CodingSchemeDesignator", 
    output.get("AnatomicRegionSequence/CodingSchemeDesignator"), 
    "Imagerie Qu\u00e9bec-DSQ")

updateValueInManifestIfRequired(
    "AnatomicRegionSequence/CodeMeaning", 
    output.get("AnatomicRegionSequence/CodeMeaning"), 
    "Code de r\u00e9gion anatomique")

updateValueInManifestIfRequired(
    ReasonForStudy, 
    output.get(ReasonForStudy), 
    get("OBR-31"))

updateValueInManifestIfRequired(
    "RequestedProcedureCodeSequence/CodeValue",
    output.get("RequestedProcedureCodeSequence/CodeValue"),
    get('OBR-4-1'))


updateValueInManifestIfRequired(
    "RequestedProcedureCodeSequence/CodingSchemeDesignator",
    output.get("RequestedProcedureCodeSequence/CodingSchemeDesignator"),
    get('OBR-4-1'))

updateValueInManifestIfRequired(
    "RequestedProcedureCodeSequence/CodeMeaning", 
    output.get("RequestedProcedureCodeSequence/CodeMeaning"), 
    get('OBR-4-2'))

updateValueInManifestIfRequired(
    "RequestedProcedureCodeSequence/CodeMeaning", 
    output.get("RequestedProcedureCodeSequence/CodeMeaning"), 
    get('OBR-4-2'))

// Changed to OBR-32-1-2 and OBR-32-1-3 as per KHC10167
updateValueInManifestIfRequired(
    NameOfPhysiciansReadingStudy,
    output.get(NameOfPhysiciansReadingStudy),
    get("OBR-32-1-2") + "^" + get("OBR-32-1-3"))
 
 
updateValueInManifestIfRequired(
    StudyStatusID,
    output.get(StudyStatusID),
    get("OBR-25"))

updateValueInManifestIfRequired(
    RequestingService,
    output.get(RequestingService),
    get("ORC-13"))

if (output.get(SpecificCharacterSet) == null) {
   set(SpecificCharacterSet, "ISO_IR 100")
   }

log.debug("Done processing order update script. Current Manifest:\n {}", output)

if (publishManifestRequired && output.get(NumberOfStudyRelatedInstances) != null) {
    log.debug("Order processing script indicating that publish is required")
    return true

} else if (publishManifestRequired && output.get(NumberOfStudyRelatedInstances) == null) {
    log.debug("Order processing script updated the manifest, but there are no images so publishing not required.")
    return false

} else {
    log.debug("Order processing script indicating that publish is not required")
    return false

}
