if (output.get(SpecificCharacterSet) == null) {
    set(SpecificCharacterSet, "ISO_IR 100")
}

def accessionNumber = get("AccessionNumber")

if (accessionNumber != null) {
    set("RequestedProcedureID", accessionNumber)
}

return true
