/* CFind Response RANK Filter
*
*   This morpher is part of the Fetch Prior Studies workflow only.
*
*   It takes THE SET of individual study responses (CFind Responses from the DIRs, as originally listed in the XDS Registry) and optionally
*   filters it out studies based on criteria, such as study age, or a maximum number of responses.
*   This limits the number of studies that are preloaded into a local PACS as part of a Fetch Prior Studies workflow.
*
*   Common Uses for this Cfind Response Filter script can be: 
*
*       - Exclude studies that are NOT the X most recent studies (i.e. absolute number of responses, by age)
*       
*
*
*   ===================================================================================================
*   For Hopital du Sacre Coeur in Montreal, this script is currently doing the following:
*
*/


/* =========  Constant Declaration  ================ */
 
  /* This defines the ABSOLUTE number of studies that will be given to a local PACS.  "null" specifies that there is no limit */
  def Absolute_Number_of_Studies_to_be_Returned = 2



/* ================  Script  ================  */
/* if( null != Absolute_Number_of_Studies_to_be_Returned){
 
    inputs.sort(byRecency)
     // "inputs" is a variable exposed to the script.  sort(byRecency) is a method which takes into account both date and time
    
    return first(inputs, Absolute_Number_of_Studies_to_be_Returned)
 }
*/
