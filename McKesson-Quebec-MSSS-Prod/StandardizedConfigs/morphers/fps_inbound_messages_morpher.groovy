/* fps_inbound_messages_morpher.groovy
*
*   Part of the HL7 Proxy Scripts
*
*   This script cleans up the HL7 Traffic that goes to the Fetch Prior Studies handler
*
*
*   Currently, the script is doing the following:
*
*       1) Only accepting ORM orders and ADT Messages (ADTs are used to update incomplete orders (i.e. without a RAMQ)
*       2) Adding the Default Character Set to Pass French Characters Correctly to the Fetch Prior Studies Server (this is not really needed)
*       3) Checking for *BOTH* a Local MRN Value and a RAMQ Value (and failing the message if they are absent)
*           This step also sets them both where the Fetch Prior Studies Server 
*       4) Hard-coding Issuer of Patient IDs for *BOTH* RAMQs and Local MRNs (needed by the PIX Resolution further in the workflow)
*            The only exception to this is when the RIS HL7 Feed is sending an explicitly-coded Patient for a different Domain
*            In this case, the "Foreing" MRN will be added to the PIX database.
*            However, if the local PACS attempts to use this patient without an Explicit Issuer of Patient ID, the PIX lookup will fail
*                TODO This might cause collisions, check the logs to see this is not happening
*
*
*/

/* ========= Shared Files Loading ============= */
    LOAD("/home/rialto/rialto/etc/morphers/Shared_Scripting_Files/shared_constants.groovy")
    // Contains commonly-used constants as attributes of the "SharedConstants" class
    // For example: SharedConstants.rialtoRetrieveAETitle  (see the file for full details)

    LOAD("/home/rialto/rialto/etc/morphers/Shared_Scripting_Files/shared_hl7_constants.groovy")


/* ================  Script  ================  */

// Trace info
    log.debug("Starting fps_inbound_messages_morpher")
    log.debug("Message as received at fps inbound messages morpher: [{}]", input)

    def hl7MessageType = get('MSH-9-1')
    hl7MessageType = hl7MessageType.toUpperCase()
    log.debug("Local Fetch Prior Studies Forwarder: Received MSH-9-1 (HL7 Message Type) was: [{}]", hl7MessageType)


//  HL7 Message validation
    def acceptedMessageTypes = ["ORM","ADT"]
    if(! acceptedMessageTypes.contains(hl7MessageType) ){
        log.debug("Non-ORM or ADT Message Received")
        return false
    }

    def AffinityPID = get(SharedConstants.HL7_Affinity_Domain_PID_Field)

    // Ensuring that orders that have a null value still go through, as the Order cache needs to be populated.  
    // Later on, we might receive an ADT that will free the orders from the cache
    if(AffinityPID != null) {
        if( !( AffinityPID.isEmpty() ) ) {
            if(QuebecHL7Validation.RAMQformatIsInvalid(AffinityPID)){
                log.error("Cannot process Fetch Prior Studies, as the HL7 message has an Invalid RAMQ value!")
                return false
            }
        }
    }


// Add the default Character set that can handle French Characters, and that many times is not explicit in the RIS Feed
    def characterEncoding = get("MSH-18")

    if (characterEncoding == null || characterEncoding.isEmpty()){
        log.info("No character set specified.  Hard-coding to 8859/1")
        set('MSH-18', '8859/1')
    }


/* Enforce that AT LEAST the MRN exists in the incoming messages */

    def MRN = get(SharedConstants.HL7_Local_MRN_PID_Field)

    log.debug("Current MRN received is: [{}]", MRN)
    log.debug("Current RAMQ received is: [{}]", AffinityPID)


    // Enforce that AT LEAST the MRN exists

        if( null != MRN){

            if(! MRN.isEmpty() ){ // MRN is valid
                MRN=MRN.toUpperCase()
                set(SharedConstants.HL7_Local_MRN_PID_Field, MRN)

            } else if(MRN.isEmpty()){  //Calling isEmpty() on a null object results in an exception
                log.error("Received Message DOES NOT contain a local MRN.  Ignoring this HL7 Message")
                return false
            } else {
                log.error("We should never see this.  Logic Error!")
                return false
            }

        }  else {
            log.error("Received Message containst NO local MRN.  Ignoring this HL7 Message")
            return false
        }

    // Set the MRN and RAMQ where the PIX Handler (ADT Handler) is expecting them
    set('PID-3-1', MRN)
    set('PID-2-1', AffinityPID)



/* Ensure that BOTH the Local MRN and the Affinity Domain (RAMQ) are fully qualified */

    def MRN_IssuerOfPID = get(SharedConstants.HL7_Local_MRN_Issuer_of_PID_Field)
    def AffinityDomain_IssuerOfPID = get(SharedConstants.HL7_Affinity_Domain_Issuer_of_PID_Field)

    log.debug("Current Issuer of Patient ID for MRN is: [{}]", MRN_IssuerOfPID)
    log.debug("Current Issuer of Patient ID for Affinity Domain PID (RAMQ) is: [{}]", AffinityDomain_IssuerOfPID)


    // Hard code Issuer of PIDs for both MRN and Affinity Domain PID

    // Hard Code Issuer of Patient ID for MRN
    if(null != MRN_IssuerOfPID){

        if(MRN_IssuerOfPID.isEmpty()) {  // If the Local Issuer of Patient ID at the PACS is empty, populate it from known value
            MRN_IssuerOfPID = SharedConstants.Local_PACS_Issuer_of_Patient_ID
            log.debug("Populating Issuer of Patient ID for MRN from Local PACS as [{}]", MRN_IssuerOfPID)

        } else if( SharedConstants.Local_PACS_Issuer_of_Patient_ID != MRN_IssuerOfPID.toUpperCase() ) { // This means that the Issuer of PID passed by RIS does not match the one known as correct
            /* THIS ONLY WORKS IN Connect Instances attached to SINGLE-CONTEXT PACS */
            log.warn("This RIS Feed is sending feeds for more than 1 local domain. \n Patient Lookups might fail and go to RIS Database!")

        } else {
            log.debug("The Issuer of PID matches what's expected")
        }

    } else {
        MRN_IssuerOfPID = SharedConstants.Local_PACS_Issuer_of_Patient_ID
        log.debug("Populating Issuer of Patient ID for MRN from Local PACS as [{}]", MRN_IssuerOfPID)
    }


    // Hard Code Issuer of Patient ID for Affinity Domain PID (i.e. RAMQ)
    if(null != AffinityDomain_IssuerOfPID) {

        if(AffinityDomain_IssuerOfPID.isEmpty() ){   //Populate with known value if empty
            AffinityDomain_IssuerOfPID = SharedConstants.HL7_Affinity_Domain_Issuer_of_PID_Field
            log.debug("Populating Issuer of Patient ID for AffinityDomain Patient (RAMQ) as [{}]", AffinityDomain_IssuerOfPID)
        } else if( SharedConstants.Affinity_Domain_OID != AffinityDomain_IssuerOfPID.toUpperCase() ){  // i.e. RAMQ Issuer Of PID is present, but not correct
            log.warn("Invalid Affinity Domain (RAMQ) Issuer of Patient ID [{}].  Populating to [{}]", AffinityDomain_IssuerOfPID, SharedConstants.Affinity_Domain_OID)
            AffinityDomain_IssuerOfPID = SharedConstants.Affinity_Domain_OID 
        } else {
            log.debug("The Affinity Domain Issuer of Patient ID is what is expected")
        }

    } else {
        AffinityDomain_IssuerOfPID = SharedConstants.Affinity_Domain_OID
        log.debug("Populating Issuer of Patient ID for AffinityDomain Patient (RAMQ) as [{}]", AffinityDomain_IssuerOfPID)
    }


    // Set the value of the MRN and AffinityDomain PID's Issuers of Patient ID where the PIX Server is expecting it

    set('PID-3-4-2', MRN_IssuerOfPID)
    set('PID-2-4-2', AffinityDomain_IssuerOfPID)



 // Tracing information

    // log.debug("This is the Object that will be passed to the Fetch Prior Studies Handler: {}", ouput)
