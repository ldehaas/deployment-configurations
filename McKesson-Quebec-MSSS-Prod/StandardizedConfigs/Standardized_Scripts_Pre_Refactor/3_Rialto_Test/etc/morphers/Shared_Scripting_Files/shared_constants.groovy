class SharedConstants {
    
    /* NOTE!: Please modify the value of "LocalIssuerIDs" in the "common_localization_routines.groovy".
    This is the only constant that needs to be changed in 2 places! */

    static final rialtoRetrieveAETitles = ['RIALTO_GAT_TST', 'RIALTO_HULL_TST']
    // You can find it in the Config file in the "Rialto_CSTORE_CSU_AETitle" variable, the Connectivity spreadsheet, or the SAD

    static final Local_Institution_Names = ['Hôpital de Gatineau', 'Hôpital de Hull']
    // Use the accented French characters

    static final Local_PACS_Issuers_of_Patient_ID = ['2.16.124.10.101.1.60.1.1008.1', '2.16.124.10.101.1.60.1.1009.1']
    // Assigning Authority of the Local PACS (i.e. the OID for the PACS)
    /* TODO  When you update this value, make sure to update the analogous value in the "common_localization_routines.groovy" file!! */

    static final Local_PACS_Calling_AE_Title = 'ALI_SCU'
    // This should be the Calling AE Title the PACS Query / Retreive SCU uses.
    //  Get this from the Connectivity Spreadsheet or the Solution Architecture Diagram

    static final Local_RIS_Sending_Facilities = ['RADIMAGE', 'RADIMAGE']
    // The HL7 Sending Facility for the Local RIS

    static final Local_RIS_Sending_Applications = ['GAT', 'HULL']
    // The HL7 Sending Facility for the Local RIS

    static final Affinity_Domain_OID = '2.16.124.10.101.1.60.100'
    // This is the Issuer of Patient ID for the Affinity Domain (i.e. the RAMQ)

    static final HL7_Sending_Application_Field = 'MSH-3-1'

    static final HL7_Sending_Facility_Field = 'MSH-4'

    static final HL7_Local_MRN_PID_Field = 'PID-3-1'
    // This is the Value in the PID Segment of an HL7 Message that includes the (unqualified) PID value of the Local MRN
    // 'PID-3-1' is the Standard

    static final HL7_Affinity_Domain_PID_Field = 'PID-2-1'
    // This is the value in the PID Segment of an HL7 Message that contains the (unqualified) PID value for the Affinity Domain (i.e. RAMQ)
    // 'PID-2-1' is the Standard

    static final HL7_Local_MRN_Issuer_of_PID_Field = 'PID-3-4-2'
    // This is the value in the PID Segment of an HL7 Message that contains the Issuer of Patient ID to qualify the Local MRN
    // 'PID-3-4-2' is the Standard

    static final HL7_Affinity_Domain_Issuer_of_PID_Field = 'PID-2-4-2'
    // This is the value in the PID Segment of an HL7 Message that contains the Issuer of Patient ID to qualify the Affinity Domain
    // 'PID-3-4-2' is the Standard

    static final HL7_Order_Modality_Field = 'OBR-24'
    // This is the Field in an HL7 Order where we can expect the Modality of an incoming order to be populated

    static final HL7_Order_Anatomic_Region_Field = 'OBR-15-4'
    // This is the field in an HL7 Order where we can expect the Anatomic Region to be populated

    static final XDS_Coding_Scheme_for_Anatomic_Region = "Imagerie Qu\u00e9bec-DSQ"
    //static final XDS_Coding_Scheme_for_Anatomic_Region_Array = ["Imagerie Québec-DSQ", "Imagerie Quebec-DSQ", "Imagerie Qu?bec-DSQ"]
    // The Coding Scheme in the XDS Event Code List that specifies an Anatomic Region

    static final XDS_Coding_Scheme_for_Modality = 'DCM'
    // The Coding Scheme in the XDS Event Code List that specifies a Modality

    static final Unwanted_Modalities_in_AdHoc_CFIND_Responses = ["KO", "SC", "SR"]
}
