/* CFind To XDS Find Documents (ITI-18)  Script
*
*   (Shared in both the Ad-Hoc Query and Fetch Prior Studies workflow)
*
*   This script takes a C-Find and Converts it to an XDS ITI-18 Registry Stored Query.  The C-Find can either come directly from the PACS (as part of an Ad-Hoc Query), or from
*       a Connect internal workflow (as part of a Fetch Prior Studies)
*
*
*   For Hopital du Sacre Coeur in Montreal, the script is currently doing the following:
*       1) Enforcing that an MRN is present in the C-FIND Query
*       2) Setting the correct **LOCAL** Patient ID and Local PACS Issuer of Patient ID in the XDS "Find Documents" Query  -- Java code will convert local PID to Regional PID
*/


/* ========= Shared Files Loading ============= */
    LOAD("../Shared_Scripting_Files/shared_constants.groovy")
     // Contains commonly-used constants as attributes of the "SharedConstants" class
     // For example: SharedConstants.rialtoRetrieveAETitle  (see the file for full details)



/* ================  Script  ================  */

 // [debug] Printing RAW DICOM C-Find Message
    log.debug("Printing RAW DICOM C-Find Message received:\n {}", input)


 /* Enforce that an MRN is present in the Query */
    // obtaining properties from cfind
    // As per the Solution Architecture Document, all queries *must* have a local MRN -whether it exists in the PACS or the RIS DB
    def pid = get(PatientID)

    if(null == pid) {
        log.error("DICOM C-FIND to XDS \"Find Documents\" Query Conversion:\n Patient ID in incoming C-Find was NULL.  Need a local Patient ID to work.\n")
        return false
    } else {
        pid = pid.toUpperCase()
    }



 /* Set the correct **LOCAL** Patient ID and Local PACS Issuer of Patient ID in the XDS "Find Documents" Query */

    // Evaluate the Calling AE Title
    def callingAE = getCallingAETitle()

    callingAE = callingAE.trim()
    callingAE = callingAE.toUpperCase()
    log.debug("Ad-Hoc C-Find: Calling AETitle initiating C-Find was \"{}\"", callingAE)


    def issuer = null

    if(SharedConstants.Local_PACS_Calling_AE_Title == callingAE){
        // Explicitly set the Issuer of PID to that of the local PACS for PIX lookup to work.  This is for when the local PACS does an Ad-Hoc Query
        issuer = SharedConstants.Local_PACS_Issuer_of_Patient_ID
        log.debug("This is an Ad Hoc Query from the PACS with a legitimate Calling AETitle")

    } else if ('PROXY_SELF' == callingAE) {
    // This case is for Fetch Prior Studies: when Rialto Connect calls itself
        log.debug("DICOM C-Find to XDS \"Find Documents\" Query Conversion: This is a Fetch Prior Study, attempting to Read IssuerOfPatientID")

        issuer = get(IssuerOfPatientID)
        // This IssuerOfPatientID was explictly populated in the 'HL7 to C-Find' script, since this is a "Fetch Prior Studies"
        log.debug("DICOM C-Find to XDS \"Find Documents\" Query Conversion: IssuerOfPatientID is {}", issuer)

    } else {
        log.error("C-Find to XDS SCRIPT: UNEXPECTED Calling AE Title [{}]received.  1) Unauthorized SCU AE Title (check script) or" +
            "2) Connect cannot ascertain whether the Order to find studies came from the PACS or through an internal Fetch Prior Studies!", callingAE)
        return false
    }


    // Setting Local PID in XDS 'Find Documents' Query  -> The Java code will run a PIX query and replace it by Affinity PID
    if(null != pid && null != issuer) {
        set(XDSPatientID, pid, issuer)
    } else {
        log.error("DICOM C-Find to XDS \"Find Documents\" Query Conversion:  either Patient ID or Issuer of Patient ID are NULL.")
        return false
    }



 /* AD HOC QUERY **ONLY**: During an Ad Hoc Query the local PACS User can constraint a query by age by populating their desired **MINIMUM** study age in the "Study Date"
    and "StudyTime" DICOM fields.
    When populating this "Study Date" field in their C-Find, they are basically overloading that field to carry the age constraint they want.
    NOTE: This SHOULD *NOT* be used in Fetch Prior Studies, as the XDS Registry will only give back studies that are at least as old as the ORM Order.
        This will make the FPS irrelevant (the order will normally be the newest item in the Patient History).        */
    import org.joda.time.DateTime

 // Constrain Study Date for Ad Hoc Query if passed in overloaded DICOM "Study Date" field
    if(SharedConstants.Local_PACS_Calling_AE_Title == callingAE){  // This is an Ad-Hoc Query from PACS
        def dicomDate = get(StudyDate)
        def dicomTime = get(StudyTime)
        def constraintByDate = false

        //TODO ensure that DICOM time is correctly formatted when passed
        if (null != dicomTime){
            if (null == dicomDate) { // Ensure date is at least today
                def now = new DateTime()
                dicomDate = String.format("%d-%02d-%02d", now.year, now.monthOfYear, now.dayOfMonth)
            }

            constraintByDate = true

        } else {
            if (null != dicomDate) { // DICOM Time is null but the Date is valid
                constraintByDate = true
            }
            //TODO validate passed date format?  needed?
        }
        // If both dicomDate and dicomTime are null, we will not constraint the Registry Query by Study Date / Time

        if(constraintByDate) {
        // Constrain the XDS Query by date/time if a value was passed in the C-Find
            set(XDSServiceStartTime, dicomDate, dicomTime)
        }
    }


/*  For an Ad-Hoc Query, Constrain the Modalities in Study based on what is passed in the DICOM C-FIND */
    if(SharedConstants.Local_PACS_Calling_AE_Title == callingAE){

        def modalitiesInStudy = getList(ModalitiesInStudy)
        def xdsEventCodes = []

        if (modalitiesInStudy != null) {
            if( ! modalitiesInStudy.isEmpty() ){    // x.isEmpty() might throw an exception when it evaluates on a null object

                modalitiesInStudy = modalitiesInStudy as Set
                    // Make the list of modalities a list of unique values

                for(eachModality in modalitiesInStudy){ xdsEventCodes.add( code(eachModality, "DCM") )}
                    // Add each value of the modalitiesInStudy to the XDS Event Codes

                log.debug("Setting Modalities constraint to: {}", modalitiesInStudy)

                // Set the correct XDS Event Codes for the ITI-18 Query

                set(XDSEventCode, xdsEventCodes)

            }
        }

    } // If no modalities in study are passed in the C-Find, this is not used as a Registry Query constraint




// [debug] Debugging the XDS Find Documents Query being created
 log.debug("This is the current XDS Query being created: \n{}", output )
