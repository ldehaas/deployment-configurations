/* Patient C-Find to PACS *SET UP*  Morpher
*
*   (Shared in both the Ad-Hoc Query and Fetch Prior Studies workflow)
*
*   This script **SETS UP THE C-FIND QUERY** that Rialto Connect will use to find out the Local Patient demographics from the local PACS.
*       Sometimes, the PACSs need to match incoming studies for something other than Accession Number or Study Instance UIDs in order to
*       assign the study to the correct context.  (Otherwise the study might go to the "penalty box" and not be associated with the
*       correct local Patient.
*       In more extreme cases, mismatched patient demographics from the DIR will actually overwrite the local information (at the
*       general Patient Record Level) in the PACS.  This script is also useful to avoid those situations.
*
*   The script runs only ONE TIME per study (that is, it won't get called again for every series or SOP that Connect is localizing)
*       The whole point of the script is to set up the C-Find request to the PACS.  Once the PACS replies, its response will be 
*       available as the 'localDemographics' variable in the "Foreign Image Localizer" script.
*       The variable will also be available for use in the "Image to HL7 Order Morpher" script.
*
*   The script takes as input the basic C-Find query that Rialto would have done to the PACS (which only contains the [Local] PatientID as query
*       constraint).  In here add any other return keys that the PACS will need to match studies to assign them to the correct
*       context and Patient.
*       To include return keys, simply set them as an empty string.  For example:  set(DicomField, "")
*
*   Inside the '../Shared_Scripting_Files' Directory, there is a 'local_PACS_demographics_methods.groovy' file.  It contains several methods that all 
*       read values and information from the 'localDemographics' variable.  These methods are available for any script that needs the local PACS
*       demographics, namely: the Foreign Image localizer, the SR to ORU converter, and the Image to HL7 Order morphers.
*
*   NOTES: 
*       1) Please remember that this script only **SETS UP** the C-Find return keys that the PACS will return to Connect.
*           That is, it only sets up the C-Find query.
*
*       2) Returning 'false' in the script causes Connect NOT to issue a C-Find to the local PACS for this Study.
*
*       3) This script is optional.  If not listed in rialto.cfg.xml, Connect will NOT attempt to query the Local PACS for local Patient Demographics.
*
*
*   Currently, the script is doing the following:
*       1) Copying over the only query constraint for the Patient Demographics Query (Patient ID)
*       2) Requesting the following key demographics as return keys:
*           a) Patient Name
*           b) Patient's Date of Birth
*
*/


/* =========  Constant Declaration  ================ */
 LOAD("../Shared_Scripting_Files/shared_constants.groovy")
 // Contains commonly-used constants as attributes of the "SharedConstants" class
 // For example: SharedConstants.rialtoRetrieveAETitle  (see the file for full details)


/* ================  Script  ================  */

 log.debug("\n** Starting \"Patient C-Find to PACS\" Morpher --Creating the C-Find to query for local PACS demographics **")
 log.debug("Current Input received by the Script (i.e. base C-Find): {}", input)


 if (get(IssuerOfPatientID) == SharedConstants.Local_PACS_Issuer_of_Patient_ID){
 // The Local Patient is in the cache, to correctly query the PACS (should be most of the time)

    // Copying the Patient ID Query Constraint over
        set(PatientID, get(PatientID) )  // This is not actually needed.  It just shows that we are querying by Patient ID

        remove(IssuerOfPatientID)
            // Remove the Issuer of Patient ID passed in the Java code. Some PACS will fail the query if Issuer Of PID is specified

    /* Remember to only configure as return keys what the PACS actually needs to match studies to a Context.
    /* Setting the local demographics needed by the PACS */

        set(PatientName, "")
        // Patient Name is needed from the PACS

        set(PatientBirthDate, "")
        // Match the Patient Date of Birth as well

    log.debug("C-Find that will be used for querying local PACS Demographics: {}", output)


 } else {
 // The local Patient ID is not in the cache (Cache has expired or we received an unexpected C-MOVE request)
 // Short-circuit the C-FIND to PACS

    log.warn("Skipping C-FIND to PACS to get local Patient Demographics. " +
        "The local Patient ID is not in the cache (Cache has expired or we received an unexpected C-MOVE request)")

    return false
 }
