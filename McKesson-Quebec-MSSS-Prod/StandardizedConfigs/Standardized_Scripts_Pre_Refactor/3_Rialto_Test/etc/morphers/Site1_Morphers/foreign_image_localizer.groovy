/* Foreign Image Localizer
*
*   (Shared in both the Ad-Hoc Query and Fetch Prior Studies workflow)
*
*   This script takesi the DICOM header for an image or a report, and modifies header data so that a local PACS can:
*       a) Accept the foreign study (this normally means using the / alocal MRN number)
*       b) Recognize it as unique (this means that it won't collide with Local PACS' accession numbers; in practice this means prefixing the 
*           foreign accession numbers)
*       c) "Normalize" the Procedure Codes.   This means to make the foreign images as "native" as possible by treating similar image type in a very similar
*               way as the PACS would treat a study that it got locally.  The hanging protocols specify how to display images.  The hanging protocols
*               are native to the PACS and change according to Procedure Codes.
*
*
*   Currenlty, for HSC, the script is doing the following:
*       1) Localizing the Patient ID
*       2) Adding the Local PACS demographics, if present 
*       3) Localizing the Accession Numbers (Prefixing foreign accession numbers)
*       4) Set Normalized Procedure Codes
*       5) Setting Rialto's RetrieveAETitle on all the SOPs, so that the PACS can access them (this is not as important here as in the CFind Response)
*           but it's done for consistency
*
*/

/* ========= Shared Files Loading ============= */
    LOAD("../Shared_Scripting_Files/shared_constants.groovy")
    // Contains commonly-used constants as attributes of the "SharedConstants" class
    // For example: SharedConstants.rialtoRetrieveAETitle  (see the file for full details)

    LOAD("../Shared_Scripting_Files/common_localization_routines.groovy")
    /* Load the shared methods that allow:
        a) Using either a Patient ID Local to the PACS, or a prefixed foreign Patient when the Patient does not exist in the PACS
        b) The Procedure Codes tables to normalize the hanging protocols at the local PACS      */

    LOAD("../Shared_Scripting_Files/shared_xsd_modality_and_anatomic_region_routines.groovy")
    /* Load the shared methods that allow:
        a) Working with the XDS Procedure Codes available to the Java code
        b) Dermine Modalities and Anatomic Regions
        c) Use the Modalities and Anatomic Regions above to map the correct Procedure Codes for a Study */

    LOAD("../Shared_Scripting_Files/procedure_codes_rules_and_logic.groovy")
    /* Load:
        a) The Procedure Code Mapping rules specific for this sites
        b) The logic that matches the Procedure Code Mapping rules to a passed modality and anatomic region */




/* ================  Script  ================  */

 // Define General Variables
    def studyInstanceUID = get(StudyInstanceUID)
    def sopInstanceUID = get(SOPInstanceUID)

 // Define the localization variables
    def sourcePid = get(PatientID)
    def sourceIssuer = get(IssuerOfPatientID)
    def localPidAtPACS = Pids.localize([sourcePid, sourceIssuer], getAllPidsQualified())
        // "localPidAtPACS" will get an array of Patient IDs, whose array value 0 will always be the local patient, or a prefixed foreign patient
        // "Pids" is a Class obtained from common_localization_routines.groovy
        // "localize" is a method that returns either a local patient or a prefixed foreign patient
        // "getAllPidsQualified" gets the OtherPatientIDSequence plus the current PatientID and IssuerOfPatientID from DICOM (or overloaded value in Java)
    def sopModality = get(Modality)


 // [debug] Printing Tracing Information
    log.debug("\n\n\n*** Starting Foreign Image LOCALIZER Script *** :\n")
    log.debug("FOREIGN Accession Number is: \"{}\".", get(AccessionNumber))
    log.debug("Processing SOP [{}] for Study Instance UID [{}].", sopInstanceUID, studyInstanceUID)
    log.debug("Source pid in C-Move response from the DIR before morphing: {}", sourcePid)
    log.debug("Source Issuer of PID in C-Move response from the DIR before morphing: {}", sourceIssuer)
    log.debug("Setting local or prefixed pid in CStore: {}", localPidAtPACS[0])
    log.debug("This is the current content of the \"Other Patient IDs\" Sequence: {}", getAllPidsQualified() )
    // TODO Remember to set log4j to trace in this category to see this if you need it
    log.trace("This is the full object as received by the script: {}", input)


 /* As a fail-safe, ignore local studies at the SOP Level */
 if(sourceIssuer == Constants.PrimaryLocalIssuerID && sourcePid == localPidAtPACS[0] ) {
    log.info("This is a local SOP for a local Study. Not moving to PACS.  Accession Number [{}], Study Instance UID [{}], SOP Instance UID [{}]", get(AccessionNumber), studyInstanceUID, sopInstanceUID)
    return false
 }
 

 /* Sink KO SOPs */
 sopModality = sopModality.toUpperCase()
 if(sopModality != null) {
    if(sopModality == "KO") {
        log.info("Dropping a KO SOP")
        return false
    }
 }

 /* Localize the Patient ID     */
    set(PatientID, localPidAtPACS[0])
        // set the Patient ID in the Image (SOP) header to the local or prefixed foreing PID

    remove(IssuerOfPatientID)
        // the PACS normally doesn't need the IssuerOfPatientID field populated for local patients; foreign patients are prefixed


 /* Add the additional Patient Demographics from the Local PACS, so the study can be matched to the correct context*/

    def localPACSpatientName = null
    def patientDateofBirthatDIR = get(PatientBirthDate)
    def localPACSpatientDOB = null

    if(localDemographics != null && !(localDemographics.isEmpty()) ) {
        localPACSpatientName = localDemographics.get(0).get(PatientName)
        localPACSpatientDOB = localDemographics.get(0).get(PatientBirthDate)
    }

    // Use the local Patient Name if it was returned from the PACS C-Find
    if(null != localPACSpatientName && !(localPACSpatientName.isEmpty()) ) {
        if(! localPACSpatientName.isEmpty() ){
            set(PatientName, localPACSpatientName)
            log.debug("Setting Patient Name to Local PACS Demographic Patient Name: {}", localPACSpatientName)
        }
    }

    // Log an error if the Patient Date of Birth if it was returned from the PACS C-Find is different for the DOB value @ DIR
    if(null != localPACSpatientDOB && ! localPACSpatientDOB.isEmpty() ) {
        if(! localPACSpatientDOB.isEmpty() ){
            if(patientDateofBirthatDIR != null && !(patientDateofBirthatDIR.isEmpty()) ){

                if(patientDateofBirthatDIR != localPACSpatientDOB) {
                    log.error("The Date of Birth [{}]for Patient \"{}\" in the PACS does not match what's in the DIR ({}). " +
                    "This study will likely go to the Penalty Box at the PACS", localPACSpatientDOB, localPidAtPACS[0], patientDateofBirthatDIR)
                }
            }
        }
    }


 /* Prefix Foreign Accession Numbers    */

    if( null == sourceIssuer || "" == sourceIssuer ){ 
        log.warn("Empty Assigning Authority (IssuerOfPatientID) [{}] at DIR. The Accession Number will be prefixed as Foreign, and unexpected", sourceIssuer) 
    }

    def prefix = Prefixes.getPrefix(sourceIssuer) 
    // Prefixes.getPrefix() is a method in the "../Shared_Scripting_Files/common_localization_routines.groovy" file that returns the correct prefix 
    if(prefix == "AU" || prefix == "in") {                                                            
        log.warn("Unknown Site OID Mapping for \"{}\". The prefixing will not be what is expected!", sourceIssuer)
    } else {
        log.debug("Current prefix to prepend to Accession Number is: \"{}\"", prefix)
    }


    // Prepend the "localizer" prefix to the Accession Number 
    prepend(AccessionNumber, prefix) 
        // The 'prepend' method directly modifies the output field --i.e. as if running a set() with a prefix

    def prefixedAccessionNumber = output.get(AccessionNumber)
    if(prefixedAccessionNumber != null) {
        if(prefixedAccessionNumber.size() > 16) {
            log.warn("Warning: the prefixed accession number \"{}\" for Study \"{}\", SOP \"{}\" exceeds " +
                "16 characters.  Undefined behaviour might be seen at the PACS",prefixedAccessionNumber, studyInstanceUID, sopInstanceUID)
        }
    }
    log.debug("Prefixed Accession Number will be \"{}\".", prefixedAccessionNumber)


 /* Prefix Study IDs */
    prepend(StudyID, prefix)



 /* Populate Standardized Procedure Codes from Modalities, Anatomic Regions and Foreign Procedure Descriptions */

    // Get the set of Event Codes for this Study from the XDS Registry
    def XDS_Modalities_and_Anatomic_Regions = xdsMetadata.get(XDSEventCodes)
        log.debug("\n**Starting Procedure Codes mapping for this SOP****\n")
        log.debug("These are the raw XDS Event Codes retreived from the Registry for this study: +{}+", XDS_Modalities_and_Anatomic_Regions)

    //TODO this is Scheme name debugging
    log.trace(" ***********************  Character Set Debugging  ***************** ")
    xdsMetadata.get(XDSEventCodes).each { log.trace("Code points for received event code scheme '{}': {}", it, it.getSchemeName().collect { it.codePointAt(0) }) }

    log.trace("Code points in schemeName \"{}\": {}", SharedConstants.XDS_Coding_Scheme_for_Modality, SharedConstants.XDS_Coding_Scheme_for_Modality.collect { it.codePointAt(0) })
    log.trace("Code points in schemeName \"{}\": {}", SharedConstants.XDS_Coding_Scheme_for_Anatomic_Region, SharedConstants.XDS_Coding_Scheme_for_Anatomic_Region.collect { it.codePointAt(0) })
    

    log.trace("Code points in LITERAL TYPED IN AT GROOVY WITH NO ESCAPE CODES schemeName \"Imagerie Québec-DSQ\" " +
                "which we are looking for: {}", "Imagerie Québec-DSQ".collect { it.codePointAt(0) })
    log.trace(" *********************** \\Character Set Debugging  ***************** ")


    // Determine the Anatomic Region from the XDS Event Codes
    def SOPanatomicRegion = Modalities.determineAnatomicRegion(XDS_Modalities_and_Anatomic_Regions, SharedConstants.XDS_Coding_Scheme_for_Anatomic_Region) 
        log.debug("Current Anatomic Region in SOP is: {}", SOPanatomicRegion)
        

    // Determine the Dominant Modality for this Study using the Hierarchical List and the XDS Event Codes

        // Determine the Modalities from the XDS Event Codes
        /* NOTE!!: These modalities will include substitutions from "illegal" or "unwanted" modality types */
        def StudyModalities =  Modalities.getCodeValues(XDS_Modalities_and_Anatomic_Regions, SharedConstants.XDS_Coding_Scheme_for_Modality)
            log.debug("These are the SUBSTITUTED modalitities in the XDS Registry for this Study: \"{}\"", StudyModalities)
/*
        // Substitute unwanted modality types for standardized modality types
        StudyModalities = StudyModalities.collect { original ->
            def substitute = Modalities.secondaryToPrimaryModalitiesMap[original]
            if (substitute != null && !substitute.isEmpty()) {
                return substitute;
            }

            return original;
        }
*/

        def dominantModalityInThisStudy = null


        if(StudyModalities.size() > 1) {
        // Determine Dominant Modality for a multi-modality study

            log.debug("There is more than 1 modality currently for this SOP, disambiguating.")
            dominantModalityInThisStudy = Modalities.determineDominantModality(XDS_Modalities_and_Anatomic_Regions, SharedConstants.XDS_Coding_Scheme_for_Modality)
                // Determine the Dominant Modality for a given study based on the Hierarchical Modality list
                // This is the case we would want to see the most

            if ( dominantModalityInThisStudy == "") {
            // No Dominant Modality could be found
                dominantModalityInThisStudy = StudyModalities[0]
                log.warn("No dominant modality could be found for Study Instance UID \"{}\", SOP Instance UID \"{}\"" +  
                    "Falling back to first published modality \"{}\"", studyInstanceUID, sopInstanceUID, StudyModalities[0])
            }
        
        } else if(StudyModalities != []) {
            // This is a single modality study
            dominantModalityInThisStudy = StudyModalities[0]
            log.debug("Single modality study.  Study Instance UID \"{}\", SOP Instance UID \"{}\"", studyInstanceUID, sopInstanceUID)
            
        } else {
            dominantModalityInThisStudy = get(Modality)    
            log.warn("Unknown XDS Study Modalities for Study Instance UID \"{}\", SOP Instance UID \"{}\"" +
                + "\nFail-safing to Current SOP Modality in DICOM: \"{}\"", studyInstanceUID, sopInstanceUID, dominantModalityInThisStudy)
        }

        log.debug("\n\n ** After the Modality Checking Logic, Dominant Modality for this Study is: \"{}\"\n\n", dominantModalityInThisStudy)



    // Get the study description from XDS, otherwise from DICOM
    def studyDescription = null
    
        // Get studyDescription from XDS Metadata
        studyDescription = xdsMetadata.get(XDSTitle) as String
            log.debug("Current Study Description from XDS Metadata is: {}", studyDescription)
    
        // If no valid data available in the XDS Registry, get values from DICOM
        if (studyDescription == null || studyDescription.isEmpty()) {

            studyDescription = get(StudyDescription) 
            log.debug("No value found in the XDS Metadata.  Using StudyDescription \"{}\" in DICOM header.", studyDescription)
        }

    // If the studyDescription is not found in either the XDS Registry or the DICOM header,
    //  set the studyDescription to empty string so as not to pass garbage to the rule-evaluating logic
    if (studyDescription == null || studyDescription.isEmpty()) {
    
        studyDescription = ""

    } else {
        studyDescription = studyDescription.toLowerCase()
            // The rule evaluation mechanism needs the studyDescription in lower case
    }


    // These variables will take the value returned from the Procedure Code Mapping logic
    def normalizedProcedureCode = null
    def normalizedProcedureDescription = null
    

    // Find the Rule that applies to the current Modality, Anatomic Region and Study Description combination

    log.info("Values of the 3 variables were are passing to the \"Find Matching Rule\" function:" +
        "\"{}\", \"{}\", \"{}\"", dominantModalityInThisStudy, SOPanatomicRegion, studyDescription)

    def ProcedureCodeMappingRule = Rules.findMatchingRule(dominantModalityInThisStudy, SOPanatomicRegion, studyDescription)
        /* As per the "findMatchingRule" fuction in the "procedure_codes_rules_and_logic" file, 
            ProcedureCodeMappingRule[3] will be the Procedure Code; 
            ProcedureCodeMappingRule[4] will be the Procedure Description   */

    log.debug("This is the content of the normalizedProcedureCode array:\n {}", ProcedureCodeMappingRule)

    if(ProcedureCodeMappingRule != null) { 
        normalizedProcedureCode = ProcedureCodeMappingRule[3]
        normalizedProcedureDescription = ProcedureCodeMappingRule[4]
    }

    // Populate the Normalized Procedure Code and Procedure Description
    if(null != normalizedProcedureCode && null != normalizedProcedureDescription) {
        set(StudyDescription, normalizedProcedureCode)
            // the PACS needs it here
        log.debug("Setting the Procedure Code in the outgoing SOP to PACS to: {}", normalizedProcedureCode)

        /* According to the last review with the Client, they did not want to set a study description
        //set(StudyDescription, normalizedProcedureDescription)
        log.debug("Setting the Procedure Description in the outgoing SOP to PACS to: {}", normalizedProcedureDescription) */

    } else {
        log.warn("NORMALIZED Procedure Code and / or Procedure Code Description are blank.  NOT passing procedure code values to the PACS." +
        "\nStudy Instance UID: {}, SOP Instance UID: {}", get(StudyInstanceUID), get(SOPInstanceUID) )
    }


    /* This logic is to prime the DICOM SR to ORU conversion */
    // Pass the original "Foreign" Procedure Code and Procedure Description so that the SR can use it
    def foreignProcedureCode = get('ProcedureCodeSequence/CodeValue')
        //TODO we might have to make a map based on foreign sites (can leverage the Accession Number prefixes to
        // populate where to get the field that actually stores the Foreign Procedure Code at each site

    def foreignProcedureDescription = get(StudyDescription) 
    def SOPmodality = get(Modality)

    if(SOPmodality != null) {
        if(SOPmodality.toUpperCase() == "SR") {
            set(AccessoryCode, foreignProcedureCode)
            set(AcquisitionComments, foreignProcedureDescription)
                // The fields that we are using to pass the original Procedure Code and Procedure Description are 
                // totally arbitrary.  The only prerequisite is that they are NOT used to construct the ORU
                // Make sure that you pick these arbitrary fields up in the ORU conversion

            set(StudyDescription, normalizedProcedureDescription)
            set(CodeValue, normalizedProcedureCode)
            // The SR to ORU script is currently expecting the values in these arbitrary fields 
        }
    }

    

 /*  Populate Institution Name according to the Assigning Authority (Issuer of Patient ID) of the Source PID */
   def inferredInstitutionName = Prefixes.getInstitutionName(sourceIssuer) 

   if (inferredInstitutionName != "Institution Inconnue") {
    // We found a known InstitutionName
    // Unfortunately, this value is very dependent on what's on the Prefixes class. If that changes, it will break this if statement and its code
   
        set(InstitutionName, inferredInstitutionName)
        log.debug("Setting Institution Name from Rialto's table as \"{}\"", inferredInstitutionName)

   } else {
     // Fall back on the DICOM value if we don't know the institution name
        log.warn("We did not know the insitution name for Study \"{}\", SOP \"{}\".  Falling back on the " +
            "original value at the DIR:\"{}\"", studyInstanceUID, sopInstanceUID, get(InstitutionName))
        // You could say 'set(InstitutionName, get(InstitutionName))' but it would be redundant
   }



 /* Setting the Retreive AETitle to Connect itself, so that the PACS goes through Connect */

    // Outputting the DIR's retreive AETitle for debugging purposes
    log.trace("Retreive AETitle at DIR is : \"{}\"", get(RetrieveAETitle))

    // Setting Rialto's AE as the RetrieveAETitle
    set('RetrieveAETitle', SharedConstants.rialtoRetrieveAETitle)
    log.debug("Current Retreive AETitle after morphing is: \"{}\"", output.get(RetrieveAETitle))



 /* If the actual SOP modality is one of the "forbidden" modalities, substituted to an expected modality */
    
    def subbedSOPmodality = Modalities.secondaryToPrimaryModalitiesMap[SOPmodality]
    def reportedModality = SOPmodality

    if(subbedSOPmodality != ""){
        set(Modality, subbedSOPmodality)
        reportedModality = subbedSOPmodality
        log.info("Changed SOP modality from original unwanted modality of [{}] to substituted modality of [{}]", input.get(Modality), subbedSOPmodality)
    }



 /*  Outputting Information on the SOP being moved to the PACS */
 log.info("Preparing to move localized SOP to PACS.  Accession Number [{}], Patient ID [{}], Foreign Institution [{}], Study Instance UID [{}], " +
    "SOP Instance UID [{}], SOP Modality [{}]", output.get(AccessionNumber), localPidAtPACS[0], inferredInstitutionName, studyInstanceUID, sopInstanceUID, reportedModality)

/* [debug] Printing Tracing Information  */
    //    log.debug("This SOP localized is for Study -- Localized Accession Number: [{}], Study Instance UID [{}]", prefixedAccessionNumber, studyInstanceUID)

    log.trace("Image (SOP) headers in outgoing C-Store to Local PACS (after morphing):\n{}", output)
    log.debug("\n\n\n*** Ending Foreign Image LOCALIZER Script *** :\n")
