/* HL7 to CFind Morpher
*
*   Takes an incoming RIS HL7 ORM order, and converts it to a DICOM C-Find Query Request.
*
*   This script is part of the 'Fetch Prior Studies' workflow.  It takes the incoming HL7 ORM order, and translates it into a DICOM C-Find.
*   Later on in the workflow (but not in this script), the DICOM C-Find that this script outputs is further processed by Connect, and transformed into and 
*   XDS Find Documents query by using the 'CFind to Find Documents Morpher'.
*
*   If you want to filter results for --ONLY-- for the Fetch Prior Studies workflow (without affecting Ad-Hoc Query / Retrieve),
*   populate the 'CFind Response Filter', which only affects results for Fetch Prior Studies originally derived from this current, below morpher.
*   (HL7 to CFind Morpher).
*
*
*
*   For Hopital du Sacre Coeur in Montreal, the high level workflow in this script, currently is:
*       1. Check to see if the HL7 ORM order has a valid prefetch order status.  Reject if not.
*       2. Populate Local Affinity Domains (Missing in ORM feed)
*       3. TODO Populate the Modality in the HL7 ORM order to the "Modalities in Study" of the C-Find
*       4. TODO Populate the Anatomic Region in the HL7 ORM order to the of the "Body Part Examined" of C-Find
*/

/* ========= Shared Files Loading ============= */
    LOAD("../Shared_Scripting_Files/shared_constants.groovy")
        // Contains commonly-used constants as attributes of the "SharedConstants" class
        // For example: SharedConstants.rialtoRetrieveAETitle  (see the file for full details)

    LOAD("../Shared_Scripting_Files/fetchpriors_logic.groovy")
        // Contains constants and methods used for Fetch Priors logic.  Including Valid Order Statuses


/* =========  Constant Declaration  ================ */
 def Silence_Noisy_RIS_with_no_Sending_Facility = false
 

/* ================  Script  ================  */

 // Tracing Info
    log.trace("Received HL7 Order: {}", input)

 // These are the Key Fields in an incoming ORM Order
    def OrderAccessionNumber = get ('ORC-3')
        // Unique Identifier for an Order.  Also known as the "Order Filler Number"

    def MRN = get(SharedConstants.HL7_Local_MRN_PID_Field)
        // Local PID

    def AffinityPID = get(SharedConstants.HL7_Affinity_Domain_PID_Field)
        // RAMQ

    def ModalityOfCurrentOrder = get(SharedConstants.HL7_Order_Modality_Field) 

    def currentAnatomicRegion = get(SharedConstants.HL7_Order_Anatomic_Region_Field)

    def ExamProcedureCode = get('OBR-4-1')
        // The Procedure Code is also the "Exam" that is being performed

    def OrderStatus = get('ORC-16')
            if(OrderStatus != null) { OrderStatus = OrderStatus.toUpperCase() }

    def OrderControlCode = get('ORC-1')

    def OrderKeyFields = [OrderAccessionNumber, ExamProcedureCode, OrderStatus, OrderControlCode]


 // Log the Key Fields in Incoming Order
    log.info("FPS: Received HL7 Order. Accession Number: \"{}\", MRN: \"{}\", RAMQ: \"{}\", Modality: \"{}\", Anatomic Region: \"{}\", Exam Procedure Code: \"{}\", Order Status: \"{}\", " +
        "OrderControlCode:\"{}\".", OrderAccessionNumber, MRN, AffinityPID, ModalityOfCurrentOrder, currentAnatomicRegion, ExamProcedureCode, OrderStatus, OrderControlCode)

    
 // Only process orders for which the Order Status is a valid trigger for Fetch Prior Studies 
    if( ! PriorsLogic.ValidOrderStatuses.contains(OrderStatus) ) {
        log.info("Received an HL7 Order with a status of \"{}\".  Not a value expected to cause a Fetch Prior Studies. Ignoring", OrderStatus)
        return false
    }


 // Only process orders whose Modality is known and valid
    if(! PriorsLogic.ValidModalities.contains(ModalityOfCurrentOrder)){
        log.error("Unknown Modality \"{}\" for Order with Accession Number \"{}\", RAMQ [{}], MRN[{}].  Ignoring.", ModalityOfCurrentOrder, OrderAccessionNumber, AffinityPID, MRN)
        return false
    }

 // Only process orders whose Anatomic Region is known and valid
    if(! PriorsLogic.ValidAnatomicRegions.contains(currentAnatomicRegion)){
        log.error("Unknown Anatomic Region \"{}\" for Order with Accession Number \"{}\", RAMQ [{}], MRN[{}].  Ignoring.", currentAnatomicRegion, OrderAccessionNumber, AffinityPID, MRN)
        return false
    }

 // Only process orders whose Control Code should cause a prefetch
    if(PriorsLogic.InvalidFetchOrderStatuses.contains(OrderControlCode) ) {
        log.info("The Order Status of \"{}\" should NOT cause a prefetch.  Ignoring Order.", OrderControlCode)
        return false
    }

 // Copy the Patient ID to the C-FIND; derive the Issuer of Patient ID and populate it in the C-FIND

    def sendingFacility = get('MSH-4')
    def issuerOfPatientID = null
    def localMRN = get(SharedConstants.HL7_Local_MRN_PID_Field)

    set(PatientID, localMRN)
    log.debug("Current Patient ID used is [{}]", localMRN)

    // Set the local Affinity Domain so that the PIX mechanism can find the RAMQ
    //     currently missing in the RIS HL7 ORM Feed ??
 
    if (SharedConstants.Local_RIS_Sending_Facilities[0]  == sendingFacility){
        issuerOfPatientID = SharedConstants.Local_PACS_Issuers_of_Patient_ID[0] 
        log.debug("Fetch Prior Studies [HL7 ORM to DICOM C-FIND]: The PIX will look up an explicitly local patient from {}.", SharedConstants.Local_Institution_Name)
    
    } else {    // Sending Facility is not what is expected
    
        if(null != sendingFacility){

            if( ! sendingFacility.isEmpty() ){ // Sending Facility is from a Different Domain
            log.warn("Received unexpected Sending Facility (MSH-4) of \"{}\". Expected \"{}\".", sendingFacility, SharedConstants.Local_RIS_Sending_Facilities[0])
            log.warn("This RIS Feed is sending feeds for more than 1 local domain. Patient Lookups might fail and go to RIS Database!"
                            + " This could Potentially Resolve to a different, Local Patient!!")
            } else {    // Sending Facility is simply empty
                
                if(Silence_Noisy_RIS_with_no_Sending_Facility){ // Turn off this overly verbose message if the RIS doesn't regularly populate a Sending Facility
                    log.trace("Fetch Prior Studies: The MSH-4 Sending Facility in this HL7 ORM Message was blank!\nThe PIX will assume assuming this Patient is from {}", SharedConstants.Local_Institution_Name)
                } else {
                    log.warn("Fetch Prior Studies: The MSH-4 Sending Facility in this HL7 ORM Message was blank!\nThe PIX will assume assuming this Patient is from {}", SharedConstants.Local_Institution_Name)
                }
        
            }
        
        } else {  //sending Facility is null
        
            if(Silence_Noisy_RIS_with_no_Sending_Facility){ // Turn off this overly verbose message if the RIS doesn't regularly populate a Sending Facility
                log.trace("Fetch Prior Studies: The MSH-4 Sending Facility in this HL7 ORM Message was null!\nThe PIX will assume assuming this Patient is from {}", SharedConstants.Local_Institution_Name)
            } else {
                log.warn("Fetch Prior Studies: The MSH-4 Sending Facility in this HL7 ORM Message was null!\nThe PIX will assume assuming this Patient is from {}", SharedConstants.Local_Institution_Name)
            }
        
        }

        // In *ALL* Cases, after writing the appropriate message to log, the Issuer of Patient ID will be that of the Local PACS

            issuerOfPatientID = SharedConstants.Local_PACS_Issuers_of_Patient_ID[0] 
    }


    // Set the value for IssuerOfPatientID derived from the logic above
        set(IssuerOfPatientID, issuerOfPatientID)




 /* Set the Correct Modality to Retreive
    In this project the initial relevant set of priors is that which matches the same Modality and same Anatomic Region as the incoming ORM order */

    // Copy Current Modality in HL7 Order

    if(null != ModalityOfCurrentOrder) {
        set(ModalitiesInStudy, ModalityOfCurrentOrder)
        log.debug("Setting modalities in study to {}", ModalityOfCurrentOrder)

    } else {
        log.error("No Modality populated in the order.  Will pass the Fetch Priors unconstrained. " +
            "This will result in an usually large number of Priors fetched from the Registry.")
    }


    // Copy current Anatomic Region in HL7 Order


    if(null != currentAnatomicRegion) {
        set(BodyPartExamined, currentAnatomicRegion)
        log.debug("Setting Normalized Anatomic Region from HL7 to C-FIND as: {}", currentAnatomicRegion)

    } else {
        log.error("No Anatomic Region populated in the order.  Will pass the Fetch Priors unconstrained." +
            "This might result in an usually large number of Priors fetched from the Registry.")
    }


    if(null == ModalityOfCurrentOrder && null == currentAnatomicRegion){
        set(BodyPartExamined, null)
        set(ModalitiesInStudy, null)
        log.error("Missing either Anatomic Region or Modality in the order.  The Fetch Priors will be processed unconstrained." +
                    "\nThe Fetch Prior Studies order will be ignored.")

        return false
    }



 // Print Tracing Information
    log.debug("Fetch Prior Studies:  The is the Current C-Find being Generated: \n {}", output)
