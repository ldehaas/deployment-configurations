/*
*  Specific Site Forwarder: Only forward traffic meant for the Rialto Connect instance on Site1
*/

LOAD("../Shared_Scripting_Files/shared_hl7_constants.groovy")
LOAD("../Shared_Scripting_Files/shared_constants.groovy")

def expectedSendingApplication = SharedConstants.Local_RIS_Sending_Facilities[0]
log.trace("Current expectedSendingApplication is [{}]", expectedSendingApplication)

def expectedSendingFacility =  SharedConstants.Local_RIS_Sending_Applications[0]
log.trace("Current expectedSendingFacility is [{}]", expectedSendingFacility)

def actualSendingApplication = get(SharedConstants.HL7_Sending_Application_Field)
log.trace("Actual Sending Application from RIS Feed was: [{}]", actualSendingApplication)

def actualSendingFacility = get(SharedConstants.HL7_Sending_Facility_Field)
log.trace("Actual Sending Facility from RIS Feed was: [{}]", actualSendingFacility)

def actualFacilityMatchesExpectedFacility = false
def actualApplicationMatchesExpectedApplication = false

if(actualSendingFacility != null && actualSendingFacility == expectedSendingFacility) {
    actualFacilityMatchesExpectedFacility = true
    log.trace("actualFacilityMatchesExpectedFacility")
}

if(actualSendingApplication != null && actualSendingApplication == expectedSendingApplication) {
    actualApplicationMatchesExpectedApplication = true
    log.trace("actualApplicationMatchesExpectedApplication")
}


log.trace("Checking whether actualApplicationMatchesExpectedApplication: {}", actualApplicationMatchesExpectedApplication)
log.trace("Checking whether actualFacilityMatchesExpectedFacility: {}", actualFacilityMatchesExpectedFacility)

if(actualFacilityMatchesExpectedFacility && actualApplicationMatchesExpectedApplication) {
    log.trace("Forwarding HL7 message to Site1. {}", output)
} else {
    log.trace("There was no match.  Dropping Message.")
    return false
}
