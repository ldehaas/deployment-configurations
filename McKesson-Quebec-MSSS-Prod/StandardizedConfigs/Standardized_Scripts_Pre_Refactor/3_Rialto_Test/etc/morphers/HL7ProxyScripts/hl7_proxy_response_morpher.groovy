/*
hl7_proxy_response_morpher.groovy
        
    Modifies responses to be sent to the HL7 RIS Feeds by default
*/

def timestamp = get('MSH-7')

if(null != timestamp) {
    // Chop the subsecond part of the timestamp
    log.trace("Removing sub-second part of the time.  The previous timestap was \"{}\"", timestamp)
    timestamp = timestamp.replaceAll("\\..*", "")
    log.trace("The current timestap is \"{}\"", timestamp)
    set('MSH-7', timestamp)
}

log.trace("Outputting HL7 Proxy Response Message to Caller:\n{}", output)

