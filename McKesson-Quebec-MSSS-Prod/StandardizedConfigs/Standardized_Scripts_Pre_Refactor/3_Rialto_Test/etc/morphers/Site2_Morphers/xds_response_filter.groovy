/*
* These rules are specific to Jewish General Hospital (Hôpital General Juif de Montréal) 
* They were derived and modified from the original rules set up at HSC
*/

// This script will filter all priors against a set of rules that will determine
// what priors are relevant based on the new order.
// This runs immediately after the XDS Responses arrive from the XDS Registry, but before the DIR is queried.

/* ========= Shared Files Loading ============= */
    LOAD("../Shared_Scripting_Files/shared_constants.groovy")
        // Contains commonly-used constants as attributes of the "SharedConstants" class
        // For example: SharedConstants.rialtoRetrieveAETitle  (see the file for full details)



// This ensures that NO LOCAL STUDIES (i.e. only foreign studies) get processed
studies = studies.findAll { study -> study.get(XDSSourcePatientID).domainUUID != SharedConstants.Local_PACS_Issuer_of_Patient_ID }

/* This code would make it so that all studies, including locals would get processed
    (comment out the above line, which disables local studies)
studies = studies.findAll { study -> study.get(XDSSourcePatientID)}
*/


log.trace("These are the raw studies from the XDS Reg being passed to the rules morpher \n {}", studies)

log.debug("Checking {} priors", studies.size())

// weird hack to make the log variable accessible inside PriorsRules
// TODO: find a better solution to accessing global scope objects in classes
class Log {
    static log = null
}
Log.log = log
    // the log that gets assigned to Log.log is the variable that all Groovy scripts use to log
    //  e.g. log.trace(...)

/**
 * Rules table and evaluation logic for prior studies.
 *
 * For each modality and anatomical region pair, there are a set of sub rules.
 * Each subrule defines the modality and anatomic region of a candidate prior,
 * as well as the maximum age and maximum number of the prior.
 */
class PriorsRules {
    static log = Log.log

    private static rules = [

      //  Rules for CT and MR
      [ modality : [ "CT", "MR" ], anatomicRegion : "TC", subrules :
        [
          [ modality : [ "CT", "MR" ], anatomicRegion : "TC", maxAge : 0, newest : 0, oldest : 1 ],
          [ modality : [ "CT", "MR" ], anatomicRegion : "TC", maxAge : 4, newest : 4, oldest : 0 ],
        ],
      ],
      [ modality : [ "CT", "MR" ], anatomicRegion : "DI", subrules :
        [
          [ modality : [ "CT", "MR" ], anatomicRegion : "DI", maxAge : 0, newest : 0, oldest : 1 ],
          [ modality : [ "CT", "MR" ], anatomicRegion : "DI", maxAge : 4, newest : 4, oldest : 0 ],
          [ modality : [ "CT", "MR" ], anatomicRegion : "AB", maxAge : 3, newest : 3, oldest : 0 ],
          [ modality : [ "CR" ], anatomicRegion : "DI", maxAge : 3, newest : 3, oldest : 0 ],
          [ modality : [ "CR" ], anatomicRegion : "AB", maxAge : 3, newest : 3, oldest : 0 ],
          [ modality : [ "RF" ], anatomicRegion : "DI", maxAge : 3, newest : 3, oldest : 0 ],
          [ modality : [ "US" ], anatomicRegion : "AB", maxAge : 3, newest : 3, oldest : 0 ],
          [ modality : [ "US" ], anatomicRegion : "DI", maxAge : 3, newest : 3, oldest : 0 ],
        ],
      ],
      [ modality : [ "CT", "MR" ], anatomicRegion : "TH", subrules :
        [
          [ modality : [ "CT", "MR" ], anatomicRegion : "TH", maxAge : 0, newest : 0, oldest : 1 ],
          [ modality : [ "CT", "MR" ], anatomicRegion : "TH", maxAge : 4, newest : 4, oldest : 0 ],
          [ modality : [ "CR" ], anatomicRegion : "TH", maxAge : 3, newest : 3, oldest : 0 ],
        ],
      ],
      [ modality : [ "CT", "MR" ], anatomicRegion : "AB", subrules :
        [
          [ modality : [ "CT", "MR" ], anatomicRegion : "DI", maxAge : 0, newest : 0, oldest : 1 ],
          [ modality : [ "CT", "MR" ], anatomicRegion : "DI", maxAge : 4, newest : 4, oldest : 0 ],
          [ modality : [ "CT", "MR" ], anatomicRegion : "AB", maxAge : 3, newest : 3, oldest : 0 ],
          [ modality : [ "CR" ], anatomicRegion : "DI", maxAge : 3, newest : 3, oldest : 0 ],
          [ modality : [ "RF" ], anatomicRegion : "DI", maxAge : 3, newest : 3, oldest : 0 ],
          [ modality : [ "US" ], anatomicRegion : "AB", maxAge : 3, newest : 3, oldest : 0 ],
          [ modality : [ "US" ], anatomicRegion : "DI", maxAge : 3, newest : 3, oldest : 0 ],
        ],
      ],
      [ modality : [ "CT", "MR" ], anatomicRegion : "CO", subrules :
        [
          [ modality : [ "CT", "MR" ], anatomicRegion : "CO", maxAge : 0, newest : 0, oldest : 1 ],
          [ modality : [ "CT", "MR" ], anatomicRegion : "CO", maxAge : 4, newest : 4, oldest : 0 ],
          [ modality : [ "CR" ], anatomicRegion : "CO", maxAge : 3, newest : 3, oldest : 0 ],
          [ modality : [ "RF" ], anatomicRegion : "CO", maxAge : 3, newest : 3, oldest : 0 ],
        ],
      ],
      [ modality : [ "CT", "MR" ], anatomicRegion : "ES", subrules :
        [
          [ modality : [ "CT", "MR" ], anatomicRegion : "ES", maxAge : 0, newest : 0, oldest : 1 ],
          [ modality : [ "CT", "MR" ], anatomicRegion : "ES", maxAge : 4, newest : 4, oldest : 0 ],
          [ modality : [ "CR" ], anatomicRegion : "ES", maxAge : 3, newest : 3, oldest : 0 ],
          [ modality : [ "US" ], anatomicRegion : "ES", maxAge : 3, newest : 3, oldest : 0 ],
        ],
      ],
      [ modality : [ "CT", "MR" ], anatomicRegion : "EI", subrules :
        [
          [ modality : [ "CT", "MR" ], anatomicRegion : "EI", maxAge : 0, newest : 0, oldest : 1 ],
          [ modality : [ "CT", "MR" ], anatomicRegion : "EI", maxAge : 4, newest : 4, oldest : 0 ],
          [ modality : [ "CR" ], anatomicRegion : "EI", maxAge : 3, newest : 3, oldest : 0 ],
          [ modality : [ "US" ], anatomicRegion : "EI", maxAge : 3, newest : 3, oldest : 0 ],
        ],
      ],
      [ modality : [ "CT", "MR" ], anatomicRegion : "CA", subrules :
        [
          [ modality : [ "CT", "MR" ], anatomicRegion : "CA", maxAge : 0, newest : 0, oldest : 1 ],
          [ modality : [ "CT", "MR" ], anatomicRegion : "CA", maxAge : 4, newest : 4, oldest : 0 ],
          [ modality : [ "CR" ], anatomicRegion : "TH", maxAge : 3, newest : 3, oldest : 0 ],
        ],
      ],
      [ modality : [ "MR" ], anatomicRegion : "SE", subrules :
        [
          [ modality : [ "MR" ], anatomicRegion : "SE", maxAge : 0, newest : 0, oldest : 1 ],
          [ modality : [ "MR" ], anatomicRegion : "SE", maxAge : 5, newest : 4, oldest : 0 ],
          [ modality : [ "MG" ], anatomicRegion : "SE", maxAge : 5, newest : 5, oldest : 0 ],
          [ modality : [ "US" ], anatomicRegion : "SE", maxAge : 5, newest : 5, oldest : 0 ],
        ],
      ],
      [ modality : [ "CT", "MR" ], anatomicRegion : "GO", subrules :
        [
          [ modality : [ "CT", "MR" ], anatomicRegion : "GO", maxAge : 0, newest : 0, oldest : 1 ],
          [ modality : [ "CT", "MR" ], anatomicRegion : "GO", maxAge : 4, newest : 4, oldest : 0 ],
          [ modality : [ "CR" ], anatomicRegion : "GO", maxAge : 3, newest : 3, oldest : 0 ],
          [ modality : [ "US" ], anatomicRegion : "GO", maxAge : 3, newest : 3, oldest : 0 ],
        ],
      ],

    // Rules for CR
      [ modality : [ "CR" ], anatomicRegion : "TC", subrules :
        [
          [ modality : [ "CR" ], anatomicRegion : "TC", maxAge : 0, newest : 0, oldest : 1 ],
          [ modality : [ "CR" ], anatomicRegion : "TC", maxAge : 3, newest : 3, oldest : 0 ],
        ],
      ],
      [ modality : [ "CR" ], anatomicRegion : "DI", subrules :
        [
          [ modality : [ "CR" ], anatomicRegion : "DI", maxAge : 0, newest : 0, oldest : 1 ],
          [ modality : [ "CR" ], anatomicRegion : "DI", maxAge : 3, newest : 3, oldest : 0 ],
          [ modality : [ "CR" ], anatomicRegion : "AB", maxAge : 3, newest : 2, oldest : 0 ],
          [ modality : [ "RF" ], anatomicRegion : "DI", maxAge : 3, newest : 2, oldest : 0 ],
          [ modality : [ "US" ], anatomicRegion : "AB", maxAge : 3, newest : 2, oldest : 0 ],
          [ modality : [ "US" ], anatomicRegion : "DI", maxAge : 3, newest : 2, oldest : 0 ],
        ],
      ],
      [ modality : [ "CR" ], anatomicRegion : "TH", subrules :
        [
          [ modality : [ "CR" ], anatomicRegion : "TH", maxAge : 0, newest : 0, oldest : 1 ],
          [ modality : [ "CR" ], anatomicRegion : "TH", maxAge : 3, newest : 3, oldest : 0 ],
        ],
      ],
      [ modality : [ "CR" ], anatomicRegion : "AB", subrules :
        [
          [ modality : [ "CR" ], anatomicRegion : "AB", maxAge : 0, newest : 0, oldest : 1 ],
          [ modality : [ "CR" ], anatomicRegion : "AB", maxAge : 3, newest : 3, oldest : 0 ],
          [ modality : [ "CR" ], anatomicRegion : "DI", maxAge : 3, newest : 2, oldest : 0 ],
          [ modality : [ "RF" ], anatomicRegion : "AB", maxAge : 3, newest : 2, oldest : 0 ],
          [ modality : [ "RF" ], anatomicRegion : "DI", maxAge : 3, newest : 2, oldest : 0 ],
          [ modality : [ "US" ], anatomicRegion : "AB", maxAge : 3, newest : 2, oldest : 0 ],
          [ modality : [ "US" ], anatomicRegion : "DI", maxAge : 3, newest : 2, oldest : 0 ],
        ],
      ],
      [ modality : [ "CR" ], anatomicRegion : "CO", subrules :
        [
          [ modality : [ "CR" ], anatomicRegion : "CO", maxAge : 0, newest : 0, oldest : 1 ],
          [ modality : [ "CR" ], anatomicRegion : "CO", maxAge : 3, newest : 3, oldest : 0 ],
          [ modality : [ "RF" ], anatomicRegion : "CO", maxAge : 3, newest : 2, oldest : 0 ],
        ],
      ],
      [ modality : [ "CR" ], anatomicRegion : "ES", subrules :
        [
          [ modality : [ "CR" ], anatomicRegion : "ES", maxAge : 0, newest : 0, oldest : 1 ],
          [ modality : [ "CR" ], anatomicRegion : "ES", maxAge : 3, newest : 3, oldest : 0 ],
        ],
      ],
      [ modality : [ "CR" ], anatomicRegion : "EI", subrules :
        [
          [ modality : [ "CR" ], anatomicRegion : "EI", maxAge : 0, newest : 0, oldest : 1 ],
          [ modality : [ "CR" ], anatomicRegion : "EI", maxAge : 3, newest : 3, oldest : 0 ],
        ],
      ],
      [ modality : [ "CR" ], anatomicRegion : "GO", subrules :
        [
          [ modality : [ "CR" ], anatomicRegion : "GO", maxAge : 0, newest : 0, oldest : 1 ],
          [ modality : [ "CR" ], anatomicRegion : "GO", maxAge : 3, newest : 3, oldest : 0 ],
          [ modality : [ "US" ], anatomicRegion : "GO", maxAge : 3, newest : 2, oldest : 0 ],
        ],
      ],
      [ modality : [ "CR" ], anatomicRegion : "SQ", subrules :
        [
          [ modality : [ "CR" ], anatomicRegion : "SQ", maxAge : 3, newest : 2, oldest : 0 ],
        ],
      ],

      // Rules for RF
      [ modality : [ "RF" ], anatomicRegion : "DI", subrules :
        [
          [ modality : [ "RF" ], anatomicRegion : "DI", maxAge : 0, newest : 0, oldest : 1 ],
          [ modality : [ "RF" ], anatomicRegion : "DI", maxAge : 3, newest : 3, oldest : 0 ],
          [ modality : [ "RF" ], anatomicRegion : "AB", maxAge : 3, newest : 2, oldest : 0 ],
          [ modality : [ "CR" ], anatomicRegion : "DI", maxAge : 3, newest : 2, oldest : 0 ],
          [ modality : [ "CR" ], anatomicRegion : "AB", maxAge : 3, newest : 2, oldest : 0 ],
          [ modality : [ "US" ], anatomicRegion : "AB", maxAge : 3, newest : 2, oldest : 0 ],
          [ modality : [ "US" ], anatomicRegion : "DI", maxAge : 3, newest : 2, oldest : 0 ],
        ],
      ],
      [ modality : [ "RF" ], anatomicRegion : "TH", subrules :
        [
          [ modality : [ "RF" ], anatomicRegion : "TH", maxAge : 0, newest : 0, oldest : 1 ],
          [ modality : [ "RF" ], anatomicRegion : "TH", maxAge : 3, newest : 3, oldest : 0 ],
          [ modality : [ "CR" ], anatomicRegion : "TH", maxAge : 3, newest : 2, oldest : 0 ],
        ],
      ],
      [ modality : [ "RF" ], anatomicRegion : "AB", subrules :
        [
          [ modality : [ "RF" ], anatomicRegion : "AB", maxAge : 0, newest : 0, oldest : 1 ],
          [ modality : [ "RF" ], anatomicRegion : "AB", maxAge : 3, newest : 3, oldest : 0 ],
          [ modality : [ "RF" ], anatomicRegion : "DI", maxAge : 3, newest : 2, oldest : 0 ],
          [ modality : [ "CR" ], anatomicRegion : "AB", maxAge : 3, newest : 2, oldest : 0 ],
          [ modality : [ "US" ], anatomicRegion : "AB", maxAge : 3, newest : 2, oldest : 0 ],
          [ modality : [ "US" ], anatomicRegion : "DI", maxAge : 3, newest : 2, oldest : 0 ],
        ],
      ],
      [ modality : [ "RF" ], anatomicRegion : "CO", subrules :
        [
          [ modality : [ "RF" ], anatomicRegion : "CO", maxAge : 0, newest : 0, oldest : 1 ],
          [ modality : [ "RF" ], anatomicRegion : "CO", maxAge : 3, newest : 3, oldest : 0 ],
          [ modality : [ "CR" ], anatomicRegion : "CO", maxAge : 3, newest : 2, oldest : 0 ],
        ],
      ],
      [ modality : [ "RF" ], anatomicRegion : "ES", subrules :
        [
          [ modality : [ "RF" ], anatomicRegion : "ES", maxAge : 0, newest : 0, oldest : 1 ],
          [ modality : [ "RF" ], anatomicRegion : "ES", maxAge : 3, newest : 3, oldest : 0 ],
          [ modality : [ "CR" ], anatomicRegion : "ES", maxAge : 3, newest : 2, oldest : 0 ],
        ],
      ],
      [ modality : [ "RF" ], anatomicRegion : "EI", subrules :
        [
          [ modality : [ "RF" ], anatomicRegion : "EI", maxAge : 0, newest : 0, oldest : 1 ],
          [ modality : [ "RF" ], anatomicRegion : "EI", maxAge : 3, newest : 3, oldest : 0 ],
          [ modality : [ "CR" ], anatomicRegion : "EI", maxAge : 3, newest : 2, oldest : 0 ],
        ],
      ],
      [ modality : [ "RF" ], anatomicRegion : "Extrémités inférieures", subrules :
        [
          [ modality : [ "RF" ], anatomicRegion : "Extrémités inférieures", maxAge : 0, newest : 0, oldest : 1 ],
          [ modality : [ "RF" ], anatomicRegion : "Extrémités inférieures", maxAge : 3, newest : 3, oldest : 0 ],
          [ modality : [ "CR" ], anatomicRegion : "Extrémités inférieures", maxAge : 3, newest : 2, oldest : 0 ],
        ],
      ],
      [ modality : [ "RF" ], anatomicRegion : "Extremites inferieures", subrules :
        [
          [ modality : [ "RF" ], anatomicRegion : "Extremites inferieures", maxAge : 0, newest : 0, oldest : 1 ],
          [ modality : [ "RF" ], anatomicRegion : "Extremites inferieures", maxAge : 3, newest : 3, oldest : 0 ],
          [ modality : [ "CR" ], anatomicRegion : "Extremites inferieures", maxAge : 3, newest : 2, oldest : 0 ],
        ],
      ],
      [ modality : [ "RF" ], anatomicRegion : "GO", subrules :
        [
          [ modality : [ "RF" ], anatomicRegion : "GO", maxAge : 0, newest : 0, oldest : 1 ],
          [ modality : [ "RF" ], anatomicRegion : "GO", maxAge : 3, newest : 3, oldest : 0 ],
          [ modality : [ "US" ], anatomicRegion : "GO", maxAge : 3, newest : 2, oldest : 0 ],
        ],
      ],
      [ modality : [ "RF" ], anatomicRegion : "SQ", subrules :
        [
          [ modality : [ "RF" ], anatomicRegion : "SQ", maxAge : 0, newest : 0, oldest : 1 ],
          [ modality : [ "RF" ], anatomicRegion : "SQ", maxAge : 3, newest : 3, oldest : 0 ],
        ],
      ],

      // Rules for US
      [ modality : [ "US" ], anatomicRegion : "TC", subrules :
        [
          [ modality : [ "US" ], anatomicRegion : "TC", maxAge : 0, newest : 0, oldest : 1 ],
          [ modality : [ "US" ], anatomicRegion : "TC", maxAge : 3, newest : 3, oldest : 0 ],
        ],
      ],
      [ modality : [ "US" ], anatomicRegion : "DI", subrules :
        [
          [ modality : [ "US" ], anatomicRegion : "DI", maxAge : 0, newest : 0, oldest : 1 ],
          [ modality : [ "US" ], anatomicRegion : "DI", maxAge : 3, newest : 3, oldest : 0 ],
          [ modality : [ "US" ], anatomicRegion : "AB", maxAge : 3, newest : 2, oldest : 0 ],
          [ modality : [ "RF" ], anatomicRegion : "DI", maxAge : 3, newest : 2, oldest : 0 ],
          [ modality : [ "CR" ], anatomicRegion : "AB", maxAge : 3, newest : 2, oldest : 0 ],
          [ modality : [ "CR" ], anatomicRegion : "DI", maxAge : 3, newest : 2, oldest : 0 ],
        ],
      ],
      [ modality : [ "US" ], anatomicRegion : "AB", subrules :
        [
          [ modality : [ "US" ], anatomicRegion : "AB", maxAge : 0, newest : 0, oldest : 1 ],
          [ modality : [ "US" ], anatomicRegion : "AB", maxAge : 3, newest : 3, oldest : 0 ],
          [ modality : [ "US" ], anatomicRegion : "DI", maxAge : 3, newest : 2, oldest : 0 ],
          [ modality : [ "CR" ], anatomicRegion : "AB", maxAge : 3, newest : 2, oldest : 0 ],
          [ modality : [ "CR" ], anatomicRegion : "DI", maxAge : 3, newest : 2, oldest : 0 ],
        ],
      ],
      [ modality : [ "US" ], anatomicRegion : "ES", subrules :
        [
          [ modality : [ "US" ], anatomicRegion : "ES", maxAge : 0, newest : 0, oldest : 1 ],
          [ modality : [ "US" ], anatomicRegion : "ES", maxAge : 3, newest : 3, oldest : 0 ],
          [ modality : [ "CR" ], anatomicRegion : "ES", maxAge : 3, newest : 2, oldest : 0 ],
          [ modality : [ "RF" ], anatomicRegion : "ES", maxAge : 3, newest : 2, oldest : 0 ],
        ],
      ],
      [ modality : [ "US" ], anatomicRegion : "EI", subrules :
        [
          [ modality : [ "US" ], anatomicRegion : "EI", maxAge : 0, newest : 0, oldest : 1 ],
          [ modality : [ "US" ], anatomicRegion : "EI", maxAge : 3, newest : 3, oldest : 0 ],
          [ modality : [ "CR" ], anatomicRegion : "EI", maxAge : 3, newest : 2, oldest : 0 ],
          [ modality : [ "RF" ], anatomicRegion : "EI", maxAge : 3, newest : 2, oldest : 0 ],
        ],
      ],
      [ modality : [ "US" ], anatomicRegion : "CA", subrules :
        [
          [ modality : [ "US" ], anatomicRegion : "CA", maxAge : 0, newest : 0, oldest : 1 ],
          [ modality : [ "US" ], anatomicRegion : "CA", maxAge : 3, newest : 3, oldest : 0 ],
          [ modality : [ "CR" ], anatomicRegion : "TH", maxAge : 3, newest : 2, oldest : 0 ],
        ],
      ],
      [ modality : [ "US" ], anatomicRegion : "SE", subrules :
        [
          [ modality : [ "US" ], anatomicRegion : "SE", maxAge : 0, newest : 0, oldest : 1 ],
          [ modality : [ "US" ], anatomicRegion : "SE", maxAge : 5, newest : 4, oldest : 0 ],
          [ modality : [ "MG" ], anatomicRegion : "SE", maxAge : 5, newest : 5, oldest : 0 ],
          [ modality : [ "MR" ], anatomicRegion : "SE", maxAge : 5, newest : 5, oldest : 0 ],
        ],
      ],
      [ modality : [ "US" ], anatomicRegion : "GO", subrules :
        [
          [ modality : [ "US" ], anatomicRegion : "GO", maxAge : 0, newest : 0, oldest : 1 ],
          [ modality : [ "US" ], anatomicRegion : "GO", maxAge : 3, newest : 3, oldest : 0 ],
          [ modality : [ "CR" ], anatomicRegion : "GO", maxAge : 3, newest : 2, oldest : 0 ],
          [ modality : [ "MR" ], anatomicRegion : "GO", maxAge : 3, newest : 2, oldest : 0 ],
        ],
      ],

      // Rules for XA
      [ modality : [ "XA" ], anatomicRegion : "TC", subrules :
        [
          [ modality : [ "XA" ], anatomicRegion : "TC", maxAge : 0, newest : 0, oldest : 1 ],
          [ modality : [ "XA" ], anatomicRegion : "TC", maxAge : 3, newest : 3, oldest : 0 ],
          [ modality : [ "AN" ], anatomicRegion : "TC", maxAge : 3, newest : 2, oldest : 0 ],
          [ modality : [ "CT", "MR" ], anatomicRegion : "TC", maxAge : 3, newest : 2, oldest : 0 ],
          [ modality : [ "US" ], anatomicRegion : "TC", maxAge : 3, newest : 2, oldest : 0 ],
        ],
      ],
      [ modality : [ "XA" ], anatomicRegion : "DI", subrules :
        [
          [ modality : [ "XA" ], anatomicRegion : "DI", maxAge : 0, newest : 0, oldest : 1 ],
          [ modality : [ "XA" ], anatomicRegion : "DI", maxAge : 3, newest : 3, oldest : 0 ],
          [ modality : [ "AN" ], anatomicRegion : "DI", maxAge : 3, newest : 2, oldest : 0 ],
          [ modality : [ "CT", "MR" ], anatomicRegion : "DI", maxAge : 3, newest : 2, oldest : 0 ],
          [ modality : [ "CT", "MR" ], anatomicRegion : "AB", maxAge : 3, newest : 2, oldest : 0 ],
          [ modality : [ "US" ], anatomicRegion : "AB", maxAge : 3, newest : 2, oldest : 0 ],
          [ modality : [ "US" ], anatomicRegion : "DI", maxAge : 3, newest : 2, oldest : 0 ],
        ],
      ],
      [ modality : [ "XA" ], anatomicRegion : "TH", subrules :
        [
          [ modality : [ "XA" ], anatomicRegion : "TH", maxAge : 0, newest : 0, oldest : 1 ],
          [ modality : [ "XA" ], anatomicRegion : "TH", maxAge : 3, newest : 3, oldest : 0 ],
          [ modality : [ "AN" ], anatomicRegion : "TH", maxAge : 3, newest : 2, oldest : 0 ],
          [ modality : [ "CT", "MR" ], anatomicRegion : "TH", maxAge : 3, newest : 2, oldest : 0 ],
          [ modality : [ "CR" ], anatomicRegion : "TH", maxAge : 3, newest : 2, oldest : 0 ],
        ],
      ],
      [ modality : [ "XA" ], anatomicRegion : "AB", subrules :
        [
          [ modality : [ "XA" ], anatomicRegion : "DI", maxAge : 0, newest : 0, oldest : 1 ],
          [ modality : [ "XA" ], anatomicRegion : "DI", maxAge : 3, newest : 3, oldest : 0 ],
          [ modality : [ "AN" ], anatomicRegion : "AB", maxAge : 3, newest : 2, oldest : 0 ],
          [ modality : [ "CT", "MR" ], anatomicRegion : "AB", maxAge : 3, newest : 2, oldest : 0 ],
          [ modality : [ "CT", "MR" ], anatomicRegion : "DI", maxAge : 3, newest : 2, oldest : 0 ],
          [ modality : [ "US" ], anatomicRegion : "AB", maxAge : 3, newest : 2, oldest : 0 ],
          [ modality : [ "US" ], anatomicRegion : "DI", maxAge : 3, newest : 2, oldest : 0 ],
        ],
      ],
      [ modality : [ "XA" ], anatomicRegion : "CO", subrules :
        [
          [ modality : [ "XA" ], anatomicRegion : "CO", maxAge : 0, newest : 0, oldest : 1 ],
          [ modality : [ "XA" ], anatomicRegion : "CO", maxAge : 3, newest : 3, oldest : 0 ],
          [ modality : [ "AN" ], anatomicRegion : "CO", maxAge : 3, newest : 2, oldest : 0 ],
          [ modality : [ "CT", "MR" ], anatomicRegion : "CO", maxAge : 3, newest : 2, oldest : 0 ],
          [ modality : [ "CR" ], anatomicRegion : "CO", maxAge : 3, newest : 2, oldest : 0 ],
        ],
      ],
      [ modality : [ "XA" ], anatomicRegion : "ES", subrules :
        [
          [ modality : [ "XA" ], anatomicRegion : "ES", maxAge : 0, newest : 0, oldest : 1 ],
          [ modality : [ "XA" ], anatomicRegion : "ES", maxAge : 3, newest : 3, oldest : 0 ],
          [ modality : [ "AN" ], anatomicRegion : "ES", maxAge : 3, newest : 2, oldest : 0 ],
          [ modality : [ "CR" ], anatomicRegion : "ES", maxAge : 3, newest : 2, oldest : 0 ],
          [ modality : [ "CT", "MR" ], anatomicRegion : "ES", maxAge : 3, newest : 2, oldest : 0 ],
        ],
      ],
      [ modality : [ "XA" ], anatomicRegion : "EI", subrules :
        [
          [ modality : [ "XA" ], anatomicRegion : "EI", maxAge : 0, newest : 0, oldest : 1 ],
          [ modality : [ "XA" ], anatomicRegion : "EI", maxAge : 3, newest : 3, oldest : 0 ],
          [ modality : [ "AN" ], anatomicRegion : "EI", maxAge : 3, newest : 2, oldest : 0 ],
          [ modality : [ "CR" ], anatomicRegion : "EI", maxAge : 3, newest : 2, oldest : 0 ],
          [ modality : [ "CT", "MR" ], anatomicRegion : "EI", maxAge : 3, newest : 2, oldest : 0 ],
        ],
      ],
      [ modality : [ "XA" ], anatomicRegion : "CA", subrules :
        [
          [ modality : [ "XA" ], anatomicRegion : "CA", maxAge : 0, newest : 0, oldest : 1 ],
          [ modality : [ "XA" ], anatomicRegion : "CA", maxAge : 3, newest : 3, oldest : 0 ],
          [ modality : [ "XA" ], anatomicRegion : "HE", maxAge : 3, newest : 2, oldest : 0 ],
          [ modality : [ "CT", "MR" ], anatomicRegion : "CA", maxAge : 3, newest : 2, oldest : 0 ],
          [ modality : [ "CR" ], anatomicRegion : "CA", maxAge : 3, newest : 2, oldest : 0 ],
          [ modality : [ "CR" ], anatomicRegion : "TH", maxAge : 3, newest : 2, oldest : 0 ],
        ],
      ],
      [ modality : [ "XA" ], anatomicRegion : "HE", subrules :
        [
          [ modality : [ "XA" ], anatomicRegion : "HE", maxAge : 0, newest : 0, oldest : 1 ],
          [ modality : [ "XA" ], anatomicRegion : "HE", maxAge : 3, newest : 3, oldest : 0 ],
          [ modality : [ "XA" ], anatomicRegion : "CA", maxAge : 3, newest : 2, oldest : 0 ],
          [ modality : [ "CT", "MR" ], anatomicRegion : "CA", maxAge : 3, newest : 2, oldest : 0 ],
          [ modality : [ "CR" ], anatomicRegion : "CA", maxAge : 3, newest : 2, oldest : 0 ],
          [ modality : [ "CR" ], anatomicRegion : "TH", maxAge : 3, newest : 2, oldest : 0 ],
        ],
      ],
      [ modality : [ "XA" ], anatomicRegion : "Hémodynamie", subrules :
        [
          [ modality : [ "XA" ], anatomicRegion : "HE", maxAge : 0, newest : 0, oldest : 1 ],
          [ modality : [ "XA" ], anatomicRegion : "HE", maxAge : 3, newest : 3, oldest : 0 ],
          [ modality : [ "XA" ], anatomicRegion : "CA", maxAge : 3, newest : 2, oldest : 0 ],
          [ modality : [ "CT", "MR" ], anatomicRegion : "CA", maxAge : 3, newest : 2, oldest : 0 ],
          [ modality : [ "CR" ], anatomicRegion : "CA", maxAge : 3, newest : 2, oldest : 0 ],
          [ modality : [ "CR" ], anatomicRegion : "TH", maxAge : 3, newest : 2, oldest : 0 ],
        ],
      ],
      [ modality : [ "XA" ], anatomicRegion : "Hemodynamie", subrules :
        [
          [ modality : [ "XA" ], anatomicRegion : "HE", maxAge : 0, newest : 0, oldest : 1 ],
          [ modality : [ "XA" ], anatomicRegion : "HE", maxAge : 3, newest : 3, oldest : 0 ],
          [ modality : [ "XA" ], anatomicRegion : "CA", maxAge : 3, newest : 2, oldest : 0 ],
          [ modality : [ "CT", "MR" ], anatomicRegion : "CA", maxAge : 3, newest : 2, oldest : 0 ],
          [ modality : [ "CR" ], anatomicRegion : "CA", maxAge : 3, newest : 2, oldest : 0 ],
          [ modality : [ "CR" ], anatomicRegion : "TH", maxAge : 3, newest : 2, oldest : 0 ],
        ],
      ],
      [ modality : [ "XA" ], anatomicRegion : "AN", subrules :
        [
          [ modality : [ "XA" ], anatomicRegion : "AN", maxAge : 0, newest : 0, oldest : 1 ],
          [ modality : [ "XA" ], anatomicRegion : "AN", maxAge : 3, newest : 3, oldest : 0 ],
        ],
      ],

      // Rules for MG
      [ modality : [ "MG" ], anatomicRegion : "SE", subrules :
        [
          [ modality : [ "MG" ], anatomicRegion : "SE", maxAge : 0, newest : 0, oldest : 1 ],
          [ modality : [ "MG" ], anatomicRegion : "SE", maxAge : 5, newest : 4, oldest : 0 ],
          [ modality : [ "US" ], anatomicRegion : "SE", maxAge : 5, newest : 5, oldest : 0 ],
          [ modality : [ "MR" ], anatomicRegion : "SE", maxAge : 5, newest : 5, oldest : 0 ],
          [ modality : [ "MN" ], anatomicRegion : "SE", maxAge : 5, newest : 5, oldest : 0 ],
        ],
      ],

      // Rules for NM
      [ modality : [ "NM" ], anatomicRegion : "TC", subrules :
        [
          [ modality : [ "NM" ], anatomicRegion : "TC", maxAge : 0, newest : 0, oldest : 1 ],
          [ modality : [ "NM" ], anatomicRegion : "TC", maxAge : 3, newest : 3, oldest : 0 ],
          [ modality : [ "CT", "MR" ], anatomicRegion : "TC", maxAge : 3, newest : 2, oldest : 0 ],
          [ modality : [ "PT" ], anatomicRegion : "TP", maxAge : 3, newest : 2, oldest : 0 ],
        ],
      ],
      [ modality : [ "NM" ], anatomicRegion : "DI", subrules :
        [
          [ modality : [ "NM" ], anatomicRegion : "DI", maxAge : 0, newest : 0, oldest : 1 ],
          [ modality : [ "NM" ], anatomicRegion : "DI", maxAge : 3, newest : 3, oldest : 0 ],
          [ modality : [ "NM" ], anatomicRegion : "AB", maxAge : 3, newest : 2, oldest : 0 ],
          [ modality : [ "CT", "MR" ], anatomicRegion : "DI", maxAge : 3, newest : 2, oldest : 0 ],
          [ modality : [ "CT", "MR" ], anatomicRegion : "AB", maxAge : 3, newest : 2, oldest : 0 ],
          [ modality : [ "CR" ], anatomicRegion : "DI", maxAge : 1, newest : 2, oldest : 0 ],
          [ modality : [ "CR" ], anatomicRegion : "AB", maxAge : 1, newest : 2, oldest : 0 ],
        ],
      ],
      [ modality : [ "NM" ], anatomicRegion : "TH", subrules :
        [
          [ modality : [ "NM" ], anatomicRegion : "TH", maxAge : 0, newest : 0, oldest : 1 ],
          [ modality : [ "NM" ], anatomicRegion : "TH", maxAge : 3, newest : 3, oldest : 0 ],
          [ modality : [ "CT", "MR" ], anatomicRegion : "TH", maxAge : 3, newest : 2, oldest : 0 ],
          [ modality : [ "CR" ], anatomicRegion : "TH", maxAge : 1, newest : 2, oldest : 0 ],
          [ modality : [ "CR" ], anatomicRegion : "AB", maxAge : 1, newest : 2, oldest : 0 ],
        ],
      ],
      [ modality : [ "NM" ], anatomicRegion : "AB", subrules :
        [
          [ modality : [ "NM" ], anatomicRegion : "AB", maxAge : 0, newest : 0, oldest : 1 ],
          [ modality : [ "NM" ], anatomicRegion : "AB", maxAge : 3, newest : 3, oldest : 0 ],
          [ modality : [ "NM" ], anatomicRegion : "DI", maxAge : 3, newest : 2, oldest : 0 ],
          [ modality : [ "CT", "MR" ], anatomicRegion : "AB", maxAge : 3, newest : 2, oldest : 0 ],
          [ modality : [ "CR" ], anatomicRegion : "AB", maxAge : 1, newest : 2, oldest : 0 ],
          [ modality : [ "CR" ], anatomicRegion : "DI", maxAge : 1, newest : 2, oldest : 0 ],
          [ modality : [ "PT" ], anatomicRegion : "TP", maxAge : 2, newest : 2, oldest : 0 ],
        ],
      ],
      [ modality : [ "NM" ], anatomicRegion : "CA", subrules :
        [
          [ modality : [ "NM" ], anatomicRegion : "CA", maxAge : 0, newest : 0, oldest : 1 ],
          [ modality : [ "NM" ], anatomicRegion : "CA", maxAge : 3, newest : 3, oldest : 0 ],
          [ modality : [ "CT", "MR" ], anatomicRegion : "CA", maxAge : 3, newest : 2, oldest : 0 ],
          [ modality : [ "US" ], anatomicRegion : "CA", maxAge : 3, newest : 2, oldest : 0 ],
          [ modality : [ "XA" ], anatomicRegion : "HE", maxAge : 3, newest : 2, oldest : 0 ],
          [ modality : [ "CR" ], anatomicRegion : "TH", maxAge : 1, newest : 2, oldest : 0 ],
          [ modality : [ "PT" ], anatomicRegion : "TP", maxAge : 3, newest : 2, oldest : 0 ],
        ],
      ],
      [ modality : [ "NM" ], anatomicRegion : "SE", subrules :
        [
          [ modality : [ "NM" ], anatomicRegion : "SE", maxAge : 0, newest : 0, oldest : 1 ],
          [ modality : [ "NM" ], anatomicRegion : "SE", maxAge : 2, newest : 3, oldest : 1 ],
          [ modality : [ "MG" ], anatomicRegion : "SE", maxAge : 2, newest : 2, oldest : 0 ],
          [ modality : [ "MR" ], anatomicRegion : "SE", maxAge : 2, newest : 2, oldest : 0 ],
          [ modality : [ "PT" ], anatomicRegion : "TP", maxAge : 2, newest : 2, oldest : 0 ],
        ],
      ],
      [ modality : [ "NM" ], anatomicRegion : "EN", subrules :
        [
          [ modality : [ "NM" ], anatomicRegion : "EN", maxAge : 0, newest : 0, oldest : 1 ],
          [ modality : [ "NM" ], anatomicRegion : "EN", maxAge : 5, newest : 3, oldest : 0 ],
          [ modality : [ "NM" ], anatomicRegion : "TC", maxAge : 3, newest : 2, oldest : 0 ],
          [ modality : [ "CT", "MR" ], anatomicRegion : "TH", maxAge : 3, newest : 2, oldest : 0 ],
          [ modality : [ "CT", "MR" ], anatomicRegion : "AB", maxAge : 3, newest : 2, oldest : 0 ],
          [ modality : [ "US" ], anatomicRegion : "TC", maxAge : 3, newest : 2, oldest : 0 ],
          [ modality : [ "PT" ], anatomicRegion : "TP", maxAge : 3, newest : 2, oldest : 0 ],
        ],
      ],
      [ modality : [ "NM" ], anatomicRegion : "Endocrinien", subrules :
        [
          [ modality : [ "NM" ], anatomicRegion : "EN", maxAge : 0, newest : 0, oldest : 1 ],
          [ modality : [ "NM" ], anatomicRegion : "EN", maxAge : 5, newest : 3, oldest : 0 ],
          [ modality : [ "NM" ], anatomicRegion : "TC", maxAge : 3, newest : 2, oldest : 0 ],
          [ modality : [ "CT", "MR" ], anatomicRegion : "TH", maxAge : 3, newest : 2, oldest : 0 ],
          [ modality : [ "CT", "MR" ], anatomicRegion : "AB", maxAge : 3, newest : 2, oldest : 0 ],
          [ modality : [ "US" ], anatomicRegion : "TC", maxAge : 3, newest : 2, oldest : 0 ],
          [ modality : [ "PT" ], anatomicRegion : "TP", maxAge : 3, newest : 2, oldest : 0 ],
        ],
      ],
      [ modality : [ "NM" ], anatomicRegion : "SQ", subrules :
        [
          [ modality : [ "NM" ], anatomicRegion : "SQ", maxAge : 0, newest : 0, oldest : 1 ],
          [ modality : [ "NM" ], anatomicRegion : "SQ", maxAge : 3, newest : 3, oldest : 0 ],
          [ modality : [ "CT", "MR" ], anatomicRegion : "TC", maxAge : 2, newest : 2, oldest : 0 ],
          [ modality : [ "CT", "MR" ], anatomicRegion : "TH", maxAge : 2, newest : 2, oldest : 0 ],
          [ modality : [ "CT", "MR" ], anatomicRegion : "CO", maxAge : 2, newest : 2, oldest : 0 ],
          [ modality : [ "CT", "MR" ], anatomicRegion : "EI", maxAge : 2, newest : 2, oldest : 0 ],
          [ modality : [ "CT", "MR" ], anatomicRegion : "ES", maxAge : 2, newest : 2, oldest : 0 ],
          [ modality : [ "CT", "MR" ], anatomicRegion : "AB", maxAge : 2, newest : 2, oldest : 0 ],
          [ modality : [ "CR" ], anatomicRegion : "TC", maxAge : 2, newest : 2, oldest : 0 ],
          [ modality : [ "CR" ], anatomicRegion : "TH", maxAge : 2, newest : 2, oldest : 0 ],
          [ modality : [ "CR" ], anatomicRegion : "CO", maxAge : 2, newest : 2, oldest : 0 ],
          [ modality : [ "CR" ], anatomicRegion : "EI", maxAge : 2, newest : 2, oldest : 0 ],
          [ modality : [ "CR" ], anatomicRegion : "ES", maxAge : 2, newest : 2, oldest : 0 ],
          [ modality : [ "CR" ], anatomicRegion : "AB", maxAge : 2, newest : 2, oldest : 0 ],
          [ modality : [ "PT" ], anatomicRegion : "TP", maxAge : 3, newest : 2, oldest : 0 ],
        ],
      ],
      [ modality : [ "NM" ], anatomicRegion : "HP", subrules :
        [
          [ modality : [ "NM" ], anatomicRegion : "HP", maxAge : 0, newest : 0, oldest : 1 ],
          [ modality : [ "NM" ], anatomicRegion : "HP", maxAge : 3, newest : 3, oldest : 0 ],
          [ modality : [ "PT" ], anatomicRegion : "TP", maxAge : 3, newest : 2, oldest : 0 ],
        ],
      ],
      [ modality : [ "NM" ], anatomicRegion : "Hématopoïétique", subrules :
        [
          [ modality : [ "NM" ], anatomicRegion : "HP", maxAge : 0, newest : 0, oldest : 1 ],
          [ modality : [ "NM" ], anatomicRegion : "HP", maxAge : 3, newest : 3, oldest : 0 ],
          [ modality : [ "PT" ], anatomicRegion : "TP", maxAge : 3, newest : 2, oldest : 0 ],
        ],
      ],
      [ modality : [ "NM" ], anatomicRegion : "Hematopoietique", subrules :
        [
          [ modality : [ "NM" ], anatomicRegion : "HP", maxAge : 0, newest : 0, oldest : 1 ],
          [ modality : [ "NM" ], anatomicRegion : "HP", maxAge : 3, newest : 3, oldest : 0 ],
          [ modality : [ "PT" ], anatomicRegion : "TP", maxAge : 3, newest : 2, oldest : 0 ],
        ],
      ],

      // Rules for PT
      [ modality : [ "PT" ], anatomicRegion : "TP", subrules :
        [
          [ modality : [ "NM" ], anatomicRegion : "HP", maxAge : 0, newest : 0, oldest : 1 ],
          [ modality : [ "NM" ], anatomicRegion : "HP", maxAge : 5, newest : 4, oldest : 0 ],
          [ modality : [ "CT", "MR" ], anatomicRegion : "TC", maxAge : 3, newest : 3, oldest : 0 ],
          [ modality : [ "CT", "MR" ], anatomicRegion : "DI", maxAge : 3, newest : 3, oldest : 0 ],
          [ modality : [ "CT", "MR" ], anatomicRegion : "TH", maxAge : 3, newest : 3, oldest : 0 ],
          [ modality : [ "CT", "MR" ], anatomicRegion : "AB", maxAge : 3, newest : 3, oldest : 0 ],
          [ modality : [ "CT", "MR" ], anatomicRegion : "CO", maxAge : 3, newest : 3, oldest : 0 ],
          [ modality : [ "CT", "MR" ], anatomicRegion : "CA", maxAge : 3, newest : 3, oldest : 0 ],
        ],
      ],
    ]

    static {
        log.trace("Number of rules: " + rules.size())
        log.trace("Logical number of rules: " + rules.collectMany({ it.modality }).size())
            // counts the total number of modalities in the rules; collects all the modalities into one list
            // we count each individual combination of modality and anatomic region as 1 rule
            // (some rules have multiple modalities)


        // Compile the rules into a map based on modality and anatomic region.
        // Slightly more complicated than List.groupBy because we need to produce
        // multiple map entries per list item
        def map = [:]
        rules.each { rule ->
            rule.modality.each { ruleModality ->
                map[[ modality: ruleModality, anatomicRegion: rule.anatomicRegion ]] = rule
            }
        }
        rules = map
            // Put an entry in the map for each modality in each rule

        // if this comes out different than the logical number of rules, the table
        // is illogical (probably has duplicate, possibly conflicting rules)
        log.trace("Actual number of rules: " + rules.size())
    }

    private static getCodeValues(prior, scheme) {
        return prior.get(XDSEventCodes).findAll({ ec -> 
            scheme == ec.getSchemeName()
        }).collect({ ec -> 
            ec.getCodeValue() 
        })
    }

    /**
     * @return true if it matches, false otherwise
     */
    private static priorMatchesSubrule(prior, subrule) {
        log.trace("Testing prior " + prior + " against rule " + subrule)

        // The prior must contain one of the modalities modality that is specified by the subrule.
        if (subrule.modality.disjoint(getCodeValues(prior, "DCM"))) {
            log.trace("Prior does not contain relevant modalities")
            return false
        }

        // The anatomic region of the prior must match the anatomic region of the subrule.
        if (!getCodeValues(prior, "Imagerie Québec-DSQ").contains(subrule.anatomicRegion)) {
            log.trace("Prior does not match anatomic region")
            return false
        }

        // Now check to see if there is a maximum age constraint and if so, does the study date 
        // of the prior fall within that constraint.
        if (subrule.maxAge > 0) {
            def earliestStudyDate = new org.joda.time.DateTime().minusYears(subrule.maxAge)
            def priorStudyDate = prior.get(XDSServiceStartTime)

            if (priorStudyDate < earliestStudyDate) {
              log.trace("Prior falls outside of maximum age")
                return false
            }
        } 

        log.trace("Prior matches rule")
        return true
    }

    /**
     * @return list containing matching priors
     */
    private static filterForNewestAndOldest(priors, subrule) {
        if (subrule.newest <= 0 && subrule.oldest <= 0) {
            log.trace("Rule has no newest/oldest restrictions")
            return priors
        }

        if (subrule.newest + subrule.oldest >= priors.size()) {
            log.trace("Newest/oldest restrictions include all priors")
            return priors
        }

        log.trace("Relevant sub priors before forward sorting is " + priors)
        priors.sort { it.get(XDSServiceStartTime) }
        log.trace("Relevant sub priors after forward sorting is " + priors)

        // sort goes low to high, so the newest are at the end of the list:
        return priors.reverse().take(subrule.newest) +
            priors.take(subrule.oldest)
    }

    /**
     * Top level method for comparing a list of priors against a single subrule.
     * @return list containing matching priors
     */
    private static getPriorsMatchingSubrule(subrule, allPriors) {
        log.trace("Subrule is " + subrule)

        def matchingPriors = allPriors.findAll { priorMatchesSubrule(it, subrule) }
        log.trace("Relevant sub priors matching subrules are: " + matchingPriors)

        matchingPriors = filterForNewestAndOldest(matchingPriors, subrule)
        log.trace("Relevant sub priors after computing newest/oldest are " + matchingPriors)

        return matchingPriors
    }

    /**
     * Starting point for evaluating rules.
     */
    public static findRelevantPriors(order, allPriors) {

        // Find the rule that matches the order's modality and anatomic region:
        def key = [ modality: order.get("OBR-24"), anatomicRegion: order.get("OBR-15-4") ]
        log.trace("Looking up rule for key: {}", key)
        def rule = rules[key]

        log.trace("Rule is: " + rule)

        if (rule == null) {
            return []
        }

        // for each subrule, get the list of matching priors and 
        // combine them all into one list:
        def relevantPriors = rule.subrules.collectMany { subrule ->
            getPriorsMatchingSubrule(subrule, allPriors)
        }

        log.trace("Relevant priors before uniquifying " + relevantPriors)

        // It may be possible that there are duplicates in the relevant priors,
        // since a single prior may match multiple rules.  Two priors are determined to
        // be duplicates if they share the same study instance UID.
        relevantPriors.unique { it.get(XDSExtendedMetadata("studyInstanceUID")) }

        log.debug("These are the Study Istance UIDs that match the rules:\n")

        for(eachPrior in relevantPriors) {
            //  THIS REALLY IS DEBUG LEVEL!
            log.debug("{}", eachPrior.get(XDSExtendedMetadata("studyInstanceUID") ) )
        }

        return relevantPriors
    }
}

return PriorsRules.findRelevantPriors(order, studies)
