/*
* This is a 'hybrid' script that takes the returned C-FIND values that the DIR gives back, and makes available the "xdsMetadata" variable, 
*   containing the extended XDS Metadata for this study.
*   The script runs one study at a time, and it makes available C-Find response information in the DIR, and its analogous XDS extended metadata.
*   This information then can be reconciled, updated, merged, etc. as needed.
*   The script is multi-purpose: (please see below)
*
* The ultimate aims of this script are to:
*   1. Localize the Patient IDs given back to the Calling PACS (i.e. so the PACS can understand the results, and assign to the correct patient)
*   2. Prefix Study Accession Numbers, so there are no collisions with local studies at the PACS
*   3. Prefix the Study ID, so there are no collisions with local studies at the PACS
*   4.  Change the Retreive AETitle to that of this Connect Instance itself.  This is so the PACS can call Rialto if it decides to fetch the studies it is 
*       querying.
*   5. (Optionally) either merge, or give preference to the Modalities in Study provided by the DIR and by the XDS Regsitry
*/


/* ========= Shared Files Loading ============= */
    LOAD("../Shared_Scripting_Files/shared_constants.groovy")
     // Contains commonly-used constants as attributes of the "SharedConstants" class
     // For example: SharedConstants.rialtoRetrieveAETitle  (see the file for full details)

    LOAD("../Shared_Scripting_Files/common_localization_routines.groovy")
    /* Load the shared methods that allow:
        a) Using either a Patient ID Local to the PACS, or a prefixed foreign Patient when the Patient does not exist in the PACS
        b) The Procedure Codes tables to normalize the hanging protocols at the local PACS      */

    LOAD("../Shared_Scripting_Files/shared_xsd_modality_and_anatomic_region_routines.groovy")
    /* Load the shared methods that allow:
        a) Working with the XDS Procedure Codes available to the Java code
        b) Dermine Modalities and Anatomic Regions
        c) Use the Modalities and Anatomic Regions above to map the correct Procedure Codes for a Study */



/* =========  Constant Declaration  ================ */
    giveBackOnlyOneModalityperStudy = true 
    // Determines whether they want only 1 modality per study


/* =========  Script  ================ */

 log.debug("\n\n**Starting C-Find Response Localizer Script**\n")


 // Checking Source Patient IDs and Issuers of PIDs received back from XDS Reg
    /* Please note that at this time the Patient ID and Issuer of Patient ID are being set by the source code to the 
            value that is in the "SourcePatientID value" of the XDS Registry.
            This means that whatever is in the DIR is getting overwritten for these 2 values   */
    def originalPID = get(PatientID)
    log.debug("Source pid in XDS Metadata (at response from DIR) before morphing to PACS: {}", originalPID)

    def originalAssigningAuthority = get(IssuerOfPatientID)
    log.debug("Source Issuer of PID in XDS Metadata (at response from DIR) before morphing to PACS: {}", originalAssigningAuthority)


 /* Ignoring local studies */
    if( originalAssigningAuthority == SharedConstants.Local_PACS_Issuer_of_Patient_ID) {
        log.debug("Dropping this study from the Ad-Hoc C-Find Response, as it is considered LOCAL to the PACS")
        return false
    }

 /* 1. Localizing the PID for responding to the calling PACS */
  /* NOTE:  Please ENSURE that the "LocalIssuerIDs" Constant defined in the class "Constants" in "../Shared_Scripting_Files/common_localization_routines.groovy"
  *         is set correctly to the Issuer Of Patient ID corresponding to the PACS that this Connect is servicing.
  *         Otherwise, all ad-hoc C-Find responses will contain incorrect foreing patients, and the C-Find responses will
  *           be useless to the calling PACSs
  */

    log.debug("This is the current value of the \"Other Patient IDs Sequence\": {}", getAllPidsQualified() )

    // TODO update the Common file
    def localOrPrefixedPID = Pids.localize([originalPID, originalAssigningAuthority], getAllPidsQualified())
        // 'getAllPidsQualified' is a Rialto Groovy Library function that gets the "Other Patient IDs" sequence available in an incoming DICOM object
        // Pids.localize() is a method which returns back either the local Patient ID for the PACS, or a Prefixed Foreign PID

    log.debug("Setting local or pre-fixed pid in C-FIND response to PACS: {}", localOrPrefixedPID)
    set(PatientID, localOrPrefixedPID[0])
    
    // Normally, the PACS doesn't need the IssuerOfPatientID populated, since all Patients are of the same domain, and it's implied
    remove(IssuerOfPatientID)


 
 /* 2. Prefixing the Accession Numbers so they can *uniquely* and clearly be recognized as foreign studies in the PACS using Connect */
    if( null == originalAssigningAuthority || "" == originalAssigningAuthority ){
        log.warn("Empty Assigning Authority (IssuerOfPatientID) at DIR. The Accession Number will be prefixed as Foreign, and unexpected")
    }
    
    def prefix = Prefixes.getPrefix(originalAssigningAuthority)
    // Prefixes.getPrefix() is a method in the "../Shared_Scripting_Files/common_localization_routines.groovy" file that returns the correct prefix
    if(prefix == "AU" || prefix == "in") {
        log.warn("Unknown Site OID Mapping for \"{}\". The prefixing will not be what is expected!", originalAssigningAuthority)
    } else {
        log.debug("Current prefix to prepend to Accession Number is: \"{}\"", prefix)
    }


    if(get(AccessionNumber).size() >= 14){
        log.warn("Accession Number \"{}\" is greater than 14 characters.  This might cause unforseen issues at PACS.")
    }
        // Prepend the "localizer" prefix to the Accession Number
        prepend(AccessionNumber, prefix)
            // The 'prepend' method directly modifies the output field --i.e. as if running a set() with a prefix


 /* 3. Prefixing the Study IDs so they can *uniquely* and clearly be recognized as foreign studies in the PACS using Connect */
   if(prefix != null && (! prefix.isEmpty() ) ) {
    // Prefix is valid
        prepend(StudyID, prefix)
    }


 /* 4.  Setting the Retreive AETitle to Connect itself, so that the PACS goes through Connect for study localization */

    // Outputting the DIR's retreive AETitle for debugging purposes
    log.debug("Retreive AETitle at DIR is : \"{}\"", get(RetrieveAETitle))

    // Setting Rialto's AE as the RetrieveAETitle
    set('RetrieveAETitle', SharedConstants.rialtoRetrieveAETitle)
    log.debug("Current Retreive AETitle after morphing is : \"{}\"", output.get(RetrieveAETitle))



 /*  5. Handle the Modalities in Study Correctly for Foreign and/or Local Studies */

    def dirModalities = null
    def xdsModalities = null
    def finalModalities = null

    /* Get the list of --unique-- Modalities returned by the --DIR-- */
    dirModalities = getList('ModalitiesInStudy') as Set
    log.debug("Modalities In Study, as passed from the DIR:\n{}", dirModalities)


    /* Get the unique set of 'Modalities' as held by the XDS Registry using the 'xdsMetadata' variable made available to this script. */

    // XDSEventCodes is where the modalities are in the XDS Registry
    xdsModalities = xdsMetadata.get(XDSEventCodes).findAll { eventCode -> "DCM".equals(eventCode.getSchemeName()) }
        // Get the XDS list of event codes that have a 'DCM' (DICOM) value
        // getSchemeName() is a Rialto scripting function for XDS objects
    
    log.debug("Event Codes (Modalities In Study) as passed raw from the XDS Registry:\n{}", xdsModalities)


    // Transform all the XDS-formatted codes holding modalities into a list of plain strings
    xdsModalities = xdsModalities.collect { 
        currentXDSmodality -> currentXDSmodality.getCodeValue()
    }
        // getCodeValue() is a Rialto scripting function for XDS objects


    // Ensure that the list of modalities in this study has unique entries
    xdsModalities = xdsModalities as Set
    log.debug("Cleaned Up Event Codes (Modalities In Study) from the XDS Registry:\n{}", xdsModalities)



    /* Don't change the modalities from what's in the DIR for local studies */
    if( SharedConstants.Local_PACS_Issuer_of_Patient_ID == originalAssigningAuthority){
        log.debug("The Study Instance UID for this study was: {}", get(StudyInstanceUID))
        log.debug("The IssuerOfPatientID received from XDS Registry was: {}.  This patient is from the local PACS.", originalAssigningAuthority )

        // Use DICOM modalities in study in the DIR as opposed to the XDS Registry's if there are valid DI-r modalities

        if(dirModalities != null &&  ! dirModalities.isEmpty() ){
            // The local PACS that published the modalities directly to the DIR, so they should be okay
            finalModalities = dirModalities

        } else {
            finalModalities = dirModalities
            finalModalities.addAll(xdsModalities)
            finalModalities = finalModalities as List
            log.debug("For LOCAL Study, DI-r Modalities in Study were blank.  " +
                "Merged XDS Event Codes and DIR Modalities in Study: \"{}\" ", finalModalities) 
        }


    } else {
      /* For Foreing Studies, merge the modalities found in "Modalities In Study" in the DIR, with the modalities in the XDS Event Codes in the Registry */

        log.debug("The IssuerOfPatientID received from XDS Registry was: {}.  This patient is foreign", originalAssigningAuthority )
        
        // Merging the unique Modalities contained in both the XDS Registry and the DIR that holds this study
        finalModalities = dirModalities
        finalModalities.addAll(xdsModalities)
        finalModalities = finalModalities as List
        log.debug("For Foreign Study, merged XDS Event Codesand DIR Modalities in Study: \"{}\"", finalModalities)


        // Substitute known Secondary Modality types for Primary types
        def evaluateFinalModalities = finalModalities
            // A second iterator is needed to not dynamically modify the iteration list itself

        for(eachModality in evaluateFinalModalities){

            if(Modalities.secondaryToPrimaryModalitiesMap[eachModality] != ""){
            // If a given modality happens to be a secondary modality, replace it with the preferred modality
                Collections.replaceAll(finalModalities, eachModality,  Modalities.secondaryToPrimaryModalitiesMap[eachModality])
            }
        }
        log.debug("Modalities after substitution of Preferred Modalities: \"{}\"", finalModalities)




        // Use if they only want 1 (dominant) modality per study
        if(giveBackOnlyOneModalityperStudy){

            def dominantModalityInThisStudy = null

            if( !(finalModalities instanceof String) ) {
            // Check if the final modalities is a string or a list

                log.debug("This is the final modality size: \"{}\"", finalModalities.size())
                if(finalModalities.size() > 1) {
                // Determine Dominant Modality for a multi-modality study

                    log.debug("There is more than 1 modality currently for this Study, disambiguating.")
                    dominantModalityInThisStudy = Modalities.determineDominantModalityFromList(finalModalities)
                    // Determine the Dominant Modality for a given study based on the Hierarchical Modality list
                    
                    log.debug("The dominant modality for this study is \"{}\"", dominantModalityInThisStudy)

                    // If the Study Doesn't pass the Dominant modality rule, sink it here
                    if(dominantModalityInThisStudy == "") {
                    // The Dominant Modality rule did not return anything
                        log.warn("The dominant modality rule produced no results for \"{}\".  Dropping this C-FIND Response.", finalModalities)
                        return false
                    }

                }  // End of if for more than 1 modality in modality list

                else if(finalModalities.size() > 0) {
                // Modality list only has 1 modality value
                    dominantModalityInThisStudy = finalModalities[0]
                    log.debug("Single Modality List, using only value: \"{}\"", dominantModalityInThisStudy) 
                
                } else {
                // Modality list is empty
                    log.warn("Empty modality list for Study Instance UID \"{}\".  Dropping this Ad Hoc C-FIND response.", get(StudyInstanceUID))
                    return false
                }

                finalModalities =  dominantModalityInThisStudy
                // This is the dominant modality in the study

            } // End of check whether merged XDS Reg and DIR modalities were not a single string
        
            // TODO there used to be code here that checked for a string finalModality, but it's no longer needed


        } else {
          // Give back multiple modalities, but exclude specific items from a list of unwanted modality types
          //  Note, this code is still needed, because the Dominant Modality List check could still produce
          //    studies whose only modality (or dominant modality) is not something that the PACS wants.

            log.debug("Cleaning up Multi-modalities in Study to only those modalities that the PACS wants.")
            def temporaryFinalModalities = finalModalities as List
            temporaryFinalModalities = temporaryFinalModalities - SharedConstants.Unwanted_Modalities_in_AdHoc_CFIND_Responses
            finalModalities = temporaryFinalModalities
            log.debug("Passing the cleaned up set of modalities to PACS:\n[{}]", finalModalities)
        }

        

     /*  Drop the "Cleaned Up" response if the only modality is something not wanted by the PACS */
        //TODO This code needs to look more like the ELSE statement below
        if(!(finalModalities instanceof String) ) {
          // finalModalities is a list 
            if(SharedConstants.Unwanted_Modalities_in_AdHoc_CFIND_Responses.contains(finalModalities) ){
                log.info("AD HOC QUERY RESPONSE to PACS would have contained an item not wanted by the PACS: \"{}\".  Dropping Response.", finalModalities)
                return false
            }

        } else {
            // finalModalities is a string
            for(aModalityToDrop in SharedConstants.Unwanted_Modalities_in_AdHoc_CFIND_Responses){ 
                if(finalModalities == aModalityToDrop) { 
                    log.info("Dropping the *only* Dominant Modality or Backup Dominant Modality value for this study. " +
                    "It would have contained an item not wanted by the PACS.  Will Drop this response. Study Instance UID =[{}]; Accession " +
                    "number =[{}].", get(StudyInstanceUID), get(AccessionNumber))
                    return false 
                }
            }
        }

    }  // End of Foreing Studies handling logic


    // Setting the Final Modalities for Foreign or Local Study
    log.debug("Final merged Modalities in Study; responding to PACS:\n{}", finalModalities)
    set(ModalitiesInStudy, finalModalities)





//  Log monitoring of the events in this morpher
log.trace("CFind response after morphing:\n{}", output)

log.debug("\n\n**Ending C-Find Response Localizer Script**\n")
