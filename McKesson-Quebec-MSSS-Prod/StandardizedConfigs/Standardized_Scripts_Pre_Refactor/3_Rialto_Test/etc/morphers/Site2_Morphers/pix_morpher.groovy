/* pix_morpher.groovy
*
*   This script is the first script to run after an HL7 message arrives at the Rialto HL7 server.  It passes the message to the FPS morpher afterward.
*
*   This script Handles different types of HL7 Messages to Populate the PIX Database that Rialto Connect uses 
*       for RAMQ to Local Patient ID (and viceversa) resolution
*
*
*   Currenlty, for HSC, the script is doing the following:
*       1) Adding the Default Character Set to Pass French Characters Correctly to the PIX Handler (ADT Handler)
*       2) Checking for *BOTH* a Local MRN Value and a RAMQ Value (and failing the message if they are absent)
*           This step also sets them both where the PIX Handler is expecting them
*       3) Hard-coding Issuer of Patient IDs for *BOTH* RAMQs and Local MRNs (needed by the PIX Resolution)
*            The only exception to this is when the RIS is sending an explicitly-coded Patient for a different Domain
*            In this case, the "Foreing" MRN will be added to the PIX database.
*            However, if the local PACS attempts to use this patient without an Explicit Issuer of Patient ID, the PIX lookup will fail
*                TODO This might cause collisions, check the logs to see this is not happening
*       4) Make ("fake") all non-ADT messages into ADT-A^31s 
*
*
*/

/* ========= Shared Files Loading ============= */
    LOAD("/home/rialto/rialto/etc/morphers/Shared_Scripting_Files/shared_constants.groovy")
    // Contains commonly-used constants as attributes of the "SharedConstants" class
    // For example: SharedConstants.rialtoRetrieveAETitle  (see the file for full details)

    LOAD("/home/rialto/rialto/etc/morphers/Shared_Scripting_Files/shared_hl7_constants.groovy")


/* ================  Script  ================  */


// Trace info
    log.trace("Starting PIX morpher")
    log.trace("Message as received at HL7 Proxy: [{}]", input)

    def hl7MessageType = get('MSH-9-1')
    hl7MessageType = hl7MessageType.toUpperCase()
    log.trace("Local ADT Forwarder: Received MSH-9-1 (HL7 Message Type) was: [{}]", hl7MessageType)

    def hl7MessageSubType = get('MSH-9-2')
    hl7MessageSubType = hl7MessageSubType.toUpperCase()
    log.trace("Local ADT Forwarder: Received MSH-9-2 (HL7 Message SUB Type) was: [{}]", hl7MessageSubType)


// Short cirtcuit the message (whatever form of HL7 traffic this might be) if the RAMQ values are not valid, to protect the PIX lookup from
//  sending invalid values to the XDS Registry
    def MRN = get(SharedConstants.HL7_Local_MRN_PID_Field)
    def AffinityPID = get(SharedConstants.HL7_Affinity_Domain_PID_Field)
    // Both of these variables are used elsewhere throughout the script, please DO NOT DELETE THEM OR COMMENT THEM OUT

    if(AffinityPID != null) {
    // It is essential to let empty string or null RAMQs fall through unaffected
        if( !( AffinityPID.isEmpty() ) ){
            if(QuebecHL7Validation.RAMQformatIsInvalid(AffinityPID)){
                log.error("Received an Invalid RAMQ [{}] for an HL7 message type [{}].  Not populating internal PIX from this message.", AffinityPID, hl7MessageType)
                return false
            }
        }
    }


// Only allow ADT subtypes that the PIX morpher can handle - i.e. A01, A04, A05, A08 and A31
    if("ADT" == hl7MessageType){
        log.debug("True ADT received from RIS Feed")

        if(! SharedHl7Constants.supported_ADT_subtypes.contains(hl7MessageSubType)) {
            log.debug("Unsupported ADT subtype found - ignoring.")
            return false
        }
    }


// Add the default Character set that can handle French Characters, and that many times is not explicit in the RIS Feed
    def characterEncoding = get("MSH-18")

    if (characterEncoding == null || characterEncoding.isEmpty()){
        log.trace("No character set specified.  Hard-coding to 8859/1")
        set('MSH-18', '8859/1')
    }



/* Enforce that *BOTH* a Local MRN and an Affinity Domain (i.e. RAMQ) PID are included in the incoming messages.
    Drop messages if they don't */
/* NOTE!!: Although the Fetch Prior Studies workflow can work without an RAMQ, THIS SCRIPT **MUST** have *BOTH* RAMQ and MRN
    to populate the PIX database correctly. */

/* Then Construct a valid Message with the fully-qualified MRN and full-qualified Affinity Domain to populate the PIX feed */ 


    log.trace("Current MRN received is: [{}]", MRN)
    log.trace("Current RAMQ received is: [{}]", AffinityPID)


    // Enforce that the RAMQ and MRN both exist in the incoming message
    if(MRN == null){
        log.trace("Received Message DOES NOT contain a local MRN.  Ignoring this HL7 Message")
        return false

    } else if (AffinityPID == null){
        log.trace("Received Message DOES NOT contain an Affinity Domain.  Ignoring this HL7 Message")
        return false

    } else {
       // Both MRN and Affinity PID have a value

        if((! MRN.isEmpty()) && (! AffinityPID.isEmpty()) ){   // Both RAMQ and MRN are valid
         // Set the unqualified MRN and Affinity PID  where the PIX Handler (ADT Handler) is expecting them

            MRN=MRN.toUpperCase()
            set(SharedConstants.HL7_Local_MRN_PID_Field, MRN)
            log.trace("Using Local MRN \"{}\"", MRN)

            AffinityPID=AffinityPID.toUpperCase()
            set(SharedConstants.HL7_Affinity_Domain_PID_Field, AffinityPID)
            log.trace("Using Affinity Domain PID (RAMQ) \"{}\"", AffinityPID)

        } else if(MRN.isEmpty() && AffinityPID.isEmpty() ){
           // Cannot use this message to populate the PIX feed.  Discard it.

            log.trace("Received Message containst NEITHER a local MRN or a RAMQ.  Ignoring this HL7 Message")
            return false

        } else if(MRN.isEmpty()){  //Calling isEmpty() on a null object results in an exception
            log.warn("Received Message DOES NOT contain a local MRN.  Ignoring this HL7 Message")
            return false

        } else if(AffinityPID.isEmpty()){
            log.trace("Received Message DOES NOT contain an Affinity Domain.  Ignoring this HL7 Message")
            return false
        }

    }
    log.trace("Leaving enforcing statements, there better be enforcing output before this")




/* Ensure that BOTH the Local MRN and the Affinity Domain (RAMQ) are fully qualified */


    log.trace("Issuer of Patient ID for MRN as received is: [{}]", MRN_IssuerOfPID)
    log.trace("Issuer of Patient ID for Affinity Domain PID (RAMQ) as received is: [{}]", AffinityDomain_IssuerOfPID)


    // Hard code Issuer of PIDs for both MRN and Affinity Domain PID

    // First, Hard Code Issuer of Patient ID for MRN
    if(null != MRN_IssuerOfPID){

        if(MRN_IssuerOfPID.isEmpty()) {  // If the Local Issuer of Patient ID at the PACS is empty, populate it from known value
            MRN_IssuerOfPID = SharedConstants.Local_PACS_Issuers_of_Patient_ID[1]
            log.trace("Populating Issuer of Patient ID for MRN from Local PACS as [{}]", MRN_IssuerOfPID)

        } else if( SharedConstants.Local_PACS_Issuers_of_Patient_ID[1] != MRN_IssuerOfPID.toUpperCase() ) { // This means that the Issuer of PID passed by RIS does not match the one known as correct
            /* THIS ONLY WORKS IN Connect Instances attached to SINGLE-CONTEXT PACS */
            log.warn("The received Issuer of Patient ID for the local PACS received was \"{}\".  The expected value was \"{}\"." +
                "This RIS Feed is sending feeds for more than 1 local domain. \n Patient Lookups might fail and go to RIS Database!"
                + "\n This could be Potentially Dangerous", MRN_IssuerOfPID, SharedConstants.Local_PACS_Issuers_of_Patient_ID)

            //TODO for right now, to hack the messages for testing with dubious data
                MRN_IssuerOfPID = SharedConstants.Local_PACS_Issuers_of_Patient_ID[1]

        } else {
            log.trace("The Issuer of PID correctly matches expected value.")
        }

    } else {
       // Issuer of Patient ID for local MRN is null (expected on some RIS Feeds)
        MRN_IssuerOfPID = SharedConstants.Local_PACS_Issuers_of_Patient_ID[1]
        log.trace("Populating Issuer of Patient ID for MRN from Local PACS as [{}]", MRN_IssuerOfPID)
    }


    // Second, Hard Code Issuer of Patient ID for Affinity Domain PID (i.e. RAMQ)
    if(null != AffinityDomain_IssuerOfPID) {

        if(AffinityDomain_IssuerOfPID.isEmpty() ){   //Populate with known value if empty
            AffinityDomain_IssuerOfPID = SharedConstants.HL7_Affinity_Domain_Issuer_of_PID_Field
            log.trace("Populating Issuer of Patient ID for AffinityDomain Patient (RAMQ) as [{}]", AffinityDomain_IssuerOfPID)

        } else if( SharedConstants.Affinity_Domain_OID != AffinityDomain_IssuerOfPID.toUpperCase() ){  // i.e. RAMQ Issuer Of PID is present, but not correct
            log.warn("Invalid Affinity Domain (RAMQ) Issuer of Patient ID [{}].  Populating to [{}]", AffinityDomain_IssuerOfPID, SharedConstants.Affinity_Domain_OID)
            AffinityDomain_IssuerOfPID = SharedConstants.Affinity_Domain_OID 
                // We can safely hard-code this value here

        } else {
            log.trace("The Affinity Domain Issuer of Patient ID is what is expected")
        }

    } else {
       // Affinity Domain Issuer of PID is blank

        AffinityDomain_IssuerOfPID = SharedConstants.Affinity_Domain_OID
        log.trace("Populating Issuer of Patient ID for AffinityDomain Patient (RAMQ) as [{}]", AffinityDomain_IssuerOfPID)
    }


    // Set the value of the MRN and AffinityDomain PID's Issuers of Patient ID where the PIX Server is expecting it
    set('PID-3-4-2', MRN_IssuerOfPID)
    set('PID-2-4-2', AffinityDomain_IssuerOfPID)
        // PID-3-4-2 and PID-2-4-2 are the HL7 Standard, and are where the PIX handler is ultimately expecting these values 

    log.trace("Issuer of Patient ID for MRN that will be used is: [{}]", MRN_IssuerOfPID)
    log.trace("Issuer of Patient ID for Affinity Domain PID (RAMQ) that will be used is: [{}]", AffinityDomain_IssuerOfPID)

    log.trace("This is the full Patient Info being passed to the PIX handler:\n MRN: [{}], MRN Issuer [{}], " +
                "RAMQ: [{}], RAMQ Issuer [{}].", MRN, MRN_IssuerOfPID, AffinityPID, AffinityDomain_IssuerOfPID )


/* Change ORMs and other Message Types to ADT A^31 to populate the PIX data */
    def convertedMessageType = 'ADT'
    def convertedMessageSubType = 'A31'

    if("ADT" != hl7MessageType){

        convertedMessageType = 'ADT'
        convertedMessageSubType = 'A31'

        set('MSH-9-1', convertedMessageType)
        set('MSH-9-2', convertedMessageSubType)

    } else {
      // The message type is ADT

      if(hl7MessageSubType != null) {

        if(hl7MessageSubType == "A40") {
            convertedMessageType = 'ADT'
            convertedMessageSubType = 'A31'

            set('MSH-9-1', convertedMessageType)
            set('MSH-9-2', convertedMessageSubType)
        }
      }
    }


 // Tracing information 
    //  log.trace("This is the Conect of binding.variables {}", binding.variables)
    log.trace("This is the Object that will be passed to the PIX handler (ADT Handler): {}", output)

    log.trace("Ending the PIX morpher")
