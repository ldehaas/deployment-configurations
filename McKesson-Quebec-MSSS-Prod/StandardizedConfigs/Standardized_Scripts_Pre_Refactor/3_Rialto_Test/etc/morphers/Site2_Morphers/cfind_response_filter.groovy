/* CFind Response Filter                                                                                                                                                                                            
*
*   This morpher is part of the Fetch Prior Studies workflow only.
*
*   It takes an individual study (CFind Responses from the DIRs, as originally listed in the XDS Registry) and optionally
*   filters it out studies based on different criteria, so they are not given to the local PACS as part of a Fetch
*   Prior Studies workflow.
*
*   (There is a different, though related script, CFind Response Rank Filter, that can filter studies based on study age
*   or total number of responses)
*
*   Common Uses for this Cfind Response Filter script can be:
*
*       - Exclude or include local studies (i.e. local to the PACS)
*
*       - Rule out studies based on Modality (if a site never wants to prefetch modality x or y, it can be achieved here)
*               Note: this type of modality filtering can only be black or white (i.e. absolute).
*                   Setting a modality to be discarded here will --ALWAYS-- get dropped in a Fetch Prior Studies workflow.
*                   To set up modality filtering that, say, only matches the passed modality in the HL7 order, this must be
*                   set in the 'HL7 to CFind' morpher script.
*
*       - Rule out specific anatomic regions (same 'absolute filering' rules as above apply)
*
*
*   ===================================================================================================
*   For Hopital du Sacre Coeur in Montreal, this script is currently doing the following:
*       1. Only processing FOREIGN priors. That is, not handling studies that are local to the PACS
*
*/


/* ========= Shared Files Loading ============= */
    LOAD("../Shared_Scripting_Files/shared_constants.groovy")
        // Contains commonly-used constants as attributes of the "SharedConstants" class
        // For example: SharedConstants.rialtoRetrieveAETitle  (see the file for full details)



/* =========  Constant Declaration  ================ */



/* ================  Script  ================  */

 // [debug] Print some context information for debugging purposes
    log.debug("\n\n**Starting Fetch Priors General FILTER Script**\n")
    log.debug("This is the received object for this script: \n {}", input)
    log.debug("The current Retreive AETitle from the the DIR is \"{}\".", get(RetrieveAETitle))
    log.debug("The Study Instance UID, as retreived from the DIR was \"{}\".\n", get(StudyInstanceUID))


 /* Ignore local studies */
    def sourceIssuer = get(IssuerOfPatientID)
    log.debug("The Issuer of Patient ID, as retreived from the XDS Reg / DIR (check preceding lines)  was \"{}\".", sourceIssuer)

    if( SharedConstants.Local_PACS_Issuer_of_Patient_ID == sourceIssuer){
        log.debug("This is considered a LOCAL study")
        log.debug("This is a Local Study. Discarding this study.  You should see *nothing* in the object output for this study.  The Event Viewer should say \"(Ignored)\".")
        return false
/*
  // temporarlily not using this code because too many studies have the source issuer null
    } else if(sourceIssuer == null || sourceIssuer.isEmpty() ) {
        log.error("EMPTY Issuer of Patient ID, as published in the DIR.  Cannot ascertain whether this is a local or foreign study.  **ABORTING STUDY TRANSFER**")
        return false
*/    
    } else {
        log.debug("Processing study, as it is deemed a FOREIGN PRIOR.  This might not be true, depending on whether DI-r is returning IssuerOfPID.  Checks in Localizer script to filter locals.")
    }

// [debug] Print some context information as the object is being passed to the localizer
    log.debug("This is the Object that the \"Fetch Priors Response Filter\" (Fetch Prior Studies) is passing to the \"Foreign Image Localizer\": {} \n\n\n **End of Object**", output)
    log.debug("\n\n**Ending Fetch Priors General FILTER Script**\n")
