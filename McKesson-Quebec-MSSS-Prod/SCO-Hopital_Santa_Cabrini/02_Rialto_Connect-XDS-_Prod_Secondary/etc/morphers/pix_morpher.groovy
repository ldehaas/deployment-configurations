/* pix_morpher.groovy
 *
 * This script Handles different types of HL7 Messages to Populate the PIX Database that Rialto Connect uses 
 * for RAMQ to Local Patient ID resolution (and viceversa)
 */

LOAD("Shared_Scripting_Files/shared_constants.groovy")
LOAD("Shared_Scripting_Files/shared_hl7_resources.groovy")
LOAD("Shared_Scripting_Files/contexts.groovy")

log.trace("PIX Morpher received message:\n{}", input)

def messageCode = get('MSH-9-1')
def triggerEvent = get('MSH-9-2')
log.debug("Local ADT Forwarder: Received message type: '{}^{}'", messageCode, triggerEvent)

// Only allow ADT subtypes that the PIX morpher can handle
if (messageCode == "ADT") {
    log.debug("True ADT received from RIS Feed")

    if (!SharedHl7.SupportedADTTriggerEvents.contains(triggerEvent)) {
        log.debug("Unsupported ADT subtype found - ignoring.")
        return false
    }
} else if (messageCode == "ORM") {
    // mini pix can handle ORMs:
    log.debug("Received order from RIS feed. Processing.")

} else {
    log.debug("Unsupported message type for pix.  Dropping message.")
    return false
}

SharedHl7.normalizeCharset(input, log)

if (!SharedHl7.normalizePids(input, log, config.context, "pix")) {
    return false
}

log.debug("This is the message that will be passed to the PIX handler:\n{}", output)

