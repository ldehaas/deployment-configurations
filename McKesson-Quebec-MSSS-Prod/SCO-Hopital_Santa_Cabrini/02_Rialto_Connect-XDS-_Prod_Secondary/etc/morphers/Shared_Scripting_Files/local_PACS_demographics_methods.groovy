
class Demographics{

    static localDemographicsExistAtPACS(){
      // Returns true if local Demographics exist at the PACS for this Patient; false if they don't

        if (localDemographics == null && localDemographics.isEmpty() ) {
            return false
        } else {
            return true
        }

    }


    static getPatientNameAtPACSorDIR(){
      // Returns the local Patient Name PACS if it exists, otherwise, it returns that populated by the DIR

      /* Note: For this method to work, you must have set the PatientName as a return key in your patient_cfind_request_morpher.groovy
            i.e.: set(PatientName, "") 
      */
      
        if (localDemographicsExistAtPACS) {
           
            // Return the local Patient Name at PACS if it exists
            if (localDemographics.get(0).get("PatientName") != null && !localDemographics.get(0).get("PatientName").isEmpty() ) {
                return localDemographics.get(0).get("PatientName")  
            
            // If it doesn't, return the DIR's
            } else {
                log.debug("The PACS has local Demographics for this Patient, however no Patient Name was found."
                + "Using the DIR's Patient Name.")

                return get(PatientID)
            }

        // Return the DIR's value if the localDemographics at the PACS are not available
        } else {
            log.debug("There are no Demographics for this Patient at the PACS.  Using the DIR's.")
            return get(PatientID)
        }
    }



} // end of Demographics Class
