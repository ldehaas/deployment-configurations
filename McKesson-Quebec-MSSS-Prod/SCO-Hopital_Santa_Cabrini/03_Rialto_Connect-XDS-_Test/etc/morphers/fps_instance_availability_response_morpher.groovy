log.info("I am the FPSInstanceAvailabilityResponseMorpher")

LOAD("../morphers/Shared_Scripting_Files/contexts.groovy")

context = Contexts.getContext(config.context)
if (context == null) {
    log.warn("Context is unknown from id: {}. Unable to unlocalize this request, passing it through unchanged. " +
        "Please correct either the connect ContextID configuration or contexts.groovy", config.context)
    return true
}

log.info("callingAETitle: {}", _callingAETitle)
if (_callingAETitle.contains('_HASHED_UIDS')) {
        log.info("unhashing")
        set(StudyInstanceUID, context.unlocalizeUID(get(StudyInstanceUID), unhash))
}
log.info("This FPSInstanceAvailabilityResponseMorpher is DONE!")
