/*
 * HL7 to CFind Morpher
 *
 * Takes an incoming RIS HL7 ORM order, and converts it to a DICOM C-Find Query Request.
 *
 * This script is part of the Fetch Prior Studies workflow.
 */

LOAD("Shared_Scripting_Files/shared_constants.groovy")
LOAD("Shared_Scripting_Files/contexts.groovy")
LOAD("Shared_Scripting_Files/fetchpriors_logic.groovy")

final SilenceNoisyRISWithNoSendingFacility = false

context = Contexts.getContext(config.context)
if (context == null) {
    log.error("Context is unknown from id: {}. Unable to convert this order, dropping it. " +
        "Please correct either the connect ContextID configuration or contexts.groovy", config.context)
    return false
}

log.trace("Received HL7 Order: {}", input)

// These are the Key Fields in an incoming ORM Order

// Unique Identifier for an Order.  Also known as the "Order Filler Number"
def orderAccessionNumber = get('ORC-3')

def mrn = get(SharedConstants.HL7_Local_MRN_PID_Field)

// RAMQ
def affinityPID = get(SharedConstants.HL7_Affinity_Domain_PID_Field)

def modalityOfCurrentOrder = get(SharedConstants.HL7_Order_Modality_Field) 

def currentAnatomicRegion = get(SharedConstants.HL7_Order_Anatomic_Region_Field)

// The Procedure Code is also the "Exam" that is being performed
def examProcedureCode = get('OBR-4-1')

def orderStatus = get('ORC-16')
if (orderStatus != null) { 
    orderStatus = orderStatus.toUpperCase()
}

def orderControlCode = get('ORC-1')


// Log the Key Fields in Incoming Order
log.info("FPS: Received HL7 Order. Accession Number: '{}', MRN: '{}', RAMQ: '{}', Modality: '{}', " +
         "Anatomic Region: '{}', Exam Procedure Code: '{}', Order Status: '{}', " +
         "OrderControlCode:'{}'.", 
         orderAccessionNumber,
         mrn,
         affinityPID,
         modalityOfCurrentOrder,
         currentAnatomicRegion,
         examProcedureCode,
         orderStatus,
         orderControlCode)

    
// Only process orders for which the Order Status is a valid trigger for Fetch Prior Studies 
if (!PriorsLogic.ValidOrderStatuses.contains(orderStatus)) {
    log.info("Received an HL7 Order with a status of '{}'.  Not a value expected to cause a Fetch Prior Studies. Ignoring", orderStatus)
    return false
}


// Only process orders whose Modality is known and valid
if (!PriorsLogic.ValidModalities.contains(modalityOfCurrentOrder)) {
    log.warn("Unknown Modality '{}' for Order with Accession Number '{}', RAMQ '{}', MRN'{}'.  Ignoring.",
              modalityOfCurrentOrder, orderAccessionNumber, affinityPID, mrn)
    return false
}

// Only process orders whose Anatomic Region is known and valid
if (!PriorsLogic.ValidAnatomicRegions.contains(currentAnatomicRegion)) {
    log.warn("Unknown Anatomic Region '{}' for Order with Accession Number '{}', RAMQ '{}', MRN'{}'.  Ignoring.",
              currentAnatomicRegion, orderAccessionNumber, affinityPID, mrn)
    return false
}

// Only process orders whose Control Code should cause a prefetch
if (PriorsLogic.InvalidFetchOrderStatuses.contains(orderControlCode)) {
    log.info("The Order Status of '{}' should NOT cause a prefetch.  Ignoring Order.", orderControlCode)
    return false
}

// Copy the Patient ID to the C-FIND; derive the Issuer of Patient ID and populate it in the C-FIND

def sendingFacility = get('MSH-4')
def localMRN = get(SharedConstants.HL7_Local_MRN_PID_Field)

set(PatientID, localMRN)
log.debug("Current Patient ID used is '{}'", localMRN)

// Set the local Affinity Domain so that the PIX mechanism can find the RAMQ.
// Currently missing in the RIS HL7 ORM Feed ??

if (context.RISSendingFacility.contains(sendingFacility)) {
    log.debug("The PIX will look up an explicitly local patient from {}.", context.InstitutionName)

} else {    // Sending Facility is not what is expected

    if (sendingFacility != null && !sendingFacility.isEmpty()) { 
        // Sending Facility is from a Different Domain
        log.warn("Received unexpected Sending Facility (MSH-4) of '{}'. Expected '{}'.",
                 sendingFacility, context.RISSendingFacility)
        log.warn("This RIS Feed is sending feeds for more than 1 local domain. " +
                 "Patient Lookups might fail and go to RIS Database! " +
                 "This could Potentially Resolve to a different, Local Patient!!")

    } else {    
        // Sending Facility is simply empty
        
        // Turn off this overly verbose message if the RIS doesn't regularly populate a Sending Facility
        logMethod = SilenceNoisyRISWithNoSendingFacility ? log.&trace : log.&warn;
        logMethod("Fetch Prior Studies: The MSH-4 Sending Facility in this HL7 ORM Message was blank! " + 
                  "The PIX will assume assuming this Patient is from {}",
                  context.InstitutionName)
    }
}

set(IssuerOfPatientID, context.PACSIssuer)


/* Set the Correct Modality to Retreive.
 * In this project the initial relevant set of priors is that which matches the same Modality
 * and same Anatomic Region as the incoming ORM order */

// Copy Current Modality in HL7 Order
if (modalityOfCurrentOrder != null) {
    set(ModalitiesInStudy, modalityOfCurrentOrder)
    log.debug("Setting modalities in study to {}", modalityOfCurrentOrder)

} else {
    log.warn("No Modality populated in the order.  Will pass the Fetch Priors unconstrained. " +
        "This will result in an unusually large number of Priors fetched from the Registry.")
}

// Copy current Anatomic Region in HL7 Order
if (currentAnatomicRegion != null) {
    set(BodyPartExamined, currentAnatomicRegion)
    log.debug("Setting Normalized Anatomic Region from HL7 to C-FIND as: {}", currentAnatomicRegion)

} else {
    log.warn("No Anatomic Region populated in the order.  Will pass the Fetch Priors unconstrained." +
        "This might result in an unusually large number of Priors fetched from the Registry.")
}

if (modalityOfCurrentOrder ==  null  && currentAnatomicRegion == null) {
    log.error("Missing both Anatomic Region or Modality in the order. " +
              "The Fetch Prior Studies order will be ignored.")

    return false
}


log.debug("Fetch Prior Studies: The is the Current C-Find being generated:\n{}", output)

