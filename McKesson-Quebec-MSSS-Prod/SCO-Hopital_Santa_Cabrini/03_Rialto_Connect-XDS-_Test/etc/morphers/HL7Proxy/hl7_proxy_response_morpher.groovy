/*
 * Modifies responses to be sent to the HL7 RIS Feeds
 */

def timestamp = get('MSH-7')

if (null != timestamp) {
    log.trace("Removing sub-second part of the time.  The previous timestap was '{}'", timestamp)
    timestamp = timestamp.replaceAll("\\..*", "")
    log.trace("The current timestap is '{}'", timestamp)
    set('MSH-7', timestamp)
}


// Respond with a positive acknowledgement no matter what the outcome of the forwarding event is
// This saves the RIS queue
set('MSA-1', 'AA')

log.trace("Outputting HL7 Proxy Response Message to Caller:\n{}", output)
