class HL7Proxy {
    /**
     * @returns whether or not the message should be forwarded for the given context
     */
    public static boolean messageMatchesContext(msg, contextId, log) {
        def context = Contexts.getContext(contextId)
        if (context == null) {
            log.error("Context is unknown from id: {}. Please configure your hl7 proxy scripts " +
                      "to match a context in contexts.groovy", contextId)
            return false
        }

        if (msg.get('MSH-18') == null) {
            // Weird to do this here, since this method is just evaluating the message,
            // but it's the easiest way without modifying each site's morpher.  This
            // matches the default we use to interpret the message when receiving it
            // (see hl7 server configuration) but is needed in the message when serializing
            // it to send it on properly:
            msg.set('MSH-18', '8859/1')
            log.trace('Set default character encoding of incoming message to 8859/1')
        }

        def app = msg.get('MSH-3')
        def fac = msg.get('MSH-4')
        def matches = context.RISSendingApplication.contains(app) &&
                      context.RISSendingFacility.contains(fac)

        log.debug("Expected sending application / facility: {} / {} {} actual: {} / {}. {} message to context {}",
            context.RISSendingApplication,
            context.RISSendingFacility,
            matches ? "matches" : "doesn't match",
            app,
            fac,
            matches ? "Forwarding" : "Not forwarding",
            context.ContextID)

        return matches
    }
}


