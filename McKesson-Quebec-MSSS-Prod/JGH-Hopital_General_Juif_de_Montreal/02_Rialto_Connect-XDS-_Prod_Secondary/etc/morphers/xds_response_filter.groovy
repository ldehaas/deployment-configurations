/*
 * This script will filter all priors against a set of rules that will determine
 * what priors are relevant based on the new order.
 * This runs immediately after the XDS Responses arrive from the XDS Registry, but before the DIR is queried.
 */

LOAD("Shared_Scripting_Files/contexts.groovy")
LOAD("Shared_Scripting_Files/shared_xds_modality_and_anatomic_region_routines.groovy")
LOAD("Shared_Scripting_Files/shared_constants.groovy")

// weird hack to make the log variable accessible inside PriorsRules
// TODO: find a better solution to accessing global scope objects in classes
class Log {
    static log = null
}
Log.log = log

/**
 * Rules table and evaluation logic for prior studies.
 *
 * For each modality and anatomical region pair, there are a set of sub rules.
 * Each subrule defines the modality and anatomic region of a candidate prior,
 * as well as the maximum age and maximum number of the prior.
 */
class PriorsRules {
    static log = Log.log

    private static rules = [

      //  Rules for CT and MR
      [ modality : [ "CT", "MR" ], anatomicRegion : "TC", subrules :
        [
          [ modality : [ "CT", "MR" ], anatomicRegion : "TC", maxAge : 0, newest : 0, oldest : 1 ],
          [ modality : [ "CT", "MR" ], anatomicRegion : "TC", maxAge : 4, newest : 4, oldest : 0 ],
          [ modality : [ "PT" ], anatomicRegion : "TP", maxAge : 0, newest : 0, oldest : 1 ],
          [ modality : [ "PT" ], anatomicRegion : "TP", maxAge : 4, newest : 4, oldest : 0 ],
          [ modality : [ "US" ], anatomicRegion : "TC", maxAge : 0, newest : 0, oldest : 1 ],
          [ modality : [ "US" ], anatomicRegion : "TC", maxAge : 4, newest : 4, oldest : 0 ],
        ],
      ],
      [ modality : [ "CT", "MR" ], anatomicRegion : "DI", subrules :
        [
          [ modality : [ "CT", "MR" ], anatomicRegion : "DI", maxAge : 0, newest : 0, oldest : 1 ],
          [ modality : [ "CT", "MR" ], anatomicRegion : "DI", maxAge : 4, newest : 4, oldest : 0 ],
          [ modality : [ "CT", "MR" ], anatomicRegion : "AB", maxAge : 3, newest : 3, oldest : 0 ],
          [ modality : [ "CR" ], anatomicRegion : "DI", maxAge : 3, newest : 3, oldest : 0 ],
          [ modality : [ "CR" ], anatomicRegion : "AB", maxAge : 3, newest : 3, oldest : 0 ],
          [ modality : [ "RF" ], anatomicRegion : "DI", maxAge : 3, newest : 3, oldest : 0 ],
          [ modality : [ "US" ], anatomicRegion : "AB", maxAge : 3, newest : 3, oldest : 0 ],
          [ modality : [ "US" ], anatomicRegion : "DI", maxAge : 3, newest : 3, oldest : 0 ],
          [ modality : [ "PT" ], anatomicRegion : "TP", maxAge : 0, newest : 0, oldest : 1 ],
          [ modality : [ "PT" ], anatomicRegion : "TP", maxAge : 4, newest : 4, oldest : 0 ],
        ],
      ],
      [ modality : [ "CT", "MR" ], anatomicRegion : "TH", subrules :
        [
          [ modality : [ "CT", "MR" ], anatomicRegion : "TH", maxAge : 0, newest : 0, oldest : 1 ],
          [ modality : [ "CT", "MR" ], anatomicRegion : "TH", maxAge : 4, newest : 4, oldest : 0 ],
          [ modality : [ "CR" ], anatomicRegion : "TH", maxAge : 3, newest : 3, oldest : 0 ],
          [ modality : [ "PT" ], anatomicRegion : "TP", maxAge : 0, newest : 0, oldest : 1 ],
          [ modality : [ "PT" ], anatomicRegion : "TP", maxAge : 4, newest : 4, oldest : 0 ],
        ],
      ],
      [ modality : [ "CT", "MR" ], anatomicRegion : "AB", subrules :
        [
          [ modality : [ "CT", "MR" ], anatomicRegion : "DI", maxAge : 0, newest : 0, oldest : 1 ],
          [ modality : [ "CT", "MR" ], anatomicRegion : "DI", maxAge : 4, newest : 4, oldest : 0 ],
          [ modality : [ "CT", "MR" ], anatomicRegion : "AB", maxAge : 3, newest : 3, oldest : 0 ],
          [ modality : [ "CR" ], anatomicRegion : "DI", maxAge : 3, newest : 3, oldest : 0 ],
          [ modality : [ "RF" ], anatomicRegion : "DI", maxAge : 3, newest : 3, oldest : 0 ],
          [ modality : [ "US" ], anatomicRegion : "AB", maxAge : 3, newest : 3, oldest : 0 ],
          [ modality : [ "US" ], anatomicRegion : "DI", maxAge : 3, newest : 3, oldest : 0 ],
          [ modality : [ "PT" ], anatomicRegion : "TP", maxAge : 0, newest : 0, oldest : 1 ],
          [ modality : [ "PT" ], anatomicRegion : "TP", maxAge : 4, newest : 4, oldest : 0 ],
        ],
      ],
      [ modality : [ "CT", "MR" ], anatomicRegion : "CO", subrules :
        [
          [ modality : [ "CT", "MR" ], anatomicRegion : "CO", maxAge : 0, newest : 0, oldest : 1 ],
          [ modality : [ "CT", "MR" ], anatomicRegion : "CO", maxAge : 4, newest : 4, oldest : 0 ],
          [ modality : [ "CR" ], anatomicRegion : "CO", maxAge : 3, newest : 3, oldest : 0 ],
          [ modality : [ "RF" ], anatomicRegion : "CO", maxAge : 3, newest : 3, oldest : 0 ],
          [ modality : [ "PT" ], anatomicRegion : "TP", maxAge : 0, newest : 0, oldest : 1 ],
          [ modality : [ "PT" ], anatomicRegion : "TP", maxAge : 4, newest : 4, oldest : 0 ],
        ],
      ],
      [ modality : [ "CT", "MR" ], anatomicRegion : "ES", subrules :
        [
          [ modality : [ "CT", "MR" ], anatomicRegion : "ES", maxAge : 0, newest : 0, oldest : 1 ],
          [ modality : [ "CT", "MR" ], anatomicRegion : "ES", maxAge : 4, newest : 4, oldest : 0 ],
          [ modality : [ "CR" ], anatomicRegion : "ES", maxAge : 3, newest : 3, oldest : 0 ],
          [ modality : [ "US" ], anatomicRegion : "ES", maxAge : 3, newest : 3, oldest : 0 ],
          [ modality : [ "PT" ], anatomicRegion : "TP", maxAge : 0, newest : 0, oldest : 1 ],
          [ modality : [ "PT" ], anatomicRegion : "TP", maxAge : 4, newest : 4, oldest : 0 ],
        ],
      ],
      [ modality : [ "CT", "MR" ], anatomicRegion : "EI", subrules :
        [
          [ modality : [ "CT", "MR" ], anatomicRegion : "EI", maxAge : 0, newest : 0, oldest : 1 ],
          [ modality : [ "CT", "MR" ], anatomicRegion : "EI", maxAge : 4, newest : 4, oldest : 0 ],
          [ modality : [ "CR" ], anatomicRegion : "EI", maxAge : 3, newest : 3, oldest : 0 ],
          [ modality : [ "US" ], anatomicRegion : "EI", maxAge : 3, newest : 3, oldest : 0 ],
          [ modality : [ "PT" ], anatomicRegion : "TP", maxAge : 0, newest : 0, oldest : 1 ],
          [ modality : [ "PT" ], anatomicRegion : "TP", maxAge : 4, newest : 4, oldest : 0 ],
        ],
      ],
      [ modality : [ "CT", "MR" ], anatomicRegion : "CA", subrules :
        [
          [ modality : [ "CT", "MR" ], anatomicRegion : "CA", maxAge : 0, newest : 0, oldest : 1 ],
          [ modality : [ "CT", "MR" ], anatomicRegion : "CA", maxAge : 4, newest : 4, oldest : 0 ],
          [ modality : [ "CR" ], anatomicRegion : "TH", maxAge : 3, newest : 3, oldest : 0 ],
          [ modality : [ "PT" ], anatomicRegion : "TP", maxAge : 0, newest : 0, oldest : 1 ],
          [ modality : [ "PT" ], anatomicRegion : "TP", maxAge : 4, newest : 4, oldest : 0 ],
        ],
      ],
      [ modality : [ "MR" ], anatomicRegion : "SE", subrules :
        [
          [ modality : [ "MR" ], anatomicRegion : "SE", maxAge : 0, newest : 0, oldest : 1 ],
          [ modality : [ "MR" ], anatomicRegion : "SE", maxAge : 5, newest : 4, oldest : 0 ],
          [ modality : [ "MG" ], anatomicRegion : "SE", maxAge : 5, newest : 5, oldest : 0 ],
          [ modality : [ "US" ], anatomicRegion : "SE", maxAge : 5, newest : 5, oldest : 0 ],
          [ modality : [ "PT" ], anatomicRegion : "TP", maxAge : 0, newest : 0, oldest : 1 ],
          [ modality : [ "PT" ], anatomicRegion : "TP", maxAge : 5, newest : 4, oldest : 0 ],
        ],
      ],
      [ modality : [ "CT", "MR" ], anatomicRegion : "GO", subrules :
        [
          [ modality : [ "CT", "MR" ], anatomicRegion : "GO", maxAge : 0, newest : 0, oldest : 1 ],
          [ modality : [ "CT", "MR" ], anatomicRegion : "GO", maxAge : 4, newest : 4, oldest : 0 ],
          [ modality : [ "CR" ], anatomicRegion : "GO", maxAge : 3, newest : 3, oldest : 0 ],
          [ modality : [ "US" ], anatomicRegion : "GO", maxAge : 3, newest : 3, oldest : 0 ],
          [ modality : [ "PT" ], anatomicRegion : "TP", maxAge : 0, newest : 0, oldest : 1 ],
          [ modality : [ "PT" ], anatomicRegion : "TP", maxAge : 4, newest : 4, oldest : 0 ],
          [ modality : [ "CT", "MR" ], anatomicRegion : "AB", maxAge : 0, newest : 0, oldest : 1 ],
          [ modality : [ "CT", "MR" ], anatomicRegion : "AB", maxAge : 4, newest : 4, oldest : 0 ],
          [ modality : [ "CR" ], anatomicRegion : "AB", maxAge : 3, newest : 3, oldest : 0 ],
          [ modality : [ "US" ], anatomicRegion : "AB", maxAge : 3, newest : 3, oldest : 0 ],

        ],
      ],

      // Rules for CR
      [ modality : [ "CR" ], anatomicRegion : "TC", subrules :
        [
          [ modality : [ "CR" ], anatomicRegion : "TC", maxAge : 0, newest : 0, oldest : 1 ],
          [ modality : [ "CR" ], anatomicRegion : "TC", maxAge : 3, newest : 3, oldest : 0 ],
        ],
      ],
      [ modality : [ "CR" ], anatomicRegion : "DI", subrules :
        [
          [ modality : [ "CR" ], anatomicRegion : "DI", maxAge : 0, newest : 0, oldest : 1 ],
          [ modality : [ "CR" ], anatomicRegion : "DI", maxAge : 3, newest : 3, oldest : 0 ],
          [ modality : [ "CR" ], anatomicRegion : "AB", maxAge : 3, newest : 2, oldest : 0 ],
          [ modality : [ "RF" ], anatomicRegion : "DI", maxAge : 3, newest : 2, oldest : 0 ],
          [ modality : [ "US" ], anatomicRegion : "AB", maxAge : 3, newest : 2, oldest : 0 ],
          [ modality : [ "US" ], anatomicRegion : "DI", maxAge : 3, newest : 2, oldest : 0 ],
        ],
      ],
      [ modality : [ "CR" ], anatomicRegion : "TH", subrules :
        [
          [ modality : [ "CR" ], anatomicRegion : "TH", maxAge : 0, newest : 0, oldest : 1 ],
          [ modality : [ "CR" ], anatomicRegion : "TH", maxAge : 3, newest : 3, oldest : 0 ],
          [ modality : [ "CT" ], anatomicRegion : "TH", maxAge : 3, newest : 1, oldest : 0 ],
        ],
      ],
      [ modality : [ "CR" ], anatomicRegion : "AB", subrules :
        [
          [ modality : [ "CR" ], anatomicRegion : "AB", maxAge : 0, newest : 0, oldest : 1 ],
          [ modality : [ "CR" ], anatomicRegion : "AB", maxAge : 3, newest : 3, oldest : 0 ],
          [ modality : [ "CR" ], anatomicRegion : "DI", maxAge : 3, newest : 2, oldest : 0 ],
          [ modality : [ "RF" ], anatomicRegion : "AB", maxAge : 3, newest : 2, oldest : 0 ],
          [ modality : [ "RF" ], anatomicRegion : "DI", maxAge : 3, newest : 2, oldest : 0 ],
          [ modality : [ "US" ], anatomicRegion : "AB", maxAge : 3, newest : 2, oldest : 0 ],
          [ modality : [ "US" ], anatomicRegion : "DI", maxAge : 3, newest : 2, oldest : 0 ],
        ],
      ],
      [ modality : [ "CR" ], anatomicRegion : "CO", subrules :
        [
          [ modality : [ "CR" ], anatomicRegion : "CO", maxAge : 0, newest : 0, oldest : 1 ],
          [ modality : [ "CR" ], anatomicRegion : "CO", maxAge : 3, newest : 3, oldest : 0 ],
          [ modality : [ "RF" ], anatomicRegion : "CO", maxAge : 3, newest : 2, oldest : 0 ],
        ],
      ],
      [ modality : [ "CR" ], anatomicRegion : "ES", subrules :
        [
          [ modality : [ "CR" ], anatomicRegion : "ES", maxAge : 0, newest : 0, oldest : 1 ],
          [ modality : [ "CR" ], anatomicRegion : "ES", maxAge : 3, newest : 3, oldest : 0 ],
        ],
      ],
      [ modality : [ "CR" ], anatomicRegion : "EI", subrules :
        [
          [ modality : [ "CR" ], anatomicRegion : "EI", maxAge : 0, newest : 0, oldest : 1 ],
          [ modality : [ "CR" ], anatomicRegion : "EI", maxAge : 3, newest : 3, oldest : 0 ],
        ],
      ],
      [ modality : [ "CR" ], anatomicRegion : "GO", subrules :
        [
          [ modality : [ "CR" ], anatomicRegion : "GO", maxAge : 0, newest : 0, oldest : 1 ],
          [ modality : [ "CR" ], anatomicRegion : "GO", maxAge : 3, newest : 3, oldest : 0 ],
          [ modality : [ "US" ], anatomicRegion : "GO", maxAge : 3, newest : 2, oldest : 0 ],
          [ modality : [ "CR" ], anatomicRegion : "AB", maxAge : 0, newest : 0, oldest : 1 ],
          [ modality : [ "CR" ], anatomicRegion : "AB", maxAge : 3, newest : 3, oldest : 0 ],
          [ modality : [ "US" ], anatomicRegion : "AB", maxAge : 3, newest : 2, oldest : 0 ],
        ],
      ],
      [ modality : [ "CR" ], anatomicRegion : "SQ", subrules :
        [
          [ modality : [ "CR" ], anatomicRegion : "SQ", maxAge : 3, newest : 2, oldest : 0 ],
        ],
      ],

      // Rules for RF
      [ modality : [ "RF" ], anatomicRegion : "DI", subrules :
        [
          [ modality : [ "RF" ], anatomicRegion : "DI", maxAge : 0, newest : 0, oldest : 1 ],
          [ modality : [ "RF" ], anatomicRegion : "DI", maxAge : 3, newest : 3, oldest : 0 ],
          [ modality : [ "RF" ], anatomicRegion : "AB", maxAge : 3, newest : 2, oldest : 0 ],
          [ modality : [ "CR" ], anatomicRegion : "DI", maxAge : 3, newest : 2, oldest : 0 ],
          [ modality : [ "CR" ], anatomicRegion : "AB", maxAge : 3, newest : 2, oldest : 0 ],
          [ modality : [ "US" ], anatomicRegion : "AB", maxAge : 3, newest : 2, oldest : 0 ],
          [ modality : [ "US" ], anatomicRegion : "DI", maxAge : 3, newest : 2, oldest : 0 ],
        ],
      ],
      [ modality : [ "RF" ], anatomicRegion : "TH", subrules :
        [
          [ modality : [ "RF" ], anatomicRegion : "TH", maxAge : 0, newest : 0, oldest : 1 ],
          [ modality : [ "RF" ], anatomicRegion : "TH", maxAge : 3, newest : 3, oldest : 0 ],
          [ modality : [ "CR" ], anatomicRegion : "TH", maxAge : 3, newest : 2, oldest : 0 ],
        ],
      ],
      [ modality : [ "RF" ], anatomicRegion : "AB", subrules :
        [
          [ modality : [ "RF" ], anatomicRegion : "AB", maxAge : 0, newest : 0, oldest : 1 ],
          [ modality : [ "RF" ], anatomicRegion : "AB", maxAge : 3, newest : 3, oldest : 0 ],
          [ modality : [ "RF" ], anatomicRegion : "DI", maxAge : 3, newest : 2, oldest : 0 ],
          [ modality : [ "CR" ], anatomicRegion : "AB", maxAge : 3, newest : 2, oldest : 0 ],
          [ modality : [ "US" ], anatomicRegion : "AB", maxAge : 3, newest : 2, oldest : 0 ],
          [ modality : [ "US" ], anatomicRegion : "DI", maxAge : 3, newest : 2, oldest : 0 ],
          [ modality : [ "CT" ], anatomicRegion : "AB", maxAge : 3, newest : 2, oldest : 0 ],
        ],
      ],
      [ modality : [ "RF" ], anatomicRegion : "CO", subrules :
        [
          [ modality : [ "RF" ], anatomicRegion : "CO", maxAge : 0, newest : 0, oldest : 1 ],
          [ modality : [ "RF" ], anatomicRegion : "CO", maxAge : 3, newest : 3, oldest : 0 ],
          [ modality : [ "CR" ], anatomicRegion : "CO", maxAge : 3, newest : 2, oldest : 0 ],
          [ modality : [ "CT" ], anatomicRegion : "CO", maxAge : 0, newest : 0, oldest : 1 ],
          [ modality : [ "CT" ], anatomicRegion : "CO", maxAge : 3, newest : 1, oldest : 0 ],
        ],
      ],
      [ modality : [ "RF" ], anatomicRegion : "ES", subrules :
        [
          [ modality : [ "RF" ], anatomicRegion : "ES", maxAge : 0, newest : 0, oldest : 1 ],
          [ modality : [ "RF" ], anatomicRegion : "ES", maxAge : 3, newest : 3, oldest : 0 ],
          [ modality : [ "CR" ], anatomicRegion : "ES", maxAge : 3, newest : 2, oldest : 0 ],
        ],
      ],
      [ modality : [ "RF" ], anatomicRegion : "EI", subrules :
        [
          [ modality : [ "RF" ], anatomicRegion : "EI", maxAge : 0, newest : 0, oldest : 1 ],
          [ modality : [ "RF" ], anatomicRegion : "EI", maxAge : 3, newest : 3, oldest : 0 ],
          [ modality : [ "CR" ], anatomicRegion : "EI", maxAge : 3, newest : 2, oldest : 0 ],
        ],
      ],
      [ modality : [ "RF" ], anatomicRegion : "Extrémités inférieures", subrules :
        [
          [ modality : [ "RF" ], anatomicRegion : "Extrémités inférieures", maxAge : 0, newest : 0, oldest : 1 ],
          [ modality : [ "RF" ], anatomicRegion : "Extrémités inférieures", maxAge : 3, newest : 3, oldest : 0 ],
          [ modality : [ "CR" ], anatomicRegion : "Extrémités inférieures", maxAge : 3, newest : 2, oldest : 0 ],
        ],
      ],
      [ modality : [ "RF" ], anatomicRegion : "Extremites inferieures", subrules :
        [
          [ modality : [ "RF" ], anatomicRegion : "Extremites inferieures", maxAge : 0, newest : 0, oldest : 1 ],
          [ modality : [ "RF" ], anatomicRegion : "Extremites inferieures", maxAge : 3, newest : 3, oldest : 0 ],
          [ modality : [ "CR" ], anatomicRegion : "Extremites inferieures", maxAge : 3, newest : 2, oldest : 0 ],
        ],
      ],
      [ modality : [ "RF" ], anatomicRegion : "GO", subrules :
        [
          [ modality : [ "RF" ], anatomicRegion : "GO", maxAge : 0, newest : 0, oldest : 1 ],
          [ modality : [ "RF" ], anatomicRegion : "GO", maxAge : 3, newest : 3, oldest : 0 ],
          [ modality : [ "US" ], anatomicRegion : "GO", maxAge : 3, newest : 2, oldest : 0 ],
          [ modality : [ "RF" ], anatomicRegion : "AB", maxAge : 0, newest : 0, oldest : 1 ],
          [ modality : [ "RF" ], anatomicRegion : "AB", maxAge : 3, newest : 3, oldest : 0 ],
          [ modality : [ "US" ], anatomicRegion : "AB", maxAge : 3, newest : 2, oldest : 0 ],
        ],
      ],
      [ modality : [ "RF" ], anatomicRegion : "SQ", subrules :
        [
          [ modality : [ "RF" ], anatomicRegion : "SQ", maxAge : 0, newest : 0, oldest : 1 ],
          [ modality : [ "RF" ], anatomicRegion : "SQ", maxAge : 3, newest : 3, oldest : 0 ],
        ],
      ],

      // Rules for US
      [ modality : [ "US" ], anatomicRegion : "TC", subrules :
        [
          [ modality : [ "US" ], anatomicRegion : "TC", maxAge : 0, newest : 0, oldest : 1 ],
          [ modality : [ "US" ], anatomicRegion : "TC", maxAge : 3, newest : 3, oldest : 0 ],
          [ modality : [ "CT" ], anatomicRegion : "TC", maxAge : 0, newest : 0, oldest : 1 ],
          [ modality : [ "CT" ], anatomicRegion : "TC", maxAge : 3, newest : 3, oldest : 0 ],
          [ modality : [ "MR" ], anatomicRegion : "TC", maxAge : 0, newest : 0, oldest : 1 ],
          [ modality : [ "MR" ], anatomicRegion : "TC", maxAge : 3, newest : 3, oldest : 0 ],
        ],
      ],
      [ modality : [ "US" ], anatomicRegion : "DI", subrules :
        [
          [ modality : [ "US" ], anatomicRegion : "DI", maxAge : 0, newest : 0, oldest : 1 ],
          [ modality : [ "US" ], anatomicRegion : "DI", maxAge : 3, newest : 3, oldest : 0 ],
          [ modality : [ "US" ], anatomicRegion : "AB", maxAge : 3, newest : 2, oldest : 0 ],
          [ modality : [ "RF" ], anatomicRegion : "DI", maxAge : 3, newest : 2, oldest : 0 ],
          [ modality : [ "CR" ], anatomicRegion : "AB", maxAge : 3, newest : 2, oldest : 0 ],
          [ modality : [ "CR" ], anatomicRegion : "DI", maxAge : 3, newest : 2, oldest : 0 ],
          [ modality : [ "CT" ], anatomicRegion : "AB", maxAge : 0, newest : 0, oldest : 1 ],
          [ modality : [ "CT" ], anatomicRegion : "AB", maxAge : 3, newest : 3, oldest : 0 ],
          [ modality : [ "MR" ], anatomicRegion : "AB", maxAge : 0, newest : 0, oldest : 1 ],
          [ modality : [ "MR" ], anatomicRegion : "AB", maxAge : 3, newest : 3, oldest : 0 ],
        ],
      ],
      [ modality : [ "US" ], anatomicRegion : "AB", subrules :
        [
          [ modality : [ "US" ], anatomicRegion : "AB", maxAge : 0, newest : 0, oldest : 1 ],
          [ modality : [ "US" ], anatomicRegion : "AB", maxAge : 3, newest : 3, oldest : 0 ],
          [ modality : [ "US" ], anatomicRegion : "DI", maxAge : 3, newest : 2, oldest : 0 ],
          [ modality : [ "CR" ], anatomicRegion : "AB", maxAge : 3, newest : 2, oldest : 0 ],
          [ modality : [ "CR" ], anatomicRegion : "DI", maxAge : 3, newest : 2, oldest : 0 ],
          [ modality : [ "CT" ], anatomicRegion : "AB", maxAge : 3, newest : 2, oldest : 0 ],
          [ modality : [ "MR" ], anatomicRegion : "AB", maxAge : 0, newest : 0, oldest : 1 ],
          [ modality : [ "MR" ], anatomicRegion : "AB", maxAge : 3, newest : 3, oldest : 0 ],
        ],
      ],
      [ modality : [ "US" ], anatomicRegion : "ES", subrules :
        [
          [ modality : [ "US" ], anatomicRegion : "ES", maxAge : 0, newest : 0, oldest : 1 ],
          [ modality : [ "US" ], anatomicRegion : "ES", maxAge : 3, newest : 3, oldest : 0 ],
          [ modality : [ "CR" ], anatomicRegion : "ES", maxAge : 3, newest : 2, oldest : 0 ],
          [ modality : [ "RF" ], anatomicRegion : "ES", maxAge : 3, newest : 2, oldest : 0 ],
        ],
      ],
      [ modality : [ "US" ], anatomicRegion : "EI", subrules :
        [
          [ modality : [ "US" ], anatomicRegion : "EI", maxAge : 0, newest : 0, oldest : 1 ],
          [ modality : [ "US" ], anatomicRegion : "EI", maxAge : 3, newest : 3, oldest : 0 ],
          [ modality : [ "CR" ], anatomicRegion : "EI", maxAge : 3, newest : 2, oldest : 0 ],
          [ modality : [ "RF" ], anatomicRegion : "EI", maxAge : 3, newest : 2, oldest : 0 ],
        ],
      ],
      [ modality : [ "US" ], anatomicRegion : "CA", subrules :
        [
          [ modality : [ "US" ], anatomicRegion : "CA", maxAge : 0, newest : 0, oldest : 1 ],
          [ modality : [ "US" ], anatomicRegion : "CA", maxAge : 3, newest : 3, oldest : 0 ],
          [ modality : [ "CR" ], anatomicRegion : "TH", maxAge : 3, newest : 2, oldest : 0 ],
        ],
      ],
      [ modality : [ "US" ], anatomicRegion : "SE", subrules :
        [
          [ modality : [ "US" ], anatomicRegion : "SE", maxAge : 0, newest : 0, oldest : 1 ],
          [ modality : [ "US" ], anatomicRegion : "SE", maxAge : 5, newest : 4, oldest : 0 ],
          [ modality : [ "MG" ], anatomicRegion : "SE", maxAge : 5, newest : 5, oldest : 0 ],
          [ modality : [ "MR" ], anatomicRegion : "SE", maxAge : 5, newest : 5, oldest : 0 ],
          [ modality : [ "MN" ], anatomicRegion : "SE", maxAge : 0, newest : 0, oldest : 1 ],
          [ modality : [ "MN" ], anatomicRegion : "SE", maxAge : 5, newest : 4, oldest : 0 ],
        ],
      ],
      [ modality : [ "US" ], anatomicRegion : "GO", subrules :
        [
          [ modality : [ "US" ], anatomicRegion : "GO", maxAge : 0, newest : 0, oldest : 1 ],
          [ modality : [ "US" ], anatomicRegion : "GO", maxAge : 3, newest : 3, oldest : 0 ],
          [ modality : [ "CR" ], anatomicRegion : "GO", maxAge : 3, newest : 2, oldest : 0 ],
          [ modality : [ "MR" ], anatomicRegion : "GO", maxAge : 3, newest : 2, oldest : 0 ],
          [ modality : [ "US" ], anatomicRegion : "AB", maxAge : 0, newest : 0, oldest : 1 ],
          [ modality : [ "US" ], anatomicRegion : "AB", maxAge : 3, newest : 3, oldest : 0 ],
          [ modality : [ "CR" ], anatomicRegion : "AB", maxAge : 3, newest : 2, oldest : 0 ],
          [ modality : [ "MR" ], anatomicRegion : "AB", maxAge : 3, newest : 2, oldest : 0 ],
        ],
      ],

      // Rules for XA
      [ modality : [ "XA" ], anatomicRegion : "TC", subrules :
        [
          [ modality : [ "XA" ], anatomicRegion : "TC", maxAge : 0, newest : 0, oldest : 1 ],
          [ modality : [ "XA" ], anatomicRegion : "TC", maxAge : 3, newest : 3, oldest : 0 ],
          [ modality : [ "AN" ], anatomicRegion : "TC", maxAge : 3, newest : 2, oldest : 0 ],
          [ modality : [ "CT", "MR" ], anatomicRegion : "TC", maxAge : 3, newest : 2, oldest : 0 ],
          [ modality : [ "US" ], anatomicRegion : "TC", maxAge : 3, newest : 2, oldest : 0 ],
        ],
      ],
      [ modality : [ "XA" ], anatomicRegion : "DI", subrules :
        [
          [ modality : [ "XA" ], anatomicRegion : "DI", maxAge : 0, newest : 0, oldest : 1 ],
          [ modality : [ "XA" ], anatomicRegion : "DI", maxAge : 3, newest : 3, oldest : 0 ],
          [ modality : [ "AN" ], anatomicRegion : "DI", maxAge : 3, newest : 2, oldest : 0 ],
          [ modality : [ "CT", "MR" ], anatomicRegion : "DI", maxAge : 3, newest : 2, oldest : 0 ],
          [ modality : [ "CT", "MR" ], anatomicRegion : "AB", maxAge : 3, newest : 2, oldest : 0 ],
          [ modality : [ "US" ], anatomicRegion : "AB", maxAge : 3, newest : 2, oldest : 0 ],
          [ modality : [ "US" ], anatomicRegion : "DI", maxAge : 3, newest : 2, oldest : 0 ],
        ],
      ],
      [ modality : [ "XA" ], anatomicRegion : "TH", subrules :
        [
          [ modality : [ "XA" ], anatomicRegion : "TH", maxAge : 0, newest : 0, oldest : 1 ],
          [ modality : [ "XA" ], anatomicRegion : "TH", maxAge : 3, newest : 3, oldest : 0 ],
          [ modality : [ "AN" ], anatomicRegion : "TH", maxAge : 3, newest : 2, oldest : 0 ],
          [ modality : [ "CT", "MR" ], anatomicRegion : "TH", maxAge : 3, newest : 2, oldest : 0 ],
          [ modality : [ "CR" ], anatomicRegion : "TH", maxAge : 3, newest : 2, oldest : 0 ],
        ],
      ],
      [ modality : [ "XA" ], anatomicRegion : "AB", subrules :
        [
          [ modality : [ "XA" ], anatomicRegion : "DI", maxAge : 0, newest : 0, oldest : 1 ],
          [ modality : [ "XA" ], anatomicRegion : "DI", maxAge : 3, newest : 3, oldest : 0 ],
          [ modality : [ "AN" ], anatomicRegion : "AB", maxAge : 3, newest : 2, oldest : 0 ],
          [ modality : [ "CT", "MR" ], anatomicRegion : "AB", maxAge : 3, newest : 2, oldest : 0 ],
          [ modality : [ "CT", "MR" ], anatomicRegion : "DI", maxAge : 3, newest : 2, oldest : 0 ],
          [ modality : [ "US" ], anatomicRegion : "AB", maxAge : 3, newest : 2, oldest : 0 ],
          [ modality : [ "US" ], anatomicRegion : "DI", maxAge : 3, newest : 2, oldest : 0 ],
        ],
      ],
      [ modality : [ "XA" ], anatomicRegion : "CO", subrules :
        [
          [ modality : [ "XA" ], anatomicRegion : "CO", maxAge : 0, newest : 0, oldest : 1 ],
          [ modality : [ "XA" ], anatomicRegion : "CO", maxAge : 3, newest : 3, oldest : 0 ],
          [ modality : [ "AN" ], anatomicRegion : "CO", maxAge : 3, newest : 2, oldest : 0 ],
          [ modality : [ "CT", "MR" ], anatomicRegion : "CO", maxAge : 3, newest : 2, oldest : 0 ],
          [ modality : [ "CR" ], anatomicRegion : "CO", maxAge : 3, newest : 2, oldest : 0 ],
        ],
      ],
      [ modality : [ "XA" ], anatomicRegion : "ES", subrules :
        [
          [ modality : [ "XA" ], anatomicRegion : "ES", maxAge : 0, newest : 0, oldest : 1 ],
          [ modality : [ "XA" ], anatomicRegion : "ES", maxAge : 3, newest : 3, oldest : 0 ],
          [ modality : [ "AN" ], anatomicRegion : "ES", maxAge : 3, newest : 2, oldest : 0 ],
          [ modality : [ "CR" ], anatomicRegion : "ES", maxAge : 3, newest : 2, oldest : 0 ],
          [ modality : [ "CT", "MR" ], anatomicRegion : "ES", maxAge : 3, newest : 2, oldest : 0 ],
        ],
      ],
      [ modality : [ "XA" ], anatomicRegion : "EI", subrules :
        [
          [ modality : [ "XA" ], anatomicRegion : "EI", maxAge : 0, newest : 0, oldest : 1 ],
          [ modality : [ "XA" ], anatomicRegion : "EI", maxAge : 3, newest : 3, oldest : 0 ],
          [ modality : [ "AN" ], anatomicRegion : "EI", maxAge : 3, newest : 2, oldest : 0 ],
          [ modality : [ "CR" ], anatomicRegion : "EI", maxAge : 3, newest : 2, oldest : 0 ],
          [ modality : [ "CT", "MR" ], anatomicRegion : "EI", maxAge : 3, newest : 2, oldest : 0 ],
        ],
      ],
      [ modality : [ "XA" ], anatomicRegion : "CA", subrules :
        [
          [ modality : [ "XA" ], anatomicRegion : "CA", maxAge : 0, newest : 0, oldest : 1 ],
          [ modality : [ "XA" ], anatomicRegion : "CA", maxAge : 3, newest : 3, oldest : 0 ],
          [ modality : [ "XA" ], anatomicRegion : "HE", maxAge : 3, newest : 2, oldest : 0 ],
          [ modality : [ "CT", "MR" ], anatomicRegion : "CA", maxAge : 3, newest : 2, oldest : 0 ],
          [ modality : [ "CR" ], anatomicRegion : "CA", maxAge : 3, newest : 2, oldest : 0 ],
          [ modality : [ "CR" ], anatomicRegion : "TH", maxAge : 3, newest : 2, oldest : 0 ],
        ],
      ],
      [ modality : [ "XA" ], anatomicRegion : "HE", subrules :
        [
          [ modality : [ "XA" ], anatomicRegion : "HE", maxAge : 0, newest : 0, oldest : 1 ],
          [ modality : [ "XA" ], anatomicRegion : "HE", maxAge : 3, newest : 3, oldest : 0 ],
          [ modality : [ "XA" ], anatomicRegion : "CA", maxAge : 3, newest : 2, oldest : 0 ],
          [ modality : [ "CT", "MR" ], anatomicRegion : "CA", maxAge : 3, newest : 2, oldest : 0 ],
          [ modality : [ "CR" ], anatomicRegion : "CA", maxAge : 3, newest : 2, oldest : 0 ],
          [ modality : [ "CR" ], anatomicRegion : "TH", maxAge : 3, newest : 2, oldest : 0 ],
        ],
      ],
      [ modality : [ "XA" ], anatomicRegion : "Hémodynamie", subrules :
        [
          [ modality : [ "XA" ], anatomicRegion : "HE", maxAge : 0, newest : 0, oldest : 1 ],
          [ modality : [ "XA" ], anatomicRegion : "HE", maxAge : 3, newest : 3, oldest : 0 ],
          [ modality : [ "XA" ], anatomicRegion : "CA", maxAge : 3, newest : 2, oldest : 0 ],
          [ modality : [ "CT", "MR" ], anatomicRegion : "CA", maxAge : 3, newest : 2, oldest : 0 ],
          [ modality : [ "CR" ], anatomicRegion : "CA", maxAge : 3, newest : 2, oldest : 0 ],
          [ modality : [ "CR" ], anatomicRegion : "TH", maxAge : 3, newest : 2, oldest : 0 ],
        ],
      ],
      [ modality : [ "XA" ], anatomicRegion : "Hemodynamie", subrules :
        [
          [ modality : [ "XA" ], anatomicRegion : "HE", maxAge : 0, newest : 0, oldest : 1 ],
          [ modality : [ "XA" ], anatomicRegion : "HE", maxAge : 3, newest : 3, oldest : 0 ],
          [ modality : [ "XA" ], anatomicRegion : "CA", maxAge : 3, newest : 2, oldest : 0 ],
          [ modality : [ "CT", "MR" ], anatomicRegion : "CA", maxAge : 3, newest : 2, oldest : 0 ],
          [ modality : [ "CR" ], anatomicRegion : "CA", maxAge : 3, newest : 2, oldest : 0 ],
          [ modality : [ "CR" ], anatomicRegion : "TH", maxAge : 3, newest : 2, oldest : 0 ],
        ],
      ],
      [ modality : [ "XA" ], anatomicRegion : "AN", subrules :
        [
          [ modality : [ "XA" ], anatomicRegion : "AN", maxAge : 0, newest : 0, oldest : 1 ],
          [ modality : [ "XA" ], anatomicRegion : "AN", maxAge : 3, newest : 3, oldest : 0 ],
        ],
      ],

      // Rules for MG
      [ modality : [ "MG" ], anatomicRegion : "SE", subrules :
        [
          [ modality : [ "MG" ], anatomicRegion : "SE", maxAge : 0, newest : 0, oldest : 1 ],
          [ modality : [ "MG" ], anatomicRegion : "SE", maxAge : 5, newest : 4, oldest : 0 ],
          [ modality : [ "US" ], anatomicRegion : "SE", maxAge : 5, newest : 5, oldest : 0 ],
          [ modality : [ "MR" ], anatomicRegion : "SE", maxAge : 5, newest : 5, oldest : 0 ],
          [ modality : [ "NM" ], anatomicRegion : "SE", maxAge : 5, newest : 5, oldest : 0 ],
        ],
      ],

      // Rules for NM
      [ modality : [ "NM" ], anatomicRegion : "TC", subrules :
        [
          [ modality : [ "NM" ], anatomicRegion : "TC", maxAge : 0, newest : 0, oldest : 1 ],
          [ modality : [ "NM" ], anatomicRegion : "TC", maxAge : 3, newest : 3, oldest : 0 ],
          [ modality : [ "CT", "MR" ], anatomicRegion : "TC", maxAge : 3, newest : 2, oldest : 0 ],
          [ modality : [ "PT" ], anatomicRegion : "TP", maxAge : 3, newest : 2, oldest : 0 ],
          [ modality : [ "CT" ], anatomicRegion : "TP", maxAge : 3, newest : 2, oldest : 0 ],
        ],
      ],
      [ modality : [ "NM" ], anatomicRegion : "DI", subrules :
        [
          [ modality : [ "NM" ], anatomicRegion : "DI", maxAge : 0, newest : 0, oldest : 1 ],
          [ modality : [ "NM" ], anatomicRegion : "DI", maxAge : 3, newest : 3, oldest : 0 ],
          [ modality : [ "NM" ], anatomicRegion : "AB", maxAge : 3, newest : 2, oldest : 0 ],
          [ modality : [ "CT", "MR" ], anatomicRegion : "DI", maxAge : 3, newest : 2, oldest : 0 ],
          [ modality : [ "CT", "MR" ], anatomicRegion : "AB", maxAge : 3, newest : 2, oldest : 0 ],
          [ modality : [ "CR" ], anatomicRegion : "DI", maxAge : 1, newest : 2, oldest : 0 ],
          [ modality : [ "CR" ], anatomicRegion : "AB", maxAge : 1, newest : 2, oldest : 0 ],
        ],
      ],
      [ modality : [ "NM" ], anatomicRegion : "TH", subrules :
        [
          [ modality : [ "NM" ], anatomicRegion : "TH", maxAge : 0, newest : 0, oldest : 1 ],
          [ modality : [ "NM" ], anatomicRegion : "TH", maxAge : 3, newest : 3, oldest : 0 ],
          [ modality : [ "CT", "MR" ], anatomicRegion : "TH", maxAge : 3, newest : 2, oldest : 0 ],
          [ modality : [ "CR" ], anatomicRegion : "TH", maxAge : 1, newest : 2, oldest : 0 ],
          [ modality : [ "CR" ], anatomicRegion : "AB", maxAge : 1, newest : 2, oldest : 0 ],
          [ modality : [ "CT" ], anatomicRegion : "TP", maxAge : 3, newest : 2, oldest : 0 ],
        ],
      ],
      [ modality : [ "NM" ], anatomicRegion : "AB", subrules :
        [
          [ modality : [ "NM" ], anatomicRegion : "AB", maxAge : 0, newest : 0, oldest : 1 ],
          [ modality : [ "NM" ], anatomicRegion : "AB", maxAge : 3, newest : 3, oldest : 0 ],
          [ modality : [ "NM" ], anatomicRegion : "DI", maxAge : 3, newest : 2, oldest : 0 ],
          [ modality : [ "CT", "MR" ], anatomicRegion : "AB", maxAge : 3, newest : 2, oldest : 0 ],
          [ modality : [ "CR" ], anatomicRegion : "AB", maxAge : 1, newest : 2, oldest : 0 ],
          [ modality : [ "CR" ], anatomicRegion : "DI", maxAge : 1, newest : 2, oldest : 0 ],
          [ modality : [ "PT" ], anatomicRegion : "TP", maxAge : 2, newest : 2, oldest : 0 ],
          [ modality : [ "CT" ], anatomicRegion : "TP", maxAge : 2, newest : 2, oldest : 0 ],
        ],
      ],
      [ modality : [ "NM" ], anatomicRegion : "CA", subrules :
        [
          [ modality : [ "NM" ], anatomicRegion : "CA", maxAge : 0, newest : 0, oldest : 1 ],
          [ modality : [ "NM" ], anatomicRegion : "CA", maxAge : 3, newest : 3, oldest : 0 ],
          [ modality : [ "CT", "MR" ], anatomicRegion : "CA", maxAge : 3, newest : 2, oldest : 0 ],
          [ modality : [ "US" ], anatomicRegion : "CA", maxAge : 3, newest : 2, oldest : 0 ],
          [ modality : [ "XA" ], anatomicRegion : "HE", maxAge : 3, newest : 2, oldest : 0 ],
          [ modality : [ "CR" ], anatomicRegion : "TH", maxAge : 1, newest : 2, oldest : 0 ],
          [ modality : [ "PT" ], anatomicRegion : "TP", maxAge : 3, newest : 2, oldest : 0 ],
          [ modality : [ "CT" ], anatomicRegion : "TP", maxAge : 3, newest : 2, oldest : 0 ],
        ],
      ],
      [ modality : [ "NM" ], anatomicRegion : "SE", subrules :
        [
          // Modified this rule as part of the 3.5.0.7 upgrade and KHC 
          [ modality : [ "NM" ], anatomicRegion : "SE", maxAge : 0, newest : 0, oldest : 1 ],
          [ modality : [ "NM" ], anatomicRegion : "SE", maxAge : 2, newest : 3, oldest : 0 ],

          [ modality : [ "MG" ], anatomicRegion : "SE", maxAge : 2, newest : 2, oldest : 0 ],
          [ modality : [ "MR" ], anatomicRegion : "SE", maxAge : 2, newest : 2, oldest : 0 ],
          [ modality : [ "PT" ], anatomicRegion : "TP", maxAge : 2, newest : 2, oldest : 0 ],
          [ modality : [ "CT" ], anatomicRegion : "TP", maxAge : 2, newest : 2, oldest : 0 ],
        ],
      ],
      [ modality : [ "NM" ], anatomicRegion : "EN", subrules :
        [
          [ modality : [ "NM" ], anatomicRegion : "EN", maxAge : 0, newest : 0, oldest : 1 ],
          [ modality : [ "NM" ], anatomicRegion : "EN", maxAge : 5, newest : 3, oldest : 0 ],
          [ modality : [ "NM" ], anatomicRegion : "TC", maxAge : 3, newest : 2, oldest : 0 ],
          [ modality : [ "CT", "MR" ], anatomicRegion : "TH", maxAge : 3, newest : 2, oldest : 0 ],
          [ modality : [ "CT", "MR" ], anatomicRegion : "AB", maxAge : 3, newest : 2, oldest : 0 ],
          [ modality : [ "US" ], anatomicRegion : "TC", maxAge : 3, newest : 2, oldest : 0 ],
          [ modality : [ "PT" ], anatomicRegion : "TP", maxAge : 3, newest : 2, oldest : 0 ],
          [ modality : [ "CT" ], anatomicRegion : "TP", maxAge : 3, newest : 2, oldest : 0 ],
        ],
      ],
      [ modality : [ "NM" ], anatomicRegion : "Endocrinien", subrules :
        [
          [ modality : [ "NM" ], anatomicRegion : "EN", maxAge : 0, newest : 0, oldest : 1 ],
          [ modality : [ "NM" ], anatomicRegion : "EN", maxAge : 5, newest : 3, oldest : 0 ],
          [ modality : [ "NM" ], anatomicRegion : "TC", maxAge : 3, newest : 2, oldest : 0 ],
          [ modality : [ "CT", "MR" ], anatomicRegion : "TH", maxAge : 3, newest : 2, oldest : 0 ],
          [ modality : [ "CT", "MR" ], anatomicRegion : "AB", maxAge : 3, newest : 2, oldest : 0 ],
          [ modality : [ "US" ], anatomicRegion : "TC", maxAge : 3, newest : 2, oldest : 0 ],
          [ modality : [ "PT" ], anatomicRegion : "TP", maxAge : 3, newest : 2, oldest : 0 ],
          [ modality : [ "CT" ], anatomicRegion : "TP", maxAge : 3, newest : 2, oldest : 0 ],
        ],
      ],
      [ modality : [ "NM" ], anatomicRegion : "SQ", subrules :
        [
          [ modality : [ "NM" ], anatomicRegion : "SQ", maxAge : 0, newest : 0, oldest : 1 ],
          [ modality : [ "NM" ], anatomicRegion : "SQ", maxAge : 3, newest : 3, oldest : 0 ],
          [ modality : [ "CT", "MR" ], anatomicRegion : "TC", maxAge : 2, newest : 2, oldest : 0 ],
          [ modality : [ "CT", "MR" ], anatomicRegion : "TH", maxAge : 2, newest : 2, oldest : 0 ],
          [ modality : [ "CT", "MR" ], anatomicRegion : "CO", maxAge : 2, newest : 2, oldest : 0 ],
          [ modality : [ "CT", "MR" ], anatomicRegion : "EI", maxAge : 2, newest : 2, oldest : 0 ],
          [ modality : [ "CT", "MR" ], anatomicRegion : "ES", maxAge : 2, newest : 2, oldest : 0 ],
          [ modality : [ "CT", "MR" ], anatomicRegion : "AB", maxAge : 2, newest : 2, oldest : 0 ],
          [ modality : [ "CR" ], anatomicRegion : "TC", maxAge : 2, newest : 2, oldest : 0 ],
          [ modality : [ "CR" ], anatomicRegion : "TH", maxAge : 2, newest : 2, oldest : 0 ],
          [ modality : [ "CR" ], anatomicRegion : "CO", maxAge : 2, newest : 2, oldest : 0 ],
          [ modality : [ "CR" ], anatomicRegion : "EI", maxAge : 2, newest : 2, oldest : 0 ],
          [ modality : [ "CR" ], anatomicRegion : "ES", maxAge : 2, newest : 2, oldest : 0 ],
          [ modality : [ "CR" ], anatomicRegion : "AB", maxAge : 2, newest : 2, oldest : 0 ],
          [ modality : [ "PT" ], anatomicRegion : "TP", maxAge : 3, newest : 2, oldest : 0 ],
          [ modality : [ "CT" ], anatomicRegion : "TP", maxAge : 3, newest : 2, oldest : 0 ],
        ],
      ],
      [ modality : [ "NM" ], anatomicRegion : "HP", subrules :
        [
          [ modality : [ "NM" ], anatomicRegion : "HP", maxAge : 0, newest : 0, oldest : 1 ],
          [ modality : [ "NM" ], anatomicRegion : "HP", maxAge : 3, newest : 3, oldest : 0 ],
          [ modality : [ "PT" ], anatomicRegion : "TP", maxAge : 3, newest : 2, oldest : 0 ],
          [ modality : [ "CT" ], anatomicRegion : "TP", maxAge : 3, newest : 2, oldest : 0 ],
        ],
      ],
      [ modality : [ "NM" ], anatomicRegion : "Hématopoïétique", subrules :
        [
          [ modality : [ "NM" ], anatomicRegion : "HP", maxAge : 0, newest : 0, oldest : 1 ],
          [ modality : [ "NM" ], anatomicRegion : "HP", maxAge : 3, newest : 3, oldest : 0 ],
          [ modality : [ "PT" ], anatomicRegion : "TP", maxAge : 3, newest : 2, oldest : 0 ],
          [ modality : [ "CT" ], anatomicRegion : "TP", maxAge : 3, newest : 2, oldest : 0 ],
        ],
      ],
      [ modality : [ "NM" ], anatomicRegion : "Hematopoietique", subrules :
        [
          [ modality : [ "NM" ], anatomicRegion : "HP", maxAge : 0, newest : 0, oldest : 1 ],
          [ modality : [ "NM" ], anatomicRegion : "HP", maxAge : 3, newest : 3, oldest : 0 ],
          [ modality : [ "PT" ], anatomicRegion : "TP", maxAge : 3, newest : 2, oldest : 0 ],
          [ modality : [ "CT" ], anatomicRegion : "TP", maxAge : 3, newest : 2, oldest : 0 ],
        ],
      ],

      // Rules for PT
      [ modality : [ "PT" ], anatomicRegion : "TP", subrules :
        [
          // Rule added as per the 3.5.0.7 Upgrade, and KHC-2194
          [ modality : [ "PT" ], anatomicRegion : "TP", maxAge : 0, newest : 0, oldest : 1 ],
          [ modality : [ "PT" ], anatomicRegion : "TP", maxAge : 5, newest : 4, oldest : 0 ],

          [ modality : [ "NM" ], anatomicRegion : "HP", maxAge : 0, newest : 0, oldest : 1 ],
          [ modality : [ "NM" ], anatomicRegion : "HP", maxAge : 5, newest : 4, oldest : 0 ],
          [ modality : [ "CT", "MR" ], anatomicRegion : "TC", maxAge : 3, newest : 3, oldest : 0 ],
          [ modality : [ "CT", "MR" ], anatomicRegion : "DI", maxAge : 3, newest : 3, oldest : 0 ],
          [ modality : [ "CT", "MR" ], anatomicRegion : "TH", maxAge : 3, newest : 3, oldest : 0 ],
          [ modality : [ "CT", "MR" ], anatomicRegion : "AB", maxAge : 3, newest : 3, oldest : 0 ],
          [ modality : [ "CT", "MR" ], anatomicRegion : "CO", maxAge : 3, newest : 3, oldest : 0 ],
          [ modality : [ "CT", "MR" ], anatomicRegion : "CA", maxAge : 3, newest : 3, oldest : 0 ],
          [ modality : [ "NM" ], anatomicRegion : "SQ", maxAge : 3, newest : 3, oldest : 0 ],
        ],
      ],
    ]

    static {
        log.trace("Number of rules: " + rules.size())
        // for rules with multiple modalities, count each modality separately:
        log.trace("Logical number of rules: " + rules.collectMany({ it.modality }).size())

        // Compile the rules into a map based on modality and anatomic region.
        // Slightly more complicated than List.groupBy because we need to produce
        // multiple map entries per list item
        def map = [:]
        rules.each { rule ->
            rule.modality.each { ruleModality ->
                map[[ modality: ruleModality, anatomicRegion: rule.anatomicRegion ]] = rule
            }
        }
        rules = map

        // if this comes out different than the logical number of rules, the table
        // is illogical (probably has duplicate, possibly conflicting rules)
        log.trace("Actual number of rules: " + rules.size())
    }

    /** Get the study instance uid for the study */
    private static suid(prior) {
        def suids = prior.get(XDSExtendedMetadata("studyInstanceUID"))
        return suids == null || suids.isEmpty() ? "??" : suids[0]
    }

    /** Get a list of study instance uids for the studies */
    private static suids(priors) {
        return priors.collect { suid(it) }
    }

    private static getCodeValues(prior, scheme) {
        return Modalities.getCodeValues(prior.get(XDSEventCodes), scheme)
    }

    /**
     * @return true if it matches, false otherwise
     */
    private static priorMatchesSubrule(prior, subrule) {
        log.debug("Testing prior '" + suid(prior) + "' against rule " + subrule)

        // The prior must contain one of the modalities modality that is specified by the subrule.
        if (subrule.modality.disjoint(getCodeValues(prior, "DCM"))) {
            log.debug("Prior '{}' does not contain relevant modalities", suid(prior))
            return false
        }

        // The anatomic region of the prior must match the anatomic region of the subrule.
        if (!getCodeValues(prior, "Imagerie Québec-DSQ").contains(subrule.anatomicRegion)) {
            log.debug("Prior '{}' does not match anatomic region", suid(prior))
            return false
        }

        // Now check to see if there is a maximum age constraint and if so, does the study date 
        // of the prior fall within that constraint.
        if (subrule.maxAge > 0) {
            def earliestStudyDate = new org.joda.time.DateTime().minusYears(subrule.maxAge)
            def priorStudyDate = prior.get(XDSServiceStartTime)

            if (priorStudyDate < earliestStudyDate) {
                log.debug("Prior '{}' with date {} falls outside of maximum age (after {})",
                          suid(prior), priorStudyDate, earliestStudyDate)
                return false
            }
        } 

        log.debug("Prior '{}' matches rule", suid(prior))
        return true
    }

    /**
     * @return list containing matching priors
     */
    private static filterForNewestAndOldest(priors, subrule) {
        if (subrule.newest <= 0 && subrule.oldest <= 0) {
            log.trace("Rule has no newest/oldest restrictions")
            return priors
        }

        if (subrule.newest + subrule.oldest >= priors.size()) {
            log.debug("Newest/oldest restrictions include all priors")
            return priors
        }

        priors.sort { it.get(XDSServiceStartTime) }
        log.debug("Relevant priors after forward sorting are " + suids(priors))

        // sort goes low to high, so the newest are at the end of the list:
        priors = priors.reverse().take(subrule.newest) +
            priors.take(subrule.oldest)

        log.debug("Relevant priors after filtering for newest and oldest are: {}", suids(priors))
        return priors
    }

    /**
     * Top level method for comparing a list of priors against a single subrule.
     * @return list containing matching priors
     */
    private static getPriorsMatchingSubrule(subrule, allPriors) {
        log.debug("Subrule is " + subrule)

        def matchingPriors = allPriors.findAll { priorMatchesSubrule(it, subrule) }
        log.debug("Relevant sub priors matching subrules are: " + suids(matchingPriors))

        matchingPriors = filterForNewestAndOldest(matchingPriors, subrule)
        log.debug("Relevant sub priors after computing newest/oldest are " + suids(matchingPriors))

        return matchingPriors
    }

    /**
     * Starting point for evaluating rules.
     */
    public static findRelevantPriors(order, allPriors) {
        def orderModality = Modalities.secondaryToPrimaryModalitiesMap[order.get(SharedConstants.HL7_Order_Modality_Field)]

        // Find the rule that matches the order's modality and anatomic region:
        def key = [ modality: orderModality, anatomicRegion: order.get(SharedConstants.HL7_Order_Anatomic_Region_Field) ]
        def rule = rules[key]

        log.debug("Key {} matches rule {}", key, rule)

        if (rule == null) {
            return []
        }

        // for each subrule, get the list of matching priors and 
        // combine them all into one list:
        def relevantPriors = rule.subrules.collectMany { subrule ->
            getPriorsMatchingSubrule(subrule, allPriors)
        }

        log.trace("Relevant priors before uniquifying " + relevantPriors)

        // It may be possible that there are duplicates in the relevant priors,
        // since a single prior may match multiple rules.  Two priors are determined to
        // be duplicates if they share the same study instance UID.
        relevantPriors.unique { it.get(XDSExtendedMetadata("studyInstanceUID")) }

        return relevantPriors
    }
}

log.debug("These are the raw studies from the XDS Reg being passed to the rules morpher:\n{}", studies)
log.debug("Checking {} priors", studies.size())

context = Contexts.getContext(config.context)
if (context == null) {
    log.warn("Context is unknown from id: {}. Unable to filter local responses. " +
        "Please correct either the connect ContextID configuration or contexts.groovy", config.context)
} else {
    studies = studies.findAll { study ->
        !context.isLocal(study.get(XDSSourcePatientID).domainUUID)
    }
}

log.debug("After filtering local studies, we are checking {} priors", studies.size())

relevantPriors = PriorsRules.findRelevantPriors(order, studies)

log.debug("These are the Study Instance UIDs that match the rules: {}", PriorsRules.suids(relevantPriors))
log.debug("These are the full relevant priors: {}", relevantPriors)

return relevantPriors

