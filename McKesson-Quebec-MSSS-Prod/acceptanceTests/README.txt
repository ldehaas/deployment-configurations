These tools help locate test patients and studies for use in acceptance tests.

query.groovy reads a file with patient information, identifies patients, searches the
registry for relevant studies and saves them in xml files on disk.  The xds registry
in Quebec is extremely slow, so caching the results on disk makes it much easier to
search through them at a later time for relevant studies.

query.groovy is designed to run over and over again (eg, if the registry craps out) and
it will only work on patients for whom it has not already completed a query.
Because it takes a very long time, it is recommended to let it run for a while in the
background before using its output.

It is recommended to use the output log of hl7rcv to feed into query.groovy.  A feed
from a production RIS will normally provide 100s of useful patients in a relativley
short amount of time.

summarizePatients.groovy reads all the data dumped by query.groovy, throws away
uninteresting studies and summarizes the rest in an easy-to-read format so that
test studies can be easily found.
