/*
 * Summarizes studies dumped by query.groovy
 * Looks for *.xml in the current directory.
 * May be run with rtkgrv or regular groovy.
 *
 * Copyright 2013 Karos Health Incorporated
 */

// config:
final LOCAL_DOMAIN = "2.16.124.10.101.1.60.1.1000.1"
final dominantModalities = [ "MG", "XA", "PT", "MR", "NM", "CT", "US", "RF", "CR", "DX", "BMD" ]

// utility methods:

filterStudies = { closure ->
    patients.each { patient ->
        patient.study.findAll(closure).each { patient.remove(it) }
    }

    // remove patients with no studies:
    patients = patients.findAll { patient ->
        patient.study.size() > 0
    }
}

sort = { element, name, closure ->
    element[name].sort(closure).each { element.remove(it); element.append(it) }
}

dominantModality = { study ->
    modalities = study.modality*.text()
    [dominantModalities.find { modalities.contains(it) }, modalities.join(", ")].find()
}

// load patients from xml files in current directory
patients = []
new File('.').eachFileMatch(~/.*\.xml/) {
    patients << new groovy.util.XmlParser().parse(it)
}
println "${patients.size()} patients"

// filter studies we're not interested in:
filterStudies { it.accession.text().isEmpty() || it.modality.isEmpty() }
println "After filtering studies with missing data, ${patients.size()} patients remain"
filterStudies { it.sourceDomain.text() == LOCAL_DOMAIN }
println "After filtering local studies, ${patients.size()} patients remain"

// print patients out in order of RAMQ:
println "\nHere are the studies, sorted by patient:"
println "    Modality AnatomicRegion StudyDate Accession"
patients.sort({ it.ramq.text() }).each { patient ->
    println "\n${patient.ramq.text()}^^^RAMQ|${patient.local.text()}"
    patient.study.collect { study ->
        "    ${dominantModality(study)} ${study.anatomicRegion.text()} ${study.date.text()[0..9]} ${study.accession.text()}"
    }.sort().each { println it }
}

