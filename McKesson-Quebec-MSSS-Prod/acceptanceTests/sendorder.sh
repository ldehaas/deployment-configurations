#!/bin/sh

# Sends an order to the local Connect instance (listening on the temporary 5557 port)
# Copyright 2013 Karos Health Incorporated

if [ $# != 4 -a $# != 5 ]; then
    echo "Usage: $0 RAMQ LOCALPID MODALITY ANATOMIC_REGION [ STATUS ]" 1>&2
    exit 1
fi

RAMQ=$1
LOCALPID=$2
MODALITY=$3
ANATOMIC_REGION=$4
STATUS=${5:-EATTEN}

hl7snd -d localhost:5557 << EOF
MSH|^~\&|RADIMAGE^143|HSM|||20131122140834||ORM^O01|15271192|P|2.3
PID|1|$RAMQ^^^RAMQ|$LOCALPID||
PV1|1|I|""^^""^^^^""||||
ORC|XO|""^""|SAMPLEACCESSION||CM||^^^201311201252||20131122140822||||ONCO|||$STATUS|||
OBR|1|""^""||| |201311201252|20131120131857|20131120132216|||||""||^^^$ANATOMIC_REGION||||||IMAGES|20131122140822||$MODALITY|
OBX|1|TX|1||Examination performed
ZDS|1.2.3^RadImage^Application^DICOM
EOF

