class SharedHl7 {
    /* General Documentation:
     *   Common ADT Codes:
     *       A34 (merge patient information-patient ID only)
     *       A35 (merge patient information-account number only)
     *       A36 (merge patient information-patient ID and account number)
     *       A39 (merge person - external ID)
     *       A40 (merge patient - internal ID)
     *       A41 (merge account - patient account number)
     *       A42 (merge visit - visit number)
     */
    static final SupportedADTTriggerEvents = ['A01', 'A04', 'A05', 'A08', 'A31']

    /**
     * Description of how to parse and normalize the patient ids in incoming hl7 messages.
     * Each entry is a map with the following keys:
     *
     * description:         Short, human readable string for logging
     *
     * field:               Location of pid within hl7 message.  Must have only the field specified
     *                      (eg. PID-3 is valid, PID-3-1 is not).
     *
     * required:            Whether or not messages without this field should be rejected.
     *                      May be a boolean value or the result of calling forOperations(...) with
     *                      a list of operations for which the field is required.  Currently
     *                      supported operations are "pix" and "fps".
     *
     * invalidationMethod:  A method or closure that takes a string and returns true if the pid
     *                      value is invalid.  If this returns false and the field is required,
     *                      the message will be rejected.
     *
     * expectedDomain:      The domain to either validate or place into the field.  May be a string
     *                      or a closure that takes the current context, for multi-context sites, and
     *                      returns the desired domains.
     */
    static final PIDConfiguration = [
        [
            description:        "RAMQ",
            field:              "PID-2",
            required:           forOperations("pix"),
            invalidationMethod: QuebecHL7Validation.&RAMQformatIsInvalid,
            expectedDomain:     SharedConstants.Affinity_Domain_OID],
        [
            description:        "MRN",
            field:              "PID-3",
            required:           true,
            expectedDomain:     { context -> context.PACSIssuer }],
    ]

    /**
     * @return a closure that, when called with the current operation,
     *         returns true if the current operation is in the list of
     *         operations specified.
     */
    private static forOperations(String... ops) {
        return { op -> (ops as List).contains(op) }
    }

    /**
     * Normalize the pids in an hl7 message by adding domains based on known
     * message locations.  Configure using PIDConfiguration above.
     * Before calling this, please be sure to include shared_constants.groovy and contexts.groovy
     *
     * @return whether or not the message is valid
     */
    public static boolean normalizePids(msg, log, contextId, operation) {
        def context = Contexts.getContext(contextId)
        if (context == null) {
            log.error("Context is unknown from id: {}. Unable to localize this message, dropping it. " +
                "Please correct either the connect ContextID configuration or contexts.groovy", contextId)
            return false
        }

        for (pidspec in PIDConfiguration) {
            def pidValue = msg.get(pidspec.field)
            if (pidValue == null || pidValue.isEmpty()) {

                def required = pidspec.required
                if (required instanceof Closure) {
                    required = required(operation)
                }

                if (required) {
                    log.debug("Message is missing required {}. Dropping.",
                        pidspec.description)
                    return false

                } else {
                    // ignore pids that aren't present and not required:
                    continue
                }
            }

            if (pidspec.invalidationMethod != null && pidspec.invalidationMethod(pidValue)) {
                log.warn("{} '{}' is invalid. Dropping message.", pidspec.description, pidValue)
                return false
            }

            def expectedDomain = pidspec.expectedDomain

            if (expectedDomain instanceof Closure) {
                expectedDomain = expectedDomain(context)
            }

            def domainLocation = pidspec.field + "-4-2"
            def domain = msg.get(domainLocation)
            if (domain == null || domain.isEmpty()) {
                msg.set(domainLocation, expectedDomain)
                log.debug("Setting domain for {} at {} to {}",
                    pidspec.description, domainLocation, expectedDomain)

            } else if (domain.toUpperCase() != expectedDomain) {
                log.warn("Unexpected domain '{}' for {} in {}.  Expected '{}'. Dropping message.",
                    domain, pidspec.description, domainLocation, expectedDomain)

                return false
            }

            msg.set(pidspec.field, pidValue.toUpperCase())
            log.debug("Using {} ({}): {} in domain {}",
                pidspec.description,
                pidspec.field,
                msg.get(pidspec.field),
                msg.get(domainLocation))
        }

        return true
    }

    public static void normalizeCharset(msg, log) {
        def characterEncoding = msg.get("MSH-18")
        if (characterEncoding == null || characterEncoding.isEmpty()){
            log.trace("No character set specified.  Hard-coding to 8859/1")
            msg.set('MSH-18', '8859/1')
        }
    }
}

class QuebecHL7Validation {
    /**
     * This function validates that the format of a passed potential RAMQ actually conforms to the RAMQ creation algorythm.
     * This function is needed in order to protect the PIX lookup mechanism, which is what eventually is used to query
     * the XDS Registry.
     *
     * Please note that the function actually evaluates for INVALID data, meaning that it will return true if the
     * RAMQ passed is invalid.  This is coded in this way for better readability in the calling script.
     *
     * The function follows the RAMQ formula, which is as follows:
     *      the first three letters of the last name;
     *      the first letter of the first name;
     *      the last two digits of the year of birth;
     *      the month of birth (to which 50 is added to indicate female);
     *      the day of birth;
     *      a two digit administrative code used by the Rée.
     */
    public static final RAMQformatIsInvalid(ramq) {
        //                 name        year    month, maybe +50       day            admin code
        return !( ramq ==~ /[A-Za-z]{4}[0-9]{2}([05][1-9]|[16][0-2])([0-2][0-9]|3[01])[0-9]{2}/ )
    }
}
