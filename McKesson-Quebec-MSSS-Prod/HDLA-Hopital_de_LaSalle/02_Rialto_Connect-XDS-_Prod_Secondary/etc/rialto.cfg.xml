
<!-- When deploying in a new machine / environment update
    [] Rialto.cfg:
        [] The "variables" section
        [] The jdbc connection for the RIS database
-->

<config>
    
    <!-- Variable Declaration: Infrastructure and Rialto Connect Configuration
        This section defines variables that are used in Rialto's configuration for All sites of the shared PACS -->

    <!-- Rialto Connect Configuration-->

    <!-- Defines the default character set for HL7 Servers-->
    <var name= "defaultCharacterSet">8859/1</var>   

    <!-- This is the Top Level Issuer Of Patient ID Value for the Quebec Province-->
    <var name="Affinity_Domain_Value">2.16.124.10.101.1.60.100</var>

    <var name="Audit_Record_Repository_Server_Port">8081</var> 
    <var name="Event_Collector_Server_Port">8080</var>
    <var name="IHE_Audit_Records_Server_UDP_Port">4000</var>

    <!-- Infrastructure Servers Configuration-->

    <!-- IBM XDS Registry -->
    <!-- KHC16118 - Change to XDS Registry endpoint -->
    <!--<var name="XDS_Registry_URI">https://xdsprod.dsq.rtss.qc.ca:5000/IBMXDSRegistry/XDSb/SOAP12/Registry</var> -->
    <!-- KHC16147 - Changed DNS name for XDS registry -->
    <var name="XDS_Registry_URI">https://prod.xds.img.rtss.qc.ca:5000/XDSRegistryWS/DocumentRegistry_Service</var>

    <!-- The Local xDL DI-R node. - -  This is the DI-r in Montreal City for the Université McGill and Université de Montréal RUISs. This is known as "RID MONTREAL" -->
    <!-- Since each Site has a local xDL node that interfaces with the actual DI-r, and different constituent sites can be pointed at different local nodes,
         the DI-r 1 will be configured at each site -->

    <!-- This is the AGFA DI-r in Quebec City for the Université Laval RUIS.  This is knowns as "RID LAVAL" -->
    <var name="DIR2_XDS_Repostory_UID">2.16.124.10.101.1.60.2.90</var>
    <var name="DIR2_AETitle">PQP1VMLIDCSAS</var>
    <var name="DIR2_Host">ridqcdicom.img.rtss.qc.ca</var>
    <var name="DIR2_Port">11112</var>

    <!-- This is the new McKesson DI-r in the Montreal-McGill RUIS. Replicated to 2 physical sites, Ogilvy and Maisonneuve.
         This DIR is known as "EIR-RID" McGill -->
    <var name="DIR3_XDS_Repostory_UID">2.16.124.10.101.1.60.2.80</var>
    <var name="DIR3_AETitle">ALI_QUERY_EIR</var>
    <var name="DIR3_Host">eir.img.rtss.qc.ca</var>
    <var name="DIR3_Port">5000</var>

    <!-- This is the AGFA DI-r in Sherbrooke City for the Université de Sherbrooke RUIS.  This is known as "RID SHERBROOKE" -->
    <!-- Turning OFF temporarily
    <var name="DIR4_XDS_Repostory_UID"></var>
    <var name="DIR4_AETitle"></var>
    <var name="DIR4_Host"></var>
    <var name="DIR4_Port"></var>
    -->

    <!-- End of Variable Declarations -->


    <!-- Rialto Shared SERVICES Declaration - These are services that are shared by all instances of Connect on this Server -->

    <!-- Internal Transaction Records (Event Repository) -->
    <!-- This should be the first service so all transactions get logged here. If it is not the first service, you will get
            errors in the rialto log file -->
    <service id="Internal_Transaction_Records_Service" type="txn-records">
        <config>
            <prop name="StorageDir">${rialto.rootdir}/var/event-viewer</prop>
            <prop name="Purge">7d</prop>
            <prop name="PurgeInterval">1h</prop>
        </config>
    </service>


    <!-- IHE Audit Record Repository -->
    <service id="IHE_Audit_Record_Repository_UDP" type="arr">
        <server idref="Audit_Record_Repository_Server-http" name="arr-api">
            <url>/arrudp/auditRecords/*</url>
        </server>
        <config>
            <prop name="IndexDir">${rialto.rootdir}/var/arr/index</prop>
            <prop name="IndexBackupDir">${rialto.rootdir}/var/arr/backup</prop>
            <prop name="ArchiveDir">${rialto.rootdir}/var/arr/archive</prop>
            <prop name="TempDir">${rialto.rootdir}/var/arr/temp</prop>
            <prop name="Listener">udp:4000</prop>
            <prop name="BulkSize">1000</prop>
            <prop name="CommitInterval">10m</prop>
            <prop name="Purge">60d</prop>
            <prop name="ForwardInterval">5m</prop>

            <!-- for Audit Record Forwarding to the Quebec ATNA Server -->
            <prop name="ForwardDestination">udp://prod.xds.img.rtss.qc.ca:514</prop>
            <prop name="ForwarderCheckpointFolder">${rialto.rootdir}/var/arr/checkpoints</prop>

        </config>
    </service>


    <!-- "Old" Navigator Service providing Event Viewer and Audit Record Viewer -->
    <service id="navigator" type="navigator">
        <!--
        <device idref="RIALTO" name="proxyDIR" />
        -->
        <server idref="Event_Collector_and_Navigator_Server-http" name="navigator-war">
            <url>/</url>
        </server>
        <config>
            <prop name="ArrURL">http://localhost:8081/arrudp</prop>
            <prop name="MaxEventDisplay">500</prop>
        </config>
    </service>
    <!-- End of Rialto Shared SERVICES Section -->

    <!-- HL7 Proxy Section - This service splits the traffic from multiple RIS interfaces received at a single port on a single server 
     to the Internal HL7 Servers for each Rialto instance.
     NOTE: not required for single context sites.-->
    <!--include location="Specific_Site_Configs/hl7proxy.xml" /-->

    <!-- Individual Site Configuration Section:  This section calls all the include files for each individual site
         NOTE: there will only be one file for single context sites-->
    <include location="Specific_Site_Configs/site1_config.xml" />
    <!-- End of Individual Site Configurations -->


    <!-- Rialto Servers Section -->
    <server id="Audit_Record_Repository_Server-http" type="http">
        <port>${Audit_Record_Repository_Server_Port}</port>
    </server>

    <server id="Event_Collector_and_Navigator_Server-http" type="http">
        <port>${Event_Collector_Server_Port}</port>                                                                                                                                                               
    </server>
    <!-- End of Rialto Servers Section -->


    <!-- Devices to Connect To -->
                                                                                                                                                                                       
    <!-- Local devices  -->
    <device id="audit" type="audit">
        <port>${IHE_Audit_Records_Server_UDP_Port}</port>
        <host>localhost</host>
    </device>


    <!-- Infrastructure Server DEVICES -->
    <device type="xdsreg" id="XDS_Registry">
        <timeout>10m</timeout>
        <url>${XDS_Registry_URI}</url>
        <tls>
            <keystore>RialtoKeystore.jks</keystore>
            <keystorepass>connect2016</keystorepass>
            <keypass>hdlaxds2019</keypass>
            <truststore>RialtoTruststore.jks</truststore>
            <truststorepass>rialto</truststorepass>
        </tls>
    </device>

    <device type="dicom" id="DIR2">
        <host>${DIR2_Host}</host>
        <port>${DIR2_Port}</port>
        <timeout>10m</timeout>
        <aetitle>${DIR2_AETitle}</aetitle>
    </device>

    <device type="dicom" id="DIR3">
        <host>${DIR3_Host}</host>
        <port>${DIR3_Port}</port>
        <timeout>10m</timeout>
        <aetitle>${DIR3_AETitle}</aetitle>
    </device>

    <!-- Turing off Sherbrooke DIR
    <device type="dicom" id="DIR4">
        <host>${DIR4_Host}</host>
        <port>${DIR4_Port}</port>
        <aetitle>${DIR4_AETitle}</aetitle>
    </device>
    -->

    <!-- End of devices section -->

</config> 

