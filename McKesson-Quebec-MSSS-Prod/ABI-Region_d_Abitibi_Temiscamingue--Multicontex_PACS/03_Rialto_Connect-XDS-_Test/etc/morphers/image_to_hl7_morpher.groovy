LOAD("Shared_Scripting_Files/contexts.groovy")
LOAD("Shared_Scripting_Files/common_localization_routines.groovy")
LOAD("Shared_Scripting_Files/shared_constants.groovy")
LOAD("Shared_Scripting_Files/shared_hl7_resources.groovy")

import com.karos.rtk.common.HL7v2Date

context = Contexts.getContext(config.context)
if (context == null) {
    log.warn("Context is unknown from id: {}. Dropping this Order. " +
        "Please correct either the connect ContextID configuration or contexts.groovy", config.context)
    return false
}

log.debug("Starting image_to_hl7_order with input:\n {}", input)

initialize('ORM', 'O01', '2.3')

// This is required so that the PACS HL7 handler accepts the message
set('MSH-6', context.preferredRISSendingFacility())

set('MSH-7', HL7v2Date.now(HL7v2Date.DateLength.MILLISECOND3))

// HL7 Character Set supported by French HMI PACS
set('MSH-18', '8859/1')


/* PID */

set("PID-1", "1")

def sourcePID = get(PatientID)
if (sourcePID != null) {
    set('PID-3', sourcePID)
} else {
    log.error("The Patient Localization failed for ORM Creation!!  The localized Patient is null")
    return false
}

set('PID-3-6', context.preferredRISSendingFacility())

pids = new Pids(originalInput, prefetchOrder)
log.debug("Known pids from originalInput: {}", pids.issuerToPid)
def patsec = pids.getPid(SharedConstants.RegionalDomainOID)
set('PID-4', patsec)
log.debug("PID-4: {}", patsec)


/**
 * Copies up to three components of a name from the dicom into the hl7 message.
 * DICOM person name elements are supposed to be of the form
 * family^given^middle^prefix^suffix.  HL7 names are sometimes of the form
 * family^given^middle^suffix^prefix but some components are actually 
 * id^family^given^...
 * 
 * In the case where the id is there, pass startingComponent=2 to offset where
 * we put the name.  However, in our samples, the values of the person names
 * actually include the id (they are in DICOM format instead of HL7).  In this
 * case, just leave the default startingComponent.
 * 
 * @param dicomTag place to get name from in dicom
 * @param hl7prefix place to put name in hl7
 * @param startingComponent if name doesn't line up at beginning of 
 * @return
 */
def setName(dicomTag, hl7prefix, startingComponent=1) {
    nameParts = split(dicomTag)
    if (nameParts == null) {
        return
    }

    // loop far enough to get the id if present, plus the family and given names
    int maxPartsToCopy = 3
    
    for (int i = 0; i < maxPartsToCopy && i < nameParts.size(); i++) {
        set(hl7prefix + "-" + (startingComponent++), nameParts[i])
    }
}

setName(PatientName, "PID-5")

set("PID-6", get(PatientMotherBirthName))

set("PID-7", get(PatientBirthDate))

set("PID-8", get(PatientSex))

set("PID-11", get(PatientAddress))

set("PID-13", get(PatientTelephoneNumbers))


/* ORC */
set("ORC-1", "NW")

set("ORC-3", get(AccessionNumber))

// order status in non-standard location:
set("ORC-16", "En attente")


/* OBR */
set("OBR-1", "1")

set("OBR-3", get(AccessionNumber))

// This is the Top-Level CodeValue
// This is currently being populated as the Normalizaed Procedure Code in the Foreign Image Localizer script
set("OBR-4-1", get(CodeValue))

set("OBR-4-2", get(StudyDescription))

// Foreign Procedure Code
set("OBR-4-4", get(AccessoryCode))

// Foreign Procedure Description
set("OBR-4-6", get(AcquisitionComments))

def studyDate = get(StudyDate)
def studyTime = get(StudyTime)
def contentDate = get(ContentDate)
def contentTime = get(ContentTime)

log.trace("Study Date/Time: {}{}; Content Date/Time: {}{}", studyDate, studyTime, contentDate, contentTime)

def observationDateTime = null

if (studyDate != null && !studyDate.isEmpty()) {
    observationDateTime = studyDate + (studyTime == null ? "" : studyTime)

} else if (contentDate != null && !contentDate.isEmpty()) {
    observationDateTime = contentDate + (contentTime == null ? "" : contentTime)
    log.debug("Study Date and Time incomplete or blank. " + 
              "Using Content Date and Time to populate HL7 Observation Date and Time")

} else {
    log.warn("Study Date is non-existent.  Leaving HL7 Observation Date and Time blank!!")
} 

if (observationDateTime != null) {
    // trim sub-second component for PACSs that don't like it:
    observationDateTime = observationDateTime.replaceAll("\\..*", "")
}

log.debug("Observation date/time: {}", observationDateTime)

set("OBR-7", observationDateTime)

// As per McKesson's request, adding the observationDateTime to OBR-6 so that Foreign Exams
// do not show up in Rad Technician's worklists that talk to the PACS
// Please refer to KHC 1384 for details
log.debug("Populating Content Date / Time in OBR-6 so that Rad Technician's worklists don't show foreign exams")
set("OBR-6", observationDateTime)

// Populate the Anatomic Region 
anatomicRegionSeq = get(AnatomicRegionSequence)
if (!anatomicRegionSeq.isEmpty()) {
    anatomicRegion = anatomicRegionSeq.first()
    set("OBR-15-1-1", anatomicRegion.get(CodeValue))
    set("OBR-15-1-2", anatomicRegion.get(CodeMeaning))
    set("OBR-15-1-3", anatomicRegion.get(CodingSchemeDesignator))
}

// our samples includes the physician id (non-standard)
setName(ReferringPhysicianName, "OBR-16")

set("OBR-19", get(AccessionNumber))

set("OBR-20", get(StudyID))

set("OBR-22", get(ContentDate) + get(ContentTime))

def modality = get(Modality)
def dominantModality = get(ModalitiesInStudy)
if (modality == "SR" && dominantModality != null && !dominantModality.isEmpty()) {
    // The image localizer script calculates the dominant modality of
    // the study from the xds metadata and stores it in ModalitiesInStudy
    // only for SRs.  If this isn't an SR, we have to hope that the first
    // image has the dominant modality.
    // TODO: use the xds metadata here once the product makes it available
    log.debug("First image in the study is an SR. Using the dominant modality " +
        "calculated by the image localizer: {}", dominantModality)
    modality = dominantModality
} else {
    log.debug("Using modality of first image in the study as the dominant one: {}", modality)
}
set('OBR-24', modality)

set('OBR-27-4', observationDateTime)

/* ZDS */
output.getMessage().addNonstandardSegment('ZDS')

set('ZDS-1-1', get(StudyInstanceUID))

set('ZDS-1-2', SharedConstants.HL7_RIS_Brand_Name)

set('ZDS-1-3', 'Application')

set('ZDS-1-4', 'DICOM')


log.debug("This is the Order that will get sent:\n{}", output)

