
/**
 * Representation of a single context.  See Contexts class below for
 * a set of instances representing the contexts for this deployment.
 */
class Context {
    /** AE title of Rialto's CMove interface */
    String RialtoRetrieveAE

    String InstitutionName

    /** Assigning Authority for the local PACS */
    String PACSIssuer

    /** The calling ae title used by the PACS for query/retrieve.
     * Get this from the connectivity spreadsheet or the SAD */
    String PACSCallingAE

    /** Context ID as known by the PACS; returned as IssuerOfPatientID
     * in cfind queries; used to identify contexts in demographics queries */
    String ContextID

    /** The HL7 sending facilities for the local RIS.
     * The first one will be used when we need to set the value somewhere. */
    List RISSendingFacility

    /** The HL7 sending applications for the local RIS.
     * The first one will be used when we need to set the value somewhere. */
    List RISSendingApplication

    String HashPrefix

    public String localizeUID(uid, hash) {
        if (HashPrefix == null || uid == null) {
            return uid
        }

        return hash(HashPrefix, uid)
    }

    public String unlocalizeUID(uid, unhash) {
        //  if we're not hashing, we're also not unhashing:
        if (HashPrefix == null) {
            return uid
        }

        return unhash(uid)
    }

    /**
     * @return the preferred sending application for this context's RIS
     */
    public String preferredRISSendingApplication() {
        return RISSendingApplication.isEmpty() ? null : RISSendingApplication[0]
    }

    /**
     * @return the preferred sending facility for this context's RIS
     */
    public String preferredRISSendingFacility() {
        return RISSendingFacility.isEmpty() ? null : RISSendingFacility[0]
    }

    /**
     * @return if the issuer is local to the PACS
     */
    public boolean isLocal(issuer) {
        if (Contexts.CONTEXTS_ARE_ISOLATED) {
            return issuer == PACSIssuer
        } else {
            return Contexts.ALL_CONTEXTS.any { id, c -> c.PACSIssuer == issuer }
        }
    }
}

class Contexts {
    /**
     * If true, the contexts in the PACS cannot see eachother's images and
     * we should fetch each study into each context.  In this case the hash
     * prefix should also be set.
     *
     * If false, they can see eachother's images.  We should not fetch any
     * studies that are local to any of the contexts.  Hashing is unnecessary.
     *
     * See Context.isLocal above.
     */
    public static final CONTEXTS_ARE_ISOLATED = false

    // Add one entry per context here.  The key should
    // correspond to the ContextID configuration parameter.
    // Set the HashPrefix for multi-context sites and leave
    // it null for single-context deployments.
    private static final ALL_CONTEXTS = [

        "VALD": new Context(
            RialtoRetrieveAE:       "RIALTO_VALD_TST",
            InstitutionName:        "Hôpital et CLSC de Val-d'Or",
            PACSIssuer:             "2.16.124.10.101.1.60.1.1013.1",
            PACSCallingAE:          "ALI_SCU",
            ContextID:              "VALD",
            RISSendingFacility:     ["106", "106.1"],
            RISSendingApplication:  ["Lawson", "MEDISOLUTION"]),

        "CHLS": new Context(
            RialtoRetrieveAE:       "RIALTO_CHLS_TST",
            InstitutionName:        "Centre de soins de courte durée La Sarre",
            PACSIssuer:             "2.16.124.10.101.1.60.1.1016.1",
            PACSCallingAE:          "ALI_SCU",
            ContextID:              "CHLS",
            RISSendingFacility:     ["105"],
            RISSendingApplication:  ["Lawson", "MEDISOLUTION"]),

        "PAVM": new Context(
            RialtoRetrieveAE:       "RIALTO_PAVM_TST",
            InstitutionName:        "Pavillon Ste-Famille",
            PACSIssuer:             "2.16.124.10.101.1.60.1.1018.1",
            PACSCallingAE:          "ALI_SCU",
            ContextID:              "PAVM",
            RISSendingFacility:     ["103"],
            RISSendingApplication:  ["Lawson", "MEDISOLUTION"]),

        "CHRN": new Context(
            RialtoRetrieveAE:       "RIALTO_CHRN",
            InstitutionName:        "Hôpital de Rouyn-Noranda",
            PACSIssuer:             "2.16.124.10.101.1.60.1.1015.1",
            PACSCallingAE:          "ALI_SCU",
            ContextID:              "101",
            RISSendingFacility:     ["101"],
            RISSendingApplication:  ["Lawson", "MEDISOLUTION"]),

        "TEKI": new Context(
            RialtoRetrieveAE:       "RIALTO_TEKI",
            InstitutionName:        "Point de service de Témiscamingue-et-de-Kipawa",
            PACSIssuer:             "2.16.124.10.101.1.60.1.1024.1",
            PACSCallingAE:          "ALI_SCU",
            ContextID:              "108",
            RISSendingFacility:     ["108"],
            RISSendingApplication:  ["Lawson", "MEDISOLUTION"]),

        "HDDA": new Context(
            RialtoRetrieveAE:       "RIALTO_HDDA",
            InstitutionName:        "Hôpital d'Amos",
            PACSIssuer:             "2.16.124.10.101.1.60.1.1023.1",
            PACSCallingAE:          "ALI_SCU",
            ContextID:              "104",
            RISSendingFacility:     ["104"],
            RISSendingApplication:  ["Lawson", "MEDISOLUTION"]),

        "CHIS": new Context(
            RialtoRetrieveAE:       "RIALTO_CHIS",
            InstitutionName:        "Centre hospitalier régional de Chisasibi",
            PACSIssuer:             "2.16.124.10.101.1.60.1.3000.102.10.1071.0",
            PACSCallingAE:          "ALI_SCU",
            ContextID:              "168",
            RISSendingFacility:     ["168"],
            RISSendingApplication:  ["Lawson", "MEDISOLUTION"]),

        "CSVO": new Context(
            RialtoRetrieveAE:       "RIALTO_CSVO",
            InstitutionName:        "CLSC de Senneterre",
            PACSIssuer:             "2.16.124.10.101.1.60.1.1012.1",
            PACSCallingAE:          "ALI_SCU",
            ContextID:              "107",
            RISSendingFacility:     ["107"],
            RISSendingApplication:  ["Lawson", "MEDISOLUTION"]),

    ]

    /**
     * Returns the context configuration for the given contextId
     * @return null if the context id isn't known
     */
    public static Context getContext(contextId) {
        // single context case, context id might not be configured
        if (contextId == null && ALL_CONTEXTS.size() == 1) {
            // return the only value:
            return ALL_CONTEXTS.findResult { it.value }
        }

        // If this returns null, we're almost certainly misconfigured
        // so we should blow up.  However, the best way to do that might
        // be to have the caller simply return false to drop the current
        // data, so we have to let the caller handle it.
        return ALL_CONTEXTS[contextId]
    }
}

