/* fps_inbound_messages_morpher.groovy
 *
 * This script cleans up the HL7 Traffic that goes to the Fetch Prior Studies handler
 */

LOAD("Shared_Scripting_Files/shared_constants.groovy")
LOAD("Shared_Scripting_Files/shared_hl7_resources.groovy")
LOAD("Shared_Scripting_Files/contexts.groovy")

log.debug("Starting fps_inbound_messages_morpher")
log.debug("Message as received at fps inbound messages morpher:\n{}", input)

SharedHl7.normalizeCharset(input, log)

def messageCode = get('MSH-9-1')
log.debug("Local Fetch Prior Studies Forwarder: Received MSH-9-1 (Message Code) was: '{}'", messageCode)

if (!["ORM", "ADT"].contains(messageCode)){
    log.debug("Non-ORM or ADT Message Received")
    return false
}

if (messageCode == 'ORM') {
    // ADTs have no OBR segment
    set('OBR-15-4', get('OBR-15-1'))
}

if (!SharedHl7.normalizePids(input, log, config.context, "fps")) {
    return false
}

log.debug("This is the message that will be passed to the Fetch Prior Studies Handler:\n{}", output)

