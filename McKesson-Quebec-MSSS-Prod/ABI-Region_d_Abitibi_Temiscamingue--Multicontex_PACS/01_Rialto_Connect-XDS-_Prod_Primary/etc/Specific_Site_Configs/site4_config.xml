<!-- Rialto Connect XDS-i Consumer Instance for Site <SITE NAME> @ <INSTITUTION NAME> Multi-context PACS
        Rialto Connect (XDS version), serving as XDS-i Consumer for the McKesson HMI PACS -->

<config>    
    <!-- Variable Declaration: Infrastructure and Rialto Connect Configuration
        This section defines variables that are used in Rialto's configuration -->

    <var name="Site4_DICOM_Server_Port">4108</var>

    <!-- 5556 is for the production RIS feed. 5557 is for manual testing with hl7snd -->
    <var name="Site4_FetchPriorStudies_and_PIX_Server_Port">5567</var>

    <!-- C-Store SCU Value that Rialto will use when moving studies into the PACS -->
    <var name="Site4_Rialto_CSTORE_CSU_AETitle">RIALTO_CHRN</var>    

    <var name="Site4_storage" value="${rialto.rootdir}/var/connect/Site4" />

    <var name="morphers">${rialto.rootdir}/etc/morphers</var>


    <!-- Infrastructure Servers Configuration-->

    <!-- The Local xDL DI-R node. - -  This is the DI-r in Montreal City for the Université McGill and Université de Montréal RUISs. This is known as "RID MONTREAL" -->
    <var name="Site4_DIR1_XDS_Repostory_UID">2.16.124.10.101.1.60.2.60</var>
    <var name="Site4_DIR1_AETitle">XDL_CHRN_QRSCP</var>
    <var name="Site4_DIR1_Host">10.241.25.59</var>
    <var name="Site4_DIR1_Port">4001</var>


    <!-- This is the local HMI PACS -->
    <var name="Site4_PACS_Host">10.20.33.139</var>

    <var name="Site4_Local_PACS_HL7_Order_Host">${Site4_PACS_Host}</var>
    <var name="Site4_Local_PACS_HL7_Order_Port">5555</var>
    <var name="Site4_Local_PACS_HL7_Order_Sending_Application">BROKER</var>
    <var name="Site4_Local_PACS_HL7_Order_Sending_Facility">101</var>

    <var name="Site4_Local_PACS_CFIND_Host">${Site4_PACS_Host}</var>
    <var name="Site4_Local_PACS_CFIND_AETitle">ALI_QUERY_SCP</var>
    <var name="Site4_Local_PACS_CFIND_Port">5000</var>

    <var name="Site4_Local_PACS_CSTORE_Host">${Site4_PACS_Host}</var>
    <var name="Site4_Local_PACS_CSTORE_AETitle">ALI_STORE_SCP</var>
    <var name="Site4_Local_PACS_CSTORE_Port">4000</var>

    <!-- // End of Variable Declarations -->


    <!-- Rialto SERVICES for Site4 - Declaration -->

    <!-- Main Connect XDS Service-->
    <service type="connect-xds" id="Site4_Connect-XDS">

        <!-- ===========   Rialto  Servers used by Connect XDS Service =========  -->

        <!-- Same DICOM server for all DICOM workflows -->
        <server idref="Site4_Main_DICOM_Server" name="cfind" />
        <server idref="Site4_Main_DICOM_Server" name="cmove" />
        <server idref="Site4_Main_DICOM_Server" name="cstore" />

        <!-- Catch-all Server to Receive All HL7 Traffic for both PIX and Fetch Prior Studies -->
        <server idref="Site4_FPS_and_PIX_Server" name="all" />


        <!-- ========= Network and Infrastructure Devices used by Connect XDS Service ============  -->

        <device idref="XDS_Registry" />

        <!-- The DIRs would go here.  Since it's Connect XDS, the Service uses the RepositoryUIDToDIRAETitleMap in the Config below
             to identify the DIRs.  The Java code simply maps the RepoUID value to the AETitle, and it searches the devices list to
             find which device to use for which DI-r.  -->

        <!-- Turning OFF Handling local studies committed to DI-r for local studies.
                Not needed; the HMI PACS at HSC queries / retreives its own local studies in the DIR  -->
        <!--
        <device idref="Site4_DIR1" name="Site4_HomeDIR" />  <!- -  Home DIR (i.e. the long term storage at the DIR for the local PACS. - ->
        -->

        <device idref="Site4_Local_PACS_CSTORE" name="PACSStorage"/>
        <device idref="Site4_Local_PACS_HL7_Order" name="PACSHL7"/>

        <!-- This "PACS Query device" is used to:
             a) query the PACS to see if it already has a study before it CMoves the study in.
             b) query the PACS for Local Patient Demographics -->
        <device idref="Site4_Local_PACS_CFIND" name="PACSQuery"/>


        <!-- ========== Rialto Connect configuration ============== -->
        <config>
            <!-- This is the Issuer of PID at the Domain Level (RAMQ) in Quebec -->
            <prop name="AffinityDomain" value="${Affinity_Domain_Value}" /> 

            <!-- This specifies the local PACS context to the shared scripts -->
            <prop name="ContextID" value="CHRN" />

            <!-- Maps the Repository UIDs (OIDs) in the XDS Registry (which is all it knows) to known DIR AETitles -->
            <prop name="RepositoryUIDToDIRAETitleMap">
                ${Site4_DIR1_XDS_Repostory_UID}:${Site4_DIR1_AETitle}

                ${DIR2_XDS_Repostory_UID}:${DIR2_AETitle}

                ${DIR3_XDS_Repostory_UID}:${DIR3_AETitle}

                <!-- Turning off the Sherbrooke DIR until it is ready.
                ${DIR4_XDS_Repostory_UID}:${DIR4_AETitle}
                -->
            </prop>

            <!-- Determines whether connect sees each DICOM association from DI-r as a study, or it can accept multiple associations per study -->
            <prop name="StudyGroupingStrategy" value="timeout" />
            <!-- Determines when a study is considered to be finished if the study strategy is "timeout" -->
            <prop name="StudyGroupingTimeout" value="30s" />

            <prop name="LocalAETitle" value="${Site4_Rialto_CSTORE_CSU_AETitle}" />   <!-- Used as Rialto's C-Store SCU Calling AETitle -->



            <!-- PIX (Mini-PIX) Configuration - Cassandra -->
            <include location="CassandraProdConfig.xml" />

            <prop name="PIXMorpher" value="${morphers}/pix_morpher.groovy" />

            <prop name="PriorStudyImmediateAck" value="true" />

            <!-- This specifies what Extended Metadata Field to look for to get a WADO URI that will serve a Medical Report
                    This is only for when the system does SRs as well as WADO reports -->
                <!-- Turning it off
            <prop name="ExtendedMetadataKeyForReportWADO" value="${Report_WADO_URI_Field}" />
                -->


            <!-- Caches Section -->

            <prop name="RetrieveAECacheTimeout" value="1h" />   <!-- Used to keep the Retrieve AE Title for study responses that Connect gets (i.e. don't query the Reg again) -->
            <prop name="RetrieveAECacheCheckFrequency" value="100s" />

            <!-- This cache remembers FULL orders that have been recently seen and ignores them if they are sent again.
                The cache uses a composite key of the affinity pid, the local pid, the modality, the accession number and the anatomic region
                of the order (PID-2, PID-3, ORC-3, OBR-24 and OBR-15-4 respectively from the order message)
                If the RIS sends the order content in a different place, you can currently morph using the HL7 Proxy.
                Setting this parameter turns on the cache -->
            <prop name="FPSRecencyCacheTimeout" value="24h" />  -->

            <!-- This cache keeps fragmentary orders until they are ready to be processed by the Fetch Prior Studies workflow of Connect.
                 It is hard-coded into Java.  If the below Property is not set, it is not used. (Meaning fragmentary orders are ignored)
                 This cache specifies how long to keep orders without affinity pids -->
            <prop name="FPSMissingAffinityCacheTimeout" value="24h" />

            <!-- This Configuration allows Rialto to retry individual orders whose prefetch failed.
                 It uses a temporary Processing Directory.  After Rialto has successfully processed the order, it moves the order to
                 an archive directory-->
            <prop name="OrderRetry"> 
                <storageDir>${Site4_storage}/orders</storageDir>
           </prop>

            <!-- This property multiplexes multiple DICOM associations from the DI-r so that Rialto sends studies over a single association
                 to the PACS.  It also enables queuing and retrying of failed images  -->
            <prop name="ImageDelivery">
                <caching>
                    <cacheDir>${Site4_storage}/images-cache</cacheDir>
                    <maxRetries>5</maxRetries> <!-- Specifies how many times failed SOPs will be retried before Rialto gives up on those images -->
                    <retryWaitTime>5m</retryWaitTime>
                    <maxConcurrentCStores>3</maxConcurrentCStores> <!-- This is throttling as to not overwhelm the PACS -->
                    <!-- Please note that if maxConcurrentCStores is enabled, each study will still only be sent over a single association
                         This actually means total number of concurrent studies being delivered to PACS at a time. -->
                </caching>
            </prop>

            <!-- Turning off Study Instance UID (SIUID cache) for Fetch Priors   NOTE: For HMI PACS and other PACS that can be queried
                    through an instance availability query, this cache is not needed and should be turned off.

            <!- - Fetch Prior Studies Cache
                    This 'cache' simply keeps a listing of Study Instance UIDs that have already been moved (pre-fetched) to the PACS, so
                    that they are not re-sent to the PACS in a given amount of time.  This is in case several copies of the same pre-fetch order
                    arrive at Connect within a specific amount of time. - ->
            <!- - This should really only be used if the PACS can't be queried for Study Instance Availability, or if for performance
                    reasons the site does not allowed the PACS to be queried for Studies - ->
            <prop name="FetchPriorStudiesCacheFolder" value="${Site4_storage}/suidcache" />
            <prop name="FetchPriorStudiesCacheTimeout" value="1m" />
            <prop name="FetchPriorStudiesCacheCheckFrequency" value="1m" />
            -->

            <!-- End of Rialto Caches Section -->


            <!-- ============================================================ -->
            <!-- ============  Scripts for the Connect XDS Service  =========  -->
            <!-- ============================================================ -->

            <!-- Fetch Prior Studies Workflow -->

                <!-- This morphing script just cleans up / rejects incoming messages-->
                <prop name="FPSInboundMorpher" value="${morphers}/fps_inbound_messages_morpher.groovy" />

                <!-- Transforms RIS HL7 Orders into a CFind. The resulting C-Find is then transformed into an  XDS Find Documents Query-->
                <prop name="HL7ToCFindMorpher" value="${morphers}/hl7_to_cfind_morpher.groovy" />

                <!-- Filters all Priors against a given set of rules  -->
                <prop name="FPSXDSResponseFilter" value="${morphers}/xds_response_filter.groovy" />

                <!-- Filters studies from being added to the set of Pre-fetched studies for FPS -->
                <prop name="CFindResponseFilter" value="${morphers}/cfind_response_filter.groovy" />

                <!-- Morpher script to Filter studies by Absolute Number of studies.  - ->
                <prop name="CFindResponseRankFilter" value="${morphers}/cfind_response_rank_filter.groovy" />
                TURNING OFF -->


            <!-- Ad-Hoc Query / Retreive Workflow -->

                <!-- This script localizes responses from the XDS Registry. It ONLY runs for Ad-Hoc C-Finds -->
                <prop name="CFindResponseLocalizer" value="${morphers}/cfind_response_localizer.groovy" />


            <!-- SHARED Scripts for both Ad-Hoc Query / Fetch Prior Studies -->

                <!-- Transform DICOM C-Finds from PACS into XDS "Find Docs" Queries - Key aspect of cfind workflow: -->
                <prop name="CFindToFindDocumentsMorpher" value="${morphers}/cfind_to_find_documents.groovy" />

                <!--TURNING OFF Handling PACS' long term local studies in "Home DI-r" segment
                <prop name="HomeDIRCFindRequestMorpher" value="${morphers}/homedir_cfind_req_morpher.groovy" />     <!- - Handles PACS' long term local studies in "Home DI-r" segment - ->
                <prop name="HomeDIRCFindResponseMorpher" value="${morphers}/homedir_cfind_resp_morpher.groovy" />
                -->

                <!-- CFIND to PACS before moving a study into the PACS -->
                <!-- *** NOTE:  The "PACS Query" in this Service's "Devices" Section must be enabled for this to work -->
                <prop name="FPSInstanceAvailabilityQueryStrategy" value="SINGLE_SUID" /> 

                <!-- This script Queries the Local PACS to get local Patient Demographics (so that the PACS assigns the study to the correct context  -->
                <!-- *** NOTE:  IMPORTANT this script MUST be declared for Connect to start!  If you don't want to query the local PACS, simply "return false"  -->
                <prop name="PatientCFindToPACSRequestMorpher">${morphers}/patient_cfind_request_morpher.groovy</prop>

                <!-- For Fetch Prior Studies and Ad-Hoc Retreives.  Localizes Images. ("Ingestion" in Event Viewer) before forwarding them to PACS -->
                <prop name="ForeignImageLocalizer" value="${morphers}/foreign_image_localizer.groovy" />

                <!-- SR to ORU Report Transformation Script  -->
                <prop name="DICOMSRToORUMorpher" value="${morphers}/sr_to_oru_morpher.groovy" />

                <!-- For Studies with multiple Reports, takes the entire set of ORU transformed reports, and runs operations on them
                     Used to only send the latest report to the PACS.  (If you need all reports, don't declare it) -->
                <prop name="ReportRankFilter">${morphers}/report_rank_filter.groovy</prop>

                <!-- ** NOTE: If you need to Query the PACS for studies before you move them, or query for Patient Demographics DO NOT Turn OFF this morpher.
                        If you don't need an order, simply "return false" -->
                <prop name="ImageToHL7OrderMorpher" value="${morphers}/image_to_hl7_morpher.groovy" />  <!-- Optionally generate an order before sending localized images to PACS -->

                <!-- Morphs the incoming C-MOVE requests from PACS to convert any hashed Study Instance UIDS -->
                <prop name="CMoveRequestMorpher" value="${morphers}/cmove_request_morpher.groovy" />
                <prop name="FPSInstanceAvailabilityMorpher" value="${morphers}/fps_instance_availability_query_morpher.groovy" />

        </config>
    </service>

    <!-- End of Rialto SERVICES for Site4 Section -->

    <!-- Rialto Servers Section -->

    <server id="Site4_FPS_and_PIX_Server" type="hl7v2">
        <host>localhost</host>
        <port>${Site4_FetchPriorStudies_and_PIX_Server_Port}</port>
        <defaultEncoding>${defaultCharacterSet}</defaultEncoding>
    </server>

    <server id="Site4_Main_DICOM_Server" type="dicom">
        <port>${Site4_DICOM_Server_Port}</port>
        <!-- Limiting AE titles that can call Rialto Connect - ->
        <allowCallingAE>ALI_SCU</allowCallingAE>
        <allowCallingAE>ALI_QUERY_SCU</allowCallingAE> -->
    </server>

    <!-- End of Rialto Servers Section -->



    <!-- Devices to Connect To -->

    <device type="dicom" id="Site4_DIR1">
        <!-- This is "Home DIR" -->
        <host>${Site4_DIR1_Host}</host>
        <port>${Site4_DIR1_Port}</port>
        <aetitle>${Site4_DIR1_AETitle}</aetitle>
    </device>

    <device type="dicom" id="Site4_Local_PACS_CFIND">
        <host>${Site4_Local_PACS_CFIND_Host}</host>
        <port>${Site4_Local_PACS_CFIND_Port}</port>
        <aetitle>${Site4_Local_PACS_CFIND_AETitle}</aetitle>
    </device>

    <device type="hl7v2" id="Site4_Local_PACS_HL7_Order">
        <host>${Site4_Local_PACS_HL7_Order_Host}</host>
        <port>${Site4_Local_PACS_HL7_Order_Port}</port>
        <sendingApplication>${Site4_Local_PACS_HL7_Order_Sending_Application}</sendingApplication>
        <sendingFacility>${Site4_Local_PACS_HL7_Order_Sending_Facility}</sendingFacility>
        <persistentConnection>true</persistentConnection>
        <timeout>5m</timeout>
    </device>

    <device type="dicom" id="Site4_Local_PACS_CSTORE">
        <host>${Site4_Local_PACS_CSTORE_Host}</host>
        <port>${Site4_Local_PACS_CSTORE_Port}</port>
        <aetitle>${Site4_Local_PACS_CSTORE_AETitle}</aetitle>
    </device>

    <!-- End of Devices Section -->
</config>
