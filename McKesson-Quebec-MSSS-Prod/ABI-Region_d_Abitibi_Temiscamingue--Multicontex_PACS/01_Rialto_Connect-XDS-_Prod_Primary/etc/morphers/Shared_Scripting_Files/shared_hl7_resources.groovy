class SharedHl7 {
    /* General Documentation:
     *   Common ADT Codes:
     *       A34 (merge patient information-patient ID only)
     *       A35 (merge patient information-account number only)
     *       A36 (merge patient information-patient ID and account number)
     *       A39 (merge person - external ID)
     *       A40 (merge patient - internal ID)
     *       A41 (merge account - patient account number)
     *       A42 (merge visit - visit number)
     */
    static final AdmitTriggerEvents = ['A01', 'A04', 'A05', 'A08', 'A31']
    static final MergeTriggerEvents = ['A34', 'A36', 'A39', 'A40', 'A43']
    static final SupportedADTTriggerEvents = AdmitTriggerEvents + MergeTriggerEvents

    /**
     * Description of how to parse and normalize the patient ids in incoming hl7 messages.
     * Each entry is a map with the following keys:
     *
     * description:         Short, human readable string for logging
     *
     * field:               Location of pid within hl7 message.  Must have only the field specified
     *                      (eg. PID-3 is valid, PID-3-1 is not).
     *
     * required:            Whether or not messages without this field should be rejected.
     *                      May be a boolean value or the result of calling forOperations(...) with
     *                      a list of operations for which the field is required.  Currently
     *                      supported operations are "pix" and "fps".
     *
     * invalidationMethod:  A method or closure that takes a string and returns true if the pid
     *                      value is invalid.  If this returns false and the field is required,
     *                      the message will be rejected.
     *
     * expectedDomain:      The domain to either validate or place into the field.  May be a string
     *                      or a closure that takes the current context, for multi-context sites, and
     *                      returns the desired domains.  May be null, indicating that any pid with
     *                      an unknown domain should be cleared.
     *
     * alwaysClear:         Optional.  If true, the field is cleared unconditionally.  Only to be
     *                      used if you know the site sends bad values here.
     */
    static final PIDConfiguration = [
        [
            description:        "RAMQ",
            field:              "PID-2",
            required:           forOperations("pix"),
            invalidationMethod: QuebecHL7Validation.&RAMQformatIsInvalid,
            expectedDomain:     SharedConstants.Affinity_Domain_OID],
        [
            description:        "MRN",
            field:              "PID-3",
            required:           true,
            expectedDomain:     { context -> context.PACSIssuer }],
        [
            description:        "PATSEC",
            field:              "PID-4",
            required:           false,
            expectedDomain:     SharedConstants.RegionalDomainOID],
        [
            description:        "Merged MRN",
            field:              "MRG-1",
            required:           all(forOperations("pix"), forTriggerEvents(MergeTriggerEvents)),
            expectedDomain:     { context -> context.PACSIssuer }],
    ]

    /**
     * @return a closure that, when called with the current operation,
     *         returns true if the current operation is in the list of
     *         operations specified.
     */
    private static forOperations(String... ops) {
        return { op, msg -> (ops as List).contains(op) }
    }

    /**
     * @return a closure that, when called with the current message,
     *         returns true if the message is only of the given trigger
     *         events.
     */
    private static forTriggerEvents(events) {
        return { op, msg -> events.contains(msg.get('MSH-9-2')) }
    }

    private static all(Closure ... predicates) {
        return { op, msg -> predicates.every { it(op, msg) } }
    }

    private static any(Closure ... predicates) {
        return { op, msg -> predicates.any { it(op, msg) } }
    }

    /**
     * Normalize the pids in an hl7 message by adding domains based on known
     * message locations.  Configure using PIDConfiguration above.
     * Before calling this, please be sure to include shared_constants.groovy and contexts.groovy
     *
     * @return whether or not the message is valid
     */
    public static boolean normalizePids(msg, log, contextId, operation) {
        def context = Contexts.getContext(contextId)
        if (context == null) {
            log.error("Context is unknown from id: {}. Unable to localize this message, dropping it. " +
                "Please correct either the connect ContextID configuration or contexts.groovy", contextId)
            return false
        }

        for (pidspec in PIDConfiguration) {
            if (pidspec.alwaysClear) {
                log.debug("Unconditionally clearing {} ({})", pidspec.description, pidspec.field)
                ignoreException { clearPid(pidspec, msg) }
                continue
            }

            def domainLocation = pidspec.field + "-4-2"

            // ignore exceptions for fields like MRG-1, which won't be
            // available in non-merge message types
            def pidValue = ignoreException { msg.get(pidspec.field) }
            if (pidValue == null || pidValue.isEmpty()) {

                def required = pidspec.required
                if (required instanceof Closure) {
                    required = required(operation, msg)
                }

                if (required) {
                    log.debug("Message is missing required {}. Dropping.",
                        pidspec.description)
                    return false

                } else {
                    // ignore pids that aren't present and not required:
                    ignoreException { clearPid(pidspec, msg) }
                    continue
                }
            }

            if (pidspec.invalidationMethod != null && pidspec.invalidationMethod(pidValue)) {
                log.warn("{} '{}' is invalid. Dropping message.", pidspec.description, pidValue)
                return false
            }

            def domain = msg.get(domainLocation)
            if ("".equals(domain)) {
                domain = null
            }

            def expectedDomain = pidspec.expectedDomain
            if (expectedDomain instanceof Closure) {
                expectedDomain = expectedDomain(context)
            }

            if (expectedDomain == null && domain == null) {
                clearPid(pidspec, msg)
                pidValue = null

                log.debug("Clearing {} because the domain is unknown", pidspec.field)

            } else if (domain == null) {
                msg.set(domainLocation, expectedDomain)
                log.debug("Setting domain for {} at {} to {}",
                    pidspec.description, domainLocation, expectedDomain)

            } else if (expectedDomain != null && domain.toUpperCase() != expectedDomain) {
                log.warn("Unexpected domain '{}' for {} in {}.  Expected '{}'. Dropping message.",
                    domain, pidspec.description, domainLocation, expectedDomain)

                return false
            }

            msg.set(pidspec.field, pidValue == null ? null : pidValue.toUpperCase())
            log.debug("Using {} ({}): {} in domain {}",
                pidspec.description,
                pidspec.field,
                msg.get(pidspec.field),
                msg.get(domainLocation))
        }

        return true
    }

    private static clearPid(pidspec, msg) {
        // be paranoid, the product is too picky about this stuff:
        msg.set(pidspec.field, null)
        msg.set(pidspec.field + "-4-1", null) // namespace
        msg.set(pidspec.field + "-4-2", null) // domain
        msg.set(pidspec.field + "-4-3", null) // domain type
    }

    private static ignoreException(closure) {
        try {
            return closure()
        } catch (e) {
            return null
        }
    }

    public static void normalizeCharset(msg, log) {
        def characterEncoding = msg.get("MSH-18")
        if (characterEncoding == null || characterEncoding.isEmpty()){
            log.trace("No character set specified.  Hard-coding to 8859/1")
            msg.set('MSH-18', '8859/1')
        }
    }
}

class QuebecHL7Validation {
    /**
     * This function validates that the format of a passed potential RAMQ actually conforms to the RAMQ creation algorythm.
     * This function is needed in order to protect the PIX lookup mechanism, which is what eventually is used to query
     * the XDS Registry.
     *
     * Please note that the function actually evaluates for INVALID data, meaning that it will return true if the
     * RAMQ passed is invalid.  This is coded in this way for better readability in the calling script.
     *
     * The function follows the RAMQ formula, which is as follows:
     *      the first three letters of the last name;
     *      the first letter of the first name;
     *      the last two digits of the year of birth;
     *      the month of birth (to which 50 is added to indicate female);
     *      the day of birth;
     *      a two digit administrative code used by the Rée.
     */
    public static final RAMQformatIsInvalid(ramq) {
        //                 name        year    month, maybe +50       day            admin code
        return !( ramq ==~ /[A-Za-z]{4}[0-9]{2}([05][1-9]|[16][0-2])([0-2][0-9]|3[01])[0-9]{2}/ )
    }
}
