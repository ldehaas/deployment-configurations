class PriorsLogic {

    // Valid Order Statuses that will cause prefetch to be triggered
    static final ValidOrderStatuses = ["EN ATTENTE", "EN COURS", "EN TRAITEMENT", "RÉALISÉ", "AMENDÉ(S)"]

    static final InvalidFetchOrderStatuses = ["CA"]

    static final ValidModalities  = [
        "AR", "AU", "BDUS", "BI", "BMD", "CR", "CT", "DOC", "DX", "ECG", "EPS", "ES", "FID", "GM",
        "HC", "HD", "IO", "IVUS", "KER", "KO", "LEN", "LS", "MG", "MR", "NM", "OAM", "OCT", "OP", "OPM", "OPR",
        "OPT", "OPV", "OT", "PS", "PT", "PX", "RF", "RG", "RTIMAGE", "REG", "RESP", "RTDOSE", "RTPLAN",
        "RTRECORD", "RTSTRUCT", "SEG", "SM", "SMR", "SR", "SRF", "TG", "US", "VA", "XA", "XC", "NONE"
    ]

    static final ValidAnatomicRegions = [
        "AB", "AN", "AU", "CA", "CO", "DI", "EI", "EN", "ES", "GO", "HE", "HP", "SE", "SQ", "TC", "TH", "TP"
    ]

    static final ValidModalitiesMap = [ 
        "AR" :       "Autorefraction",
        "AU" :       "Audio", 
        "BDUS" :     "Bone Densitometry (ultrasound)",
        "BI" :       "Biomagnetic imaging", 
        "BMD" :      "Bone Densitometry (X-Ray)" ,
        "CR" :       "Computed Radiography", 
        "CT" :       "Computed Tomography", 
        "DG" :       "Diaphanography", 
        "DOC" :      "Document", 
        "DX" :       "Digital Radiography", 
        "ECG" :      "Electrocardiography", 
        "EPS" :      "Cardiac Electrophysiology", 
        "ES" :       "Endoscopy", 
        "FID" :      "Fiducials", 
        "GM" :       "General Microscopy", 
        "HC" :       "Hard Copy", 
        "HD" :       "Hemodynamic Waveform", 
        "IO" :       "Intra-oral Radiography", 
        "IVUS" :     "Intravascular Ultrasound" ,
        "KER" :      "Keratometry", 
        "KO" :       "Key Object Selection", 
        "LEN" :      "Lensometry", 
        "LS" :       "Laser surface scan", 
        "MG" :       "Mammography", 
        "MR" :       "Magnetic Resonance", 
        "NM" :       "Nuclear Medicine", 
        "OAM" :      "Ophthalmic Axial Measurements", 
        "OCT" :      "Optical Coherence Tomography", 
        "OP" :       "Ophthalmic Photography", 
        "OPM" :      "Ophthalmic Mapping", 
        "OPR" :      "Ophthalmic Refraction", 
        "OPT" :      "Ophthalmic Tomography", 
        "OPV" :      "Ophthalmic Visual Field", 
        "OT" :       "Other", 
        "PS" :       "Presentation State", 
        "PT" :       "Positron emission tomography", 
        "PX" :       "Panoramic X-Ray", 
        "RF" :       "Radiofluoroscopy", 
        "RG" :       "Radiographic imaging", 
        "RTIMAGE" :  "Radiotherapy Image", 
        "REG" :      "Registration", 
        "RESP" :     "Respiratory Waveform", 
        "RTDOSE" :   "Radiotherapy Dose", 
        "RTPLAN" :   "Radiotherapy Plan", 
        "RTRECORD" : "RT Treatment Record", 
        "RTSTRUCT" : "Radiotherapy Structure Set", 
        "SEG" :      "Segmentation", 
        "SM" :       "Slide Microscopy", 
        "SMR" :      "Stereometric Relationship", 
        "SR" :       "SR Document", 
        "SRF" :      "Subjective Refraction", 
        "TG" :       "Thermography", 
        "US" :       "Ultrasound", 
        "VA" :       "Visual Acuity", 
        "XA" :       "X-Ray Angiography", 
        "XC" :       "External-camera Photography", 
        "NONE" :     "None"
    ]
    
    static final ValidAnatomicRegionsMap = [
        "AB" : "Abdomen",           
        "AN" : "Angiographie",      
        "AU" : "Autre",
        "CA" : "Cardio-vasculaire",         
        "CO" : "Colonnes",          
        "DI" : "Digestif",          
        "EI" : "Extrémités inférieures",    
        "EN" : "Endocrinien",       
        "ES" : "Extrémités supérieures",    
        "GO" : "Gynéco-obstétrique",    
        "HE" : "Hémodynamie",       
        "HP" : "Hématopoïétique",       
        "SE" : "Sein",          
        "SQ" : "Squelettique",      
        "TC" : "Tête et cou",       
        "TH" : "Thorax",            
        "TP" : "Tomographie par émission de positrons" 
    ]
}

