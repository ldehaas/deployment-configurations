/*
 * Filter traffic not meant for this context.
 */

LOAD("../Shared_Scripting_Files/contexts.groovy")
LOAD("common.groovy")

return HL7Proxy.messageMatchesContext(input, "TEKI", log)

