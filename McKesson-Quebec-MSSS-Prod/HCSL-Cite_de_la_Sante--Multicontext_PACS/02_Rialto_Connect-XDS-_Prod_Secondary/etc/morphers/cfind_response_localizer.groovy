/*
 * Runs in the adhoc workflow.
 *
 * Runs once per study that was found by a PACS's CFind query and either
 * drops the response or localizes it for the PACS.
 *
 * Has access to the XDS metadata for the study in the xdsMetadata variable.
 */

LOAD("Shared_Scripting_Files/shared_constants.groovy")
LOAD("Shared_Scripting_Files/common_localization_routines.groovy")
LOAD("Shared_Scripting_Files/shared_xds_modality_and_anatomic_region_routines.groovy")
LOAD("Shared_Scripting_Files/contexts.groovy")

/** Determines whether they want only 1 modality per study */
final giveBackOnlyOneModalityPerStudy = true

log.debug("\n\n**Starting C-Find Response Localizer Script**\n")


context = Contexts.getContext(config.context)
if (context == null) {
    log.warn("Context is unknown from id: {}. Unable to localize this response, passing it through unchanged. " +
        "Please correct either the connect ContextID configuration or contexts.groovy", config.context)
    return true
}

/* Prior to running this script, Connect has copied the SourcePatientID from the xds registry
 * into the input, overwriting what was there in the DIR, if it was different. */
def originalPID = get(PatientID)
log.debug("Source pid in XDS Metadata (at response from DIR) before morphing to PACS: {}", originalPID)

def originalAssigningAuthority = get(IssuerOfPatientID)
log.debug("Source Issuer of PID in XDS Metadata (at response from DIR) before morphing to PACS: {}", originalAssigningAuthority)


if (context.isLocal(originalAssigningAuthority)) {
    log.debug("Dropping this study from the Ad-Hoc C-Find Response, as it is considered LOCAL to the PACS")
    return false
}

/* 1. Localizing the PID for responding to the calling PACS */

log.debug("This is the current value of the Other Patient IDs Sequence: {}", getAllPidsQualified())

def localOrPrefixedPID = Pids.localize([originalPID, originalAssigningAuthority], getAllPidsQualified(), context.PACSIssuer)

log.debug("Setting local or pre-fixed pid in C-FIND response to PACS: {}", localOrPrefixedPID[0])
set(PatientID, localOrPrefixedPID[0])

// Normally, the PACS doesn't need the IssuerOfPatientID populated, since all Patients are of the same domain, and it's implied
remove(IssuerOfPatientID)


/* 2. Prefixing the Accession Numbers so they can *uniquely* and clearly be recognized as foreign studies in the PACS using Connect */
if (originalAssigningAuthority == null || originalAssigningAuthority.isEmpty()){
    log.warn("Empty Assigning Authority (IssuerOfPatientID) at DIR. The Accession Number will be prefixed as Foreign, and unexpected")
}

def prefix = Prefixes.getPrefix(originalAssigningAuthority, context.PACSIssuer)
// TODO: constantize AU
if (prefix == "AU") {
    log.warn("Unknown Site OID Mapping for '{}'. The prefixing will not be what is expected!", originalAssigningAuthority)
} else {
    log.debug("Current prefix to prepend to Accession Number is: '{}'", prefix)
}

prepend(AccessionNumber, prefix)
if (get(AccessionNumber).size() >= 16){
    log.warn("Prefixed Accession Number '{}' is greater than 16 characters. This might cause unforseen issues at PACS.", get(AccessionNumber))
}


/* 3. Prefixing the Study IDs so they can *uniquely* and clearly be recognized as foreign studies in the PACS using Connect */
// TODO: why null check here if we don't do it for AccessionNumber?
if (prefix != null) {
    prepend(StudyID, prefix)
}


/* 4. Hashing relevant Study, Series and SOP Instance UIDs if Connect serves a multi-context PACS */
UIDs.localizeUIDs(output, context, hash, log)


/* 5. Setting the Retreive AETitle to Connect itself, so that the PACS goes through Connect for study localization */
log.trace("Retreive AETitle at DIR is : '{}'", get(RetrieveAETitle))

set(RetrieveAETitle, context.RialtoRetrieveAE)
log.trace("Current Retreive AETitle after morphing is '{}'", get(RetrieveAETitle))


/* 6. Handle the Modalities in Study Correctly for Foreign and/or Local Studies */

/* Get the list of unique Modalities returned by the DIR */
def dirModalities = getList(ModalitiesInStudy) as Set
log.debug("Modalities In Study, as passed from the DIR: {}", dirModalities)

/* Get the unique set of 'Modalities' as held by the XDS Registry */
def xdsModalities = Modalities.getCodeValues(xdsMetadata.get(XDSEventCodes), "DCM") as Set
log.debug("Event Codes (Modalities In Study) as passed raw from the XDS Registry: {}", xdsModalities)

// Merging the unique Modalities contained in both the XDS Registry and the DIR
def finalModalities = [] as Set
if (dirModalities != null) {
    finalModalities.addAll(dirModalities)
}
if (xdsModalities != null) {
    finalModalities.addAll(xdsModalities)
}
log.debug("For Foreign Study, merged XDS Event Codes and DIR Modalities in Study: {}", finalModalities)

// Substitute known Secondary Modality types with Primary types
finalModalities = finalModalities.collect { modality ->
    Modalities.secondaryToPrimaryModalitiesMap[modality]
} as Set
log.debug("Modalities after substitution of Preferred Modalities: {}", finalModalities)

// find dominant if they only want one modality per study
if (giveBackOnlyOneModalityPerStudy && finalModalities.size() > 1) {
    log.debug("There is more than 1 modality currently for this Study, disambiguating.")
    def dominantModality = Modalities.determineDominantModalityFromList(finalModalities)

    log.debug("The dominant modality for this study is '{}'", dominantModality)

    if (dominantModality == "") {
        log.warn("The dominant modality rule produced no results for {}. Dropping this C-FIND Response.", finalModalities)
        return false
    }

    finalModalities = [dominantModality]
}

log.debug("Cleaning up Modalities in Study to only those modalities that the PACS wants.")
finalModalities.removeAll(SharedConstants.Unwanted_Modalities_in_AdHoc_CFIND_Responses)
log.debug("Passing the cleaned up set of modalities to PACS: {}", finalModalities)

if (finalModalities.isEmpty()) {
    log.info("Ad hoc query response to PACS contained only unwanted modalities. Dropping study '{}', accession '{}'.",
             get(StudyInstanceUID), get(AccessionNumber))
    return false
}

set(ModalitiesInStudy, finalModalities)


log.trace("CFind response after morphing:\n{}", output)

log.debug("\n\n**Ending C-Find Response Localizer Script**\n")

