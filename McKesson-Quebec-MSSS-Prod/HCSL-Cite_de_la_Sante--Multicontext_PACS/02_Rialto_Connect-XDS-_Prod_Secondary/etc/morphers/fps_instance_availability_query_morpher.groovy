/*
 * Runs for Fetch Prior Studies
 * Modifies the Instance Availability C-FIND used to query the PACS to see if specific studies are already
 * in the foreign studies cache.
 */

LOAD("Shared_Scripting_Files/common_localization_routines.groovy")
LOAD("Shared_Scripting_Files/contexts.groovy")

context = Contexts.getContext(config.context)
if (context == null) {
    log.warn("Context is unknown from id: {}. Unable to localize this query, passing it through unchanged. " +
        "Please correct either the connect ContextID configuration or contexts.groovy", config.context)
    return true
}

UIDs.localizeUIDs(output, context, hash, log)

log.debug("This is the outgoing Instance Availability Query C-FIND going to the PACS: {}", output)

