class Constants {
    static ConectServesMultiContextPACS = true

    static LocalIssuerIDs = [
    "Issuer1", "Issuer2"
    ]
}


class DICOMutilsTemp { 
 
    // Loads all the OIDS representing the contexts served by the local PACS 
    private static final ContextsServedByLocalPACS = Constants.LocalIssuerIDs.values() 
 
    private static GenericHasher(uid, issuerOfPID) { 
        if(Constants.ConectServesMultiContextPACS){ 
 
            if(ContextsServedByLocalPACS.contains(issuerOfPID) ){ 
            // Hash DICOM UIDs if the foreign studies originate in contexts that are also handled by the local PACS  
                return hasher.hash(issuerOfPID, uid) 
            } 
 
        } else { 
            // For "true foreign" studies (i.e. in contexts outside local PACS), and local studies, keep the original UID 
            return uid 
        } 
    } 
 
    private static GenericUnhasher(hashedUID, issuerOfPID, unhash){ 
        if(Constants.ConectServesMultiContextPACS){ 
 
            if(ContextsServedByLocalPACS.contains(issuerOfPID) ){ 
            // Unhash DICOM UIDs if the foreign studies originate in contexts that are also handled by the local PACS  
                return unhash(hashedUID) 
            } 
 
        } else { 
            // For "true foreign" studies (i.e. in contexts outside local PACS), and local studies, keep the original UID 
            return hashedUID  
        } 
     
    } 
     
    /* The following methods exist for readability in the calling scripts */ 
    static transformStudyInstUIDforPACS(studyInstanceUID, issuerOfPID) { 
        return GenericHasher(studyInstanceUID, issuerOfPID) 
    } 
 
    static transformSeriesInstUIDforPACS(seriesInstanceUID, issuerOfPID) { 
        return GenericHasher(seriesInstanceUID, issuerOfPID) 
    } 
 
    static transformSOPinstUIDforPACS(sopInstanceUID, issuerOfPID) { 
        return GenericHasher(sopInstanceUID, issuerOfPID) 
    } 
 /*
 
    static transformStudyInstUIDforDIR(studyInstanceUID, issuerOfPID, unhash) { 
        return GenericUnhasher(studyInstanceUID, issuerOfPID, unhash) 
    } 
 
    static transformSeriesInstUIDforDIR(seriesInstanceUID, issuerOfPID, unhash) { 
        return GenericUnhasher(seriesInstanceUID, issuerOfPID, unhash) 
    } 
 
    static transformSOPinstUIDforDIR(sopInstanceUID, issuerOfPID, unhash){ 
        return GenericUnhasher(sopInstanceUID, issuerOfPID, unhash) 
    }
*/
}

class hasher {

    static hash(uid, prefix) {
        return prefix + uid
    }
}


def studyInstanceUID = "111SIUID111"
def originalAssigningAuthority = "Issuer2"

println "(CHANGE TO TRACE) -- Study Instance UID as received from the DI-r is " + studyInstanceUID

studyInstanceUID = DICOMutilsTemp.transformStudyInstUIDforPACS(studyInstanceUID, originalAssigningAuthority)

println "(CHANGE TO TRACE) -- Setting current hashed Study Instance UID as " + studyInstanceUID
