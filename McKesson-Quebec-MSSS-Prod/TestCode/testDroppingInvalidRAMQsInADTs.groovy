
def EvaluateEmptyQuotes( value ) {
    if( value == '""' ) {
        println "Value " + value + " is Empty Quotes"
    } else {
        println "Value " + value + " is NOT Empty Quotes"
    }
}

def EvaluateAllLetters(value){
    if( value ==~ /[A-Z]+/ ) {
        println "Value " + value + " is ALL uppercase letters"

    } else if (value ==~ /[a-z]+/ ) {
        println "Value " + value + " is ALL lowercase letters"

    } else if (value ==~ /[a-zA-Z]+/ ) {
        println "Value " + value + " is ALL letters"
    } else {
        println "Value " + value + " contains values other than all letters"
    }
}

def EvaluateValidRAMQFormat_Simple( ramq ) {

    if(ramq ==~ /[A-Z]{4}[0-9]{8}/) {
        println "Value " + ramq + " has a valid RAMQ format!"
    } else {
        println "Value " + ramq + " has an INVALID  RAMQ format!"
    }
}

def EvaluateValidRAMQFormat_Simple_True_False( ramq ) {
    if(ramq ==~ /[A-Z]{4}[0-9]{8}/) {
        return true
    } else {
        return false
    }
}

// class QuebecFormatting
def EvaluateValidRAMQFormat_Full(Potential_RAMQ){
    if(Potential_RAMQ ==~ /[A-Za-z]{4}[0-9]{2}([05][1-9]|[16][0-2])([0-2][0-9]|3[01])[0-9]{2}/ ) {
        println "Value " + Potential_RAMQ + " has a **full** valid RAMQ format!"
    } else {
        println "Value " + Potential_RAMQ + " has an INVALID **full** RAMQ format!"
    }
}

/***************************  Test section ********************/

/*
def testRamqs = [
    '""',
    'CARP55052919',
    'CALB01234567',
    '214007129408',
    'GONR77081599',
    'HUIJ43213490'
]
*/

def testRamqs = [
    'SCHP61550622'
]



for(ramq1 in testRamqs){

    /*
    println "\nAsserting whether \"" + ramq1 + "\" has a valid RAMQ format using the simple method:"
    EvaluateValidRAMQFormat_Simple(ramq1)
    */

    println "\nAsserting whether \"" + ramq1 + "\" has a valid **full** RAMQ format using the FULL method:"
    EvaluateValidRAMQFormat_Full(ramq1)

    println "------------"
}

println ""

/*
println "Passing \"\" to EvaluateEmptyQuotes"
EvaluateEmptyQuotes('""')

println "Passing \"Elephant\" to EvaluateEmptyQuotes"
EvaluateEmptyQuotes('Elephant')
*/


