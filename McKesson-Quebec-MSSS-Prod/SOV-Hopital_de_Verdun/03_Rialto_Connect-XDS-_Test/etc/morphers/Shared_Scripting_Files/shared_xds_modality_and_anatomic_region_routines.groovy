/* This file contains worker functions for working with the XDS Event Codes, which contain both the Modalities and Anatomic Region
        for a given study --as published to the XDS Registry --  */

class Modalities {

    /**
     * This list determines what the Hierarchical Modalities are to determine the Dominant Modalities
     * of multi-modality studies (used for Procedure Code Mapping)
     * This list might be different from site to site.  The worker functions should all work correctly regardless of the content
     * of the list.
     */
    private static hierarchicalModalityList = [ "MG", "XA", "PT", "MR", "NM", "CT", "US", "RF", "CR", "DX", "BMD"]

    public static secondaryToPrimaryModalitiesMap = [
        "DX": "CR",
        "MN": "NM",
        "TP": "PT",
    ].withDefault{it}

    /**
     * For the given coding scheme, we will automatically apply replacement event code values.
     */
    private static schemeToReplacementMap = [
        // for all DCM event codes, use secondaryToPrimaryModalitiesMap to fix modalities
        "DCM": secondaryToPrimaryModalitiesMap
    ].withDefault{
        [:].withDefault{it}
    }

    public static determineDominantModality(eventCodes, modalityCodingScheme) {
        def xdsModalities = getCodeValues(eventCodes, modalityCodingScheme)

        for (modality in hierarchicalModalityList) {
            if (xdsModalities.contains(modality)) {
                return modality
            }
        }

        return ""
    }

    /**
     * Returns (as a string) the Anatomic Region in the Raw XDS Event Codes
     * If there is more than 1 Anatomic Region (there shouldn't be), it returns the first one it finds
     */
    public static determineAnatomicRegion(eventCodes, codingSchemeForAnatomicRegion){

        def regions = getCodeValues(eventCodes, codingSchemeForAnatomicRegion)

        return regions.isEmpty() ? "" : regions[0]
    }

    /**
     * Filter the list of event codes to return only those with the specified
     * coding scheme.
     */
    public static getCodeValues(eventCodes, codingScheme) {

        // filter for scheme:
        eventCodes = eventCodes.findAll { eventCode -> codingScheme.equalsIgnoreCase(eventCode.getSchemeName()) }

        // collect just the code values:
        eventCodes = eventCodes.collect { eventCode -> eventCode.getCodeValue() }
        
        // Substitute invalid values:
        eventCodes = eventCodes.collect { eventCode -> schemeToReplacementMap[codingScheme][eventCode] }

        return eventCodes
    }

    /**
     * Like "determineDominantModality", but uses a list of pre-parsed modalities instead of XDS Event Codes.
     * Expects a list, if you pass a string, the results will be incorrect.
     * Will return a modality as a string.  If no dominant modality is found, it will return an empty string.
     * (if you get an empty string back, use the first modality in your script code)
     * If an empty lists was passed, it will return an empty string.
     */
    public static determineDominantModalityFromList(modalityList) {

        for (modality in hierarchicalModalityList) {
            if (modalityList.contains(modality)) {
                return modality
            }
        }

        return ""
    }
}

