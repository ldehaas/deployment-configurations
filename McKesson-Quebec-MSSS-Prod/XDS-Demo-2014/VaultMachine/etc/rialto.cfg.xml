<?xml version="1.0"?>
<config>

    <!-- ################################################################## -->
    <!--                                                                    -->
    <!--  Rialto Configuration                                              -->
    <!--                                                                    -->
    <!-- ################################################################## -->

    <!-- ################################################################## -->
    <!--  VARIABLES                                                         -->
    <!-- ################################################################## -->

    <var name="HostIP" value="${env.IP_ADDR_ETH0}" />

    <!-- Configure to point to Connect in the demo.-->
    <var name="Connect-Host" value="connect" />
    <var name="Connect-DICOM-Port" value="4104" />
    <var name="Connect-AETitle" value="RIALTOCONNECT" />

    <!-- Configure to point to local PACS in the demo.-->
    <var name="PACS-Host" value="pacs" />
    <var name="PACS-DICOM-Port" value="4104" />
    <var name="PACS-AETitle" value="RIALTO_PACS" />

    <!-- Configure to point to foreign DIR in the demo.-->
    <var name="DIR-Host" value="dir" />
    <var name="DIR-DICOM-Port" value="4104" />
    <var name="DIR-AETitle" value="RIALTO" />

    <!-- Note: Cassandra runs on 127.0.0.1 on a demo system.  For production, change this value: -->
    <var name="CassandraHost1" value="127.0.0.1" />
    <var name="CassandraClusterDatacenterName" value="DC1" />
    <var name="CassandraKeyspacePrefix" value="" />
    <var name="CassandraReplicationOption" value="DC1=1" />

    <!-- AUDIT RECORD REPOSITORY VARIABLES -->
    <var name="ARR-Protocol" value="udp" />
    <var name="ARR-GUIPort" value="7070" />
    <var name="ARR-Port" value="4000" />
    <var name="ARR-Host" value="localhost" />

    <!-- SYSTEM COMMON VARIABLES -->
    <var name="System-AffinityDomain" value="EMPI_CROSS_AFFINITY_DOMAIN_ID" />
    <var name="System-AffinityNamespace" value="RIALTO" />
    <var name="System-DefaultLocalDomain" value="county.general" />
    <var name="System-DefaultNamespace" value="CG" />
    <var name="System-UniversalIDType" value="ISO" />
    <var name="System-DefaultLocalFullyQualifiedDomain" value="${System-DefaultNamespace}&amp;${System-DefaultLocalDomain}&amp;${System-UniversalIDType}" />
    <var name="System-AffinityFullyQualifiedDomain" value="${System-AffinityNamespace}&amp;${System-AffinityDomain}&amp;${System-UniversalIDType}" />

    <!-- XDS REGISTRY VARIABLES -->
    <var name="XDSRegistry-Host" value="localhost"/>
    <var name="XDSRegistry-Port" value="8081"/>
    <var name="XDSRegistry-Path" value="/registry"/>
    <var name="XDSRegistry-URL" value="http://${XDSRegistry-Host}:${XDSRegistry-Port}${XDSRegistry-Path}"/>
    <var name="XDSRegistry-HL7v2Port" value="2397"/>
    <var name="XDSRegistry-MetadataUpdateEnabled" value="true"/>

    <!-- XDS REPOSITORY VARIABLES -->
    <var name="XDSRepository-Host" value="localhost" />
    <var name="XDSRepository-Port" value="8082" />
    <var name="XDSRepository-Path" value="/vault/repo" />
    <var name="XDSRepository-ID" value="2.16.840.1.113883.3.239.16.30001"/>
    <var name="XDSRepository-URL" value="http://${XDSRepository-Host}:${XDSRepository-Port}${XDSRepository-Path}"/>

    <!-- VAULT - IMAGE ARCHIVE VARIABLES -->
    <var name="IA-DicomPort" value="4104" />
    <var name="IA-AETitle" value="${Connect-AETitle}" />
    <!-- Port which image archive listens on for changing study availability messages -->
    <var name="ChangeAvailability-HL7v2Port" value="2456" />
    <var name="UpdateStudies-HL7v2Port" value="2455" />

    <!-- EMPI VARIABLES -->
    <var name="EMPI-Host" value="localhost" />
    <var name="EMPI-HL7v2Port" value="2399" />
    <var name="EMPI-SendingApplication" value="RIALTO_EMPI"/>
    <var name="EMPI-SendingFacility" value="KAROS_HEALTH"/>

    <!-- HL7 Proxy Variables -->
    <var name="HL7Proxy-Port" value="2398" />

    <!-- HCP -->
    <var name="HCPURL" value="http://10.0.16.220/rest/" />
    <var name="HCPHost" value="rialto.rialto.hcp-demo.hcpdomain.com" />
    <var name="HCPUsername" value="rialto" />
    <var name="HCPPassword" value="r1@lto" />

    <!-- DATABASE URLs: "DatabaseURL" parameter name is used by the navigator-security script, do not change the parameter name  -->
    <var name="DatabaseURL" value="jdbc:postgresql://localhost/navigatordb?user=rialto"/>

    <!-- NAVIGATOR VARIABLES -->
    <var name="Navigator-Host" value="${HostIP}"/>
    <var name="Navigator-HostProtocol" value="https" />
    <var name="Navigator-GUIPort" value="2525" />
    <var name="Navigator-APIPort" value="2524" />
    <var name="Navigator-RootPath" value="/rest" />
    <var name="Navigator-APIBasePath" value="/rialto" />
    <var name="Navigator-GUIBasePath" value="/" />
    <var name="Navigator-DefaultLocale" value="en" />
    <var name="Navigator-DefaultTheme" value="rialto-light" />
    <var name="Navigator-MaxPatientSearchResults" value="1000" />
    <var name="Navigator-MaxDocumentSearchResults" value="1000" />

    <!-- USERMANAGEMENT VARIABLES -->
    <var name="Usermanagement-APIBasePath" value="/usermanagement" />
    <var name="Usermanagement-URL" value="${Navigator-HostProtocol}://${Navigator-Host}:${Navigator-APIPort}${Navigator-RootPath}" />
    <var name="Usermanagement-AuthenticatedURL" value="${Navigator-HostProtocol}://${Navigator-Host}:${Navigator-APIPort}${Navigator-RootPath}/api${Usermanagement-APIBasePath}" />

    <!-- CLUSTER CONFIGURATION -->
    <!-- Single Sign On in a Cluster -->
    <var name="SSO-Cluster-Bind-IP" value="127.0.0.1"/>
    <var name="SSO-Cluster-Multicast-IP" value="232.10.10.201"/>
    <var name="SSO-Cluster-Multicast-PORT" value="45201"/>

    <!-- Navigator Cache Manager in a cluster -->
    <var name="Navigator-Cluster-Bind-IP" value="127.0.0.1"/>
    <var name="Navigator-Cluster-Multicast-IP" value="232.10.10.202"/>
    <var name="Navigator-Cluster-Multicast-PORT" value="45202"/>

    <!-- Navigator Server access controller in a cluster -->
    <var name="Navigator-Server-Cluster-Bind-IP" value="127.0.0.1"/>
    <var name="Navigator-Server-Cluster-Multicast-IP" value="232.10.10.203"/>
    <var name="Navigator-Server-Cluster-Multicast-PORT" value="45203"/>

    <!-- TLS CONFIGURATION -->
    <var name="TLSKeyStore" value="sample.jks" />
    <var name="TLSKeyStorePass" value="password" />
    <var name="TLSKeyPass" value="password" />
    <var name="TLSTrustStore" value="sample.jks" />
    <var name="TLSTrustStorePass" value="password" />
    <var name="TLSClientAuth" value="false" />
    <var name="TLSV2Hello" value="true" />

    <!-- User Sessions -->
    <var name="IdleUserSessionTimeout" value="30m" />

    <!-- Database Backup -->
    <var name="BackupRemotePath" value="/home/rialto/rialto/var/database-backups" />
    <var name="BackupMaxSnapshots" value="5" />

    <!-- ################################################################## -->
    <!--  SERVERS                                                           -->
    <!-- ################################################################## -->
    <server type="http" id="authenticated-http">
        <port>${Navigator-APIPort}</port>
        <tls>
            <keystore>${TLSKeyStore}</keystore>
            <keystorepass>${TLSKeyStorePass}</keystorepass>
            <keypass>${TLSKeyPass}</keypass>
            <truststore>${TLSTrustStore}</truststore>
            <truststorepass>${TLSTrustStorePass}</truststorepass>
            <clientauth>${TLSClientAuth}</clientauth>
            <v2hello>${TLSV2Hello}</v2hello>
        </tls>
    </server>

    <server type="http" id="navigator-http">
        <port>${Navigator-GUIPort}</port>
        <tls>
            <keystore>${TLSKeyStore}</keystore>
            <keystorepass>${TLSKeyStorePass}</keystorepass>
            <keypass>${TLSKeyPass}</keypass>
            <truststore>${TLSTrustStore}</truststore>
            <truststorepass>${TLSTrustStorePass}</truststorepass>
            <clientauth>${TLSClientAuth}</clientauth>
            <v2hello>${TLSV2Hello}</v2hello>
        </tls>
    </server>

    <server type="http" id="audit-http">
        <port>${ARR-GUIPort}</port>
        <tls>
            <keystore>${TLSKeyStore}</keystore>
            <keystorepass>${TLSKeyStorePass}</keystorepass>
            <keypass>${TLSKeyPass}</keypass>
            <truststore>${TLSTrustStore}</truststore>
            <truststorepass>${TLSTrustStorePass}</truststorepass>
            <clientauth>${TLSClientAuth}</clientauth>
            <v2hello>${TLSV2Hello}</v2hello>
        </tls>
    </server>

    <server type="http" id="registry-http">
        <port>${XDSRegistry-Port}</port>
    </server>

    <server type="http" id="repo-http">
        <port>${XDSRepository-Port}</port>
    </server>

    <!-- Web server - Used for WADO, RIDS, and MINT -->
    <server type="http" id="http8080">
       <port>8080</port>
    </server>

    <!-- ################################################################## -->
    <!--  SERVICES                                                          -->
    <!-- ################################################################## -->

    <!-- Cassandra Startup Service should be the first service to be started. -->
    <service id="cassandra-startup-service" type="cassandra-startup-service">
        <config>
            <prop name="CassandraConfiguration">
                <clusterHosts>${CassandraHost1}</clusterHosts>
                <clusterDatacenterName>${CassandraClusterDatacenterName}</clusterDatacenterName>
                <keyspaceName>${CassandraKeyspacePrefix}startuptest</keyspaceName>
                <keyspaceReplicationStrategy>NetworkTopologyStrategy</keyspaceReplicationStrategy>
                <keyspaceReplicationStrategyOptions>${CassandraReplicationOption}</keyspaceReplicationStrategyOptions>
                <consistencyLevel>LOCAL_QUORUM</consistencyLevel>
                <serialConsistencyLevel>LOCAL_SERIAL</serialConsistencyLevel>
                <keyspaceCreateEnabled>true</keyspaceCreateEnabled>
                <dynamicConsistencyLevelEnabled>false</dynamicConsistencyLevelEnabled>
                <schemaUpdateEnabled>false</schemaUpdateEnabled>
            </prop>
        </config>
    </service>

    <!-- EVENT COLLECTOR SERVICE -->
    <service id="TransactionRecordsServiceForEventCollector" type="txn-records">
        <config>
            <prop name="StorageDir">${rialto.rootdir}/var/txn_records</prop>
        </config>
    </service>

    <!-- AUDIT RECORD REPOSITORY SERVICE -->
    <service id="arr" type="arr2">
        <tls name="repository">
            <keystore>samplePrivateKeyA.jks</keystore>
            <truststore>samplePublicKeyA.jks</truststore>
            <clientauth>true</clientauth>
            <ciphers>AES</ciphers>
        </tls>

        <server idref="audit-http" name="arr-api">
            <url>/auditRecords/*</url>
        </server>

        <config>
            <prop name="Listener" value="${ARR-Protocol}:${ARR-Port}"/>
            <prop name="CassandraConfiguration">
                <clusterHosts>${CassandraHost1}</clusterHosts>
                <clusterDatacenterName>${CassandraClusterDatacenterName}</clusterDatacenterName>
                <keyspaceName>${CassandraKeyspacePrefix}audit</keyspaceName>
                <keyspaceReplicationStrategy>NetworkTopologyStrategy</keyspaceReplicationStrategy>
                <keyspaceReplicationStrategyOptions>${CassandraReplicationOption}</keyspaceReplicationStrategyOptions>
                <consistencyLevel>LOCAL_QUORUM</consistencyLevel>
                <serialConsistencyLevel>LOCAL_SERIAL</serialConsistencyLevel>
                <keyspaceCreateEnabled>true</keyspaceCreateEnabled>
                <dynamicConsistencyLevelEnabled>false</dynamicConsistencyLevelEnabled>
                <schemaUpdateEnabled>false</schemaUpdateEnabled>
            </prop>
        </config>
    </service>

    <include location="navigator/navigator.xml"/>

    <!-- ################################################################## -->
    <!--  DEVICES                                                           -->
    <!-- ################################################################## -->

    <!-- AUDIT DEVICE -->
    <device type="audit" id="audit">
        <transport>${ARR-Protocol}</transport>
        <tls>
            <keystore>samplePrivateKeyA.jks</keystore>
            <truststore>samplePublicKeyA.jks</truststore>
        </tls>
        <host>${ARR-Host}</host>
        <port>${ARR-Port}</port>
    </device>

    <!-- EMPI PDQ DEVICE -->
    <device id="pdq" type="pdq">
        <protocol>v2</protocol>
        <host>${EMPI-Host}</host>
        <port>${EMPI-HL7v2Port}</port>
        <sendingApplication>RIALTO_CG</sendingApplication>
        <sendingFacility>KAROS_CG</sendingFacility>
        <receivingApplication>RIALTO_EMPI</receivingApplication>
        <receivingFacility>KAROS_HEALTH</receivingFacility>
        <resultsPerQuery>25</resultsPerQuery>
    </device>

    <!-- EMPI PIX DEVICE -->
    <device type="pix" id="pix">
        <protocol>v2</protocol>
        <host>${EMPI-Host}</host>
        <port>${EMPI-HL7v2Port}</port>
        <sendingApplication>RIALTO_CG</sendingApplication>
        <sendingFacility>KAROS_CG</sendingFacility>
        <receivingApplication>RIALTO_EMPI</receivingApplication>
        <receivingFacility>KAROS_HEALTH</receivingFacility>
    </device>

    <!-- XDS Registry HL7 device -->
    <device type="hl7v2" id="hl7xdsreg">
        <protocol>v2</protocol>
        <host>${XDSRegistry-Host}</host>
        <port>${XDSRegistry-HL7v2Port}</port>
        <receivingApplication>RIALTO_XDS_REG</receivingApplication>
        <receivingFacility>KAROS_HEALTH</receivingFacility>
    </device>

    <!-- XDS REPOSITORY DEVICE -->
    <device type="xdsrep" id="xdsrep">
        <uniqueid>${XDSRepository-ID}</uniqueid>
        <url>${XDSRepository-URL}</url>
    </device>

    <!-- XDS REGISTRY DEVICE -->
    <device type="xdsreg" id="xdsreg">
        <url>${XDSRegistry-URL}</url>
        <affinitydomain>${System-AffinityDomain}</affinitydomain>
    </device>
    
     <!-- This configuration tells the admin tools where to query Conenct --> 
     <device type="dicom" id="rialto-connect">
        <host>${Connect-Host}</host>
        <port>${Connect-DICOM-Port}</port>
        <aetitle>${Connect-AETitle}</aetitle>
        <domain>1.2.3</domain>
     </device>

     <device type="dicom" id="rialto-connect-pacs">
        <host>${PACS-Host}</host>
        <port>${PACS-DICOM-Port}</port>
        <aetitle>${PACS-AETitle}</aetitle>
        <domain>1.2.3</domain>
     </device>

     <device type="dicom" id="connect-dir">
        <host>${DIR-Host}</host>
        <port>${DIR-DICOM-Port}</port>
        <aetitle>${DIR-AETitle}</aetitle>
        <domain>1.2.3</domain>
     </device>
</config>
