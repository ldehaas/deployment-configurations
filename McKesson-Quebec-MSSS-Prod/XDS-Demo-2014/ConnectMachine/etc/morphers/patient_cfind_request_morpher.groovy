return false
/* Patient C-Find to PACS Morpher
 *
 * Shared in both the Ad-Hoc Query and Fetch Prior Studies workflow
 *
 * When the first image in a study is received, this script sets up a cfind query
 * that will be sent to the PACS to find local patient demographics for the patient.
 * The results can later be used to localize the entire study.
 *
 * Returning 'false' causes Connect not to issue a C-Find to the local PACS for this Study.
 *
 * This script is optional.  If not listed in rialto.cfg.xml, Connect will not attempt
 * to query the Local PACS for local Patient Demographics.
 */


LOAD("Shared_Scripting_Files/shared_constants.groovy")
LOAD("Shared_Scripting_Files/contexts.groovy")

context = Contexts.getContext(config.context)
if (context == null) {
    log.error("Context is unknown from id: {}. Unable to localize this query, dropping it. " +
        "Please correct either the connect ContextID configuration or contexts.groovy", config.context)
    return false
}

log.debug("** Starting 'Patient C-Find to PACS' Morpher --Creating the C-Find to query for local PACS demographics **")
log.debug("Current Input received by the Script (i.e. base C-Find):\n{}", input)


if (get(IssuerOfPatientID) == context.PACSIssuer) {
    // Patient Name and birth date are needed from the PACS
    set(PatientName, "")
    set(PatientBirthDate, "")

    // Some PACSs with multiple patient contexts do not have any way to detect which
    // context we mean when we query, which can result in results from unrelated domains.
    // By asking for the Issuer, we can later determine which results are actually
    // for the patient we're interested in.
    // Note that if not set as a return key, it should normally be removed altogether,
    // as most PACSs can't handle a value.
    set(IssuerOfPatientID, "")

    log.debug("C-Find that will be used for querying local PACS Demographics:\n{}", output)

} else {
    log.warn("Skipping C-FIND to PACS to get local Patient Demographics because the local pid is not known.")

    return false
}

