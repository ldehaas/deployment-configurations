class SharedConstants {
    /** This is the Issuer of Patient ID for the Affinity Domain (i.e. the RAMQ) */
    static final Affinity_Domain_OID = '2.16.124.10.101.1.60.100'

    /** This is the value in the MSH Segment of an HL7 Message that contains the HL7 Sending Application to know from where the HL7 is coming.
     * 'MSH-3' is the Standard; in Quebec, usually MSH-3-1 has the value of interest **/
    static final HL7_Sending_Application_Field = 'MSH-3-1'

    /** This is the value in the MSH Segment of an HL7 Message that contains the HL7 Sending Facility to know from where the HL7 is coming.
     * 'MSH-4' is the Standard **/
    static final HL7_Sending_Facility_Field = 'MSH-4'

    /** This is the Value in the PID Segment of an HL7 Message that includes the (unqualified) PID value of the Local MRN
     * 'PID-3-1' is the Standard */
    static final HL7_Local_MRN_PID_Field = 'PID-3-1'

    /** This is the value in the PID Segment of an HL7 Message that contains the (unqualified) PID value for the Affinity Domain (i.e. RAMQ)
     * 'PID-2-1' is the Standard */
    static final HL7_Affinity_Domain_PID_Field = 'PID-2-1'

    /** This is the value in the PID Segment of an HL7 Message that contains the Issuer of Patient ID to qualify the Local MRN
     * 'PID-3-4-2' is the Standard */
    static final HL7_Local_MRN_Issuer_of_PID_Field = 'PID-3-4-2'

    /** This is the value in the PID Segment of an HL7 Message that contains the Issuer of Patient ID to qualify the Affinity Domain
     * 'PID-3-4-2' is the Standard */
    static final HL7_Affinity_Domain_Issuer_of_PID_Field = 'PID-2-4-2'

    /** This is the Field in an HL7 Order where we can expect the Modality of an incoming order to be populated */
    static final HL7_Order_Modality_Field = 'OBR-24'

    /** This is the field in an HL7 Order where we can expect the Anatomic Region to be populated */
    static final HL7_Order_Anatomic_Region_Field = 'OBR-15-4'

    /** This is the value to use in the ZDS Segments for the PACS HL7 Listener to Accept Reports and Orders */
    static final HL7_RIS_Brand_Name = 'RadImage'

    /** The Coding Scheme in the XDS Event Code List that specifies an Anatomic Region */
    static final XDS_Coding_Scheme_for_Anatomic_Region = "Imagerie Qu\u00e9bec-DSQ"

    /** The Coding Scheme in the XDS Event Code List that specifies a Modality */
    static final XDS_Coding_Scheme_for_Modality = 'DCM'

    static final Unwanted_Modalities_in_AdHoc_CFIND_Responses = ["KO", "SC", "SR"]
}
