
/**
 * Encapsulates a dicom object to provide easy access to
 * the patient identifiers.
 */
class Pids {
    /**
     * Searches through the supplied pids and finds the one in the local domain.
     * If no local pid is found, a new one is created by prefixing the source
     * patient id with the source issuer.
     * <p>
     * TODO: use an instance of the class instead of this static method
     * 
     * @return a two element array containing the localized pid and issuer
     * @param qualifiedSourcePid a two element array containing the qualified source
     *      pid and source issuer
     * @param qualifiedOtherPids an array of qualified pids
     */
    static localize(qualifiedSourcePid, qualifiedOtherPids, localIssuer) {
        def sourcePid = qualifiedSourcePid[0]
        def sourceIssuer = qualifiedSourcePid[1]
    
        if (localIssuer == sourceIssuer) {
            // this image originated from the local PACS
            return qualifiedSourcePid
        }
    
        for (def pid : qualifiedOtherPids) {
            if (localIssuer == pid[1]) {
                // finds the local PID if available
                return pid
            }
        }
    
        // no linked pid found, create one by prefixing
        // For the demo, we use the site code, since it's shorter and easier to understand.
        // For prod, we should be using the full source issuer, since it's more likely to be unique.
        return [Prefixes.getPrefix(sourceIssuer, localIssuer) + sourcePid, localIssuer]
    }

    private issuerToPid = [:]
    private sourceDomain
    private sop

    public Pids(sop, order=null) {
        this.sop = sop

        sourceDomain = sop.get(IssuerOfPatientID)
        issuerToPid[sourceDomain] = sop.get(PatientID)

        sop.get(OtherPatientIDsSequence).each {
            issuerToPid[it.get(IssuerOfPatientID)] = it.get(PatientID)
        }

        if (order != null) {
            ['PID-2', 'PID-3'].each { pidspec ->
                // assumes order has already been normalized:
                def domain = order.get(pidspec + "-4-2")
                def pid = order.get(pidspec)
                if (domain != null && pid != null) {
                    issuerToPid[domain] = pid
                }
            }
        }
    }

    /**
     * Find a patient identifier in the local domain.
     */
    public getLocalPid(localDomain) {
        def localPid = issuerToPid[localDomain]
        if (localPid != null) {
            return [localPid, issuer]
        }

        // no linked pid found, create one by prefixing
        return [sourceDomain + "_" + issuerToPid[sourceDomain], localIssuer]
    }

    public getPid(domain) {
        issuerToPid[domain]
    }

    /**
     * Return the source domain, or null if it is not known.
     */
    public getSourceDomain() {
        return sourceDomain
    }
}

class Prefixes {

    static getPrefix(assigningAuthority, localIssuer) {
        if (localIssuer == assigningAuthority) {
            // Study belongs to the local PACS.  No prefix needed
            return ""

        } else {
            return sourceIssuerToPrefixAndInstitutionName[assigningAuthority][0]
        }
    }

    static getInstitutionName(assigningAuthority) {
        return sourceIssuerToPrefixAndInstitutionName[assigningAuthority][1]
    }

    private static final sourceIssuerToPrefixAndInstitutionName = [
        "2.16.124.10.101.1.60.1.3000.102.10.1076.0" : ["AB", "Anciens combattants Canada"],
        "2.16.124.10.101.1.60.1.3000.102.10.1048.3" : ["AC", "Centre ambulatoire"],
        "2.16.124.10.101.1.60.1.3000.102.10.1045.1" : ["AD", "Centre Comptois"],
        "2.16.124.10.101.1.60.1.3000.102.10.1060.4" : ["AE", "Centre de santé de Chibougamau"],
        "2.16.124.10.101.1.60.1.3000.102.10.1061.1" : ["BC", "Centre de santé et services sociaux d'Argenteuil"],
        "2.16.124.10.101.1.60.1.3000.101.10.1030.1" : ["BD", "Centre de santé et services sociaux de la Basse-Côte-Nord"],
        "2.16.124.10.101.1.60.1.3000.101.10.1028.1" : ["BE", "Centre de santé et services sociaux de la Minganie"],
        "2.16.124.10.101.1.60.1.3000.101.10.1003.1" : ["BF", "Centre de santé et services sociaux de la Mitis"],
        "2.16.124.10.101.1.60.1.3000.103.10.1005.1" : ["BG", "Centre de santé et services sociaux de la MRC de Cooaticook"],
        "2.16.124.10.101.1.60.1.3000.101.10.1029.1" : ["BH", "Centre de santé et services sociaux de l'Hématite"],
        "2.16.124.10.101.1.60.1.3000.103.10.1003.1" : ["BJ","Centre de santé et services sociaux de Memphrémagog"],
        "2.16.124.10.101.1.60.1.3000.101.10.1027.1" : ["BK", "Centre de santé et services sociaux de Port-Cartier"],
        "2.16.124.10.101.1.60.1.3000.103.10.1006.1" : ["BL", "Centre de santé et services sociaux du Granit"],
        "2.16.124.10.101.1.60.1.3000.102.10.1043.1" : ["BM", "Centre de santé et services sociaux du Haut-St-Maurice"],
        "2.16.124.10.101.1.60.1.3000.102.10.1069.0" : ["BN", "Centre de santé Inuulitsivik"],
        "2.16.124.10.101.1.60.1.3000.102.10.1069.1" : ["BP", "Centre de santé Inuulitsivik"],
        "2.16.124.10.101.1.60.1.3000.102.10.1060.1" : ["BQ", "Centre de santé Isle-Dieu"],
        "2.16.124.10.101.1.60.1.3000.102.10.1060.2" : ["BR", "Centre de santé Lebel"],
        "2.16.124.10.101.1.60.1.3000.102.10.1060.3" : ["BS", "Centre de santé René-Ricard"],
        "2.16.124.10.101.1.60.1.3000.102.10.1070.0" : ["BT", "Centre de santé Tulattavik de l'Ungava"],
        "2.16.124.10.101.1.60.1.3000.102.10.1070.1" : ["BU", "Centre de santé Tulattavik de l'Ungava"],
        "2.16.124.10.101.1.60.1.3000.102.10.1052.1" : ["BV", "Centre de services de Rivière-Rouge"],
        "2.16.124.10.101.1.60.1.1016.1" :             ["BW", "Centre de soins de courte durée La Sarre"],
        "2.16.124.10.101.1.60.1.3000.103.10.1007.1" : ["BX", "Centre d'hébergement de Windsor"],
        "2.16.124.10.101.1.60.1.3000.102.10.1073.2" : ["BY", "Centre d'hébergement du Centre-ville-de-Montréal"],
        "2.16.124.10.101.1.60.1.3000.102.10.1044.2" : ["BZ", "Centre d'hébergement et hôpital du Christ-Roi"],
        "2.16.124.10.101.1.60.1.3000.102.10.1073.1" : ["CA", "Centre d'hébergement Jacques-Viger"],
        "2.16.124.10.101.1.60.1.3000.102.10.1041.2" : ["CB", "Centre d'hébergement Laflèche"],
        "2.16.124.10.101.1.60.1.1036.1" :             ["CC", "Centre d'hébergement Notre-Dame-de-la-Merci"],
        "2.16.124.10.101.1.60.1.3000.101.10.1040.1" : ["CD", "Centre hospitalier affilié universitaire Hôtel-Dieu de Lévis"],
        "2.16.124.10.101.1.60.1.3000.102.10.1065.1" : ["CE", "Centre hospitalier Anna-Laberge"],
        "2.16.124.10.101.1.60.1.1000.1" :             ["CF", "Centre hospitalier de St. Mary"],
        "2.16.124.10.101.1.60.1.1025.1" :             ["CG", "Centre hospitalier gériatrique Maimonides"],
        "2.16.124.10.101.1.60.1.3000.102.10.1050.1" : ["CH", "Centre hospitalier régional de Lanaudière"],
        "2.16.124.10.101.1.60.1.3000.101.10.1001.1" : ["CJ", "Centre hospitalier régional du Grand-Portage"],
        "2.16.124.10.101.1.60.1.3000.101.10.1006.1" : ["CK", "Centre hospitalier Trois-Pistoles"],
        "2.16.124.10.101.1.60.1.3000.102.10.1060.0" : ["CL", "Centre régional de santé et de services sociaux de la Baie-James"],
        "2.16.124.10.101.1.60.1.1029.1" :             ["CM", "CHSLD juif de Montréal"],
        "2.16.124.10.101.1.60.1.3000.101.10.1016.0" : ["CN", "CHU de Québec - Centre hospitalier affilié universitaire de Québec"],
        "2.16.124.10.101.1.60.1.3000.101.10.1015.0" : ["CP", "CHU de Québec - Centre hospitalier universitaire de Québec"],
        "2.16.124.10.101.1.60.1.3000.103.10.1001.0" : ["CQ", "CHU de Sherbrooke"],
        "2.16.124.10.101.1.60.1.1004.1" :             ["CR", "CHU Ste-Justine"],
        "2.16.124.10.101.1.60.1.3000.102.10.1055.0" : ["CS", "CHUM"],
        "2.16.124.10.101.1.60.1.3000.102.10.1044.1" : ["CT", "CLSC de Fortierville"],
        "2.16.124.10.101.1.60.1.1027.1" :             ["CU", "CLSC de Pointe-aux-Trembles-Montréal-est"],
        "2.16.124.10.101.1.60.1.1012.1" :             ["CV", "CLSC de Senneterre"],
        "2.16.124.10.101.1.60.1.3000.101.10.1022.1" : ["CW", "CLSC de St-Raymond"],
        "2.16.124.10.101.1.60.1.1019.1" :             ["CX", "CLSC et Centre d'hébergement de la Petite-Nation"],
        "2.16.124.10.101.1.60.1.1021.1" :             ["CY", "CLSC Fort-Coulonge"],
        "2.16.124.10.101.1.60.1.3000.101.10.1041.0" : ["CZ", "CLSC Naskapi"],
        "2.16.124.10.101.1.60.1.3000.101.10.1041.1" : ["DA", "CLSC Naskapi"],
        "2.16.124.10.101.1.60.1.3000.101.10.1005.2" : ["DB", "CLSC Pohénégamook"],
        "2.16.124.10.101.1.60.1.3000.102.10.1050.3" : ["DC", "CLSC St-Donat"],
        "2.16.124.10.101.1.60.1.3000.102.10.1042.1" : ["DD", "CLSC Ste-Geneviève-de-la-Batiscan"],
        "2.16.124.10.101.1.60.1.3000.102.10.1048.2" : ["DE", "CLSC Ste-Rose/Centre d'hébergement Rose-de-Lima"],
        "2.16.124.10.101.1.60.1.3000.102.10.1042.2" : ["DF", "CLSC Ste-Tite"],
        "2.16.124.10.101.1.60.1.1037.1" :             ["DG", "CLSC St-Michel"],
        "2.16.124.10.101.1.60.1.3000.102.10.1050.2" : ["DH", "CLSC St-Michel-des-Saints"],
        "2.16.124.10.101.1.60.1.3000.102.10.1046.1" : ["DJ", "CLSC, Centre d'hébergement et hôpital Cloutier-du-Rivage"],
        "2.16.124.10.101.1.60.1.3000.101.10.1021.1" : ["DK", "CLSC, hôpital et centre d'hébergement Christ-Roi"],
        "2.16.124.10.101.1.60.1.3000.102.10.1071.0" : ["DL", "Conseil Cri de la Santé et des Services sociaux de la Baie-James"],
        "2.16.124.10.101.1.60.1.3000.102.10.1071.1" : ["DM", "Conseil Cri de la Santé et des Services sociaux de la Baie-James"],
        "2.16.124.10.101.1.60.1.3000.102.10.1058.0" : ["DN", "CSSS Ahuntsic et Montréal-Nord"],
        "2.16.124.10.101.1.60.1.3000.101.10.1040.0" : ["DP", "CSSS Alphonse-Desjardins"],
        "2.16.124.10.101.1.60.1.3000.102.10.1054.0" : ["DQ", "CSSS Champlain-Charles-Le-Moyne"],
        "2.16.124.10.101.1.60.1.3000.101.10.1014.0" : ["DR", "CSSS Cléophas-Claveau"],
        "2.16.124.10.101.1.60.1.3000.102.10.1052.0" : ["DS", "CSSS d'Antoine-Labelle"],
        "2.16.124.10.101.1.60.1.3000.102.10.1061.0" : ["DT", "CSSS d'Argenteuil"],
        "2.16.124.10.101.1.60.1.3000.103.10.1008.0" : ["DU", "CSSS d'Arthabaska-et-de-l'Érable"],
        "2.16.124.10.101.1.60.1.3000.101.10.1037.0" : ["DV", "CSSS de Beauce"],
        "2.16.124.10.101.1.60.1.3000.102.10.1044.0" : ["DW", "CSSS de Bécancour-Nicolet-Yamaska"],
        "2.16.124.10.101.1.60.1.3000.101.10.1019.0" : ["DX", "CSSS de Charlevoix"],
        "2.16.124.10.101.1.60.1.3000.101.10.1009.0" : ["DY", "CSSS de Chicoutimi"],
        "2.16.124.10.101.1.60.1.3000.103.10.1005.0" : ["DZ", "CSSS de de la MRC-de-Coaticook"],
        "2.16.124.10.101.1.60.1.3000.102.10.1059.0" : ["EA", "CSSS de Dorval-Lachine-Lasalle"],
        "2.16.124.10.101.1.60.1.3000.101.10.1013.0" : ["EB", "CSSS de Jonquière"],
        "2.16.124.10.101.1.60.1.3000.101.10.1002.0" : ["EC", "CSSS de Kamoukaska"],
        "2.16.124.10.101.1.60.1.3000.101.10.1034.0" : ["ED", "CSSS de la Baie-des-Chaleurs"],
        "2.16.124.10.101.1.60.1.3000.101.10.1030.0" : ["EE", "CSSS de la Basse-Côte-Nord"],
        "2.16.124.10.101.1.60.1.3000.101.10.1032.0" : ["EF", "CSSS de la Côte-de-Gaspé"],
        "2.16.124.10.101.1.60.1.3000.101.10.1026.0" : ["EG", "CSSS de la Haute-Côte-Nord"],
        "2.16.124.10.101.1.60.1.3000.101.10.1033.0" : ["EH", "CSSS de la Haute-Gaspésie"],
        "2.16.124.10.101.1.60.1.3000.103.10.1010.0" : ["EJ", "CSSS de la Haute-Yamaska"],
        "2.16.124.10.101.1.60.1.3000.101.10.1008.0" : ["EK", "CSSS de la Matapédia"],
        "2.16.124.10.101.1.60.1.3000.101.10.1028.0" : ["EL", "CSSS de la Minganie"],
        "2.16.124.10.101.1.60.1.3000.101.10.1003.0" : ["EM", "CSSS de la Mitis"],
        "2.16.124.10.101.1.60.1.3000.101.10.1038.0" : ["EN", "CSSS de la région de Thetford"],
        "2.16.124.10.101.1.60.1.3000.102.10.1042.0" : ["EP", "CSSS de la Vallée-de-la-Batiscan"],
        "2.16.124.10.101.1.60.1.3000.101.10.1021.0" : ["EQ", "CSSS de la Vieille-Capitale"],
        "2.16.124.10.101.1.60.1.3000.101.10.1010.0" : ["ER", "CSSS de Lac St-Jean-Est"],
        "2.16.124.10.101.1.60.1.3000.102.10.1048.0" : ["ES", "CSSS de Laval"],
        "2.16.124.10.101.1.60.1.3000.102.10.1041.0" : ["ET", "CSSS de l'Énergie"],
        "2.16.124.10.101.1.60.1.3000.101.10.1029.0" : ["EU", "CSSS de l'Hématite"],
        "2.16.124.10.101.1.60.1.3000.101.10.1025.0" : ["EV", "CSSS de Manicouagan"],
        "2.16.124.10.101.1.60.1.3000.102.10.1045.0" : ["EW", "CSSS de Maskinongé"],
        "2.16.124.10.101.1.60.1.3000.101.10.1004.0" : ["EX", "CSSS de Matane"],
        "2.16.124.10.101.1.60.1.3000.103.10.1003.0" : ["EY", "CSSS de Memphrémagog"],
        "2.16.124.10.101.1.60.1.3000.101.10.1027.0" : ["EZ", "CSSS de Port-Cartier"],
        "2.16.124.10.101.1.60.1.3000.101.10.1022.0" : ["FA", "CSSS de Portneuf"],
        "2.16.124.10.101.1.60.1.3000.101.10.1020.0" : ["FB", "CSSS de Québec-Nord"],
        "2.16.124.10.101.1.60.1.3000.101.10.1007.0" : ["FC", "CSSS de Rimouski-Neigette"],
        "2.16.124.10.101.1.60.1.3000.101.10.1001.0" : ["FD", "CSSS de Rivière-du-Loup"],
        "2.16.124.10.101.1.60.1.3000.101.10.1031.0" : ["FE", "CSSS de Sept-Îles"],
        "2.16.124.10.101.1.60.1.3000.102.10.1064.0" : ["FF", "CSSS de St-Jérôme"],
        "2.16.124.10.101.1.60.1.3000.101.10.1005.0" : ["FG", "CSSS de Témiscouata"],
        "2.16.124.10.101.1.60.1.3000.102.10.1046.0" : ["FH", "CSSS de Trois-Rivières"],
        "2.16.124.10.101.1.60.1.3000.102.10.1047.0" : ["FJ", "CSSS de Trois-Rivières"],
        "2.16.124.10.101.1.60.1.3000.103.10.1007.0" : ["FK", "CSSS de Val St-François"],
        "2.16.124.10.101.1.60.1.3000.101.10.1006.0" : ["FL", "CSSS des Basques"],
        "2.16.124.10.101.1.60.1.3000.101.10.1035.0" : ["FM", "CSSS des Iles"],
        "2.16.124.10.101.1.60.1.3000.102.10.1062.0" : ["FN", "CSSS des Sommets"],
        "2.16.124.10.101.1.60.1.3000.103.10.1004.0" : ["FP", "CSSS des Sources"],
        "2.16.124.10.101.1.60.1.3000.101.10.1011.0" : ["FQ", "CSSS Domaine-du-Roy"],
        "2.16.124.10.101.1.60.1.3000.103.10.1009.0" : ["FR", "CSSS Drumond"],
        "2.16.124.10.101.1.60.1.3000.103.10.1006.0" : ["FS", "CSSS du Granit"],
        "2.16.124.10.101.1.60.1.3000.102.10.1068.0" : ["FT", "CSSS du Haut St-Laurent"],
        "2.16.124.10.101.1.60.1.3000.102.10.1043.0" : ["FU", "CSSS du Haut-St-Maurice"],
        "2.16.124.10.101.1.60.1.3000.102.10.1063.0" : ["FV", "CSSS du Lac-des-Deux-Montagnes"],
        "2.16.124.10.101.1.60.1.3000.102.10.1050.0" : ["FW", "CSSS du Nord de Lanaudière"],
        "2.16.124.10.101.1.60.1.3000.101.10.1036.0" : ["FX", "CSSS du Rocher-Percé"],
        "2.16.124.10.101.1.60.1.3000.102.10.1051.0" : ["FY", "CSSS du Sud de Lanaudière"],
        "2.16.124.10.101.1.60.1.3000.102.10.1066.0" : ["FZ", "CSSS du Suroît"],
        "2.16.124.10.101.1.60.1.3000.103.10.1013.0" : ["GA", "CSSS Haut-Richelieu-Rouville"],
        "2.16.124.10.101.1.60.1.3000.102.10.1065.0" : ["GB", "CSSS Jardins-Roussillon"],
        "2.16.124.10.101.1.60.1.3000.102.10.1073.0" : ["GC", "CSSS Jeanne-Mance"],
        "2.16.124.10.101.1.60.1.3000.103.10.1012.0" : ["GD", "CSSS La Pommeraie"],
        "2.16.124.10.101.1.60.1.3000.101.10.1012.0" : ["GE", "CSSS Maria-Chapdelaine"],
        "2.16.124.10.101.1.60.1.3000.101.10.1039.0" : ["GF", "CSSS Montmagny-l'Islet"],
        "2.16.124.10.101.1.60.1.3000.102.10.1053.0" : ["GG", "CSSS Pierre-Boucher"],
        "2.16.124.10.101.1.60.1.3000.102.10.1067.0" : ["GH", "CSSS Pierre-de-Saurel"],
        "2.16.124.10.101.1.60.1.3000.103.10.1011.0" : ["GJ", "CSSS Richelieu-Yamaska"],
        "2.16.124.10.101.1.60.1.3000.102.10.1057.0" : ["GK", "CUSM"],
        "2.16.124.10.101.1.60.1.3000.102.10.1069.3" : ["GL", "Dispensaire de Kuujuarapik"],
        "2.16.124.10.101.1.60.1.3000.102.10.1069.4" : ["GM", "Dispensaire de Salluit"],
        "2.16.124.10.101.1.60.1.3000.102.10.1071.2" : ["GN", "Dispensaire de Wapmagoostui"],
        "2.16.124.10.101.1.60.1.3000.102.10.1069.2" : ["GP", "Dispensaire d'Inukjuak"],
        "2.16.124.10.101.1.60.1.3000.102.10.1068.1" : ["GQ", "Hôpital Barrie Memorial"],
        "2.16.124.10.101.1.60.1.3000.103.10.1012.1" : ["GR", "Hôpital Brome-Missisquoi-Perkins"],
        "2.16.124.10.101.1.60.1.3000.102.10.1054.1" : ["GS", "Hôpital Charles LeMoyne"],
        "2.16.124.10.101.1.60.1.3000.101.10.1020.2" : ["GT", "Hôpital Chaveau"],
        "2.16.124.10.101.1.60.1.3000.102.10.1048.1" : ["GU", "Hôpital Cité de la Santé"],
        "2.16.124.10.101.1.60.1.3000.101.10.1010.1" : ["GV", "Hôpital d'Alma"],
        "2.16.124.10.101.1.60.1.1023.1" :             ["GW", "Hôpital d'Amos"],
        "2.16.124.10.101.1.60.1.3000.101.10.1008.1" : ["GX", "Hôpital d'Amqui"],
        "2.16.124.10.101.1.60.1.3000.101.10.1019.1" : ["GY", "Hôpital de Baie-St-Paul"],
        "2.16.124.10.101.1.60.1.3000.101.10.1036.1" : ["GZ", "Hôpital de Chandler"],
        "2.16.124.10.101.1.60.1.3000.101.10.1009.1" : ["HA", "Hôpital de Chicoutimi"],
        "2.16.124.10.101.1.60.1.3000.101.10.1012.1" : ["HB", "Hôpital de Dolbeau-Mistassini"],
        "2.16.124.10.101.1.60.1.1008.1" :             ["HC", "Hôpital de Gatineau"],
        "2.16.124.10.101.1.60.1.3000.103.10.1010.1" : ["HD", "Hôpital de Granby"],
        "2.16.124.10.101.1.60.1.1009.1" :             ["HE", "Hôpital de Hull"],
        "2.16.124.10.101.1.60.1.3000.101.10.1014.1" : ["HF", "Hôpital de la Baie"],
        "2.16.124.10.101.1.60.1.3000.101.10.1019.2" : ["HG", "Hôpital de la Malbaie"],
        "2.16.124.10.101.1.60.1.3000.101.10.1035.1" : ["HH", "Hôpital de l'Archipel"],
        "2.16.124.10.101.1.60.1.3000.102.10.1059.1" : ["HJ", "Hôpital de Lasalle"],
        "2.16.124.10.101.1.60.1.3000.101.10.1016.1" : ["HK", "Hôpital de l'Enfant-Jésus"],
        "2.16.124.10.101.1.60.1.1010.1" :             ["HL", "Hôpital de Maniwaki"],
        "2.16.124.10.101.1.60.1.3000.101.10.1034.1" : ["HM", "Hôpital de Maria"],
        "2.16.124.10.101.1.60.1.3000.101.10.1004.1" : ["HN", "Hôpital de Matane"],
        "2.16.124.10.101.1.60.1.3000.102.10.1052.2" : ["HP", "Hôpital de Mont-Laurier"],
        "2.16.124.10.101.1.60.1.3000.101.10.1039.1" : ["HQ", "Hôpital de Montmagny"],
        "2.16.124.10.101.1.60.1.3000.101.10.1005.1" : ["HR", "Hôpital de Notre-Dame-du-Lac"],
        "2.16.124.10.101.1.60.1.1020.1" :             ["HS", "Hôpital de Papineau"],
        "2.16.124.10.101.1.60.1.1015.1" :             ["HT", "Hôpital de Rouyn-Noranda"],
        "2.16.124.10.101.1.60.1.3000.101.10.1020.1" : ["HU", "Hôpital de Ste-Anne-de-Beaupré"],
        "2.16.124.10.101.1.60.1.3000.101.10.1033.1" : ["HV", "Hôpital de Ste-Anne-des-Monts"],
        "2.16.124.10.101.1.60.1.3000.102.10.1063.1" : ["HW", "Hôpital de St-Eustache"],
        "2.16.124.10.101.1.60.1.3000.101.10.1037.1" : ["HX", "Hôpital de St-Georges"],
        "2.16.124.10.101.1.60.1.3000.101.10.1038.1" : ["HY", "Hôpital de Thetford Mines"],
        "2.16.124.10.101.1.60.1.1006.1" :             ["HZ", "Hôpital de Verdun"],
        "2.16.124.10.101.1.60.1.3000.102.10.1074.1" : ["JA", "Hôpital Douglas"],
        "2.16.124.10.101.1.60.1.3000.102.10.1041.1" : ["JB", "Hôpital du Centre-de-la-Mauricie"],
        "2.16.124.10.101.1.60.1.3000.103.10.1013.1" : ["JC", "Hôpital du Haut-Richelieu"],
        "2.16.124.10.101.1.60.1.1022.1" :             ["JD", "Hôpital du Pontiac"],
        "2.16.124.10.101.1.60.1.1001.1" :             ["JE", "Hôpital du Sacré-Coeur de Montréal"],
        "2.16.124.10.101.1.60.1.3000.101.10.1016.2" : ["JF", "Hôpital du St-Sacrement"],
        "2.16.124.10.101.1.60.1.3000.102.10.1066.1" : ["JG", "Hôpital du Suroît"],
        "2.16.124.10.101.1.60.1.3000.101.10.1013.1" : ["JH", "Hôpital et centre de réadaptation de Jonquière"],
        "2.16.124.10.101.1.60.1.3000.101.10.1031.1" : ["JJ", "Hôpital et centre d'hébergement de Sept-Iles"],
        "2.16.124.10.101.1.60.1.3000.103.10.1002.1" : ["JK", "Hôpital et centre d'hébergement d'Youville"],
        "2.16.124.10.101.1.60.1.1013.1" :             ["JL", "Hôpital et CLSC de Val-d'Or"],
        "2.16.124.10.101.1.60.1.3000.103.10.1001.1" : ["JM", "Hôpital Fleurimont"],
        "2.16.124.10.101.1.60.1.3000.102.10.1058.1" : ["JN", "Hôpital Fleury"],
        "2.16.124.10.101.1.60.1.3000.102.10.1057.1" : ["JP", "Hôpital général de Montréal"],
        "2.16.124.10.101.1.60.1.1007.1" :             ["JQ", "Hôpital général du Lakeshore"],
        "2.16.124.10.101.1.60.1.1002.1" :             ["JR", "Hôpital général juif"],
        "2.16.124.10.101.1.60.1.3000.103.10.1011.1" : ["JS", "Hôpital Honoré-Mercier"],
        "2.16.124.10.101.1.60.1.3000.101.10.1032.1" : ["JT", "Hôpital Hôtel-Dieu de Gaspé"],
        "2.16.124.10.101.1.60.1.1005.1" :             ["JU", "Hôpital Jean-Talon"],
        "2.16.124.10.101.1.60.1.3000.101.10.1023.1" : ["JV", "Hôpital Jeffery Hale"],
        "2.16.124.10.101.1.60.1.3000.101.10.1023.0" : ["JW", "Hôpital Jeffery Hale - Saint Brigid's "],
        "2.16.124.10.101.1.60.1.3000.102.10.1049.0" : ["JX", "Hôpital juif de réadaptation"],
        "2.16.124.10.101.1.60.1.3000.102.10.1049.1" : ["JY", "Hôpital juif de réadaptation"],
        "2.16.124.10.101.1.60.1.3000.102.10.1057.4" : ["JZ", "Hôpital Lachine"],
        "2.16.124.10.101.1.60.1.3000.102.10.1062.1" : ["KA", "Hôpital Laurentien"],
        "2.16.124.10.101.1.60.1.3000.101.10.1025.1" : ["KB", "Hôpital le Royer"],
        "2.16.124.10.101.1.60.1.1030.1" :             ["KC", "Hôpital Louis-H. Lafontaine"],
        "2.16.124.10.101.1.60.1.3000.102.10.1056.0" : ["KD", "Hôpital Maisonneuve-Rosemont"],
        "2.16.124.10.101.1.60.1.3000.102.10.1075.1" : ["KE", "Hôpital Marie-Clarac"],
        "2.16.124.10.101.1.60.1.3000.102.10.1075.0" : ["KF", "Hôpital Marie-Clarac des sœurs de la charité de Ste-Marie"],
        "2.16.124.10.101.1.60.1.1017.1" :             ["KG", "Hôpital memorial de Wakefield/Wakefield Mem. Hospital"],
        "2.16.124.10.101.1.60.1.1026.1" :             ["KH", "Hôpital Mont-Sinai"],
        "2.16.124.10.101.1.60.1.3000.102.10.1057.5" : ["KJ", "Hôpital neurologique de Montréal"],
        "2.16.124.10.101.1.60.1.3000.102.10.1055.3" : ["KK", "Hôpital Notre-Dame du CHUM"],
        "2.16.124.10.101.1.60.1.3000.101.10.1002.1" : ["KL", "Hôpital Notre-Dame-de-Fatima"],
        "2.16.124.10.101.1.60.1.3000.102.10.1053.1" : ["KM", "Hôpital Pierre-Boucher"],
        "2.16.124.10.101.1.60.1.3000.102.10.1051.1" : ["KN", "Hôpital Pierre-Le Gardeur"],
        "2.16.124.10.101.1.60.1.1011.1" :             ["KP", "Hôpital psychiatrique de Malartic"],
        "2.16.124.10.101.1.60.1.3000.101.10.1022.2" : ["KQ", "Hôpital régional de Portneuf"],
        "2.16.124.10.101.1.60.1.3000.101.10.1007.1" : ["KR", "Hôpital régional de Rimouski"],
        "2.16.124.10.101.1.60.1.3000.102.10.1064.1" : ["KS", "Hôpital régional de St-Jérôme"],
        "2.16.124.10.101.1.60.1.1031.1" :             ["KT", "Hôpital Rivière-des-Prairies"],
        "2.16.124.10.101.1.60.1.3000.102.10.1057.2" : ["KU", "Hôpital Royal-Victoria"],
        "2.16.124.10.101.1.60.1.1003.1" :             ["KV", "Hôpital Santa-Cabrini"],
        "2.16.124.10.101.1.60.1.3000.102.10.1077.1" : ["KW", "Hôpital Shriners pour enfants (Québec)"],
        "2.16.124.10.101.1.60.1.3000.102.10.1077.0" : ["KX", "Hôpital Shriners pour enfants (Québec) Inc."],
        "2.16.124.10.101.1.60.1.3000.102.10.1076.1" : ["KY", "Hôpital Ste-Anne"],
        "2.16.124.10.101.1.60.1.3000.103.10.1009.1" : ["KZ", "Hôpital Ste-Croix"],
        "2.16.124.10.101.1.60.1.3000.102.10.1055.2" : ["LA", "Hôpital St-Luc du CHUM"],
        "2.16.124.10.101.1.60.1.3000.103.10.1004.1" : ["LB", "Hôpital, CLSC et Centre d'hébergement d'Asbestos"],
        "2.16.124.10.101.1.60.1.3000.101.10.1011.1" : ["LC", "Hôpital, CLSC et Centre d'hébergement de Roberval"],
        "2.16.124.10.101.1.60.1.1035.1" :             ["LD", "HôpitalCatherine-Booth"],
        "2.16.124.10.101.1.60.1.3000.103.10.1008.1" : ["LE", "Hôtel-Dieu d'Arthabaska"],
        "2.16.124.10.101.1.60.1.3000.103.10.1001.2" : ["LF", "Hôtel-Dieu de Sherbrooke"],
        "2.16.124.10.101.1.60.1.3000.102.10.1067.1" : ["LG", "Hôtel-Dieu de Sorel"],
        "2.16.124.10.101.1.60.1.3000.102.10.1055.1" : ["LH", "Hôtel-Dieu du CHUM"],
        "2.16.124.10.101.1.60.1.3000.102.10.1078.0" : ["LJ", "Institut de cardiologie de Montréal"],
        "2.16.124.10.101.1.60.1.3000.102.10.1078.1" : ["LK", "Institut de cardiologie de Montréal"],
        "2.16.124.10.101.1.60.1.1032.1" :             ["LL", "Institut de réadaptation de Montréal"],
        "2.16.124.10.101.1.60.1.3000.101.10.1017.0" : ["LM", "Institut de réadaptation en déficience physique de Québec"],
        "2.16.124.10.101.1.60.1.3000.101.10.1017.1" : ["LN", "Institut de réadaptation en déficience physique de Québec"],
        "2.16.124.10.101.1.60.1.1033.1" :             ["LP", "Institut Philippe-Pinel de Montréal"],
        "2.16.124.10.101.1.60.1.3000.102.10.1057.6" : ["LQ", "Institut thoracique de Montréal"],
        "2.16.124.10.101.1.60.1.3000.101.10.1024.0" : ["LR", "Institut universitaire de cardiologie et de pneumologie de Québec"],
        "2.16.124.10.101.1.60.1.3000.101.10.1024.1" : ["LS", "Institut universitaire de cardiologie et de pneumologie de Québec"],
        "2.16.124.10.101.1.60.1.3000.103.10.1002.0" : ["LT", "Institut universitaire de gériatrie de Sherbrooke"],
        "2.16.124.10.101.1.60.1.3000.101.10.1018.0" : ["LU", "Institut universitaire en santé mentale de Québec"],
        "2.16.124.10.101.1.60.1.3000.101.10.1018.1" : ["LV", "Institut universitaire en santé mentale de Québec"],
        "2.16.124.10.101.1.60.1.3000.102.10.1074.0" : ["LW", "Institut universitaire en santé mentale Douglas"],
        "2.16.124.10.101.1.60.1.1028.1" :             ["LX", "L'hôpital Chinois de Montréal (1963)"],
        "2.16.124.10.101.1.60.1.3000.102.10.1057.3" : ["LY", "L'hôpital de Montréal pour enfants"],
        "2.16.124.10.101.1.60.1.3000.102.10.1056.1" : ["LZ", "Pav. Maisonneuve/Pav. Marcel-Lamoureux"],
        "2.16.124.10.101.1.60.1.3000.101.10.1015.3" : ["MA", "Pavillon centre hospitalier de l'université Laval (CHUL)"],
        "2.16.124.10.101.1.60.1.1034.1" :             ["MB", "Pavillon Côte-des-Neige"],
        "2.16.124.10.101.1.60.1.3000.101.10.1026.1" : ["MC", "Pavillon Escoumin"],
        "2.16.124.10.101.1.60.1.3000.101.10.1026.2" : ["MD", "Pavillon Forestville"],
        "2.16.124.10.101.1.60.1.3000.101.10.1015.2" : ["ME", "Pavillon l'Hôtel-Dieu de Québec"],
        "2.16.124.10.101.1.60.1.1018.1" :             ["MF", "Pavillon Ste-Famille"],
        "2.16.124.10.101.1.60.1.3000.102.10.1047.1" : ["MG", "Pavillon Ste-Marie (CHRTR)"],
        "2.16.124.10.101.1.60.1.3000.101.10.1015.1" : ["MH", "Pavillon St-François d'Assise"],
        "2.16.124.10.101.1.60.1.3000.102.10.1047.2" : ["MJ", "Pavillon St-Joseph (CHRTR)"],
        "2.16.124.10.101.1.60.1.1024.1" :             ["MK", "Point de service de Témiscamingue-et-de-Kipawa"],
        "toronto.lakeshore" :                         ["TL", "Toronto Lakeshore"],

        // won't be able to determine the institution for a blank Assigning Authority
        "" : ["ZZ", "Institution Inconnue"],

        // Same with null Assigning Authorities
        null : ["ZZ", "Institution Inconnue"]

    ].withDefault{["AU", "Autre Institution Inconnue"]}

} // end of Prefixes class


class Demographics {

    /** @return an array with [0]=family name; [1]=given name  */
    public static parseName(String name) {

        // assume "family, given middle etc"
        if (name.contains(",")) {
            // get rid of extra splitters left in by some dicom systems:
            // returns middle name as part of given name
            name = name.replaceAll("\\^", "")

            // only split on first comma:
            return name.split(",", 2).collect{it.trim()}
       }

        // assume dicom standard "family^given^middle^etc"
        if (name.contains("^")) {
            // split on all ^s but only take the first two
            // doesn't return middle name, should we?
            return name.split("\\^")[0..1]
        }

        // assume "given family" where family might be multiple parts
        // too difficult to tell difference between middle names and
        // multi-part last names
        return name.split("\\s+", 2).reverse()
    }
}

class UIDs {
    private static localizeUID(sop, context, log, hash, tag) {
        def uids = sop.getList(tag)
        log.debug("{} is {}", tag, uids)

        // only set if there is at least one, in case we're touching an
        // object with no ids (eg, a cfind request might have no
        // series or sop instance uids)
        if (!uids.isEmpty()) {
            uids = uids.collect { context.localizeUID(it, hash) }
            sop.set(tag, uids, null)
            log.debug("Setting hashed {} to {}", tag, uids)
        }

        if (uids.isEmpty()) {
            // wasn't present, we effectively leave it null
            return null
        } else if (uids.size() == 1) {
            // common case of only one uid, they only want one
            // back...
            return uids[0]
        } else {
            // if we localized multiple uids, make sure they
            // get them all
            return uids
        }
    }

    /**
     * Localize study, series and sop instances uids.
     * Supports lists of uids as well.
     *
     * @param sop the dicom object with the uids
     * @param context the current context from contexts.groovy
     * @param hash the hash closure provided by connect
     * @param log the logger
     * @return a list of the new study, series and sop uids in that order.
     *         If any had multiple values, a nested list is returned.
     */
    public static localizeUIDs(sop, context, hash, log) {
        return [
            localizeUID(sop, context, log, hash, 'StudyInstanceUID'),
            localizeUID(sop, context, log, hash, 'SeriesInstanceUID'),
            localizeUID(sop, context, log, hash, 'SOPInstanceUID')
        ]
    }
}

