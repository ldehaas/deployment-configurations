/* This file contains both the Actual Rules and the Procedure Code Mapping code that actually apply the rules */

/* The content of the file is the following:

    -  'rules' contains the actual site-specific Procedure Code Mapping rules

    - findMatchingRule finds the rule that applies to a given modality and anatomic region
    - matches, all and any are worker functions
*/


class Rules {

    /**
     * Site-specific Procedure Code Mapping Rules
     * [ modality, anatomic region, study description pattern, procedure code, procedure description ]
     */
    static rules = [
        [ "CT", "TC", any(),                                 "6834TC01", "DSQ_CT_Tête et cou", ],
        [ "CT", "DI", any(),                                 "6834DI01", "DSQ_CT_Digestif", ],

        /* these were in the 'Reles de prechargement v1.2 document, but did not have any keywords associated with them
        [ "CT", "DI", any(),                                 "6834DI02", "DSQ_CT_Coloscopie virtuelle", ],
        [ "CT", "DI", any(),                                 "6834DI03", "DSQ_CT_Entéroscan", ],
        */

        [ "CT", "TH", any(),                                 "6834TH01", "DSQ_CT_Thorax", ],
        [ "CT", "AB", all("thor" , "abd", "pelv"),           "6834AB04", "DSQ_CT_Thorax-Abdomen-Pelvien", ],
        [ "CT", "AB", all("abd", "pelv"),                    "6834AB03", "DSQ_CT_Abdomen-Pelvien", ],
        [ "CT", "AB", all("pelv"),                           "6834AB02", "DSQ_CT_Pelvien", ],
        [ "CT", "AB", any(),                                 "6834AB01", "DSQ_CT_Abdomen", ],
        [ "CT", "CO", any(),                                 "6834CO01", "DSQ_CT_Colonnes", ],
        [ "CT", "ES", any(),                                 "6834ES01", "DSQ_CT_Membres supérieurs", ],
        [ "CT", "EI", all("bassin", "inf"),                  "6834EI03", "DSQ_CT_Bassin osseux-Membres inférieurs", ],
        [ "CT", "EI", all("bassin"),                         "6834EI02", "DSQ_CT_Bassin osseux", ],
        [ "CT", "EI", any(),                                 "6834EI01", "DSQ_CT_Membres inférieurs", ],
        [ "CT", "CA", any(),                                 "6834CA01", "DSQ_CT_Cardiaque", ],
        [ "CT", "GO", any(),                                 "6834TG01", "DSQ_CT_Gynéco-obstétrique", ],
        [ "CT", "TP", any(),                                 "6834TP01", "DSQ_CT_TEP", ],
        [ "MR", "TC", any(),                                 "6835TC01", "DSQ_MR_Tête et cou", ],
        [ "MR", "DI", any(),                                 "6835DI01", "DSQ_MR_Digestif", ],
        [ "MR", "TH", any(),                                 "6835TH01", "DSQ_MR_Thorax", ],
        [ "MR", "AB", all("thor", "abd", "pelv"),            "6835AB04", "DSQ_MR_Thorax-Abdomen-Pelviene", ],
        [ "MR", "AB", all("abd", "pelv"),                    "6835AB03", "DSQ_MR_Abdomen-Pelvien", ],
        [ "MR", "AB", all("pelv"),                           "6835AB02", "DSQ_MR_Pelvien", ],
        [ "MR", "AB", any(),                                 "6835AB01", "DSQ_MR_Abdomen", ],
        [ "MR", "CO", any(),                                 "6835CO01", "DSQ_MR_Colonnes", ],
        [ "MR", "ES", any(),                                 "6835ES01", "DSQ_MR_Membres supérieurs", ],
        [ "MR", "EI", all("bassin", "inf"),                  "6835EI03", "DSQ_MR_Bassin osseux-Membres inférieurs", ],
        [ "MR", "EI", all("bassin"),                         "6835EI02", "DSQ_MR_Bassin osseux", ],
        [ "MR", "EI", any(),                                 "6835EI01", "DSQ_MR_Membres inférieurs", ],
        [ "MR", "CA", any(),                                 "6835CA01", "DSQ_MR_Cardiaque", ],
        [ "MR", "GO", any(),                                 "6835TG01", "DSQ_MR_Gynéco-obstétrique", ],
        [ "CR", "TC", any("crâne", "crane"),                 "6831TC02", "DSQ_CR_Crâne", ],
        [ "CR", "TC", all("sinus"),                          "6831TC03", "DSQ_CR_Sinus", ],
        [ "CR", "TC", all("masto"),                          "6831TC04", "DSQ_CR_Mastoïdes", ],
        [ "CR", "TC", all("orbite"),                         "6831TC05", "DSQ_CR_Orbites", ],
        [ "CR", "TC", all("cou"),                            "6831TC06", "DSQ_CR_Tissus mous du cou", ],
        [ "CR", "TC", any(),                                 "6831TC01", "DSQ_CR_Tête et cou", ],
        [ "CR", "DI", any(),                                 "6831DI01", "DSQ_CR_Digestif", ],
        [ "CR", "TH", all("thorax"),                         "6831TH02", "DSQ_CR_Thorax osseux", ],
        [ "CR", "TH", any(),                                 "6831TH01", "DSQ_CR_Poumons", ],
        [ "CR", "AB", any(),                                 "6831AB01", "DSQ_CR_Abdomen", ],
        [ "CR", "CO", all("cerv", "dors"),                   "6831CO05", "DSQ_CR_Colonne cervico-dorsale", ],
        [ "CR", "CO", all("dors", "lomb"),                   "6831CO06", "DSQ_CR_Colonne dorso-lombaire", ],
        [ "CR", "CO", all("cerv"),                           "6831CO02", "DSQ_CR_Colonne cervicale", ],
        [ "CR", "CO", all("dors"),                           "6831CO03", "DSQ_CR_Colonne dorsale", ],
        [ "CR", "CO", all("lomb"),                           "6831CO04", "DSQ_CR_Collonne lombaire", ],
        [ "CR", "CO", all("scolio"),                         "6831CO07", "DSQ_CR_Scoliose", ],
        [ "CR", "CO", any(),                                 "6831CO01", "DSQ_CR_Colonnes", ],
        [ "CR", "ES", all("sternum"),                        "6831ES02", "DSQ_CR_Sternum", ],
        [ "CR", "ES", all("omoplate"),                       "6831ES03", "DSQ_CR_Omoplate", ],
        [ "CR", "ES", all("clavicule"),                      "6831ES04", "DSQ_CR_Clavicule", ],
        [ "CR", "ES", any("epaule", "épaule"),               "6831ES05", "DSQ_CR_Épaule", ],
        [ "CR", "ES", all("humérus"),                        "6831ES06", "DSQ_CR_Humérus", ],
        [ "CR", "ES", all("coude"),                          "6831ES07", "DSQ_CR_Coude", ],
        [ "CR", "ES", all("avant-bras"),                     "6831ES08", "DSQ_CR_Avant-bras", ],
        [ "CR", "ES", all("main", "poignet"),                "6831ES11", "DSQ_CR_Main-poignet", ],
        [ "CR", "ES", all("poignet"),                        "6831ES09", "DSQ_CR_Poignet", ],
        [ "CR", "ES", all("main"),                           "6831ES10", "DSQ_CR_Main", ],
        [ "CR", "ES", any("doigt", "pouce", "auriculaire", "annulaire", "majeur", "index"), "6831ES12", "DSQ_CR_Doigt", ],
        [ "CR", "ES", any(),                                 "6831ES01", "CR_Membres supérieurs", ],
        [ "CR", "EI", any("bassin", "hanche"),               "6831EI02", "DSQ_CR_Bassin-Hanches", ],
        [ "CR", "EI", any("femur", "fémur"),                 "6831EI03", "DSQ_CR_Fémur", ],
        [ "CR", "EI", all("genou"),                          "6831EI04", "DSQ_CR_Genou", ],
        [ "CR", "EI", all("jambe"),                          "6831EI05", "DSQ_CR_Jambe", ],
        [ "CR", "EI", all("cheville", "pied"),               "6831EI08", "DSQ_CR_Pied-cheville", ],
        [ "CR", "EI", all("cheville"),                       "6831EI06", "DSQ_CR_Cheville", ],
        [ "CR", "EI", all("pied"),                           "6831EI07", "DSQ_CR_Pied", ],
        [ "CR", "EI", all("orteil"),                         "6831EI09", "DSQ_CR_Orteil", ],
        [ "CR", "EI", any(),                                 "6831EI01", "DSQ_CR_Membres inférieurs", ],
        [ "CR", "GO", any(),                                 "6831GO01", "DSQ_CR_Gynéco-obstétrique", ],
        [ "CR", "SQ", any(),                                 "6831SQ01", "DSQ_CR_Série squelettique", ],
        [ "RF", "DI", all("oeso"),                           "6831DI52", "DSQ_RF_Oesophage", ],
        [ "RF", "DI", all("repas", any("grele", "grêle")),   "6831DI55", "DSQ_RF_Repas baryté et grêle", ],
        [ "RF", "DI", all("repas"),                          "6831DI53", "DSQ_RF_Repas baryté", ],
        [ "RF", "DI", any("grele", "grêle"),                 "6831DI54", "DSQ_RF_Grêle", ],
        [ "RF", "DI", all("lavem"),                          "6831DI56", "DSQ_RF_Lavement baryté", ],
        [ "RF", "DI", any(),                                 "6831DI51", "DSQ_RD_Digestif", ],
        [ "RF", "AB", all("cysto"),                          "6831AB52", "DSQ_RF_Cystographie", ],
        [ "RF", "AB", any(),                                 "6831AB51", "DSQ_RF_Abdomen", ],
        [ "RF", "CO", all("myelo"),                          "6831CO52", "DSQ_RF_Myélographie", ],
        [ "RF", "CO", any(),                                 "6831CO51", "DSQ_RF_Colonnes", ],
        [ "RF", "ES", any("epaule", "épaule"),               "6831SQ52", "DSQ_RF_Arthrographie épaule", ],
        [ "RF", "ES", all("coude"),                          "6831SQ53", "DSQ_RF_Arthrographie coude", ],
        [ "RF", "ES", all("poignet"),                        "6831SQ54", "DSQ_RF_Arthrographie poignet", ],
        [ "RF", "ES", any(),                                 "6831ES51", "DSQ_RF_Extrémités supérieurs", ],
        [ "RF", "EI", all("hanche"),                         "6831SQ55", "DSQ_RF_Arthrographie hanche", ],
        [ "RF", "EI", all("genou"),                          "6831SQ56", "DSQ_RF_Arthrographie genou", ],
        [ "RF", "EI", all("cheville"),                       "6831SQ57", "DSQ_RF_Arthrographie cheville", ],
        [ "RF", "EI", any(),                                 "6831EI51", "DSQ_RF_Extrémités inférieurs", ],
        [ "RF", "SE", any(),                                 "6831SE51", "DSQ_RF_Sein", ],
        [ "RF", "AN", any(),                                 "6831AN51", "DSQ_RF_Interventions vasculaires", ],
        [ "RF", "GO", any(),                                 "6831GO51", "DSQ_RF_Gynéco-obstétrique", ],
        [ "RF", "SQ", any(),                                 "6831SQ51", "DSQ_RF_Musculo-squelettique", ],
        [ "US", "TC", any(),                                 "6832TC01", "DSQ_US_Tête et cou", ],
        [ "US", "DI", any(),                                 "6832DI01", "DSQ_US_Digestif", ],
        [ "US", "AB", all("abd", "pelv"),                    "6832AB03", "DSQ_US_Abdomino-pelvienne", ],
        [ "US", "AB", all("pelv"),                           "6832AB02", "DSQ_US_Pelvienne", ],
        [ "US", "AB", all("prost"),                          "6832AB04", "DSQ_US_Prostate", ],
        [ "US", "AB", all("test"),                           "6832AB05", "DSQ_US_Testicule", ],
        [ "US", "AB", all("doppler", any("renal", "rénal")), "6832AB07", "DSQ_US_Doppler rénal", ],
        [ "US", "AB", all("doppler", "portal"),              "6832AB08", "DSQ_US_Doppler portal", ],
        [ "US", "AB", all("doppler"),                        "6832AB06", "DSQ_US_Doppler abdominal ou pelvien", ],
        [ "US", "AB", any(),                                 "6832AB01", "DSQ_US_Abdomen", ],
        [ "US", "ES", all("dopp"),                           "6832ES02", "DSQ_US_Doppler des membres supérieurs", ],
        [ "US", "ES", any(),                                 "6832ES01", "DSQ_US_Extrémités supérieures", ],
        [ "US", "EI", all("dopp"),                           "6832EI02", "DSQ_US_Doppler des membres inférieurs", ],
        [ "US", "EI", any(),                                 "6832EI01", "DSQ_US_Extrémités inférieures", ],
        [ "US", "CA", any(),                                 "6832CA01", "DSQ_US_Cardio-vasculaire", ],
        [ "US", "HE", any(),                                 "6832HE01", "DSQ_US_Cardiaque", ],
        [ "US", "SE", any(),                                 "6832SE01", "DSQ_US_Sein", ],
        [ "US", "GO", all("gyn", "pelv"),                    "6832GO02", "DSQ_US_Pelvienne gynécologique", ],
        [ "US", "GO", all("trimes", "obst"),                 "6832GO03", "DSQ_US_Obstétricale", ],
        [ "US", "GO", any(),                                 "6832GO01", "DSQ_US_Gynéco-obstétrique", ],
        [ "MG", "SE", any("mammo", "clich", "agrandis"),     "6833SE02", "DSQ_MG_Mammographie diagnostique", ],
        [ "MG", "SE", any(),                                 "6833SE01", "DSQ_MG_Imagerie du sein", ],
        [ "XA", "TC", any(),                                 "6836TC01", "DSQ_XA_Angio diagnostique Tête et cou", ],
        [ "XA", "DI", any(),                                 "6836DI01", "DSQ_XA_Angio diagnostique Digestif", ],
        [ "XA", "TH", any(),                                 "6836TH01", "DSQ_XA_Angio diagnostique Thorax", ],
        [ "XA", "AB", any(),                                 "6836AB01", "DSQ_XA_Angio diagnostique Abdomen", ],
        [ "XA", "CO", any(),                                 "6836CO01", "DSQ_XA_Angio diagnostique Colonnes", ],
        [ "XA", "ES", any(),                                 "6836ES01", "DSQ_XA_Angio diagnostique Extrémités supérieures", ],
        [ "XA", "EI", any(),                                 "6836EI01", "DSQ_XA_Angio diagnostique Extrémités inférieures", ],
        [ "XA", "CA", any(),                                 "6836CA01", "DSQ_XA_Angio diagnostique Cardiaque", ],
        [ "XA", "HE", any(),                                 "6836HE01", "DSQ_XA_Angio interventionnelle Cardiaque ", ],
        [ "XA", "AN", any(),                                 "6836AN01", "DSQ_XA_Angio interventionnelle", ],
        [ "NM", "TC", all("thyroïde"),                       "6785TC02", "DSQ_MN_Thyroide", ],
        [ "NM", "TC", all("parathyroïde"),                   "6785TC03", "DSQ_MN_Parathyroide", ],
        [ "NM", "TC", all("trait", "iode"),                  "6785TC04", "DSQ_MN_Traitement a l'iode", ],
        [ "NM", "TC", all("cisterno"),                       "6785TC05", "DSQ_MN_Cisternographie", ],
        [ "NM", "TC", any(),                                 "6785TC01", "DSQ_MN_Tête et cou", ],
        [ "NM", "DI", all("hemor"),                          "6785DI02", "DSQ_MN_Hemorragie digestive (Bleeding)", ],
        [ "NM", "DI", all("HIDA"),                           "6785DI03", "DSQ_MN_HIDA", ],
        [ "NM", "DI", all("Meckel"),                         "6785DI04", "DSQ_MN_Meckel", ],
        [ "NM", "DI", any(),                                 "6785DI01", "DSQ_MN_Digestif", ],
        [ "NM", "TH", all("poumon"),                         "6785TH02", "DSQ_MN_Poumons", ],
        [ "NM", "TH", any(),                                 "6785TH01", "DSQ_MN_Thorax", ],
        [ "NM", "AB", any("heman", "héman"),                 "6785AB02", "DSQ_MN_Hemangiome (Liver RBC)", ],
        [ "NM", "AB", all("rein", "DMSA"),                   "6785AB04", "DSQ_MN_Reins DMSA", ],
        [ "NM", "AB", all("rein"),                           "6785AB03", "DSQ_MN_Reins", ],
        [ "NM", "AB", all("testi"),                          "6785AB05", "DSQ_MN_Testicules", ],
        [ "NM", "AB", all("MIBG"),                           "6785AB06", "DSQ_MN_MIBG", ],
        [ "NM", "AB", all("cysto"),                          "6785AB07", "DSQ_MN_Cystographie mictionnelle", ],
        [ "NM", "AB", all("octreo"),                         "6785AB08", "DSQ_MN_Octreotide", ],
        [ "NM", "AB", any(),                                 "6785AB01", "DSQ_MN_Abdomen", ],
        [ "NM", "CA", all("Mibi"),                           "6785CA02", "DSQ_MN_MIBI cardiaque", ],
        [ "NM", "CA", all("vein"),                           "6785CA03", "DSQ_MN_Veinographie", ],
        [ "NM", "CA", all("ventric"),                        "6785CA04", "DSQ_MN_Ventriculographie isotopique ", ],
        [ "NM", "CA", all("thallium"),                       "6785CA05", "DSQ_MN_Thallium", ],
        [ "NM", "CA", any(),                                 "6785CA01", "DSQ_MN_Cardio-vasculaire", ],
        [ "NM", "SE", all("sentin"),                         "6785SE02", "DSQ_MN_Ganglion sentinelle", ],
        [ "NM", "SE", any(),                                 "6785SE01", "DSQ_MN_Sein", ],
        [ "NM", "EN", any(),                                 "6785EN01", "DSQ_MN_Endocrinien", ],
        [ "NM", "TP", any(),                                 "6785TP01", "DSQ_MN_Tomographie par émission de positrons", ],
        [ "NM", "SQ", all("osseuse", "gallium"),             "6785SQ03", "DSQ_MN_Osseuse + gallium", ],
        [ "NM", "SQ", all("osseuse"),                        "6785SQ02", "DSQ_MN_Osseuse", ],
        [ "NM", "SQ", any("osteo", "ostéo"),                 "6785SQ04", "DSQ_MN_Osteodensitometrie", ],
        [ "NM", "SQ", all("blanc"),                          "6785SQ05", "DSQ_MN_Globules blancs indium", ],
        [ "NM", "SQ", all("Gallium"),                        "6785SQ06", "DSQ_MN_Gallium", ],
        [ "NM", "SQ", any(),                                 "6785SQ01", "DSQ_MN_Squelettique", ],
        [ "NM", "HP", any(),                                 "6785HP01", "DSQ_MN_Hématopoïetique", ],
        [ "PT", "TP", any(),                                 "6786TP01", "DSQ_PT_Tomodensitométrie par émission de positrons", ],
        [ "MR", "SE", any(),                                 "6835SE01", "DSQ_MR_Sein", ],
    // Turns into a map with the modality and anatomic region as the key and a list of matching
    // rules as the value. Default empty list to ease processing:
    ].groupBy{ [it[0], it[1]] }.withDefault{ [] }


    /** 
     * Finds the rule that applies to a given modality and anatomic region
     * @return null when there is no rule match
     */
    public static findMatchingRule(modality, anatomicRegion, studyDesc) {
        for (rule in rules[[modality, anatomicRegion]]) {
            if (modality == rule[0] && anatomicRegion == rule[1] && rule[2](studyDesc)) {
                return rule
            }
        }

        return null
    }

    /**
     * Creates a closure that returns true if its argument
     * matches all of these criteria.
     */
    private static all(Object ... args) {
        return { candidate -> 
            for (arg in args) {
                if (!matches(arg, candidate)) {
                    return false
                }
            }

            return true
        }
    }

    /**
     * Creates a closure that returns true if its argument
     * matches any of these criteria.
     */
    private static any(Object ... args) {
        // an empty list just means to always match
        if (args.length == 0) {
            return { true }
        }

        return { candidate ->
            for (arg in args) {
                if (matches(arg, candidate)) {
                    return true
                }
            }

            return false
        }
    }

    /**
     * Checks if criteria is a closure that returns true
     * for the candidate or if the criteria is a string
     * that is contains the candidate.
     */
    private static matches(criteria, candidate) {
        if (criteria instanceof Closure) {
            return criteria(candidate)
        } else {
            return candidate.contains(criteria)
        }
    }
}

