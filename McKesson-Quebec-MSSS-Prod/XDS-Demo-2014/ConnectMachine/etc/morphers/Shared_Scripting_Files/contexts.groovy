
/**
 * Representation of a single context.  See Contexts class below for
 * a set of instances representing the contexts for this deployment.
 */
class Context {
    /** AE title of Rialto's CMove interface */
    String RialtoRetrieveAE

    String InstitutionName

    /** Assigning Authority for the local PACS */
    String PACSIssuer

    /** The calling ae title used by the PACS for query/retrieve.
     * Get this from the connectivity spreadsheet or the SAD */
    String PACSCallingAE

    /** Context ID as known by the PACS; returned as IssuerOfPatientID
     * in cfind queries; used to identify contexts in demographics queries */
    String ContextID

    /** The HL7 sending facility for the local RIS */
    List RISSendingFacility

    /** The HL7 sending application for the local RIS */
    List RISSendingApplication

    String HashPrefix

    public String localizeUID(uid, hash) {
        if (HashPrefix == null || uid == null) {
            return uid
        }

        return hash(HashPrefix, uid)
    }

    public String unlocalizeUID(uid, unhash) {
        //  if we're not hashing, we're also not unhashing:
        if (HashPrefix == null) {
            return uid
        }

        return unhash(uid)
    }

    /**
     * @return the preferred sending application for this context's RIS
     */
    public String preferredRISSendingApplication() {
        return RISSendingApplication.isEmpty() ? null : RISSendingApplication[0]
    }
    
    /**
     * @return the preferred sending facility for this context's RIS
     */
    public String preferredRISSendingFacility() {
        return RISSendingFacility.isEmpty() ? null : RISSendingFacility[0]
    }

    /**
     * @return if the issuer is local to the PACS
     */
    public boolean isLocal(issuer) {
        if (Contexts.CONTEXTS_ARE_ISOLATED) {
            return issuer == PACSIssuer
        } else {
            return Contexts.ALL_CONTEXTS.any { id, c -> c.PACSIssuer == issuer }
        }
    }

}

class Contexts {
    // Add one entry per context here.  The key should
    // correspond to the ContextID configuration parameter.
    // Set the HashPrefix for multi-context sites and leave
    // it null for single-context deployments.
    private static final ALL_CONTEXTS = [
        "LOCAL": new Context(
            RialtoRetrieveAE:       "XDSDEMOSCP",
            InstitutionName:        "Hôpital Louis-H. Lafontaine",
            PACSIssuer:             "2.16.124.10.101.1.60.1.1030.1",
            PACSCallingAE:          "ALI_SCU",
            ContextID:              "HJTT",
            RISSendingFacility:     ["642"],
            RISSendingApplication:  ["RADIMAGE"],
            HashPrefix:             null),
    ]

    /**
     * Returns the context configuration for the given contextId
     * @return null if the context id isn't known
     */
    public static Context getContext(contextId) {
        // single context case, context id might not be configured
        if (contextId == null && ALL_CONTEXTS.size() == 1) {
            // return the only value:
            return ALL_CONTEXTS.findResult { it.value }
        }

        // If this returns null, we're almost certainly misconfigured
        // so we should blow up.  However, the best way to do that might
        // be to have the caller simply return false to drop the current
        // data, so we have to let the caller handle it.
        return ALL_CONTEXTS[contextId]
    }
}

