<tests service="connect-xds"
       script="FPSXDSResponseFilter"
       file="xds_response_filter.groovy">
    <test name="basic">
        <inputs>
            <list name="studies">
                <ohtMetadata>
                    <tns:DocumentEntry xmlns:tns="urn:org:openhealthtools:ihe:xds:metadata">
                      <eventCode>
                        <code>MR</code>
                        <schemeName>DCM</schemeName>
                      </eventCode>
                      <eventCode>
                        <code>DI</code>
                        <schemeName>Imagerie Québec-DSQ</schemeName>
                      </eventCode>
                      <extension>
                          <name>studyInstanceUID</name>
                          <value>matches</value>
                      </extension>
                    </tns:DocumentEntry>
                </ohtMetadata>
                <ohtMetadata>
                    <tns:DocumentEntry xmlns:tns="urn:org:openhealthtools:ihe:xds:metadata">
                      <eventCode>
                        <code>MR</code>
                        <schemeName>DCM</schemeName>
                      </eventCode>
                      <eventCode>
                        <code>TH</code>
                        <schemeName>Imagerie Québec-DSQ</schemeName>
                      </eventCode>
                      <extension>
                          <name>studyInstanceUID</name>
                          <value>doesn't match</value>
                      </extension>
                    </tns:DocumentEntry>
                </ohtMetadata>
            </list>
            <hl7 name="order">
                MSH|^~\&amp;|RAD.BGH^^|BGH|||201108020914||ORM^O01|PACS-OR/RR.1.3288623|P|2.3|||AL|NE
                PID|1|H0137197|H97136|MOUSE^MICKEY^^^^||19490415|M|||
                PV1|1|I|
                ORC|NW||||U|N|||201108020914|||||||
                OBR|1|001667984BGH||||201108020914|20110802||||||||^^^AB|||||||||MR
            </hl7>
        </inputs>
        <assertions>
            assert returned instanceof Collection
            assert returned.size() == 1
            assert returned[0].get(XDSExtendedMetadata("studyInstanceUID")) == ["matches"]
        </assertions>
    </test>

    <!-- NB: many of these may break due to the passage of time.
         The dates need to be bumped when this happens. -->

    <test name="maxAge">
        <!-- Tests against a rule that only applies maxAge and newest -->
        <inputs>
            <list name="studies">
                <ohtMetadata>
                    <tns:DocumentEntry xmlns:tns="urn:org:openhealthtools:ihe:xds:metadata">
                      <eventCode>
                        <code>MR</code>
                        <schemeName>DCM</schemeName>
                      </eventCode>
                      <eventCode>
                        <code>AB</code>
                        <schemeName>Imagerie Québec-DSQ</schemeName>
                      </eventCode>
                      <extension>
                          <name>studyInstanceUID</name>
                          <value>matches</value>
                      </extension>
                      <!-- well in the future: -->
                      <serviceStartTime>20200101</serviceStartTime>
                    </tns:DocumentEntry>
                </ohtMetadata>
                <ohtMetadata>
                    <tns:DocumentEntry xmlns:tns="urn:org:openhealthtools:ihe:xds:metadata">
                      <eventCode>
                        <code>MR</code>
                        <schemeName>DCM</schemeName>
                      </eventCode>
                      <eventCode>
                        <code>AB</code>
                        <schemeName>Imagerie Québec-DSQ</schemeName>
                      </eventCode>
                      <extension>
                          <name>studyInstanceUID</name>
                          <value>too old</value>
                      </extension>
                      <!-- well in the past: -->
                      <serviceStartTime>20000101</serviceStartTime>
                    </tns:DocumentEntry>
                </ohtMetadata>
            </list>
            <hl7 name="order">
                MSH|^~\&amp;|RAD.BGH^^|BGH|||201108020914||ORM^O01|PACS-OR/RR.1.3288623|P|2.3|||AL|NE
                PID|1|H0137197|H97136|MOUSE^MICKEY^^^^||19490415|M|||
                PV1|1|I|
                ORC|NW||||U|N|||201108020914|||||||
                OBR|1|001667984BGH||||201108020914|20110802||||||||^^^AB|||||||||MR
            </hl7>
        </inputs>
        <assertions>
            assert returned instanceof Collection
            assert returned.size() == 1
            assert returned[0].get(XDSExtendedMetadata("studyInstanceUID")) == ["matches"]
        </assertions>
    </test>

    <test name="newest">
        <!-- Tests against a rule that only applies maxAge and newest -->
        <inputs>
            <list name="studies">
                <ohtMetadata>
                    <tns:DocumentEntry xmlns:tns="urn:org:openhealthtools:ihe:xds:metadata">
                      <eventCode>
                        <code>MR</code>
                        <schemeName>DCM</schemeName>
                      </eventCode>
                      <eventCode>
                        <code>AB</code>
                        <schemeName>Imagerie Québec-DSQ</schemeName>
                      </eventCode>
                      <extension>
                          <name>studyInstanceUID</name>
                          <value>1</value>
                      </extension>
                      <serviceStartTime>20200101</serviceStartTime>
                    </tns:DocumentEntry>
                </ohtMetadata>
                <ohtMetadata>
                    <tns:DocumentEntry xmlns:tns="urn:org:openhealthtools:ihe:xds:metadata">
                      <eventCode>
                        <code>MR</code>
                        <schemeName>DCM</schemeName>
                      </eventCode>
                      <eventCode>
                        <code>AB</code>
                        <schemeName>Imagerie Québec-DSQ</schemeName>
                      </eventCode>
                      <extension>
                          <name>studyInstanceUID</name>
                          <value>2</value>
                      </extension>
                      <serviceStartTime>20200102</serviceStartTime>
                    </tns:DocumentEntry>
                </ohtMetadata>
                <ohtMetadata>
                    <tns:DocumentEntry xmlns:tns="urn:org:openhealthtools:ihe:xds:metadata">
                      <eventCode>
                        <code>MR</code>
                        <schemeName>DCM</schemeName>
                      </eventCode>
                      <eventCode>
                        <code>AB</code>
                        <schemeName>Imagerie Québec-DSQ</schemeName>
                      </eventCode>
                      <extension>
                          <name>studyInstanceUID</name>
                          <value>3</value>
                      </extension>
                      <serviceStartTime>20200103</serviceStartTime>
                    </tns:DocumentEntry>
                </ohtMetadata>
                <ohtMetadata>
                    <tns:DocumentEntry xmlns:tns="urn:org:openhealthtools:ihe:xds:metadata">
                      <eventCode>
                        <code>MR</code>
                        <schemeName>DCM</schemeName>
                      </eventCode>
                      <eventCode>
                        <code>AB</code>
                        <schemeName>Imagerie Québec-DSQ</schemeName>
                      </eventCode>
                      <extension>
                          <name>studyInstanceUID</name>
                          <value>4</value>
                      </extension>
                      <serviceStartTime>20200104</serviceStartTime>
                    </tns:DocumentEntry>
                </ohtMetadata>
            </list>
            <hl7 name="order">
                MSH|^~\&amp;|RAD.BGH^^|BGH|||201108020914||ORM^O01|PACS-OR/RR.1.3288623|P|2.3|||AL|NE
                PID|1|H0137197|H97136|MOUSE^MICKEY^^^^||19490415|M|||
                PV1|1|I|
                ORC|NW||||U|N|||201108020914|||||||
                OBR|1|001667984BGH||||201108020914|20110802||||||||^^^AB|||||||||MR
            </hl7>
        </inputs>
        <assertions>
            assert returned instanceof Collection
            assert returned.collect({ it.get(XDSExtendedMetadata("studyInstanceUID"))[0] }) as Set == ["2", "3", "4"] as Set
        </assertions>
    </test>

    <test name="oldest">
        <!-- Uses a rule that has newest and oldest, but all priors are older than the maxAge on the newest -->
        <inputs>
            <list name="studies">
                <ohtMetadata>
                    <tns:DocumentEntry xmlns:tns="urn:org:openhealthtools:ihe:xds:metadata">
                      <eventCode>
                        <code>MR</code>
                        <schemeName>DCM</schemeName>
                      </eventCode>
                      <eventCode>
                        <code>DI</code>
                        <schemeName>Imagerie Québec-DSQ</schemeName>
                      </eventCode>
                      <extension>
                          <name>studyInstanceUID</name>
                          <value>1</value>
                      </extension>
                      <serviceStartTime>20000101</serviceStartTime>
                    </tns:DocumentEntry>
                </ohtMetadata>
                <ohtMetadata>
                    <tns:DocumentEntry xmlns:tns="urn:org:openhealthtools:ihe:xds:metadata">
                      <eventCode>
                        <code>MR</code>
                        <schemeName>DCM</schemeName>
                      </eventCode>
                      <eventCode>
                        <code>DI</code>
                        <schemeName>Imagerie Québec-DSQ</schemeName>
                      </eventCode>
                      <extension>
                          <name>studyInstanceUID</name>
                          <value>2</value>
                      </extension>
                      <serviceStartTime>20000102</serviceStartTime>
                    </tns:DocumentEntry>
                </ohtMetadata>
            </list>
            <hl7 name="order">
                MSH|^~\&amp;|RAD.BGH^^|BGH|||201108020914||ORM^O01|PACS-OR/RR.1.3288623|P|2.3|||AL|NE
                PID|1|H0137197|H97136|MOUSE^MICKEY^^^^||19490415|M|||
                PV1|1|I|
                ORC|NW||||U|N|||201108020914|||||||
                OBR|1|001667984BGH||||201108020914|20110802||||||||^^^AB|||||||||MR
            </hl7>
        </inputs>
        <assertions>
            assert returned instanceof Collection
            assert returned.collect({ it.get(XDSExtendedMetadata("studyInstanceUID"))[0] }) == ["1"]
        </assertions>
    </test>

    <test name="oldestAndNewest">
        <inputs>
            <list name="studies">
                <ohtMetadata>
                    <tns:DocumentEntry xmlns:tns="urn:org:openhealthtools:ihe:xds:metadata">
                      <eventCode>
                        <code>MR</code>
                        <schemeName>DCM</schemeName>
                      </eventCode>
                      <eventCode>
                        <code>DI</code>
                        <schemeName>Imagerie Québec-DSQ</schemeName>
                      </eventCode>
                      <extension>
                          <name>studyInstanceUID</name>
                          <value>1</value>
                      </extension>
                      <serviceStartTime>20200101</serviceStartTime>
                    </tns:DocumentEntry>
                </ohtMetadata>
                <ohtMetadata>
                    <tns:DocumentEntry xmlns:tns="urn:org:openhealthtools:ihe:xds:metadata">
                      <eventCode>
                        <code>MR</code>
                        <schemeName>DCM</schemeName>
                      </eventCode>
                      <eventCode>
                        <code>DI</code>
                        <schemeName>Imagerie Québec-DSQ</schemeName>
                      </eventCode>
                      <extension>
                          <name>studyInstanceUID</name>
                          <value>2</value>
                      </extension>
                      <serviceStartTime>20200102</serviceStartTime>
                    </tns:DocumentEntry>
                </ohtMetadata>
                <ohtMetadata>
                    <tns:DocumentEntry xmlns:tns="urn:org:openhealthtools:ihe:xds:metadata">
                      <eventCode>
                        <code>MR</code>
                        <schemeName>DCM</schemeName>
                      </eventCode>
                      <eventCode>
                        <code>DI</code>
                        <schemeName>Imagerie Québec-DSQ</schemeName>
                      </eventCode>
                      <extension>
                          <name>studyInstanceUID</name>
                          <value>3</value>
                      </extension>
                      <serviceStartTime>20200103</serviceStartTime>
                    </tns:DocumentEntry>
                </ohtMetadata>
                <ohtMetadata>
                    <tns:DocumentEntry xmlns:tns="urn:org:openhealthtools:ihe:xds:metadata">
                      <eventCode>
                        <code>MR</code>
                        <schemeName>DCM</schemeName>
                      </eventCode>
                      <eventCode>
                        <code>DI</code>
                        <schemeName>Imagerie Québec-DSQ</schemeName>
                      </eventCode>
                      <extension>
                          <name>studyInstanceUID</name>
                          <value>4</value>
                      </extension>
                      <serviceStartTime>20200104</serviceStartTime>
                    </tns:DocumentEntry>
                </ohtMetadata>
                <ohtMetadata>
                    <tns:DocumentEntry xmlns:tns="urn:org:openhealthtools:ihe:xds:metadata">
                      <eventCode>
                        <code>MR</code>
                        <schemeName>DCM</schemeName>
                      </eventCode>
                      <eventCode>
                        <code>DI</code>
                        <schemeName>Imagerie Québec-DSQ</schemeName>
                      </eventCode>
                      <extension>
                          <name>studyInstanceUID</name>
                          <value>5</value>
                      </extension>
                      <serviceStartTime>20200105</serviceStartTime>
                    </tns:DocumentEntry>
                </ohtMetadata>
                <ohtMetadata>
                    <tns:DocumentEntry xmlns:tns="urn:org:openhealthtools:ihe:xds:metadata">
                      <eventCode>
                        <code>MR</code>
                        <schemeName>DCM</schemeName>
                      </eventCode>
                      <eventCode>
                        <code>DI</code>
                        <schemeName>Imagerie Québec-DSQ</schemeName>
                      </eventCode>
                      <extension>
                          <name>studyInstanceUID</name>
                          <value>6</value>
                      </extension>
                      <serviceStartTime>20200106</serviceStartTime>
                    </tns:DocumentEntry>
                </ohtMetadata>
            </list>
            <hl7 name="order">
                MSH|^~\&amp;|RAD.BGH^^|BGH|||201108020914||ORM^O01|PACS-OR/RR.1.3288623|P|2.3|||AL|NE
                PID|1|H0137197|H97136|MOUSE^MICKEY^^^^||19490415|M|||
                PV1|1|I|
                ORC|NW||||U|N|||201108020914|||||||
                OBR|1|001667984BGH||||201108020914|20110802||||||||^^^AB|||||||||MR
            </hl7>
        </inputs>
        <assertions>
            assert returned instanceof Collection
            assert returned.collect({ it.get(XDSExtendedMetadata("studyInstanceUID"))[0] }) as Set ==
                ["1", "3", "4", "5", "6"] as Set
        </assertions>
    </test>

    <test name="normalizedModalityInMetadata">
        <inputs>
            <list name="studies">
                <ohtMetadata>
                    <tns:DocumentEntry xmlns:tns="urn:org:openhealthtools:ihe:xds:metadata">
                      <eventCode>
                        <code>DX</code>
                        <schemeName>DCM</schemeName>
                      </eventCode>
                      <eventCode>
                        <code>TC</code>
                        <schemeName>Imagerie Québec-DSQ</schemeName>
                      </eventCode>
                      <extension>
                          <name>studyInstanceUID</name>
                          <value>matches</value>
                      </extension>
                    </tns:DocumentEntry>
                </ohtMetadata>
                <ohtMetadata>
                    <tns:DocumentEntry xmlns:tns="urn:org:openhealthtools:ihe:xds:metadata">
                      <eventCode>
                        <code>MR</code>
                        <schemeName>DCM</schemeName>
                      </eventCode>
                      <eventCode>
                        <code>TH</code>
                        <schemeName>Imagerie Québec-DSQ</schemeName>
                      </eventCode>
                      <extension>
                          <name>studyInstanceUID</name>
                          <value>doesn't match</value>
                      </extension>
                    </tns:DocumentEntry>
                </ohtMetadata>
            </list>
            <hl7 name="order">
                MSH|^~\&amp;|RAD.BGH^^|BGH|||201108020914||ORM^O01|PACS-OR/RR.1.3288623|P|2.3|||AL|NE
                PID|1|H0137197|H97136|MOUSE^MICKEY^^^^||19490415|M|||
                PV1|1|I|
                ORC|NW||||U|N|||201108020914|||||||
                OBR|1|001667984BGH||||201108020914|20110802||||||||^^^TC|||||||||CR
            </hl7>
        </inputs>
        <assertions>
            assert returned instanceof Collection
            assert returned.size() == 1
            assert returned[0].get(XDSExtendedMetadata("studyInstanceUID")) == ["matches"]
        </assertions>
    </test>

    <test name="normalizedModalityInOrder">
        <inputs>
            <list name="studies">
                <ohtMetadata>
                    <tns:DocumentEntry xmlns:tns="urn:org:openhealthtools:ihe:xds:metadata">
                      <eventCode>
                        <code>CR</code>
                        <schemeName>DCM</schemeName>
                      </eventCode>
                      <eventCode>
                        <code>TC</code>
                        <schemeName>Imagerie Québec-DSQ</schemeName>
                      </eventCode>
                      <extension>
                          <name>studyInstanceUID</name>
                          <value>matches</value>
                      </extension>
                    </tns:DocumentEntry>
                </ohtMetadata>
                <ohtMetadata>
                    <tns:DocumentEntry xmlns:tns="urn:org:openhealthtools:ihe:xds:metadata">
                      <eventCode>
                        <code>MR</code>
                        <schemeName>DCM</schemeName>
                      </eventCode>
                      <eventCode>
                        <code>TH</code>
                        <schemeName>Imagerie Québec-DSQ</schemeName>
                      </eventCode>
                      <extension>
                          <name>studyInstanceUID</name>
                          <value>doesn't match</value>
                      </extension>
                    </tns:DocumentEntry>
                </ohtMetadata>
            </list>
            <hl7 name="order">
                MSH|^~\&amp;|RAD.BGH^^|BGH|||201108020914||ORM^O01|PACS-OR/RR.1.3288623|P|2.3|||AL|NE
                PID|1|H0137197|H97136|MOUSE^MICKEY^^^^||19490415|M|||
                PV1|1|I|
                ORC|NW||||U|N|||201108020914|||||||
                OBR|1|001667984BGH||||201108020914|20110802||||||||^^^TC|||||||||DX
            </hl7>
        </inputs>
        <assertions>
            assert returned instanceof Collection
            assert returned.size() == 1
            assert returned[0].get(XDSExtendedMetadata("studyInstanceUID")) == ["matches"]
        </assertions>
    </test>
</tests>
