/* Foreign Image Localizer
 *
 * Shared in both the Ad-Hoc Query and Fetch Prior Studies workflow
 *
 * Runs once per incoming SOP and either drops it or localizes it.
 */

LOAD("Shared_Scripting_Files/shared_constants.groovy")
LOAD("Shared_Scripting_Files/contexts.groovy")
LOAD("Shared_Scripting_Files/common_localization_routines.groovy")
LOAD("Shared_Scripting_Files/shared_xds_modality_and_anatomic_region_routines.groovy")
LOAD("Shared_Scripting_Files/procedure_codes_rules_and_logic.groovy")


log.debug("This is the incoming raw DICOM header from the DI-r: {}", input)

context = Contexts.getContext(config.context)
if (context == null) {
    log.warn("Context is unknown from id: {}. Assuming this is a foreign study, and passing it through. " +
        "Please correct either the connect ContextID configuration or contexts.groovy", config.context)
    return true
}

// Set the SpecificCharacterSet to ensure extended characters (accented, Çç, etc) display correctly
if (input.get(SpecificCharacterSet) == null) {
    input.set(SpecificCharacterSet, "ISO_IR 100")
}


def studyInstanceUID = get(StudyInstanceUID)
def seriesInstanceUID = get(SeriesInstanceUID)
def sopInstanceUID = get(SOPInstanceUID)


// localization variables
def sourcePid = get(PatientID)
// Protect sourceIssuer from being null if images come directly from the DI-r
def sourceIssuer = get(IssuerOfPatientID)
def localPidAtPACS = Pids.localize([sourcePid, sourceIssuer], getAllPidsQualified(), context.PACSIssuer)
def sopModality = get(Modality)


log.debug("\n*** Starting Foreign Image LOCALIZER Script *** :\n")
log.debug("FOREIGN Accession Number is: '{}'.", get(AccessionNumber))
log.debug("Processing SOP '{}' for Study Instance UID '{}'.", sopInstanceUID, studyInstanceUID)
log.debug("Source pid in C-Move response from the DIR before morphing: {}", sourcePid)
log.debug("Source Issuer of PID in C-Move response from the DIR before morphing: {}", sourceIssuer)
log.debug("Setting local or prefixed pid in CStore: {}", localPidAtPACS[0])
log.debug("This is the current content of the 'Other Patient IDs' Sequence: {}", getAllPidsQualified())
log.trace("This is the full object as received by the script:\n{}", input)


/* As a fail-safe, ignore local studies at the SOP Level */
if (sourceIssuer == context.PACSIssuer) {
    log.info("This is a local SOP for a local Study. Not moving to PACS. " +
             "Accession Number '{}', Study Instance UID '{}', SOP Instance UID '{}'",
             get(AccessionNumber), studyInstanceUID, sopInstanceUID)
    return false
}
 

/* Sink KO SOPs */
if (sopModality == "KO") {
    log.info("Dropping a KO SOP")
    return false
}


// set the Patient ID in the Image (SOP) header to the local or prefixed foreign PID
set(PatientID, localPidAtPACS[0])
// the PACS normally doesn't need the IssuerOfPatientID field populated for local patients; foreign patients are prefixed
remove(IssuerOfPatientID)


log.debug("Attempting to localize patient demographics with:\n{}", localDemographics)
if (localDemographics != null) {
    // The PACS may not support context filtering on cfinds.  In this case, we have asked for
    // the Issuer and will look for an appropriate record:
    contextDemographics = localDemographics.findAll {
        it.get(IssuerOfPatientID) == context.ContextID
    }

    if (contextDemographics.size() > 1) {
        log.error("Multiple ({}) local demographics results for {} / {}. " +
                  "Unable to safely localize so we'll skip this step.",
            contextDemographics.size(), localPidAtPACS[0], context.ContextID)

    } else if (contextDemographics.isEmpty()) {
        log.debug("No known local demographics.")

    } else {
        demographics = contextDemographics[0]

        log.debug("Localizing using demographics:\n{}", demographics)

        def localPatientBirthDate = demographics.get(PatientBirthDate)
        def dirPatientBirthDate = get(PatientBirthDate)

        if (localPatientBirthDate != null && !localPatientBirthDate.isEmpty() &&
            dirPatientBirthDate != null && !dirPatientBirthDate.isEmpty() &&
            localPatientBirthDate != dirPatientBirthDate) {

            log.warn("The Date of Birth '{}' for Patient '{}' in the PACS does " +
                     "not match what's in the DIR '{}'. " +
                     "Assuming we have the wrong local demographics so we will not " +
                     "use them.",
                     localPatientBirthDate, localPidAtPACS[0], dirPatientBirthDate);

        } else {
            def localPatientName = demographics.get(PatientName)
            if (localPatientName != null && !localPatientName.isEmpty()) {
                log.debug("Changing Patient Name from '{}' to '{}'",
                    get(PatientName), localPatientName)
                set(PatientName, localPatientName)
            }
        }
    }

} else {
	log.debug("Local demographics from the PACS were null.")
}


/* Transparently hash Study, Series and SOP Instance UIDs when required if serving a multi-context PACS */
(studyInstanceUID, seriesInstanceUID, sopInstanceUID) = UIDs.localizeUIDs(input, context, hash, log)


/* Prefix Foreign Accession Numbers    */
if (sourceIssuer == null || sourceIssuer.isEmpty()) {
    log.warn("Empty Assigning Authority (IssuerOfPatientID) '{}' at DIR. " +
             "The Accession Number will be prefixed as Foreign and unexpected", sourceIssuer) 
}

def prefix = Prefixes.getPrefix(sourceIssuer, context.PACSIssuer)
if (prefix == "AU" || prefix == "ZZ") {
    log.warn("Unknown Site OID Mapping for '{}'. The prefixing will not be what is expected!", sourceIssuer)
} else {
    log.debug("Current prefix to prepend to Accession Number is: '{}'", prefix)
}

prepend(AccessionNumber, prefix) 

if (get(AccessionNumber).size() > 16) {
    log.warn("The prefixed accession number '{}' for Study '{}', SOP '{}' exceeds " +
             "16 characters. Undefined behaviour might be seen at the PACS",
             get(AccessionNumber), studyInstanceUID, sopInstanceUID)
}
log.debug("Prefixed Accession Number will be '{}'.", get(AccessionNumber))


/* Prefix Study IDs */
prepend(StudyID, prefix)


/* Populate Standardized Procedure Codes from Modalities, Anatomic Regions and Foreign Procedure Descriptions.
   XDS metadata might not be known for a few reasons:
   - unsolicited stores
   - studies found in home dir, but not in registry
   - studies found by direct query to a dir based on foreign patient id (emergency import)
*/
if (xdsMetadata != null) {
    log.debug("\n**Starting Procedure Codes mapping for this SOP****\n")

	def eventCodes = xdsMetadata.get(XDSEventCodes)
    log.trace("Current Event Codes for this SOP are: ", eventCodes)

	// Determine the Anatomic Region from the XDS Event Codes
	anatomicRegion = Modalities.determineAnatomicRegion(eventCodes, SharedConstants.XDS_Coding_Scheme_for_Anatomic_Region) 
	log.debug("Current Anatomic Region in SOP is: {}", anatomicRegion)

	// Determine the Dominant Modality for this Study using the Hierarchical List and the XDS Event Codes
	dominantModality = Modalities.determineDominantModality(eventCodes, SharedConstants.XDS_Coding_Scheme_for_Modality)

	if (dominantModality == "") {
	    def xdsModalities =  Modalities.getCodeValues(eventCodes, SharedConstants.XDS_Coding_Scheme_for_Modality)
	    log.debug("No dominant modality found. Checking the modalitities in the XDS Registry for this Study: {}", xdsModalities)

	    if (xdsModalities.isEmpty()) {
            dominantModality = get(Modality)
            log.warn("Unknown XDS Study Modalities for Study Instance UID '{}', SOP Instance UID '{}'. " +
                "Fail-safing to Current SOP Modality in DICOM: '{}'",
                studyInstanceUID, sopInstanceUID, dominantModality)

	    } else {
            dominantModality = xdsModalities[0]
            log.warn("No dominant modality could be found for Study Instance UID '{}', SOP Instance UID '{}'" +
                "Falling back to first published modality '{}'",
                studyInstanceUID, sopInstanceUID, dominantModality)
	    }
	}

	log.debug("After the Modality Checking Logic, Dominant Modality for this Study is '{}'", dominantModality)


	// Get the study description from XDS, otherwise from DICOM
	studyDescription = xdsMetadata.get(XDSTitle)
	log.debug("Current Study Description from XDS Metadata is: {}", studyDescription)

	// If no valid data available in the XDS Registry, get values from DICOM
	if (studyDescription == null || studyDescription.isEmpty()) {
	    studyDescription = get(StudyDescription) 
	    log.debug("No value found in the XDS Metadata. Using StudyDescription from DICOM header: '{}'",
		      studyDescription)
	}

	// If the studyDescription is not found in either the XDS Registry or the DICOM header,
	// set the studyDescription to empty string so as not to pass garbage to the rule-evaluating logic
	if (studyDescription == null) {

	    studyDescription = ""

	} else {
	    // The rule evaluation mechanism needs the studyDescription in lower case
	    studyDescription = studyDescription.toLowerCase()
	}

    log.debug("Checking for priors rules using modality: '{}', anatomic region: '{}' and study description: '{}'",
              dominantModality, anatomicRegion, studyDescription)

    /* procedureCodeMappingRule[3] is the Procedure Code; 
     * procedureCodeMappingRule[4] is the Procedure Description */
    def procedureCodeMappingRule = Rules.findMatchingRule(dominantModality, anatomicRegion, studyDescription)

    log.debug("Found procedure code mapping rule: {}", procedureCodeMappingRule)

    def normalizedProcedureCode = null
    def normalizedProcedureDescription = null

    /* In Prod, we would use the normalizedProcedureCode as StudyDescription */
    if (procedureCodeMappingRule != null) { 
        normalizedProcedureCode = procedureCodeMappingRule[3]
        normalizedProcedureDescription = procedureCodeMappingRule[4]

        // Populate the Normalized Procedure Code and Procedure Description; the PACS needs it here

        /* DEMO Scenario 1: 
         * a) Study Description (0008,1030) to use normalized DSQ codes
         * b) Procedure Code Sequence i.e. "Code d'examen" not to be substituted at all
         * As described in "3.2 Règles de coercition des descriptions d’étude avant l’envoi au PACS" */
        /*set(StudyDescription, normalizedProcedureCode)
        log.debug("Setting the Procedure Code in the outgoing SOP to PACS to: {}", normalizedProcedureCode)*/

    } else {
        log.warn("NORMALIZED Procedure Code and / or Procedure Code Description are blank. " +
                 "NOT passing procedure code values to the PACS. " +
                 "Study Instance UID: {}, SOP Instance UID: {}", 
                 studyInstanceUID, sopInstanceUID)
    }
    
    /* DEMO Scenario 2:
    * a) Study Description should be created by Concatenating the "HL7" (XDS) Modality and Ananatomic Region
    * b) Procedure Code should be created in the same way
    * Described in "Instructions_study_description_and_exam_number_coercion.txt" */
    // Turning off for now
    // Set Study Description
    set(StudyDescription, dominantModality + anatomicRegion)
    log.debug("Setting the Study Description in the outgoing SOP to PACS to: {}", StudyDescription)


    // Set Procedure Code
    set("ProcedureCodeSequence/CodeValue", dominantModality + anatomicRegion)
    log.debug("Setting the Code Value of the Procedure Code Sequence in the outgoing SOP to PACS to: {}", dominantModality + anatomicRegion)
    set("ProcedureCodeSequence/CodeMeaning", "")
    set("ProcedureCodeSequence/CodingSchemeDesignator", "")


    /* This logic is to prime the DICOM SR to ORU conversion */
    if (sopModality == "SR") {
        // This is only relevant to build ORUs out of SR Reports.
        // The fields that we are using to pass the original Procedure Code and Procedure Description are 
        // totally arbitrary.  The only prerequisite is that they are NOT used to construct the ORU
        // Make sure that you pick these arbitrary fields up in the ORU conversion
        set(AccessoryCode, get(StudyDescription))
        set(AcquisitionComments, get(StudyDescription))

        set(StudyDescription, normalizedProcedureDescription)
        set(CodeValue, normalizedProcedureCode)
    }

    /* Prime the Anatomic Region to be used in Order Creation */
    if (anatomicRegion != null || !anatomicRegion.isEmpty()) {
        set("AnatomicRegionSequence/CodeValue", anatomicRegion)
        set("AnatomicRegionSequence/CodeMeaning", "")
        set("AnatomicRegionSequence/CodingSchemeDesignator", "Imagerie Québec-DSQ")
    }
}


/*  Populate Institution Name according to the Assigning Authority (Issuer of Patient ID) of the Source PID */
def inferredInstitutionName = Prefixes.getInstitutionName(sourceIssuer)

if (inferredInstitutionName != "Institution Inconnue") {
    // We found a known InstitutionName
    // Unfortunately, this value is very dependent on what's on the Prefixes class.
    // If that changes, it will break this if statement and its code
   
    set(InstitutionName, inferredInstitutionName)
    log.debug("Setting Institution Name from Rialto's table as '{}'", inferredInstitutionName)

} else {
     // Fall back on the DICOM value of IssuerOfPatientID if we don't know the institution name
     log.warn("We did not know the insitution name for Study '{}', SOP '{}'.  Falling back on the " +
              "original value at the DIR: '{}'", studyInstanceUID, sopInstanceUID, get(InstitutionName))
}



/* If the actual SOP modality is one of the "forbidden" modalities, substitute to an expected modality */
def subbedSopModality = Modalities.secondaryToPrimaryModalitiesMap[sopModality]
def reportedModality = sopModality

if (subbedSopModality != "") {
    set(Modality, subbedSopModality)
    reportedModality = subbedSopModality
    if(sopModality != subbedSopModality) {
	    log.info("Changed SOP modality from original unwanted modality of '{}' to substituted modality of '{}'",
	     sopModality, subbedSopModality)
    }
}


/*  Outputting Information on the SOP being moved to the PACS */
log.info("Preparing to move localized SOP to PACS.  Accession Number '{}', Patient ID '{}', Foreign Institution '{}', " +
         "Study Instance UID '{}', SOP Instance UID '{}', SOP Modality '{}'",
         get(AccessionNumber), localPidAtPACS[0], inferredInstitutionName, studyInstanceUID, sopInstanceUID, reportedModality)

log.debug("Image (SOP) headers in outgoing C-Store to Local PACS (after morphing):\n{}", output)
log.debug("*** Ending Foreign Image LOCALIZER Script *** :\n")

