/* pix_morpher.groovy
 *
 * This script Handles different types of HL7 Messages to Populate the PIX Database that Rialto Connect uses 
 * for RAMQ to Local Patient ID resolution (and viceversa)
 */

LOAD("Shared_Scripting_Files/shared_constants.groovy")
LOAD("Shared_Scripting_Files/shared_hl7_resources.groovy")
LOAD("Shared_Scripting_Files/contexts.groovy")

log.trace("Starting PIX morpher")
log.trace("Message as received:\n{}", input)

context = Contexts.getContext(config.context)
if (context == null) {
    log.error("Context is unknown from id: {}. Unable to localize this message, dropping it. " +
        "Please correct either the connect ContextID configuration or contexts.groovy", config.context)
    return false
}

def messageCode = get('MSH-9-1')
def triggerEvent = get('MSH-9-2')
log.trace("Local ADT Forwarder: Received message type: '{}^{}'", messageCode, triggerEvent)

// Only allow ADT subtypes that the PIX morpher can handle
if (messageCode == "ADT") {
    log.debug("True ADT received from RIS Feed")

    if (!SharedHl7.supported_ADT_subtypes.contains(triggerEvent)) {
        // TODO: could we just change it to A31 like we do for ORMs below?
        log.debug("Unsupported ADT subtype found - ignoring.")
        return false
    }
} else {
    // Change ORMs and other Message Types to ADT^A31.
    // This should be safe because we validate that the required PID segment data
    // is available, regardless of message type.
    set('MSH-9-1', "ADT")
    set('MSH-9-2', "A31")
}

// Add the default Character set that can handle French Characters, which is often not explicit in the RIS Feed
def characterEncoding = get("MSH-18")
if (characterEncoding == null || characterEncoding.isEmpty()){
    log.trace("No character set specified.  Hard-coding to 8859/1")
    set('MSH-18', '8859/1')
}

// The pix database requires valid and fully qualified local and affinity domain pids.
// Anything else needs to be dropped.

// Validate pids:
def mrn = get(SharedConstants.HL7_Local_MRN_PID_Field)
log.trace("Current MRN received is: [{}]", mrn)
def affinityPID = get(SharedConstants.HL7_Affinity_Domain_PID_Field)
log.trace("Current RAMQ received is: [{}]", affinityPID)

if (mrn == null || mrn.isEmpty()) {
    log.trace("Received Message DOES NOT contain a local MRN.  Ignoring this HL7 Message")
    return false

} else if (affinityPID == null || affinityPID.isEmpty()) {
    log.trace("Received Message DOES NOT contain an Affinity Domain.  Ignoring this HL7 Message")
    return false

} else if (QuebecHL7Validation.RAMQformatIsInvalid(affinityPID)) {
    log.warn("Received an Invalid RAMQ '{}' for an HL7 message type '{}'. " +
              "Not populating internal PIX from this message.", affinityPID, messageCode)
    return false

} else {
    mrn = mrn.toUpperCase()
    set(SharedConstants.HL7_Local_MRN_PID_Field, mrn)
    log.trace("Using Local MRN '{}'", mrn)

    affinityPID = affinityPID.toUpperCase()
    set(SharedConstants.HL7_Affinity_Domain_PID_Field, affinityPID)
    log.trace("Using Affinity Domain PID (RAMQ) '{}'", affinityPID)
}


// Validate domains:
def mrnDomain = get(SharedConstants.HL7_Local_MRN_Issuer_of_PID_Field)
log.trace("Issuer of Patient ID for MRN as received is: '{}'", mrnDomain)
def affinityDomain = get(SharedConstants.HL7_Affinity_Domain_Issuer_of_PID_Field)
log.trace("Issuer of Patient ID for Affinity Domain PID (RAMQ) as received is: '{}'", affinityDomain)

if (mrnDomain != null && !mrnDomain.isEmpty()) {
    if (context.PACSIssuer != mrnDomain.toUpperCase()) {
        // This means that the Issuer of PID passed by RIS does not match the one known as correct
        // THIS ONLY WORKS IN Connect Instances attached to SINGLE-CONTEXT PACS
        log.warn("The received Issuer of Patient ID for the local PACS received was '{}'.  The expected value was '{}'." +
            "This RIS Feed is sending feeds for more than 1 local domain. Patient Lookups might fail and go to RIS Database! " +
            "This could be Potentially Dangerous", mrnDomain, context.PACSIssuer)

        // TODO for right now, to hack the messages for testing with dubious data
        mrnDomain = context.PACSIssuer

    } else {
        log.trace("The Issuer of PID correctly matches expected value.")
    }

} else {
    // Issuer of Patient ID for local MRN is null (expected on some RIS Feeds)
    mrnDomain = context.PACSIssuer
    log.trace("Populating Issuer of Patient ID for MRN from Local PACS as '{}'", mrnDomain)
}

if (affinityDomain != null && !affinityDomain.isEmpty()) {
    if (SharedConstants.Affinity_Domain_OID != affinityDomain.toUpperCase()) {  
        // i.e. RAMQ Issuer Of PID is present, but not correct
        log.warn("Invalid Affinity Domain (RAMQ) Issuer of Patient ID '{}'. Populating to '{}'",
                 affinityDomain, SharedConstants.Affinity_Domain_OID)
        // We can safely hard-code this value here
        // TODO: how is this safe? could this not cause us to link to the wrong patient?
        affinityDomain = SharedConstants.Affinity_Domain_OID 

    } else {
        log.trace("The Affinity Domain Issuer of Patient ID is what is expected")
    }

} else {
    // Affinity Domain Issuer of PID is blank
    affinityDomain = SharedConstants.Affinity_Domain_OID
    log.trace("Populating Issuer of Patient ID for AffinityDomain Patient (RAMQ) as '{}'", affinityDomain)
}

set('PID-3-4-2', mrnDomain)
set('PID-2-4-2', affinityDomain)

set('PID-4', null)

log.trace("This is the full Patient Info being passed to the PIX handler: MRN: '{}', MRN Issuer '{}', " +
            "RAMQ: '{}', RAMQ Issuer '{}'.", mrn, mrnDomain, affinityPID, affinityDomain)

log.trace("This is the message that will be passed to the PIX handler (ADT Handler):\n{}", output)

log.trace("Ending the PIX morpher")
