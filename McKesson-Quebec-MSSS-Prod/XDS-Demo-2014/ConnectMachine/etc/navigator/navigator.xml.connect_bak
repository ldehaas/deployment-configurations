<config>
    <var name="System-AffinityDomain" value="EMPI_CROSS_AFFINITY_DOMAIN_ID" />
    <var name="System-DefaultLocalDomain" value="mobile.army" />

    <!-- DATABASE URLs: "DatabaseURL" parameter name is used by the navigator-security script, do not change the parameter name  -->
    <var name="DatabaseURL" value="jdbc:postgresql://localhost/navigatordb?user=rialto"/>

    <!-- NAVIGATOR VARIABLES -->
    <var name="Navigator-Host" value="localhost"/>
    <var name="Navigator-HostProtocol" value="https" />
    <var name="Navigator-GUIPort" value="2525" />
    <var name="Navigator-APIPort" value="2524" />
    <var name="Navigator-APIBasePath" value="/navigator" />
    <var name="Navigator-GUIBasePath" value="/" />
    <var name="Navigator-DefaultLocale" value="en" />
    <var name="Navigator-DefaultTheme" value="rialto-light" />
    <var name="Navigator-MaxPatientSearchResults" value="1000" />
    <var name="Navigator-MaxDocumentSearchResults" value="1000" />

    <!-- USERMANAGEMENT VARIABLES -->
    <var name="Usermanagement-APIBasePath" value="/usermanagement" />
    <var name="Usermanagement-URL" value="${Navigator-HostProtocol}://${Navigator-Host}:${Navigator-APIPort}${Navigator-APIBasePath}" />
    <var name="Usermanagement-AuthenticatedURL" value="${Navigator-HostProtocol}://${Navigator-Host}:${Navigator-APIPort}${Navigator-APIBasePath}/api${Usermanagement-APIBasePath}" />

    <!-- CLUSTER CONFIGURATION - not needed by default -->
    <!-- Single Sign On in a Cluster -->
    <!--var name="SSO-Cluster-Bind-IP" value="127.0.0.1"/>
    <var name="SSO-Cluster-Multicast-IP" value="232.10.10.201"/>
    <var name="SSO-Cluster-Multicast-PORT" value="45201"/-->

    <!-- Navigator Cache Manager in a cluster -->
    <!--var name="Navigator-Cluster-Bind-IP" value="127.0.0.1"/>
    <var name="Navigator-Cluster-Multicast-IP" value="232.10.10.202"/>
    <var name="Navigator-Cluster-Multicast-PORT" value="45202"/-->

    <!-- Navigator Server access controller in a cluster -->
    <!--var name="Navigator-Server-Cluster-Bind-IP" value="127.0.0.1"/>
    <var name="Navigator-Server-Cluster-Multicast-IP" value="232.10.10.203"/>
    <var name="Navigator-Server-Cluster-Multicast-PORT" value="45203"/-->

    <!-- TLS CONFIGURATION -->
    <var name="TLSKeyStore" value="sample.jks" />
    <var name="TLSKeyStorePass" value="password" />
    <var name="TLSKeyPass" value="password" />
    <var name="TLSTrustStore" value="sample.jks" />
    <var name="TLSTrustStorePass" value="password" />
    <var name="TLSClientAuth" value="false" />
    <var name="TLSV2Hello" value="true" />

    <!-- User Sessions -->
    <var name="IdleUserSessionTimeout" value="30m" />

    <server type="http" id="authenticated-http">
        <port>${Navigator-APIPort}</port>
        <tls>
            <keystore>${TLSKeyStore}</keystore>
            <keystorepass>${TLSKeyStorePass}</keystorepass>
            <keypass>${TLSKeyPass}</keypass>
            <truststore>${TLSTrustStore}</truststore>
            <truststorepass>${TLSTrustStorePass}</truststorepass>
            <clientauth>${TLSClientAuth}</clientauth>
            <v2hello>${TLSV2Hello}</v2hello>
        </tls>
    </server>

    <server type="http" id="http">
        <port>${Navigator-GUIPort}</port>
        <tls>
            <keystore>${TLSKeyStore}</keystore>
            <keystorepass>${TLSKeyStorePass}</keystorepass>
            <keypass>${TLSKeyPass}</keypass>
            <truststore>${TLSTrustStore}</truststore>
            <truststorepass>${TLSTrustStorePass}</truststorepass>
            <clientauth>${TLSClientAuth}</clientauth>
            <v2hello>${TLSV2Hello}</v2hello>
        </tls>
    </server>

    <service type="authentication" id="authentication">
        <server idref="authenticated-http" name="web-api">
            <url>${Navigator-APIBasePath}</url>
        </server>

        <config>
            <include location="userauth.xml"/>
        </config>
    </service>

    <service type="usermanagement" id="usermanagement">
        <config>
            <prop name="SystemDefaultPermissions">
                <permission name="rialto.arr" value="false"/>
                <permission name="rialto.administration" value="false"/>
                <permission name="rialto.usermanagement.internalrealm" value="false"/>
            </prop>
            <prop name="web-api-path" value="${Usermanagement-APIBasePath}"/>
            <prop name="DatabaseURL" value="${DatabaseURL}"/>
            <prop name="UserPreferencesMode" value="USER_WITH_DEFAULTS"/>

            <include location="defaultuserprefs.xml"/>
            <include location="userauth.xml"/>
        </config>
    </service>

    <service type="navigator.server" id="navigator.server">
        <device idref="pix" />
        <device idref="pdq" />
        <device idref="xdsrep"/>
        <device idref="xdsreg"/>

        <server idref="http" name="web-ui">
            <url>${Navigator-GUIBasePath}</url>
        </server>

        <server idref="authenticated-http" name="web-api">
            <url>/public/*</url>
        </server>

        <config>
            <prop name="CrossAffinityDomainID" value="${System-AffinityDomain}"/>
            <prop name="DocumentTypeCodes"></prop>
            <prop name="BreakTheGlassOptions"></prop>
            <prop name="SystemDefaultPermissions">
                <permission name="rialto.navigator.patients.search.internal" value="true"/>
                <permission name="rialto.navigator.patients.search.external" value="false"/>
	   	<permission name="rialto.navigator.study.metadata.view.internal" value="true" />
	   	<permission name="rialto.navigator.study.metadata.view.external" value="true" />
	   	<permission name="rialto.navigator.study.move" value="true" />	
		<permission name="rialto.navigator.docs.metadata.view.unknown.internal" value="true"/>
                <permission name="rialto.navigator.docs.metadata.view.unknown.external" value="true"/>
                <permission name="rialto.navigator.docs.metadata.edit.unknown.internal" value="false"/>
                <permission name="rialto.navigator.docs.metadata.edit.unknown.external" value="false"/>
                <permission name="rialto.navigator.docs.view.unknown.internal.always" value="false"/>
                <permission name="rialto.navigator.docs.view.unknown.external.always" value="false"/>
                <permission name="rialto.navigator.docs.view.unknown.internal.onbreakglass" value="true"/>
                <permission name="rialto.navigator.docs.view.unknown.external.onbreakglass" value="true"/>
            </prop>
            <prop name="SupportedThemes">
                rialto-light
                rialto-dark
            </prop>
            <prop name="SupportedLocales">
                        en
                        fr
            </prop>
            <prop name="Plugins"></prop>
            <prop name="GlobalInactivitySessionTimeout" value="${IdleUserSessionTimeout}" />
            <prop name="ConfidentialityCodeSchemaName">TEST</prop>
            <prop name="DatabaseURL" value="${DatabaseURL}"/>
            <prop name="web-api-path" value="${Navigator-APIBasePath}"/>
            <prop name="DefaultTheme" value="${Navigator-DefaultTheme}"/>
            <prop name="DefaultLocale" value="${Navigator-DefaultLocale}"/>

            <!-- Variables used for passing information to the GUI -->
            <prop name="ApiURL" value="${Navigator-HostProtocol}://${Navigator-Host}:${Navigator-APIPort}${Navigator-APIBasePath}/api/navigator"/>
            <prop name="ArrURL" value="${Navigator-HostProtocol}://${Navigator-Host}:${ARR-GUIPort}"/>
            <prop name="UserManagementURL" value="${Usermanagement-URL}"/>
            <prop name="PublicURL" value="${Navigator-HostProtocol}://${Navigator-Host}:${Navigator-APIPort}/public"/>

            <include location="patientidentitydomains.xml"/>
            <include location="cacheStorage.xml"/>
        </config>
    </service>
</config>


