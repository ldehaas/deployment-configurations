/*
 * Test data generator for Connect XDS
 *
 * To run:
 * $ rtkgrv gen.groovy
 */

import org.joda.time.*;
import org.dcm4che2.net.*;
import org.dcm4che2.data.*;
import static org.dcm4che2.data.Tag.*;
import org.dcm4che2.io.*;
import org.dcm4che2.util.*;
import com.karos.rialto.dcm.*;
import com.karos.rialto.platform.dcm.*;
import com.karos.groovy.morph.dcm.*;
import com.karos.rtk.common.*;
import ca.uhn.hl7v2.parser.*;

def RAMQD = "RAMQ&2.16.124.10.101.1.60.100&ISO"

def FOREIGN_MRND_UID = "toronto.lakeshore"
def FOREIGN_MRND = "TL&$FOREIGN_MRND_UID&ISO"

def DIR_AE = "RIALTO"
def DIR_IP = "10.0.1.130"
def DIR_PORT = 4104

def storeDest = new DicomDevice(
        DIR_AE,
        (String) null,      // domain
        DIR_IP,
        DIR_PORT)

def empi = new HL7v2Client(DIR_IP, 2399)
def connectpix = new HL7v2Client("10.0.1.242", 5556)

def SR_TEMPLATE = "./sr.dcm"
def IMG_TEMPLATE = "./img.dcm"

/**
 * Creates a random study with two sops and the given
 * properties.  One sop is an image and the other is an sr.
 *
 * @param overlay map of properties to override from the template sops
 * @param dir name of directory to store studies in.
 * @return the generated study instance uid
 */
def genStudy = { overlay, dir ->
    def suid = UIDUtils.createUID()
    def suiddir = new File(new File("studies", dir), suid)
    assert suiddir.mkdirs()

    [IMG_TEMPLATE, SR_TEMPLATE].each { infilename ->
        def instream = new FileInputStream(infilename)
        def obj = new StreamingDicomModifier(instream)

        def header = new DicomScriptingAPI(obj.getHeader())
        overlay.each { tag, value ->
            // Don't overwrite SR:
            if (!(tag == "Modality" && header.get("Modality") == "SR")) {
                header.set(tag, value, null)
            }
        }
        header.set(StudyInstanceUID, suid)
        header.set(SeriesInstanceUID, UIDUtils.createUID())
        header.set(SOPInstanceUID, UIDUtils.createUID())

        def out = new FileOutputStream(new File(suiddir, infilename))
        obj.writeTo(out, obj.header.getString(TransferSyntaxUID))
        out.flush()
        out.close()
        instream.close()
    }

    return suiddir
}

def sendStudy = { dir ->

  def sendCommand = """dcmsnd $DIR_AE@$DIR_IP:$DIR_PORT $dir"""
  def proc = sendCommand.execute()
  proc.waitFor()         
  if (proc.exitValue() != 0) {
    println "Sending study failed!"
  }

}

def sendHl7 = { dest, msg ->
    // for pretty-printing multi-line strings:
    msg = msg.replaceAll("\n\\s*", "\n")
    println "Sending request to $dest:\n$msg"
    msg = new PipeParser().parse(msg.replaceAll("\n", "\r"))
    def rsp = dest.send(msg)
    println "Got response from $dest:\n${rsp.toString().replaceAll('\r', '\n')}"
    return rsp
}

/**
 * Link foreign patient and ramq in empi.
 */
def empiLink = { foreignMrn, ramq ->
    sendHl7(empi,
        """MSH|^~\\&|RIALTO_TL|KAROS_TL|||20130723131442||ADT^A08||P|2.5
           PID|1||$foreignMrn^^^$FOREIGN_MRND~$ramq^^^$RAMQD|""")
}

/**
 * Link local patient and ramq in connect.
 */
def connectLink = { localMrn, ramq ->
    sendHl7(connectpix,
        """MSH|^~\\&|||||||ADT^A08|||2.5
           PID||$ramq|$localMrn""")
}

/**
 * Create a new patient with studies on the given dates.
 */
def createPatientWithDataSet = { patientName, foreignMrn, localMrn, ramq, studyParams ->
    empiLink(foreignMrn, ramq)
    connectLink(localMrn, ramq)

    studyParams.each { accessionNumber, modality, anatomicRegion, year, month, shouldFetch ->
        def time = new DateTime(year, month, 1, 0, 0, 0, 0)
        def negation = shouldFetch ? "" : "n't"
        suid = genStudy([
            AccessionNumber: accessionNumber,
            StudyID: ("SID_" + accessionNumber),
            PatientID: foreignMrn,
            PatientName: patientName,
            IssuerOfPatientID: FOREIGN_MRND_UID,
            Modality: modality,
            "AnatomicRegionSequence/CodeValue": anatomicRegion,
            StudyDate: time,
            StudyTime: time,
            // this might not be useful for non-prefetch tests:
            StudyDescription: "Should$negation be fetched for $modality/$anatomicRegion order"
        ], "$ramq/$modality$anatomicRegion")
        
        println "Generated $suid"

        sendStudy(suid)

        println "Sent $suid"
    }
}

/* Matching rule:
  [ modality : [ "CR" ], anatomicRegion : "TH", subrules :
    [
      [ modality : [ "CR" ], anatomicRegion : "TH", maxAge : 0, newest : 0, oldest : 1 ],
      [ modality : [ "CR" ], anatomicRegion : "TH", maxAge : 3, newest : 3, oldest : 0 ],
    ],
  ],
  Generate 4 newish studies and 2 old studies.  The 3 newest should be selected and the 1 oldest.
  Note that to fully test the above rules, we'd need more than one data set.
*/
createPatientWithDataSet("POND^AMY", "F111", "L111", "AAAA82552313",
            [ ["PACR1", "CR", "TH", 2014, 6, true],
              ["PACR2", "CR", "TH", 2014, 5, true],
              ["PACR3", "CR", "TH", 2013, 6, true],
              ["PACR4", "CR", "TH", 2012, 6, false], // young enough, but not one of the 3 newest
              ["PACR5", "CR", "TH", 2006, 6, false], // too old and not the oldest
              ["PACR6", "CR", "TH", 2005, 6, true]])


/* Matching rule:
 [ modality : [ "US" ], anatomicRegion : "AB", subrules :
        [
          [ modality : [ "US" ], anatomicRegion : "AB", maxAge : 0, newest : 0, oldest : 1 ],
          [ modality : [ "US" ], anatomicRegion : "AB", maxAge : 3, newest : 3, oldest : 0 ],
          ...
        ]
*/
createPatientWithDataSet("POND^AMY", "F111", "L111", "AAAA82552313",
            [ ["PAUS1", "US", "AB", 2014, 6, true],
              ["PAUS2", "US", "AB", 2014, 5, true],
              ["PAUS3", "US", "AB", 2013, 6, true],
              ["PAUS4", "US", "AB", 2012, 6, false], // young enough, but not one of the 3 newest
              ["PAUS5", "US", "AB", 2006, 6, false], // too old and not the oldest
              ["PAUS6", "US", "AB", 2005, 6, true]])

createPatientWithDataSet("WILLIAMS^RORY", "F222", "L222", "BBBB82552313",
            [ ["WRUS1", "US", "AB", 2014, 6, true],
              ["WRUS2", "US", "AB", 2014, 5, true],
              ["WRUS3", "US", "AB", 2013, 6, true],
              ["WRUS4", "US", "AB", 2012, 6, false], // young enough, but not one of the 3 newest
              ["WRUS5", "US", "AB", 2006, 6, false], // too old and not the oldest
              ["WRUS6", "US", "AB", 2005, 6, true]])

// For foreign patient query.
createPatientWithDataSet("LILY^JUNO", "F333", "L333", "CCCC82552313",
            [ ["LJUS1", "US", "AB", 2014, 6, true]])
