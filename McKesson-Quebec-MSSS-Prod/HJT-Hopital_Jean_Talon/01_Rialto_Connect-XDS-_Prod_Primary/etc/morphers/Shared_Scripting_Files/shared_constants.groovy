class SharedConstants {
    /** This is the Issuer of Patient ID for the Affinity Domain (i.e. the RAMQ) */
    static final Affinity_Domain_OID = '2.16.124.10.101.1.60.100'

    /** Issuer of Patient ID for a regional domain (eg. a PATSEC) */
    static final RegionalDomainOID = null

    /** Location in an HL7 Message that contains the (unqualified) PID value of the Local MRN */
    static final HL7_Local_MRN_PID_Field = 'PID-3-1'

    /** Location in an HL7 Message that contains the (unqualified) PID value for the Affinity Domain (i.e. RAMQ) */
    static final HL7_Affinity_Domain_PID_Field = 'PID-2-1'

    /** Location in the PID Segment of an HL7 Message that contains the Issuer of Patient ID to qualify the Local MRN
     * 'PID-3-4-2' is the Standard */
    static final HL7_Local_MRN_Issuer_of_PID_Field = 'PID-3-4-2'

    /** Location in the PID Segment of an HL7 Message that contains the Issuer of Patient ID to qualify the Affinity Domain
     * 'PID-3-4-2' is the Standard */
    static final HL7_Affinity_Domain_Issuer_of_PID_Field = 'PID-2-4-2'

    /** Location in an HL7 Order where we can expect the Modality of an incoming order to be populated */
    static final HL7_Order_Modality_Field = 'OBR-24'

    /** Location in an HL7 Order where we can expect the Anatomic Region to be populated */
    static final HL7_Order_Anatomic_Region_Field = 'OBR-15-4'

    /** Value in the ZDS Segments for the PACS HL7 Listener to Accept Reports and Orders */
    static final HL7_RIS_Brand_Name = 'RadImage'

    /** The Coding Scheme in the XDS Event Code List that specifies an Anatomic Region */
    static final XDS_Coding_Scheme_for_Anatomic_Region = "Imagerie Qu\u00e9bec-DSQ"

    /** The Coding Scheme in the XDS Event Code List that specifies a Modality */
    static final XDS_Coding_Scheme_for_Modality = 'DCM'

    static final Unwanted_Modalities_in_AdHoc_CFIND_Responses = ["KO", "SC", "SR"]
}

