// Optionally and transparently modifies the incoming Study Instance UIDs to honor the original UIDs in a multi-context PACS

LOAD("Shared_Scripting_Files/contexts.groovy")

context = Contexts.getContext(config.context)
if (context == null) {
    log.warn("Context is unknown from id: {}. Unable to unlocalize this request, passing it through unchanged. " +
        "Please correct either the connect ContextID configuration or contexts.groovy", config.context)
    return true
}

def pacsStudyInstanceUID = get(StudyInstanceUID)

log.debug("Study Instance UID as received from the PACS is '{}'", pacsStudyInstanceUID)

def studyInstanceUID = context.unlocalizeUID(pacsStudyInstanceUID, unhash)

if (studyInstanceUID == null) {
    log.error("Unexpected NULL Study Instance UID.  Dropping this C-Move request!! Original Study Instance UID '{}'",
        pacsStudyInstanceUID)
    return false
}

if (studyInstanceUID != pacsStudyInstanceUID) {
    log.info("PACS requesting C-MOVE for PACS Study Instance UID '{}' for original (DI-r) Study Instance UID '{}'.",
        pacsStudyInstanceUID, studyInstanceUID)

} else {
    // PACS had an unhashed Study Instance UID which would be the same as the DI-r would expect
    log.trace("The PACS passed a non-hashed Study Instance UID.  Respecting that value.")
}

set(StudyInstanceUID, studyInstanceUID)
