/* CFind To XDS Find Documents (ITI-18)  Script
 *
 * Shared in both the Ad-Hoc Query and Fetch Prior Studies workflow
 *
 * This script takes a C-Find and Converts it to an XDS ITI-18 Registry Stored Query.
 * The C-Find can either come directly from the PACS (as part of an Ad-Hoc Query), or from
 * a Connect internal workflow (as part of a Fetch Prior Studies)
 */

LOAD("Shared_Scripting_Files/shared_constants.groovy")
LOAD("Shared_Scripting_Files/contexts.groovy")

import org.joda.time.DateTime

context = Contexts.getContext(config.context)
if (context == null) {
    log.warn("Context is unknown from id: {}. Unable to convert this cfind, dropping it. " +
        "Please correct either the connect ContextID configuration or contexts.groovy", config.context)
    return false
}

log.debug("RAW DICOM C-Find Message received:\n{}", input)

// As per the Solution Architecture Document, all queries *must* have a local MRN
// -whether it exists in the PACS or the RIS DB
def pid = get(PatientID)

if (pid == null) {
    log.error("Patient ID in incoming C-Find was NULL. Need a local Patient ID to work.")
    return false
}

pid = pid.toUpperCase()


// Set the correct **LOCAL** Patient ID and Local PACS Issuer of Patient ID in 
// the XDS "Find Documents" Query

def callingAE = getCallingAETitle()
log.debug("Ad-Hoc C-Find: Calling AETitle initiating C-Find was '{}'", callingAE)

def issuer = null
def isAdhoc = false

if (context.PACSCallingAE == callingAE) {
    // Explicitly set the Issuer of PID to that of the local PACS for PIX lookup to work.
    // This is for when the local PACS does an Ad-Hoc Query
    issuer = context.PACSIssuer
    log.debug("This is an Ad Hoc Query from the PACS with a legitimate Calling AETitle")

    isAdhoc = true

} else if ('PROXY_SELF' == callingAE) {
    log.debug("This is a Fetch Prior Study")

    issuer = get(IssuerOfPatientID)
    // This IssuerOfPatientID was explictly populated in the 'HL7 to C-Find' script
    log.debug("IssuerOfPatientID is {}", issuer)
    if (issuer == null) {
        log.error("Fetch Priors query has no IssuerOfPatientID. " + 
                  "Cannot convert to XDS Find Documents.")
        return false
    }

} else {
    log.error("UNEXPECTED Calling AE Title [{}] received.\n" +
              "1) Unauthorized SCU AE Title (check script) or\n" +
              "2) Connect cannot ascertain whether the Order to find studies " +
              "came from the PACS or through an internal Fetch Prior Studies!",
              callingAE)
    return false
}

// Setting Local PID in XDS 'Find Documents' Query  -> 
// The Java code will run a PIX query and replace it by Affinity PID
set(XDSPatientID, pid, issuer)


// For KHC2051: Remove study instance uid from ad hoc C-Find queries
// to allow refetch of studies at multi-context sites:
input.remove(StudyInstanceUID)


/* For Ad hoc query **ONLY**: support a lower bound on the study date / time
 * (TODO: do they only ever specify the lower bound? should we handle ranges?)
 *
 * NOTE: This SHOULD *NOT* be used in Fetch Prior Studies, as the XDS Registry
 * will only give back studies that are at least as old as the ORM Order.
 * This will make the FPS irrelevant (the order will normally be the newest item
 * in the Patient History). */
if (isAdhoc) {
    // TODO validate correct formats?
    def dicomDate = get(StudyDate)
    def dicomTime = get(StudyTime)

    if (dicomDate != null) {
        // if time is null, this will be interpreted as midnight
        set(XDSServiceStartTime, dicomDate, dicomTime)
    } else if (dicomTime != null) {
        // if date is null but time isn't, they mean today:
        set(XDSServiceStartTime, new DateTime().toString("yyyyMMdd"), dicomTime)
    }
}

/*  For an Ad-Hoc Query, use the modalities to constrain the event codes */
if (isAdhoc){
    def modalitiesInStudy = getList(ModalitiesInStudy) as Set
    if (modalitiesInStudy != null) {
        log.debug("Setting Modalities constraint to: {}", modalitiesInStudy)
        def xdsEventCodes = modalitiesInStudy.collect { modality -> code(modality, "DCM") }
        set(XDSEventCode, xdsEventCodes)
    }
}


log.debug("This is the current XDS Query being created:\n{}", output)

