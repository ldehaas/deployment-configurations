<tests service="connect-xds" script="SRToORUMorpher" file="sr_to_oru_morpher.groovy">
    <test name="basic">
        <inputs>
            <dicom name="input">
                PatientID: pid
            </dicom>
            <dicom name="originalInput">
                OtherPatientIDsSequence/PatientID: patsec
                OtherPatientIDsSequence/IssuerOfPatientID: 2.16.124.10.101.1.60.1.3000.102.10.1112.0
            </dicom>
            <string name="context">Gatineau</string>
        </inputs>
        <assertions>
            assert returned == null || returned == true

            assert output.get('MSH-6') == "GAT"

            assert output.get('MSH-9-1') == "ORU"
            assert output.get('MSH-9-2') == "R01"

            assert output.get('PID-3') == "pid"
            assert output.get('PID-3-6') == "GAT"
            assert output.get('PID-4') == null
        </assertions>
    </test>

    <test name="noSubsecondInTimestamp">
        <inputs>
            <dicom name="input">
                PatientID: pid
                StudyDate: 20130721
                StudyTime: 135749.703000
            </dicom>
            <dicom name="originalInput" />
            <string name="context">Gatineau</string>
        </inputs>
        <assertions>
            assert returned != false

            assert output.get('OBR-7') == "20130721135749"
            assert output.get('OBR-6') == "20130721135749"
        </assertions>
    </test>

    <test name="dominantModality">
        <inputs>
            <dicom name="input">
                PatientID: pid
                Modality: SR
                ModalitiesInStudy: CT
            </dicom>
            <dicom name="originalInput" />
            <string name="context">Gatineau</string>
        </inputs>
        <assertions>
            assert returned != false

            assert output.get('OBR-24') == 'CT'
        </assertions>
    </test>

    <test name="unknownDominantModality">
        <inputs>
            <dicom name="input">
                PatientID: pid
                Modality: SR
            </dicom>
            <dicom name="originalInput" />
            <string name="context">Gatineau</string>
        </inputs>
        <assertions>
            assert returned != false

            // not sure if this is right, but it's what we've done historically:
            assert output.get('OBR-24') == null
        </assertions>
    </test>

    <test name="basicSRText">
        <inputs>
            <dicom name="input">
                PatientID: pid
                Modality: SR
                ContentSequence[1]/ValueType: CONTAINER
                ContentSequence[1]/ContentSequence/ValueType: TEXT
                ContentSequence[1]/ContentSequence/ConceptNameCodeSequence/CodeMeaning: title 1
                ContentSequence[1]/ContentSequence/TextValue: text 1
                ContentSequence[2]/ValueType: CONTAINER
                ContentSequence[2]/ContentSequence/ValueType: TEXT
                ContentSequence[2]/ContentSequence/ConceptNameCodeSequence/CodeMeaning: title 2
                ContentSequence[2]/ContentSequence/TextValue: text 2
            </dicom>
            <dicom name="originalInput" />
            <string name="context">Gatineau</string>
        </inputs>
        <assertions>
            assert returned != false

            reportLines = output.getList('OBX-5(*)')
            assert reportLines == ['TITLE 1', '', 'text 1', '', '', 'TITLE 2', '', 'text 2', '', '']
        </assertions>
    </test>

    <test name="missingSRTitle">
        <inputs>
            <dicom name="input">
                PatientID: pid
                Modality: SR
                ContentSequence[1]/ValueType: CONTAINER
                ContentSequence[1]/ContentSequence/ValueType: TEXT
                ContentSequence[1]/ContentSequence/TextValue: text 1
                ContentSequence[2]/ValueType: CONTAINER
                ContentSequence[2]/ContentSequence/ValueType: TEXT
                ContentSequence[2]/ContentSequence/ConceptNameCodeSequence/CodeMeaning: title 2
                ContentSequence[2]/ContentSequence/TextValue: text 2
            </dicom>
            <dicom name="originalInput" />
            <string name="context">Gatineau</string>
        </inputs>
        <assertions>
            assert returned != false

            reportLines = output.getList('OBX-5(*)')
            assert reportLines == ['___', '', 'text 1', '', '', 'TITLE 2', '', 'text 2', '', '']
        </assertions>
    </test>

    <test name="missingSRText">
        <inputs>
            <dicom name="input">
                PatientID: pid
                Modality: SR
                ContentSequence[1]/ValueType: CONTAINER
                ContentSequence[1]/ContentSequence/ValueType: TEXT
                ContentSequence[1]/ContentSequence/ConceptNameCodeSequence/CodeMeaning: title 1
                ContentSequence[2]/ValueType: CONTAINER
                ContentSequence[2]/ContentSequence/ValueType: TEXT
                ContentSequence[2]/ContentSequence/ConceptNameCodeSequence/CodeMeaning: title 2
                ContentSequence[2]/ContentSequence/TextValue: text 2
            </dicom>
            <dicom name="originalInput" />
            <string name="context">Gatineau</string>
        </inputs>
        <assertions>
            assert returned != false

            reportLines = output.getList('OBX-5(*)')
            assert reportLines == ['TITLE 1', '', '', '', '', 'TITLE 2', '', 'text 2', '', '']
        </assertions>
    </test>

    <test name="replaceInvalidEscapeSequences">
        <inputs>
            <dicom name="input">
                PatientID: pid
                Modality: SR
                ContentSequence[1]/ValueType: CONTAINER
                ContentSequence[1]/ContentSequence/ValueType: TEXT
                ContentSequence[1]/ContentSequence/ConceptNameCodeSequence/CodeMeaning: title 1
                # NB: backslashes are escapes in this dicom format too, hence the extra set:
                ContentSequence[1]/ContentSequence/TextValue: text\\X09\\1
                ContentSequence[2]/ValueType: CONTAINER
                ContentSequence[2]/ContentSequence/ValueType: TEXT
                ContentSequence[2]/ContentSequence/ConceptNameCodeSequence/CodeMeaning: title 2
                ContentSequence[2]/ContentSequence/TextValue: text 2
            </dicom>
            <dicom name="originalInput" />
            <string name="context">Gatineau</string>
        </inputs>
        <assertions>
            assert returned != false

            reportLines = output.getList('OBX-5(*)')
            assert reportLines == ['TITLE 1', '', 'text\t1', '', '', 'TITLE 2', '', 'text 2', '', '']
        </assertions>
    </test>

    <test name="replaceInvalidEscapeSequencesWithCarriageReturn">
        <inputs>
            <dicom name="input">
                PatientID: pid
                Modality: SR
                ContentSequence[1]/ValueType: CONTAINER
                ContentSequence[1]/ContentSequence/ValueType: TEXT
                ContentSequence[1]/ContentSequence/ConceptNameCodeSequence/CodeMeaning: title 1
                # NB: backslashes are escapes in this dicom format too, hence the extra set:
                ContentSequence[1]/ContentSequence/TextValue: text\\X0D\\1
                ContentSequence[2]/ValueType: CONTAINER
                ContentSequence[2]/ContentSequence/ValueType: TEXT
                ContentSequence[2]/ContentSequence/ConceptNameCodeSequence/CodeMeaning: title 2
                ContentSequence[2]/ContentSequence/TextValue: text 2
            </dicom>
            <dicom name="originalInput" />
            <string name="context">Gatineau</string>
        </inputs>
        <assertions>
            assert returned != false

            reportLines = output.getList('OBX-5(*)')
            // the carriage return becomes a new line, which becomes a new repetition:
            assert reportLines == ['TITLE 1', '', 'text', '1', '', '', 'TITLE 2', '', 'text 2', '', '']
        </assertions>
    </test>

    <test name="withPrefetchOrder">
        <inputs>
            <dicom name="input">
                PatientID: pid
            </dicom>
            <dicom name="originalInput" />
            <hl7 name="prefetchOrder">
                MSH|^~\#|||||201108020914||ORM^O01|3288623|P|2.3
                PID|1|RAMQ14010101|local|patsec2^^^#2.16.124.10.101.1.60.1.3000.102.10.1112.0#ISO
                PV1|1|
                ORC|NW||||U|N|||201108020914|||||||EN ATTENTE
                OBR|1|001667984BGH||||201108020914|20110802||||||||AB|||||||||MR
            </hl7>
            <string name="context">Gatineau</string>
        </inputs>
        <assertions>
            assert returned != false

            // nothing else to test at this site, except that we don't blow up
        </assertions>
    </test>
</tests>

