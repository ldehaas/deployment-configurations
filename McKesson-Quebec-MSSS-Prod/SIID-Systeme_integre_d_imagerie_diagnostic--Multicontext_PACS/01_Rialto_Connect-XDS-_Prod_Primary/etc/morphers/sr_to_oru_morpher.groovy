/* SR to ORU Morpher
 *
 * Shared in both the Ad-Hoc Query and Fetch Prior Studies workflow.
 *
 * This script creates an ORU report message from a DICOM Structured Report.  
 * Optional; if not configured, SRs will be passed directly to the PACS.
 */

LOAD("Shared_Scripting_Files/contexts.groovy")
LOAD("Shared_Scripting_Files/common_localization_routines.groovy")
LOAD("Shared_Scripting_Files/shared_constants.groovy")
LOAD("Shared_Scripting_Files/shared_hl7_resources.groovy")

import com.karos.rtk.common.HL7v2Date

context = Contexts.getContext(config.context)
if (context == null) {
    log.warn("Context is unknown from id: {}. Dropping this SR. " +
        "Please correct either the connect ContextID configuration or contexts.groovy", config.context)
    return false
}

// Explicitly set the charset for SRs that don't have them, assuming ISO IR 100
if (input.get(SpecificCharacterSet) == null) {
    input.set(SpecificCharacterSet, "ISO_IR 100")
}

log.debug("SR to ORU Report conversion script starting with input:\n{}", input)

initialize("ORU", "R01", "2.3")

// This is required so that the PACS HL7 handler accepts the message
set('MSH-6', context.preferredRISSendingFacility())

set('MSH-7', HL7v2Date.now(HL7v2Date.DateLength.MILLISECOND3))

// HL7 Character Set supported by French HMI PACS
set('MSH-18', '8859/1')


/* PID */

set("PID-1", "1")

def sourcePID = get(PatientID)
if (sourcePID != null) {
    set('PID-3', sourcePID)
} else {
    log.error("The Patient Localization failed for SR to ORU Creation!!  The localized Patient is null")
    return false
}

set('PID-3-6', context.preferredRISSendingFacility())

pids = new Pids(originalInput, prefetchOrder)
log.debug("Known pids from originalInput: {}", pids.issuerToPid)
def patsec = pids.getPid(SharedConstants.RegionalDomainOID)
set('PID-4', patsec)
log.debug("PID-4: {}", patsec)


/**
 * Copies up to three components of a name from the dicom into the hl7 message.
 * DICOM person name elements are supposed to be of the form
 * family^given^middle^prefix^suffix.  HL7 names are sometimes of the form
 * family^given^middle^suffix^prefix but some components are actually 
 * id^family^given^...
 * 
 * In the case where the id is there, pass startingComponent=2 to offset where
 * we put the name.  However, in our samples, the values of the person names
 * actually include the id (they are in DICOM format instead of HL7).  In this
 * case, just leave the default startingComponent.
 * 
 * @param dicomTag place to get name from in dicom
 * @param hl7prefix place to put name in hl7
 * @param startingComponent if name doesn't line up at beginning of 
 * @return
 */
def setName(dicomTag, hl7prefix, startingComponent=1) {
    nameParts = split(dicomTag)
    if (nameParts == null) {
        return
    }

    // loop far enough to get the id if present, plus the family and given names
    int maxPartsToCopy = 3
    
    for (int i = 0; i < maxPartsToCopy && i < nameParts.size(); i++) {
        set(hl7prefix + "-" + (startingComponent++), nameParts[i])
    }
}

setName(PatientName, "PID-5")

set("PID-6", get(PatientMotherBirthName))

set("PID-7", get(PatientBirthDate))

set("PID-8", get(PatientSex))

set("PID-11", get(PatientAddress))

set("PID-13", get(PatientTelephoneNumbers))


/* ORC */

// RE = Observations to follow
set("ORC-1", "RE")

set("ORC-3", get(AccessionNumber))


/* OBR */
set("OBR-1", "1")

set("OBR-3", get(AccessionNumber))

// This is the Top-Level CodeValue
// This is currently being populated as the Normalizaed Procedure Code in the Foreign Image Localizer script
set("OBR-4-1", get(CodeValue))

set("OBR-4-2", get(StudyDescription))

// Foreign Procedure Code
set("OBR-4-4", get(AccessoryCode))

// Foreign Procedure Description
set("OBR-4-6", get(AcquisitionComments))

def studyDate = get(StudyDate)
def studyTime = get(StudyTime)
def contentDate = get(ContentDate)
def contentTime = get(ContentTime)

log.trace("Study Date/Time: {}{}; Content Date/Time: {}{}", studyDate, studyTime, contentDate, contentTime)

def observationDateTime = null

if (studyDate != null && !studyDate.isEmpty()) {
    observationDateTime = studyDate + (studyTime == null ? "" : studyTime)

} else if (contentDate != null && !contentDate.isEmpty()) {
    observationDateTime = contentDate + (contentTime == null ? "" : contentTime)
    log.debug("Study Date and Time incomplete or blank. " + 
              "Using Content Date and Time to populate HL7 Observation Date and Time")

} else {
    log.warn("Study Date is non-existent.  Leaving HL7 Observation Date and Time blank!!")
} 

if (observationDateTime != null) {
    // trim sub-second component for PACSs that don't like it:
    observationDateTime = observationDateTime.replaceAll("\\..*", "")
}

log.debug("Observation date/time: {}", observationDateTime)

set("OBR-7", observationDateTime)

// As per McKesson's request, adding the observationDateTime to OBR-6 so that Foreign Exams
// do not show up in Rad Technician's worklists that talk to the PACS
// Please refer to KHC 1384 for details
log.debug("Populating Content Date / Time in OBR-6 so that Rad Technician's worklists don't show foreign exams")
set("OBR-6", observationDateTime)

// Populate the Anatomic Region 
anatomicRegionSeq = get(AnatomicRegionSequence)
if (!anatomicRegionSeq.isEmpty()) {
    anatomicRegion = anatomicRegionSeq.first()
    set("OBR-15-1-1", anatomicRegion.get(CodeValue))
    set("OBR-15-1-2", anatomicRegion.get(CodeMeaning))
    set("OBR-15-1-3", anatomicRegion.get(CodingSchemeDesignator))
}

// our samples includes the physician id (non-standard)
setName(ReferringPhysicianName, "OBR-16")

set("OBR-19", get(AccessionNumber))

set("OBR-20", get(StudyID))

set("OBR-22", get(ContentDate) + get(ContentTime).replaceAll("\\..*",""))

def dominantModality = get(ModalitiesInStudy)
log.debug("The modality of this report is '{}'", get(Modality))
log.debug("The Dominant Modality of this Study is '{}'", dominantModality)

if (dominantModality == null || dominantModality.isEmpty()) {
   log.warn("Null or empty Dominant Modality for this Study.  The PACS might reject this report!")
} else {
    set("OBR-24", dominantModality)
}

set("OBR-25", get(CompletionFlag))

set("OBR-27-4", observationDateTime)
log.debug("SR to ORU Workflow: Setting the OBR-27-4 to '{}'", output.get('OBR-27-4'))


// Setting Verifying Observer ID and Names (Lastname,Firstname) into OBR-32 sub-segments
def verifyingObserverName = null
def verifyingObserverSeq = get(VerifyingObserverSequence)
if (!verifyingObserverSeq.isEmpty()) {

    def verifyingObserver = verifyingObserverSeq.first()
    verifyingObserverName = verifyingObserver.get(VerifyingObserverName)
    log.debug("Verifying Observer Name as received from the DIR in this SR: '{}'", verifyingObserverName)

    def verifyingObserverIDCodeSeq = verifyingObserver.get(VerifyingObserverIdentificationCodeSequence)
    if (!verifyingObserverIDCodeSeq.isEmpty()) {
        def vObsId = verifyingObserverIDCodeSeq.first()
        set("OBR-32-1", vObsId.get(CodeValue))
        log.debug("Verifying Observer ID Code as received from DIR is '{}'", output.get("OBR-32-1"))
    }
}

if (verifyingObserverName != null && !verifyingObserverName.isEmpty()) {

    verifyingObserverName = Demographics.parseName(verifyingObserverName)

    // Use the comma format expected at the Report Reader (RADReport)
    verifyingObserverName = verifyingObserverName.join(", ")
}

log.debug("This is the final verifying observer name (Report Author??) to be used: '{}'", verifyingObserverName)
set("OBR-32-2", verifyingObserverName)

/* Populate the Expected OBX Segments */

/**
 * Some SRs that we have seen in production have invalid escape sequences in
 * their report content.  For example, the string "\X09\" instead of a tab
 * character.  This causes visual problems in the reports as displayed by
 * PACSs so we should replace them with what is likely the correct character...
 */
def replaceInvalidEscapeSequences(content) {

    content = content.replaceAll("\\\\X(\\p{XDigit}{2})\\\\") {
        (char) Integer.parseInt(it[1], 16)
    }

    // now replace all carriage returns with newlines, since carriage
    // returns get escaped by us and then usually misinterpreted by a PACS
    return content.replaceAll("\r", "\n");
}

// Preliminary setup
def gatherTextContent(contentSeq, output) {
    contentSeq.each { content ->
        if ("TEXT".equals(content.get(ValueType))) {
            meaning = null
            conceptNameCodeSeq = content.get(ConceptNameCodeSequence)
            if (!conceptNameCodeSeq.isEmpty()) {
                meaning = conceptNameCodeSeq.first().get(CodeMeaning)
            }
            
            output.add([meaning, content.get(TextValue)])
            
        } else if ("CONTAINER".equals(content.get(ValueType))) {
            gatherTextContent(content.get(ContentSequence), output)
        }
    }
}

reportTextItems = []
gatherTextContent(get(ContentSequence), reportTextItems)

// Set OBX Segments
set("OBX-1", "1")

set("OBX-2", "TX")

// Set actual Report contents in OBX-5
textRepetition = 0
reportTextItems.each { reportTextItem ->
    title = reportTextItem[0] == null ? "___" : reportTextItem[0].toUpperCase()
    text = reportTextItem[1] ==  null ? "" : replaceInvalidEscapeSequences(reportTextItem[1])

    set("OBX-5(" + textRepetition++ + ")", title)
    // blank line after title
    set("OBX-5(" + textRepetition++ + ")", "")

    text.split("\n").each { line ->
        set("OBX-5(" + textRepetition++ + ")", line.trim())
    }

    // 2 blank lines after each section
    set("OBX-5(" + textRepetition++ + ")", "")
    set("OBX-5(" + textRepetition++ + ")", "")
}

def verificationFlag = get(VerificationFlag)
if (verificationFlag == "VERIFIED") {
    verificationFlag = "F"
} else if(verificationFlag == "UNVERIFIED") {
    verificationFlag = "P"
}

// McKesson needs this for their PACS report-displaying mechanism
/* "The current logic is setup where ‘F’ and ‘A’ in OBX-11 will be reported and everything else is transcribed." */
set("OBX-11", verificationFlag)
log.debug("DICOM SR to HL7 ORU Report creation: Setting Verification Flag in OBX-11 as '{}'", verificationFlag)


set("OBX-14", get(ContentDate) + get(ContentTime).replaceAll("\\..",""))
// TODO: split name and use subcomponents of OBX-16, starting at OBX-16-2
//set("OBX-16", verifyingObserverName)


/* ZDS */
output.getMessage().addNonstandardSegment('ZDS')

set('ZDS-1-1', get(StudyInstanceUID))

set('ZDS-1-2', SharedConstants.HL7_RIS_Brand_Name)

set('ZDS-1-3', 'Application')

set('ZDS-1-4', 'DICOM')


log.debug("This is the Report that will get sent:\n{}", output)

