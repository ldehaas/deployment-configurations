// This script will filter all priors against a set of rules that will determine
// what priors are relevant based on the new order.

log.debug("Checking {} priors", studies.size())

LOAD("priors_rules.groovy")

def getCodeValues(prior, scheme) {
    return prior.get(XDSEventCodes).findAll({ ec -> 
        scheme == ec.getSchemeName()
    }).collect({ ec -> 
        ec.getCodeValue() 
    })
}

/**
 * @return true if it matches, false otherwise
 */
def priorMatchesSubrule(prior, subrule) {
    log.debug("Testing prior " + prior + " against rule " + subrule)

    // The prior must contain one of the modalities modality that is specified by the subrule.
    if (subrule.modality.disjoint(getCodeValues(prior, "DCM"))) {
        log.debug("Prior does not contain relevant modalities")
        return false
    }

    // The anatomic region of the prior must match the anatomic region of the subrule.
    if (!getCodeValues(prior, "Imagerie Québec-DSQ").contains(subrule.anatomicRegion)) {
        log.debug("Prior does not match anatomic region")
        return false
    }

    // Now check to see if there is a maximum age constraint and if so, does the study date 
    // of the prior fall within that constraint.
    if (subrule.maxAge > 0) {
        def earliestStudyDate = new org.joda.time.DateTime().minusYears(subrule.maxAge)
        def priorStudyDate = prior.get(XDSServiceStartTime)

        if (priorStudyDate < earliestStudyDate) {
            log.debug("Prior falls outside of maximum age")
            return false
        }
    } 

    log.debug("Prior matches rule")
    return true
}

/**
 * @return list containing matching priors
 */
def filterForNewestAndOldest(priors, subrule) {
    if (subrule.newest <= 0 && subrule.oldest <= 0) {
        log.debug("Rule has no newest/oldest restrictions")
        return priors
    }

    if (subrule.newest + subrule.oldest >= priors.size()) {
        log.debug("Newest/oldest restrictions include all priors")
        return priors
    }

    log.debug("Relevant sub priors before forward sorting is " + priors)
    priors.sort { it.get(XDSServiceStartTime) }
    log.debug("Relevant sub priors after forward sorting is " + priors)

    // sort goes low to high, so the newest are at the end of the list:
    return priors.reverse().take(subrule.newest) +
        priors.take(subrule.oldest)
}

/**
 * Top level method for comparing a list of priors against a single subrule.
 * @return list containing matching priors
 */
def getPriorsMatchingSubrule(subrule, allPriors) {
    log.debug("Subrule is " + subrule)

    def matchingPriors = allPriors.findAll { priorMatchesSubrule(it, subrule) }
    log.debug("Relevant sub priors matching subrules are: " + matchingPriors)

    matchingPriors = filterForNewestAndOldest(matchingPriors, subrule)
    log.debug("Relevant sub priors after computing newest/oldest are " + matchingPriors)

    return matchingPriors
}

/**
 * Starting point for evaluating rules.
 */
def findRelevantPriors(order, allPriors) {
    def rule = PriorsRules.getRule(order.get("OBR-24"), order.get("OBR-15-4"))

    log.debug("Rule is: " + rule)

    if (rule == null) {
        return []
    }

    // for each subrule, get the list of matching priors and 
    // combine them all into one list:
    def relevantPriors = rule.subrules.collectMany { subrule ->
        getPriorsMatchingSubrule(subrule, allPriors)
    }

    log.debug("Relevant priors before uniquifying " + relevantPriors)

    // It may be possible that there are duplicates in the relevant priors,
    // since a single prior may match multiple rules.  Two priors are determined to
    // be duplicates if they share the same study instance UID.
    relevantPriors.unique { it.get(XDSExtendedMetadata("studyInstanceUID")) }

    return relevantPriors
}

return findRelevantPriors(order, studies)

