

ia = get(InstanceAvailability)

// OsiriX does not recognize InstanceAvailability.
if (ia == null) {
    ia = 'ONLINE'
}

// dcm4chee uses UNAVAILABLE for purged studies
if (ia.equalsIgnoreCase('UNAVAILABLE')) {
        set(InstanceAvailability, 'NEARLINE')
}
