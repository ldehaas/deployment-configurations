/* SR to ORU Morpher
 *
 * Shared in both the Ad-Hoc Query and Fetch Prior Studies workflow.
 *
 * This script creates an ORU report message from a DICOM Structured Report.  
 * Optional; if not configured, SRs will be passed directly to the PACS.
 */

LOAD("shared_scripting_files/common_localization_routines.groovy")


// Create a new, blank ORU message with the Default MSH segment codes specified in the SAD
initialize("ORU", "R01", "2.3")


// Explicitly set the charset for SRs that don't have them, assuming ISO IR 100
if (input.get(SpecificCharacterSet) == null) {
    input.set(SpecificCharacterSet, "ISO_IR 100")
}


log.debug("\n\n\n*** Starting DICOM SR to HL7 ORU Report Conversion Script *** :\n")
log.debug("This is the object received from the DICOM Study (after modifying char set):\n{}", input)

// Setting HL7 Character Set supported by French HMI PACS
set('MSH-18', '8859/1')


set("PID-1", "1")

def sourcePID = get(PatientID)
if (sourcePID != null) {
    set('PID-3', sourcePID) 
} else {
    log.error("The Patient Localization failed for SR to ORU Creation!! " +
        "The localized Patient is null")
    return false
}

/**
 * Copies up to three components of a name from the SR into the hl7 message.
 * DICOM person name elements are supposed to be of the form
 * family^given^middle^prefix^suffix.  HL7 names are sometimes of the form
 * family^given^middle^suffix^prefix but some components are actually 
 * id^family^given^...
 * 
 * In the case where the id is there, pass startingComponent=2 to offset where
 * we put the name.  However, in our sample SRs, the values of the person names
 * actually include the id (they are in DICOM format instead of HL7).  In this
 * case, just leave the default startingComponent.
 * 
 * @param dicomTag place to get name from in SR
 * @param hl7prefix place to put name in hl7
 * @param startingComponent if name doesn't line up at beginning of 
 * @return
 */
def setName(dicomTag, hl7prefix, startingComponent=1) {
    nameParts = split(dicomTag)
    if (nameParts == null) {
        return
    }

    // loop far enough to get the id if present, plus the family and given names
    int maxPartsToCopy = 3
    
    for (int i = 0; i < maxPartsToCopy && i < nameParts.size(); i++) {
        set(hl7prefix + "-" + (startingComponent++), nameParts[i])
    }
}

// Set the Patient Name with up to 3 components from DICOM
setName(PatientName, "PID-5")

// Set Mother's Maiden Name, if present in the DICOM SR
set("PID-6", get(PatientMotherBirthName))

set("PID-7", get(PatientBirthDate))

set("PID-8", get(PatientSex))

set("PID-11", get(PatientAddress))

set("PID-13", get(PatientTelephoneNumbers))



/* Set the expected ORC Segments */ 
set("ORC-1", "RE")

set("ORC-3", get(AccessionNumber))



/* Set the expected OBR Segments */ 
set("OBR-1", "1")

set("OBR-3", get(AccessionNumber))

// This is the Top-Level CodeValue
// This is currently being populated as the Normalizaed Procedure Code in the Foreign Image Localizer script
set("OBR-4-1", get(CodeValue))

set("OBR-4-2", get(StudyDescription))

// Foreign Procedure Code
set("OBR-4-4", get(AccessoryCode))

// Foreign Procedure Description
set("OBR-4-6", get(AcquisitionComments))


/* Handle the Report date and time correctly */

def studyDate = get(StudyDate)
def studyTime = get(StudyTime)
def contentDate = get(ContentDate)
def contentTime = get(ContentTime)

def observationDateTime = null

if (studyDate != null && !studyDate.isEmpty()) {
    observationDateTime = studyDate + (studyTime == null ? "" : studyTime)

} else if (contentDate != null && !contentDate.isEmpty()) {
    observationDateTime = contentDate + (contentTime == null ? "" : contentTime)
    log.debug("Study Date and Time incomplete or blank. " + 
              "Using Content Date and Time to populate HL7 Observation Date and Time")

} else {
    log.warn("Report Date is non-existent.  Leaving HL7 ORU Observation Date and Time blank!!")
} 

set("OBR-7", observationDateTime)


// Populate the Anatomic Region 
anatomicRegionSeq = get(AnatomicRegionSequence)
if (!anatomicRegionSeq.isEmpty()) {
    anatomicRegion = anatomicRegionSeq.first()
    set("OBR-15-1-1", anatomicRegion.get(CodeValue))
    set("OBR-15-1-2", anatomicRegion.get(CodeMeaning))
    set("OBR-15-1-3", anatomicRegion.get(CodingSchemeDesignator))
}

// our sample SR includes the physician id (non-standard)
setName(ReferringPhysicianName, "OBR-16")

set("OBR-19", get(AccessionNumber))

set("OBR-20", get(StudyID))

set("OBR-22", get(ContentDate) + get(ContentTime))

set("OBR-24", get(ModalitiesInStudy))
log.debug("The modality of this report is '{}'", get(Modality))
log.debug("The Modalities of this Study are '{}'", get(ModalitiesInStudy))

completionFlag = get(CompletionFlag)
// TODO: document why this is commented out, or remove it
//performedProcedureSeq = get(PerformedProcedureCodeSequence)
//if (!performedProcedureSeq.isEmpty()) {
//    performedProcedure = performedProcedureSeq.first()
//    completionFlag = performedProcedure.get(CompletionFlag)
//}
set("OBR-25", completionFlag)


set("OBR-27-4", observationDateTime)
log.debug("SR to ORU Workflow: Setting the OBR-27-4 to '{}'", output.get('OBR-27-4'))


// Setting Verifying Observer ID and Names (Lastname,Firstname) into OBR-32 sub-segments
def verifyingObserverName = null
def verifyingObserverSeq = get(VerifyingObserverSequence)
if (!verifyingObserverSeq.isEmpty()) {

    def verifyingObserver = verifyingObserverSeq.first()
    verifyingObserverName = verifyingObserver.get(VerifyingObserverName)
    log.debug("Verifying Observer Name as received from the DIR in this SR: '{}'", verifyingObserverName)

    def verifyingObserverIDCodeSeq = verifyingObserver.get(VerifyingObserverIdentificationCodeSequence)
    if (!verifyingObserverIDCodeSeq.isEmpty()) {
        def vObsId = verifyingObserverIDCodeSeq.first()
        set("OBR-32-1", vObsId.get(CodeValue))
        log.debug("Verifying Observer ID Code as received from DIR is '{}'", output.get("OBR-32-1"))
    }
}

if (verifyingObserverName != null && !verifyingObserverName.isEmpty()) {

    verifyingObserverName = Demographics.parseName(verifyingObserverName)

    // Use the comma format expected at the Report Reader (RADReport)
    verifyingObserverName = verifyingObserverName.join(", ")
}

log.debug("This is the final verifying observer name (Report Author??) to be used: '{}'", verifyingObserverName)
set("OBR-32-2", verifyingObserverName)

/* Populate the Expected OBX Segments */

/**
 * Some SRs that we have seen in production have invalid escape sequences in
 * their report content.  For example, the string "\X09\" instead of a tab
 * character.  This causes visual problems in the reports as displayed by
 * PACSs so we should replace them with what is likely the correct character...
 */
def replaceInvalidEscapeSequences(content) {

    content = content.replaceAll("\\\\X(\\p{XDigit}{2})\\\\") {
        (char) Integer.parseInt(it[1], 16)
    }

    // now replace all carriage returns with newlines, since carriage
    // returns get escaped by us and then usually misinterpreted by a PACS
    return content.replaceAll("\r", "\n");
}

// Preliminary setup
def gatherTextContent(contentSeq, output) {
    contentSeq.each { content ->
        if ("TEXT".equals(content.get(ValueType))) {
            meaning = null
            conceptNameCodeSeq = content.get(ConceptNameCodeSequence)
            if (!conceptNameCodeSeq.isEmpty()) {
                meaning = conceptNameCodeSeq.first().get(CodeMeaning)
            }
            
            output.add([meaning, content.get(TextValue)])
            
        } else if ("CONTAINER".equals(content.get(ValueType))) {
            gatherTextContent(content.get(ContentSequence), output)
        }
    }
}

reportTextItems = []
gatherTextContent(get(ContentSequence), reportTextItems)

// Set OBX Segments
set("OBX-1", "1")

set("OBX-2", "TX")

// Set actual Report contents in OBX-5
textRepetition = 0
reportTextItems.each { reportTextItem ->
    title = reportTextItem[0] == null ? "___" : reportTextItem[0].toUpperCase()
    text = replaceInvalidEscapeSequences(reportTextItem[1])

    set("OBX-5(" + textRepetition++ + ")", title)
    // blank line after title
    set("OBX-5(" + textRepetition++ + ")", "")

    text.split("\n").each { line ->
        set("OBX-5(" + textRepetition++ + ")", line.trim())
    }

    // 2 blank lines after each section
    set("OBX-5(" + textRepetition++ + ")", "")
    set("OBX-5(" + textRepetition++ + ")", "")
}

def verificationFlag = get(VerificationFlag)
if (verificationFlag == "VERIFIED") {
    verificationFlag = "F"
} else if(verificationFlag == "UNVERIFIED") {
    verificationFlag = "P"
}

// McKesson needs this for their PACS report-displaying mechanism
/* "The current logic is setup where ‘F’ and ‘A’ in OBX-11 will be reported and everything else is transcribed." */
set("OBX-11", verificationFlag)
log.debug("DICOM SR to HL7 ORU Report creation: Setting Verification Flag in OBX-11 as '{}'", verificationFlag)


set("OBX-14", get(ContentDate) + get(ContentTime))
// TODO: split name and use subcomponents of OBX-16, starting at OBX-16-2
//set("OBX-16", verifyingObserverName)


/* Populate the Expected ZDS Segment */
output.getMessage().addNonstandardSegment('ZDS')

set('ZDS-1-1', get(StudyInstanceUID))

set('ZDS-1-2', 'RadImage')

set('ZDS-1-3', 'Application')

set('ZDS-1-4', 'DICOM')


log.debug("This is the Report that will get sent:\n{}", output)

