/*
 * this scripts constructs a cfind query from an hl7 order message to search
 * for prior studies of interest
 * 
 * this script can return false to signal not to do any prefetching for a 
 * given order
 */
 
def pid = get( "PID-3-1" )
log.debug("found pid '{}'", pid)

/*
if (pid ==~ /regular expression/) {
    return false
}
*/

// construct the cfind
set ( PatientID, pid )

// etc etc