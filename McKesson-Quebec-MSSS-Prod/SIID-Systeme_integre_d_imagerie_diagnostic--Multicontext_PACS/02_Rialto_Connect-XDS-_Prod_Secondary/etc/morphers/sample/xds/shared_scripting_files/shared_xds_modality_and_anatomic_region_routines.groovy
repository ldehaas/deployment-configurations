/* This file contains worker fucntions for working with the XDS Event Codes, which contain both the Modalities and Anatomic Region
        for a given study --as published to the XDS Registry --  */

/* The content of the file is the following:

    - 'cleanUpEventCodes' takes the raw Event codes as made available by the Java code, and cleans them up
        in a format usable by various matching rules througout Connect

*/


class Modalities {

    /**
     * This list determines what the Hierachical Modalities are to determine the Dominant Modalities
     * of multi-modality studies (used for Procedure Code Mapping)
     * This list might be different from site to site.  The worker functions should all work correctly regardless of the content
     * of the list.
     */
    private static hierarchicalModalityList = [ "MG", "XA", "PT", "MR", "NM", "CT", "US", "RF", "CR", "DX", "BMD"]

    /**
     * Determines the Dominant Modality for a given list of XDS Codes from the XDS Registry
     * Expects a list, if you pass a string, the results will be incorrect.
     * Will return a modality as a string.  If no dominant modality is found, it will return an empty string.
     * (if you get an empty string back, use the first modality in your script code)
     * If an empty lists was passed, it will return an empty string.
     */
    public static secondaryToPrimaryModalitiesMap = [
        "DX" : "CR",
    ].withDefault{""}

    /**
     * For the given coding scheme, we will automatically apply replacement event code values.
     */
    private static schemeToReplacementMap = [
        // for all DCM event codes, use secondaryToPrimaryModalitiesMap to fix modalities
        "DCM": secondaryToPrimaryModalitiesMap
    ].withDefault{null}

    public static determineDominantModality(eventCodes, modalityCodingScheme) {
        def xdsModalities = getCodeValues(eventCodes, modalityCodingScheme)

        for (modality in hierarchicalModalityList) {
            if (xdsModalities.contains(modality)) {
                return modality
            }
        }

        return ""
    }

    /**
     * Returns (as a string) the Anatomic Region in the Raw XDS Event Codes
     * If there is more than 1 Anatomic Region (there shouldn't be), it returns the first one it finds
     */
    public static determineAnatomicRegion(eventCodes, codingSchemeForAnatomicRegion){

        def regions = getCodeValues(eventCodes, codingSchemeForAnatomicRegion)

        return regions.isEmpty() ? "" : regions[0]
    }

    /**
     * Filter the list of event codes to return only those with the specified
     * coding scheme.
     */
    public static getCodeValues(eventCodes, codingScheme) {

        // filter for scheme:
        eventCodes = eventCodes.findAll { eventCode -> codingScheme.equalsIgnoreCase(eventCode.getSchemeName()) }

        // collect just the code values:
        eventCodes = eventCodes.collect { eventCode -> eventCode.getCodeValue() }
        
        // Substitute invalid values:
        def substitutionMap = schemeToReplacementMap[codingScheme]
        if (substitutionMap != null) {
            eventCodes = eventCodes.collect { eventCode ->
                def sub = substitutionMap[eventCode]
                if (sub != "") {
                    return sub
                }
                return eventCode
            }
        }

        return eventCodes
    }

    /**
     * Like "determineDominantModality", but uses a list of pre-parsed modalities instead of XDS Event Codes.
     * Expects a list, if you pass a string, the results will be incorrect.
     * Will return a modality as a string.  If no dominant modality is found, it will return an empty string.
     * (if you get an empty string back, use the first modality in your script code)
     * If an empty lists was passed, it will return an empty string.
     */
    public static determineDominantModalityFromList(modalityList) {

        for (modality in hierarchicalModalityList) {
            if (modalityList.contains(modality)) {
                return modality
            }
        }

        return ""
    }

}
