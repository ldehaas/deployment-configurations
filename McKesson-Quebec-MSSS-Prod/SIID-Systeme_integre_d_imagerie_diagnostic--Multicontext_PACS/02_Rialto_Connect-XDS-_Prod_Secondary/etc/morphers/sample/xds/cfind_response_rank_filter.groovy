/* CFind Response RANK Filter
 *
 * Fetch Prior Studies workflow only.
 *
 * Runs on the complete list of study responses and filters them as needed.
 */

/* This defines the number of studies that will be given to a local PACS.
 * "null" means that there is no limit */
final MaxNumberOfStudiesToBeReturned = 2

if (MaxNumberOfStudiesToBeReturned != null) {
    inputs.sort(byRecency)
    
    return first(inputs, MaxNumberOfStudiesToBeReturned)
}

// no return means to accept all
