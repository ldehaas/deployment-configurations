
/*
 * this scripts modifies adhoc cfind requests. Often, this script is used to
 * infer missing information or sanitize quirky requests.
 */
// This script is used to add return keys from the local patient cfind
// To add a return key, just set the DICOM field name ('IssuerOfPatientID', 'PatientName', etc.) to a blank string
// See below for an example.

set(PatientName, '')

