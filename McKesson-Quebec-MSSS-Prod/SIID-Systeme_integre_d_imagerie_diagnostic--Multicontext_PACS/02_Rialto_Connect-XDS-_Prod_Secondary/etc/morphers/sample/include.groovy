
/*
 * this example demonstrates include or LOAD functionality
 *
 * to load this helper script from another script, ues
 * LOAD(path_to_this_file)
 * where the must path be a string, either absolute, or 
 * relative to the includer script
 *
 * ONLY classes can be included (e.g. no closures)
 *
 * Passing in all image headers as function arguments is a little tricky.
 * You have to know the underlying variable that holds all the data.
 * Usually it's called 'input' or 'output', usually.
 */
         
// for example, constants:
// by default everything is public
class Constants {
    static final LocalAETitleIssuerLookup = [
        "CLIENT0":"issuer0",
        "CLIENT1":"issuer1",
    ]
    
    static final LocalIssuers = LocalAETitleIssuerLookup.values()
    static final PrimaryLocalIssuerID = LocalIssuers[0]
}

// helper methods
class Pids {
    /**
     * this is a commonly needed localize function. it looks through all the 
     * issuers of patient ids, and check if they are local accordingly to 
     * Constants.LocalIssuers. It would either return the local patient id if
     * there is one, or prefix the source patient id to generate a new local
     * patient id.
     *
     * @return a two element array containing the localized pid and issuer
     * @param qualifiedSourcePid
     *      a two element array containing source pid and source issuer
     * @param qualifiedOtherPids
     *      an array of other qualified pids, usually obtained from calling
     *      getAllPidsQualified()
     */
    static localize(qualifiedSourcePid, qualifiedOtherPids) {
        def sourcePid = qualifiedSourcePid[0]
        def sourceIssuer = qualifiedSourcePid[1]
     
        if ( Constants.LocalIssuerIDs.contains(sourceIssuer) ) {
            // this image originated from the local PACS
            return qualifiedSourcePid;
        }
    
        def foundPid = null
        for (def pid : qualifiedOtherPids) {
            if ( Constants.LocalIssuerIDs.contains(pid[1]) ) {
                foundPid = pid
                break
            }
        }
    
        // no linked pid found, create one by prefixing
        if (foundPid == null) {
            foundPid = [ sourceIssuer+"_"+sourcePid, Constants.PrimaryLocalIssuerID]
        }
        
        return foundPid
    }
}
