
/*
 * this scripts modifies adhoc cfind requests. Often, this script is used to
 * infer missing information or sanitize quirky requests.
 */

// for example infer issuer from ae title

/*
def issuers = ['AE1':'Issuer1', 'AE2':'Issuer2']
def ae = getCallingAETitle()
def issuer = issuers[ae]

if (issuer == null) {
    // set a default issuer or throw an exception
}

set(IssuerOfPatientID, issuer)
*/

