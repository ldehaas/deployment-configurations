class SharedHl7Constants {
    static final supported_ADT_subtypes = ['A01','A04','A05','A08','A31']

    static final RISprefixes_Applications = ['RADIMAGE', 'RADIMAGE', 'RADIMAGE', 'RADIMAGE', 'RADIMAGE', 'RADIMAGE', 'RADIMAGE', 'RADIMAGE', 'RADIMAGE', 'RADIMAGE', 'RADIMAGE', 'RADIMAGE']

    static final RISprefixes_Facilities = ['602', '604', '622', '632', '635', '642', '649', '653', '656', '677', '681', 'ICM']
}

class SharedHL7Resources {
    
    static final PrefixesAreIncorrect(prefixNumber, log) {
    // Sanity Check -- Some sites have blank prefixes, but null should be illegal

        def receivingFacility = SharedHl7Constants.RISprefixes_Facilities[prefixNumber]
        def receivingApplication = SharedHl7Constants.RISprefixes_Applications[prefixNumber]
        
        if(receivingFacility == null || receivingApplication == null) {
            log.error("Unexpected NULL Receiving or Sending Application")
            return true 
        }

        return false
    }
}


class QuebecHL7Validation {
    static final  RAMQformatIsInvalid(Potential_RAMQ){
    /*
    *  This function validates that the format of a passed potential RAMQ actually conforms to the RAMQ creation algorythm.
    *   This function is needed in order to protect the PIX lookup mechanism, which is what eventually is used to query
    *   the XDS Registry.
    *
    *  Please note that the function actually evaluates for INVALID data, meaning that it will return true if the
    *   RAMQ passed is invalid.  This is coded in this way for better readability in the calling script.
    *
    *  The function follows the RAMQ formula, which is as follows:
    *       the first three letters of the last name;
    *       the first letter of the first name;
    *       the last two digits of the year of birth;
    *       the month of birth (to which 50 is added to indicate female);
    *       the day of birth;
    *       a two digit administrative code used by the Rée.
    *
    *       Expressed in Regex (simple): /[A-Z]{4}[0-9]{8}/
    *       that is, 4 upper case digits followed by 8 numbers
    *
    *       To implement the full rule:
    *       1-12 can be expressed as (0[1-9]|1[0-2])
    *       similarly, 51-62 can be expressed as (5[1-9]|6[0-2])
    *       and together: (0[1-9]|1[0-2]|5[1-9]|6[0-2])
    *       or more compactly: ([05][0-9]|[16][0-2])
    *
    *       0-31 can be ([0-2][0-9]|3[01])
    *       so all together: /[A-Z]{4}[0-9]{2}([05][0-9]|[16][0-2])([0-2][0-9]|3[01])[0-9]{2}/
    *       the | means "or"
    */

        if(Potential_RAMQ ==~ /[A-Za-z]{4}[0-9]{2}([05][1-9]|[16][0-2])([0-2][0-9]|3[01])[0-9]{2}/ ) {
            return false
        } else {
            return true
        }
    } // End of RAMQformatIsValid function

} // End of QuebecHL7Validation class
