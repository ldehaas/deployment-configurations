
/**
 * Representation of a single context.  See Contexts class below for
 * a set of instances representing the contexts for this deployment.
 */
class Context {
    /** AE title of Rialto's CMove interface */
    String RialtoRetrieveAE

    String InstitutionName

    /** Assigning Authority for the local PACS */
    String PACSIssuer

    /** The calling ae title used by the PACS for query/retrieve.
     * Get this from the connectivity spreadsheet or the SAD */
    String PACSCallingAE

    /** Context ID as known by the PACS; returned as IssuerOfPatientID
     * in cfind queries; used to identify contexts in demographics queries */
    String ContextID

    /** The HL7 sending facilities for the local RIS.
     * The first one will be used when we need to set the value somewhere. */
    List RISSendingFacility

    /** The HL7 sending applications for the local RIS.
     * The first one will be used when we need to set the value somewhere. */
    List RISSendingApplication

    String HashPrefix

    public String localizeUID(uid, hash) {
        if (HashPrefix == null || uid == null) {
            return uid
        }

        return hash(HashPrefix, uid)
    }

    public String unlocalizeUID(uid, unhash) {
        //  if we're not hashing, we're also not unhashing:
        if (HashPrefix == null) {
            return uid
        }

        return unhash(uid)
    }

    /**
     * @return the preferred sending application for this context's RIS
     */
    public String preferredRISSendingApplication() {
        return RISSendingApplication.isEmpty() ? null : RISSendingApplication[0]
    }

    /**
     * @return the preferred sending facility for this context's RIS
     */
    public String preferredRISSendingFacility() {
        return RISSendingFacility.isEmpty() ? null : RISSendingFacility[0]
    }

    /**
     * @return if the issuer is local to the PACS
     */
    public boolean isLocal(issuer) {
        if (Contexts.CONTEXTS_ARE_ISOLATED) {
            return issuer == PACSIssuer
        } else {
            return Contexts.ALL_CONTEXTS.any { id, c -> c.PACSIssuer == issuer }
        }
    }
}

class Contexts {
    /**
     * If true, the contexts in the PACS cannot see eachother's images and
     * we should fetch each study into each context.  In this case the hash
     * prefix should also be set.
     *
     * If false, they can see eachother's images.  We should not fetch any
     * studies that are local to any of the contexts.  Hashing is unnecessary.
     *
     * See Context.isLocal above.
     */
    public static final CONTEXTS_ARE_ISOLATED = true

    // Add one entry per context here.  The key should
    // correspond to the ContextID configuration parameter.
    // Set the HashPrefix for multi-context sites and leave
    // it null for single-context deployments.
    private static final ALL_CONTEXTS = [
        "IPPM": new Context(
            RialtoRetrieveAE:       "RIALTO_IPPM",
            InstitutionName:        "PP-IPPM-Institut Philippe-Pinel de Montréal",
            PACSIssuer:             "2.16.124.10.101.1.60.1.1033.1",
            PACSCallingAE:          "ALI_SCU_IPPM",
            ContextID:              "632",
            RISSendingFacility:     ["632"],
            RISSendingApplication:  ["RADIMAGE"],
            HashPrefix:             "2.16.124.113638.4.2.2"),
        "PTME": new Context(
            RialtoRetrieveAE:       "RIALTO_PTME",
            InstitutionName:        "PP-PTME-CLSC de Pointe-aux-Trembles-Montréal-Est",
            PACSIssuer:             "2.16.124.10.101.1.60.1.1027.1",
            PACSCallingAE:          "ALI_SCU_PTME",
            ContextID:              "681",
            RISSendingFacility:     ["681"],
            RISSendingApplication:  ["RADIMAGE"],
            HashPrefix:             "2.16.124.113638.4.2.3"),
        "HDOU": new Context(
            RialtoRetrieveAE:       "RIALTO_HDOU",
            InstitutionName:        "PP-HDOU-Hôpital Douglas",
            PACSIssuer:             "2.16.124.10.101.1.60.1.3000.102.10.1074.1",
            PACSCallingAE:          "ALI_SCU_HDOU",
            ContextID:              "604",
            RISSendingFacility:     ["604"],
            RISSendingApplication:  ["RADIMAGE"],
            HashPrefix:             "2.16.124.113638.4.2.4"),
        "IUGM": new Context(
            RialtoRetrieveAE:       "RIALTO_IUGM",
            InstitutionName:        "PP-IUGM-Institut universitaire de gériatrie de Montréal",
            PACSIssuer:             "2.16.124.10.101.1.60.1.1034.1",
            PACSCallingAE:          "ALI_SCU_IUGM",
            ContextID:              "653",
            RISSendingFacility:     ["653"],
            RISSendingApplication:  ["RADIMAGE"],
            HashPrefix:             "2.16.124.113638.4.2.5"),
        "HNDM": new Context(
            RialtoRetrieveAE:       "RIALTO_HNDM",
            InstitutionName:        "PP-HNDM-Centre d'hébergement Notre-Dame-de-la-Merci",
            PACSIssuer:             "2.16.124.10.101.1.60.1.1036.1",
            PACSCallingAE:          "ALI_SCU_HNDM",
            ContextID:              "677",
            RISSendingFacility:     ["677"],
            RISSendingApplication:  ["RADIMAGE"],
            HashPrefix:             "2.16.124.113638.4.2.6"),
        "HCB": new Context(
            RialtoRetrieveAE:       "RIALTO_HCB",
            InstitutionName:        "PP-HCB-Hôpital Catherine-Booth",
            PACSIssuer:             "2.16.124.10.101.1.60.1.1035.1",
            PACSCallingAE:          "ALI_SCU_HCB",
            ContextID:              "602",
            RISSendingFacility:     ["602"],
            RISSendingApplication:  ["RADIMAGE"],
            HashPrefix:             "2.16.124.113638.4.2.7"),
        "CHMS": new Context(
            RialtoRetrieveAE:       "RIALTO_CHMS",
            InstitutionName:        "PP-CHMS-Hôpital Mont-Sinaï",
            PACSIssuer:             "2.16.124.10.101.1.60.1.1026.1",
            PACSCallingAE:          "ALI_SCU_CHMS",
            ContextID:              "656",
            RISSendingFacility:     ["656"],
            RISSendingApplication:  ["RADIMAGE"],
            HashPrefix:             "2.16.124.113638.4.2.8"),
        "HCIM": new Context(
            RialtoRetrieveAE:       "RIALTO_HCIM",
            InstitutionName:        "PP-HCIM-L'hôpital chinois de Montréal (1963)",
            PACSIssuer:             "2.16.124.10.101.1.60.1.1028.1",
            PACSCallingAE:          "ALI_SCU_HCIM",
            ContextID:              "622",
            RISSendingFacility:     ["622"],
            RISSendingApplication:  ["RADIMAGE"],
            HashPrefix:             "2.16.124.113638.4.2.9"),
        "HLHL": new Context(
            RialtoRetrieveAE:       "RIALTO_HLHL",
            InstitutionName:        "PP-HLHL-Institut universitaire en santé mentale de Montréal",
            PACSIssuer:             "2.16.124.10.101.1.60.1.1030.1",
            PACSCallingAE:          "ALI_SCU_HLHL",
            ContextID:              "642",
            RISSendingFacility:     ["642"],
            RISSendingApplication:  ["RADIMAGE"],
            HashPrefix:             "2.16.124.113638.4.2.10"),  
         "IRGLM": new Context(
            RialtoRetrieveAE:       "RIALTO_IRGLM",
            InstitutionName:        "PP-IRGLM-Institut de réadaptation de Montréal",
            PACSIssuer:             "2.16.124.10.101.1.60.1.1032.1",
            PACSCallingAE:          "ALI_SCU_IRGLM",
            ContextID:              "635",
            RISSendingFacility:     ["635"],
            RISSendingApplication:  ["RADIMAGE"],
            HashPrefix:             "2.16.124.113638.4.2.11"), 
         "SMSL": new Context(
            RialtoRetrieveAE:       "RIALTO_SMSL",
            InstitutionName:        "PP-SMSL-CLSC de Saint-Léonard et Saint-Michel",
            PACSIssuer:             "2.16.124.10.101.1.60.1.1037.1",
            PACSCallingAE:          "ALI_SCU_SMSL",
            ContextID:              "649",
            RISSendingFacility:     ["649"],
            RISSendingApplication:  ["RADIMAGE"],
            HashPrefix:             "2.16.124.113638.4.2.12"), 
         "ICM": new Context(
            RialtoRetrieveAE:       "RIALTO_ICM",
            InstitutionName:        "PP-ICM-Institut de cardiologie de Montréal",
            PACSIssuer:             "2.16.124.10.101.1.60.1.3000.102.10.1078.1",
            PACSCallingAE:          "ALI_SCU_ICM",
            ContextID:              "ICM",
            RISSendingFacility:     ["ICM"],
            RISSendingApplication:  ["RADIMAGE"],
            HashPrefix:             "2.16.124.113638.4.2.13"),
    ]

    /**
     * Returns the context configuration for the given contextId
     * @return null if the context id isn't known
     */
    public static Context getContext(contextId) {
        // single context case, context id might not be configured
        if (contextId == null && ALL_CONTEXTS.size() == 1) {
            // return the only value:
            return ALL_CONTEXTS.findResult { it.value }
        }

        // If this returns null, we're almost certainly misconfigured
        // so we should blow up.  However, the best way to do that might
        // be to have the caller simply return false to drop the current
        // data, so we have to let the caller handle it.
        return ALL_CONTEXTS[contextId]
    }
}

