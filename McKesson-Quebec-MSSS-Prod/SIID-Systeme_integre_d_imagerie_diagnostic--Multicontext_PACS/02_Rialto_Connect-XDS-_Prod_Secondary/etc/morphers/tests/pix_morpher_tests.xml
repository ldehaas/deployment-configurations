<tests service="connect-xds" script="PIXMorpher" file="pix_morpher.groovy">
  <test name="unsupportedTriggerEvent">
    <inputs>
      <hl7 name="input">
        MSH|^~\&amp;|RAD.BGH^^|BGH|||201108020914||ADT^A99||P|2.3|
        PID|1|ABCD82121300|mrn|
        MRG|oldpid|
      </hl7>
    </inputs>

    <assertions>
      assert returned == false
    </assertions>
  </test>

  <test name="setDomains">
    <inputs>
      <hl7 name="input">
        MSH|^~\&amp;|RAD.BGH^^|BGH|||201108020914||ADT^A01||P|2.3|
        PID|1|ABCD82121300|mrn|
      </hl7>
      <string name="context">Gatineau</string>
    </inputs>

    <assertions>
        assert returned != false
        assert get('PID-2-4-2') == '2.16.124.10.101.1.60.100'
        assert get('PID-3') == 'MRN'
        assert get('PID-3-4-2') == '2.16.124.10.101.1.60.1.1008.1'
    </assertions>
  </test>

  <test name="invalidRAMQ">
    <inputs>
      <hl7 name="input">
        MSH|^~\&amp;|RAD.BGH^^|BGH|||201108020914||ADT^A01||P|2.3|
        PID|1|ramq|mrn|
      </hl7>
      <string name="context">Gatineau</string>
    </inputs>

    <assertions>
        assert returned == false
    </assertions>
  </test>

  <test name="wrongRAMQDomain">
    <inputs>
      <hl7 name="input">
        MSH|^~\#|RAD.BGH^^|BGH|||201108020914||ADT^A01||P|2.3|
        PID|1|ABCD82121300^^^#notramqdomain|mrn|
      </hl7>
      <string name="context">Gatineau</string>
    </inputs>

    <assertions>
        assert returned == false
    </assertions>
  </test>

  <test name="wrongMRNDomain">
    <inputs>
      <hl7 name="input">
        MSH|^~\#|RAD.BGH^^|BGH|||201108020914||ADT^A01||P|2.3|
        PID|1|ABCD82121300|mrn^^^#notlocaldomain|
      </hl7>
      <string name="context">Gatineau</string>
    </inputs>

    <assertions>
        assert returned == false
    </assertions>
  </test>

  <test name="missingRAMQ">
    <inputs>
      <hl7 name="input">
        MSH|^~\#|RAD.BGH^^|BGH|||201108020914||ADT^A01||P|2.3|
        PID|1||mrn|
      </hl7>
      <string name="context">Gatineau</string>
    </inputs>
    <assertions>
        assert returned == false
    </assertions>
  </test>

  <test name="missingPATSEC">
    <inputs>
      <hl7 name="input">
        MSH|^~\#|RAD.BGH^^|BGH|||201108020914||ADT^A01||P|2.3|
        PID|1|GOSJ36621514|mrn||
      </hl7>
      <string name="context">Gatineau</string>
    </inputs>
    <assertions>
        assert returned != false
    </assertions>
  </test>

  <test name="missingRAMQandPATSEC">
    <inputs>
      <hl7 name="input">
        MSH|^~\#|RAD.BGH^^|BGH|||201108020914||ADT^A01||P|2.3|
        PID|1||mrn||
      </hl7>
      <string name="context">Gatineau</string>
    </inputs>
    <assertions>
        assert returned == false
    </assertions>
  </test>

  <test name="clearPID4">
    <inputs>
      <hl7 name="input">
        MSH|^~\#|RAD.BGH^^|BGH|||201108020914||ADT^A01||P|2.3|
        PID|1|GOSJ36621514|mrn|unknown|
      </hl7>
      <string name="context">Gatineau</string>
    </inputs>
    <assertions>
        assert returned != false

        assert get('PID-4') == null
    </assertions>
  </test>

  <test name="leavePID4withDomain">
    <inputs>
      <hl7 name="input">
        MSH|^~\#|RAD.BGH^^|BGH|||201108020914||ADT^A01||P|2.3|
        PID|1|GOSJ36621514|mrn|unknown^^^#domain|
      </hl7>
      <string name="context">Gatineau</string>
    </inputs>
    <assertions>
        assert returned != false

        assert get('PID-4') == 'UNKNOWN'
        assert get('PID-4-4-2') == 'domain'
    </assertions>
  </test>

  <test name="clearPID4withOnlyDomain">
    <inputs>
      <hl7 name="input">
        MSH|^~\#|RAD.BGH^^|BGH|||201108020914||ADT^A01||P|2.3|
        PID|1|GOSJ36621514|mrn|^^^#domain|
      </hl7>
      <string name="context">Gatineau</string>
    </inputs>
    <assertions>
        assert returned != false

        assert get('PID-4') == null
        assert get('PID-4-4-1') == null
        assert get('PID-4-4-2') == null
        assert get('PID-4-4-3') == null
    </assertions>
  </test>

  <test name="clearPID4withOnlyNamespace">
    <inputs>
      <hl7 name="input">
        MSH|^~\#|RAD.BGH^^|BGH|||201108020914||ADT^A01||P|2.3|
        PID|1|GOSJ36621514|mrn|^^^ns|
      </hl7>
      <string name="context">Gatineau</string>
    </inputs>
    <assertions>
        assert returned != false

        assert get('PID-4') == null
        assert get('PID-4-4-1') == null
        assert get('PID-4-4-2') == null
        assert get('PID-4-4-3') == null
    </assertions>
  </test>

  <test name="missingMRN">
    <inputs>
      <hl7 name="input">
        MSH|^~\#|RAD.BGH^^|BGH|||201108020914||ADT^A01||P|2.3|
        PID|1|ABCD82121300||
      </hl7>
      <string name="context">Gatineau</string>
    </inputs>
    <assertions>
        assert returned == false
    </assertions>
  </test>

  <test name="order">
    <inputs>
      <hl7 name="input">
        MSH|^~\#|RAD.BGH^^|BGH|||201108020914||ORM^R01||P|2.3|
        PID|1|ABCD82121300|mrn|
      </hl7>
      <string name="context">Gatineau</string>
    </inputs>

    <assertions>
        assert returned != false

        assert get('MSH-9-1') == 'ORM'
        assert get('PID-2-4-2') == '2.16.124.10.101.1.60.100'
    </assertions>
  </test>

  <test name="merge">
    <inputs>
      <hl7 name="input">
        MSH|^~\#|RAD.BGH^^|BGH|||201108020914||ADT^A34||P|2.3|
        PID|1|ABCD82121300|mrn|
        MRG|mrg|
      </hl7>
      <string name="context">Gatineau</string>
    </inputs>

    <assertions>
        assert returned != false

        assert get('MRG-1-4-2') == '2.16.124.10.101.1.60.1.1008.1'
    </assertions>
  </test>

  <test name="mergeMissingMRG1">
    <inputs>
      <hl7 name="input">
        MSH|^~\#|RAD.BGH^^|BGH|||201108020914||ADT^A34||P|2.3|
        PID|1|ABCD82121300|mrn|
        MRG||wrongfield|
      </hl7>
      <string name="context">Gatineau</string>
    </inputs>

    <assertions>
        assert returned == false
    </assertions>
  </test>

  <!--
    Tests to do:
    - set character encoding
   -->
</tests>

