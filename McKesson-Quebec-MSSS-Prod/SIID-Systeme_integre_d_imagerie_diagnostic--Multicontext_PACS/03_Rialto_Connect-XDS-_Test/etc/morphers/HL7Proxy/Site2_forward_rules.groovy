/*
*  Specific Site Forwarder: Only forward traffic meant for the Rialto Connect instance on Site2
*/

LOAD("../Shared_Scripting_Files/shared_hl7_constants.groovy")
LOAD("../Shared_Scripting_Files/shared_constants.groovy")

def expectedReceivingApplication = SharedConstants.Local_RIS_Sending_Facilities[1]
def expectedReceivingFacility =  SharedConstants.Local_RIS_Sending_Applications[1]

def actualReceivingApplication = get(SharedConstants.HL7_Sending_Application_Field)
def actualReceivingFacility = get(SharedConstants.HL7_Sending_Facility_Field)

def actualFacilityMatchesExpectedFacility = false
def actualApplicationMatchesExpectedApplication = false

if(actualReceivingFacility != null && actualReceivingFacility == expectedReceivingFacility) {
    actualFacilityMatchesExpectedFacility == true
}

if(actualReceivingApplication != null && actualReceivingApplication == expectedReceivingApplication) {
    actualApplicationMatchesExpectedApplication = true
}

if(actualFacilityMatchesExpectedFacility && actualApplicationMatchesExpectedApplication) {
    log.trace("Forwarding HL7 message to Site2. {}", output)
} else {
    return false
}
