/* fps_inbound_messages_morpher.groovy
 *
 * This script cleans up the HL7 Traffic that goes to the Fetch Prior Studies handler
 */

LOAD("shared_scripting_files/shared_constants.groovy")
LOAD("shared_scripting_files/shared_hl7_constants.groovy")

log.debug("Starting fps_inbound_messages_morpher")
log.debug("Message as received at fps inbound messages morpher: {}", input)

def messageCode = get('MSH-9-1')
log.debug("Local Fetch Prior Studies Forwarder: Received MSH-9-1 " +
    "(Message Code) was: '{}'", messageCode)


// HL7 Message validation
if (!["ORM", "ADT"].contains(messageCode)){
    log.debug("Non-ORM or ADT Message Received")
    return false
}

def affinityPID = get(SharedConstants.HL7_Affinity_Domain_PID_Field)

// Ensuring that orders that have a null value still go through, as the Order 
// cache needs to be populated. Later on, we might receive an ADT that will free
// the orders from the cache
if (affinityPID != null && !affinityPID.isEmpty()) {
    if (QuebecHL7Validation.RAMQformatIsInvalid(affinityPID)){
        log.error("Cannot process Fetch Prior Studies, as the HL7 message " +
            "has an Invalid RAMQ value: '{}'", affinityPID)
        return false
    }
}


// Add the default Character set that can handle French Characters, and that 
// many times is not explicit in the RIS Feed
def characterEncoding = get("MSH-18")
if (characterEncoding == null || characterEncoding.isEmpty()){
    log.debug("No character set specified.  Hard-coding to 8859/1")
    set('MSH-18', '8859/1')
}


// Enforce that AT LEAST the MRN exists in the incoming messages

def mrn = get(SharedConstants.HL7_Local_MRN_PID_Field)

log.debug("Current MRN received is: '{}'", mrn)
log.debug("Current RAMQ received is: '{}'", affinityPID)


// Enforce that AT LEAST the MRN exists
if (mrn != null && !mrn.isEmpty()) {
    mrn = mrn.toUpperCase()
    set(SharedConstants.HL7_Local_MRN_PID_Field, mrn)

} else {
    log.error("Received Message containst NO local MRN.  Ignoring this HL7 Message")
    return false
}


// Ensure that BOTH the Local MRN and the Affinity Domain (RAMQ) are fully qualified

def mrnDomain = get(SharedConstants.HL7_Local_MRN_Issuer_of_PID_Field)
def affinityDomain = get(SharedConstants.HL7_Affinity_Domain_Issuer_of_PID_Field)

log.debug("Current mrn domain is: '{}'", mrnDomain)
log.debug("Current affinity domain (RAMQ) is: '{}'", affinityDomain)


// Hard code Issuer of PIDs for both MRN and Affinity Domain PID

if (mrnDomain != null && !mrnDomain.isEmpty()) {
    if (SharedConstants.Local_PACS_Issuer_of_Patient_ID != mrnDomain) { 
        // This means that the domain passed by RIS does not match the one known as correct
        // THIS ONLY WORKS IN Connect Instances attached to SINGLE-CONTEXT PACS
        log.warn("This RIS Feed is sending feeds for more than 1 local domain. " +
            "Patient Lookups might fail and go to RIS Database!")

    } else {
        log.debug("The Issuer of PID matches what's expected")
    }

} else {
    // If the local domain is empty, populate it from known value
    mrnDomain = SharedConstants.Local_PACS_Issuer_of_Patient_ID
    log.debug("Populating mrn domain from Local PACS as [{}]", mrnDomain)
}


if (affinityDomain != null && !affinityDomain.isEmpty()) {
    if (SharedConstants.Affinity_Domain_OID != affinityDomain.toUpperCase()) {  
        // i.e. RAMQ Issuer Of PID is present, but not correct
        log.warn("Invalid Affinity Domain (RAMQ) Issuer of Patient ID '{}'. " +
            "Populating to '{}'",
                 affinityDomain, SharedConstants.Affinity_Domain_OID)
        affinityDomain = SharedConstants.Affinity_Domain_OID 
    } else {
        log.debug("The Affinity Domain Issuer of Patient ID is what is expected")
    }

} else {
    // Populate with known value if empty
    affinityDomain = SharedConstants.Affinity_Domain_OID
    log.debug("Populating Issuer of Patient ID for AffinityDomain Patient " +
        "(RAMQ) as '{}'", affinityDomain)
}

// Set the value of the MRN and AffinityDomain PID's Issuers of Patient ID where
// the PIX Server is expecting it
set('PID-3-4-2', mrnDomain)
set('PID-2-4-2', affinityDomain)

