
/**
 * Rules table and evaluation logic for prior studies.
 *
 * For each modality and anatomical region pair, there are a set of sub rules.
 * Each subrule defines the modality and anatomic region of a candidate prior,
 * as well as the maximum age and maximum number of the prior.
 */
class PriorsRules {
    private static rules = [

      //  Rules for CT and MR

      [ modality : [ "CT", "MR" ], anatomicRegion : "TC", subrules :
        [
          [ modality : [ "CT", "MR" ], anatomicRegion : "TC", maxAge : 5, newest : 6, oldest : 0 ],
        ],
      ],
      [ modality : [ "CT", "MR" ], anatomicRegion : "DI", subrules :
        [
          [ modality : [ "CT", "MR" ], anatomicRegion : "DI", maxAge : 0, newest : 0, oldest : 0 ],
          [ modality : [ "CT", "MR" ], anatomicRegion : "AB", maxAge : 0, newest : 0, oldest : 0 ],
          [ modality : [ "CR" ], anatomicRegion : "DI", maxAge : 0, newest : 0, oldest : 0 ],
          [ modality : [ "CR" ], anatomicRegion : "AB", maxAge : 0, newest : 0, oldest : 0 ],
          [ modality : [ "RF" ], anatomicRegion : "DI", maxAge : 0, newest : 0, oldest : 0 ],
          [ modality : [ "US" ], anatomicRegion : "AB", maxAge : 0, newest : 0, oldest : 0 ],
          [ modality : [ "US" ], anatomicRegion : "DI", maxAge : 0, newest : 0, oldest : 0 ],
        ],
      ],
      [ modality : [ "CT" ], anatomicRegion : "TH", subrules :
        [
          [ modality : [ "CT" ], anatomicRegion : "TH", maxAge : 0, newest : 2, oldest : 1 ],
        ],
      ],
      [ modality : [ "MR" ], anatomicRegion : "TH", subrules :
        [
          [ modality : [ "MR" ], anatomicRegion : "TH", maxAge : 0, newest : 1, oldest : 0 ],
        ],
      ],
      [ modality : [ "CT", "MR" ], anatomicRegion : "AB", subrules :
        [
          [ modality : [ "CT", "MR" ], anatomicRegion : "DI", maxAge : 0, newest : 0, oldest : 0 ],
          [ modality : [ "CT", "MR" ], anatomicRegion : "AB", maxAge : 0, newest : 0, oldest : 0 ],
          [ modality : [ "CR" ], anatomicRegion : "DI", maxAge : 0, newest : 0, oldest : 0 ],
          [ modality : [ "RF" ], anatomicRegion : "DI", maxAge : 0, newest : 0, oldest : 0 ],
          [ modality : [ "US" ], anatomicRegion : "AB", maxAge : 0, newest : 0, oldest : 0 ],
          [ modality : [ "US" ], anatomicRegion : "DI", maxAge : 0, newest : 0, oldest : 0 ],
        ],
      ],
      [ modality : [ "CT", "MR" ], anatomicRegion : "CO", subrules :
        [
          [ modality : [ "CT", "MR" ], anatomicRegion : "CO", maxAge : 5, newest : 6, oldest : 0 ],
          [ modality : [ "CR" ], anatomicRegion : "CO", maxAge : 2, newest : 4, oldest : 0 ],
          [ modality : [ "RF" ], anatomicRegion : "CO", maxAge : 2, newest : 2, oldest : 0 ],
        ],
      ],
      [ modality : [ "CT", "MR" ], anatomicRegion : "ES", subrules :
        [
          [ modality : [ "CT", "MR" ], anatomicRegion : "ES", maxAge : 5, newest : 6, oldest : 0 ],
          [ modality : [ "CR" ], anatomicRegion : "ES", maxAge : 2, newest : 8, oldest : 0 ],
          [ modality : [ "US" ], anatomicRegion : "ES", maxAge : 2, newest : 2, oldest : 0 ],
        ],
      ],
      [ modality : [ "CT", "MR" ], anatomicRegion : "EI", subrules :
        [
          [ modality : [ "CT", "MR" ], anatomicRegion : "EI", maxAge : 5, newest : 6, oldest : 0 ],
          [ modality : [ "CR" ], anatomicRegion : "EI", maxAge : 2, newest : 8, oldest : 0 ],
          [ modality : [ "US" ], anatomicRegion : "EI", maxAge : 2, newest : 2, oldest : 0 ],
        ],
      ],
      [ modality : [ "CT", "MR" ], anatomicRegion : "CA", subrules :
        [
          [ modality : [ "CT", "MR" ], anatomicRegion : "CA", maxAge : 5, newest : 6, oldest : 0 ],
          [ modality : [ "CR" ], anatomicRegion : "TH", maxAge : 2, newest : 6, oldest : 0 ],
        ],
      ],
      [ modality : [ "MR" ], anatomicRegion : "SE", subrules :
        [
          [ modality : [ "MR" ], anatomicRegion : "SE", maxAge : 5, newest : 6, oldest : 0 ],
          [ modality : [ "MG" ], anatomicRegion : "SE", maxAge : 5, newest : 6, oldest : 0 ],
          [ modality : [ "US" ], anatomicRegion : "SE", maxAge : 5, newest : 6, oldest : 0 ],
        ],
      ],
      [ modality : [ "CT", "MR" ], anatomicRegion : "GO", subrules :
        [
          [ modality : [ "CT", "MR" ], anatomicRegion : "GO", maxAge : 5, newest : 6, oldest : 0 ],
          [ modality : [ "CR" ], anatomicRegion : "GO", maxAge : 2, newest : 2, oldest : 0 ],
          [ modality : [ "US" ], anatomicRegion : "GO", maxAge : 2, newest : 8, oldest : 0 ],
        ],
      ],

      // Rules for CR

      [ modality : [ "CR" ], anatomicRegion : "TC", subrules :
        [
          [ modality : [ "CR" ], anatomicRegion : "TC", maxAge : 5, newest : 1, oldest : 0 ],
        ],
      ],
      [ modality : [ "CR" ], anatomicRegion : "DI", subrules :
        [
          [ modality : [ "CR" ], anatomicRegion : "AB", maxAge : 0, newest : 1, oldest : 0 ],
          [ modality : [ "US" ], anatomicRegion : "AB", maxAge : 0, newest : 0, oldest : 0 ],
          [ modality : [ "US" ], anatomicRegion : "DI", maxAge : 0, newest : 0, oldest : 0 ],
        ],
      ],
      [ modality : [ "CR" ], anatomicRegion : "TH", subrules :
        [
          [ modality : [ "CR" ], anatomicRegion : "TH", maxAge : 0, newest : 2, oldest : 1 ],
        ],
      ],
      [ modality : [ "CR" ], anatomicRegion : "AB", subrules :
        [
          [ modality : [ "CR" ], anatomicRegion : "AB", maxAge : 0, newest : 1, oldest : 0 ],
          [ modality : [ "US" ], anatomicRegion : "AB", maxAge : 0, newest : 0, oldest : 0 ],
          [ modality : [ "US" ], anatomicRegion : "DI", maxAge : 0, newest : 0, oldest : 0 ],
        ],
      ],
      [ modality : [ "CR" ], anatomicRegion : "CO", subrules :
        [
          [ modality : [ "CR" ], anatomicRegion : "CO", maxAge : 5, newest : 3, oldest : 0 ],
          [ modality : [ "RF" ], anatomicRegion : "CO", maxAge : 2, newest : 2, oldest : 0 ],
        ],
      ],
      [ modality : [ "CR" ], anatomicRegion : "ES", subrules :
        [
          [ modality : [ "CR" ], anatomicRegion : "ES", maxAge : 5, newest : 10, oldest : 0 ],
        ],
      ],
      [ modality : [ "CR" ], anatomicRegion : "EI", subrules :
        [
          [ modality : [ "CR" ], anatomicRegion : "EI", maxAge : 5, newest : 10, oldest : 0 ],
        ],
      ],
      [ modality : [ "CR" ], anatomicRegion : "GO", subrules :
        [
          [ modality : [ "CR" ], anatomicRegion : "GO", maxAge : 1, newest : 2, oldest : 0 ],
          [ modality : [ "US" ], anatomicRegion : "GO", maxAge : 1, newest : 4, oldest : 0 ],
        ],
      ],
      [ modality : [ "CR" ], anatomicRegion : "SQ", subrules :
        [
          [ modality : [ "CR" ], anatomicRegion : "SQ", maxAge : 5, newest : 6, oldest : 0 ],
        ],
      ],

      // Rules for RF

      [ modality : [ "RF" ], anatomicRegion : "DI", subrules :
        [
          [ modality : [ "RF" ], anatomicRegion : "DI", maxAge : 5, newest : 6, oldest : 0 ],
          [ modality : [ "RF" ], anatomicRegion : "AB", maxAge : 5, newest : 6, oldest : 0 ],
          [ modality : [ "CR" ], anatomicRegion : "DI", maxAge : 2, newest : 4, oldest : 0 ],
          [ modality : [ "CR" ], anatomicRegion : "AB", maxAge : 2, newest : 4, oldest : 0 ],
          [ modality : [ "US" ], anatomicRegion : "AB", maxAge : 2, newest : 4, oldest : 0 ],
          [ modality : [ "US" ], anatomicRegion : "DI", maxAge : 2, newest : 4, oldest : 0 ],
        ],
      ],
      [ modality : [ "RF" ], anatomicRegion : "AB", subrules :
        [
          [ modality : [ "RF" ], anatomicRegion : "AB", maxAge : 5, newest : 6, oldest : 0 ],
          [ modality : [ "RF" ], anatomicRegion : "DI", maxAge : 5, newest : 6, oldest : 0 ],
          [ modality : [ "CR" ], anatomicRegion : "AB", maxAge : 2, newest : 4, oldest : 0 ],
          [ modality : [ "US" ], anatomicRegion : "AB", maxAge : 2, newest : 4, oldest : 0 ],
          [ modality : [ "US" ], anatomicRegion : "DI", maxAge : 2, newest : 4, oldest : 0 ],
        ],
      ],
      [ modality : [ "RF" ], anatomicRegion : "CO", subrules :
        [
          [ modality : [ "RF" ], anatomicRegion : "CO", maxAge : 5, newest : 10, oldest : 0 ],
          [ modality : [ "CR" ], anatomicRegion : "CO", maxAge : 2, newest : 2, oldest : 0 ],
        ],
      ],
      [ modality : [ "RF" ], anatomicRegion : "ES", subrules :
        [
          [ modality : [ "RF" ], anatomicRegion : "ES", maxAge : 5, newest : 4, oldest : 0 ],
          [ modality : [ "CR" ], anatomicRegion : "ES", maxAge : 2, newest : 2, oldest : 0 ],
        ],
      ],
      [ modality : [ "RF" ], anatomicRegion : "EI", subrules :
        [
          [ modality : [ "RF" ], anatomicRegion : "EI", maxAge : 5, newest : 4, oldest : 0 ],
          [ modality : [ "CR" ], anatomicRegion : "EI", maxAge : 5, newest : 6, oldest : 0 ],
        ],
      ],
      [ modality : [ "RF" ], anatomicRegion : "GO", subrules :
        [
          [ modality : [ "RF" ], anatomicRegion : "GO", maxAge : 2, newest : 2, oldest : 0 ],
          [ modality : [ "US" ], anatomicRegion : "GO", maxAge : 2, newest : 4, oldest : 0 ],
        ],
      ],
      [ modality : [ "RF" ], anatomicRegion : "SQ", subrules :
        [
          [ modality : [ "RF" ], anatomicRegion : "SQ", maxAge : 5, newest : 6, oldest : 0 ],
        ],
      ],

      // Rules for US

      [ modality : [ "US" ], anatomicRegion : "TC", subrules :
        [
          [ modality : [ "US" ], anatomicRegion : "TC", maxAge : 5, newest : 6, oldest : 0 ],
        ],
      ],
      [ modality : [ "US" ], anatomicRegion : "DI", subrules :
        [
          [ modality : [ "US" ], anatomicRegion : "DI", maxAge : 0, newest : 0, oldest : 0 ],
          [ modality : [ "US" ], anatomicRegion : "AB", maxAge : 0, newest : 0, oldest : 0 ],
        ],
      ],
      [ modality : [ "US" ], anatomicRegion : "AB", subrules :
        [
          [ modality : [ "US" ], anatomicRegion : "AB", maxAge : 0, newest : 0, oldest : 0 ],
          [ modality : [ "US" ], anatomicRegion : "DI", maxAge : 0, newest : 0, oldest : 0 ],
        ],
      ],
      [ modality : [ "US" ], anatomicRegion : "ES", subrules :
        [
          [ modality : [ "US" ], anatomicRegion : "ES", maxAge : 5, newest : 6, oldest : 0 ],
          [ modality : [ "CR" ], anatomicRegion : "ES", maxAge : 2, newest : 8, oldest : 0 ],
          [ modality : [ "RF" ], anatomicRegion : "ES", maxAge : 2, newest : 2, oldest : 0 ],
        ],
      ],
      [ modality : [ "US" ], anatomicRegion : "CA", subrules :
        [
          [ modality : [ "US" ], anatomicRegion : "CA", maxAge : 5, newest : 6, oldest : 0 ],
          [ modality : [ "CR" ], anatomicRegion : "TH", maxAge : 2, newest : 6, oldest : 0 ],
        ],
      ],
      [ modality : [ "US" ], anatomicRegion : "SE", subrules :
        [
          [ modality : [ "US" ], anatomicRegion : "SE", maxAge : 5, newest : 6, oldest : 0 ],
          [ modality : [ "MG" ], anatomicRegion : "SE", maxAge : 5, newest : 6, oldest : 0 ],
          [ modality : [ "MR" ], anatomicRegion : "SE", maxAge : 5, newest : 6, oldest : 0 ],
        ],
      ],
      [ modality : [ "US" ], anatomicRegion : "GO", subrules :
        [
          [ modality : [ "US" ], anatomicRegion : "GO", maxAge : 5, newest : 6, oldest : 0 ],
          [ modality : [ "CR" ], anatomicRegion : "GO", maxAge : 2, newest : 2, oldest : 0 ],
          [ modality : [ "MR" ], anatomicRegion : "GO", maxAge : 2, newest : 2, oldest : 0 ],
        ],
      ],

      // Rules for XA

      [ modality : [ "XA" ], anatomicRegion : "TC", subrules :
        [
          [ modality : [ "XA" ], anatomicRegion : "TC", maxAge : 5, newest : 6, oldest : 0 ],
          [ modality : [ "AN" ], anatomicRegion : "TC", maxAge : 5, newest : 6, oldest : 0 ],
          [ modality : [ "CT", "MR" ], anatomicRegion : "TC", maxAge : 2, newest : 4, oldest : 0 ],
          [ modality : [ "US" ], anatomicRegion : "TC", maxAge : 2, newest : 4, oldest : 0 ],
        ],
      ],
      [ modality : [ "XA" ], anatomicRegion : "DI", subrules :
        [
          [ modality : [ "XA" ], anatomicRegion : "DI", maxAge : 5, newest : 6, oldest : 0 ],
          [ modality : [ "AN" ], anatomicRegion : "DI", maxAge : 5, newest : 6, oldest : 0 ],
          [ modality : [ "CT", "MR" ], anatomicRegion : "DI", maxAge : 2, newest : 4, oldest : 0 ],
          [ modality : [ "CT", "MR" ], anatomicRegion : "AB", maxAge : 2, newest : 4, oldest : 0 ],
          [ modality : [ "US" ], anatomicRegion : "AB", maxAge : 2, newest : 4, oldest : 0 ],
          [ modality : [ "US" ], anatomicRegion : "DI", maxAge : 2, newest : 4, oldest : 0 ],
        ],
      ],
      [ modality : [ "XA" ], anatomicRegion : "TH", subrules :
        [
          [ modality : [ "XA" ], anatomicRegion : "TH", maxAge : 5, newest : 6, oldest : 0 ],
          [ modality : [ "AN" ], anatomicRegion : "TH", maxAge : 2, newest : 6, oldest : 0 ],
          [ modality : [ "CT", "MR" ], anatomicRegion : "TH", maxAge : 2, newest : 4, oldest : 0 ],
          [ modality : [ "CR" ], anatomicRegion : "TH", maxAge : 2, newest : 4, oldest : 0 ],
        ],
      ],
      [ modality : [ "XA" ], anatomicRegion : "AB", subrules :
        [
          [ modality : [ "XA" ], anatomicRegion : "DI", maxAge : 5, newest : 6, oldest : 0 ],
          [ modality : [ "AN" ], anatomicRegion : "AB", maxAge : 5, newest : 6, oldest : 0 ],
          [ modality : [ "CT", "MR" ], anatomicRegion : "AB", maxAge : 2, newest : 4, oldest : 0 ],
          [ modality : [ "CT", "MR" ], anatomicRegion : "DI", maxAge : 2, newest : 4, oldest : 0 ],
          [ modality : [ "US" ], anatomicRegion : "AB", maxAge : 2, newest : 4, oldest : 0 ],
          [ modality : [ "US" ], anatomicRegion : "DI", maxAge : 2, newest : 4, oldest : 0 ],
        ],
      ],
      [ modality : [ "XA" ], anatomicRegion : "CO", subrules :
        [
          [ modality : [ "XA" ], anatomicRegion : "CO", maxAge : 5, newest : 6, oldest : 0 ],
          [ modality : [ "AN" ], anatomicRegion : "CO", maxAge : 5, newest : 6, oldest : 0 ],
          [ modality : [ "CT", "MR" ], anatomicRegion : "CO", maxAge : 2, newest : 4, oldest : 0 ],
          [ modality : [ "CR" ], anatomicRegion : "CO", maxAge : 2, newest : 4, oldest : 0 ],
        ],
      ],
      [ modality : [ "XA" ], anatomicRegion : "ES", subrules :
        [
          [ modality : [ "XA" ], anatomicRegion : "ES", maxAge : 5, newest : 6, oldest : 0 ],
          [ modality : [ "AN" ], anatomicRegion : "ES", maxAge : 5, newest : 6, oldest : 0 ],
          [ modality : [ "CR" ], anatomicRegion : "ES", maxAge : 2, newest : 8, oldest : 0 ],
          [ modality : [ "CT", "MR" ], anatomicRegion : "ES", maxAge : 2, newest : 2, oldest : 0 ],
        ],
      ],
      [ modality : [ "XA" ], anatomicRegion : "CA", subrules :
        [
          [ modality : [ "XA" ], anatomicRegion : "CA", maxAge : 5, newest : 6, oldest : 0 ],
          [ modality : [ "XA" ], anatomicRegion : "HE", maxAge : 5, newest : 6, oldest : 0 ],
          [ modality : [ "CT", "MR" ], anatomicRegion : "CA", maxAge : 2, newest : 6, oldest : 0 ],
          [ modality : [ "CR" ], anatomicRegion : "CA", maxAge : 2, newest : 6, oldest : 0 ],
          [ modality : [ "CR" ], anatomicRegion : "TH", maxAge : 2, newest : 6, oldest : 0 ],
        ],
      ],
      [ modality : [ "XA" ], anatomicRegion : "HE", subrules :
        [
          [ modality : [ "XA" ], anatomicRegion : "HE", maxAge : 5, newest : 6, oldest : 0 ],
          [ modality : [ "XA" ], anatomicRegion : "CA", maxAge : 5, newest : 6, oldest : 0 ],
          [ modality : [ "CT", "MR" ], anatomicRegion : "CA", maxAge : 2, newest : 6, oldest : 0 ],
          [ modality : [ "CR" ], anatomicRegion : "CA", maxAge : 2, newest : 6, oldest : 0 ],
          [ modality : [ "CR" ], anatomicRegion : "TH", maxAge : 2, newest : 6, oldest : 0 ],
        ],
      ],
      [ modality : [ "XA" ], anatomicRegion : "AN", subrules :
        [
          [ modality : [ "XA" ], anatomicRegion : "AN", maxAge : 5, newest : 6, oldest : 0 ],
        ],
      ],

      // Rules for MG

      [ modality : [ "MG" ], anatomicRegion : "SE", subrules :
        [
          [ modality : [ "MG" ], anatomicRegion : "SE", maxAge : 5, newest : 10, oldest : 0 ],
          [ modality : [ "US" ], anatomicRegion : "SE", maxAge : 0, newest : 0, oldest : 0 ],
          [ modality : [ "MR" ], anatomicRegion : "SE", maxAge : 5, newest : 0, oldest : 0 ],
        ],
      ],

      // Rules for NM

      [ modality : [ "NM" ], anatomicRegion : "TC", subrules :
        [
          [ modality : [ "NM" ], anatomicRegion : "TC", maxAge : 5, newest : 6, oldest : 0 ],
          [ modality : [ "CT", "MR" ], anatomicRegion : "TC", maxAge : 2, newest : 4, oldest : 0 ],
          [ modality : [ "PT" ], anatomicRegion : "TP", maxAge : 5, newest : 4, oldest : 0 ],
        ],
      ],
      [ modality : [ "NM" ], anatomicRegion : "DI", subrules :
        [
          [ modality : [ "NM" ], anatomicRegion : "DI", maxAge : 5, newest : 6, oldest : 0 ],
          [ modality : [ "NM" ], anatomicRegion : "AB", maxAge : 5, newest : 6, oldest : 0 ],
          [ modality : [ "CT", "MR" ], anatomicRegion : "DI", maxAge : 2, newest : 4, oldest : 0 ],
          [ modality : [ "CT", "MR" ], anatomicRegion : "AB", maxAge : 2, newest : 4, oldest : 0 ],
          [ modality : [ "CR" ], anatomicRegion : "DI", maxAge : 2, newest : 4, oldest : 0 ],
          [ modality : [ "CR" ], anatomicRegion : "AB", maxAge : 2, newest : 4, oldest : 0 ],
          [ modality : [ "PT" ], anatomicRegion : "TP", maxAge : 5, newest : 4, oldest : 0 ],
        ],
      ],
      [ modality : [ "NM" ], anatomicRegion : "TH", subrules :
        [
          [ modality : [ "NM" ], anatomicRegion : "TH", maxAge : 5, newest : 6, oldest : 0 ],
          [ modality : [ "CT", "MR" ], anatomicRegion : "TH", maxAge : 5, newest : 4, oldest : 0 ],
          [ modality : [ "CR" ], anatomicRegion : "TH", maxAge : 5, newest : 4, oldest : 0 ],
          [ modality : [ "PT" ], anatomicRegion : "TP", maxAge : 5, newest : 4, oldest : 0 ],
        ],
      ],
      [ modality : [ "NM" ], anatomicRegion : "AB", subrules :
        [
          [ modality : [ "NM" ], anatomicRegion : "AB", maxAge : 5, newest : 6, oldest : 0 ],
          [ modality : [ "NM" ], anatomicRegion : "DI", maxAge : 5, newest : 6, oldest : 0 ],
          [ modality : [ "CT", "MR" ], anatomicRegion : "AB", maxAge : 2, newest : 4, oldest : 0 ],
          [ modality : [ "CR" ], anatomicRegion : "AB", maxAge : 2, newest : 4, oldest : 0 ],
          [ modality : [ "CR" ], anatomicRegion : "DI", maxAge : 2, newest : 4, oldest : 0 ],
          [ modality : [ "PT" ], anatomicRegion : "TP", maxAge : 5, newest : 4, oldest : 0 ],
        ],
      ],
      [ modality : [ "NM" ], anatomicRegion : "CA", subrules :
        [
          [ modality : [ "NM" ], anatomicRegion : "CA", maxAge : 5, newest : 6, oldest : 0 ],
          [ modality : [ "CT", "MR" ], anatomicRegion : "CA", maxAge : 2, newest : 4, oldest : 0 ],
          [ modality : [ "CR" ], anatomicRegion : "TH", maxAge : 2, newest : 6, oldest : 0 ],
          [ modality : [ "PT" ], anatomicRegion : "TP", maxAge : 5, newest : 4, oldest : 0 ],
        ],
      ],
      [ modality : [ "NM" ], anatomicRegion : "SE", subrules :
        [
          [ modality : [ "NM" ], anatomicRegion : "SE", maxAge : 5, newest : 6, oldest : 0 ],
          [ modality : [ "MG" ], anatomicRegion : "SE", maxAge : 5, newest : 6, oldest : 0 ],
          [ modality : [ "US" ], anatomicRegion : "SE", maxAge : 5, newest : 6, oldest : 0 ],
          [ modality : [ "MR" ], anatomicRegion : "SE", maxAge : 5, newest : 6, oldest : 0 ],
          [ modality : [ "PT" ], anatomicRegion : "TP", maxAge : 5, newest : 4, oldest : 0 ],
        ],
      ],
      [ modality : [ "NM" ], anatomicRegion : "EN", subrules :
        [
          [ modality : [ "NM" ], anatomicRegion : "EN", maxAge : 5, newest : 6, oldest : 0 ],
          [ modality : [ "PT" ], anatomicRegion : "TP", maxAge : 5, newest : 4, oldest : 0 ],
        ],
      ],
      [ modality : [ "NM" ], anatomicRegion : "SQ", subrules :
        [
          [ modality : [ "NM" ], anatomicRegion : "SQ", maxAge : 5, newest : 6, oldest : 0 ],
          [ modality : [ "PT" ], anatomicRegion : "TP", maxAge : 5, newest : 4, oldest : 0 ],
        ],
      ],
      [ modality : [ "NM" ], anatomicRegion : "HP", subrules :
        [
          [ modality : [ "NM" ], anatomicRegion : "HP", maxAge : 5, newest : 6, oldest : 0 ],
          [ modality : [ "PT" ], anatomicRegion : "TP", maxAge : 5, newest : 4, oldest : 0 ],
        ],
      ],
    ]

    static { 
        // Compile the rules into a map based on modality and anatomic region.
        // Slightly more complicated than List.groupBy because we need to produce
        // multiple map entries per list item
        def map = [:]
        rules.each { rule ->
            rule.modality.each { ruleModality ->
                map[[ modality: ruleModality, anatomicRegion: rule.anatomicRegion ]] = rule
            }
        }
        rules = map
    }

    public static getRule(modality, anatomicRegion) {
        return rules[[ modality: modality, anatomicRegion: anatomicRegion ]]
    }
}


