/*
 * Morphs the c-find to pacs in AdhocRetrieve workflow to get back the study metadata. 
 * the c-find response will be available to the morpher configured as
 * AdhocRetrievePriorExamsForeignImageMorpher 
 */

// Query for PatientName
set(PatientName, "")

// Query for Patient Birth Date
set(PatientBirthDate, "")

