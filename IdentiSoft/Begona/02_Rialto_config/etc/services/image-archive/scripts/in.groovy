log.debug("in.groovy: Start inbound morpher")

def issuerOfPatientId = get(IssuerOfPatientID)
def universalEntityID = get("IssuerOfPatientIDQualifiersSequence/UniversalEntityID")
def universalEntityIDType = get("IssuerOfPatientIDQualifiersSequence/UniversalEntityIDType")


log.debug("in.groovy: IssuerOfPatientId is {} ", issuerOfPatientId)
log.debug("in.groovy: universalEntityID is {} ", universalEntityID)
log.debug("in.groovy: universalEntityIDType is {} ", universalEntityIDType)

set(IssuerOfPatientID, 'HOSBEG')
set("IssuerOfPatientIDQualifiersSequence/UniversalEntityID", '2.16.124.113638.1.3.1')
set("IssuerOfPatientIDQualifiersSequence/UniversalEntityIDType", 'ISO')

log.debug("in.groovy: End inbound morpher")
