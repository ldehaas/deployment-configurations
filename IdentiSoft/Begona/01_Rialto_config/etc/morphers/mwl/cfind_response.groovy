// CFIND response morpher for modality worklists
log.info("MWL CFIND response Morpher")

def requestedProcedure = imagingServiceRequest.getRequestedProcedureSequence().get(0)
log.debug("Setting StudyDescription tag to {}", requestedProcedure.getRequestedProcedureDescription())
set(StudyDescription, requestedProcedure.getRequestedProcedureDescription())

log.debug("Imaging Service Request is:\n{}", imagingServiceRequest)

def scheduledProcedureStep = requestedProcedure.getScheduledProcedureStepSequence().get(0)
def AETitle = scheduledProcedureStep.getScheduledProcedureStepLocation()
if (AETitle == "INSPIRATION"){
    set(PerformingPhysicianName, imagingServiceRequest.getAttendingPhysician())
}
