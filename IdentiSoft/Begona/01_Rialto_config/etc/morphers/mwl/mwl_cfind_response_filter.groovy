/* Used to filter out DICOM tags for GE CT modality as per KHC #10628.
*/

log.info("MWL CFind Response Morpher START")
log.debug("MWL CFind Response Morpher - input:{}\n", input)

callingAE = getCallingAETitle()

if (callingAE == "REVOLUTION"){
    remove(tag(0x0008,0x0051))
    remove(tag(0x0010,0x0024))
    remove(tag(0x0040,0x0031))
    remove(tag(0x0040,0x0032))
    remove(tag(0x0040,0x0033))
}

// KHC10628 - Get and reformat Scheduled Procedure Step Start Time from 10 to 6 digits
if (AETitle =="REVOLUTION"){
    def truncatedSPSST = (get(tag(0x0040,0x0003)).take(6))
    log.info("Changing the ScheduledProcedureStepStartTime to 6 digit format:{} -> {}", get(tag(0x0040,0x0003), truncatedSPSST))
    set(tag(0x0040,0x0003), truncatedSPSST)
}

log.debug("MWL CFind Response Morpher - output:{}\n", output)
log.info("MWL CFind Response END")