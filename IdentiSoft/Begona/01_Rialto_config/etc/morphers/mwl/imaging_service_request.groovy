import com.karos.rtk.common.Terser;

// Imaging service request morpher for modality worklist system tests
log.info("MWL ISR Morpher - The order message:\n{}", input)
log.info("MWL ISR Morpher - The initial Imaging Service Request:\n{}", imagingServiceRequest)

def requestedProcedure = imagingServiceRequest.getRequestedProcedureSequence().get(0)
requestedProcedure.setRequestedProcedureID(imagingServiceRequest.getAccessionNumber())

def scheduledProcedureStep = requestedProcedure.getScheduledProcedureStepSequence().get(0)
scheduledProcedureStep.setScheduledProcedureStepIDString(imagingServiceRequest.getAccessionNumber())

if (scheduledProcedureStep.getScheduledProcedureStepLocation() == null) {
    scheduledProcedureStep.setScheduledProcedureStepLocation("UNKNOWN");
}

if (scheduledProcedureStep.getModality() == null) {
    scheduledProcedureStep.setModality("UNKNOWN");
}

def AETitle = scheduledProcedureStep.getScheduledProcedureStepLocation()
log.debug("Setting Scheduled Station AE Title to {}", AETitle);
scheduledProcedureStep.setScheduledStationAETitle(AETitle)

def sopCommon = imagingServiceRequest.getSopCommon()

// KHC10628 - Added REVOLUTION to use ISO 100
if ((AETitle == "INSPIRATION") || (AETitle =="REVOLUTION")){
    sopCommon.setSpecificCharacterSet('ISO_IR 100')
} else {
    sopCommon.setSpecificCharacterSet('ISO_IR 192')
}

// KHC10628 - Get and reformat Scheduled Procedure Step Start Time from 10 to 6 digits
if (AETitle =="REVOLUTION"){
    def truncatedSPSST = (get(tag(0x0040,0x0003)).take(6))
    log.info("Changing the ScheduledProcedureStepStartTime to 6 digit format:{} -> {}", get(tag(0x0040,0x0003), truncatedSPSST))
    set(tag(0x0040,0x0003), truncatedSPSST)
}

requestedProcedure.setRequestedProcedureDescription(get("OBR-5-2"))

log.info("MWL ISR Morpher - Finished fixing up the Imaging Service Request:\n{}", imagingServiceRequest)