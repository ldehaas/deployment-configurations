import com.karos.rtk.common.Terser;


//def patientIdentification = imagingServiceRequest.getPatientIdentification()
//if (patientIdentification.getPatientIdUniversalId() == null) {
//    patientIdentification.setPatientIdNamespaceId("CR")
//    patientIdentification.setPatientIdUniversalId(get("PID-2-1"))
//    patientIdentification.setPatientIdUniversalIdType("ISO")
//}

// Imaging service request morpher for modality worklist system tests
log.info("MWL ISR Morpher - The order message:\n{}", input)
log.info("MWL ISR Morpher - The initial Imaging Service Request:\n{}", imagingServiceRequest)

def requestedProcedure = imagingServiceRequest.getRequestedProcedureSequence().get(0)
requestedProcedure.setRequestedProcedureID(imagingServiceRequest.getAccessionNumber())

def scheduledProcedureStep = requestedProcedure.getScheduledProcedureStepSequence().get(0)
scheduledProcedureStep.setScheduledProcedureStepIDString(imagingServiceRequest.getAccessionNumber())

if (scheduledProcedureStep.getScheduledProcedureStepLocation() == null) {
    scheduledProcedureStep.setScheduledProcedureStepLocation("UNKNOWN");
}

if (scheduledProcedureStep.getModality() == null) {
    scheduledProcedureStep.setModality("UNKNOWN");
}

def AETitle = scheduledProcedureStep.getScheduledProcedureStepLocation()
log.debug("Setting Scheduled Station AE Title to {}", AETitle);
scheduledProcedureStep.setScheduledStationAETitle(AETitle)

def sopCommon = imagingServiceRequest.getSopCommon()

if (AETitle == "INSPIRATION") {
    sopCommon.setSpecificCharacterSet('ISO_IR 100')
} else {
    sopCommon.setSpecificCharacterSet('ISO_IR 192')
}

requestedProcedure.setRequestedProcedureDescription(get("OBR-5-2"))

log.info("MWL ISR Morpher - Finished fixing up the Imaging Service Request:\n{}", imagingServiceRequest)

