//Cfind reqest morpher for image archive

log.info("Image Archive CFIND request Morpher")

if (get('PatientID') != null)
{
    set('IssuerOfPatientID', 'CM')
    set('IssuerOfPatientIDQualifiersSequence/UniversalEntityID', '2.16.124.113638.1.3.3')
    set('IssuerOfPatientIDQualifiersSequence/UniversalEntityIDType', 'ISO')
}

