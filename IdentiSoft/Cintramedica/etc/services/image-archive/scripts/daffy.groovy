import java.text.SimpleDateFormat
log.debug("daffy.groovy: Start Study Content Notification morpher")

initialize( 'ORM', 'O01', '2.3' );
output.getMessage().addNonstandardSegment('IPC')
log.debug("daffy.groovy: input is {}", input);
log.debug("daffy.groovy:Calling AE Title is {}", getCallingAETitle());
//
//set('MSH-7', '20130827132217')
sdf = new SimpleDateFormat("yyyyMMddHHmmss")
set('MSH-7', sdf.format(new Date()))
log.debug("daffy.groovy: Date set is {}",sdf.format(new Date()));
//set('MSH-7', new Date().format('yyyyMMddHHmmss'))
//
setPersonName('PID-5', input.get(PatientName));
set('PID-3-1', input.get(PatientID));

//set('PID-3-4', 'MASTER')
set('PID-3-4','CM')
set('PID-3-4-2', '2.16.124.113638.1.3.3')
set('PID-3-4-3', 'ISO')

set('PID-3-5', 'MPI')
//
set('ORC-1', 'XO')
set('ORC-5', 'COMPLETED')
//
set('OBR-1', '1')
set('OBR-3', input.get(AccessionNumber))
//
// Set the Procedure Code depending on the Calling AE Title
//if (getCallingAETitle() == 'SYNGOTSTAPP01' ) {
//    set('OBR-4-1',  input.get(RequestedProcedureID))
//} else if (getCallingAETitle() == 'TSTPACSDB1' ) {
//    set('OBR-4-1',  input.get(MilitaryRank))
//}
//
set('OBR-4-2', input.get(StudyDescription))
set('OBR-4-3', 'RMH')
//
set('OBR-16-9', 'SMART')
set('OBR-16-13', 'PROV')

log.debug("daffy.groovy: Accession Number is {}", input.get(AccessionNumber));
set('OBR-18', input.get(AccessionNumber))
set('OBR-18-2', 'CM')
set('OBR-18-3', '2.16.124.113638.1.3.3')
set('OBR-18-4', 'ISO')

set('OBR-25', 'I')
//
set('OBX-1', '1')
set('OBX-2', 'TX')
set('OBX-3-1-2', 'GDT')
set('OBX-5', 'Y')
set('OBX-9', input.get(StudyInstanceUID))

set('IPC-1', input.get(AccessionNumber))
set('IPC-3', input.get(StudyInstanceUID))
set('IPC-1-2', 'CM')
set('IPC-1-3', '2.16.124.113638.1.3.3')
set('IPC-1-4', 'ISO')
log.debug("daffy.groovy: Study IUID is {}", input.get(StudyInstanceUID));

log.debug("daffy.groovy: End Study Content Notification morpher")
//
//set('IPC-1', input.get(AccessionNumber))
//set('IPC-3', input.get(StudyInstanceUID))
