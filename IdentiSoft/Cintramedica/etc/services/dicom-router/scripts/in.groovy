log.debug("in.groovy: Start inbound morpher")

def issuerOfPatientId = get(IssuerOfPatientID)
def universalEntityID = get("IssuerOfPatientIDQualifiersSequence/UniversalEntityID")
def universalEntityIDType = get("IssuerOfPatientIDQualifiersSequence/UniversalEntityIDType")

set(IssuerOfPatientID, 'CM')
set("IssuerOfPatientIDQualifiersSequence/UniversalEntityID", '2.16.124.113638.1.3.3')
set("IssuerOfPatientIDQualifiersSequence/UniversalEntityIDType", 'ISO')

log.debug("in.groovy: End inbound morpher")
