// grh has patients and accounts, but no visits.
// vault has patients and visits. this morpher is used to bridge
// the differences.

// map grh specific merge / move / rename messages to the ones vault expects
def msh91 = get('MSH-9-1')
def msh92 = get('MSH-9-2')

//KHC1764 - ignoring HL7 messages with null or blank vistID/account number
if (get('PID-18') == null || get('PID-18') == '') {
    return false
}

if (msh91 == 'ADT') {
    if (msh92 == 'A06' || msh92 == 'A07') {
        set('MRG-5', get('MRG-3'))
        set('MSH-9-2', 'A50')

    } else if (msh92 == 'A36') {
        set('MRG-5', get('MRG-3'))
        set('MSH-9-2', 'A45')

        // PV1 cannot exist in A36 by spec, but since we are aliasing this
        // to A45 which requires PV1, add it as non standard
        input.getMessage().addNonstandardSegment('PV1')
    }

    // Map appropriate values for allergy types
    def allergyIds = null;
    try {
        allergyIds = getList('/.AL1(*)-3')
    } catch (Exception e) {
        // do nothing!
    }
    if (allergyIds != null) {
         def allergyTypeMap = [
             'DA':'DRUG',
             'FA':'FOOD',
             'MA':'OTHER',
             '':'NONE'
         ]

        for (int i = 0; i < allergyIds.size(); i++) {
            def allergyId = get('/.AL1(' + i + ')-3-1')
            def allergyText = get('/.AL1(' + i + ')-3-2')
            if ((allergyId == null || ''.equals(allergyId)) &&
                    (allergyText == null || ''.equals(allergyText))) {
                continue;
            }

            def allergyTypeField = '/.AL1(' + i + ')-2'
            def allergyType = get(allergyTypeField);

            if (allergyType == null || '""'.equals(allergyType)) {
                set(allergyTypeField, allergyTypeMap[''])
            } else if (allergyTypeMap[allergyType] != null) {
                set(allergyTypeField, allergyTypeMap[allergyType])
            } else if (allergyTypeMap.values().contains(allergyType)) {
                // do nothing, already set properly!
            } else {
                set(allergyTypeField, allergyTypeMap['MA'])
            }
        }
    }


} else if (msh91 == 'ORU') {

    def obr43 = get('OBR-4-3')
    def obr24 = get('OBR-24')
    if (!('LAB'.equalsIgnoreCase(obr43) && (
           'HEMA'.equalsIgnoreCase(obr24) ||
           'CHEM'.equalsIgnoreCase(obr24) ||
           'BBNK'.equalsIgnoreCase(obr24) ||
           'MICR'.equalsIgnoreCase(obr24)))) {

        set('OBR-32-1-2', get('OBR-32-2'))
        set('OBR-32-1-3', get('OBR-32-3'))
        set('OBR-35-1-2', get('OBR-35-2'))
        set('OBR-35-1-3', get('OBR-35-3'))

        if (!'COPATH'.equalsIgnoreCase(obr43)) {
            set('OBR-35-2', get('OBR-22-1'))
            set('OBR-22-1', get('OBR-22-3'))
            set('OBR-32-2', get('OBR-22-3'))
        }

        // the report number is everything after the dash (if the dash exists)
        reportNumber = get('OBR-3-1')
        reportNumberArray = reportNumber.split('-')
        if (reportNumberArray.length > 1) {
            reportNumber = reportNumberArray[1]
        }
        set('OBR-3-1', reportNumber)
    }
}

// use account id as visit id
try {
    set('PV1-19', get('PID-18'))
} catch (Exception e) {
    // sink
    // this fails when the type of hl7 message does not allow PV1
    // in those cases, PV1 doesn't matter
}

// Ensure the MRN is in the first repetition of the PID-3 field
def MRN_IDENTIFIER_TYPE = 'MR'
def identifierType = get('/.PID-3-5')
if (!MRN_IDENTIFIER_TYPE.equalsIgnoreCase(identifierType)) {

    def patientIds = null;
    try {
        patientIds = getList('/.PID-3(*)-1')
    } catch (Exception e) {
        // do nothing!
    }

    def firstId = get('/.PID-3-1')
    def firstCheckDigit = get('/.PID-3-2')
    def firstCheckDigitCode = get('/.PID-3-3')
    def firstNamespaceId = get('/.PID-3-4-1')
    def firstUniversalId = get('/.PID-3-4-2')
    def firstUniversalIdType = get('/.PID-3-4-3')
    def firstIdentifierType = get('/.PID-3-5')
    for (int i = 0; i < patientIds.size(); i++) {
        def currIdentifierType = get('/.PID-3(' + i + ')-5')
        if (MRN_IDENTIFIER_TYPE.equalsIgnoreCase(currIdentifierType)) {

            // Copy MRN into first repetition
            set('/.PID-3-1', get('/.PID-3(' + i + ')-1'))
            set('/.PID-3-2', get('/.PID-3(' + i + ')-2'))
            set('/.PID-3-3', get('/.PID-3(' + i + ')-3'))
            set('/.PID-3-4-1', get('/.PID-3(' + i + ')-4-1'))
            set('/.PID-3-4-2', get('/.PID-3(' + i + ')-4-2'))
            set('/.PID-3-4-3', get('/.PID-3(' + i + ')-4-3'))
            set('/.PID-3-5', get('/.PID-3(' + i + ')-5'))

            // Copy the old 'first' repetition where the MRN was found
            set('/.PID-3(' + i + ')-1', firstId)
            set('/.PID-3(' + i + ')-2', firstCheckDigit)
            set('/.PID-3(' + i + ')-3', firstCheckDigitCode)
            set('/.PID-3(' + i + ')-4-1', firstNamespaceId)
            set('/.PID-3(' + i + ')-4-2', firstUniversalId)
            set('/.PID-3(' + i + ')-4-3', firstUniversalIdType)
            set('/.PID-3(' + i + ')-5', firstIdentifierType)

            break;
        }
    }
}
