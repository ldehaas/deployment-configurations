<config>

    <server type="http" id="navigator-http">
        <port>2526</port>
    </server>

    <server type="http" id="http-admin-tools">
        <port>2527</port>
    </server>

    <service type="authentication" id="authentication">
        <server idref="authenticated-http" name="web-api">
            <url>/rest/</url>
        </server>

        <config>
            <include location="userauth.xml"/>
        </config>
    </service>

    <service type="usermanagement" id="usermanagement">
        <config>
            <prop name="SystemDefaultPermissions">
                <permission name="rialto.arr" value="false"/>
                <permission name="rialto.dashboard" value="false"/>
                <permission name="rialto.dashboard.editWidgets" value="false"/>
                <permission name="rialto.dashboard.editAlerts" value="false"/>
		<permission name="rialto.administration" value="false"/>
                <permission name="rialto.usermanagement.internalrealm" value="false"/>
                <permission name="rialto.events" value="false" />
                <permission name="rialto.quality.management" value="false" />
                <permission name="rialto.modality.worklist" value="false" />
                <permission name="rialto.modality.worklist.updates" value="false" />

                <!-- Workflows/Dataflows permissions - should never have both configured at the same time -->
                <!--permission name="rialto.dataflows" value="false" /-->
                <permission name="rialto.workflows" value="false" />
                <permission name="rialto.workflows.superAdmin" value="false" />
            </prop>

            <include location="defaultuserprefs.xml"/>
            <include location="defaultdashboardprefs.xml"/>
        </config>
    </service>

    <!-- PLUGIN: External Viewer (Download) -->
    <var name="DownloadPlugin-Name" value="External Viewer (Download)" />
    <var name="DownloadPlugin-Description" value="External platform-specific viewer or download to local disk" />
    <service type="navigator.plugin.download" id="navigator.plugin.download">
        <device idref="pix" />
        <device idref="pdq" />
        <device idref="xdsrep"/>
        <device idref="xdsreg"/>

        <config>
            <prop name="CrossAffinityDomainID" value="${System-AffinityDomain}"/>
            <prop name="ForcedMimeTypeFileExtension">
                <prop mimeType="application/vnd.ms-excel" fileExtension="xls"/>
            </prop>
            <prop name="CassandraConfiguration">
                <clusterHosts>${CassandraClusterHosts}</clusterHosts>
                <clusterDatacenterName>${CassandraClusterDatacenterName}</clusterDatacenterName>
                <keyspaceName>${CassandraKeyspacePrefix}navigator</keyspaceName>
                <keyspaceReplicationStrategy>NetworkTopologyStrategy</keyspaceReplicationStrategy>
                <keyspaceReplicationStrategyOptions>${CassandraReplicationOption}</keyspaceReplicationStrategyOptions>
                <consistencyLevel>LOCAL_QUORUM</consistencyLevel>
                <serialConsistencyLevel>LOCAL_SERIAL</serialConsistencyLevel>
                <keyspaceCreateEnabled>true</keyspaceCreateEnabled>
                <dynamicConsistencyLevelEnabled>false</dynamicConsistencyLevelEnabled>
                <schemaUpdateEnabled>false</schemaUpdateEnabled>
            </prop>
            <prop name="web-api-path" value="/plugins/download"/>
            <prop name="UserManagementURL" value="${Usermanagement-AuthenticatedURL}"/>

            <include location="patientidentitydomains.xml"/>
            <include location="cacheStorage.xml"/>
        </config>
    </service>

    <!-- PLUGIN: CDA Viewer   -->
    <var name="CDAPlugin-Name" value="CDA Viewer" />
    <var name="CDAPlugin-Description" value="HL7 CDA (Clinical Document Architecture) Viewer" />
    <service type="navigator.plugin.cda" id="navigator.plugin.cda">
        <device idref="pix" />
        <device idref="pdq" />
        <device idref="xdsrep"/>
        <device idref="xdsreg"/>

        <config>
            <prop name="CrossAffinityDomainID" value="${System-AffinityDomain}"/>
            <prop name="CassandraConfiguration">
                <clusterHosts>${CassandraClusterHosts}</clusterHosts>
                <clusterDatacenterName>${CassandraClusterDatacenterName}</clusterDatacenterName>
                <keyspaceName>${CassandraKeyspacePrefix}navigator</keyspaceName>
                <keyspaceReplicationStrategy>NetworkTopologyStrategy</keyspaceReplicationStrategy>
                <keyspaceReplicationStrategyOptions>${CassandraReplicationOption}</keyspaceReplicationStrategyOptions>
                <consistencyLevel>LOCAL_QUORUM</consistencyLevel>
                <serialConsistencyLevel>LOCAL_SERIAL</serialConsistencyLevel>
                <keyspaceCreateEnabled>true</keyspaceCreateEnabled>
                <dynamicConsistencyLevelEnabled>false</dynamicConsistencyLevelEnabled>
                <schemaUpdateEnabled>false</schemaUpdateEnabled>
            </prop>
            <prop name="web-api-path" value="/plugins/cda"/>
            <prop name="DefaultStylesheet" value="${rialto.rootdir}/etc/navigator/plugins/cda/default.xsl"/>
            <prop name="UserManagementURL" value="${Usermanagement-AuthenticatedURL}"/>

            <include location="patientidentitydomains.xml"/>
            <include location="cacheStorage.xml"/>
        </config>
    </service>


    <service type="navigator.server" id="navigator.server">
        <!--device idref="pix" />
        <device idref="pdq" /-->
        <device idref="xdsrep"/>
        <device idref="xdsreg"/>

        <config>
            <prop name="DocumentSearchOptimizeFor" value="TOTAL_TIME"/>
            <prop name="CrossAffinityDomainID" value="${System-AffinityDomain}"/>
            <prop name="CassandraConfiguration">
                <clusterHosts>${CassandraClusterHosts}</clusterHosts>
                <clusterDatacenterName>${CassandraClusterDatacenterName}</clusterDatacenterName>
                <keyspaceName>${CassandraKeyspacePrefix}navigator</keyspaceName>
                <keyspaceReplicationStrategy>NetworkTopologyStrategy</keyspaceReplicationStrategy>
                <keyspaceReplicationStrategyOptions>${CassandraReplicationOption}</keyspaceReplicationStrategyOptions>
                <consistencyLevel>LOCAL_QUORUM</consistencyLevel>
                <serialConsistencyLevel>LOCAL_SERIAL</serialConsistencyLevel>
                <keyspaceCreateEnabled>true</keyspaceCreateEnabled>
                <dynamicConsistencyLevelEnabled>false</dynamicConsistencyLevelEnabled>
                <schemaUpdateEnabled>false</schemaUpdateEnabled>
            </prop>
            <prop name="Cluster">
                <name>NavigatorServerClusterGroupId</name>
                <udpstack>
                    <bindAddress>${Navigator-Server-Cluster-Bind-IP}</bindAddress>
                    <multicastAddress>${Navigator-Server-Cluster-Multicast-IP}</multicastAddress>
                    <multicastPort>${Navigator-Server-Cluster-Multicast-PORT}</multicastPort>
                </udpstack>
            </prop>

            <!-- Rialto XDS Registry cannot search by extended metadata -->
            <prop name="ExtendedMetadataAttributes">
		<ExtendedMetadataAttribute displayName="pageCount" searchKey="pageCount"/>
                <ExtendedMetadataAttribute displayName="studyInstanceUid" searchKey="studyInstanceUid"/>
                <ExtendedMetadataAttribute displayName="accessionNumber" searchKey="accessionNumber"/>            
                <ExtendedMetadataAttribute displayName="Category" searchKey="Category"/>
                <ExtendedMetadataAttribute displayName="DocumentType" searchKey="DocumentType"/>
                <ExtendedMetadataAttribute displayName="ScanningBatch" searchKey="ScanningBatch"/>
                <ExtendedMetadataAttribute displayName="ClixBatch" searchKey="ClixBatch"/>
                <ExtendedMetadataAttribute displayName="documentsource" searchKey="documentsource"/>
                <ExtendedMetadataAttribute displayName="Skanoperator" searchKey="Skanoperator"/>
                <ExtendedMetadataAttribute displayName="SidtypIMACS" searchKey="SidtypIMACS"/>
                <ExtendedMetadataAttribute displayName="KlinikIMACS" searchKey="KlinikIMACS"/>
                <ExtendedMetadataAttribute displayName="IMACSbunt" searchKey="IMACSbunt"/>
                <ExtendedMetadataAttribute displayName="TempDokTyp" searchKey="TempDokTyp"/>
                <ExtendedMetadataAttribute displayName="Beskrivning" searchKey="Beskrivning"/>
                <ExtendedMetadataAttribute displayName="Inraettning" searchKey="Inraettning"/>
            </prop>

            <prop name="SystemDefaultPermissions">
                <permission name="rialto.navigator.patients.search.internal" value="true"/>
                <permission name="rialto.navigator.patients.search.external" value="false"/>
                <permission name="rialto.navigator.study.metadata.view" value="true" />
                <permission name="rialto.navigator.study.move" value="true" />
                <permission name="rialto.navigator.study.changeavailability" value="false" />
                <permission name="rialto.navigator.study.physical.delete" value="false" />
                <permission name="rialto.navigator.study.qc" value="false" />
                <permission name="rialto.navigator.study.reconciliation" value="false"/>
                <permission name="rialto.navigator.study.version.restore" value="false" />
                <permission name="rialto.navigator.docs.metadata.view.unknown.internal" value="true"/>
                <permission name="rialto.navigator.docs.metadata.view.unknown.external" value="true"/>
                <permission name="rialto.navigator.docs.metadata.edit.unknown.internal" value="false"/>
                <permission name="rialto.navigator.docs.metadata.edit.unknown.external" value="false"/>
                <permission name="rialto.navigator.docs.move.unknown.internal" value="false" />
                <permission name="rialto.navigator.docs.view.unknown.internal.always" value="false"/>
                <permission name="rialto.navigator.docs.view.unknown.external.always" value="false"/>
                <permission name="rialto.navigator.docs.view.unknown.internal.onbreakglass" value="true"/>
                <permission name="rialto.navigator.docs.view.unknown.external.onbreakglass" value="true"/>
                <permission name="rialto.navigator.docs.upload.internal" value="false" />
                <permission name="rialto.vip.access" value="false" />
            </prop>

            <prop name="DefaultConfidentialityCodeSchemeID" value="1.2.3.4.5.6.1" />
            <prop name="DefaultConfidentialityCodeSchemeName" value="Karos Health demo confidentialityCodes" />
            <prop name="MaximumPatientResultSetSize" value="${Navigator-MaxPatientSearchResults}"/>
            <prop name="MaximumDocumentResultSetSize" value="${Navigator-MaxDocumentSearchResults}"/>
            <prop name="AllowMetadataUpdates">${XDSRegistry-MetadataUpdateEnabled}</prop>
            <prop name="ConfidentialityCodeSchemaName">TEST</prop>
            <prop name="UseTargetDomainForPDQ">true</prop>

            <include location="patientidentitydomains.xml"/>
            <include location="cacheStorage.xml"/>

            <prop name="XDSDocumentCodes">
                <!-- classCode maps to "Department", or "Service Area" -->
                <!-- Value set from DICOM Part 16 CID 7030: http://medical.nema.org/dicom/2013/output/chtml/part16/sect_CID_7030.html -->
                <classification name="classCodes">
                     <Code codeValue="NotUsed" schemeName="NotUsed" schemeID="NotUsed" displayName="NotUsed" />
                     <!--Code codeValue="R-3027B" schemeName="SRT" schemeID="2.16.840.1.113883.6.5" displayName="Radiology" />
                     <Code codeValue="R-300E3" schemeName="SRT" schemeID="2.16.840.1.113883.6.5" displayName="Emergency" />
                     <Code codeValue="R-30248" schemeName="SRT" schemeID="2.16.840.1.113883.6.5" displayName="Cardiology" />
                     <Code codeValue="R-3023B" schemeName="SRT" schemeID="2.16.840.1.113883.6.5" displayName="Oncology" />
                     <Code codeValue="R-30250" schemeName="SRT" schemeID="2.16.840.1.113883.6.5" displayName="Dermatology" />
                     <Code codeValue="R-421D4" schemeName="SRT" schemeID="2.16.840.1.113883.6.5" displayName="Endoscopy" />
                     <Code codeValue="R-30254" schemeName="SRT" schemeID="2.16.840.1.113883.6.5" displayName="General Medicine" />
                     <Code codeValue="R-3023D" schemeName="SRT" schemeID="2.16.840.1.113883.6.5" displayName="Intensive Care" />
                     <Code codeValue="R-3025E" schemeName="SRT" schemeID="2.16.840.1.113883.6.5" displayName="Neurology" />
                     <Code codeValue="R-30263" schemeName="SRT" schemeID="2.16.840.1.113883.6.5" displayName="Ob/Gyn" />
                     <Code codeValue="R-3025C" schemeName="SRT" schemeID="2.16.840.1.113883.6.5" displayName="Opthamology" />
                     <Code codeValue="R-3026B" schemeName="SRT" schemeID="2.16.840.1.113883.6.5" displayName="Pathology" />
                     <Code codeValue="R-305EA" schemeName="SRT" schemeID="2.16.840.1.113883.6.5" displayName="Pediatrics" />
                     <Code codeValue="S-8000A" schemeName="SRT" schemeID="2.16.840.1.113883.6.5" displayName="Primary Care" />
                     <Code codeValue="R-30246" schemeName="SRT" schemeID="2.16.840.1.113883.6.5" displayName="Allergy and Immunology" />
                     <Code codeValue="R-30283" schemeName="SRT" schemeID="2.16.840.1.113883.6.5" displayName="Dental Surgery" />
                     <Code codeValue="R-30294" schemeName="SRT" schemeID="2.16.840.1.113883.6.5" displayName="Orthopedic Surgery" /-->
                </classification>
                <!-- for file upload: contentTypeCode will be set to the same value as classCode -->
                <classification name="contentTypeCodes">
                    <Code codeValue="NotUsed" schemeName="Karos demo contentTypeCodes" schemeID="1.2.3.4.5.6.8" displayName="NotUsed" />
                    <Code codeValue="NotUsed" schemeName="NotUsed" schemeID="NotUsed" displayName="NotUsed" />
                </classification>
                <!-- typeCode maps to "Document Title" -->
                <!-- Value set from LOINC codes (GUI should reduce the visible set based on the login/user clinical area.
                     Or, default to "Clinical Note". -->
                <classification name="typeCodes">
                    <Code codeValue="001" schemeName="Ursprung" schemeID="Ursprung" displayName="Papper" />
                    <Code codeValue="002" schemeName="Ursprung" schemeID="Ursprung" displayName="Media"  />
                    <Code codeValue="003" schemeName="Ursprung" schemeID="Ursprung" displayName="Depona"  />
                    <Code codeValue="JOURNAL" schemeName="Ursprung" schemeID="Ursprung" displayName="IMACS"  />
                    <Code codeValue="KOVIS" schemeName="Ursprung" schemeID="Ursprung" displayName="KOVIS"  />
                    <Code codeValue="LTH" schemeName="Ursprung" schemeID="Ursprung" displayName="Landstingshälsen"  />
                    <Code codeValue="PATIENT" schemeName="Ursprung" schemeID="Ursprung" displayName="IMACS"  />
                    <Code codeValue="RECEPTION" schemeName="Ursprung" schemeID="Ursprung" displayName="IMACS"  />
                    <Code codeValue="18842-5" schemeName="LOINC" schemeID="2.16.840.1.113883.6.1" displayName="Discharge Summary" />
                    <Code codeValue="51849-8" schemeName="LOINC" schemeID="2.16.840.1.113883.6.1" displayName="Admission History and Physical Note" />
                    <Code codeValue="67860-7" schemeName="LOINC" schemeID="2.16.840.1.113883.6.1" displayName="Post-operative Note" />
                    <Code codeValue="34094-3" schemeName="LOINC" schemeID="2.16.840.1.113883.6.1" displayName="Cardiology Admit H&amp;P Note" />
                    <Code codeValue="34109-9" schemeName="LOINC" schemeID="2.16.840.1.113883.6.1" displayName="Clinical Note" />
                    <Code codeValue="68629-5" schemeName="LOINC" schemeID="2.16.840.1.113883.6.1" displayName="Allergy and Immunology Note" />
                    <Code codeValue="34752-6" schemeName="LOINC" schemeID="2.16.840.1.113883.6.1" displayName="Cardiology Note" />
                    <Code codeValue="34754-2" schemeName="LOINC" schemeID="2.16.840.1.113883.6.1" displayName="Critical Care Note" />
                    <Code codeValue="28618-7" schemeName="LOINC" schemeID="2.16.840.1.113883.6.1" displayName="Dentistry Note" />
                    <Code codeValue="34759-1" schemeName="LOINC" schemeID="2.16.840.1.113883.6.1" displayName="Dermatology Note" />
                    <Code codeValue="34878-9" schemeName="LOINC" schemeID="2.16.840.1.113883.6.1" displayName="Emergency Medicine Note" />
                    <Code codeValue="34765-8" schemeName="LOINC" schemeID="2.16.840.1.113883.6.1" displayName="General Medicine Note" />
                    <Code codeValue="34780-7" schemeName="LOINC" schemeID="2.16.840.1.113883.6.1" displayName="Hematology Oncology Note" />
                    <Code codeValue="34778-1" schemeName="LOINC" schemeID="2.16.840.1.113883.6.1" displayName="Obstetrics and Gynecology Note" />
                    <Code codeValue="34815-1" schemeName="LOINC" schemeID="2.16.840.1.113883.6.1" displayName="Orthopaedic Note" />
                    <Code codeValue="34832-6" schemeName="LOINC" schemeID="2.16.840.1.113883.6.1" displayName="Radiation Oncology Note" />
                    <Code codeValue="11488-4" schemeName="LOINC" schemeID="2.16.840.1.113883.6.1" displayName="Consult Note" />
                    <Code codeValue="34099-2" schemeName="LOINC" schemeID="2.16.840.1.113883.6.1" displayName="Cardiology Consult Note" />
                    <Code codeValue="73575-3" schemeName="LOINC" schemeID="2.16.840.1.113883.6.1" displayName="Radiology Consult Note" />
                    <Code codeValue="18748-4" schemeName="LOINC" schemeID="2.16.840.1.113883.6.1" displayName="Diagnostic Imaging Report - text/scanned" />
                    <Code codeValue="72230-6" schemeName="LOINC" schemeID="2.16.840.1.113883.6.1" displayName="Diagnostic Imaging Report - structured CDA" />
                    <Code codeValue="57170-3" schemeName="LOINC" schemeID="2.16.840.1.113883.6.1" displayName="Cardiology Referral Note" />
                    <Code codeValue="34124-8" schemeName="LOINC" schemeID="2.16.840.1.113883.6.1" displayName="Cardiology Outpatient Progress Note" />
                    <Code codeValue="34896-1" schemeName="LOINC" schemeID="2.16.840.1.113883.6.1" displayName="Cardiology Interventional Procedure Report" />
                    <Code codeValue="18142-0" schemeName="LOINC" schemeID="2.16.840.1.113883.6.1" displayName="Cardiology Echo Heart Chambers Report" />
                    <Code codeValue="18145-3" schemeName="LOINC" schemeID="2.16.840.1.113883.6.1" displayName="Cardiology Echo Heart Valves Report" />
                    <Code codeValue="18140-4" schemeName="LOINC" schemeID="2.16.840.1.113883.6.1" displayName="Cardiology Echo Report" />
                    <Code codeValue="72170-4" schemeName="LOINC" schemeID="2.16.840.1.113883.6.1" displayName="Dermatology Image" />
                    <Code codeValue="34759-1" schemeName="LOINC" schemeID="2.16.840.1.113883.6.1" displayName="Dermatology Report" />
                    <Code codeValue="34122-2" schemeName="LOINC" schemeID="2.16.840.1.113883.6.1" displayName="Pathology Procedure Note" />
                    <Code codeValue="60570-9" schemeName="LOINC" schemeID="2.16.840.1.113883.6.1" displayName="Pathology Report - text/scanned" />
                    <Code codeValue="60571-7" schemeName="LOINC" schemeID="2.16.840.1.113883.6.1" displayName="Pathology Report - structured CDA" />
                    <Code codeValue="NotUsed" schemeName="NotUsed" schemeID="NotUsed" displayName="NotUsed" />
                </classification>
                <!-- formatCode should be selected to match the MIME type of the file to be uploaded. -->
                <classification name="formatCodes">
                    <Code codeValue="application/pdf" schemeName="pdf" schemeID="pdf" displayName="PDF" />
                    <Code codeValue="image/tif" schemeName="tif" schemeID="tif" displayName="image/tif" />
                    <Code codeValue="image/jpeg" schemeName="jpg" schemeID="jpg" displayName="image/jpeg" />
                    <Code codeValue="image/png" schemeName="png" schemeID="png" displayName="image/png" />
                    <Code codeValue="application/dicom" schemeName="dcm" schemeID="dcm" displayName="DICOM" />
                    <Code codeValue="video/mp4" schemeName="mp4" schemeID="mp4" displayName="video/mp4" />
                    <Code codeValue="video/mov" schemeName="mov" schemeID="mov" displayName="video/quicktime" />
                    <Code codeValue="video/wmv" schemeName="wmv" schemeID="wmv" displayName="video/x-ms-wmv" />
                    <Code codeValue="video/avi" schemeName="avi" schemeID="avi" displayName="video/msvideo" />
                    <Code codeValue="1.2.840.10008.1.2.4.50" schemeName="DICOM" schemeID="1.2.840.10008.2.6.1" displayName="JPEG" />
                    <Code codeValue="urn:ihe:rad:PDF" schemeName="IHE" schemeID="1.3.6.1.4.1.19376.1.2.3" displayName="PDF" />
                    <Code codeValue="urn:ihe:iti:xds-sd:text:2008" schemeName="IHE" schemeID="1.3.6.1.4.1.19376.1.2.3" displayName="Scanned Document" />
                    <Code codeValue="1.2.840.10008.5.1.4.1.1.88.59" schemeName="DICOM" schemeID="1.2.840.10008.2.6.1" displayName="DICOM KOS" />
                    <Code codeValue="urn:ihe:rad:TEXT" schemeName="IHE" schemeID="1.3.6.1.4.1.19376.1.2.3" displayName="CDA Wrapped Text" />
                    <Code codeValue="urn:ihe:rad:CDA:ImagingReportStructuredHeadings:2013" schemeName="IHE" schemeID="1.3.6.1.4.1.19376.1.2.3" displayName="CDA Document" />
                    <Code codeValue="urn:ihe:iti:bppc:2007" schemeName="IHE" schemeID="1.3.6.1.4.1.19376.1.2.3" displayName="Privacy and Consent Coded Form" />
                    <Code codeValue="urn:ihe:iti:bppc-sd:2007" schemeName="IHE" schemeID="1.3.6.1.4.1.19376.1.2.3" displayName="Privacy and Consent Scanned Form" />
                    <Code codeValue="NotUsed" schemeName="NotUsed" schemeID="NotUsed" displayName="NotUsed" />
                </classification>
                <!-- for file upload: practiceSettingCode will be set to the same value as classCode -->
                <classification name="practiceSettingCodes">
                    <Code codeValue="NotUsed" schemeName="Karos demo practiceSettingCodes" schemeID="1.2.3.4.5.6.8" displayName="NotUsed" />
                    <Code codeValue="NotUsed" schemeName="NotUsed" schemeID="NotUsed" displayName="NotUsed" />
                </classification>
                <!--  Take these values from HL7 v3 RoleCode [2.16.840.1.113883.5.111] (HL7 defined coding scheme)
                Value set is children of ServiceDeliveryLocationRoleType in the file:
                CDA_R2_NormativeWebEdition2005\infrastructure\vocabulary\RoleCode.htm
                Note that the coding scheme UID is from a later release of HL7v3, as it had not been
                assigned yet in 2005.  Also note that there HL7 defines all departments and divisions (about 70 values)
                and the list here is a very small subset.
                -->
                <classification name="healthcareFacilityTypeCodes">
                    <Code codeValue="NotUsed" schemeName="NotUsed" schemeID="NotUsed" displayName="NotUsed" />
                    <!--Code codeValue="HOSP" schemeName="HL7" schemeID="2.16.840.1.113883.5.111" displayName="Hospital" />
                    <Code codeValue="CHR" schemeName="HL7" schemeID="2.16.840.1.113883.5.111" displayName="Chronic Care Facility" />
                    <Code codeValue="RH" schemeName="HL7" schemeID="2.16.840.1.113883.5.111" displayName="Rehabilitation Hospital" />
                    <Code codeValue="ER" schemeName="HL7" schemeID="2.16.840.1.113883.5.111" displayName="Emergency Department" />
                    <Code codeValue="ICU" schemeName="HL7" schemeID="2.16.840.1.113883.5.111" displayName="Intensive Care Unit" />
                    <Code codeValue="NCCF" schemeName="HL7" schemeID="2.16.840.1.113883.5.111" displayName="Nursing/Custodial Care Facility" />
                    <Code codeValue="ORTHO" schemeName="HL7" schemeID="2.16.840.1.113883.5.111" displayName="Orthopedics Clinic" />
                    <Code codeValue="PEDC" schemeName="HL7" schemeID="2.16.840.1.113883.5.111" displayName="Pediatrics Clinic" />
                    <Code codeValue="OF" schemeName="HL7" schemeID="2.16.840.1.113883.5.111" displayName="Outpatient Clinic" />
                    <Code codeValue="PROFF" schemeName="HL7" schemeID="2.16.840.1.113883.5.111" displayName="Provider's Office" />
                    <Code codeValue="AMB" schemeName="HL7" schemeID="2.16.840.1.113883.5.111" displayName="Ambulance" /-->
                </classification>
                <classification name="eventCodes">
                    <!-- Please do NOT reorder this list, e.g., do not put it into alphabetic order. -->
                    <!-- This is the correct order as listed here. -->
                    <Code codeValue="T-D1100" schemeName="SRT" schemeID="2.16.840.1.113883.6.5" displayName="Head" />
                    <Code codeValue="T-D1600" schemeName="SRT" schemeID="2.16.840.1.113883.6.5" displayName="Neck" />
                    <Code codeValue="T-D8000" schemeName="SRT" schemeID="2.16.840.1.113883.6.5" displayName="Upper Extremity" />
                    <Code codeValue="T-32000" schemeName="SRT" schemeID="2.16.840.1.113883.6.5" displayName="Cardiovascular" />
                    <Code codeValue="T-D3000" schemeName="SRT" schemeID="2.16.840.1.113883.6.5" displayName="Chest" />
                    <Code codeValue="T-11501" schemeName="SRT" schemeID="2.16.840.1.113883.6.5" displayName="Cervical Spine" />
                    <Code codeValue="T-11502" schemeName="SRT" schemeID="2.16.840.1.113883.6.5" displayName="Thoracic Spine" />
                    <Code codeValue="T-11503" schemeName="SRT" schemeID="2.16.840.1.113883.6.5" displayName="Lumbar Spine" />
                    <Code codeValue="T-D4000" schemeName="SRT" schemeID="2.16.840.1.113883.6.5" displayName="Abdomen" />
                    <Code codeValue="T-D6000" schemeName="SRT" schemeID="2.16.840.1.113883.6.5" displayName="Pelvis" />
                    <Code codeValue="T-D9000" schemeName="SRT" schemeID="2.16.840.1.113883.6.5" displayName="Lower Extremity" />
                    <Code codeValue="T-D0010" schemeName="SRT" schemeID="2.16.840.1.113883.6.5" displayName="Entire Body" />
                </classification>
            </prop>
        </config>
    </service>

    <service type="rialto.ui" id="rialto.ui">
        <device idref="pix" />
        <device idref="pdq" />

        <server idref="navigator-http" name="web-ui">
            <url>/</url>
        </server>

        <server idref="http-admin-tools" name="admin-tools">
        <!-- Not configurable at this time -->
            <url>/v2/</url>
        </server>

        <server idref="authenticated-http" name="web-api">
            <url>/public/*</url>
        </server>

        <config>
            <prop name="ApiURL" value="${Navigator-HostProtocol}://${Navigator-Host}:${Navigator-APIPort}/rest/api/rialto"/>
            <prop name="ArrURL" value="${Navigator-HostProtocol}://${Navigator-Host}:${ARR-GUIPort}"/>
            <prop name="StudyManagementURL" value="http://${HostIP}:8080${IA-StudyManagement-Path}"/>
            <prop name="WadoURL" value="http://${HostIP}:8080/vault/wado" />
            <prop name="MintApiURL" value="http://${HostIP}:8080/vault/mint" />
            <prop name="DataflowApiURL" value="http://${HostIP}:13337/monitoring" />
            <prop name="WorkflowApiURL" value="http://${HostIP}:13337/workflows" />
            <prop name="ElasticsearchRestApiURL" value="http://10.0.1.165:9200" />
            <prop name="DeviceRegistryURL" value="http://${HostIP}:8080/rialto/deviceregistry" />
            <prop name="ModalityWorklistURL" value="http://${HostIP}:8085/mwl" />
            <prop name="DicomQcToolsURL" value="http://${HostIP}:8080/vault/qc" />
            <prop name="IlmURL" value="http://${HostIP}:8080/vault/ilm" />
            <prop name="GlobalInactivitySessionTimeout" value="${IdleUserSessionTimeout}" />
            <prop name="ExternalPDQConfigured" value="false" />
            <prop name="RialtoAsACache" value="false" />

            <prop name="RialtoImageArchiveAETitle" value="${Router-AETitle}" />

            <!-- ConnectQueryRetrieveAETitle is used for query/retrieve of catalogued studies
                 This property should be enabled if you want to enable the import of catalogued studies in Rialto Admin tools
                 It's value should match a configured Connect device -->
            <!-- <prop name="ConnectQueryRetrieveAETitle" value="CONNECT" /> -->

            <prop name="SupportedThemes">
                rialto-light
                rialto-dark
            </prop>
            <prop name="DefaultTheme" value="${Navigator-DefaultTheme}"/>

            <prop name="SupportedLocales">
                en
                fr
            </prop>
            <prop name="DefaultLocale" value="${Navigator-DefaultLocale}"/>

            <prop name="BreakTheGlassOptions">
                <BreakTheGlassOption>Emergency access required for patient care.</BreakTheGlassOption>
            </prop>

            <prop name="Plugins">
                <plugin>
                    <name>${CDAPlugin-Name}</name>
                    <description>${CDAPlugin-Description}</description>
                    <mimeTypes>text/xml</mimeTypes>
                    <url>${Navigator-HostProtocol}://${Navigator-Host}:${Navigator-APIPort}/rest/api/plugins/cda</url>
                    <embedded>true</embedded>
                </plugin>

                <plugin>
                    <name>${DownloadPlugin-Name}</name>
                    <description>${DownloadPlugin-Description}</description>
                    <mimeTypes>*</mimeTypes>
                    <url>${Navigator-HostProtocol}://${Navigator-Host}:${Navigator-APIPort}/rest/api/plugins/download</url>
                    <embedded>true</embedded>
                </plugin>
            </prop>

            <!-- Configure eventCodeSchemes to support searching for documents by eventCode -->
            <prop name="EventCodeSchemes">
                <scheme schemeID="" schemeName="CareGiverScheme" />
                <scheme schemeID="" schemeName="CareUnitScheme" />
                <scheme schemeID="" schemeName="kibiGroupId" />
                <scheme schemeID="" schemeName="DocTitle" />
            </prop>
            <include location="patientidentitydomains.xml"/>

        </config>
    </service>
</config>


