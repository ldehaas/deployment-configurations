/**
 * converts HL7 ORU to a DICOM SR object
 * draft according to the SAD v0.5
 *
 * many of the HL7 locations are potentially different in the field. this 
 * script is simply a starting point.
 */
import org.dcm4che2.util.UIDUtils
import org.joda.time.DateTime
LOAD("common.groovy")



set(SpecificCharacterSet, 'ISO_IR_100')

// Top-level Variables used
def acc_num = get('OBR-19')

def studyFromLakeShore = null
def studyFromSteJustine = null
def studyFromSteJustineFalse = null


log.info("Received an ORU, will attempt to Convert it to an SR and Publish it")

log.debug("Checking the Value of MSH-4 to see what the Raw ORU has as Issuer Of Pid:\"{}\"", get('MSH-4') )

// Set local Affinity Domain.  It will be missing in the ORU feed
 def issuerOfPID = null
 def officialIssuerOfPID = get('PID-3-4-2')
 def affinityNotSet = true

 if ( null != officialIssuerOfPID ) {
    // The Issuer of PID officially should be in PID-3-4-2, but most of the time, it is not sent
    
    knownIssuersOfPID = [
        '2.16.124.10.101.1.60.1.1007.1':'Lakeshore',
        '2.16.124.10.101.1.60.1.1004.1':'Ste-Justine',
	'2.16.124.113638.1.4.3':'RJH',
	'1.1.2':'Context2',
	'1.1.3':'Context3'
    ]

    if(null != knownIssuersOfPID[officialIssuerOfPID] ){

        // IssuerOfPatientID was set correctly from the start
        set(IssuerOfPatientID, officialIssuerOfPID)
        affinityNotSet = false
        log.debug("Issuer of Patient ID (i.e. Affinity Domain) is populated as per the Standard on PID-3-4-2")
        log.debug("The ORU to SR Conversion is assuming this Patient is from " + knownIssuersOfPID[officialIssuerOfPID] )
    }
 }  


 if(affinityNotSet) {

    def msh4 = get('MSH-4').toUpperCase().replaceAll(" ", "");

    log.debug("The current uppercase non-space value of the MSH-4 segment in this ORU message is: \"{}\"", msh4)

    if ( "SHSJPACSTST2" == get('MSH-4') ) {
        issuerOfPID = '2.16.124.10.101.1.60.1.1004.1'
        log.debug("The ORU to SR Conversion is assuming this Patient is from Ste-Justine (Fake Study)")
        set(InstitutionName, 'HSJ')
        studyFromSteJustineFalse = true
    } else if ("HSJ" == get('MSH-4')) {
        issuerOfPID = '2.16.124.10.101.1.60.1.1004.1'
        log.debug("The ORU to SR Conversion is assuming this Patient is from Ste-Justine (Real Study)")
        set(InstitutionName, 'HSJ')
        studyFromSteJustine = true
    } else if ( "SVR-TSERV0" == get('MSH-4') || "LKSHR" == get('MSH-4') ) {
        issuerOfPID = '2.16.124.10.101.1.60.1.1007.1'
        log.debug("The ORU to SR Conversion is assuming this Patient is from Lakeshore")
        set(InstitutionName, 'Lakeshore')
        studyFromLakeShore = true
    } else if ("RJH" == msh4) {
	issuerOfPID = '2.16.124.113638.1.4.3';
        log.debug("The ORU to SR Conversion is assuming this Patient is from RJH")
        set(InstitutionName, 'RJH')
	studyFromLakeShore = true
    } else if ("CONTEXT2" == msh4) {
        issuerOfPID = '1.1.2';
        log.debug("The ORU to SR Conversion is assuming this Patient is from Context 2")
        set(InstitutionName, 'Context 2')
        studyFromLakeShore = true
    } else if ("CONTEXT3" == msh4) {
        issuerOfPID = '1.1.3';
        log.debug("The ORU to SR Conversion is assuming this Patient is from Context 3")
        set(InstitutionName, 'Context 3')
        studyFromLakeShore = true
    } else { 
        log.warn("WARNING: This script doesn't know what to do with the ORU traffic it's receiving.  The Affinity Domain will likely be wrong and the PIX Lookup will fail.")
        log.info("ORU to SR conversion doesn't know the correct Sending Facility, and cannot assign an IssuerOfPatientID.")
        
        // UNKWOWN is an actual value that will be caught by the Manifest Morpher and submission will fail.  
        // We need to populate it to have complete statistics.
        issuerOfPID = 'UNKNOWN'
    }

    set(IssuerOfPatientID, issuerOfPID)
    log.debug("\n\nORU to SR Conversion: the Issuer of Patient ID being set is \"{}\"", issuerOfPID)
 }  



// Temporary marker for Received Reports
log.info("\n\nReceived an ORU Report, and will add it to an existing Manifest\n\n")


now = new DateTime()
set(InstanceCreationDate, now)
set(InstanceCreationTime, now)

// This is a unique Value per instance of Vault
set(InstanceCreatorUID, '1.2.3.4.5.6.7.8.9')


set(SOPClassUID, '1.2.840.10008.5.1.4.1.1.88.11') // basic SR

def SIUID = get('ZDS-1') 
if(null == SIUID)
{
    log.error("Study Instance UID as received in the ZDS-1 Segment is empty!!  Submission will certainly fail!")
} else if(SIUID.isEmpty() ){
    log.error("Study Instance UID as received in the ZDS-1 Segment is empty!!  Submission will certainly fail!")
} else {
    log.debug("Current Value of the Study Instance UID appended in the ZDS Segment is \"{}\"", SIUID)
}


set(StudyInstanceUID, SIUID)
def uid_root = '2.25.123456' // this must be no longer than 23 chars
set(SeriesInstanceUID, UIDUtils.createUID(uid_root))
set(SOPInstanceUID, UIDUtils.createUID(uid_root))

// Set study time and date
 def obr6 = get('OBR-6')
 def obr7 = get('OBR-7')
 log.debug("\nORU to SR Conversion:  Contents of the OBR-6 = \"{}\"",obr6)
 def obr22 = get('OBR-22')

 def obr6_valid = false
 def obr7_valid = false
 def obr22_valid = false

 def studyDate = null
 def studyTime = null
 def studyDateAlt = null
 def studyTimeAlt = null
 def contentDate = null
 def contentTime = null

 def checkValidation = null


 // Validate Dates and Times
    checkValidation = DICOMTime.validateDateTime(obr6)
    log.debug("Current Value of DICOM Time validation for OBR6 = {}", checkValidation)
  
  if (! DICOMTime.validateDateTime(obr6) ) {
   // Fail the OBR-6 as a valid date segment if it is not complete
    obr6_valid = false
    log.warn("\n\n\nWARNING: ORU to SR Conversion: OBR-6 Segment did **NOT** contain a Valid Date Format.  ORU to SR Conversion might fail.")
  } else {
    // Parse the date and time
    log.debug("Valid Study date and time found in OBR-6")
    obr6_valid = true
  }

    checkValidation = DICOMTime.validateDateTime(obr7)
    log.debug("Current Value of DICOM Time validation for OBR7 = {}", checkValidation)
  if (! DICOMTime.validateDateTime(obr7)) {
   // Fail the OBR-7 as a valid date segment if it is not complete
    obr7_valid = false
    if(! obr6_valid) {
      log.warn("\n\n\nWARNING: ORU to SR Conversion: OBR-7 Segment did **NOT** contain a Valid Date Format.  ORU to SR Conversion might fail.")
    }
  } else {
   // Parse the date and time
    log.debug("Valid Study date and time found in OBR-7")
    obr7_valid = true
  }

    checkValidation = DICOMTime.validateDateTime(obr22)
    log.debug("Current Value of DICOM Time validation for OBR22 = {}", checkValidation)
  if (! DICOMTime.validateDateTime(obr22)) {
   // Fail the OBR-22 as a valid date segment if it is not complete
    throw new Exception ("ORU to SR Conversion: Invalid Date / Time Format in both OBR-22.  Aborting SR to ORU Conversion\n")
  } else {
    // Parse the date and time
    set(ContentDate, DICOMTime.parseDate(obr22))
    set(ContentTime, DICOMTime.parseTime(obr22))
  }


 // Set Dates and Times
 if(obr6_valid){
    log.debug("Setting Study and Series date and time from OBR-6")
    studyDate = DICOMTime.parseDate(obr6)
    studyTime = DICOMTime.parseTime(obr6)
 } else if(obr7_valid){
    log.debug("Setting Study and Series date and time from OBR-7")
    studyDate = DICOMTime.parseDate(obr7)
    studyTime = DICOMTime.parseTime(obr7)
 } else {
    throw new Exception ("ORU to SR Conversion: Invalid Date Formats in both OBR-6 and OBR-7.  Aborting SR to ORU Conversion\n")
 }
 
 set(StudyDate, studyDate)
 set(StudyTime, studyTime)
 set(SeriesDate, studyDate)
 set(SeriesTime, studyTime)



// Some more top-level fields 
 set(AccessionNumber, acc_num)
 set(Modality, 'SR')

 set(StudyID, acc_num)

// Referring Physicians Name and Sequence
 setPersonName(ReferringPhysicianName, 'OBR-16', startingPosition=2)
 set('ReferringPhysicianIdentificationSequence/InstitutionName', get('MSH-4'))

 def seq = 'ReferringPhysicianIdentificationSequence/PersonIdentificationCodeSequence';
 set(seq+'/CodeValue', get('OBR-16-1'))
 set(seq+'/CodingSchemeDesignator', get('MSH-4'))
 set(seq+'/CodeMeaning', 'Referring Physician ID')

// Some more top-level fields 
 set(StationName, get('ORC-13'))
 set(StudyDescription, get('OBR-4-2'))

 def nameOfDrReadingStudy = get('OBR-32-2')
 nameOfDrReadingStudy = nameOfDrReadingStudy.replace(',', '^')
 set(NameOfPhysiciansReadingStudy, nameOfDrReadingStudy)

// Anatomic Region Sequence

 anatomicMap = [
    "AB":"Abdomen",
    "AN":"Angiographie",
    "AU":"Autre",
    "CA":"Cardio-vasculaire",
    "CO":"Colonnes",
    "DI":"Digestif",
    "EI":"Extrémités inférieures",
    "EN":"Endocrinien",
    "ES":"Extrémités supérieures",
    "GO":"Gynéco-obstétrique",
    "HE":"Hémodynamie",
    "HP":"Hématopoïétique",
    "SE":"Sein",
    "SQ":"Squelettique",
    "TC":"Tête et cou",
    "TH":"Thorax",
    "TP":"Tomographie par émission de positrons"
    ]

 def anatomicRegionCode = null
 def anatomicCodeDescription = null
 def anatomicCodeScheme = null

 // LakeShore sends the anatomic region in a different place
 if(studyFromLakeShore){
    anatomicRegionCode = get('OBR-15-4')
    anatomicCodeScheme = 'Imagerie Québec-DSQ'
 
 } else {
    anatomicRegionCode = get('OBR-15-1-1')
    
    if (null != get('OBR-15-1-3')) {
        anatomicCodeScheme = get('OBR-15-1-3')
    } else {
       log.warn("ORU to SR Report Conversion: Non-LakeShore ORU had a blank segment in OBR-15-1-3.  Falling back to default value, \"Imagerie Québec-DSQ\"")
       anatomicCodeScheme = 'Imagerie Québec-DSQ'
    }
 }

 // Derive Anatomic Region Code Display Name from Anatomic Region
 if(null != anatomicRegionCode) {
    if(anatomicMap[anatomicRegionCode] != null){
        anatomicCodeDescription = anatomicMap[anatomicRegionCode]
    } else {
        // The Region Code exists, but it's not know in the Anatomic Region Value Map
        anatomicCodeDescription = 'Autre'
    }
 } else {
    anatomicRegionCode = "AU"
    anatomicCodeDescription = 'Autre'
 }

 log.debug("Current anatomicRegionCode is {}", anatomicRegionCode)
 log.debug("Current anatomicCodeDescription is {}", anatomicCodeDescription)
 log.debug("Current anatomicCodeScheme is {}", anatomicCodeScheme)

 set('AnatomicRegionSequence/CodeValue', anatomicRegionCode)
 set('AnatomicRegionSequence/CodingSchemeDesignator', anatomicCodeScheme)
 set('AnatomicRegionSequence/CodeMeaning', anatomicCodeDescription)
 


// Patient ID and Demographics
 setPersonName(PatientName, 'PID-5')
 set(PatientID, get('PID-3-1'))
 set(PatientBirthDate, get('PID-7'))
 set(PatientSex, get('PID-8'))

// Study Metadata
 set(InstanceNumber, '1')
 set(StudyStatusID, get('OBR-25'))
 set(ReasonForStudy, get('ORC-16'))
 if(studyFromLakeShore || studyFromSteJustine){
    def orc12_2 = get('ORC-12-2')
    if (orc12_2 != null) {
        orc12_2 = orc12_2.replace(',', '^')
        set(RequestingPhysician, orc12_2)
    }
 } else {
    set(RequestingPhysician, get('ORC-12'))
 }
 set(RequestingService, get('MSH-4'))

// Requested Procedure Code Sequence
 set('RequestedProcedureCodeSequence/CodeValue', get('OBR-4-1'))
 log.debug("\n\n\n\n\n\n\n\n\n\n THE CURRENT VALUE OF OBR-4-1 in HL7 ORU is: {}",get('OBR-4-1'))
 set('RequestedProcedureCodeSequence/CodingSchemeDesignator', 'Imagerie Québec-DSQ')
 set('RequestedProcedureCodeSequence/CodeMeaning', get('OBR-4-2'))

 set(RequestedProcedureID, acc_num)

// Current Patient Location
 currentLocation = get('PV1-3')
 if(null != currentLocation) {
    set(CurrentPatientLocation, currentLocation)
    log.debug("\nORU To SR Conversion: Setting Current Patient Location to \"{}\"", currentLocation)
 }

set(ValueType, 'CONTAINER')

set('ConceptNameCodeSequence/CodeValue', '18748-4')
set('ConceptNameCodeSequence/CodingSchemeDesignator', 'LN')
set('ConceptNameCodeSequence/CodeMeaning', 'Diagnostic Imaging Report')

set(ContinuityOfContent, 'CONTINUOUS')

// Verifying Observer Sequence
 def obr32_2 = get('OBR-32-2')
    obr32_2 = obr32_2.replace(',', '^')
 set('VerifyingObserverSequence/VerifyingOrganization', get('MSH-4'))
 if(null != obr22) {
    set('VerifyingObserverSequence/VerificationDateTime', obr22)
    log.debug("Populating VerificationDateTime In Verifying Observer Sequence from OBR-22")
 } else {
    set('VerifyingObserverSequence/VerificationDateTime', get('OBR-7'))
    log.warn("SR to ORU Conversion:  WARNING\n\nThe VerificationDateTime In the Verifying Observer Sequence is currently being Populated from OBR-7, as opposed to from OBR-22.\n (OBR-22 was NULL)  As a result, the WADOURI for this report might NOT be correctly updated by the XDS Registry.\n\n ")
 }
 set('VerifyingObserverSequence/VerifyingObserverName', obr32_2)

 // Verifying Observer Identification code sub-sequence
   // The sequence itself is optional; if provided, however, all constituent values are mandatory
    seq = 'VerifyingObserverSequence/VerifyingObserverIdentificationCodeSequence'
    set(seq+'/CodeValue', get('OBR-32-1'))
    set(seq+'/CodingSchemeDesignator', get('MSH-4'))
    set(seq+'/CodeMeaning', obr32_2)


// Referenced Request Sequence
 set('ReferencedRequestSequence/AccessionNumber', acc_num)
 set('ReferencedRequestSequence/StudyInstanceUID', get('ZDS-1'))
 set('ReferencedRequestSequence/RequestedProcedureDescription', get('OBR-4-2'))
 set('ReferencedRequestSequence/RequestedProcedureID', acc_num)
 
 if(studyFromLakeShore || studyFromSteJustine || studyFromSteJustineFalse) {
    // set('ReferencedRequestSequence/PlacerOrderNumberImagingServiceRequest', get('OBR-19'))
    set('ReferencedRequestSequence/PlacerOrderNumberImagingServiceRequest', get('OBR-3'))
 } else {
    set('ReferencedRequestSequence/PlacerOrderNumberImagingServiceRequest', get('ORC-3'))
    // set('ReferencedRequestSequence/PlacerOrderNumberImagingServiceRequest', get('ORC-2'))
 }

 set('ReferencedRequestSequence/FillerOrderNumberImagingServiceRequest', get('OBR-19'))
 // set('ReferencedRequestSequence/FillerOrderNumberProcedure', get('ORC-3'))

 seq = 'ReferencedRequestSequence/RequestedProcedureCodeSequence'
    set(seq+'/CodeValue', get('OBR-4-1'))
    set(seq+'/CodeMeaning', get('OBR-4-2'))
    if(studyFromLakeShore || studyFromSteJustine || studyFromSteJustineFalse) {
        set(seq+'/CodingSchemeDesignator', get('MSH-4'))
    }



// Some more Top-Level Attributes
 set(CompletionFlag, 'COMPLETE')
 

 // Verification Flag
 def verificationFlag = null
 def verifiedStudy = false

 if(studyFromLakeShore || studyFromSteJustine){
   // Use OBX-11 for Lakeshore
    verificationFlag = get('OBX-11')
    log.debug("Study is from Lakeshore, and OBX-11 has \"{}\" in its value.", verificationFlag)
 } else {
   // Use OBR-25 for everything else
    verificationFlag = get('OBR-25')
    log.debug("Study is NOT from Lakeshore, and OBR-25 has \"{}\" in its value.", verificationFlag)
 }

 if(null != verificationFlag){ 
        verificationFlag = verificationFlag.trim() 
        verificationFlag = verificationFlag.toUpperCase() 
 }
 log.debug("ORU to SR: Cleaned up Verification Flag Content is: \"{}\"", verificationFlag)

 switch(verificationFlag) {
        case 'F' :
            verifiedStudy = true
            break
        case 'C' :
            verifiedStudy = true
            break
        case 'A' :
            verifiedStudy = true
            break
        default:
            verifiedStudy = false
 }

 if (verifiedStudy) {
        set(VerificationFlag, 'VERIFIED')
 } else {
        set(VerificationFlag, 'UNVERIFIED')
 }
    
    

// ContentSequence
 set('ContentSequence[1]/RelationshipType', 'HAS CONCEPT MOD')
 set('ContentSequence[1]/ValueType', 'CODE')
 set('ContentSequence[1]/ConceptNameCodeSequence/CodeValue', '121058')
 set('ContentSequence[1]/ConceptNameCodeSequence/CodingSchemeDesignator', 'DCM')
 set('ContentSequence[1]/ConceptNameCodeSequence/CodeMeaning', 'Procedure Reported')

 set('ContentSequence[1]/ConceptCodeSequence/CodeValue', get('OBR-4-1'))
 set('ContentSequence[1]/ConceptCodeSequence/CodingSchemeDesignator', 'RP')
 set('ContentSequence[1]/ConceptCodeSequence/CodeMeaning', get('OBR-4-2'))
 
 set('ContentSequence[2]/RelationshipType', 'HAS CONCEPT MOD')
 set('ContentSequence[2]/ValueType', 'CODE')
 set('ContentSequence[2]/ConceptNameCodeSequence/CodeValue', '121049')
 set('ContentSequence[2]/ConceptNameCodeSequence/CodingSchemeDesignator', 'DCM')
 set('ContentSequence[2]/ConceptNameCodeSequence/CodeMeaning', 'Language of Content Item and Descendants')

 set('ContentSequence[2]/ConceptCodeSequence/CodeValue', 'fr') // or en?
 set('ContentSequence[2]/ConceptCodeSequence/CodingSchemeDesignator', 'RFC3066')
 set('ContentSequence[2]/ConceptCodeSequence/CodeMeaning', 'French') // or English?

 set('ContentSequence[3]/RelationshipType', 'HAS OBS CONTEXT')
 set('ContentSequence[3]/ValueType', 'CODE')
 set('ContentSequence[3]/ConceptNameCodeSequence/CodeValue', '121005')
 set('ContentSequence[3]/ConceptNameCodeSequence/CodingSchemeDesignator', 'DCM')
 set('ContentSequence[3]/ConceptNameCodeSequence/CodeMeaning', 'Observer Type')

 set('ContentSequence[3]/ConceptCodeSequence/CodeValue', '121006')
 set('ContentSequence[3]/ConceptCodeSequence/CodingSchemeDesignator', 'DCM')
 set('ContentSequence[3]/ConceptCodeSequence/CodeMeaning', 'Person')

 set('ContentSequence[4]/RelationshipType', 'HAS OBS CONTEXT')
 set('ContentSequence[4]/ValueType', 'CODE')
 set('ContentSequence[4]/ConceptNameCodeSequence/CodeValue', '121011')
 set('ContentSequence[4]/ConceptNameCodeSequence/CodingSchemeDesignator', 'DCM')
 set('ContentSequence[4]/ConceptNameCodeSequence/CodeMeaning', 'Person Observer\'s Role in this Procedure')

 set('ContentSequence[4]/ConceptCodeSequence/CodeValue', '121097')
 set('ContentSequence[4]/ConceptCodeSequence/CodingSchemeDesignator', 'DCM')
 set('ContentSequence[4]/ConceptCodeSequence/CodeMeaning', 'Recording')
 
 set('ContentSequence[5]/RelationshipType', 'CONTAINS')
 set('ContentSequence[5]/ValueType', 'CONTAINER')
 set('ContentSequence[5]/ConceptNameCodeSequence/CodeValue', '121064')
 set('ContentSequence[5]/ConceptNameCodeSequence/CodingSchemeDesignator', 'DCM')
 set('ContentSequence[5]/ConceptNameCodeSequence/CodeMeaning', 'Current Procedure Description')
 set('ContentSequence[5]/ContinuityOfContent', 'CONTINUOUS')

 // Content Sequence [5] "Content Sequence" subsequence
 seq = 'ContentSequence[5]/ContentSequence'
    set(seq+'/RelationshipType', 'CONTAINS')
    set(seq+'/ValueType', 'TEXT')
    set(seq+'/ConceptNameCodeSequence/CodeValue', '121065')
    set(seq+'/ConceptNameCodeSequence/CodingSchemeDesignator', 'DCM')
    set(seq+'/ConceptNameCodeSequence/CodeMeaning', 'Procedure Description')
    set(seq+'/TextValue', get('OBR-4-1') + '^' + get('OBR-4-2'))

 set('ContentSequence[6]/RelationshipType', 'CONTAINS')
 set('ContentSequence[6]/ValueType', 'CONTAINER')
 set('ContentSequence[6]/ContinuityOfContent', 'CONTINUOUS')
 set('ContentSequence[6]/ConceptNameCodeSequence/CodeValue', '121070')
 set('ContentSequence[6]/ConceptNameCodeSequence/CodingSchemeDesignator', 'DCM')
 set('ContentSequence[6]/ConceptNameCodeSequence/CodeMeaning', 'Findings')

 // Content Sequence [6] "Content Sequence" subsequence
 seq = 'ContentSequence[6]/ContentSequence'
 set(seq+'/RelationshipType', 'CONTAINS') // Not sure if this line-item should actually be here
 set(seq+'/ValueType', 'TEXT')
 set(seq+'/ConceptNameCodeSequence/CodeValue', '121071')
 set(seq+'/ConceptNameCodeSequence/CodingSchemeDesignator', 'DCM')
 set(seq+'/ConceptNameCodeSequence/CodeMeaning', 'Finding')

 set(seq+'/TextValue', getList('OBSERVATION(*)/OBX-5(*)').join('\n'))

 
 set('ContentSequence[7]/RelationshipType', 'CONTAINS')
 set('ContentSequence[7]/ValueType', 'CONTAINER')
 //Concept Name Code Sequence
 set('ContentSequence[7]/ConceptNameCodeSequence/CodeValue', '121072')
 set('ContentSequence[7]/ConceptNameCodeSequence/CodingSchemeDesignator', 'DCM')
 set('ContentSequence[7]/ConceptNameCodeSequence/CodeMeaning', 'Impressions')
 
 // Begin Content According to Wayne
 set('ContentSequence[7]/ContinuityOfContent', 'SEPARATE')
 set('ContentSequence[7]/ContentSequence/RelationshipType', 'CONTAINS')
 set('ContentSequence[7]/ContentSequence/ValueType', 'TEXT')
 set('ContentSequence[7]/ContentSequence/ConceptNameCodeSequence/CodeValue', '121073')
 set('ContentSequence[7]/ContentSequence/ConceptNameCodeSequence/CodingSchemeDesignator', 'DCM')
 set('ContentSequence[7]/ContentSequence/ConceptNameCodeSequence/CodeMeaning', 'Impression')
 set('ContentSequence[7]/ContentSequence/TextValue', 'This is a hard-coded impresssion, put in here by Rodrigo.  Just as a placeholder of what would be in the actual report.  We still do not know where to map it from')


 /*
 // COULD I ACTUALLY SKIP THIS?? Concept Code Sequence
 set('ContentSequence[7]/ConceptCodeSequence/CodeValue', ' I DONT KNOW IF YOU CAN SEE THESE These are some hard-coded impresssions, put in here by Rodrigo.  Just as a placeholder of what would be in the actual report.  We still do not know where to map it from')
 set('ContentSequence[7]/ConceptCodeSequence/CodingSchemeDesignator', 'DCM')
 set('ContentSequence[7]/ConceptCodeSequence/CodeMeaning', 'Impression')
 // END OF POSSIBLY IRRELEVANT TEXT

 set('ContentSequence[8]/RelationshipType', 'CONTAINS')
 set('ContentSequenc[8]/ValueType', 'CONTAINER')
set('ContentSequence[8]/ContinuityOfContent', 'CONTINUOUS') 
 set('ContentSequence[8]/ContentSequence/ValueType', 'TEXT')

 set('ContentSequence[8]/ContentSequence/ConceptNameCodeSequence/CodeValue', '121073')
 set('ContentSequence[8]/ContentSequence/ConceptNameCodeSequence/CodingSchemeDesignator','DCM')
 set('ContentSequence[8]/ContentSequence/ConceptNameCodeSequence/CodeMeaning', 'Impression')
 set('ContentSequence[8]/ContentSequence/TextValue', 'These are some hard-coded impresssions, put in here by Rodrigo.  Just as a placeholder of what would be in the actual report.  We still do not know where to map it from') */ 
