log.info("Here's how the order message looks:]\n{}", input)
log.info("And here's how the initial Imaging Service Request looks like\n{}", imagingServiceRequest)

if (imagingServiceRequest.getAccessionNumberUniversalId() == null) {
    imagingServiceRequest.setAccessionNumberNamespaceId("RJH")
    imagingServiceRequest.setAccessionNumberUniversalId("2.16.124.113638.1.4.3")
    imagingServiceRequest.setAccessionNumberUniversalIdType("ISO")
}
def patientIdentification = imagingServiceRequest.getPatientIdentification()

if (patientIdentification.getPatientIdUniversalId() == null) {
    patientIdentification.setPatientIdNamespaceId("RJH")
    patientIdentification.setPatientIdUniversalId(get("2.16.124.113638.1.4.3"))
    patientIdentification.setPatientIdUniversalIdType("ISO")
}

imagingServiceRequest.setPatientIdentification(patientIdentification)

def patientDemographics = imagingServiceRequest.getPatientDemographics()
//patientDemographics.setPatientBirthDate(get("PID-6"))
imagingServiceRequest.setPatientDemographics(patientDemographics)

def requestedProcedure = imagingServiceRequest.getRequestedProcedureSequence().get(0)
requestedProcedure.setRequestedProcedureDescription(get("OBR-5"))
//requestedProcedure.setStudyInstanceUID(get("ZDS-1"))

def scheduledProcedureStep = requestedProcedure.getScheduledProcedureStepSequence().get(0)
//scheduledProcedureStep.setScheduledProcedureStepStatus(get("OBR-1"))
scheduledProcedureStep.setScheduledProcedureStepLocation(get("OBR-21"))

if (scheduledProcedureStep.getScheduledProcedureStepLocation() == null) {
    log.debug("MWL ISR Morpher - about to set Scheduled Procedure Step Location to UNKNOWN");
    scheduledProcedureStep.setScheduledProcedureStepLocation("UNKNOWN");
}

scheduledProcedureStep.setScheduledStationName(get("OBR-20"))
if (scheduledProcedureStep.getScheduledStationName() == null) {
    log.debug("MWL ISR Morpher - about to set Scheduled Station Name to UNKNOWN");
    scheduledProcedureStep.setScheduledStationName("UNKNOWN");
}

if (scheduledProcedureStep.getModality() == null) {
    log.debug("MWL ISR Morpher - about to set Modality to UNKNOWN");
    scheduledProcedureStep.setModality("UNKNOWN");
}

def orderControlCodeReason = get("ORC-1")
if (orderControlCodeReason != null) {
    switch (orderControlCodeReason) {
        case 'NW':
            log.debug("MWL ISR Morpher - about to set Scheduled Procedure Step Status to SCHEDULED")
            scheduledProcedureStep.setScheduledProcedureStepStatus('SCHEDULED')
            break
        case 'CA':
            log.debug("MWL ISR Morpher - about to set Scheduled Procedure Step Status to DISCONTINUED")
            scheduledProcedureStep.setScheduledProcedureStepStatus('DISCONTINUED')
            break
        case 'SC':
            log.debug("MWL ISR Morpher - about to set Scheduled Procedure Step Status to INPROGRESS")
            scheduledProcedureStep.setScheduledProcedureStepStatus('INPROGRESS')
            break
        default:
            log.debug("MWL ISR Morhper - about to set Scheduled Procedure Step Status to UNKNOWN")
            scheduledProcedureStep.setScheduledProcedureStepStatus('UNKNOWN')
    }
}

//scheduledProcedureStep.setModality(get("OBR-3"))
//if (scheduledProcedureStep.getModality() == null) {
//    scheduledProcedureStep.setModality("CT");
//}

if (requestedProcedure.getRequestedProcedureID() == null){
    requestedProcedure.setRequestedProcedureID(imagingServiceRequest.getAccessionNumber())
}

if (scheduledProcedureStep.getScheduledProcedureStepIDString() == null){
    scheduledProcedureStep.setScheduledProcedureStepIDString(imagingServiceRequest.getAccessionNumber())
}

def AETitle = scheduledProcedureStep.getScheduledProcedureStepLocation()
scheduledProcedureStep.setScheduledStationAETitle(AETitle);
log.debug("Setting Scheduled Station AE Title to {}", AETitle);

log.info("Finished fixing up the Imaging Service Request...Here it is\n{}", imagingServiceRequest)
