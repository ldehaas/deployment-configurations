log.info("Start EMPI Morpher")

set('PID-3-4-1', 'RJH');
set('PID-3-4-2', '2.16.124.113638.1.4.3');
set('PID-3-4-3', 'ISO');

set('MSH-3', 'RIALTO_RJH');
set('MSH-4', 'KAROS_RJH');

def messageType = get('/.MSH-9-1')
def triggerEvent = get('/.MSH-9-2')

log.info("messageType is: {}", messageType)
log.info("triggerEvent is: {}", triggerEvent)

if (messageType != null && triggerEvent != null) {
    if ("ADT".equals(messageType) && ("A08".equals(triggerEvent) || "A40".equals(triggerEvent) || "A31".equals(triggerEvent) || "A39".equals(triggerEvent) || "A04".equals(triggerEvent))) {
        log.info("will forward message to EMPI Update")
    } else {
        log.info("will not forward to EMPI Update")
        return false
    }
}

log.info("End EMPI Morpher")
