log.debug("document_metadata_morpher.groovy: Start morpher")

// use extended metadata to keep track of study instance uid
set(XDSExtendedMetadata('studyInstanceUid'), get(StudyInstanceUID))

//set(XDSExtendedMetadata('accessionNumber'), get('AccessionNumber'))
def accessionNumber = get(AccessionNumber)

// Sanity Check
if(null == accessionNumber) {
    log.warn("XDS Metadata creation, manifest submission:\nAccession Number was NULL.")
} else {
    set(XDSExtendedMetadata('accessionNumber'), accessionNumber)
}

//set(XDSCreationTime, get('StudyDate'))
//set XDSCreationTime to InstanceCreationTime, StudyDate or CurrentDate.
if (get(InstanceCreationDate) != null) {
    set(XDSCreationTime,get(InstanceCreationDate),get(InstanceCreationTime))
} else if (get(StudyDate) != null) {
    set(XDSCreationTime,get(StudyDate),get(StudyTime))
} else {
    set(XDSCreationTime, new java.util.Date())
}

set(XDSLanguage, 'English')
set(XDSServiceStartTime, get(StudyDate),get(StudyTime))

// these codes are environment specific.
set(XDSTypeCode, code('Test Type Code', 'Test Scheme', 'Test Type Code'))

set(XDSClassCode,
    code('DICOM Study Manifest', 'Karos Health demo classCodes', 'DICOM Study Manifest'))

set(XDSHealthcareFacilityTypeCode,
    code('Hospital Unit', 'Karos Health demo healthcareFacilityTypeCodes', 'Hospital Unit'))

set(XDSPracticeSettingCode,
    code('Test Practice Setting Code', 'Test Scheme', 'Test Practice Setting Code'))

set(XDSFormatCode,
    code('DICOM', 'Karos Health demo formatCodes', 'DICOM'))

set(XDSConfidentialityCode,
    code('N', 'Karos Health demo confidentialityCodes', 'Normal'))

set(XDSEventCode, code('Test', 'Test', 'Test'))

def localPID = get(PatientID)
if(null == localPID) {
    log.warn("XDS Metadata creation, manifest submission:\nLocal Patient ID was NULL.  Manifest submission will likely fail!!")
}

def issuerOfLocalPID = null
def (namespace, domain, type) = get(IssuerOfPatientID).tokenize("&")
if (null == domain) {
    issuerOfLocalPID = get("IssuerOfPatientIDQualifiersSequence/UniversalEntityID")
} else {
    issuerOfLocalPID = domain
}

// Sanity Check
if(null == issuerOfLocalPID) {
    log.warn("XDS Metadata creation, manifest submission:\nLocal Domain was NULL.  Manifest submission will likely fail!!")
}

def qualifiedLocalPID = localPID + issuerOfLocalPID
// Sanity Check
if(null == qualifiedLocalPID) {
    log.warn("XDS Metadata creation, manifest submission:\nLocal Patient ID and Issuer were *both*  NULL.  Manifest submission will likely fail!!")
}

def referringPhysicianName = get(tag(0x0008,0x0090))
// Sanity Check
if(null == referringPhysicianName) {
    log.warn("XDS Metadata creation, manifest submission:\nReferring Physician Name is NULL. Not including referring physician name in manifest")
} else {
    set(XDSExtendedMetadata('referringPhysician'), referringPhysicianName)
}

log.debug("document_metadata_morpher.groovy: Debugging XDS Metadata creation, manifest submission: \nLocal Patient ID is \"{}\", and its Issuer is: \"{}\"", localPID, issuerOfLocalPID)
set(XDSSourcePatientID, localPID, issuerOfLocalPID)

//set(XDSTitle, get(StudyDescription))
def studyDescription = get(StudyDescription)
// Sanity Check
if(null == studyDescription) {
    log.debug("XDS Metadata creation, manifest submission:\nStudy Description is NULL. Setting XDSTitle to default DICOM Study")
    set(XDSTitle, 'DICOM Study')
} else {
    set(XDSTitle, studyDescription)
}

set(XDSAuthorGivenName, ' ')

log.debug("document_metadata_morpher.groovy: End morpher")
