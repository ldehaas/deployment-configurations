def setAuthorInfo = { ->
  def referringPhysicianName = get(tag(0x0008,0x0090))
  if (referringPhysicianName != null) {
      def referringPhysicianNameParts = referringPhysicianName.tokenize("^")
      if (referringPhysicianNameParts.size() >= 2) {
        set(XDSAuthorFamilyName, referringPhysicianNameParts[0])
        set(XDSAuthorGivenName, referringPhysicianNameParts[1])
      }
  }
  def authorId = get("ReferringPhysicianIdentificationSequence/PersonIdentificationCodeSequence/CodeValue")

  if (authorId != null) {
      set(XDSAuthorID, authorId)
  } else {
      set(XDSAuthorID, "NA")
  }
}

def getModalityDescription = { modality ->
    def modalityMap = [
        "AR":"Autorefraction",
        "AU":"Audio",
        "BDUS":"Bone Densitometry (ultrasound)",
        "BI":"Biomagnetic imaging",
        "BMD":"Bone Densitometry  (X-Ray)",
        "CR":"Computed Radiography",
        "CT":"Computed Tomography",
        "DG":"Diaphanography",
        "DOC":"Document",
        "DX":"Digital Radiography",
        "ECG":"Electrocardiography",
        "EPS":"Cardiac Electrophysiology",
        "ES":"Endoscopy",
        "FID":"Fiducials",
        "GM":"General Microscopy",
        "HC":"Hard Copy",
        "HD":"Hemodynamic Waveform",
        "IO":"Intra-oral Radiography",
        "IVUS":"Intravascular Ultrasound",
        "KER":"Keratometry",
        "KO":"Key Object Selection",
        "LEN":"Lensometry",
        "LS":"Laser surface scan",
        "MG":"Mammography",
        "MR":"Magnetic Resonance",
        "NM":"Nuclear Medicine",
        "OAM":"Ophthalmic Axial Measurements",
        "OCT":"Optical Coherence Tomography",
        "OP":"Ophthalmic Photography",
        "OPM":"Ophthalmic Mapping",
        "OPR":"Ophthalmic Refraction",
        "OPT":"Ophthalmic Tomography",
        "OPV":"Ophthalmic Visual Field",
        "OT":"Other",
        "PS":"Presentation State",
        "PT":"Positron emission tomography",
        "PX":"Panoramic X-Ray",
        "RF":"Radiofluoroscopy",
        "RG":"Radiographic imaging",
        "RTIMAGE":"Radiotherapy Image",
        "REG":"Registration",
        "RESP":"Respiratory Waveform",
        "RTDOSE":" Radiotherapy Dose",
        "RTPLAN":" Radiotherapy Plan",
        "RTRECORD":"RT Treatment Record",
        "RTSTRUCT":"Radiotherapy Structure Set",
        "SC":"Secondary Capture",
        "SEG":"Segmentation",
        "SM":"Slide Microscopy",
        "SMR":"Stereometric Relationship",
        "SR":"SR Document",
        "SRF":"Subjective Refraction",
        "TG":"Thermography",
        "US":"Ultrasound",
        "VA":"Visual Acuity",
        "XA":"X-Ray Angiography",
        "XC":"External-camera Photography",
        "NONE":"NONE"
    ].withDefault{"Other"}

    return modalityMap[modality]
}

def setEventCodes = { ->
  def xdsEventCodes = []

  def modalitiesInStudy = getList(ModalitiesInStudy) as Set
  if (modalitiesInStudy != null) {
      log.debug("Found ModalitiesInStudy: {}", modalitiesInStudy)
      for (modality in modalitiesInStudy) {
          if (!"\"\"".equals(modality)) {
              log.debug("Adding modality: {}", modality)
              xdsEventCodes += code(modality, "DCM", getModalityDescription(modality))
          }
      }
  }

  def anatomicRegion = get("AnatomicRegionSequence/CodeValue")
  if (anatomicRegion != null) {
      log.debug("Found anaomic region {}", anatomicRegion)
      xdsEventCodes += code(anatomicRegion, 'Imagerie Qu\u00e9bec-DSQ')
  }

  if (xdsEventCodes.size() > 0) {
      log.debug("Seting XDS Event Codes to {}", xdsEventCodes)
      set(XDSEventCode, xdsEventCodes)
  } 
}

def setSourcePatient = { ->

  def localPID = get(PatientID)
  def issuerOfLocalPID = get(IssuerOfPatientID)

  log.debug("Source patient ID is {}, and its Issuer is: {}", localPID, issuerOfLocalPID)
  set(XDSSourcePatientID, localPID, issuerOfLocalPID)

  def patientName = get(PatientName);
  if (patientName != null) {
      def patientNameParts = patientName.tokenize("^")
      if (patientNameParts.size() >= 2) {
        set(XDSSourcePatientFamilyName, patientNameParts[0])
        set(XDSSourcePatientGivenName, patientNameParts[1])
      }
  }

  set(XDSSourcePatientSex, get(PatientSex))
  set(XDSSourcePatientBirthDate, get(PatientBirthDate))
}

set(XDSExtendedMetadata('accessionNumberList'), get('AccessionNumber'))

setAuthorInfo()

set(XDSClassCode, code('examen imagerie', 'Imagerie Qu\u00e9bec-DSQ', 'examen imagerie'))

set(XDSConfidentialityCode, code('N', 'Karos', 'Normal'))

set(XDSCreationTime,get(StudyDate),get(StudyTime))

setEventCodes()
set(XDSFormatCode, code('urn:ihe:rad:1.2.840.10008.5.1.4.1.1.88.59', '1.2.840.10008.2.6.1', 'urn:ihe:rad:1.2.840.10008.5.1.4.1.1.88.59'))
set(XDSHealthcareFacilityTypeCode, code('Hospital Unit', 'Karos Health healthcareFacilityTypeCodes', 'Hospital Unit'))
   
set(XDSPracticeSettingCode, code('Radiologie', 'Imagerie Qu\u00e9bec-DSQ', 'Radiologie'))
    
set(XDSServiceStartTime, get(StudyDate), get(StudyTime))

setSourcePatient()

def studyDescription = get(StudyDescription)
set(XDSTitle, studyDescription)
set(XDSTypeCode, code(get("RequestedProcedureCodeSequence/CodeValue"), 'Imagerie Qu\u00e9bec-DSQ', studyDescription))

