log.info("HL7Proxy Inbound Morpher - START")

def messageType = get('/.MSH-9-1')
def triggerEvent = get('/.MSH-9-2')

log.info("messageType is: {}", messageType)
log.info("triggerEvent is: {}", triggerEvent)

if (messageType == null || !"ADT".equals(messageType)) {
    log.warn("HL7Proxy Inbound Morpher - Will not proxy because the HL7 message is of type {}", messageType)
    log.info("HL7Proxy Inbound Morpher - END")
    return false
}

set('MSH-3', 'CAMEL');
set('MSH-4', 'REGIONH');
set('MSH-12', '2.3.1');
set('PID-3-4','CRD');

if ("ADT".equals(messageType) && ("A40".equals(triggerEvent))) {
    set('MRG-1-4', "CRD")
}

def patientIds = null;
try {
    patientIds = getList('/.PID-3(*)-1')
} catch (Exception e) {
    // do nothing!
}

for (int i = 0; i < patientIds.size(); i++) {
    def currNamespace = get('/.PID-3(' + i + ')-4-1')
    def currUniversalId = get('/.PID-3(' + i + ')-4-2')
    if (!"CRD".equalsIgnoreCase(currNamespace) &&
        !"2.16.124.113638.6.1.1.1".equalsIgnoreCase(currUniversalId)) {

        // Remove MRN
        set('/.PID-3(' + i + ')-1', '')
        set('/.PID-3(' + i + ')-2', '')
        set('/.PID-3(' + i + ')-3', '')
        set('/.PID-3(' + i + ')-4-1', '')
        set('/.PID-3(' + i + ')-4-2', '')
        set('/.PID-3(' + i + ')-4-3', '')
        set('/.PID-3(' + i + ')-5', '')
    }
}

log.info("HL7Proxy Inbound Morpher - END")
