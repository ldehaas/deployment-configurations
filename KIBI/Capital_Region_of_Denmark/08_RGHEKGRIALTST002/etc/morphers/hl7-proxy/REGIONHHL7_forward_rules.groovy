log.info("Start REGIONHHL7 Update Morpher")

def orderIssuer = get('/.OBR-2-2')
def result = true

if (orderIssuer == 'EPC') {
    log.info("Dropping EPIC SCN as not needed for REGIONH proxy destination")
    result = false
}
log.info("End REGIONHHL7 Update Morpher")
return result
