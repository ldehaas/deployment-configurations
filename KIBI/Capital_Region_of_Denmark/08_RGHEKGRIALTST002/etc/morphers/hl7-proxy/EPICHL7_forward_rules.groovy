log.info("Start EPICHL7 Update Morpher")

def orderIssuer = get('/.OBR-2-2')
def result = true

if (orderIssuer != 'EPC') {
    log.info("OrderIssuer is not EPC so not needed for EPICHL7 proxy destination")
    result = false
}
log.info("End EPICHL7 Update Morpher")
return result
