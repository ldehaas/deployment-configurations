<tests service="xds2scn"
       script="Morpher"
       file="xds2scn.groovy">
  <test name="dropNonEkg">
    <inputs>
      <ohtMetadata name="input">
        <tns:DocumentEntry xmlns:tns="urn:org:openhealthtools:ihe:xds:metadata">
            <entryUUID>nonecg</entryUUID>
            <formatCode>
                <code>xyz</code>
                <schemeName>abc</schemeName>
            </formatCode>
            <mimeType>application/octet-stream</mimeType>
        </tns:DocumentEntry>
      </ohtMetadata>
    </inputs>
    <assertions>
        assert returned == false
    </assertions>
  </test>

  <!-- no related documents -->
  <test name="dropJustEkg">
    <inputs>
      <ohtMetadata name="input">
        <tns:DocumentEntry xmlns:tns="urn:org:openhealthtools:ihe:xds:metadata">
            <entryUUID>aecg</entryUUID>
            <formatCode>
                <code>93010</code>
                <schemeName>CPT-4</schemeName>
            </formatCode>
            <mimeType>application/xml</mimeType>
        </tns:DocumentEntry>
      </ohtMetadata>
    </inputs>
    <assertions>
        assert returned == false
    </assertions>
  </test>

  <!-- Both pdf and aecg are present, but the current update is for a different
       document so we don't actually want to trigger a new notification. -->
  <test name="dropUnrelatedUpdate">
    <inputs>
      <ohtMetadata name="input">
        <tns:DocumentEntry xmlns:tns="urn:org:openhealthtools:ihe:xds:metadata">
            <entryUUID>nonecg</entryUUID>
            <formatCode>
                <code>xyz</code>
                <schemeName>abc</schemeName>
            </formatCode>
            <mimeType>application/xml</mimeType>
        </tns:DocumentEntry>
      </ohtMetadata>
      <list name="relatedDocuments">
        <ohtMetadata>
          <tns:DocumentEntry xmlns:tns="urn:org:openhealthtools:ihe:xds:metadata">
            <entryUUID>pdf</entryUUID>
            <formatCode>
              <code>93010</code>
              <schemeName>CPT-4</schemeName>
            </formatCode>
            <mimeType>application/pdf</mimeType>
          </tns:DocumentEntry>
        </ohtMetadata>
        <ohtMetadata>
          <tns:DocumentEntry xmlns:tns="urn:org:openhealthtools:ihe:xds:metadata">
            <entryUUID>aecg</entryUUID>
            <formatCode>
              <code>93010</code>
              <schemeName>CPT-4</schemeName>
            </formatCode>
            <mimeType>application/xml</mimeType>
          </tns:DocumentEntry>
        </ohtMetadata>
      </list>
    </inputs>
    <assertions>
        assert returned == false
    </assertions>
  </test>

  <test name="basic">
    <inputs>
      <ohtMetadata name="input">
        <tns:DocumentEntry xmlns:tns="urn:org:openhealthtools:ihe:xds:metadata">
            <entryUUID>aecg</entryUUID>
            <formatCode>
                <code>93010</code>
                <schemeName>CPT-4</schemeName>
            </formatCode>
            <patientId>
                <idNumber>affinitypid</idNumber>
                <assigningAuthorityUniversalId>affinity</assigningAuthorityUniversalId>
            </patientId>
            <sourcePatientId>
                <idNumber>srcpid</idNumber>
                <assigningAuthorityUniversalId>local</assigningAuthorityUniversalId>
            </sourcePatientId>
            <sourcePatientInfo>
                <patientName>
                    <familyName>familyName</familyName>
                    <givenName>givenName</givenName>
                </patientName>
                <patientDateOfBirth>19821213</patientDateOfBirth>
                <patientSex>M</patientSex>
            </sourcePatientInfo>
            <serviceStartTime>201601051753</serviceStartTime>
            <serviceStopTime>201601051759</serviceStopTime>
            <extension>
                <name>departmentSection</name>
                <value>depsec</value>
            </extension>
            <extension>
                <name>urn:ihe:iti:xds:2013:referenceIdList</name>
                <value>accn^^^&amp;domain&amp;ISO^urn:ihe:iti:xds:2013:accession</value>
            </extension>
            <mimeType>application/xml</mimeType>
        </tns:DocumentEntry>
      </ohtMetadata>
      <list name="relatedDocuments">
        <ohtMetadata>
          <tns:DocumentEntry xmlns:tns="urn:org:openhealthtools:ihe:xds:metadata">
            <entryUUID>pdf</entryUUID>
            <formatCode>
              <code>93010</code>
              <schemeName>CPT-4</schemeName>
            </formatCode>
            <mimeType>application/pdf</mimeType>
          </tns:DocumentEntry>
        </ohtMetadata>
      </list>
    </inputs>
    <assertions>
        assert returned != false

        assert output.get("PID-3") == "srcpid"
        assert output.get("PID-3-4-1") == null
        // Unrecognized assigning authority.  Leave it in
        // unchanged as a safety precaution.
        assert output.get("PID-3-4-2") == "local"
        assert output.get("PID-3-4-3") == "ISO"
        assert output.get("PID-3-5-1") == null

        assert output.get("PID-5-1") == "familyName"
        assert output.get("PID-5-2") == "givenName"
        assert output.get("PID-7") == "19821213"
        assert output.get("PID-8") == "M"

        assert output.get("PV1-3-1") == "depsec"
        assert output.get("PV1-3-4") == "depsec"

        assert output.get("ORC-3") == "accn"
        assert output.get("OBR-3") == "accn"

        assert output.get("OBR-4") == "DNK05219"

        import org.joda.time.*
        def startTime = new DateTime(2016, 1, 5, 17, 53, DateTimeZone.UTC)
        startTime = startTime.withZone(DateTimeZone.getDefault())
        def stopTime = startTime.plusMinutes(6)

        import com.karos.rtk.common.*
        assert output.get("ORC-7-4") == HL7v2Date.format(startTime)
        assert output.get("ORC-7-5") == HL7v2Date.format(stopTime)
        assert output.get("OBR-27-4") == output.get("ORC-7-4")
        assert output.get("OBR-27-5") == output.get("ORC-7-5")

        assert output.get("OBR-21") == "J"
        assert output.get("OBR-25") == "P"

        // for prod, change this to P:
        assert output.get("MSH-11") == "T"

        assert output.get("ZDS-1") == "aecg"
    </assertions>
  </test>

  <test name="assigningAuthorityNamespaceUnknown">
    <inputs>
      <ohtMetadata name="input">
        <tns:DocumentEntry xmlns:tns="urn:org:openhealthtools:ihe:xds:metadata">
            <entryUUID>aecg</entryUUID>
            <formatCode>
                <code>93010</code>
                <schemeName>CPT-4</schemeName>
            </formatCode>
            <patientId>
                <idNumber>affinitypid</idNumber>
                <assigningAuthorityUniversalId>affinity</assigningAuthorityUniversalId>
            </patientId>
            <sourcePatientId>
                <idNumber>short</idNumber>
                <assigningAuthorityUniversalId>2.16.124.113638.6.1.1.1</assigningAuthorityUniversalId>
            </sourcePatientId>
            <extension>
                <name>departmentSection</name>
                <value>depsec</value>
            </extension>
            <mimeType>application/xml</mimeType>
        </tns:DocumentEntry>
      </ohtMetadata>
      <list name="relatedDocuments">
        <ohtMetadata>
          <tns:DocumentEntry xmlns:tns="urn:org:openhealthtools:ihe:xds:metadata">
            <entryUUID>pdf</entryUUID>
            <formatCode>
              <code>93010</code>
              <schemeName>CPT-4</schemeName>
            </formatCode>
            <mimeType>application/pdf</mimeType>
          </tns:DocumentEntry>
        </ohtMetadata>
      </list>
    </inputs>
    <assertions>
        assert returned != false

        assert output.get("PID-3") == "short"
        // CPR is the default namespace for this OID.
        assert output.get("PID-3-4-1") == "CPR"
        assert output.get("PID-3-4-2") == null
        assert output.get("PID-3-4-3") == null
        assert output.get("PID-3-5-1") == "CPR"
    </assertions>
  </test>

  <test name="assigningAuthorityNamespaceECPR">
    <inputs>
      <ohtMetadata name="input">
        <tns:DocumentEntry xmlns:tns="urn:org:openhealthtools:ihe:xds:metadata">
            <entryUUID>aecg</entryUUID>
            <formatCode>
                <code>93010</code>
                <schemeName>CPT-4</schemeName>
            </formatCode>
            <patientId>
                <idNumber>affinitypid</idNumber>
                <assigningAuthorityUniversalId>affinity</assigningAuthorityUniversalId>
            </patientId>
            <sourcePatientId>
                <idNumber>1234567AA</idNumber>
                <assigningAuthorityUniversalId>2.16.124.113638.6.1.1.1</assigningAuthorityUniversalId>
            </sourcePatientId>
            <extension>
                <name>departmentSection</name>
                <value>depsec</value>
            </extension>
            <mimeType>application/xml</mimeType>
        </tns:DocumentEntry>
      </ohtMetadata>
      <list name="relatedDocuments">
        <ohtMetadata>
          <tns:DocumentEntry xmlns:tns="urn:org:openhealthtools:ihe:xds:metadata">
            <entryUUID>pdf</entryUUID>
            <formatCode>
              <code>93010</code>
              <schemeName>CPT-4</schemeName>
            </formatCode>
            <mimeType>application/pdf</mimeType>
          </tns:DocumentEntry>
        </ohtMetadata>
      </list>
    </inputs>
    <assertions>
        assert returned != false

        assert output.get("PID-3") == "1234567AA"
        // ECPR is the namespace when the 8th and 9th characters are alphabetic:
        assert output.get("PID-3-4-1") == "ECPR"
        assert output.get("PID-3-4-2") == null
        assert output.get("PID-3-4-3") == null
        assert output.get("PID-3-5-1") == "ECPR"
    </assertions>
  </test>

  <test name="assigningAuthorityNamespaceCPR">
    <inputs>
      <ohtMetadata name="input">
        <tns:DocumentEntry xmlns:tns="urn:org:openhealthtools:ihe:xds:metadata">
            <entryUUID>aecg</entryUUID>
            <formatCode>
                <code>93010</code>
                <schemeName>CPT-4</schemeName>
            </formatCode>
            <patientId>
                <idNumber>affinitypid</idNumber>
                <assigningAuthorityUniversalId>affinity</assigningAuthorityUniversalId>
            </patientId>
            <sourcePatientId>
                <idNumber>123456799</idNumber>
                <assigningAuthorityUniversalId>2.16.124.113638.6.1.1.1</assigningAuthorityUniversalId>
            </sourcePatientId>
            <extension>
                <name>departmentSection</name>
                <value>depsec</value>
            </extension>
            <mimeType>application/xml</mimeType>
        </tns:DocumentEntry>
      </ohtMetadata>
      <list name="relatedDocuments">
        <ohtMetadata>
          <tns:DocumentEntry xmlns:tns="urn:org:openhealthtools:ihe:xds:metadata">
            <entryUUID>pdf</entryUUID>
            <formatCode>
              <code>93010</code>
              <schemeName>CPT-4</schemeName>
            </formatCode>
            <mimeType>application/pdf</mimeType>
          </tns:DocumentEntry>
        </ohtMetadata>
      </list>
    </inputs>
    <assertions>
        assert returned != false

        assert output.get("PID-3") == "123456799"
        // The normal case for the CPR namespace is with this oid and the
        // 8th and 9th characters of the pid being numeric:
        assert output.get("PID-3-4-1") == "CPR"
        assert output.get("PID-3-4-2") == null
        assert output.get("PID-3-4-3") == null
        assert output.get("PID-3-5-1") == "CPR"
    </assertions>
  </test>

  <test name="departmentSectionSKS">
    <inputs>
      <ohtMetadata name="input">
        <tns:DocumentEntry xmlns:tns="urn:org:openhealthtools:ihe:xds:metadata">
            <entryUUID>aecg</entryUUID>
            <formatCode>
                <code>93010</code>
                <schemeName>CPT-4</schemeName>
            </formatCode>
            <patientId>
                <idNumber>affinitypid</idNumber>
                <assigningAuthorityUniversalId>affinity</assigningAuthorityUniversalId>
            </patientId>
            <sourcePatientId>
                <idNumber>srcpid</idNumber>
                <assigningAuthorityUniversalId>local</assigningAuthorityUniversalId>
            </sourcePatientId>
            <extension>
                <name>departmentSection</name>
                <value>130101</value>
            </extension>
            <mimeType>application/xml</mimeType>
        </tns:DocumentEntry>
      </ohtMetadata>
      <list name="relatedDocuments">
        <ohtMetadata>
          <tns:DocumentEntry xmlns:tns="urn:org:openhealthtools:ihe:xds:metadata">
            <entryUUID>pdf</entryUUID>
            <formatCode>
              <code>93010</code>
              <schemeName>CPT-4</schemeName>
            </formatCode>
            <mimeType>application/pdf</mimeType>
          </tns:DocumentEntry>
        </ohtMetadata>
      </list>
    </inputs>
    <assertions>
        assert returned != false

        assert output.get("PV1-3-1-1") == "130101"
        assert output.get("PV1-3-1-2") == "SKS"
        assert output.get("PV1-3-4-1") == "130101"
        assert output.get("PV1-3-4-2") == "SKS"
    </assertions>
  </test>

  <test name="departmentSectionSOR">
    <inputs>
      <ohtMetadata name="input">
        <tns:DocumentEntry xmlns:tns="urn:org:openhealthtools:ihe:xds:metadata">
            <entryUUID>aecg</entryUUID>
            <formatCode>
                <code>93010</code>
                <schemeName>CPT-4</schemeName>
            </formatCode>
            <patientId>
                <idNumber>affinitypid</idNumber>
                <assigningAuthorityUniversalId>affinity</assigningAuthorityUniversalId>
            </patientId>
            <sourcePatientId>
                <idNumber>srcpid</idNumber>
                <assigningAuthorityUniversalId>local</assigningAuthorityUniversalId>
            </sourcePatientId>
            <extension>
                <name>departmentSection</name>
                <value>386851000016007</value>
            </extension>
            <mimeType>application/xml</mimeType>
        </tns:DocumentEntry>
      </ohtMetadata>
      <list name="relatedDocuments">
        <ohtMetadata>
          <tns:DocumentEntry xmlns:tns="urn:org:openhealthtools:ihe:xds:metadata">
            <entryUUID>pdf</entryUUID>
            <formatCode>
              <code>93010</code>
              <schemeName>CPT-4</schemeName>
            </formatCode>
            <mimeType>application/pdf</mimeType>
          </tns:DocumentEntry>
        </ohtMetadata>
      </list>
    </inputs>
    <assertions>
        assert returned != false

        assert output.get("PV1-3-1-1") == "386851000016007"
        assert output.get("PV1-3-1-2") == "SOR"
        assert output.get("PV1-3-4-1") == "386851000016007"
        assert output.get("PV1-3-4-2") == "SOR"
    </assertions>
  </test>

  <test name="copyPdf">
    <inputs>
      <ohtMetadata name="input">
        <tns:DocumentEntry xmlns:tns="urn:org:openhealthtools:ihe:xds:metadata">
            <entryUUID>pdf</entryUUID>
            <formatCode>
                <code>93010</code>
                <schemeName>CPT-4</schemeName>
            </formatCode>
            <mimeType>application/pdf</mimeType>
        </tns:DocumentEntry>
        <!-- NB: rialto normalizes this wrapped content to having no spaces or breaks: -->
        <content>
            JVBERi0xLjEKJcKlwrHDqwoKMSAwIG9iagogIDw8IC9UeXBlIC9DYXRhbG9nCiAgICAgL1B
            hZ2VzIDIgMCBSCiAgPj4KZW5kb2JqCgoyIDAgb2JqCiAgPDwgL1R5cGUgL1BhZ2VzCiAgIC
            AgL0tpZHMgWzMgMCBSXQogICAgIC9Db3VudCAxCiAgICAgL01lZGlhQm94IFswIDAgMzAwI
            DE0NF0KICA+PgplbmRvYmoKCjMgMCBvYmoKICA8PCAgL1R5cGUgL1BhZ2UKICAgICAgL1Bh
            cmVudCAyIDAgUgogICAgICAvUmVzb3VyY2VzCiAgICAgICA8PCAvRm9udAogICAgICAgICA
            gIDw8IC9GMQogICAgICAgICAgICAgICA8PCAvVHlwZSAvRm9udAogICAgICAgICAgICAgIC
            AgICAvU3VidHlwZSAvVHlwZTEKICAgICAgICAgICAgICAgICAgL0Jhc2VGb250IC9UaW1lc
            y1Sb21hbgogICAgICAgICAgICAgICA+PgogICAgICAgICAgID4+CiAgICAgICA+PgogICAg
            ICAvQ29udGVudHMgNCAwIFIKICA+PgplbmRvYmoKCjQgMCBvYmoKICA8PCAvTGVuZ3RoIDU
            1ID4+CnN0cmVhbQogIEJUCiAgICAvRjEgMTggVGYKICAgIDAgMCBUZAogICAgKEhlbGxvIF
            dvcmxkKSBUagogIEVUCmVuZHN0cmVhbQplbmRvYmoKCnhyZWYKMCA1CjAwMDAwMDAwMDAgN
            jU1MzUgZiAKMDAwMDAwMDAxOCAwMDAwMCBuIAowMDAwMDAwMDc3IDAwMDAwIG4gCjAwMDAw
            MDAxNzggMDAwMDAgbiAKMDAwMDAwMDQ1NyAwMDAwMCBuIAp0cmFpbGVyCiAgPDwgIC9Sb29
            0IDEgMCBSCiAgICAgIC9TaXplIDUKICA+PgpzdGFydHhyZWYKNTY1CiUlRU9GCg==
        </content>
      </ohtMetadata>
      <list name="relatedDocuments">
        <ohtMetadata>
          <tns:DocumentEntry xmlns:tns="urn:org:openhealthtools:ihe:xds:metadata">
            <entryUUID>aecg</entryUUID>
            <formatCode>
              <code>93010</code>
              <schemeName>CPT-4</schemeName>
            </formatCode>
            <mimeType>application/xml</mimeType>
            <patientId>
                <idNumber>affinitypid</idNumber>
                <assigningAuthorityUniversalId>affinity</assigningAuthorityUniversalId>
            </patientId>
            <sourcePatientId>
                <idNumber>srcpid</idNumber>
                <assigningAuthorityUniversalId>local</assigningAuthorityUniversalId>
            </sourcePatientId>
          </tns:DocumentEntry>
          <content>YmFpIQ==</content>
        </ohtMetadata>
      </list>
    </inputs>
    <assertions>
        assert returned != false

        def obx = "/.OBSERVATION(0)/OBX"
        assert output.get("$obx-2") == "ED"
        assert output.get("$obx-3") == "EKG"
        assert output.get("$obx-5-1") == "KIBI"
        assert output.get("$obx-5-2") == "application"
        assert output.get("$obx-5-3") == "pdf"
        assert output.get("$obx-5-4") == "Base64"

        def b64pdf = output.get("$obx-5-5")
        assert b64pdf ==~ /^[a-zA-Z0-9+]+=*$/
        import org.apache.commons.codec.binary.*
        // hax: just making sure this really looks like a pdf:
        assert new String(Base64.decodeBase64(b64pdf)).startsWith("%PDF-1.")
    </assertions>
  </test>

  <test name="pdfContentNotAvailable">
    <inputs>
      <ohtMetadata name="input">
        <tns:DocumentEntry xmlns:tns="urn:org:openhealthtools:ihe:xds:metadata">
            <entryUUID>pdf</entryUUID>
            <formatCode>
                <code>93010</code>
                <schemeName>CPT-4</schemeName>
            </formatCode>
            <mimeType>application/pdf</mimeType>
        </tns:DocumentEntry>
      </ohtMetadata>
      <list name="relatedDocuments">
        <ohtMetadata>
          <tns:DocumentEntry xmlns:tns="urn:org:openhealthtools:ihe:xds:metadata">
            <entryUUID>aecg</entryUUID>
            <formatCode>
              <code>93010</code>
              <schemeName>CPT-4</schemeName>
            </formatCode>
            <mimeType>application/xml</mimeType>
            <patientId>
                <idNumber>affinitypid</idNumber>
                <assigningAuthorityUniversalId>affinity</assigningAuthorityUniversalId>
            </patientId>
            <sourcePatientId>
                <idNumber>srcpid</idNumber>
                <assigningAuthorityUniversalId>local</assigningAuthorityUniversalId>
            </sourcePatientId>
          </tns:DocumentEntry>
          <content>YmFpIQ==</content>
        </ohtMetadata>
      </list>
    </inputs>
    <assertions>
        assert returned != false

        // we can't really prove that no OBX segments were added, and this actually
        // creates a new one if there were none, but we should be able to see that
        // OBX-1 wasn't populated:
        def obx = "/.OBSERVATION(0)/OBX"
        assert output.get("$obx-1") == null
    </assertions>
  </test>

  <test name="commentsAndApproval">
    <inputs>
      <ohtMetadata name="input">
        <tns:DocumentEntry xmlns:tns="urn:org:openhealthtools:ihe:xds:metadata">
            <entryUUID>aecg</entryUUID>
            <formatCode>
                <code>93010</code>
                <schemeName>CPT-4</schemeName>
            </formatCode>
            <mimeType>application/xml</mimeType>
            <patientId>
                <idNumber>affinitypid</idNumber>
                <assigningAuthorityUniversalId>affinity</assigningAuthorityUniversalId>
            </patientId>
            <sourcePatientId>
                <idNumber>srcpid</idNumber>
                <assigningAuthorityUniversalId>local</assigningAuthorityUniversalId>
            </sourcePatientId>
            <extension>
                <name>Viewer_Comments</name>
                <value>[kard0007 13-01-2016 14:45:16 Comment] Test/Patrick</value>
            </extension>
            <extension>
                <name>Viewer_Approved</name>
                <value>[kard0007 13-01-2016 14:46:58 Approval] ECG approved by kard0007 – Test3/Patrick</value>
            </extension>
        </tns:DocumentEntry>
        <content>YmFpIQ==</content>
      </ohtMetadata>
      <list name="relatedDocuments">
        <ohtMetadata>
          <tns:DocumentEntry xmlns:tns="urn:org:openhealthtools:ihe:xds:metadata">
            <entryUUID>pdf</entryUUID>
            <formatCode>
              <code>93010</code>
              <schemeName>CPT-4</schemeName>
            </formatCode>
            <mimeType>application/pdf</mimeType>
          </tns:DocumentEntry>
          <content>aGFpIQ==</content>
        </ohtMetadata>
      </list>
    </inputs>
    <assertions>
        assert returned != false

        // the order of the comments, approval and pdf are probably not important
        // but here we're counting on the order that the morpher happens to put them:
        def obx = "/.OBSERVATION(0)/OBX"
        assert output.get("$obx-1") == "1"
        assert output.get("$obx-2") == "ST"
        assert output.get("$obx-3-1") == "Viewer_Comments"
        assert output.get("$obx-3-2") == "Comments from Kardia Viewer"
        assert output.get("$obx-5") == "[kard0007 13-01-2016 14:45:16 Comment] Test/Patrick"

        obx = "/.OBSERVATION(1)/OBX"
        assert output.get("$obx-1") == "2"
        assert output.get("$obx-2") == "ST"
        assert output.get("$obx-3-1") == "Viewer_Approved"
        assert output.get("$obx-3-2") == "Approved in Kardia Viewer"
        assert output.get("$obx-5") == "[kard0007 13-01-2016 14:46:58 Approval] " +
                "ECG approved by kard0007 – Test3/Patrick"

        // just making sure the pdf is still on the end:
        obx = "/.OBSERVATION(2)/OBX"
        assert output.get("$obx-1") == "3"
        assert output.get("$obx-2") == "ED"
    </assertions>
  </test>

  <test name="annotations">
    <inputs>
      <ohtMetadata name="input">
        <tns:DocumentEntry xmlns:tns="urn:org:openhealthtools:ihe:xds:metadata">
            <entryUUID>aecg</entryUUID>
            <formatCode>
                <code>93010</code>
                <schemeName>CPT-4</schemeName>
            </formatCode>
            <mimeType>application/xml</mimeType>
            <patientId>
                <idNumber>affinitypid</idNumber>
                <assigningAuthorityUniversalId>affinity</assigningAuthorityUniversalId>
            </patientId>
            <sourcePatientId>
                <idNumber>srcpid</idNumber>
                <assigningAuthorityUniversalId>local</assigningAuthorityUniversalId>
            </sourcePatientId>
            <extension>
                <name>Annotations</name>
                <!-- Note the trailing carriage return: we need to be able to work around at least
                     a little bit of additional formatting. -->
                <value>OBX|1|NM|HR^Heart rate||93|/min&#13;OBX|2|NM|PR^PR interval||134|ms&#13;</value>
            </extension>
        </tns:DocumentEntry>
      </ohtMetadata>
      <list name="relatedDocuments">
        <ohtMetadata>
          <tns:DocumentEntry xmlns:tns="urn:org:openhealthtools:ihe:xds:metadata">
            <entryUUID>pdf</entryUUID>
            <formatCode>
              <code>93010</code>
              <schemeName>CPT-4</schemeName>
            </formatCode>
            <mimeType>application/pdf</mimeType>
          </tns:DocumentEntry>
          <content>aGFpIQ==</content>
        </ohtMetadata>
      </list>
    </inputs>
    <assertions>
        assert returned != false

        def obx = "/.OBSERVATION(0)/OBX"
        assert output.get("$obx-1") == "1"
        assert output.get("$obx-2") == "NM"
        assert output.get("$obx-3-1") == "HR"
        assert output.get("$obx-3-2") == "Heart rate"
        assert output.get("$obx-5") == "93"
        assert output.get("$obx-6") == "/min"

        obx = "/.OBSERVATION(1)/OBX"
        assert output.get("$obx-1") == "2"
        assert output.get("$obx-2") == "NM"
        assert output.get("$obx-3-1") == "PR"
        assert output.get("$obx-3-2") == "PR interval"
        assert output.get("$obx-5") == "134"
        assert output.get("$obx-6") == "ms"

        // just making sure the pdf is still on the end:
        obx = "/.OBSERVATION(2)/OBX"
        assert output.get("$obx-1") == "3"
        assert output.get("$obx-2") == "ED"
    </assertions>
  </test>

  <test name="malformedAnnotations">
    <inputs>
      <ohtMetadata name="input">
        <tns:DocumentEntry xmlns:tns="urn:org:openhealthtools:ihe:xds:metadata">
            <entryUUID>aecg</entryUUID>
            <formatCode>
                <code>93010</code>
                <schemeName>CPT-4</schemeName>
            </formatCode>
            <mimeType>application/xml</mimeType>
            <patientId>
                <idNumber>affinitypid</idNumber>
                <assigningAuthorityUniversalId>affinity</assigningAuthorityUniversalId>
            </patientId>
            <sourcePatientId>
                <idNumber>srcpid</idNumber>
                <assigningAuthorityUniversalId>local</assigningAuthorityUniversalId>
            </sourcePatientId>
            <extension>
                <name>Annotations</name>
                <value>MSHXXX|1|NM|HR^Heart rate||93|/min</value>
            </extension>
        </tns:DocumentEntry>
      </ohtMetadata>
      <list name="relatedDocuments">
        <ohtMetadata>
          <tns:DocumentEntry xmlns:tns="urn:org:openhealthtools:ihe:xds:metadata">
            <entryUUID>pdf</entryUUID>
            <formatCode>
              <code>93010</code>
              <schemeName>CPT-4</schemeName>
            </formatCode>
            <mimeType>application/pdf</mimeType>
          </tns:DocumentEntry>
          <content>aGFpIQ==</content>
        </ohtMetadata>
      </list>
    </inputs>
    <assertions>
        assert returned != false

        // Annotations should be skipped, which we can detect by seeing if the pdf
        // is the first thing:
        def obx = "/.OBSERVATION(0)/OBX"
        assert output.get("$obx-1") == "1"
        assert output.get("$obx-2") == "ED"
    </assertions>
  </test>

  <test name="annotationsAndComments">
    <inputs>
      <ohtMetadata name="input">
        <tns:DocumentEntry xmlns:tns="urn:org:openhealthtools:ihe:xds:metadata">
            <entryUUID>aecg</entryUUID>
            <formatCode>
                <code>93010</code>
                <schemeName>CPT-4</schemeName>
            </formatCode>
            <mimeType>application/xml</mimeType>
            <patientId>
                <idNumber>affinitypid</idNumber>
                <assigningAuthorityUniversalId>affinity</assigningAuthorityUniversalId>
            </patientId>
            <sourcePatientId>
                <idNumber>srcpid</idNumber>
                <assigningAuthorityUniversalId>local</assigningAuthorityUniversalId>
            </sourcePatientId>
            <extension>
                <name>Annotations</name>
                <value>OBX|1|NM|HR^Heart rate||93|/min</value>
            </extension>
            <extension>
                <name>Viewer_Comments</name>
                <value>[kard0007 13-01-2016 14:45:16 Comment] Test/Patrick</value>
            </extension>
        </tns:DocumentEntry>
      </ohtMetadata>
      <list name="relatedDocuments">
        <ohtMetadata>
          <tns:DocumentEntry xmlns:tns="urn:org:openhealthtools:ihe:xds:metadata">
            <entryUUID>pdf</entryUUID>
            <formatCode>
              <code>93010</code>
              <schemeName>CPT-4</schemeName>
            </formatCode>
            <mimeType>application/pdf</mimeType>
          </tns:DocumentEntry>
          <content>aGFpIQ==</content>
        </ohtMetadata>
      </list>
    </inputs>
    <assertions>
        assert returned != false

        // Like above tests, the order probably isn't crucial here, but we're using it
        // to verify that using using OBX segments for both comments and annotations
        // works as expected

        def obx = "/.OBSERVATION(0)/OBX"
        assert output.get("$obx-1") == "1"
        assert output.get("$obx-2") == "ST"
        assert output.get("$obx-3-1") == "Viewer_Comments"
        assert output.get("$obx-3-2") == "Comments from Kardia Viewer"
        assert output.get("$obx-5") == "[kard0007 13-01-2016 14:45:16 Comment] Test/Patrick"

        obx = "/.OBSERVATION(1)/OBX"
        assert output.get("$obx-1") == "2"
        assert output.get("$obx-2") == "NM"
        assert output.get("$obx-3-1") == "HR"
        assert output.get("$obx-3-2") == "Heart rate"
        assert output.get("$obx-5") == "93"
        assert output.get("$obx-6") == "/min"

        // just making sure the pdf is still on the end:
        obx = "/.OBSERVATION(2)/OBX"
        assert output.get("$obx-1") == "3"
        assert output.get("$obx-2") == "ED"
    </assertions>
  </test>
</tests>

