/**
 * This script is executed once per XDS document-set upload during
 * the auto-reconcile workflow. The input 'xdsinput' is a collection
 * of DocumentMetadataScriptingAPI each wrapping an XDS document from
 * the document-set (here the document-set comprises all documents that
 * have matching accession number within the current patient domain).
 *
 * The aECG and PDF documents can be indentified by their respective
 * MIME types, and can be interrogated for properties that will form
 * the basis for composing the DICOM modality worklist query. For
 * example this script might populate the output 'dcmquery' with
 * patient [issuer] identifiers and accession number as below:
 * 
 * def findECG(mimeType) {
 *   return xdsinput.find { document ->
 *       def format = document.get(XDSFormatCode)
 *       return format.getCodeValue() == "93010" &&
 *              format.getSchemeName() == "CPT-4" &&
 *              mimeType.equals(document.get(XDSMimeType))
 *   }
 * }
 *
 * def aecg = findECG("application/xml")
 * dcmquery.set(PatientID, aecg.get(XDSSourcePatientID).pid)
 * dcmquery.set(IssuerOfPatientID, aecg.get(XDSSourcePatientID).domainUUID)
 * dcmquery.set(AccessionNumber, aecg.get(XDSAccessionNumber)[0].getId())
 * 
 * The resulting dcmquery will be issued against the configured DICOM
 * modality worklist and the results of that query will be analyzed by
 * downstream morphers that are responsible for order-matching rules.
 * Note: 'dcmquery' uses the familiar DicomScriptingAPI and represents
 * the matching criteria for subsequent DICOM modality worklist query.
 */

import org.joda.time.*
import com.karos.rtk.common.HL7v2Date;

log.debug("Starting morpher [Compose DICOM Modality Worklist Query]")
log.debug("Morpher input contains {} XDS documents", xdsinput.size())

def findECG(mimeType) {
    return xdsinput.find { document ->
        def format = document.get(XDSFormatCode)
        return format.getCodeValue() == "93010" &&
               format.getSchemeName() == "CPT-4" &&
               mimeType.equals(document.get(XDSMimeType))
    }
}

def aecg = findECG("application/xml")
if (aecg == null) {
    aecg = findECG("text/xml")
}
if (aecg == null) {
    log.error("aECG was not found - terminating workflow")
    return false
}

log.trace("aECG document details:{}", aecg)

def  pdf = findECG("application/pdf")

if (pdf == null) {
    log.error("PDF was not found - terminating workflow")
    return false
}

log.trace("PDF document details:{}", pdf)

log.debug("Populating worklist query from AECG and PDF documents")

dcmquery.set(PatientID, aecg.get(XDSSourcePatientID).pid)
dcmquery.set("IssuerOfPatientIDQualifiersSequence/UniversalEntityID",
    aecg.get(XDSSourcePatientID).domainUUID, VR.UT)

return true
