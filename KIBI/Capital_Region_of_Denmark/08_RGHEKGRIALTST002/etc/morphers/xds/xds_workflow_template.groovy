import com.karos.rialto.workflow.model.GenericWorkflow
import com.karos.rialto.workflow.model.AttemptEvent
import com.karos.rialto.workflow.model.Task.Status
import com.karos.rialto.workflow.common.tasks.*
import com.karos.rtk.xds.DocumentMetadata
import com.karos.rialto.workflow.events.common.*
import com.karos.rialto.workflow.events.xds.reconcile.*
import com.karos.xds.workflow.tasks.*

log.debug("Starting XDS Worklist Reconciliation Workflow Factory")
log.debug("xdsinputs contains {} xds documents", xdsinputs.size())

if (xdsinputs.size() < 3) {
    log.debug("missing related document(s) - skipping workflow creation")
    return null
}

def findECG(mimeType) {
    return xdsinputs.find { document ->
        def format = document.get(XDSFormatCode)
        log.debug("Format CodeValue and SchemeName = {}, {}", format.getCodeValue(), format.getSchemeName())
        return format.getCodeValue() == "93010" &&
               format.getSchemeName() == "CPT-4" &&
               mimeType.equals(document.get(XDSMimeType))
    }
}

def aecg = findECG("text/xml")

if (aecg == null) {
    aecg = findECG("application/xml")
}

if (aecg == null) {
    log.debug("aECG not found - skipping workflow creation")
    return null
}

def orderingID = aecg.get(XDSExtendedMetadata("orderingID"))
def suppressworkflow = aecg.get(XDSExtendedMetadata("suppressWorkFlow"))
log.debug("suppressWorkFlow = {}", suppressworkflow)
if(suppressworkflow != null && (suppressworkflow.isEmpty() || Boolean.parseBoolean(suppressworkflow.get(0)))){
    log.debug("suppressWorkFlow flag is true in the Extended Metadata.  As a result, no workflow will be created.  suppressWorkFlow = {}, orderingID = {}", suppressworkflow, orderingID)
    return null
}

def pdf = findECG("application/pdf")

if (pdf == null) {
    log.debug("PDF not found - skipping workflow creation")
    return null
}

// when manual reconciliation endpoint is invoked, these variables will be populated in java
def automatic = (patientId == null || patientDomain == null || requestedProcedureId == null)

def registerDocumentsTaskBuilder = new RegisterDocumentsTask.Builder()
def claimWorklistEntryTaskBuilder = null

if (automatic) {

    claimWorklistEntryTaskBuilder = new ClaimWorklistEntryTask.Builder()
    claimWorklistEntryTaskBuilder.setWorklistRequestScript("/home/rialto/rialto/etc/morphers/xds/worklist_request.groovy")
    claimWorklistEntryTaskBuilder.setWorklistResultsScript("/home/rialto/rialto/etc/morphers/xds/worklist_results.groovy")

} else {

    claimWorklistEntryTaskBuilder = new UserClaimWorklistEntryTask.Builder()
    claimWorklistEntryTaskBuilder.setRequestedProcedureId(requestedProcedureId)
    claimWorklistEntryTaskBuilder.setIssuerOfAccessionNumber(issuerOfAccessionNumber)
    claimWorklistEntryTaskBuilder.setAccessionNumber(accessionNumber)
    claimWorklistEntryTaskBuilder.setPatientDomain(patientDomain)
    claimWorklistEntryTaskBuilder.setPatientId(patientId)

}

def reconcileDocumentsTaskBuilder = new ReconcileDocumentsTask.Builder()
def createEpicHL7FromDocumentsTaskBuilder = new CreateHL7FromDocumentsTask.Builder()
def createRHELHL7FromDocumentsTaskBuilder = new CreateHL7FromDocumentsTask.Builder()
def createKIBIHL7FromDocumentsTaskBuilder = new CreateHL7FromDocumentsTask.Builder()
//def createKIBITESTHL7FromDocumentsTaskBuilder = new CreateHL7FromDocumentsTask.Builder()
def sendEpicHL7TaskBuilder = new SendHL7Task.Builder()
def sendRHELHL7TaskBuilder = new SendHL7Task.Builder()
def sendKIBIHL7TaskBuilder = new SendHL7Task.Builder()
//def sendKIBITESTHL7TaskBuilder = new SendHL7Task.Builder()

def workflowTitle = automatic ? "Reconcile XDS Documents with Imaging Service Request (Automatic)"
                              : "Reconcile XDS Documents with Imaging Service Request (Manual)"

def workflowBuilder = new GenericWorkflow.Builder(workflowTitle)

for (DocumentMetadata document : documents) {
	registerDocumentsTaskBuilder.addEvents(new DocumentRegisteredEvent(document,
        registerDocumentsTaskBuilder.getId(), 1, AttemptEvent.Status.SUCCESS))
}

registerDocumentsTaskBuilder.setStatus(Status.SUCCESS)

claimWorklistEntryTaskBuilder.addDataDependency(registerDocumentsTaskBuilder, Status.SUCCESS)
reconcileDocumentsTaskBuilder.addDataDependency(registerDocumentsTaskBuilder, Status.SUCCESS)
reconcileDocumentsTaskBuilder.addDataDependency(claimWorklistEntryTaskBuilder, Status.SUCCESS)
createEpicHL7FromDocumentsTaskBuilder.addDataDependency(registerDocumentsTaskBuilder, Status.SUCCESS)
createEpicHL7FromDocumentsTaskBuilder.addDataDependency(reconcileDocumentsTaskBuilder, Status.SUCCESS)
createEpicHL7FromDocumentsTaskBuilder.addDataDependency(claimWorklistEntryTaskBuilder, Status.SUCCESS)
sendEpicHL7TaskBuilder.addDataDependency(createEpicHL7FromDocumentsTaskBuilder, Status.SUCCESS)
createRHELHL7FromDocumentsTaskBuilder.addDataDependency(registerDocumentsTaskBuilder, Status.SUCCESS)
createRHELHL7FromDocumentsTaskBuilder.addDataDependency(reconcileDocumentsTaskBuilder, Status.SUCCESS)
createRHELHL7FromDocumentsTaskBuilder.addDataDependency(claimWorklistEntryTaskBuilder, Status.SUCCESS)
sendRHELHL7TaskBuilder.addDataDependency(createRHELHL7FromDocumentsTaskBuilder, Status.SUCCESS)
createKIBIHL7FromDocumentsTaskBuilder.addDataDependency(registerDocumentsTaskBuilder, Status.SUCCESS)
createKIBIHL7FromDocumentsTaskBuilder.addDataDependency(reconcileDocumentsTaskBuilder, Status.SUCCESS)
createKIBIHL7FromDocumentsTaskBuilder.addDataDependency(claimWorklistEntryTaskBuilder, Status.SUCCESS)
sendKIBIHL7TaskBuilder.addDataDependency(createKIBIHL7FromDocumentsTaskBuilder, Status.SUCCESS)
//createKIBITESTHL7FromDocumentsTaskBuilder.addDataDependency(registerDocumentsTaskBuilder, Status.SUCCESS)
//createKIBITESTHL7FromDocumentsTaskBuilder.addDataDependency(reconcileDocumentsTaskBuilder, Status.SUCCESS)
//createKIBITESTHL7FromDocumentsTaskBuilder.addDataDependency(claimWorklistEntryTaskBuilder, Status.SUCCESS)
//sendKIBITESTHL7TaskBuilder.addDataDependency(createKIBIHL7FromDocumentsTaskBuilder, Status.SUCCESS)

workflowBuilder.addTaskBuilder(registerDocumentsTaskBuilder)
workflowBuilder.addTaskBuilder(claimWorklistEntryTaskBuilder)
workflowBuilder.addTaskBuilder(reconcileDocumentsTaskBuilder)
workflowBuilder.addTaskBuilder(createEpicHL7FromDocumentsTaskBuilder)
workflowBuilder.addTaskBuilder(createRHELHL7FromDocumentsTaskBuilder)
workflowBuilder.addTaskBuilder(createKIBIHL7FromDocumentsTaskBuilder)
//workflowBuilder.addTaskBuilder(createKIBITESTHL7FromDocumentsTaskBuilder)
workflowBuilder.addTaskBuilder(sendEpicHL7TaskBuilder)
workflowBuilder.addTaskBuilder(sendRHELHL7TaskBuilder)
workflowBuilder.addTaskBuilder(sendKIBIHL7TaskBuilder)
//workflowBuilder.addTaskBuilder(sendKIBITESTHL7TaskBuilder)

def pid = aecg.get(XDSSourcePatientID)
def acn = aecg.get(XDSAccessionNumber)

if (acn != null && acn.size() == 1){
	workflowBuilder.setAccessionNumber(acn[0].getId())
}

workflowBuilder.setUniversalId(pid.domainUUID)
workflowBuilder.setPatientId(pid.pid)

claimWorklistEntryTaskBuilder.setWorklistAETitle(worklistAE)
claimWorklistEntryTaskBuilder.setWorklistHost(worklistHost)
claimWorklistEntryTaskBuilder.setWorklistPort(worklistPort)
claimWorklistEntryTaskBuilder.setLocalAETitle(localAE)
claimWorklistEntryTaskBuilder.setWorklistResource("http://localhost:8085/mwl/mwl/update")

reconcileDocumentsTaskBuilder.setReconcileDocumentScript("/home/rialto/rialto/etc/morphers/xds/reconcile_document.groovy")
reconcileDocumentsTaskBuilder.setXDSRegistryResource("http://localhost:8081/registry")

createEpicHL7FromDocumentsTaskBuilder.setXdsToHl7ConversionScript("/home/rialto/rialto/etc/morphers/xds/convert_xds2scn_EPIC.groovy")
createRHELHL7FromDocumentsTaskBuilder.setXdsToHl7ConversionScript("/home/rialto/rialto/etc/morphers/xds/convert_xds2scn_RHEL.groovy")
createKIBIHL7FromDocumentsTaskBuilder.setXdsToHl7ConversionScript("/home/rialto/rialto/etc/morphers/xds/convert_xds2scn_KIBI.groovy")
//createKIBITESTHL7FromDocumentsTaskBuilder.setXdsToHl7ConversionScript("/home/rialto/rialto/etc/morphers/xds/convert_xds2scn_KIBI.groovy")

sendEpicHL7TaskBuilder.setMorpher("/home/rialto/rialto/etc/morphers/xds/morph_scn.groovy")
sendEpicHL7TaskBuilder.setDestinationHost("spintt04.sp.local")
sendEpicHL7TaskBuilder.setDestinationPort(9193)

sendRHELHL7TaskBuilder.setMorpher("/home/rialto/rialto/etc/morphers/xds/morph_scn.groovy")
sendRHELHL7TaskBuilder.setDestinationHost("tint112.unix.regionh.top.local")
sendRHELHL7TaskBuilder.setDestinationPort(4084)

sendKIBIHL7TaskBuilder.setMorpher("/home/rialto/rialto/etc/morphers/xds/morph_scn.groovy")
sendKIBIHL7TaskBuilder.setDestinationHost("10.144.2.114")
sendKIBIHL7TaskBuilder.setDestinationPort(11280)

//sendKIBITESTHL7TaskBuilder.setMorpher("/home/rialto/rialto/etc/morphers/xds/morph_scn.groovy")
//sendKIBITESTHL7TaskBuilder.setDestinationHost("10.144.2.114")
//sendKIBITESTHL7TaskBuilder.setDestinationPort(25565)

return workflowBuilder.build()
