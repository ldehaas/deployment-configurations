import org.joda.time.*
import com.karos.rtk.common.HL7v2Date;

def findMatchingWorklistEntry(serviceStartDatetime) {

    if (serviceStartDatetime == null) {
        log.warn("The match target document does not know its service start datetime.")
        return null
    }

    log.debug("service start datetime:{}", serviceStartDatetime)

    return dcminputs.find { dcmobject ->
        
        def issuedDatetime = new DateTime(dcmobject.getDate(
            IssueDateOfImagingServiceRequest,
            IssueTimeOfImagingServiceRequest))

        log.debug("issued datetime:{}", issuedDatetime)

        def scheduledProcedureList = dcmobject.get(ScheduledProcedureStepSequence)
        if (scheduledProcedureList.isEmpty()) {
            log.warn("The worklist entry does not contain scheduled procedures.")
            return false
        }

        def scheduledProcedure = scheduledProcedureList[0]

        def scheduledDatetime = new DateTime(
            scheduledProcedure.getDate(
                ScheduledProcedureStepStartDate,
                ScheduledProcedureStepStartTime))
        
        log.debug("scheduled datetime:{}", scheduledDatetime)

        def scheduledStatus = scheduledProcedure.get(ScheduledProcedureStepStatus)

        log.debug("scheduled status:{}", scheduledStatus)


        def placerordernumber = dcmobject.get(PlacerOrderNumberImagingServiceRequest)
        log.debug("placer order number:{}", placerordernumber)

    //KHC9329 - do not process RHEL orders prior to October 16, 2017 4:00 pm CEST
    if (placerordernumber != null && placerordernumber.contains("RHEL") && issuedDatetime.isBefore(new DateTime(2017, 10, 16, 14, 0).withZone(DateTimeZone.UTC))) {
        log.debug("KHC9329 - RHEL order is prior to 2017-10-16T14:00:00 - dropping")
        return false
    }

        if (placerordernumber != null && placerordernumber.contains("RHEL")) {
                return scheduledStatus != "APPROVED" &&
                serviceStartDatetime.isAfter(issuedDatetime) &&
                serviceStartDatetime.isBefore(scheduledDatetime.plusWeeks(3)) &&
                serviceStartDatetime.isAfter(scheduledDatetime.minusWeeks(3))
        }
        else {
        return scheduledStatus != "VERIFIED" &&
            scheduledStatus != "CANCELLED" &&
            scheduledStatus != "APPROVED" &&
            serviceStartDatetime.isAfter(issuedDatetime) &&
            serviceStartDatetime.isBefore(scheduledDatetime.plusDays(3)) &&
            serviceStartDatetime.isAfter(scheduledDatetime.minusDays(3))
        }
    }
}

def findECG(mimeType) {
    return xdsinputs.find { document ->
        def format = document.get(XDSFormatCode)
        return format.getCodeValue() == "93010" &&
               format.getSchemeName() == "CPT-4" &&
               mimeType.equals(document.get(XDSMimeType))
    }
}

def getDocumentStatus(match) {

    def scheduledProcedureList = dcmOutput.get(ScheduledProcedureStepSequence)
    def scheduledProcedure = scheduledProcedureList[0]
    def scheduledStatus = scheduledProcedure.get(ScheduledProcedureStepStatus)
    def placerordernumber = dcmobject.get(PlacerOrderNumberImagingServiceRequest)

    // document status is a script variable that indicates whether or not we
    // want to update the WorklistEntryStatus to verified
    //only set this value if you wish to do that.
    if (scheduledStatus == "APPROVED") {
        return "APPROVED"
    }
    if (placerordernumber.contains("RHEL")) {
        return null
    }
}

def findPreviouslyMatchedWorklistEntry(orderingID) {
    return dcminputs.find { dcmobject ->
        dcmobject.get(PlacerOrderNumberImagingServiceRequest) == orderingID
    }
}

def aecg = findECG("text/xml")

if (aecg == null) {
    aecg = findECG("application/xml")
}

if (aecg == null) {
    log.debug("aECG was not found - returning without match")
    return null
}

def orderingID = aecg.get(XDSExtendedMetadata("orderingID"))

log.debug("Evaluating worklist candidates against ECG documents")

// clone the original unsorted list for later.
def originals = dcminputs.collect { it }

// sort by serviceStartDateTime
dcminputs.sort { a, b ->

    def scheduledProcedureListA = a.get(ScheduledProcedureStepSequence)
    if (scheduledProcedureListA.isEmpty()) {
        log.warn("The worklist entry does not contain scheduled procedures.")
        return 1
    }

    def scheduledProcedureListB = b.get(ScheduledProcedureStepSequence)
    if (scheduledProcedureListB.isEmpty()) {
        log.warn("The worklist entry does not contain scheduled procedures.")
        return -1
    }

    def scheduledProcedureA = scheduledProcedureListA[0]
    def scheduledProcedureB = scheduledProcedureListB[0]

    def scheduledDatetimeA = new DateTime(
            scheduledProcedureA.getDate(
                ScheduledProcedureStepStartDate,
                ScheduledProcedureStepStartTime))

    def scheduledDatetimeB = new DateTime(
            scheduledProcedureB.getDate(
                ScheduledProcedureStepStartDate,
                ScheduledProcedureStepStartTime))
    
    return scheduledDatetimeA <=> scheduledDatetimeB
}

log.debug("Sort dcminputs by scheduled procedure start datetime {}", dcminputs)

// shifts any EPC orders to front of the list.
dcminputs.sort {
    def id = it.get(PlacerOrderNumberImagingServiceRequest)
    id != null && id.contains('EPC') ? -1 : 1
}

log.debug("Sort dcminputs to put any EPC orders first:{}", dcminputs)

def match = null

if (orderingID != null && !orderingID.isEmpty()) {
    match = findPreviouslyMatchedWorklistEntry(orderingID[0])
    log.debug("result from findPreviouslyMatchedWorklistEntry:{}", match)
} else {
    match = findMatchingWorklistEntry(aecg.get(XDSServiceStartTime))
    log.debug("result from findMatchingWorklistEntry:{}", match)
}

if (match == null) {
    log.debug("Worklist match was not found")
    return null
}

def status = aecg.get(XDSExtendedMetadata("Status"))

if (status == null || status.isEmpty()) {
   status = aecg.get(XDSExtendedMetadata("status"))
}

// because there's a match
documentStatus = "VERIFIED"

if (status[0] == "Approved") {
    // because aECG's approved
    documentStatus = "APPROVED"
}

return rawobjects[originals.indexOf(match)]
