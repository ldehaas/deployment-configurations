/**
 * This script is executed once per XDS document during auto-reconcile
 * workflow. The input is an instance of the DicomScriptingAPI wrapped
 * around the modality worklist entry that was selected upstream based
 * on the order matching rules. The output is an instance of the
 * DocumentMetadataScriptingAPI wrapped around the XDS document.
 *
 * This script can be used to update the document metadata in place,
 * using attributes of the matched DICOM modality worklist entry (the
 * order). Return true if the metadata has been updated, as this change
 * should be reflected in the registry. Return false if this script has
 * not made any modifications to the document metadata as this will
 * signal the workflow that no metadata update request need be sent.
 */

log.debug("Starting morpher [Reconcile Document via Worklist Entry]")

def reconcileWithEpicOrder() {

    log.debug("Reconciling document with an EPIC worklist entry")

    def modified = false

    def studyInstanceUid = input.get(StudyInstanceUID)

    if (studyInstanceUid != null && !studyInstanceUid.isEmpty()) {
        output.set(XDSExtendedMetadata('studyInstanceUid'), studyInstanceUid)
        log.debug("Reconciled study instance UID '{}'", studyInstanceUid)
        modified = true
    }

    def orderingPerson = input.get(OrderEnteredBy)

    if (orderingPerson != null && !orderingPerson.isEmpty()) {
        output.set(XDSExtendedMetadata('orderingPerson'), orderingPerson)
        log.debug("Reconciled ordering person '{}'", orderingPerson)
        modified = true
    }

    def orderingID = input.get(PlacerOrderNumberImagingServiceRequest)

    if (orderingID != null && !orderingID.isEmpty()) {
        output.set(XDSExtendedMetadata('orderingID'), orderingID)
        log.debug("Reconciled ordering ID '{}'", orderingID)
        modified = true
    }

    def orderingDepartmentSection = input.get(OrderEntererLocation)

    if (orderingDepartmentSection != null && !orderingDepartmentSection.isEmpty()) {
        output.set(XDSExtendedMetadata('orderingDepartmentSection'), orderingDepartmentSection)
        log.debug("Reconciled ordering department section '{}'", orderingDepartmentSection)
        modified = true
    }

    return modified
}

def reconcileWithRhelOrder() {

    log.debug("Reconciling document with an RHEL worklist entry")

    def modified = false

    def analysisType = input.get(RequestedProcedureDescription)

    if (analysisType != null && !analysisType.isEmpty()) {
        output.set(XDSExtendedMetadata('analysisType'), analysisType)
        log.debug("Reconciled analysis type '{}'", analysisType)
        modified = true
    }

    def appointmentTime = input.get(ScheduledProcedureStepStartTime)

    if (appointmentTime != null && !appointmentTime.isEmpty()) {
        output.set(XDSExtendedMetadata('appointmentTime'), appointmentTime)
        log.debug("Reconciled appointment time '{}'", appointmentTime)
        modified = true
    }

    def appointmentUnit = input.get(CurrentPatientLocation)

    if (appointmentUnit != null && !appointmentUnit.isEmpty()) {
        output.set(XDSExtendedMetadata('appointmentUnit'), appointmentUnit)
        log.debug("Reconciled appointment unit '{}'", appointmentUnit)
        modified = true   
    }

    def studyInstanceUid = input.get(StudyInstanceUID)

    if (studyInstanceUid != null && !studyInstanceUid.isEmpty()) {
        output.set(XDSExtendedMetadata('studyInstanceUid'), studyInstanceUid)
        log.debug("Reconciled study instance UID '{}'", studyInstanceUid)
        modified = true
    }

    def orderingPerson = input.get(OrderEnteredBy)

    if (orderingPerson != null && !orderingPerson.isEmpty()) {
        output.set(XDSExtendedMetadata('orderingPerson'), orderingPerson)
        log.debug("Reconciled ordering person '{}'", orderingPerson)
        modified = true
    }

    def orderingID = input.get(PlacerOrderNumberImagingServiceRequest)

    if (orderingID != null && !orderingID.isEmpty()) {
        output.set(XDSExtendedMetadata('orderingID'), orderingID)
        log.debug("Reconciled ordering ID '{}'", orderingID)
        modified = true
    }

    def orderingDepartmentSection = input.get(OrderEntererLocation)

    if (orderingDepartmentSection != null && !orderingDepartmentSection.isEmpty()) {
        output.set(XDSExtendedMetadata('orderingDepartmentSection'), orderingDepartmentSection)
        log.debug("Reconciled ordering department section '{}'", orderingDepartmentSection)
        modified = true
    }

    return modified
}

def orderingID = output.get(XDSExtendedMetadata('orderingID'))
if (orderingID != null && !orderingID.isEmpty()) {
    return false // previous version is reconciled
}   

orderingID = input.get(PlacerOrderNumberImagingServiceRequest)

if (orderingID.endsWith("^EPC")) {
    return reconcileWithEpicOrder()
} else {
    return reconcileWithRhelOrder()
}
