// CFIND response morpher for modality worklists
log.info("MWL CFIND Response Morpher")

def orderingID = imagingServiceRequest.getPlacerIssuerAndNumber()
log.debug("Setting ordering ID to {}", orderingID)
set(PlacerOrderNumberImagingServiceRequest, orderingID)

def orderingDepartment = imagingServiceRequest.getOrderEnteringLocation()
log.debug("Setting ordering department to {}", orderingDepartment)
set(OrderEntererLocation, orderingDepartment)

def visitStatus = imagingServiceRequest.getVisitStatus()
log.debug("visitStatus in cfind response morpher is {}", visitStatus)
if (visitStatus != null) {
    def patientLocation = visitStatus.getCurrentPatientLocation()
    if (patientLocation != null) {
        set(CurrentPatientLocation, patientLocation)
    }
}

def orderingPerson = imagingServiceRequest.getEnteredBy()
log.debug("Setting orderingPerson to {}", orderingPerson)
set(OrderEnteredBy, orderingPerson)

//If the requestedProcedureDescription is not automatically returned in mwl, will need to uncomment the following.

def requestedProcedureSequence = imagingServiceRequest.getRequestedProcedureSequence()
if (requestedProcedureSequence != null && requestedProcedureSequence.size() == 1) {
    def requestedProcedureDescription = requestedProcedureSequence[0].getRequestedProcedureDescription();
    set(RequestedProcedureDescription, requestedProcedureDescription)
}

log.debug("Imaging Service Request is:\n{}", imagingServiceRequest)

