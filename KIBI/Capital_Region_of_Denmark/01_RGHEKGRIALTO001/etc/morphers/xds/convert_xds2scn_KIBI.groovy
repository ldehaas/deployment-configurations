import org.joda.time.*
import com.karos.rtk.common.HL7v2Date;
 
log.debug("Starting morpher [XDS Documents -> Study Change Notification - KIBI")
log.debug("Morpher input contains {} XDS documents", xdsinputs.size())
 
def findECG(mimeType) {
    return xdsinputs.find { document ->
        def format = document.get(XDSFormatCode)
        return format.getCodeValue() == "93010" &&
               format.getSchemeName() == "CPT-4" &&
               mimeType.equals(document.get(XDSMimeType))
    }
}

def aecg = findECG("application/xml")
if (aecg == null) {
    aecg = findECG("text/xml")
}
if (aecg == null) {
    log.error("AECG was not found - terminating workflow")
    return false
}

log.debug("AECG document details:{}", aecg)

def status = aecg.get(XDSExtendedMetadata("Status"))

if (status == null || status.isEmpty()) {
    status = aecg.get(XDSExtendedMetadata("status"))
}

log.debug("Document status - KIBI convert script {}", status)

def metadatanames = aecg.get(XDSExtendedMetadataNames)

log.debug("Document metadatanames - KIBI convert script {}", metadatanames)

//if (status != null && status.contains("Approved")) {
//    log.debug("Skipped creation of ORU from Approved documents")
//    return false
//}

def  pdf = findECG("application/pdf")

if (pdf == null) {
    log.error("PDF was not found - terminating workflow")
    return false
}

//log.trace("PDF document details:{}", pdf)

log.debug("Populating notification from aECG and PDF documents")

initialize("ORU", "R01", "2.5")

def priority = aecg.get(XDSExtendedMetadata("priority"))
if (priority != null && !priority.isEmpty()) {
    def obx = reserveOBX()
    set("$obx-2", priority[0])
}

def analysistype = aecg.get(XDSExtendedMetadata("analysisType"))
if (analysistype != null && !analysistype.isEmpty()) {
    set("OBR-4", analysistype[0])
}

def appointmentunit = aecg.get(XDSExtendedMetadata("appointmentUnit"))
log.debug("Appointment Unit {}", appointmentunit)
if (appointmentunit != null && !appointmentunit.isEmpty()) {
    set("PV1-3", appointmentunit[0])
}

set("MSH-7", HL7v2Date.now(DateTimeZone.getDefault()))
 
// indicate that the message is a test (comment this out for prod)
set("MSH-11", "P")
 
set("MSH-18", "8859/1")
 
def pid = aecg.get(XDSSourcePatientID)
set("PID-3-1", pid.pid)
 
if (pid.domainUUID == "2.16.124.113638.6.1.1.1") {
    def namespace = "CPR"
    if (pid.pid ==~ /.{7}[A-Za-z]{2}.*/) {
        namespace = "ECPR"
    }
    set("PID-3-4-1", namespace)
    set("PID-3-5-1", namespace)
} else {
    set("PID-3-4-2", pid.domainUUID)
    set("PID-3-4-3", "ISO")
}
 
set("PID-5-1", aecg.get(XDSSourcePatientFamilyName))
set("PID-5-2", aecg.get(XDSSourcePatientGivenName))
set("PID-7", aecg.get(XDSSourcePatientBirthDate))
set("PID-8", aecg.get(XDSSourcePatientSex))
 
def orderingID = aecg.get(XDSExtendedMetadata("orderingID"))

log.debug("orderingID {} ", orderingID)

if (orderingID == null || orderingID.isEmpty()) {
    return false
}

def viewerComments = aecg.get(XDSExtendedMetadata("Viewer_Comments"))
def viewerApproved = aecg.get(XDSExtendedMetadata("Viewer_Approved"))

def viewerApprovedPresent = (viewerApproved != null && !viewerApproved.isEmpty())
def viewerCommentsPresent = (viewerComments != null && !viewerComments.isEmpty())

if (orderingID != null && !orderingID.isEmpty()) {
    def splitID = orderingID.get(0).split("\\^")
    if (splitID.size() < 2 || splitID[1] != 'RHEL' || viewerApprovedPresent || viewerCommentsPresent) {
            log.debug("orderingID for non RHEL order - exiting convert_xds2scn_KIBI.groovy")
            return false
    }
    set("OBR-2",splitID[0])
    set("OBR-2-2",splitID[1])
    set("ORC-2",splitID[0])
    set("ORC-2-2",splitID[1])
}

def accns = aecg.get(XDSAccessionNumber)
if (!accns.isEmpty()) {
    set("OBR-3", accns[0].getId())
    set("ORC-3", accns[0].getId())
}

def local(dt) {
    return dt == null ? null : dt.withZone(DateTimeZone.getDefault())
}
 
def startTime = local(aecg.get(XDSServiceStartTime))
set("ORC-7-4", startTime)
set("OBR-27-4", startTime)
 
def stopTime = local(aecg.get(XDSServiceStopTime))
set("ORC-7-5", stopTime)
set("OBR-27-5", stopTime)

def orderingperson = aecg.get(XDSExtendedMetadata("orderingPerson"))
log.debug("Ordering Person {}", orderingperson)
if (orderingperson != null && !orderingperson.isEmpty()){
    def splitorderingperson = orderingperson.get(0).split("\\^")
    log.debug("Ordering Person after split {}", splitorderingperson)   
    for (i = 0; i < splitorderingperson.size(); i++)
    set("ORC-12-"+(i+1), splitorderingperson[i])
}

def orderingdepartmentsection = aecg.get(XDSExtendedMetadata("orderingDepartmentSection"))
log.debug("Ordering Department Section {}", orderingdepartmentsection)
if (orderingdepartmentsection != null && !orderingdepartmentsection.isEmpty()) {
    def splitorderingdepartmentsection = orderingdepartmentsection.get(0).split("\\^")
    log.debug("Ordering Department Section after split {}", splitorderingdepartmentsection)
    for (i = 0; i < splitorderingdepartmentsection.size(); i++)    
    set("ORC-13-"+(i+1), splitorderingdepartmentsection[i])
}

// Mystery value requested by customer
set("OBR-21", "J")
 
// Result status id: F = final
set("OBR-25", "F")
 
/**
 * Copy an observation from the xds metadata to a new OBX segment.
 *
 * @param slotName extended metadata slot name containing the observation,
 *                 also used as the OBX-3 observation identifier
 * @param description OBX-3-2 observation identifier text
 */

set("ZDS-1", aecg.get(XDSEntryUUID))

return true
