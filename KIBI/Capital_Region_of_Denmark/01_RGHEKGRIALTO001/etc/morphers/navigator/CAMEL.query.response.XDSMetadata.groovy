def contact = get('PV1-3-1')

log.debug('Contact in response from CAMEL: \"' + contact + '\"')

if (contact == null || "".equals(contact)) {
  log.warn('Contact was not found! Not modifying XDS metadata!')
  return
}

def hospital = contact.substring(0, 4)
def department = contact

log.info('Setting AuthorIstitution to: \"' + hospital + '\"')
set(XDSAuthorInstitutions, institution(hospital))

log.info('Setting DepartmentSection to: \"' + department + '\"')
set(XDSExtendedMetadata('departmentSection'), department)
