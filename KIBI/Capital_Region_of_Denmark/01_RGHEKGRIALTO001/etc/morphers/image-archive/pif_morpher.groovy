log.debug("Morphing PIF")

// The key is the domain and the value is a list holding the values for [/.MSH-3, /.MSH-4, /.MSH-5, /.MSH-6]
def sendingAndRecievingInformation = [
    "CG&county.general&ISO":["RIALTO_CG", "KAROS_CG", "RIALTO_NAVIGATOR", "KAROS_HEALTH"],
    "TL&toronto.lakeshore&ISO":["RIALTO_TL", "KAROS_TL", "RIALTO_NAVIGATOR", "KAROS_HEALTH"],
    "DR&don.river&ISO":["RIALTO_DR", "KAROS_DR", "RIALTO_NAVIGATOR", "KAROS_HEALTH"],
    "CANON&hc.ontario&ISO":["RIALTO_ONT", "KAROS_ONT", "RIALTO_NAVIGATOR", "KAROS_HEALTH"],
    "NAVIGATOR&rialto.navigator&ISO":["RIALTO_NAVIGATOR", "KAROS_HEALTH", "RIALTO_NAVIGATOR", "KAROS_HEALTH"],
    ]

def domain = get(IssuerOfPatientID)
log.info("IssuerOfPatientID is " + domain)
def info = sendingAndRecievingInformation.get(domain);

if (info == null) {
    log.warn("PIF Morphing script does not know about the given IssuerOfPatientID. Will not update MSH-3,4,5,6 based on IssuerOfPatientID.")
} else {
    set("/.MSH-3", info.get(0))
    set("/.MSH-4", info.get(1))
    set("/.MSH-5", info.get(2))
    set("/.MSH-6", info.get(3))
}

/**
 * Copies up to three components of a name from the SR into the hl7 message.
 * DICOM person name elements are supposed to be of the form
 * family^given^middle^prefix^suffix.  HL7 names are sometimes of the form
 * family^given^middle^suffix^prefix but some components are actually 
 * id^family^given^...
 * 
 * In the case where the id is there, pass startingComponent=2 to offset where
 * we put the name.  However, in our sample SRs, the values of the person names
 * actually include the id (they are in DICOM format instead of HL7).  In this
 * case, just leave the default startingComponent.
 * 
 * @param dicomTag place to get name from in SR
 * @param hl7prefix place to put name in hl7
 * @param startingComponent if name doesn't line up at beginning of 
 * @return
 */
def setName(dicomTag, hl7prefix, startingComponent=1) {
    nameParts = split(dicomTag)
    if (nameParts == null) {
        return
    }

    // loop far enough to get the id if present, plus the family and given names
    int maxPartsToCopy = 3
    
    for (int i = 0; i < maxPartsToCopy && i < nameParts.size(); i++) {
        set(hl7prefix + "-" + (startingComponent++), nameParts[i])
    }
}

// Set the Patient Name with up to 3 components from DICOM
setName(PatientName, "PID-5")
setName(PatientMotherBirthName, "PID-6")

set("PID-7", get(PatientBirthDate))
set("PID-8", get(PatientSex))
set("PID-11", get(PatientAddress))
set("PID-13", get(PatientTelephoneNumbers))
log.debug("End Morphing PIF")