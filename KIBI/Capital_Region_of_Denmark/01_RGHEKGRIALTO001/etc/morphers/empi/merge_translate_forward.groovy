log.info("Starting merge_translate_forward.groovy")

def messageType = get('MSH-9-1')
def triggerEvent = get('MSH-9-2')

log.info("message type is: {}, and trigger event is: {}", messageType, triggerEvent)

if (messageType != null && triggerEvent != null && "ADT".equals(messageType) && "A43".equals(triggerEvent)) {
    log.info("setting MSH-9-2 to 40")
    set('MSH-9-2', 'A40')
}

log.info("Done merge_translate_forward.groovy")
