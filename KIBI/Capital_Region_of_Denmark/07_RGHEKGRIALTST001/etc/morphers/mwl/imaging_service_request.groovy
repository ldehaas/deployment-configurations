import com.karos.rtk.common.Terser
import ca.uhn.hl7v2.model.Message
import ca.uhn.hl7v2.model.Segment
import ca.uhn.hl7v2.HL7Exception
import ca.uhn.hl7v2.model.Type

def getField(segmentName, fieldNumber) {

    def terser = new Terser(input.getMessage())

    def segment = null
    try {
        segment = terser.getSegment(segmentName, false)
    } catch (HL7Exception e) {
        log.warn("Message does not contain segment {}", segmentName)
        return null
    }

    def field = null
    try {
        field = segment.getField(fieldNumber, 0)
    } catch (HL7Exception e) {
        log.warn("Segment does not contain field number {}", fieldNumber)
        return null
    }

    return field.encode()
}

log.debug("imaging_service_request START")

log.info("Here's how the ORM looks:\n{}", input)
log.info("Here's how the ISR looks (before):\n{}", imagingServiceRequest)

imagingServiceRequest.setAccessionNumberNamespaceId("CRD")
imagingServiceRequest.setAccessionNumberUniversalId("2.16.124.113638.6.1.1.1")
imagingServiceRequest.setAccessionNumberUniversalIdType("ISO")

def patientIdentification = imagingServiceRequest.getPatientIdentification()
patientIdentification.setPatientIdNamespaceId("CRD")
patientIdentification.setPatientIdUniversalId("2.16.124.113638.6.1.1.1")
patientIdentification.setPatientIdUniversalIdType("ISO")

def requestedProcedure = imagingServiceRequest.getRequestedProcedureSequence().get(0)
requestedProcedure.setRequestedProcedureID(imagingServiceRequest.getAccessionNumber())
def scheduledProcedureStep = requestedProcedure.getScheduledProcedureStepSequence().get(0)
scheduledProcedureStep.setScheduledProcedureStepIDString(imagingServiceRequest.getAccessionNumber())

scheduledProcedureStep.setScheduledProcedureStepStatus("SCHEDULED")

def orc10 = getField("/.ORC", 10)
if (orc10 != null && !orc10.isEmpty()) {
    log.debug("ORC-10 (ordering person): {}", orc10)
    imagingServiceRequest.setEnteredBy(orc10)
}

def orc13 = getField("/.ORC", 13)
if (orc13 != null && !orc13.isEmpty()) {
    log.debug("ORC-13 (orderer's location): {}", orc13)
    imagingServiceRequest.setOrderEnteringLocation(orc13)
}

log.info("Here's how the ISR looks (after):\n{}", imagingServiceRequest)

log.debug("imaging_service_request END")
