import com.karos.rialto.workflow.model.GenericWorkflowBuilder
import com.karos.rialto.workflow.model.AttemptEvent
import com.karos.rialto.workflow.model.Task.Status
import com.karos.rialto.workflow.common.tasks.*
import com.karos.rtk.xds.DocumentMetadata
import com.karos.rialto.workflow.events.common.*
import com.karos.rialto.workflow.events.xds.reconcile.*
import com.karos.xds.workflow.tasks.*

log.debug("Starting morpher [XDS Worklist Reconciliation Factory]")
log.debug("Morpher input contains {} XDS documents", xdsinputs.size())

if (xdsinputs.size() < 3) {
    log.debug("missing related document(s) - skipping workflow creation")
    return null
}

def findECG(mimeType) {
    return xdsinputs.find { document ->
        def format = document.get(XDSFormatCode)
        log.debug("Format CodeValue and SchemeName = {}, {}", format.getCodeValue(), format.getSchemeName())
        return format.getCodeValue() == "93010" &&
               format.getSchemeName() == "CPT-4" &&
               mimeType.equals(document.get(XDSMimeType))
    }
}

def aecg = findECG("text/xml")

if (aecg == null) {
    aecg = findECG("application/xml")
}

if (aecg == null) {
    log.debug("aECG not found - skipping workflow creation")
    return null
}

def pdf = findECG("application/pdf")

if (pdf == null) {
    log.debug("PDF not found - skipping workflow creation")
    return null
}

def registerDocumentsTaskBuilder = new RegisterDocumentsTaskBuilder()
def claimWorklistEntryTaskBuilder = new ClaimWorklistEntryTaskBuilder()
def reconcileDocumentsTaskBuilder = new ReconcileDocumentsTaskBuilder()
def createEpicHL7FromDocumentsTaskBuilder = new CreateHL7FromDocumentsTaskBuilder()
def createKibiHL7FromDocumentsTaskBuilder = new CreateHL7FromDocumentsTaskBuilder()
def sendEpicHL7TaskBuilder = new SendHL7TaskBuilder()
def sendKibiHL7TaskBuilder = new SendHL7TaskBuilder()

def workflowBuilder = new GenericWorkflowBuilder("Reconcile XDS Metadata via Modality Worklist")

for (DocumentMetadata document : documents) {
	registerDocumentsTaskBuilder.addInitialEvent(new DocumentRegisteredEvent(document,
        registerDocumentsTaskBuilder.getId(), AttemptEvent.Status.SUCCESS))
}

registerDocumentsTaskBuilder.setInitialStatus(Status.SUCCESS)

claimWorklistEntryTaskBuilder.addDependency(registerDocumentsTaskBuilder, true, Status.SUCCESS)
reconcileDocumentsTaskBuilder.addDependency(registerDocumentsTaskBuilder, true, Status.SUCCESS)
reconcileDocumentsTaskBuilder.addDependency(claimWorklistEntryTaskBuilder, true, Status.SUCCESS)
createEpicHL7FromDocumentsTaskBuilder.addDependency(registerDocumentsTaskBuilder, true, Status.SUCCESS)
createEpicHL7FromDocumentsTaskBuilder.addDependency(reconcileDocumentsTaskBuilder, true, Status.SUCCESS)
createEpicHL7FromDocumentsTaskBuilder.addDependency(claimWorklistEntryTaskBuilder, true, Status.SUCCESS)
sendEpicHL7TaskBuilder.addDependency(createEpicHL7FromDocumentsTaskBuilder, true, Status.SUCCESS)
createKibiHL7FromDocumentsTaskBuilder.addDependency(registerDocumentsTaskBuilder, true, Status.SUCCESS)
createKibiHL7FromDocumentsTaskBuilder.addDependency(reconcileDocumentsTaskBuilder, true, Status.SUCCESS)
createKibiHL7FromDocumentsTaskBuilder.addDependency(claimWorklistEntryTaskBuilder, true, Status.SUCCESS)
sendKibiHL7TaskBuilder.addDependency(createKibiHL7FromDocumentsTaskBuilder, true, Status.SUCCESS)

workflowBuilder.addTaskBuilder(registerDocumentsTaskBuilder)
workflowBuilder.addTaskBuilder(claimWorklistEntryTaskBuilder)
workflowBuilder.addTaskBuilder(reconcileDocumentsTaskBuilder)
workflowBuilder.addTaskBuilder(createEpicHL7FromDocumentsTaskBuilder)
workflowBuilder.addTaskBuilder(createKibiHL7FromDocumentsTaskBuilder)
workflowBuilder.addTaskBuilder(sendEpicHL7TaskBuilder)
workflowBuilder.addTaskBuilder(sendKibiHL7TaskBuilder)

def pid = aecg.get(XDSSourcePatientID)
def acn = aecg.get(XDSAccessionNumber)

if (acn != null && acn.size() == 1){
	workflowBuilder.setAccessionNumber(acn[0].getId())
}

workflowBuilder.setUniversalId(pid.domainUUID)
workflowBuilder.setPatientId(pid.pid)

claimWorklistEntryTaskBuilder.setWorklistRequestScript("/home/rialto/rialto/etc/morphers/xds/worklist_request.groovy")
claimWorklistEntryTaskBuilder.setWorklistResultsScript("/home/rialto/rialto/etc/morphers/xds/worklist_results.groovy")
claimWorklistEntryTaskBuilder.setWorklistAETitle(worklistAE)
claimWorklistEntryTaskBuilder.setWorklistHost(worklistHost)
claimWorklistEntryTaskBuilder.setWorklistPort(worklistPort)
claimWorklistEntryTaskBuilder.setLocalAETitle(localAE)
claimWorklistEntryTaskBuilder.setWorklistResource("http://localhost:8085/mwl/mwl/update")

reconcileDocumentsTaskBuilder.setReconcileDocumentScript("/home/rialto/rialto/etc/morphers/xds/reconcile_document.groovy")
reconcileDocumentsTaskBuilder.setXDSRegistryResource("http://localhost:8081/registry")

createEpicHL7FromDocumentsTaskBuilder.setXdsToHl7ConversionScript("/home/rialto/rialto/etc/morphers/xds/convert_xds2scn_EPIC.groovy")
createKibiHL7FromDocumentsTaskBuilder.setXdsToHl7ConversionScript("/home/rialto/rialto/etc/morphers/xds/convert_xds2scn_KIBI.groovy")

sendEpicHL7TaskBuilder.setMorpher("/home/rialto/rialto/etc/morphers/xds/morph_scn.groovy")
sendEpicHL7TaskBuilder.setDestinationHost("spintt04.sp.local")
sendEpicHL7TaskBuilder.setDestinationPort(9193)

sendKibiHL7TaskBuilder.setMorpher("/home/rialto/rialto/etc/morphers/xds/morph_scn.groovy")
sendKibiHL7TaskBuilder.setDestinationHost("10.144.2.235")
sendKibiHL7TaskBuilder.setDestinationPort(21110)

return workflowBuilder.build()
