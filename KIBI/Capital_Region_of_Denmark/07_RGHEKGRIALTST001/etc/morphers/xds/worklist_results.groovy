import org.joda.time.*
import com.karos.rtk.common.HL7v2Date;

def findMatchingWorklistEntry(serviceStartDatetime) {
    if (serviceStartDatetime == null) {
        log.warn("The match target document does not know its service start datetime.")
        return null
    }

    log.debug("service start datetime:{}", serviceStartDatetime)

    return dcminputs.find { dcmobject ->
        
        def issuedDatetime = new DateTime(dcmobject.getDate(
            IssueDateOfImagingServiceRequest,
            IssueTimeOfImagingServiceRequest))

        log.debug("issued datetime:{}", issuedDatetime)

        def scheduledProcedureList = dcmobject.get(ScheduledProcedureStepSequence)
        if (scheduledProcedureList.isEmpty()) {
            log.warn("The worklist entry does not contain scheduled procedures.")
            return false
        }

        def scheduledProcedure = scheduledProcedureList[0]

        def scheduledDatetime = new DateTime(
            scheduledProcedure.getDate(
                ScheduledProcedureStepStartDate,
                ScheduledProcedureStepStartTime))
        
        log.debug("scheduled datetime:{}", scheduledDatetime)

        def scheduledStatus = scheduledProcedure.get(ScheduledProcedureStepStatus)

        log.debug("scheduled status:{}", scheduledStatus)

        return scheduledStatus != "VERIFIED" &&
            serviceStartDatetime.isAfter(issuedDatetime) &&
            serviceStartDatetime.isBefore(scheduledDatetime.plusDays(3)) &&
            serviceStartDatetime.isAfter(scheduledDatetime.minusDays(3))
    }
}

def findECG(mimeType) {
    return xdsinputs.find { document ->
        def format = document.get(XDSFormatCode)
        return format.getCodeValue() == "93010" &&
               format.getSchemeName() == "CPT-4" &&
               mimeType.equals(document.get(XDSMimeType))
    }
}

def aecg = findECG("text/xml")

if (aecg == null) {
    aecg = findECG("application/xml")
}

if (aecg == null) {
    log.debug("aECG was not found - returning without match")
    return null
}

log.debug("Evaluating worklist candidates against ECG documents")

def match = findMatchingWorklistEntry(aecg.get(XDSServiceStartTime))

if (match == null) {
    log.debug("Worklist match was not found")
    return null
}

return rawobjects[dcminputs.indexOf(match)]
