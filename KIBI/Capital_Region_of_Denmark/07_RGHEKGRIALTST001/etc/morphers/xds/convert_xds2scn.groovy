import org.joda.time.*
import com.karos.rtk.common.HL7v2Date;
 
log.error("Starting morpher [XDS Documents -> Study Change Notification]")
log.error("Morpher input contains {} XDS documents", xdsinputs.size())
 
def findECG(mimeType) {
    return xdsinputs.find { document ->
        def format = document.get(XDSFormatCode)
        return format.getCodeValue() == "93010" &&
               format.getSchemeName() == "CPT-4" &&
               mimeType.equals(document.get(XDSMimeType))
    }
}

def aecg = findECG("application/xml")
if (aecg == null) {
    aecg = findECG("text/xml")
}
if (aecg == null) {
    log.error("AECG was not found - terminating workflow")
    return false
}

log.trace("AECG document details:{}", aecg)

def  pdf = findECG("application/pdf")

if (pdf == null) {
    log.error("PDF was not found - terminating workflow")
    return false
}

log.trace("PDF document details:{}", pdf)

log.error("Populating notification from aECG and PDF documents")
 
initialize("ORU", "R01", "2.5")
 
set("MSH-7", HL7v2Date.now(DateTimeZone.getDefault()))
 
// indicate that the message is a test (comment this out for prod)
set("MSH-11", "T")
 
set("MSH-18", "8859/1")
 
def pid = aecg.get(XDSSourcePatientID)
set("PID-3-1", pid.pid)
 
if (pid.domainUUID == "2.16.124.113638.6.1.1.1") {
    def namespace = "CPR"
    if (pid.pid ==~ /.{7}[A-Za-z]{2}.*/) {
        namespace = "ECPR"
    }
    set("PID-3-4-1", namespace)
    set("PID-3-5-1", namespace)
} else {
    set("PID-3-4-2", pid.domainUUID)
    set("PID-3-4-3", "ISO")
}
 
set("PID-5-1", aecg.get(XDSSourcePatientFamilyName))
set("PID-5-2", aecg.get(XDSSourcePatientGivenName))
set("PID-7", aecg.get(XDSSourcePatientBirthDate))
set("PID-8", aecg.get(XDSSourcePatientSex))
 
def departmentSections = aecg.get(XDSExtendedMetadata("departmentSection"))
if (departmentSections != null && !departmentSections.isEmpty()) {
    def depSec = departmentSections[0]
 
    if (depSec != null) {
        def depSecType = "SKS"
 
        if (depSec.size() > 8) {
            depSecType = "SOR"
        }
 
        set("PV1-3-1-1", depSec)
        set("PV1-3-1-2", depSecType)
        set("PV1-3-4-1", depSec)
        set("PV1-3-4-2", depSecType)
    }
}

def orderingID = aecg.get(XDSExtendedMetadata("orderingID"))
if (orderingID != null && !orderingID.isEmpty()) {
    set("OBR-2",orderingID[0])
    set("OBR-2-2","EPC")
    set("ORC-2",orderingID[0])
    set("ORC-2-2","EPC")
}

def accns = aecg.get(XDSAccessionNumber)
if (!accns.isEmpty()) {
    set("OBR-3", accns[0].getId())
    set("ORC-3", accns[0].getId())
}
 
set("OBR-4", "DNK05219")
 
def local(dt) {
    return dt == null ? null : dt.withZone(DateTimeZone.getDefault())
}
 
def startTime = local(aecg.get(XDSServiceStartTime))
set("ORC-7-4", startTime)
set("OBR-27-4", startTime)
 
def stopTime = local(aecg.get(XDSServiceStopTime))
set("ORC-7-5", stopTime)
set("OBR-27-5", stopTime)
 
// Mystery value requested by customer
set("OBR-21", "J")
 
// Result status id: F = final
set("OBR-25", "F")
 
/**
 * Copy an observation from the xds metadata to a new OBX segment.
 *
 * @param slotName extended metadata slot name containing the observation,
 *                 also used as the OBX-3 observation identifier
 * @param description OBX-3-2 observation identifier text
 */
def copyObservation = { slotName, description ->
    def values = aecg.get(XDSExtendedMetadata(slotName))
    if (values != null && !values.isEmpty()) {
        def obx = reserveOBX()
 
        log.error("Recording extended metadata slot {} into {}: {}", slotName, obx, values)
 
        set("$obx-2", "ST")
        set("$obx-3-1", slotName)
        set("$obx-3-2", description)
        set("$obx-5", values[0])
 
    } else {
        log.error("No extended metadata with name {}.  Not creating corresponding observation OBX",
            slotName)
    }
}
 
copyObservation("Viewer_Comments", "Comments from Kardia Viewer")
copyObservation("Viewer_Approved", "Approved in Kardia Viewer")
 
def annotations = aecg.get(XDSExtendedMetadata("Annotations"))
if (annotations != null && !annotations.isEmpty()) {
    def annotation = annotations[0]
    log.warn("annotation: {}", annotation)
    try {
        // TODO: work around any potentially strange input by trimming out anything
        // that doesn't start with OBX?
        def annotationCount = annotation.split('\r').size()
        log.error("Copying {} annotations", annotationCount)
 
        // This is a workaround: we are given raw OBX segments that use the standard
        // encoding characters and to parse them, we paste together enough of a header
        // to make a syntactically valid hl7 message:
        def annotationHL7 = newHL7("MSH|^~\\&|||||||ORU^R01|||2.3\rPID|\rORC|\rOBR|\r" + annotation)
        log.error("Parsed annotations as HL7 message:\n{}", annotationHL7)
 
        for (int i = 0; i < annotationCount; i++) {
           def annoObx = "/.OBSERVATION($i)/OBX"
           def scnObx = reserveOBX()
 
            // copy a defined set of fields from the annotation to a new OBX in the scn:
            ["2", "3-1", "3-2", "5", "6"].each { field ->
                set("$scnObx-$field", annotationHL7.get("$annoObx-$field"))
            }
        }
    } catch (Exception e) {
        log.warn("Unable to parse annotations", e)
    }
}
 
def content = pdf.get(XDSDocumentContent).getBase64()
if (content != null) {
    def obx = reserveOBX()
    set("$obx-2", "ED")
    set("$obx-3", "EKG")
    set("$obx-5-1", "KIBI")
    set("$obx-5-2", "application")
    set("$obx-5-3", "pdf")
    set("$obx-5-4", "Base64")
    set("$obx-5-5", content)
 
    log.error("Inserted pdf with length {}", content.length())
 
    // This is a pretty small little base64-encoded hello-world pdf for testing purposes:

    if (false) {
        set("$obx-5-5",
           "JVBERi0xLjEKJcKlwrHDqwoKMSAwIG9iagogIDw8IC9UeXBlIC9DYXRhbG9nCiAgICAgL1B" +
           "hZ2VzIDIgMCBSCiAgPj4KZW5kb2JqCgoyIDAgb2JqCiAgPDwgL1R5cGUgL1BhZ2VzCiAgIC" +
           "AgL0tpZHMgWzMgMCBSXQogICAgIC9Db3VudCAxCiAgICAgL01lZGlhQm94IFswIDAgMzAwI" +
           "DE0NF0KICA+PgplbmRvYmoKCjMgMCBvYmoKICA8PCAgL1R5cGUgL1BhZ2UKICAgICAgL1Bh" +
           "cmVudCAyIDAgUgogICAgICAvUmVzb3VyY2VzCiAgICAgICA8PCAvRm9udAogICAgICAgICA" +
           "gIDw8IC9GMQogICAgICAgICAgICAgICA8PCAvVHlwZSAvRm9udAogICAgICAgICAgICAgIC" +
           "AgICAvU3VidHlwZSAvVHlwZTEKICAgICAgICAgICAgICAgICAgL0Jhc2VGb250IC9UaW1lc" +
           "y1Sb21hbgogICAgICAgICAgICAgICA+PgogICAgICAgICAgID4+CiAgICAgICA+PgogICAg" +
           "ICAvQ29udGVudHMgNCAwIFIKICA+PgplbmRvYmoKCjQgMCBvYmoKICA8PCAvTGVuZ3RoIDU" +
           "1ID4+CnN0cmVhbQogIEJUCiAgICAvRjEgMTggVGYKICAgIDAgMCBUZAogICAgKEhlbGxvIF" +
           "dvcmxkKSBUagogIEVUCmVuZHN0cmVhbQplbmRvYmoKCnhyZWYKMCA1CjAwMDAwMDAwMDAgN" +
           "jU1MzUgZiAKMDAwMDAwMDAxOCAwMDAwMCBuIAowMDAwMDAwMDc3IDAwMDAwIG4gCjAwMDAw" +
           "MDAxNzggMDAwMDAgbiAKMDAwMDAwMDQ1NyAwMDAwMCBuIAp0cmFpbGVyCiAgPDwgIC9Sb29" +
           "0IDEgMCBSCiAgICAgIC9TaXplIDUKICA+PgpzdGFydHhyZWYKNTY1CiUlRU9GCg==")
    }
 
} else {
    log.warn("Unable to retrieve pdf content. Not inserting into ORU.")
}

set("ZDS-1", aecg.get(XDSEntryUUID))

return true
