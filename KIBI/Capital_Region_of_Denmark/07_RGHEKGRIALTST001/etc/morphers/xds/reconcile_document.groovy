/**
 * This script is executed once per XDS document during auto-reconcile
 * workflow. The input is an instance of the DicomScriptingAPI wrapped
 * around the modality worklist entry that was selected upstream based
 * on the order matching rules. The output is an instance of the
 * DocumentMetadataScriptingAPI wrapped around the XDS document.
 *
 * This script can be used to update the document metadata in place,
 * using attributes of the matched DICOM modality worklist entry (the
 * order). Return true if the metadata has been updated, as this change
 * should be reflected in the registry. Return false if this script has
 * not made any modifications to the document metadata as this will
 * signal the workflow that no metadata update request need be sent.
 */

log.debug("Starting morpher [Reconcile Document via Worklist Entry]")

def modified = false

def xdsStudyUIDs = output.get(XDSExtendedMetadata('studyInstanceUid'))

def xdsStudyIUID = null
if (xdsStudyUIDs != null && !xdsStudyUIDs.isEmpty()) {
    xdsStudyIUID = xdsStudyUIDs[0]
}

log.debug("xdsStudyIUID = {}", xdsStudyIUID)

def dcmStudyIUID = input.get(StudyInstanceUID)

log.debug("dcmStudyIUID = {}", dcmStudyIUID)

if (xdsStudyIUID != dcmStudyIUID && dcmStudyIUID != null) {
    log.debug("Reconciled study instance UID from '{}' to '{}'", xdsStudyIUID, dcmStudyIUID)
    output.set(XDSExtendedMetadata('studyInstanceUid'), dcmStudyIUID)
    modified = true
}

def xdsOrderingPersons = output.get(XDSExtendedMetadata('orderingPerson'))

def xdsOrderingPerson = null
if (xdsOrderingPersons != null && !xdsOrderingPersons.isEmpty()) {
    xdsOrderingPerson = xdsOrderingPersons[0]
}

log.debug("xdsOrderingPerson = {}", xdsOrderingPerson)

def dcmOrderingPerson = input.get(OrderEnteredBy)

log.debug("dcmOrderingPerson = {}", dcmOrderingPerson)

if (xdsOrderingPerson != dcmOrderingPerson && dcmOrderingPerson != null) {
    log.debug("Reconciled ordering person from '{}' to '{}'", xdsOrderingPerson, dcmOrderingPerson)
    output.set(XDSExtendedMetadata('orderingPerson'), dcmOrderingPerson)
    modified = true
}

def xdsOrderingIDs = output.get(XDSExtendedMetadata('orderingID'))

def xdsOrderingID = null
if (xdsOrderingIDs != null && !xdsOrderingIDs.isEmpty()) {
    xdsOrderingID = xdsOrderingIDs[0]
}

log.debug("xdsOrderingID = {}", xdsOrderingID)

def dcmOrderingID = input.get(PlacerOrderNumberImagingServiceRequest)

log.debug("dcmOrderingID = {}", dcmOrderingID)

if (xdsOrderingID != dcmOrderingID && dcmOrderingID != null) {
    log.debug("Reconciled ordering ID from '{}' to '{}'",xdsOrderingID, dcmOrderingID)
    output.set(XDSExtendedMetadata('orderingID'), dcmOrderingID)
    modified = true
}

def xdsOrderingDepartmentSections = output.get(XDSExtendedMetadata('orderingDepartmentSection'))

def xdsOrderingDepartmentSection = null
if (xdsOrderingDepartmentSections != null && !xdsOrderingDepartmentSections.isEmpty()) {
    xdsOrderingDepartmentSection = xdsOrderingDepartmentSections[0]
}

log.debug("xdsOrderingDepartmentSection = {}", xdsOrderingDepartmentSection)

def dcmOrderingDepartmentSection = input.get(OrderEntererLocation)

log.debug("dcmOrderingDepartmentSection = {}", dcmOrderingDepartmentSection)

if (xdsOrderingDepartmentSection != dcmOrderingDepartmentSection && dcmOrderingDepartmentSection != null) {
    log.debug("Reconciled ordering department section from '{}' to '{}'",
        xdsOrderingDepartmentSection, dcmOrderingDepartmentSection)
    output.set(XDSExtendedMetadata('orderingDepartmentSection'), dcmOrderingDepartmentSection)
    modified = true
}

return modified
