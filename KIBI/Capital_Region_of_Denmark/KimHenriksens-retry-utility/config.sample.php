<?php

define('WORKFLOW_ITEM_CREATED_NOT_BEFORE', '2017-05-17');
define('WORKFLOW_ITEM_CREATED_NOT_AFTER', '2017-05-23');
define('WORKFLOW_ITEM_STATUS', 'FAILURE');
define('WORKFLOW_ITEM_RETRY_TASK_NO', 3); // "Create HL7 message from XDS Documents Task"
define('RIALTO_WEBUI', 'https://localhost:2525');
define('RIALTO_USERNAME', 'system.root');
define('RIALTO_PASSWORD', 'password');
define('RIALTO_REALM', 'DefaultSystemRealm');
define('COOKIE_JAR', '/tmp/rialto.cookiejar');
