<?php

/**
 * Retry task script 
 * 
 * This script will retry failed items by htting the right order of URLs 
 * in the web ui for a given work item
 * 
 * 1. Request a list of failed workflow items between 2017-05-17 and 2017-05-23  limited to 1 item
 * 3. Load that item page 
 * 4. Find the retry url for the failed task
 * 5. Retry the task by hitting the url
 *
 * @author	Kim Henriksen <khenriksen@vitalimages.com>
 */
include __DIR__ . DIRECTORY_SEPARATOR . '..' . DIRECTORY_SEPARATOR . 'vendor' . DIRECTORY_SEPARATOR . 'autoload.php';
include __DIR__ . DIRECTORY_SEPARATOR . '..' . DIRECTORY_SEPARATOR . 'config.php';

$login_url        = RIALTO_WEBUI . '/user_management/users/login';
$worflow_list_url = RIALTO_WEBUI . '/dataflows/workflows/datatable?sEcho=1&iColumns=7&sColumns=kh_column_status%2Ckh_column_type%2Ckh_column_patientId%2Ckh_column_universalId%2Ckh_column_accessionNumber%2Ckh_column_created%2Ckh_column_updated&iDisplayStart=0&iDisplayLength=1&mDataProp_0=0&mDataProp_1=1&mDataProp_2=2&mDataProp_3=3&mDataProp_4=4&mDataProp_5=5&mDataProp_6=6&iSortCol_0=6&sSortDir_0=desc&iSortingCols=1&bSortable_0=false&bSortable_1=false&bSortable_2=true&bSortable_3=true&bSortable_4=true&bSortable_5=true&bSortable_6=true&createdNotBefore=' . WORKFLOW_ITEM_CREATED_NOT_BEFORE . '&createdNotAfter=' . WORKFLOW_ITEM_CREATED_NOT_AFTER . '&modifiedNotBefore=&modifiedNotAfter=&patientId=&type=&accessionNumber=&createdDateRange=other&createdNotBefore_hour=0&createdNotBefore_minute=0&createdNotAfter_hour=0&createdNotAfter_minute=0&modifiedDateRange=select&modifiedNotBefore_hour=0&modifiedNotBefore_minute=0&modifiedNotAfter_hour=0&modifiedNotAfter_minute=0&universalId=2.16.124.113638.6.1.1.1&status=' . WORKFLOW_ITEM_STATUS . '&createdTime_basic=true&modifiedTime_basic=true&patientId_basic=true&type_basic=true&accessionNumber_basic=true&status_basic=true';

// Perform login
echo "Authenticating\n";
echo "---------------------------------------------------------------------\n";
$response = Httpful\Request::post($login_url)
    ->body(['user_id' => RIALTO_USERNAME, 'password' => RIALTO_PASSWORD, 'realm' => RIALTO_REALM], Httpful\Mime::FORM)
    ->addOnCurlOption(CURLOPT_COOKIEJAR, COOKIE_JAR)
    ->addOnCurlOption(CURLOPT_COOKIEFILE, COOKIE_JAR)
    ->send();
printf("%d GET: %s\n\n", $response->code, $login_url);

// Get worklist of failed items between 2017-05-17 and 2017-05-23 (we only request 1 item in the list)
echo "Getting failed workflow list\n";
echo "---------------------------------------------------------------------\n";
$response = Httpful\Request::get($worflow_list_url)
    ->addOnCurlOption(CURLOPT_COOKIEJAR, COOKIE_JAR)
    ->addOnCurlOption(CURLOPT_COOKIEFILE, COOKIE_JAR)
    ->expectsJson()
    ->send();
printf("%d GET: %s\n\n", $response->code, $worflow_list_url);

if (!empty($response->body->aaData[0]) AND
    $row    = $response->body->aaData[0] AND ! empty($row[0]) AND
    $column = $row[0] AND
    preg_match('/href=\'([^\']*)\'/', $column, $matches))
{
    // Get the work flow item uri
    $workflow_item_url = RIALTO_WEBUI . $matches[1];

    // Get the workflow item page
    echo "Getting failed workflow item\n";
    echo "---------------------------------------------------------------------\n";
    $response = Httpful\Request::get($workflow_item_url)
        ->addOnCurlOption(CURLOPT_COOKIEJAR, COOKIE_JAR)
        ->addOnCurlOption(CURLOPT_COOKIEFILE, COOKIE_JAR)
        ->expectsHtml()
        ->send();
    printf("%d GET: %s\n\n", $response->code, $workflow_item_url);

    // Parse the workflow item page and get the uri for the HL7 resend task
    $dom               = new PHPHtmlParser\Dom;
    $dom->load($response->body);
    $csrf_token        = $dom->find('meta[name=csrf-token]')->getAttribute('content');
    $tasks             = $dom->find('#kh_list_dataflowtasks tbody');
    $workflow_task_url = RIALTO_WEBUI . $tasks[WORKFLOW_ITEM_RETRY_TASK_NO]->getAttribute('data-taskurl');

    // Hit the task url
    echo "Retrying failed workflow task\n";
    echo "---------------------------------------------------------------------\n";
    $response = Httpful\Request::put($workflow_task_url)
        ->addOnCurlOption(CURLOPT_COOKIEJAR, COOKIE_JAR)
        ->addOnCurlOption(CURLOPT_COOKIEFILE, COOKIE_JAR)
        ->expectsJson()
        ->send();
    printf("%d PUT: %s\n\n", $response->code, $workflow_item_url);
    echo "Done\n";
    echo "Rialto response: " . $response->body->message . "\n";
}