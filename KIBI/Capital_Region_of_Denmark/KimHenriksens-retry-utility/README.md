# Rialto retry task

This script can retry a task for workflow items

## Usage

1. Rename config.sample.php to config.php and edit
2. composer install
3. php bin/retry_task.php

### Composer

Get [Composer](https://getcomposer.org/download)
