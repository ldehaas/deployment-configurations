<?xml version="1.0"?>
<config>

    <!-- ################################################################## -->
    <!--                                                                    -->
    <!--  Rialto Configuration                                              -->
    <!--                                                                    -->
    <!-- ################################################################## -->

    <!-- ################################################################## -->
    <!--  VARIABLES                                                         -->
    <!-- ################################################################## -->

    <var name="HostIP" value="${env.IP_ADDR_ETH0}" />

    <!-- Cassandra and HCP variables -->
    <!-- NOTE: these variables will be the same for Application Servers within the same data centre, but different for Application Servers at different data centres -->
    <include location="cassandra.hcp.cfg.xml"/>

    <!-- HCR - Status API call -->
    <var name="HealthCheckDicomPort" value="44444" />
    <var name="HC-AETitle" value="RIALTO_HC" />

    <!-- AUDIT RECORD REPOSITORY VARIABLES -->
    <var name="ARR-Protocol" value="udp" />
    <var name="ARR-GUIPort" value="7070" />
    <var name="ARR-Port" value="4000" />
    <var name="ARR-Host" value="localhost" />

    <!-- SYSTEM COMMON VARIABLES -->
    <var name="System-AffinityDomain" value="2.16.124.113638.1.1.1.0" />
    <var name="System-AffinityNamespace" value="HCR_EMPI" />
    <var name="System-DefaultLocalDomain" value="2.16.124.113638.1.1.1.1" />
    <var name="System-DefaultNamespace" value="MASTER" />
    <var name="System-UniversalIDType" value="ISO" />
    <var name="System-DefaultLocalFullyQualifiedDomain" value="${System-DefaultNamespace}&amp;${System-DefaultLocalDomain}&amp;${System-UniversalIDType}" />
    <var name="System-AffinityFullyQualifiedDomain" value="${System-AffinityNamespace}&amp;${System-AffinityDomain}&amp;${System-UniversalIDType}" />

    <var name="AGFA-IMPAX-LocalDomain" value="1.1.2" />
    <var name="SIEMENS-SYNGO-LocalDomain" value="1.1.3" />
    <var name="SECTRA-LocalDomain" value="1.1.5" />

    <!-- XDS REGISTRY VARIABLES -->
    <var name="XDSRegistry-Host" value="localhost"/>
    <var name="XDSRegistry-Port" value="8081"/>
    <var name="XDSRegistry-Path" value="/registry"/>
    <var name="XDSRegistry-URL" value="http://${XDSRegistry-Host}:${XDSRegistry-Port}${XDSRegistry-Path}"/>
    <var name="XDSRegistry-HL7v2Port" value="2397"/>
    <var name="XDSRegistry-MetadataUpdateEnabled" value="true"/>

    <!-- XDS REPOSITORY VARIABLES -->
    <var name="XDSRepository-Host" value="localhost" />
    <var name="XDSRepository-Port" value="8082" />
    <var name="XDSRepository-Path" value="/vault/repo" />
    <var name="XDSRepository-ID" value="2.16.840.1.113883.3.239.16.30001"/>
    <var name="XDSRepository-URL" value="http://${XDSRepository-Host}:${XDSRepository-Port}${XDSRepository-Path}"/>

    <!-- VAULT - IMAGE ARCHIVE VARIABLES -->
    <var name="IA-DicomPort" value="4104" />
    <var name="IA-AETitle" value="HCR" />
    <var name="UpdateStudies-HL7v2Port" value="2455" />
    <var name="ChangeAvailability-HL7v2Port" value="2456" />
    <var name="IA-StudyManagement-Path" value="/vault/studymanagement" />

    <!-- HL7Proxy VARIABLES -->
    <var name="HL7Proxy-Port" value="2399" />

    <!-- EMPI VARIABLES -->
    <var name="EMPI-Host" value="localhost" />
    <var name="EMPI-HL7v2Port" value="2398" />
    <var name="EMPI-SendingApplication" value="RIALTO_EMPI"/>
    <var name="EMPI-SendingFacility" value="KAROS_HEALTH"/>

    <!-- DATABASE URLs: "DatabaseURL" parameter name is used by the navigator-security script, do not change the parameter name  -->
    <var name="DatabaseURL" value="jdbc:postgresql://localhost/navigatordb?user=rialto"/>

    <!-- NAVIGATOR VARIABLES -->
    <var name="Navigator-Host" value="${HostIP}"/>
    <var name="Navigator-HostProtocol" value="https" />
    <var name="Navigator-GUIPort" value="2525" />
    <var name="Navigator-APIPort" value="2524" />
    <var name="Navigator-APIBasePath" value="/rialto" />
    <var name="Navigator-RootPath" value="/rest" />
    <var name="Navigator-GUIBasePath" value="/" />
    <var name="Navigator-DefaultLocale" value="en" />
    <var name="Navigator-DefaultTheme" value="hitachi-light" />
    <var name="Navigator-MaxPatientSearchResults" value="1000" />
    <var name="Navigator-MaxDocumentSearchResults" value="1000" />

    <!-- USERMANAGEMENT VARIABLES -->
    <var name="Usermanagement-APIBasePath" value="/usermanagement" />
    <var name="Usermanagement-URL" value="${Navigator-HostProtocol}://${Navigator-Host}:${Navigator-APIPort}${Navigator-RootPath}" />
    <var name="Usermanagement-AuthenticatedURL" value="${Navigator-HostProtocol}://${Navigator-Host}:${Navigator-APIPort}${Navigator-RootPath}/api${Usermanagement-APIBasePath}" />

    <!-- CLUSTER CONFIGURATION -->
    <!-- Single Sign On in a Cluster -->
    <var name="SSO-Cluster-Bind-IP" value="127.0.0.1"/>
    <var name="SSO-Cluster-Multicast-IP" value="232.10.10.201"/>
    <var name="SSO-Cluster-Multicast-PORT" value="45201"/>

    <!-- Navigator Cache Manager in a cluster -->
    <var name="Navigator-Cluster-Bind-IP" value="127.0.0.1"/>
    <var name="Navigator-Cluster-Multicast-IP" value="232.10.10.202"/>
    <var name="Navigator-Cluster-Multicast-PORT" value="45202"/>

    <!-- Navigator Server access controller in a cluster -->
    <var name="Navigator-Server-Cluster-Bind-IP" value="127.0.0.1"/>
    <var name="Navigator-Server-Cluster-Multicast-IP" value="232.10.10.203"/>
    <var name="Navigator-Server-Cluster-Multicast-PORT" value="45203"/>

    <!-- TLS CONFIGURATION -->
    <var name="TLSKeyStore" value="sample.jks" />
    <var name="TLSKeyStorePass" value="password" />
    <var name="TLSKeyPass" value="password" />
    <var name="TLSTrustStore" value="sample.jks" />
    <var name="TLSTrustStorePass" value="password" />
    <var name="TLSClientAuth" value="false" />
    <var name="TLSV2Hello" value="true" />

    <!-- User Sessions -->
    <var name="IdleUserSessionTimeout" value="30m" />
 

    <!-- Database Backup -->
    <var name="BackupRemotePath" value="/home/rialto/rialto/var/database-backups" />
    <var name="BackupMaxSnapshots" value="5" />

    <!-- ################################################################## -->
    <!--  SERVERS                                                           -->
    <!-- ################################################################## -->
    <server type="http" id="authenticated-http">
        <port>${Navigator-APIPort}</port>
        <tls>
            <keystore>${TLSKeyStore}</keystore>
            <keystorepass>${TLSKeyStorePass}</keystorepass>
            <keypass>${TLSKeyPass}</keypass>
            <truststore>${TLSTrustStore}</truststore>
            <truststorepass>${TLSTrustStorePass}</truststorepass>
            <clientauth>${TLSClientAuth}</clientauth>
            <v2hello>${TLSV2Hello}</v2hello>
        </tls>
    </server>

    <server type="http" id="navigator-http">
        <port>${Navigator-GUIPort}</port>
        <tls>
            <keystore>${TLSKeyStore}</keystore>
            <keystorepass>${TLSKeyStorePass}</keystorepass>
            <keypass>${TLSKeyPass}</keypass>
            <truststore>${TLSTrustStore}</truststore>
            <truststorepass>${TLSTrustStorePass}</truststorepass>
            <clientauth>${TLSClientAuth}</clientauth>
            <v2hello>${TLSV2Hello}</v2hello>
        </tls>
    </server>

    <server type="http" id="audit-http">
        <port>${ARR-GUIPort}</port>
        <tls>
            <keystore>${TLSKeyStore}</keystore>
            <keystorepass>${TLSKeyStorePass}</keystorepass>
            <keypass>${TLSKeyPass}</keypass>
            <truststore>${TLSTrustStore}</truststore>
            <truststorepass>${TLSTrustStorePass}</truststorepass>
            <clientauth>${TLSClientAuth}</clientauth>
            <v2hello>${TLSV2Hello}</v2hello>
        </tls>
    </server>

    <server type="http" id="registry-http">
        <port>${XDSRegistry-Port}</port>
    </server>

    <server type="http" id="repo-http">
        <port>${XDSRepository-Port}</port>
    </server>

    <!-- Web server - Used for MINT, Study Validation and Sync -->
    <server type="http" id="http8080">
       <port>8080</port>
    </server>

    <server type="hl7v2" id="empi-hl7">
        <port>${EMPI-HL7v2Port}</port>
    </server>

    <server type="hl7v2" id="ia-update-hl7">
        <port>${UpdateStudies-HL7v2Port}</port>
    </server>

    <server type="hl7v2" id="ia-study-delete-hl7">
        <port>${ChangeAvailability-HL7v2Port}</port>
    </server>

    <server type="hl7v2" id="registry-hl7">
        <port>${XDSRegistry-HL7v2Port}</port>
    </server>

    <!-- NON-TLS DICOM server for Vault -->
    <server type="dicom" id="vault-dicom">
        <port>${IA-DicomPort}</port>
    </server>

    <!-- Server for the HL7 Proxy -->
    <server id="hl7ProxyReceiverServer" type="hl7v2">
        <port>${HL7Proxy-Port}</port>
    </server>

    <!-- ################################################################## -->
    <!--  SERVICES                                                          -->
    <!-- ################################################################## -->

    <!-- Health Checker service for load balancer. -->
    <service id="rialto-health-check" type="rialto-health-check">
        <server idref="navigator-http" name="web-api">
            <url>/rialto/*</url>
        </server>

        <config>
            <prop name="HealthCheckAETitle">${HC-AETitle}</prop>
            <prop name="RialtoAETitle">${IA-AETitle}</prop>

            <prop name="UsermanagementApiURL">${Usermanagement-URL}</prop>

            <prop name="WebServicesUserId">admin</prop>
            <prop name="WebServicesPassword">admin</prop>

            <prop name="TestStudyInstanceUID">1.2.40.0.13.1.10.0.2.115.28380482.20091203145432563.1</prop>

            <prop name="HealthCheckDicomPort">${HealthCheckDicomPort}</prop>

            <prop name="RialtoDicomPort">${IA-DicomPort}</prop>

            <prop name="HcpConfiguration">
                <HcpNodeConfiguration>
                    <HcpUrl>${HCPURL}</HcpUrl>
                    <HcpHost>${HCPHost}</HcpHost>
                    <HcpUsername>${HCPUsername}</HcpUsername>
                    <HcpPassword>${HCPPassword}</HcpPassword>                
                </HcpNodeConfiguration>

                <HttpConnectionConfiguration>
                    <poolMaxConnectionsPerHost>25</poolMaxConnectionsPerHost>
                    <poolMaxTotalConnections>100</poolMaxTotalConnections>
                </HttpConnectionConfiguration>
            </prop>

            <prop name="ConsecutiveFailureTolerance">2</prop>
            <prop name="CMoveTimeout">10s</prop>

            <prop name="HCPTestDirectory">vault_imaging</prop>

        </config>
    </service>

    <!-- Cassandra Startup Service should be the first service to be started. -->
    <service id="cassandra-startup-service" type="cassandra-startup-service">
        <config>
            <prop name="CassandraConfiguration">
                <clusterHosts>${CassandraClusterHosts}</clusterHosts>
                <clusterDatacenterName>${CassandraClusterDatacenterName}</clusterDatacenterName>
                <keyspaceName>${CassandraKeyspacePrefix}startuptest</keyspaceName>
                <keyspaceReplicationStrategy>NetworkTopologyStrategy</keyspaceReplicationStrategy>
                <keyspaceReplicationStrategyOptions>${CassandraReplicationOption}</keyspaceReplicationStrategyOptions>
                <consistencyLevel>LOCAL_QUORUM</consistencyLevel>
                <serialConsistencyLevel>LOCAL_SERIAL</serialConsistencyLevel>
                <keyspaceCreateEnabled>true</keyspaceCreateEnabled>
                <dynamicConsistencyLevelEnabled>false</dynamicConsistencyLevelEnabled>
                <schemaUpdateEnabled>false</schemaUpdateEnabled>
            </prop>
        </config>
    </service>

    <!-- EVENT COLLECTOR SERVICE -->
    <service id="TransactionRecordsServiceForEventCollector" type="txn-records">
        <config>
            <prop name="StorageDir">${rialto.rootdir}/var/txn_records</prop>
            <prop name="Purge">14d</prop>
            <prop name="PurgeInterval">1d</prop>
            <prop name="OptimizeOnStartup">true</prop>
        </config>
    </service>

    <!-- AUDIT RECORD REPOSITORY SERVICE -->
    <service id="arr" type="arr2">
        <tls name="repository">
            <keystore>samplePrivateKeyA.jks</keystore>
            <truststore>samplePublicKeyA.jks</truststore>
            <clientauth>true</clientauth>
        </tls>

        <server idref="audit-http" name="arr-api">
            <url>/auditRecords/*</url>
        </server>

        <config>
            <prop name="Listener" value="${ARR-Protocol}:${ARR-Port}"/>
            <prop name="CassandraConfiguration">
                <clusterHosts>${CassandraClusterHosts}</clusterHosts>
                <clusterDatacenterName>${CassandraClusterDatacenterName}</clusterDatacenterName>
                <keyspaceName>${CassandraKeyspacePrefix}audit</keyspaceName>
                <keyspaceReplicationStrategy>NetworkTopologyStrategy</keyspaceReplicationStrategy>
                <keyspaceReplicationStrategyOptions>${CassandraReplicationOption}</keyspaceReplicationStrategyOptions>
                <consistencyLevel>LOCAL_QUORUM</consistencyLevel>
                <serialConsistencyLevel>LOCAL_SERIAL</serialConsistencyLevel>
                <keyspaceCreateEnabled>true</keyspaceCreateEnabled>
                <dynamicConsistencyLevelEnabled>false</dynamicConsistencyLevelEnabled>
                <schemaUpdateEnabled>false</schemaUpdateEnabled>
            </prop>
        </config>
    </service>

    <!-- NAVIGATOR (Including Usermanagement and plugins) -->
    <include location="navigator/navigator.xml"/>

    <!-- VAULT - IMAGE ARCHIVE -->
    <include location="vault/image-archive.xml"/>

    <!-- VAULT - XDS REPOSITORY -->
    <include location="vault/repo.xml"/>

    <!-- RIALTO EMPI SERVICE -->
    <service id="empi" type="empi">
        <server idref="empi-hl7" name="pif" />
        <server idref="empi-hl7" name="merge" />
        <server idref="empi-hl7" name="pix" />
        <server idref="empi-hl7" name="pdq" />

        <config>
            <prop name="SendingApplication" value="${EMPI-SendingApplication}"/>
            <prop name="SendingFacility" value="${EMPI-SendingFacility}"/>
            <prop name="HL7Morpher" value="${rialto.rootdir}/etc/morphers/empi/hl7_morpher.groovy"/>
            <prop name="CrossAffinityDomain" value="${System-AffinityFullyQualifiedDomain}" />
            <prop name="QueryLimit" value="3000" />

            <!-- KHC1673 -->
            <prop name="MaxLockRetries" value="200" />

            <!-- new variables in HCR 4.3 -->
            <prop name="OutBoundHL7Morpher" value="${rialto.rootdir}/etc/morphers/empi/outbound_hl7_morpher.groovy"/>

            <!-- When set to true, only domains that are preconfigured will be accepted by the EMPI in patient feeds and queries.
                 When false, domains will be added to the database as they are observed. -->
            <prop name="IsStrictDomainCheckingEnabled" value="true"/>

            <!-- When set to false, the affinity domain id will not be returned in queries. When set to true, the regional
                 id and afinity domain will be return in the patient identifier list in the response to a query. -->
            <prop name="IsCrossAffinityDomainVisible" value="false"/>
            
            <include location="empi/identitysources.xml" />          <!-- The identity sources to accept patient feeds from -->
            <include location="empi/hl7v2demographics.xml" />        <!-- What and how to parse the values from HL7v2 messages -->
            <include location="empi/pixupdatedestinations.xml" />    <!-- Destinations for PIX update notifications -->

            <prop name="CassandraConfiguration">
                <clusterHosts>${CassandraClusterHosts}</clusterHosts>
                <clusterDatacenterName>${CassandraClusterDatacenterName}</clusterDatacenterName>
                <keyspaceName>${CassandraKeyspacePrefix}empi</keyspaceName>
                <keyspaceReplicationStrategy>NetworkTopologyStrategy</keyspaceReplicationStrategy>
                <keyspaceReplicationStrategyOptions>${CassandraReplicationOption}</keyspaceReplicationStrategyOptions>
                <consistencyLevel>LOCAL_QUORUM</consistencyLevel>
                <serialConsistencyLevel>LOCAL_SERIAL</serialConsistencyLevel>
                <keyspaceCreateEnabled>true</keyspaceCreateEnabled>
                <dynamicConsistencyLevelEnabled>false</dynamicConsistencyLevelEnabled>
                <schemaUpdateEnabled>false</schemaUpdateEnabled>
            </prop>
       </config>
    </service>

    <service id="xdsreg" type="xdsreg">
        <server idref="registry-http">
            <url>/registry</url>
        </server>

        <server idref="registry-hl7" />

        <config>
            <prop name="CassandraConfiguration">
                <clusterHosts>${CassandraClusterHosts}</clusterHosts>
                <clusterDatacenterName>${CassandraClusterDatacenterName}</clusterDatacenterName>
                <keyspaceName>${CassandraKeyspacePrefix}xdsreg</keyspaceName>
                <keyspaceReplicationStrategy>NetworkTopologyStrategy</keyspaceReplicationStrategy>
                <keyspaceReplicationStrategyOptions>${CassandraReplicationOption}</keyspaceReplicationStrategyOptions>
                <consistencyLevel>LOCAL_QUORUM</consistencyLevel>
                <serialConsistencyLevel>LOCAL_SERIAL</serialConsistencyLevel>
                <keyspaceCreateEnabled>true</keyspaceCreateEnabled>
                <dynamicConsistencyLevelEnabled>false</dynamicConsistencyLevelEnabled>
                <schemaUpdateEnabled>false</schemaUpdateEnabled>
            </prop>
            <prop name="AffinityDomain" value="${System-DefaultLocalDomain}"/>
            <prop name="AffinityDomainNamespace" value="${System-DefaultNamespace}"/>
            <prop name="PatientValidationMode" value="ACCEPT_ALL"/>
            <prop name="SupportDeleteDocumentSet" value="true"/>
            <prop name="ExtendedMetadata" value="true"/>
            <prop name="PatientValidationMode" value="AFFINITY_DOMAIN"/>
        </config>
    </service>

    <service id="HL7Proxy" type="hl7-proxy">
        <server idref="hl7ProxyReceiverServer" />

        <config>
            <prop name="Destinations">
                <!-- Forward to EMPI -->
                <destination>
                    <facility>CARILION</facility>
                    <application>EMPI_PIF</application>
                    <script>${rialto.rootdir}/etc/morphers/HL7ProxyScripts/EMPI_forward_rules.groovy</script>
                </destination>

                <!-- Forward to Image Archive Updates -->
                <destination>
                    <facility>CARILION</facility>
                    <application>IA</application>
                    <script>${rialto.rootdir}/etc/morphers/HL7ProxyScripts/IA_UPDT_forward_rules.groovy</script>
                </destination>

                 <!-- Foward to XDS Registry -->
                <destination>
                    <facility>CARILION</facility>
                    <application>XDS_REG</application>
                    <script>${rialto.rootdir}/etc/morphers/HL7ProxyScripts/XDSReg_forward_rules.groovy</script>
                </destination>
            </prop>

            <prop name="ResponseMorpher" value="${rialto.rootdir}/etc/morphers/HL7ProxyScripts/hl7_proxy_response_morpher.groovy" />

        </config>
    </service>


    <!-- ################################################################## -->
    <!--  DEVICES                                                           -->
    <!-- ################################################################## -->

    <!-- AUDIT DEVICE -->
    <device type="audit" id="audit">
        <transport>${ARR-Protocol}</transport>
        <tls>
            <keystore>samplePrivateKeyA.jks</keystore>
            <truststore>samplePublicKeyA.jks</truststore>
        </tls>
        <host>${ARR-Host}</host>
        <port>${ARR-Port}</port>
    </device>

    <!-- EMPI PDQ DEVICE -->
    <device id="pdq" type="pdq">
        <protocol>v2</protocol>
        <host>${EMPI-Host}</host>
        <port>${EMPI-HL7v2Port}</port>
        <sendingApplication>EMPI_PDQ</sendingApplication>
        <sendingFacility>CARILION</sendingFacility>
        <receivingApplication>EMPI_PDQ</receivingApplication>
        <receivingFacility>CARILION</receivingFacility>
        <resultsPerQuery>25</resultsPerQuery>
        <timeout>60s</timeout>
    </device>

    <!-- EMPI PIX DEVICE -->
    <device type="pix" id="pix">
        <protocol>v2</protocol>
        <host>${EMPI-Host}</host>
        <port>${EMPI-HL7v2Port}</port>
        <sendingApplication>EMPI_PIX</sendingApplication>
        <sendingFacility>CARILION</sendingFacility>
        <receivingApplication>EMPI_PIX</receivingApplication>
        <receivingFacility>CARILION</receivingFacility>
        <timeout>60s</timeout>
    </device>

    <!-- EMPI PIF -->
    <device type="hl7v2" id="pif">
        <protocol>v2</protocol>
        <host>${EMPI-Host}</host>
        <port>${EMPI-HL7v2Port}</port>
        <sendingApplication>IMG_AVAIL</sendingApplication>
        <sendingFacility>HCR</sendingFacility>
        <receivingApplication>EMPI_PIF</receivingApplication>
        <receivingFacility>CARILION</receivingFacility>
        <timeout>60s</timeout>
    </device>

    <!-- XDS Registry HL7 device -->
    <device type="hl7v2" id="hl7xdsreg">
        <protocol>v2</protocol>
        <host>${XDSRegistry-Host}</host>
        <port>${XDSRegistry-HL7v2Port}</port>
        <receivingApplication>XDS_REG</receivingApplication>
        <receivingFacility>CARILION</receivingFacility>
        <timeout>60s</timeout>
    </device>

    <!-- IA Update -->
    <device type="hl7v2" id="vault-hl7-updt">
        <host>${HostIP}</host>
        <port>${UpdateStudies-HL7v2Port}</port>
        <sendingApplication>IA</sendingApplication>
        <sendingFacility>CARILION</sendingFacility>
        <receivingApplication>IA</receivingApplication>
        <receivingFacility>CARILION</receivingFacility>
        <timeout>300s</timeout>
    </device>

    <device type="hl7v2" id="vault-hl7-delete">
        <host>${HostIP}</host>
        <port>${ChangeAvailability-HL7v2Port}</port>
        <sendingApplication>IA</sendingApplication>
        <sendingFacility>CARILION</sendingFacility>
        <receivingApplication>IA</receivingApplication>
        <receivingFacility>CARILION</receivingFacility>
        <timeout>20s</timeout>
    </device>

    <!-- XDS REPOSITORY DEVICE -->
    <device type="xdsrep" id="xdsrep">
        <uniqueid>${XDSRepository-ID}</uniqueid>
        <url>${XDSRepository-URL}</url>
    </device>

    <!-- XDS REGISTRY DEVICE -->
    <device type="xdsreg" id="xdsreg">
        <url>${XDSRegistry-URL}</url>
        <affinitydomain>${System-DefaultLocalDomain}</affinitydomain>
    </device>

    <device type="dicom" id="rialto-image-archive">
        <host>localhost</host>
        <port>${IA-DicomPort}</port>
        <aetitle>${IA-AETitle}</aetitle>
        <domain>1.2.3</domain>
     </device>

     <device type="dicom" id="rialto-health-check">
        <host>localhost</host>
        <port>${HealthCheckDicomPort}</port>
        <aetitle>${HC-AETitle}</aetitle>
        <domain>1.2.3</domain>
     </device>

    <!--SCN Notification Device-->
    <device id="epic-emr" type="hl7v2">
        <host>ieprd.carilion.com</host>
        <port>21611</port>
        <sendingApplication>IMG_AVAIL</sendingApplication>
        <sendingFacility>HCR</sendingFacility>
        <receivingApplication>HYPERSPACE</receivingApplication>
        <receivingFacility>CARILION</receivingFacility>
    </device>

    <!-- External DICOM devices that Rialto can send to -->
    <include location="vault/external.dicom.devices.cfg.xml"/>

</config>
