log.info("Begin instanceAvailabilityFilter morpher")

log.info("instanceAvailabilityFilter Source CFIND result: {}", source)
log.info("instanceAvailabilityFilter Target CFIND result: {}", target)

source_num_series = source.get("NumberOfStudyRelatedSeries")
target_num_series = target.get("NumberOfStudyRelatedSeries")

log.info("instanceAvailabilityFilter Number of series in source: {}", source_num_series)
log.info("instanceAvailabilityFilter Number of series in target: {}", target_num_series)

if (source_num_series <= target_num_series) {
    log.info("instanceAvailabilityFilter Source number of series is less that or equal to target number of series - not prefetching")
    return false
}

