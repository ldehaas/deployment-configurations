/**
 * This script is executed once per HL7 (SCN; ORU) message at the end of
 * XDS auto-reconcile workflow. The message is modified in place before
 * being sent to the configured SCN destination. Return false if the
 * message should not be sent.
 *
 * Note: this script uses the familiar HL7ScriptingAPI and the message
 * modified in-place can be accessed using 'input' or 'output' fields.
 */
 
log.debug("Starting morpher [Morph Outbound SCN Message]")
