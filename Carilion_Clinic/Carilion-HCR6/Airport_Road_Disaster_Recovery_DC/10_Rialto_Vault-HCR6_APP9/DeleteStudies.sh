# Delete duplicate studies from HCR6's HCP tenant
# Written by: Jason Yip
# Date: 2019-01-06

#!/bin/bash

IFS=","
filepath="/tmp/commonStudies"
filename="common-studies-in-both-HCR4-HCR6.txt"
today=$(date +%Y-%m-%d)

while read f1 f2 f3
do
    
    # PHYSICAL DELETION of studies
    curl http://localhost:8080/vault/mint/studies/$f1 -X DELETE -v
    echo \

done < $filepath/$filename
