def scriptName = "SCN Connect Postfetch morpher - "

log.info(scriptName + "START")

initialize( 'ORM', 'O01', '2.3' );
output.getMessage().addNonstandardSegment('IPC')
log.debug("Prefetch SCN Calling AE Title is {}", getCallingAETitle());
log.debug("Prefetch Called AE Title is {}", getCalledAETitle());

log.debug("SCN type is: {}",type)
if (type != "ARCHIVE") {
    log.debug("SCN type is not ARCHIVE, not sending message to HCR4 for prefetching")
    return false
    }

def callingAET = getCallingAETitle()
if (callingAET == 'HCR' || callingAET == 'SYNGO-TST-SQL01' || callingAET == 'SYNGO-PRD-SQL01'  || callingAET == 'DATAFIRST' || callingAET == 'DATAFIRST_GE') {
    log.debug ("Calling AE Title is {}, not sending prefetch SCN message for a HCR4 prefetch study",getCallingAETitle())
    return false
    }

//
set('MSH-3', 'HCR6')       //SendingApplication
set('MSH-4', 'HCR')        //SendingFacility
set('MSH-5', "CONNECT")    //ReceivingApplication   
set('MSH-6', "CARILION")   //ReceivingFacility
set('MSH-7', '20130827132217')
//
setPersonName('PID-5', input.get(PatientName));
set('PID-3-1', input.get(PatientID));

//set('PID-3-4', 'MASTER')

// MASTER&2.16.124.113638.1.1.1.1&ISO
set('PID-3-4-1', 'MASTER')
set('PID-3-4-2', '2.16.124.113638.1.1.1.1')
set('PID-3-4-3', 'ISO')
set('PID-3-5', 'MPI')
//
set('ORC-1', 'XO')
//
set('OBR-1', '1')
set('OBR-3', input.get(AccessionNumber))
//
// Hardcoding values for OBR-4-1 and 4-3 
set('OBR-4-1', 'RXR')
//
set('OBR-4-2', input.get(StudyDescription))
set('OBR-4-3', 'RMH')
//
set('OBR-16-9', 'SMART')
set('OBR-16-13', 'PROV')
set('OBR-25', 'I')
//
set('OBX-1', '1')
set('OBX-2', 'TX')
set('OBX-3-1-2', 'GDT')
set('OBX-5', 'Y')
set('OBX-9', input.get(StudyInstanceUID))

log.debug(scriptName + "output is \n{}", output)

log.info(scriptName + "END")
