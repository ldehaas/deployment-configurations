<?xml version="1.0" encoding="UTF-8"?>
<!DOCTYPE log4j:configuration SYSTEM "log4j.dtd">
<log4j:configuration xmlns:log4j="http://jakarta.apache.org/log4j/" debug="false">
    <!-- NB: do not use log4j's ConsoleAppender here.  Rialto redirects stdout
        and stderr to the logging system and printing logs back to stdout creates
        a loop. -->

    <appender name="MASTER_LOG" class="org.apache.log4j.RollingFileAppender">
        <param name="File" value="${rialto.logdir}/rialto.log" />
        <param name="Append" value="true" />
        <param name="MaxFileSize" value="200MB" />
        <param name="MaxBackupIndex" value="100" />
        <layout class="org.apache.log4j.PatternLayout">
            <param name="ConversionPattern" value="%d,%p,%c - [%t] %m\n" />
        </layout>
    </appender>

    <appender name="CONNECT_LOG" class="org.apache.log4j.RollingFileAppender">
        <param name="File" value="${rialto.logdir}/connect.log" />
        <param name="Append" value="true" />
        <param name="MaxFileSize" value="200MB" />
        <param name="MaxBackupIndex" value="10" />
        <layout class="org.apache.log4j.PatternLayout">
            <param name="ConversionPattern" value="%d,%p,%c - [%t] %m\n" />
        </layout>
    </appender>

    <appender name="DICOM_LOG" class="org.apache.log4j.RollingFileAppender">
        <param name="File" value="${rialto.logdir}/dicom.log" />
        <param name="Append" value="true" />
        <param name="MaxFileSize" value="200MB" />
        <param name="MaxBackupIndex" value="10" />
        <layout class="org.apache.log4j.PatternLayout">
            <param name="ConversionPattern" value="%d,%p,%c - [%t] %m\n" />
        </layout>
    </appender>

    <!-- Uncomment (1 of 2) to use local instance of Logstash -->

    <!-- delete-line-to-enable-logstash
    <appender name="LOGSTASH" class="org.apache.log4j.AsyncAppender">
      <param name="Blocking" value="false" />
      <param name="BufferSize" value="500" />
      <appender-ref ref="AppendRemote" />
    </appender>

    <appender name="AppendRemote" class="org.apache.log4j.net.SocketAppender">
        <param name="RemoteHost" value="0.0.0.0" />
        <param name="ReconnectionDelay" value="60000" />
        <param name="Threshold" value="DEBUG" />
    </appender>
    delete-line-to-enable-logstash -->

    <appender name="CQL3" class="org.apache.log4j.RollingFileAppender">
        <param name="File" value="${rialto.logdir}/cql3.log"/>
        <param name="Append" value="true"/>
        <param name="MaxFileSize" value="60MB"/>
        <param name="MaxBackupIndex" value="5"/>
        <!-- <layout class="org.apache.log4j.PatternLayout"> -->
        <layout class="com.karos.log4j.ColoredPatternLayout">
            <param name="ConversionPattern" value="%d,%p,%c - [%t] %m\n"/>
        </layout>
    </appender>

    <appender name="perf" class="org.apache.log4j.RollingFileAppender">
        <param name="File" value="${rialto.logdir}/rialto_perf.log" />
        <param name="Append" value="true" />
        <param name="MaxFileSize" value="25MB" />
        <param name="MaxBackupIndex" value="5" />
        <layout class="org.apache.log4j.PatternLayout">
            <param name="ConversionPattern" value="%d,%p,%c - %m\n" />
        </layout>
    </appender>

    <appender name="health-check" class="org.apache.log4j.RollingFileAppender">
        <param name="File" value="${rialto.logdir}/rialto_health_check.log" />
        <param name="Append" value="true" />
        <param name="MaxFileSize" value="25MB" />
        <param name="MaxBackupIndex" value="5" />
        <layout class="org.apache.log4j.PatternLayout">
            <param name="ConversionPattern" value="%d,%p,%c - [%t] %m\n" />
        </layout>
    </appender>

    <appender name="audit" class="org.apache.log4j.RollingFileAppender">
        <param name="File" value="${rialto.logdir}/rialto_audit.log" />
        <param name="Append" value="true" />
        <param name="MaxFileSize" value="100MB" />
        <param name="MaxBackupIndex" value="5" />
        <layout class="org.apache.log4j.PatternLayout">
            <param name="ConversionPattern" value="%d,%p,%c - [%t] %m\n" />
        </layout>
    </appender>

    <!-- Any services that want to back up CAStor tickets should log them here -->
    <appender name="tickets" class="org.apache.log4j.FileAppender">
        <!-- Note the location outside of the log directory.  This file should
             NOT be deleted, even if cleaning up the rest of the log files.
             There is important data in this file.  It should not roll over
             and delete old files. -->
        <param name="File" value="${rialto.rootdir}/var/rialto_tickets.log" />
        <param name="Append" value="true" />
        <layout class="org.apache.log4j.PatternLayout">
            <param name="ConversionPattern" value="%m\n" />
        </layout>
    </appender>

    <!-- logging used to count the number of studies that come in -->
    <appender name="metrics" class="org.apache.log4j.RollingFileAppender">
        <param name="File" value="${rialto.logdir}/rialto-performance-metrics.log" />
        <param name="Append" value="true" />
        <param name="MaxFileSize" value="50MB" />
        <param name="MaxBackupIndex" value="20" />
        <layout class="org.apache.log4j.PatternLayout">
            <param name="ConversionPattern" value="%d - %m\n" />
        </layout>
    </appender>

    <!-- 16558 - Appender created to record study deletions using cURL -->
    <appender name="deleted-studies" class="org.apache.log4j.RollingFileAppender">
        <param name="File" value="${rialto.logdir}/deleted-studies/deleted-studies.log" />
        <param name="Append" value="true" />
        <param name="MaxFileSize" value="200MB" />
        <param name="MaxBackupIndex" value="100" />
        <layout class="org.apache.log4j.PatternLayout">
            <param name="ConversionPattern" value="%d,%p,%c - %m\n" />
        </layout>
    </appender>

    <category name= "com.karos.rialto.ids.performance">
        <appender-ref ref="perf"/>
    </category>

    <category name= "com.karos.rialtohealthcheck.http.HealthCheckResource" additivity="false">
        <appender-ref ref="health-check"/>
    </category>

    <category name= "com.karos.arr2" additivity="false">
        <appender-ref ref="audit"/>
    </category>

    <category name= "service.arr" additivity="false">
        <appender-ref ref="audit"/>
    </category>

    <category name= "service.connect" additivity="false">
        <appender-ref ref="CONNECT_LOG"/>
    </category>

    <category name="org.dcm4che2.net.PDUEncoder">
        <priority value="debug" />
        <appender-ref ref="DICOM_LOG"/>
    </category>
    <category name="org.dcm4che2.net.PDUDecoder">
        <priority value="debug" />
        <appender-ref ref="DICOM_LOG"/>
    </category>

    <!-- all ticket logs should have their category "tickets" prefix -->
    <category name="tickets" additivity="false">
        <appender-ref ref="tickets" />
    </category>

    <!-- These libraries tend to be a bit chatty at info -->
    <category name="ca.uhn.hl7v2">
        <priority value="warn" />
    </category>
    <category name="com.mchange.v2">
        <priority value="warn" />
    </category>
    <category name="http.navigator-http">
        <priority value="warn" />
    </category>
    <category name="httpclient.wire.content">
        <priority value="warn" />
    </category>
    <category name="httpclient.wire.header">
        <priority value="warn"/>
    </category>
    <category name="navigator.blazeds">
        <priority value="warn" />
    </category>
    <category name="org.apache.commons.httpclient">
        <priority value="warn" />
    </category>
    <category name="org.dcm4che2.net">
        <priority value="warn" />
    </category>
    <category name="org.hibernate">
        <priority value="warn" />
    </category>
    <category name="org.openhealthtools.ihe">
        <priority value="warn" />
    </category>

    <!-- Make sure these libraries don't get into debug if we put root at debug
         because they spew way too much useless stuff -->
    <category name="org.apache.axiom">
        <priority value="info" />
    </category>
    <category name="org.apache.axis2">
        <priority value="info" />
    </category>
    <category name="org.mortbay.log">
        <priority value="info" />
    </category>
    <category name="org.apache.fop">
        <priority value="info" />
    </category>
    <category name="org.apache.http">
        <priority value="info" />
    </category>
    <category name="com.datastax.driver">
        <priority value="warn" />
    </category>
    <category name="io.searchbox">
        <priority value="info" />
    </category>

    <category name="org.openhealthtools.ihe.xds.metadata.extract.EbXML_3_0DocumentEntryExtractor">
        <!-- this class warns about extended metadata -->
        <priority value="ERROR" />
    </category>

    <category name="org.hibernate.ejb.Ejb3Configuration">
        <!-- this class warns about 
         'hibernate.connection.autocommit = false break the EJB3 specification' -->
        <priority value="ERROR" />
    </category>

    <category name="hl7v2.empi-hl7">
        <priority value="trace"/>
    </category>

    <category name="hl7v2.ia-update-hl7">
        <priority value="trace"/>
    </category>

    <category name="hl7v2.registry-hl7">
        <priority value="trace"/>
    </category>

    <category name="hl7v2.hl7ProxyReceiverServer">
        <priority value="trace"/>
    </category>

    <category name="com.karos.rtk.common.hl7v2.parser">
        <priority value="info"/>
    </category>

    <category name="com.datastax.driver" additivity="false">
        <priority value="warn"/>
        <appender-ref ref="CQL3"/>
    </category>

    <category name="com.karos.cql3" additivity="false">
        <priority value="debug"/>
        <appender-ref ref="CQL3"/>
    </category>

    <category name="rialto.ui.active-resource">
        <priority value="info" />
    </category>
    <category name="rialto.ui.admin-tools">
        <priority value="info" />
    </category>
    <category name="rialto.ui.assets">
        <priority value="warn" />
    </category>

<!--
    <category name="com.karos.ids">
        <priority value="trace" />
    </category>
    <category name="service">
        <priority value="trace" />
    </category>
    <category name= "service.ids1.cache.janitor">
        <priority value="debug" />
    </category>
-->

    <category name= "com.karos.rialto.imagearchive.performance.StopWatch">
        <appender-ref ref="metrics"/>
    </category>

    <!-- 16558 - Appender created to record study deletions using cURL -->
    <category name="service.rialto-image-archive.web.mint.studies" additivity="false">
        <appender-ref ref="deleted-studies"/>
    </category>

    <root>
        <priority value="debug" />
        <appender-ref ref="MASTER_LOG" />
        <!-- Uncomment (2 of 2) to use Logstash -->

        <!-- delete-line-to-enable-logstash
        <appender-ref ref="LOGSTASH" />
        delete-line-to-enable-logstash -->
    </root>
</log4j:configuration>
