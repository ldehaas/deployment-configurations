// This file is used to modify incoming messages

def messageType = get('MSH-9-1')
def triggerEvent = get('MSH-9-2')

if ("A40".equals(triggerEvent)) {
    log.info("EMPI MORPHER - Ignoring A40 messages")
    return false;
}
