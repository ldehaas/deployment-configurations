log.debug("in.groovy: Start inbound morpher")

set(IssuerOfPatientID, "MASTER")
set("IssuerOfPatientIDQualifiersSequence/UniversalEntityID", '2.16.124.113638.1.1.1.1')
set("IssuerOfPatientIDQualifiersSequence/UniversalEntityIDType", 'ISO')

log.debug("in.groovy: End inbound morpher")

return true
