def findECG(mimeType) {
    return xdsinput.find { document ->
        def format = document.get(XDSFormatCode)
        return format.getCodeValue() == "93010" &&
               format.getSchemeName() == "CPT-4" &&
               mimeType.equals(document.get(XDSMimeType))
    }
}
 
def aecg = findECG("text/xml")
 
if (aecg == null) {
    aecg = findECG("application/xml")
}
 
if (aecg == null) {
    log.debug("aECG was not found - skipping MWL query")
    return false
}
 
dcmquery.set(PatientID, aecg.get(XDSSourcePatientID).pid)
dcmquery.set("IssuerOfPatientIDQualifiersSequence/UniversalEntityID",
    aecg.get(XDSSourcePatientID).domainUUID, VR.UT)
 
return true
