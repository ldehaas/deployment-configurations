class Carilion {

    def log = null;
    def message = null;

    def Carilion(log, message) {
        this.log = log;
        this.message = message;
    }

    def forceSendingAppAndFac() {
        log.info("CARILION MORPHER - Forcing sending app and facility to be Carilion specific (Epic/MASTER)");

        message.set('/.MSH-3', 'Epic');
        message.set('/.MSH-4', 'CARILION');
    }

    def groomUpdateMessage(triggerEvent) {
        if ("A31".equals(triggerEvent)) {
            log.info("CARILION MORPHER - Converting A31 message to A08");
            message.set('/.MSH-9-2', 'A08');
        }
    }

    def groomMergeMessage(triggerEvent) {
        if ("A39".equals(triggerEvent)) {
            log.info("CARILION MORPHER - Converting A39 message to A40");
            message.set('/.MSH-9-2', 'A40');
            message.set('/.MSH-12', '2.3.1');
            reformatMergePids();
        }
    }

    def reformatMergePids() {
        def MPI_IDENTIFIER_TYPE = 'MPI';
        def identifierType = message.get('/.MRG-1-5');
        if (!MPI_IDENTIFIER_TYPE.equalsIgnoreCase(identifierType)) {

            def patientIds = null;
            try {
                patientIds = message.getList('/.MRG-1(*)-1');
            } catch (Exception e) {
                log.warn("CARILION MORPHER - Problem getting list of patient identifiers to subsume", e);
            }

            for (int i = 0; i < patientIds.size(); i++) {
                def currIdentifierType = message.get('/.MRG-1(' + i + ')-5');
                if (MPI_IDENTIFIER_TYPE.equalsIgnoreCase(currIdentifierType)) {
                    // Copy MPI identifier into first repetition
                    message.set('/.MRG-1-1', message.get('/.MRG-1(' + i + ')-1'));
                    message.set('/.MRG-1-2', message.get('/.MRG-1(' + i + ')-2'));
                    message.set('/.MRG-1-3', message.get('/.MRG-1(' + i + ')-3'));
                    message.set('/.MRG-1-4-1', message.get('/.MRG-1(' + i + ')-4-1'));
                    message.set('/.MRG-1-4-2', message.get('/.MRG-1(' + i + ')-4-2'));
                    message.set('/.MRG-1-4-3', message.get('/.MRG-1(' + i + ')-4-3'));
                    message.set('/.MRG-1-5', message.get('/.MRG-1(' + i + ')-5'));
                }

                // Clear out patient identifier
                message.set('/.MRG-1(' + i + ')-1', '');
                message.set('/.MRG-1(' + i + ')-2', '');
                message.set('/.MRG-1(' + i + ')-3', '');
                message.set('/.MRG-1(' + i + ')-4-1', '');
                message.set('/.MRG-1(' + i + ')-4-2', '');
                message.set('/.MRG-1(' + i + ')-4-3', '');
                message.set('/.MRG-1(' + i + ')-5', '');
            }
        }
    }

    def groom(triggerEvent) {
        forceSendingAppAndFac();
        groomUpdateMessage(triggerEvent);
        groomMergeMessage(triggerEvent);
    }

}
