/*
 * HL7 to cfind morpher
 * Convert an order message into a cfind message.
 * get() works on the order message, while set() works on the 
 * cfind message.
 * 
 * The order is ignored if this script returns false,
 * e.g. no images will be fetched for the order.
 */

def scriptName="Connect HL7 to C-Find Morpher - ";

log.info(scriptName + "Start HL7 to CFind morpher")

log.info(scriptName + "Incoming Message {}\n",input)

def pid = get("PID-3-1")

def namespace = get("PID-3-4-1")
def universalID = get("PID-3-4-2")
def universalType = get("PID-3-4-3")

if (namespace == null) {
    namespace = ""
}
if (universalID == null) {
    universalID = ""
}
if (universalType == null) {
    universalType = ""
}
log.info(scriptName + "found pid '{}' and issuer '{}&{}&{}'", pid, namespace, universalID, universalType)
issuer = namespace+"&"+universalID+"&"+universalType
log.info(scriptName + "the issuer is '{}'", issuer)

// no need to check pid == null, it's handed earlier in the workflow

// filter out out-of-province patients
if (pid.startsWith("000") || pid.startsWith("111")) {
    log.debug(scriptName + "Pid '{}' is from out-of-province.  Ignoring order message.")
    return false;
}


// construct the cfind
set(PatientID, pid)
set(IssuerOfPatientID, issuer)
set("NumberOfStudyRelatedSeries", null)
//set ( StudyDescription, get( "OBR-4" ))

log.info(scriptName + "End HL7 to CFind morpher")
