/* CFind Response RANK Filter
 *
 *   This morpher is part of the Fetch Prior Studies workflow only.
 *
 *   It takes THE ENTIRE SET of individual study responses (CFind Responses from the DIR) and optionally
 *   filters it out studies based on criteria, such as study age, or a maximum number of responses.
 *   This limits the number of studies that are preloaded into a local PACS as part of a Fetch Prior Studies workflow.
 *
 *   Common Uses for this Cfind Response Filter script can be:_
 *
 *       - Exclude studies that are NOT the X most recent studies (i.e. absolute number of responses, by age)
 */

log.debug("Starting cfind_response_rank_filter")

// C-FIND_Response_Rank_Filter
//
// HDIRS Requirements (from the SADD):
//  5 most recent studies up to 3 years old for all but MG studies
//  5 most recent studies up to 5 years old for MG studies
//

// Default to returning the last 3 years of studies
def requestedModality = "unknown"
def studyYears = 40
/*
if (null == order) {

    log.warn("C-FIND response rank filter has no order.  See CONNECT-707.  Wrong version of Rialto Connect?")

} else {

    // If the modality is MG, instead we need the last 5 years of studies
    requestedModality = order.get("OBR-24")
    if (requestedModality == "MG") {

        studyYears = 40
    }
}
*/

def validPriorList = []
def earliestStudyDate =  new org.joda.time.DateTime().minusYears(studyYears)
inputs.each {

    if (it.getDate(StudyDate, StudyTime) < earliestStudyDate) {

        log.trace("Prior for modality {} falls outside of maximum age {}", requestedModality, earliestStudyDate)

    } else {

        // Add it to the list of studies that are within the correct date range
        log.trace("Prior for modality {} falls inside of maximum age {}", requestedModality, earliestStudyDate)
        validPriorList.add(it)
    }
}

validPriorList.sort(byRecency)

return validPriorList // Return the whole list for now
//return first(validPriorList, 5)         // Last 5 studies in the last 3 or 5 years

// End cfind_response_rank_filter.groovy
