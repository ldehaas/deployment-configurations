<?xml version="1.0"?>
<config>

    <server type="dicom" id="dicom-main">
        <port>${IA-DicomPort}</port>
    </server>

    <server id="hl7-ia-update" type="hl7v2">
        <port>${UpdateStudies-HL7v2Port}</port>
    </server>

    <!-- Image Archive -->
    <!-- Most functionality on the same DICOM port as router, but CStore on secondary -->
    <service id="rialto-image-archive" type="image-archive">

        <server idref="dicom-main"      name="CStoreSCP" />
        <server idref="dicom-main"      name="StgCmtSCP" />
        <server idref="dicom-main"      name="StgCmtSCU" />
        <server idref="dicom-main"      name="CFindSCP" />
        <server idref="dicom-main"      name="CMoveSCP" />
        <server idref="hl7-ia-update"   name="update" />
        <server idref="hl7-ia-update"   name="merge" />
        <server idref="hl7-ia-update"   name="order" />
        <server idref="hl7-ia-update"   name="oru" />

        <server idref="http-rest" name="MINT">
            <url>/vault/mint/*</url>
        </server>

        <!-- comment out ILM and QC servers
        <server idref="http-rest" name="ILM">
            <url>/vault/ilm/*</url>
        </server>

        <server idref="http-rest" name="QC">
            <url>/vault/qc/*</url>
        </server>
        -->

        <server idref="http-rest" name="WADO">
            <url>/vault/wado</url>
        </server>

        <server idref="http-rest" name="StudyManagement">
            <url>${IA-StudyManagement-Path}/*</url>
        </server>

        <server idref="http-rest" name="StowRS">
          <url>/vault/stowrs/*</url>
        </server>

        <!-- Study Validation web service -->
        <server idref="http-rest" name="StudyValidation">
            <url>/vault/utils/*</url>
        </server>

        <device idref="xdsrep" />
        <device idref="xdsreg" />
        <device idref="pix" />
        <device idref="pdq" />
        <device idref="pif" />
        <device idref="cassandra-dc1" />

        <config>
            <prop name="TagMorphers">
               <script direction="IN" file="${rialto.rootdir}/etc/services/image-archive/scripts/inbound.groovy">
                    <callingAE>*</callingAE>
                    <calledAE>*</calledAE>
                </script>

                <!-- Updated to include fully qualified domain for CFIND request -->
                <script direction="CFIND_REQUEST" file="${rialto.rootdir}/etc/services/image-archive/scripts/cfind_request_morpher.groovy">
                    <callingAE>*</callingAE>
                   <calledAE>*</calledAE>
                </script> 
            </prop>

            <prop name="StorerType" value="HCP" />
            <prop name="Cache" value="${rialto.rootdir}/var/ids1/cache" />
            <prop name="Inbox" value="${rialto.rootdir}/var/ids1/index" />
            <prop name="JobQueue" value="${rialto.rootdir}/var/ids1/jobs" />
            <prop name="QCWorkDirectory" value="${rialto.rootdir}/var/ids1/qc" />
            <prop name="CacheSize" value="10" />
            <prop name="AETitle" value="${IA-AETitle}" />

            <!-- DefaultDomain:  For incoming studies, if Issuer of Patient ID (0010,0021) is empty, the
                 study is assumed to be from this domain.  Format: namespace&domainUID&domainUIDType -->
            <prop name="DefaultDomain" value="${System-DefaultLocalFullyQualifiedDomain}" />
            <prop name="AffinityDomain" value="${System-AffinityFullyQualifiedDomain}" />

            <prop name="IndexerType" value="CQL3" />
            <prop name="IndexerRetryTime" value="1m" />

            <!-- An ILM configuration, for archive mode -->
            <prop name="ImagingLifeCycleManagementConfiguration">
                <Mode>DISABLED</Mode>
                <!-- if you change data center name ensure that you also add eligibility script -->
                <DataCenter>DC1</DataCenter>
            </prop>

            <prop name="CassandraConfiguration">
                <clusterHosts>${CassandraClusterHosts}</clusterHosts>
                <clusterDatacenterName>${CassandraClusterDatacenterName}</clusterDatacenterName>
                <keyspaceName>${CassandraKeyspacePrefix}imagearchive</keyspaceName>
                <keyspaceReplicationStrategy>NetworkTopologyStrategy</keyspaceReplicationStrategy>
                <keyspaceReplicationStrategyOptions>${CassandraReplicationOption}</keyspaceReplicationStrategyOptions>
                <keyspaceCreateEnabled>true</keyspaceCreateEnabled>
                <dynamicConsistencyLevelEnabled>false</dynamicConsistencyLevelEnabled>
                <schemaUpdateEnabled>false</schemaUpdateEnabled>
            </prop>

            <prop name="DirectAccess" value="true" />
            <prop name="MintEnabled" value="true" />

            <!-- HCP Configuration -->
            <prop name="HcpConfiguration">

                <HcpNodeConfiguration>
                    <HcpUrl>${HCPURL_DC1_RW}</HcpUrl>
                    <HcpHost>${HCPHost_DC1_RW}</HcpHost>
                    <HcpAuthHeader>${HCPAuthHeader_DC1_RW}</HcpAuthHeader>
                    <HcpUsername>${HCPUsername_DC1_RW}</HcpUsername>
                    <HcpPassword>${HCPPassword_DC1_RW}</HcpPassword>
                </HcpNodeConfiguration>

                <HcpNodeConfiguration>
                    <HcpUrl>${HCPURL_DC2_RW}</HcpUrl>
                    <HcpHost>${HCPHost_DC2_RW}</HcpHost>
                    <HcpAuthHeader>${HCPAuthHeader_DC2_RW}</HcpAuthHeader>
                    <HcpUsername>${HCPUsername_DC2_RW}</HcpUsername>
                    <HcpPassword>${HCPPassword_DC2_RW}</HcpPassword>
                </HcpNodeConfiguration>

                <HttpConnectionConfiguration>
                <tls>
                    <TLSTrustStore>${rialto.rootdir}/etc/tls/RialtoTruststore.jks</TLSTrustStore>
                    <TLSTrustStorePass>C@rili0n!</TLSTrustStorePass>
                </tls>
                    <poolMaxConnectionsPerHost>${HCPPoolMaxConnectionsPerHost}</poolMaxConnectionsPerHost>
                    <poolMaxTotalConnections>${HCPPoolMaxTotalConnections}</poolMaxTotalConnections>
                </HttpConnectionConfiguration>
            </prop>

            <prop name="MWLReconciliationEnabled">false</prop>
            <prop name="QCToolsReconciliationScript">${rialto.rootdir}/etc/services/image-archive/scripts/qcreconciliation.groovy</prop>
            <prop name="ReconciliationScript">${rialto.rootdir}/etc/services/image-archive/scripts/qcreconciliation.groovy</prop>

            <prop name="PatientNameInHL7Location" value="/.PID-5" />

            <!-- Manifest Publish Configuration -->
            <prop name="PublisherType" value="DISCARD" />
            <prop name="MetadataStudyInstanceUIDKey">studyInstanceUid</prop>
            <prop name="HealthCareFacilityCode" value="HealthCareFacilityCodeValue, HealthCareFaciltiyCodeScheme, HealthCareFaciltiyCodeDisplay" />
            <prop name="PracticeSettingCode" value="PracticeSettingCodeValue, PracticeSettingCodeScheme, PracticeSettingCodeSchemeDisplay" />
            <prop name="ClassCode" value="ClassCodeValue, ClassCodeScheme, ClassCodeDisplay" />
            <prop name="TypeCode" value="TypeCodeValue, TypeCodeScheme,TypeCodeDisplay" />
            <prop name="ContentTypeCode" value="ContentTypeCodeValue, ContentTypeCodeScheme, ContentTypeCodeDisplay" />
            <prop name="ConfidentialityCode" value="ConfidentialityCodeValue, ConfidentialityCodeScheme,ConfidentialityCodeDisplay" />
            <!-- Manifest metadata is now determined from the groovy script -->
            <prop name="DocumentMetadataMorpher">${rialto.rootdir}/etc/services/image-archive/scripts/document_metadata_morpher.groovy</prop>

            <prop name="PatientIdentityFeedMorpher">${rialto.rootdir}/etc/services/image-archive/scripts/pif_morpher.groovy</prop>

            <!-- Study Content Notification -->
            <!-- Note: When configuring notification destinations, please ensure the receivingApplication
                 and receivingFacility match up with a configured device -->
            <prop name="StudyContentNotifierType" value="BASIC" />

            <prop name="NotificationDestinations">
                <destination name="Epic-EMR">
                    <receivingApplication>HYPERSPACE</receivingApplication>
                    <receivingFacility>CARILION</receivingFacility>
                    <morphingScript>${rialto.rootdir}/etc/services/image-archive/scripts/daffy.groovy</morphingScript>
                </destination>
                <destination name="HCR4 Prefetch">
                    <receivingApplication>CONNECT</receivingApplication>
                    <receivingFacility>CARILION</receivingFacility>
                    <morphingScript>${rialto.rootdir}/etc/services/image-archive/scripts/scn_prefetch.groovy</morphingScript>
                </destination>
            </prop>

            <!-- Storage Commit Variables  -->
            <prop name="StgCmtInitDelay"  value="1m" />
            <prop name="StgCmtRetry"  value="5" />
            <prop name="StgCmtRetryInterval"  value="30s" />

            <prop name="CStoreSCPTransferCapabilities"> 
                <enable>
                    <sopClass uid="1.2.840.10008.5.1.4.1.1.13.1.3" />
                </enable>
            </prop>

        </config>
    </service>

</config>
