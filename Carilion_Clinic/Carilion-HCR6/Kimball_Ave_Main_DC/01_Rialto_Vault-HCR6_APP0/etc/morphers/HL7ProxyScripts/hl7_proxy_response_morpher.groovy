def empirsp = responses.find {
   it.get("MSH-3") == "HYPERSPACE" && it.get("MSH-4") == "CARILION"
}

if (empirsp == null) {
    log.debug("NO EMPI response found")
    set("MSA-1", "AR")
}

if (empirsp != null && empirsp.get("MSA-1") == "AE" && empirsp.get("ERR-7") == "RECOVERABLE") {
    set("MSA-1", "AR")
}
