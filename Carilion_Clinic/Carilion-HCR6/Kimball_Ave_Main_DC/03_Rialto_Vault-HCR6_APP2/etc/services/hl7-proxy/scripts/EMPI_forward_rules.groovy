def scriptName = "EMPI Morpher: "

log.info(scriptName + "Start")

log.debug(scriptName + "processing input:\n{}", input)

LOAD('common_functions.groovy')
Pids pids = new Pids(log, input);
LOAD('common_carilion.groovy')
Carilion car = new Carilion(log, input);

def messageType = get('/.MSH-9-1')
def triggerEvent = get('/.MSH-9-2')

log.info(scriptName + "messageType is: {}, triggerEvent is: {}", messageType, triggerEvent)

car.groom(triggerEvent);

pids.clearAllPids();
pids.copyPid2ToPid3();

set('MSH-5', 'HYPERSPACE');
set('MSH-6', 'CARILION');

log.info(scriptName + "End")
