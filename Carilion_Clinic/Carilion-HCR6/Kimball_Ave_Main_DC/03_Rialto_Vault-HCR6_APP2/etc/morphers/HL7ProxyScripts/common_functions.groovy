class Pids {
    def log = null;
    def message = null;

    def Pids(log, message) {
        this.log = log;
        this.message = message;
    }

    def clearAllPids() {
        log.info("COMMON FUNCTIONS MORPHER - Removing all patient identifiers");

        def patientIds = null;
        try {
            patientIds = message.getList('/.PID-3(*)-1');
        } catch (Exception e) {
            log.warn("COMMON FUNCTIONS MORPHER - Problem clearing all patient identifiers", e);
        }

        if (patientIds != null) {
            for (int i = 0; i < patientIds.size(); i++) {
                message.set('/.PID-3(' + i + ')-1', '');
                message.set('/.PID-3(' + i + ')-2', '');
                message.set('/.PID-3(' + i + ')-3', '');
                message.set('/.PID-3(' + i + ')-4-1', '');
                message.set('/.PID-3(' + i + ')-4-2', '');
                message.set('/.PID-3(' + i + ')-4-3', '');
                message.set('/.PID-3(' + i + ')-5', '');
            }
        }
    }

    def copyPid2ToPid3() {
        log.info("COMMON FUNCTIONS MORPHER - Moving PID-2 into PID-3");

        message.set('/.PID-3(0)-1', message.get('/.PID-2-1'));
        message.set('/.PID-3(0)-2', message.get('/.PID-2-2'));
        message.set('/.PID-3(0)-3', message.get('/.PID-2-3'));
        message.set('/.PID-3(0)-4-1', message.get('/.PID-2-4-1'));
        message.set('/.PID-3(0)-4-2', '2.16.124.113638.1.1.1.1');
        message.set('/.PID-3(0)-4-3', 'ISO');
        message.set('/.PID-3(0)-5', message.get('/.PID-2-5'));
    }

}
