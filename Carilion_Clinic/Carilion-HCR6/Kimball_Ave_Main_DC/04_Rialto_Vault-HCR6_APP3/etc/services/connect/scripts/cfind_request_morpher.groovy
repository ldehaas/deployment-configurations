log.info("Cfind request morpher - Start")

/* workaround before RIALTO-6455 was fixed. Leaving it in for now.
def issuerToNamespace = [
    "mobile.army":"MASH",
    "princeton.plainsboro":"PPTH",
    "sick.kids":"SICK",
    "royal.hospital":"RH",
    "saint.joes":"STJOES",
    ]

issuer = get(IssuerOfPatientID)
log.info("Found the following issuer in the request: {}", issuer)
sourceIssuer = issuerToNamespace.get(issuer)
if (sourceIssuer != null) {
    log.info("Will replace {} in the cfind request with {}", issuer, sourceIssuer)
    set('IssuerOfPatientID', sourceIssuer)
} else {
    log.info("source issuer is null, cfind request will remain untouched!")        
}
*/

set("NumberOfStudyRelatedSeries", null)
log.info("Do not change the source issuer which is: {}", get(IssuerOfPatientID))
log.info("Cfind request morpher - THE END!")

