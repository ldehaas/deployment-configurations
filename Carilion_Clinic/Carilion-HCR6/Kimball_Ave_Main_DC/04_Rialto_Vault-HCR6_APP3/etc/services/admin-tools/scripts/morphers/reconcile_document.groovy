/**
 * This script is executed once per XDS document during auto-reconcile
 * workflow. The input is an instance of the DicomScriptingAPI wrapped
 * around the modality worklist entry that was selected upstream based
 * on the order matching rules. The output is an instance of the
 * DocumentMetadataScriptingAPI wrapped around the XDS document.
 *
 * This script can be used to update the document metadata in place,
 * using attributes of the matched DICOM modality worklist entry (the
 * order). Return true if the metadata has been updated, as this change
 * should be reflected in the registry. Return false if this script has
 * not made any modifications to the document metadata as this will
 * signal the workflow that no metadata update request need be sent.
 */
 
log.debug("Starting morpher [Worklist Match -> XDS Document Update]")
 
def xdsStudyIUID = output.get(XDSExtendedMetadata('studyInstanceUid'))
 
def dcmStudyIUID = input.get(StudyInstanceUID)
 
def modified = false
 
if (xdsStudyIUID != dcmStudyIUID) {
    log.debug("Reconciled study instance UID from '{}' to '{}'", xdsStudyIUID, dcmStudyIUID)
    output.set(XDSExtendedMetadata('studyInstanceUid'), dcmStudyIUID)
    modified = true
}
 
def xdsPlacerIssuer = output.get(XDSExtendedMetadata('orderingId'))
 
def dcmPlacerIssuer = input.get(PlacerOrderNumberImagingServiceRequest)
 
if (xdsPlacerIssuer != dcmPlacerIssuer) {
    log.debug("Reconciled ordering ID from '{}' to '{}'", xdsPlacerIssuer, dcmPlacerIssuer)
    output.set(XDSExtendedMetadata('orderingId'), dcmPlacerIssuer)
    modified = true
}
 
return modified
