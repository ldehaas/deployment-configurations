// Image Archive C-Find Request Morpher
//
// output variable name: output
// output variable type: DICOM
// output variable docs: https://karoshealth.atlassian.net/wiki/spaces/R73/pages/438108346/Script+basics#Scriptbasics-dicomDICOMmethods
//
// VC 6.7: grep "IA DICOM C-Find Request Morpher" /home/rialto/rialto/log/rialto.log
// VC 7.x: grep "IA DICOM C-Find Request Morpher" /var/log/rialto/rialto.log

def scriptName="IA DICOM C-Find Request Morpher - "

log.info(scriptName + "START ...")
log.debug(scriptName + "input: \n{}", input)

if (IssuerOfPatientID) {
    log.debug(scriptName + "about to set IPID to MASTER&2.16.124.113638.1.1.1.1&ISO")
    set(IssuerOfPatientID, 'MASTER')
    set("IssuerOfPatientIDQualifiersSequence/UniversalEntityIDType", 'ISO')
    set("IssuerOfPatientIDQualifiersSequence/UniversalEntityID", '2.16.124.113638.1.1.1.1')
} else {
    log.debug(scriptName + "IssuerOfPatientID is {}", get(IssuerOfPatientID))
}

log.debug(scriptName + "output: \n{}", output)
log.info(scriptName + "END ...")