log.info("Connect forward rules MORPHER - Start")


LOAD('common_functions.groovy')
Pids pids = new Pids(log, input);
LOAD('common_carilion.groovy')
Carilion car = new Carilion(log, input);

def messageType = get('/.MSH-9-1')
def triggerEvent = get('/.MSH-9-2')

log.info("Connect - messageType is: {}, triggerEvent is: {}", messageType, triggerEvent)

if (!"A31".equals(triggerEvent) && !"A39".equals(triggerEvent)) {
    log.warn("Connect MORPHER - Will not forward to Connect service");
    return false;
} 

car.groom(triggerEvent);

pids.clearAllPids();
pids.copyPid2ToPid3();


log.info("Connect forward rules MORPHER - End")
