log.info("ilm_policy.groovy START")

def modalities = study.getModalities()

log.info("Modalities: {}", modalities)

// NOTE: If the production site contains studies used by the
// health check service, these should never be removed from the system.
// e.g. Check if this is a Test study that is used by HealthCheck service.
// StudyInstanceUIDs below are examples only. Change to match production requirements.
if (study.getStudyInstanceUID == '1.2.392.200036.9125.2.12345679' || study.getStudyInstanceUID() == '1.2.392.200036.9125.2.987654321') {
    log.info(scriptName + "study with StudyInstanceUID {} is used by HealthCheck service, skipping...", study.getStudyInstanceUID());
    log.debug(scriptName + "END - Done looking at study with StudyInstanceUID {}", study.getStudyInstanceUID());
    return;
}

if (modality.contains("CT")) {
    ops.scheduleStudyForward("DESTINATION_AE")
}

log.info("ilm_policy.groovy END")
