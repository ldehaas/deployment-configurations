log.debug("in.groovy: Start inbound morpher")

def issuerOfPatientId = get(IssuerOfPatientID)
def universalEntityID = get("IssuerOfPatientIDQualifiersSequence/UniversalEntityID")
def universalEntityIDType = get("IssuerOfPatientIDQualifiersSequence/UniversalEntityIDType")

if (issuerOfPatientId == null) {
    issuerOfPatientId = 'CG'
    set(IssuerOfPatientID, issuerOfPatientId)
}

log.debug("in.groovy: IssuerOfPatientId is {} ", issuerOfPatientId)

def (namespace, domain, type) = issuerOfPatientId.tokenize("&")
log.debug("in.groovy: namespace = {}, domain = {}, type = {}", namespace, domain, type)

if (namespace != "") {
    set(IssuerOfPatientID, namespace)
}

if (universalEntityID == null && domain == null) {
    if (get(IssuerOfPatientID) == "CG") {
        set("IssuerOfPatientIDQualifiersSequence/UniversalEntityID", 'county.general')
    } else if (get(IssuerOfPatientID) == "TL") {
        set("IssuerOfPatientIDQualifiersSequence/UniversalEntityID", 'toronto.lakeshore')
    } else if (get(IssuerOfPatientID) == "DR") {
        set("IssuerOfPatientIDQualifiersSequence/UniversalEntityID", 'don.river')
    } else if (get(IssuerOfPatientID) == "MASH") {
        set("IssuerOfPatientIDQualifiersSequence/UniversalEntityID", 'mobile.army')
    } else if (get(IssuerOfPatientID) == "PPTH") {
        set("IssuerOfPatientIDQualifiersSequence/UniversalEntityID", 'princeton.plainsboro')
    } else {
        set("IssuerOfPatientIDQualifiersSequence/UniversalEntityID", 'county.general')
    }
} else if (universalEntityID == null && domain != null){
    set("IssuerOfPatientIDQualifiersSequence/UniversalEntityID", domain)
}

if (universalEntityIDType == null && type == null) {
    set("IssuerOfPatientIDQualifiersSequence/UniversalEntityIDType", 'ISO')
} else if (universalEntityIDType == null && type != null) {
    set("IssuerOfPatientIDQualifiersSequence/UniversalEntityIDType", type)
}

log.debug("in.groovy: End inbound morpher")
