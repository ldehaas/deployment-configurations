initialize( 'ORM', 'O01', '2.3' );
output.getMessage().addNonstandardSegment('IPC')
log.debug("input is {}", input);
log.debug("Calling AE Title is {}", getCallingAETitle());
//
set('MSH-3', 'HCR');
set('MSH-4', 'IMG_AVAIL');
set('MSH-7', '20130827132217')
//
setPersonName('PID-5', input.get(PatientName));
set('PID-3-1', input.get(PatientID));
set('PID-3-4', 'MASTER')
set('PID-3-5', 'MPI')
//
set('ORC-1', 'XO')
//
set('OBR-1', '1')
set('OBR-3', input.get(AccessionNumber))
//
// Hardcoding values for OBR-4-1 and 4-3 
set('OBR-4-1', 'RXR')
//
set('OBR-4-2', input.get(StudyDescription))
set('OBR-4-3', 'RMH')
//
set('OBR-16-9', 'SMART')
set('OBR-16-13', 'PROV')
set('OBR-25', 'I')
//
set('OBX-1', '1')
set('OBX-2', 'TX')
set('OBX-3-1-2', 'GDT')
set('OBX-5', 'Y')
set('OBX-9', input.get(StudyInstanceUID))
//
//set('IPC-1', input.get(AccessionNumber))
//set('IPC-3', input.get(StudyInstanceUID))
