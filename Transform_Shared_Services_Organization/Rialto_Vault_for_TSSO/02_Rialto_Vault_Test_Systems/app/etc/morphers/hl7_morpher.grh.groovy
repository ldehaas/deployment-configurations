// grh has patients and accounts, but no visits.
// vault has patients and visits. this morpher is used to bridge
// the differences.

// map grh specific merge / move / rename messages to the ones vault expects
def msh91 = get('MSH-9-1')
def msh92 = get('MSH-9-2')

//if (msh91 == 'ORM') {
//    set('OBR-7',get('OBR-27-4'))
//}


if (msh91 == 'ADT') {
    if (msh92 == 'A06' || msh92 == 'A07' || msh92 == 'A35') {
        set('MRG-5', get('MRG-3'))
        set('MSH-9-2', 'A50')

    } else if (msh92 == 'A44') {
        set('MRG-5', get('MRG-3'))
        set('MSH-9-2', 'A45')

        // PV1 cannot exist in A36 by spec, but since we are aliasing this
        // to A45 which requires PV1, add it as non standard
        input.getMessage().addNonstandardSegment('PV1')
    }

    // Map appropriate values for allergy types
    def allergyIds = null;
    try {
        allergyIds = getList('/.AL1(*)-3')
    } catch (Exception e) {
        // do nothing!
    }
    if (allergyIds != null) {
         def allergyTypeMap = [
             'DA':'DRUG',
             'FA':'FOOD',
             'MA':'OTHER',
             '':'NONE'
         ]

        for (int i = 0; i < allergyIds.size(); i++) {
            def allergyId = get('/.AL1(' + i + ')-3-1')
            def allergyText = get('/.AL1(' + i + ')-3-2')
            if ((allergyId == null || ''.equals(allergyId)) &&
                    (allergyText == null || ''.equals(allergyText))) {
                continue;
            }

            def allergyTypeField = '/.AL1(' + i + ')-2'
            def allergyType = get(allergyTypeField);

            if (allergyType == null || '""'.equals(allergyType)) {
                set(allergyTypeField, allergyTypeMap[''])
            } else if (allergyTypeMap[allergyType] != null) {
                set(allergyTypeField, allergyTypeMap[allergyType])
            } else if (allergyTypeMap.values().contains(allergyType)) {
                // do nothing, already set properly!
            } else {
                set(allergyTypeField, allergyTypeMap['MA'])
            }
        }
    }
}

// use account id as visit id
try {
    set('PV1-19', get('PID-18'))
} catch (Exception e) {
    // sink
    // this fails when the type of hl7 message does not allow PV1
    // in those cases, PV1 doesn't matter
}

// Ensure the MRN is in the first repetition of the PID-3 field
def MRN_IDENTIFIER_TYPE = 'MR'
def identifierType = get('/.PID-3-5')
if (!MRN_IDENTIFIER_TYPE.equalsIgnoreCase(identifierType)) {

    def patientIds = null;
    try {
        patientIds = getList('/.PID-3(*)-1')
    } catch (Exception e) {
        // do nothing!
    }

    def firstId = get('/.PID-3-1')
    def firstCheckDigit = get('/.PID-3-2')
    def firstCheckDigitCode = get('/.PID-3-3')
    def firstNamespaceId = get('/.PID-3-4-1')
    def firstUniversalId = get('/.PID-3-4-2')
    def firstUniversalIdType = get('/.PID-3-4-3')
    def firstIdentifierType = get('/.PID-3-5')
    for (int i = 0; i < patientIds.size(); i++) {
        def currIdentifierType = get('/.PID-3(' + i + ')-5')
        if (MRN_IDENTIFIER_TYPE.equalsIgnoreCase(currIdentifierType)) {

            // Copy MRN into first repetition
            set('/.PID-3-1', get('/.PID-3(' + i + ')-1'))
            set('/.PID-3-2', get('/.PID-3(' + i + ')-2'))
            set('/.PID-3-3', get('/.PID-3(' + i + ')-3'))
            set('/.PID-3-4-1', get('/.PID-3(' + i + ')-4-1'))
            set('/.PID-3-4-2', get('/.PID-3(' + i + ')-4-2'))
            set('/.PID-3-4-3', get('/.PID-3(' + i + ')-4-3'))
            set('/.PID-3-5', get('/.PID-3(' + i + ')-5'))

            // Copy the old 'first' repetition where the MRN was found
            set('/.PID-3(' + i + ')-1', firstId)
            set('/.PID-3(' + i + ')-2', firstCheckDigit)
            set('/.PID-3(' + i + ')-3', firstCheckDigitCode)
            set('/.PID-3(' + i + ')-4-1', firstNamespaceId)
            set('/.PID-3(' + i + ')-4-2', firstUniversalId)
            set('/.PID-3(' + i + ')-4-3', firstUniversalIdType)
            set('/.PID-3(' + i + ')-5', firstIdentifierType)

            break;
        }
    }
}

//TSSO wants the order number and drug name appended in order number for RDE messages when they use the same order number for 2 different drugs
if (msh91 == 'RDE') {
    def orderId = get('ORC-2')
    def drugName = get('RXE-2-1')
    if ((orderId != null && orderId != '')  && (drugName != null && drugName != '')) {
        set('ORC-2', orderId + drugName)
    }
}

//TSSO wants the HCN and Version Code appended together in the HCN field if both of them are not blank or null.
def HCN = get('PID-19-1')
def versionCode = get('PID-19-2')
if ((HCN != null && HCN != '')  && (versionCode != null && versionCode != '')) {
    set('PID-19-1', HCN + '-' + versionCode)
}

//TSSO wants to use a value of 'CA' or 'OC' in ORC-1 for all orders to indicate it is cancelled - KHC4643
if (msh91 == ‘ORM’) {
    def orderStatus = get('ORC-1')
    if (orderStatus == 'CA' || orderStatus == 'OC') {
        set('OBR-25', 'OC')
    }   
}
