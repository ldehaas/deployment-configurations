#!/bin/bash
#
# Backup Cassandra Databases
#
# This script is invoked by cron to perform a daily full backup of all the
# Cassandra keyspaces.
#
# Written by: John E. Lammers, March 2013
# Copyright (c) 2013 Karos Health Inc.
#


#
# Constants.
#
PATH='/bin:/usr/bin:/home/rialto/bin:/usr/local/sbin:/usr/sbin:/sbin'

. /etc/profile.d/cassandra.sh

APP_NAME='backup-cassandra'
APP_USER='rialto'

CQLSH=$CASS_HOME/bin/cqlsh
NODETOOL=$CASS_HOME/bin/nodetool
NICE='/bin/nice /usr/bin/ionice -n7'

# LOG_DIR='.'
LOG_DIR=/home/rialto/var/log
STDOUT_FILE=$LOG_DIR/$APP_NAME.out
LOG_FILE=$LOG_DIR/$APP_NAME.log

# PID_DIR='.'
PID_DIR=/var/run/cassandra
APP_PID=$PID_DIR/$APP_NAME.pid

# TOPOLOGY_FILE='/tmp/cassandra-topology.properties'
TOPOLOGY_FILE='/opt/cassandra/conf/cassandra-topology.properties'

#
# Subroutines
#
. /etc/rc.d/init.d/functions

log() {
    if [ -z "$Quiet" ]; then
        echo $(date) "$*"
    fi
    echo $(date) "$*" >>$LOG_FILE
}

die() {
    log "FATAL: $*"
    exit 1
}

isAppAlive() {
    [ -f $APP_PID ] && checkpid `cat $APP_PID`
}

# Apply scheduling algorithm to determine when database backup
# should be performed.
schedule() {
    # Get a list of the local machine's network interfaces (nics).
    nicList=$(ifconfig | grep '^[a-z][a-z0-9]*' | awk '{print $1}')
    log "Local NIC(s):" $nicList

    # Get the IP addresses for the local machine's nics.
    localIpList=''
    for i in $nicList ; do
        localIpList+=$(ifconfig $i | grep 'inet addr' | \
            awk -F '[: ]+' '{print $4}')" "
    done
    log "Local IP address(es): $localIpList"

    # Search the Cassandra topology file for the local machine's IP address.
    found=false
    for i in $localIpList ; do
        ipString=$(echo $i | sed 's/\./\\./g') # Escape dots
        line=$(grep "^[ ]*$ipString[ ]*=" $TOPOLOGY_FILE)
        if [ "$line" ]; then
            found=true
            ipAddress=$i
            break
        fi
    done
    if [ $found == true ]; then
        log "Found local node in topology file: $line"
    else
        die "Failed to find local IP address in topology file $TOPOLOGY_FILE"
    fi

    # Get the list of all IP addresses in the cluster.
    clusterIpList=$(
        grep "^[ ]*[1-9][0-9\.]*[ ]*=" $TOPOLOGY_FILE \
        | sed -e 's/=.*$//' -e 's/ //g' \
        | sort -n -t '.' -k 1,1n -k 2,2n -k 3,3n -k 4,4n
    )
    if [ -z "$clusterIpList" ]; then
        die "Failed to extract cluster IP addresses from topology file"
    fi
    log "Cluster IP addresses:" $clusterIpList

    # Figure out our "node number" (which corresponds to the sorted list
    # of IP addresses.
    # e.g. If we're the lowest numbered IP address, then our number is 0.
    # e.g. If we're the 2nd lowest numbered IP address, then our number is 1.
    # e.g. If we're the 3rd lowest numbered IP address, then our number is 2.
    # etc.
    declare -i number=0
    for i in $clusterIpList ; do
        if [ "$i" == "$ipAddress" ]; then
            break
        fi
        let ++number
    done
    log "My IP address is number: $number (first is zero)"

    # From our number, we determine what time slot we will run.
    # Timeslot 0 is now
    # Timeslot 1 is N minutes later
    # Timeslot 2 is N*2 minutes later
    # For cluster performance, it's important that two nodes do not backup
    # at the same time.
    log "My timeslot to run is timeslot: $number"
    declare -i HOUR=60*60
    declare -i delay=0*$HOUR
    log "My time delay is: $number hours"
    if ((delay != 0)); then
        log "Sleeping for $delay seconds"
        sleep $delay
        log "Yawn!  Time to run the backup ... where's my coffee?"
    fi
}

usage() {
cat <<EOF
Usage: $0 [options}*
Options:
    -q  Quiet mode.  Write messages to log file, not stdout / stderr.  Used when
        this script is invoked by cron.
    -s  Scheduling mode.  Perform backup according to the scheduling
        algorithm.  Used when this script is invoked by cron.
    -?  Display this usage message.
EOF
}


#######################
# Execution starts here
#######################

# Process command line arguments
Quiet='' # empty = false
Schedule='' # empty = false
while getopts 'qs?' flag; do
    case $flag in
        q) # Do not log to stdout (only to log file)
            Quiet='true'
            ;;
        s) # Run according to scheduling algorithm
            Schedule='true'
            ;;
        \?)
            usage
            exit 1
            ;;
        *)
            echo "Unknown command line option '-$flag'"
            usage
            exit 1
            ;;
    esac
done

# Sanity checks
if [ "$USER" != 'rialto' ]; then
    # Do not call die() otherwise the log file will be owned by root if
    # accidentally invoked by root
    echo 'Must be run by userid rialto'
    exit 1
fi
if isAppAlive; then
    die "$APP_NAME is already running"
fi
echo $$ >$APP_PID

REPAIR_PID=$PID_DIR/repair-cassandra.pid
if [ -f $REPAIR_PID ] && checkpid `cat $REPAIR_PID`; then
    die "Database repair is running"
fi

if [ -z "$CASS_HOME" ]; then
    die 'Cassandra home not defined (CASS_HOME)'
fi
if [ ! -d "$CASS_HOME/bin" ]; then
    die 'Cassandra home not accessible (CASS_HOME)'
fi
if [ -z "$CASS_DATA" ]; then
    die 'Cassandra data directory not defined (CASS_DATA)'
fi
if [ ! -d "$CASS_DATA" ]; then
    die 'Cassandra data directory not accessible (CASS_DATA)'
fi
if [ -z "$CQLSH_HOST" ]; then
    die 'Cassandra host not defined (CQLSH_HOST)'
fi
if [ ! -x $CQLSH ]; then
    die "Application executable file not found: $CQLSH"
fi
if [ ! -x $NODETOOL ]; then
    die "Application executable file not found: $NODETOOL"
fi
if ! rpm -q rsync >/dev/null ; then
    die 'Error: rsync is not installed'
fi
if [ ! -f $TOPOLOGY_FILE ]; then
    die "Cassandra topology file not found: $TOPOLOGY_FILE"
fi


# If enabled, check scheduling algorithm to determine when and if
# to perform backup.
if [ "$Schedule" ]; then
    schedule
fi



# Obtain list of keyspaces to back up
log '*** Cassandra backup starting ***'
KEYSPACE_LIST=~rialto/rialto/etc/cassandra.databases
if [ -r $KEYSPACE_LIST ]; then
    log "Reading keyspaces to back up from $KEYSPACE_LIST"
    keyspaceList=$(cat $KEYSPACE_LIST)
else
    log "Using cqlsh to determine keyspaces to back up"
    keyspaceList=$(echo 'desc keyspaces' | $CQLSH 2>> $LOG_FILE)
    if [ $? -ne 0 ]; then
        die "Error querying cassandra for the list of keyspaces"
    fi

    # Don't back up system keyspaces:
    keyspaceList=$(echo $keyspaceList | sed 's/\bsystem\S*//g')
fi

log "Processing keyspaces: $keyspaceList"


# Delete old snapshots
# When the Cassandra database gets large, we can't keep snapshots forever.
log 'Clearing any old database snapshots ...'
snapshotDirs=$(find $CASS_DATA -type d -name 'snapshots')
OLD='+30' # days
for i in $snapshotDirs; do
    find $i -maxdepth 1 -type d \! -name 'snapshots' -mtime $OLD -print0 \
        | xargs -0 rm -fr
done


# Delete prior backups
LOCAL_BACKUP_FOLDER=$CASS_DATA/Backup
rm -fr "$LOCAL_BACKUP_FOLDER"
mkdir -p $LOCAL_BACKUP_FOLDER


# Create and pack a snapshot for each keyspace, one at a time
cd $CASS_DATA
for keyspace in $keyspaceList ; do

    # Ensure configured keyspace name exists
    keyspacePath=$CASS_DATA/$keyspace
    if [ ! -d "$keyspacePath" ]; then
        log "Folder for keyspace '$keyspace' not found"
        continue
    fi

    # Create snapshot
    backupId="$(date '+%Y%m%d-%H%M%S')" # Uniquely identifies this backup set
    snapshotName="${keyspace}-${backupId}"
    log "Creating snapshot '$snapshotName' for keyspace '$keyspace' ..."
    $NODETOOL snapshot $keyspace -t $snapshotName &>>$LOG_FILE
    if [ $? -ne 0 ]; then
        log "Creating snapshot of '$keyspace' failed"
        continue
    fi
    sleep 20 # See RIALTO-4815

    # This makes patterns that match no files expand to an empty string
    # instead of the pattern itself.  This prevents tar errors for empty
    # databases.
    shopt -s nullglob

    # Compress right away to minimize disk space used
    log "Packing up files for snapshot '$snapshotName' ..."
    backupFile="$LOCAL_BACKUP_FOLDER/${snapshotName}.tgz"
    $NICE tar czf $backupFile $keyspace/*/snapshots/$snapshotName \
        /home/rialto/rialto/etc \
        /home/rialto/.ssh \
        /opt/cassandra/conf \
        /etc/security/limits.d/cassandra.conf \
        /etc/sysconfig/network \
        /etc/hosts &>>$LOG_FILE
    if [ $? -ne 0 ]; then
        log "Error or warning tarring up snapshot for keyspace '$keyspace'"
        log "Note that 'file changed as we read it' warnings can be ignored"
        log "Continuing with other keyspaces."
    fi
    sleep 2 # See RIALTO-4815

    shopt -u nullglob

    # Delete snapshot
    log "Deleting snapshot '$snapshotName' ..."
    $NICE $NODETOOL clearsnapshot $keyspace -t $snapshotName &>>$LOG_FILE
    if [ $? -ne 0 ]; then
        log "Error deleting snapshot '$snapshotName'"
    fi

    sleep 4 # Be nice
done

log "Exporting backup"
cd
$NICE bin/archive-backup.sh "$LOCAL_BACKUP_FOLDER" &>>$LOG_FILE
if [ $? -ne 0 ]; then
    die "Error exporting backup"
fi
rm -fr "$LOCAL_BACKUP_FOLDER"

log 'Cassandra backup complete'
rm $APP_PID
sleep 2 # Without this output from "script" command is truncated
