def called_AET = getCalledAETitle().toUpperCase();

if (called_AET.equals("COL_RTR_MIRADA")) {
    log.info("Called AET is COL_RTR_MIRADA will send only to Mirada");
    return "etc/dataflows/mirada.xml";
}



// the default is to only route to vitrea advanced
return "etc/dataflows/vitrea.xml";
