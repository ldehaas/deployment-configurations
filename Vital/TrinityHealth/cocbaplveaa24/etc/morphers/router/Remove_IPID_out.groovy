log.debug("out.groovy: Start outbound morpher")
//
// we need to remove the temporary IPID before we send on the images
// rialto forces us to set an IPID so we did this on the Wheaton router
//
def issuerOfPatientId = get(IssuerOfPatientID)
def universalEntityID = get("IssuerOfPatientIDQualifiersSequence/UniversalEntityID")

if (issuerOfPatientId == 'Trinity') {
    log.debug("out.groovy: issuerOfPatientId removed")
    remove(0x00100021)
}




if (universalEntityID == '1.2.3.4') {
        log.debug("out.groovy: IssuerOfPatientIDQualifiersSequence removed")
        remove(0x00100024)
}


log.debug("out.groovy: End outbound morpher")

