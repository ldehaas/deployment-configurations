// this script will decide which dataflow document to use.
// the dataflow document has the destination and transcoding
// for now some NM images do not transcode to jpeg80 correctly so we
// have to pick a different transcode which is jpeg70

def called_AET = getCalledAETitle().toUpperCase();

if (get(Modality) != null && get(Modality).equalsIgnoreCase("NM")){
    switch (called_AET) {
            case  'VITREAADV':
                return "etc/dataflows/vitrea-nm.xml";
            case 'MIRADAADV':
                return "etc/dataflows/mirada-nm.xml";
            default: 
                return "etc/dataflows/vitrea-nm.xml"; 
    }
} else {
    switch (called_AET) {
            case  'VITREAADV':
                return "etc/dataflows/vitrea.xml";
            case 'MIRADAADV':
                return "etc/dataflows/mirada.xml";
            default: 
                return "etc/dataflows/vitrea.xml"; 
    }

}
