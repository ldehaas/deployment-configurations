/*
 * this in.groovy script is used to set a dummy IPID value on incoming studies.  
 * because there is a 'bug' were you must set this or it will not get processed
 * we will set it to a bogus value and then remove the tags on the Columbas router
*/

log.debug("in.groovy: Start inbound morpher")

def issuerOfPatientId = get(IssuerOfPatientID)
def universalEntityID = get("IssuerOfPatientIDQualifiersSequence/UniversalEntityID")
def universalEntityIDType = get("IssuerOfPatientIDQualifiersSequence/UniversalEntityIDType")

if (issuerOfPatientId == null) {
    issuerOfPatientId = 'Trinity'
    set(IssuerOfPatientID, issuerOfPatientId)
}

log.debug("in.groovy: IssuerOfPatientId is {} ", issuerOfPatientId)



if (universalEntityID == null) {
        set("IssuerOfPatientIDQualifiersSequence/UniversalEntityID", '1.2.3.4')
}

if (universalEntityIDType == null) {
    set("IssuerOfPatientIDQualifiersSequence/UniversalEntityIDType", 'ISO')
}

log.debug("in.groovy: End inbound morpher")

