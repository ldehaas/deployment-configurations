def scriptname="EMPI_forward_rules.groovy"
log.debug("{} starting", scriptname)

def messageType = get('/.MSH-9-1')
def triggerEvent = get('/.MSH-9-2')

if (!"ADT".equalsIgnoreCase(messageType)) {
    log.debug("{} skipping {} messageType", scriptname, messageType)
    return false;
}

// First names are frequently being followed by spaces which is breaking the PDQ lookups.
def givenNameValidation = get('PID-5-2').trim()
set('PID-5-2', givenNameValidation)

log.debug("{} ending", scriptname)
