def scriptname="XDSReg_forward_rules.groovy"

log.debug("{} starting", scriptname)

if (get('/.MSH-9-2') != null && get('/.MSH-9-2') == "A39")
    set('/.MSH-9-2', 'A40')

def messageType = get('/.MSH-9-1')
def triggerEvent = get('/.MSH-9-2')

/*
log.info("messageType is: {}", messageType)
log.info("triggerEvent is: {}", triggerEvent)
*/

if (messageType != null && triggerEvent != null) {
    if ("ADT".equals(messageType) && ("A40".equals(triggerEvent))) {
        log.info("{} updating XDSRegistry")
    } else {
        return false
    }
} else {
    return false
}

// First names are frequently being followed by spaces which is breaking the PDQ lookups.
def givenNameValidation = get('PID-5-2').trim()
set('PID-5-2', givenNameValidation)

log.debug("{} ending", scriptname)
