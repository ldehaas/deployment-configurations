
log.info("routing.groovy v03: Begin routing script.")

import org.dcm4che2.data.DicomObject
import org.dcm4che2.data.Tag.*

import com.karos.rialto.workflow.model.GenericWorkflow
import com.karos.rialto.workflow.common.tasks.*
import com.karos.rialto.storage.StorageRepository
import com.karos.rialto.storage.file.FileSystemStorageRepository

import com.karos.rialto.workflow.model.Task.Status
import com.karos.rialto.workflow.model.Task.TaskPriority

import org.joda.time.DateTime
import org.joda.time.LocalDate
import java.time.LocalDateTime

import com.karos.rtk.common.Pid;
import com.karos.rtk.pixpdq.PDQConsumer;
import com.karos.rtk.pixpdq.PDQuery;
import com.karos.rtk.pixpdq.PDQResult;
import com.karos.rtk.pixpdq.hl7v2.HL7v2PDQConsumer;

def currentDate = LocalDateTime.now()

def fsRepo = new FileSystemStorageRepository('Router','/data/router')
String[] sopPath = [
        getCallingAETitle(),
        getCalledAETitle(),
        currentDate.getYear(),
        currentDate.getMonthValue(),
        currentDate.getDayOfMonth(),
        currentDate.getHour(),
        currentDate.getMinute(),
        currentDate.getSecond(),
        input.get(StudyInstanceUID)
]

def morpherPath = '/home/rialto/rialto/etc/services/dicom-router/scripts/'
def routerAE = 'ROUTER'
def archiveAE = 'VITREA'
// KHC12628 - Updated archive2AE from RIALTO2 to UPLOAD
def archive2AE = 'UPLOAD'
def siavashWS = 'seshaghi_ws'
def nigelWS = 'nsommerfeld_ws'
def pdqIP = 'localhost'
def pdqPort = 2399

// Receive images
def receiveImageTaskBuilder = new ReceiveImageTask.Builder()
// receiveImageTaskBuilder.setStorageRepository(fsRepo) (using default)
receiveImageTaskBuilder.setStorageTags(sopPath)

def patientID = input.get(PatientID)
def issuerOfPatientId = input.get(IssuerOfPatientID)
def universalEntityID = input.get("IssuerOfPatientIDQualifiersSequence/UniversalEntityID")
def universalEntityIDType = input.get("IssuerOfPatientIDQualifiersSequence/UniversalEntityIDType")
def patientName = input.get(PatientName).tokenize('^')
def tempBirthDate = input.get(PatientBirthDate)
LocalDate patientBirthDate = new LocalDate(tempBirthDate.substring(0,4) + '-' + tempBirthDate.substring(4,6) + '-' + tempBirthDate.substring(6,8))
def patientSex = input.get(PatientSex)
def studyUID = input.get(StudyInstanceUID)
def callingAE = getCallingAETitle()
def calledAE = getCalledAETitle()

log.debug("routing.groovy: Inputs\nCalling AE: {}, Called AE: {}\nPatient ID: {}^^^{}&{}&{}, Patient Name: {}, Patient Sex: {}, Patient Birth Date: {}", callingAE, calledAE, patientID, issuerOfPatientId, universalEntityID, universalEntityIDType,  patientName, patientSex, patientBirthDate)

def morphStudyTaskBuilder = new MorphStudyTask.Builder()
def sendStudyTaskBuilder = new SendStudyTask.Builder()
sendStudyTaskBuilder.setMaxRetries(5)
sendStudyTaskBuilder.setRetryDelays("1m", "5m", "30m", "120m", "480m" )
def cleanStorageLocationTaskBuilder = new CleanStorageLocationTask.Builder()

// Make PDQ call
PDQConsumer pdqConsumer = new HL7v2PDQConsumer(log, pdqIP, pdqPort, null); //last is encryption params
Pid queryPid = new Pid (patientID,issuerOfPatientId,universalEntityID,universalEntityIDType);
PDQuery query = new PDQuery()
query.familyName = patientName[0]
query.givenName = patientName[1]
query.sex = patientSex
query.birthDate = patientBirthDate
PDQResult pdqResult = pdqConsumer.findPatients(query);
log.debug("routing.groovy: PDQ Result\n{}", pdqResult)

if (pdqResult.getResults().size() != 1) { 
log.info("routing.groovy: PDQ Found {} results. Sending Study {} to upload cache.", pdqResult.getResults().size(), studyUID)
    // Case 0
    // 0 or 2-n results

    // Perform tag morphing for external studies.
    morphStudyTaskBuilder.setMorpherScriptName(morpherPath + 'externalExam.groovy')
    morphStudyTaskBuilder.addDataDependency(receiveImageTaskBuilder,Status.SUCCESS)

   // Send study
    sendStudyTaskBuilder.addDataDependency(morphStudyTaskBuilder, Status.SUCCESS)
    sendStudyTaskBuilder.setCallingAe(routerAE)
    sendStudyTaskBuilder.setDestination(archive2AE)

    // Clean storage
    cleanStorageLocationTaskBuilder.addDataDependency(sendStudyTaskBuilder, Status.SUCCESS)
    cleanStorageLocationTaskBuilder.addDataDependency(morphStudyTaskBuilder, Status.SUCCESS)
    cleanStorageLocationTaskBuilder.addDataDependency(receiveImageTaskBuilder, Status.SUCCESS)

    // Run workflow
    def case0 = new GenericWorkflow.Builder("Send to upload cache.")
    case0.addTaskBuilder(receiveImageTaskBuilder)
    case0.addTaskBuilder(morphStudyTaskBuilder)
    case0.addTaskBuilder(sendStudyTaskBuilder)
    case0.addTaskBuilder(cleanStorageLocationTaskBuilder)
    return case0.build()

} else {

// Case 1
// 1 result
log.info("routing.groovy: Found 1 result. Sending Study {} to archive.", pdqResult.getResults().size(), studyUID)
File mappingFile = new File('/home/rialto/rialto/var/tmp/' + get(StudyInstanceUID) + '.qry')

if(!mappingFile.exists()) {
    // Mapping file will never exist at this stage except in error cases
    mappingFile.deleteOnExit()

    log.debug("routing.groovy: Temp file: {}", mappingFile.getAbsolutePath())

    pdqResult.getResults()[0].getPids().each { identifier ->
        log.debug("routing.groovy: Idenfier found: {}.", identifier)
        mappingFile << identifier.toString() + '\n'
    }
}

//log.info("mengler: DICOM PID {}, PDQ PID {}", patientID, Pid[0])

//set(PatientID) = Pid[0]

morphStudyTaskBuilder.setMorpherScriptName(morpherPath + 'updatePID.groovy')
morphStudyTaskBuilder.addDataDependency(receiveImageTaskBuilder,Status.SUCCESS)

//Transcode study for sending to the WS which seem to have limited presentation state support.
//def transcodeTaskBuilder = new TranscodeInstancesTask.Builder()
//transcodeTaskBuilder.addDataDependency(morphStudyTaskBuilder, Status.SUCCESS)
//transcodeTaskBuilder.setSupportedSourceTs("1.2.840.10008.1.2.4.70")
//transcodeTaskBuilder.setDestinationTs("1.2.840.10008.1.2.1")

// Send study
sendStudyTaskBuilder.addDataDependency(morphStudyTaskBuilder, Status.SUCCESS)
sendStudyTaskBuilder.setCallingAe(routerAE)
sendStudyTaskBuilder.setDestination(archiveAE)

//Routing for individual workstations
// Dr. Siavash
def sendStudyWS1 = new SendStudyTask.Builder()
//sendStudyWS1.addDataDependency(transcodeTaskBuilder, Status.SUCCESS)
sendStudyWS1.addDataDependency(morphStudyTaskBuilder, Status.SUCCESS)
sendStudyWS1.setCallingAe(routerAE)
sendStudyWS1.setDestination(siavashWS)

// Nigel
def sendStudyWS2 = new SendStudyTask.Builder()
//sendStudyWS2.addDataDependency(transcodeTaskBuilder, Status.SUCCESS)
sendStudyWS2.addDataDependency(morphStudyTaskBuilder, Status.SUCCESS)
sendStudyWS2.setCallingAe(routerAE)
sendStudyWS2.setDestination(nigelWS)

// Clean storage
cleanStorageLocationTaskBuilder.addDataDependency(sendStudyTaskBuilder, Status.SUCCESS)
cleanStorageLocationTaskBuilder.addDataDependency(sendStudyWS1, Status.SUCCESS)
cleanStorageLocationTaskBuilder.addDataDependency(sendStudyWS2, Status.SUCCESS)
//cleanStorageLocationTaskBuilder.addDataDependency(transcodeTaskBuilder, Status.SUCCESS)
cleanStorageLocationTaskBuilder.addDataDependency(morphStudyTaskBuilder, Status.SUCCESS)
cleanStorageLocationTaskBuilder.addDataDependency(receiveImageTaskBuilder, Status.SUCCESS)

// Run workflow
def case1 = new GenericWorkflow.Builder("Update Patient ID and send to VNA.")
case1.addTaskBuilder(receiveImageTaskBuilder)
case1.addTaskBuilder(morphStudyTaskBuilder)
//case1.addTaskBuilder(transcodeTaskBuilder)
case1.addTaskBuilder(sendStudyTaskBuilder)
case1.addTaskBuilder(sendStudyWS1)
case1.addTaskBuilder(sendStudyWS2)
case1.addTaskBuilder(cleanStorageLocationTaskBuilder)
return case1.build()

}

log.info("routing.groovy: Ending routing script. This message should not appear")
