import java.time.LocalDateTime
import java.time.temporal.ChronoUnit
import java.time.format.DateTimeFormatter
import java.time.format.DateTimeParseException

def scriptname = "router/in.groovy"
log.debug("{} starting", scriptname)

def issuerOfPatientId = get(IssuerOfPatientID)
def universalEntityID = get("IssuerOfPatientIDQualifiersSequence/UniversalEntityID")
def universalEntityIDType = get("IssuerOfPatientIDQualifiersSequence/UniversalEntityIDType")

if (issuerOfPatientId == null) {
    issuerOfPatientId = 'LSP'
    set(IssuerOfPatientID, issuerOfPatientId)
}

log.debug("{}: IssuerOfPatientId is {} ", scriptname, issuerOfPatientId)

def (namespace, domain, type) = issuerOfPatientId.tokenize("&")
log.debug("{}: namespace = {}, domain = {}, type = {}", scriptname, namespace, domain, type)

if (namespace != "") {
    set(IssuerOfPatientID, namespace)
}

if (universalEntityID == null && domain == null) {
    set("IssuerOfPatientIDQualifiersSequence/UniversalEntityID", 'ad.lungscreen.com')
} else if (universalEntityID == null && domain != null){
    set("IssuerOfPatientIDQualifiersSequence/UniversalEntityID", domain)
}

if (universalEntityIDType == null && type == null) {
    set("IssuerOfPatientIDQualifiersSequence/UniversalEntityIDType", 'ISO')
} else if (universalEntityIDType == null && type != null) {
    set("IssuerOfPatientIDQualifiersSequence/UniversalEntityIDType", type)
}

log.debug("{} ending", scriptname)

