import java.time.LocalDateTime
import java.time.temporal.ChronoUnit
import java.time.format.DateTimeFormatter
import java.time.format.DateTimeParseException

import org.dcm4che2.data.DicomObject
import org.dcm4che2.data.Tag.*

import org.joda.time.DateTime
import org.joda.time.LocalDate
// import java.time.LocalDateTime

import com.karos.rtk.common.Pid
import com.karos.rtk.pixpdq.PDQConsumer
import com.karos.rtk.pixpdq.PDQuery
import com.karos.rtk.pixpdq.PDQResult
import com.karos.rtk.pixpdq.hl7v2.HL7v2PDQConsumer
import static org.apache.commons.lang.StringUtils.getLevenshteinDistance

def scriptname = "updatePID.groovy"
log.debug("{} starting", scriptname)

def morpherPath = '/home/rialto/rialto/etc/services/dicom-router/script/'
def pdqIp = 'localhost'
def pdqPort = 2399
def localDomain = 'LSA'
def localUniversalEntity = 'ad.lungscreen.com'
def govId = 'HID'
def govUniversalEntity = 'health.gov.au'
def origId = 'OLD'
def origUniversalEntity = 'original.domain'

def callingAE = getCallingAETitle()
def calledAE = getCalledAETitle()

def patientID = get(PatientID)
def issuerOfPatientId = (get(IssuerOfPatientID) ?: origId)
def universalEntityID = (get("IssuerOfPatientIDQualifiersSequence/UniversalEntityID") ?: origUniversalEntity)
def universalEntityIDType = (get("IssuerOfPatientIDQualifiersSequence/UniversalEntityIDType") ?: 'ISO')

def patientName = get(PatientName).tokenize('^')

def tempBirthDate = get(PatientBirthDate)
LocalDate patientBirthDate = new LocalDate(tempBirthDate.substring(0,4) + '-' + tempBirthDate.substring(4,6) + '-' + tempBirthDate.substring(6,8))

def patientSex = get(PatientSex)
def studyUID = get(StudyInstanceUID)
def studyDescription = get(StudyDescription)

log.debug("{}: Inputs\nCalling AE: {}, Called AE: {}\nPatient ID: {}^^^{}&{}&{}, Patient Name: {}, Patient Sex: {}, Patient Birth Date: {}, Study Description: {}", scriptname, callingAE, calledAE, patientID, issuerOfPatientId, universalEntityID, universalEntityIDType,  patientName, patientSex, patientBirthDate, studyDescription)

File mappingFile = new File('/home/rialto/rialto/var/tmp/' + get(StudyInstanceUID) + '.qry')

if (!studyDescription == null && studyDescription.toUpperCase().startsWith('QCC:')) {
    studyDescription = studyDescription.substring(4, studyDescription.length())
    
    def uuidString = UUID.randomUUID().toString()
    def uidString = ''
    def uidRoot = '1.2.840.113747.20080222'

    uuidString.each { digit ->
        if (digit == '-') {
            uidString += '.'
        }
        else {
            uidString += Integer.parseInt(digit, 16).toString()
        }
    }
    def newUID = uidRoot + '.' + uidString

    set(StudyDescription, studyDescription)
    set(StudyInstanceUID, newUID) 
}


if(!mappingFile.exists()) {
    // If mapping file does not exist, made PDQ call and create it
    PDQConsumer pdqConsumer = new HL7v2PDQConsumer(log, pdqIp, pdqPort, null) //last is encryption params
    PDQuery query = new PDQuery()
    query.familyName = patientName[0]
    query.givenName = patientName[1]
    query.sex = patientSex
    query.birthDate = patientBirthDate
    PDQResult pdqResult = pdqConsumer.findPatients(query);
    log.debug("{}: PDQ Result\n{}", scriptname, pdqResult)


    // Levenshtein matching
    def weightedFields = [
        LastName : 30,
        FirstName  : 10,
        PatientBirthDate: 30,
        PatientSex  : 30,
    ]
    def THRESHOLD = 90
    def levScores = []
    def studyValues = [
        LastName: patientName[0], 
        FirstName: patientName[1], 
        PatientBirthDate: patientBirthDate.toString(),
        PatientSex: patientSex
    ]

    for (candidate in pdqResult.getResults()) {
        log.debug("{}: Inpecting candidate\n{}", scriptname, candidate)
        def levScoreForCandidate = 0

        def candidateValues = [
            LastName: candidate.getNames()[0].getFamilyName(), 
            FirstName: candidate.getNames()[0].getGivenName(), 
            PatientBirthDate: candidate.getBirthDate().toString(),
            PatientSex: candidate.getSex()
        ]
        log.debug("{}: Candidate Values: {}", scriptname, candidateValues)

        weightedFields.each { field ->
            def studyValue = studyValues.get(field.key)
            def candidateValue = candidateValues.get(field.key)
            if (studyValue != null && candidateValue != null) {
                def lev = getLevenshteinDistance(studyValue, candidateValue)
                def weight = field.value
                def maxScore = studyValue.size()

                def score = ((maxScore - lev).div(maxScore)) * weight
                log.debug("{}: Levenshtein between {} and {} is {} (weighted score={})", scriptname, studyValue, candidateValue, lev, score)
                levScoreForCandidate += score
            }
        }

        log.debug("{}: Total score for candidate is: {}", scriptname, levScoreForCandidate);
        levScores += levScoreForCandidate    
    }

    def highestLev = 0
    def index = -1
    for (int i = 0; i < levScores.size(); i++) {
        if (levScores[i] > THRESHOLD) {
            if (levScores[i] > highestLev) {
                highestLev = levScores[i]
                index = i
            }
        }
    }

    if (index == -1) { // shouldn't happen since Router made this check but being safe
        log.warn("{}: Local patient not found. Study:Instance {}:{} will not be saved.", scriptname, get(StudyInstanceUID), get(SOPInstanceUID))
        return false
    }

    mappingFile.deleteOnExit()

    log.debug("{}: Temp file: {}", scriptname, mappingFile.getAbsolutePath())

    pdqResult.getResults()[index].getPids().each { identifier ->
        log.debug("{}: Idenfier found: {}.", scriptname, identifier)
        mappingFile << identifier.toString() + '\n'
    }
}

def patientIdentifiers = []

mappingFile.eachLine { identifier ->
    patientIdentifiers << identifier
}

// put all identifiers in one array then work out which are primary / secondary
patientIdentifiers << patientID + '^^^' + issuerOfPatientId + '&' + universalEntityID + '&' + universalEntityIDType

get(OtherPatientIDsSequence).each { otherID ->
    patientIdentifiers << otherID.get(PatientID) + '^^^' + otherID.get(IssuerOfPatientID) + '&' + otherID.get("IssuerOfPatientIDQualifiersSequence/UniversalEntityID") + '&' + otherID.get("IssuerOfPatientIDQualifiersSequence/UniversalEntityIDType")
}
patientIdentifiers.unique()

log.info("{}: Identifiers found:\n{}", scriptname, patientIdentifiers)

remove(OtherPatientIDsSequence)
def otherIDCount = 1
patientIdentifiers.each { identifier ->
    def identifiers = identifier.tokenize('^&')
    log.info("{}: Processing identifier: {}", scriptname, identifiers)
    if ((identifiers[1] == localDomain) && (identifiers[2] == localUniversalEntity)) {
        set(PatientID, identifiers[0])
        set(IssuerOfPatientID, identifiers[1])
        set("IssuerOfPatientIDQualifiersSequence/UniversalEntityID", identifiers[2])
        set("IssuerOfPatientIDQualifiersSequence/UniversalEntityIDType", identifiers[3])

    } else {
        set("OtherPatientIDsSequence[" + otherIDCount + "]/PatientID", identifiers[0])
        set("OtherPatientIDsSequence[" + otherIDCount + "]/IssuerOfPatientID", identifiers[1])
        set("OtherPatientIDsSequence[" + otherIDCount + "]/IssuerOfPatientIDQualifiersSequence/UniversalEntityID", identifiers[2])
        set("OtherPatientIDsSequence[" + otherIDCount + "]/IssuerOfPatientIDQualifiersSequence/UniversalEntityIDType", identifiers[3])
        otherIDCount += 1
    }
}

log.info("{}: New Patient ID:{}^^^{}&{}&{}\nOther Patient IDs\n{}", scriptname, get(PatientID), get(IssuerOfPatientID), get("IssuerOfPatientIDQualifiersSequence/UniversalEntityID"), get("IssuerOfPatientIDQualifiersSequence/UniversalEntityIDType"), get(OtherPatientIDsSequence))

//keep code to calculate patientAge

def patientDateOfBirth = get(PatientBirthDate)
def patientTimeOfBirth = get(PatientBirthTime)
def studyDate = get(StudyDate)
def studyTime = get(StudyTime)
def patientAge = get(PatientAge)

try {
    if (patientAge == null) {
        set("PatientAge", calculateAge(patientDateOfBirth, patientTimeOfBirth, studyDate, studyTime))
    }
} catch (Exception e) {
    log.warn("Failed to compute PatientAge. Proceeding without changing PatientAge. ", e)
}


String calculateAge(String patientDateOfBirth, String patientTimeOfBirth, String studyDate, String studyTime){
    def TIPPING_WEEKS_TO_DAYS = 4
    def TIPPING_MONTHS_TO_WEEKS = 3
    def TIPPING_YEARS_TO_MONTHS = 2
    
    if (patientDateOfBirth == null || studyDate == null) {
        return null
    }

    patientDateOfBirth = patientDateOfBirth.replace(":", "")
    studyDate = studyDate.replace(":", "")

    def patientDateTimeOfBirth
    def studyDateTime

    try {
        def dtf = DateTimeFormatter.ofPattern("yyyyMMddHHmmss")
        patientDateTimeOfBirth = LocalDateTime.parse(patientDateOfBirth + clea(patientTimeOfBirth), dtf)
        studyDateTime = LocalDateTime.parse(studyDate + clea(studyTime), dtf)
    } catch (DateTimeParseException e) {
        log.warn("in.groovy: Failed to parse PatientBirthDate or StudyDate.", e)
        return null
    }

    if (!patientDateTimeOfBirth.isBefore(studyDateTime)) {
        return null
    }

    def years = ChronoUnit.YEARS.between(patientDateTimeOfBirth, studyDateTime)
    def months = ChronoUnit.MONTHS.between(patientDateTimeOfBirth, studyDateTime)
    def weeks = ChronoUnit.WEEKS.between(patientDateTimeOfBirth, studyDateTime)
    def days = ChronoUnit.DAYS.between(patientDateTimeOfBirth, studyDateTime)
    def age = ""

    if (years > 999) {
        return null
    }
    if (years >= TIPPING_YEARS_TO_MONTHS) {
        age = years + "Y"
    } else if (months >= TIPPING_MONTHS_TO_WEEKS) {
        age = months + "M"
    } else if (weeks >= TIPPING_WEEKS_TO_DAYS) {
        age = weeks + "W"
    } else {
        age = days + "D"
    }

    return age.padLeft(4, "0")
}

String cleanTime(String str) {
    if (str == null) {
        return "000000"
    }
    str = str.replace(":", "") 
    if (str.indexOf(".") == 6) {
        str = str.substring(0, 6)
    }
    if (str.contains(".") || str.length() > 6 || str.length()%2 == 1) {
        return "000000"
    }
    return str.padRight(6, "0")
}

log.debug("{} ending", scriptname)

