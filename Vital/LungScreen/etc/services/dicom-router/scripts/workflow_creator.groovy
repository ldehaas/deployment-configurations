LOAD('router_workflow_builder.groovy')

import com.karos.rialto.workflow.model.Task.TaskPriority;

def scriptname = "workflow_creator.groovy"
log.debug("{} starting", scriptname)

def builder = new RouterWorkflowBuilder(
    'var/router', 
    getCalledAETitle(),
    getCallingAETitle(),
    get(StudyInstanceUID))

builder.setInboundMorpher("etc/services/dicom-router/scripts/in.groovy")

builder.addDestination(calledAe: getCalledAETitle())

log.debug("{} ending", scriptname)
return builder.build()
