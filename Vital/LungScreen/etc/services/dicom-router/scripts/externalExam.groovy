//              externalExam.groovy v03
//              If Accession Number is empty generates one using reversed MRN
//              Sets Issuer of patient id and universal entity on study if blank.
//              Applies 'OFX_' prefix to Accession Number
//              Sets Study Description and Procedure Code to dummy value
log.info("externalExam.groovy v03: Starting external exam localizer script. ")

def scriptname = "router/externalExam.groovy"


def issuerOfPatientId = (get(IssuerOfPatientID) ?: 'NotProvided')
def universalEntityID = get("IssuerOfPatientIDQualifiersSequence/UniversalEntityID")
def universalEntityIDType = get("IssuerOfPatientIDQualifiersSequence/UniversalEntityIDType")
def accessionNumber = get(AccessionNumber)
def patientName = get(PatientName)
def sopClassUid = get(SOPClassUID)


// MENGLER: Added this as there were a plethora of studies coming in with spaces after the patient last name which was causing a matching issue.
log.info("{}: Testing if Patient Name Needs to be fixed - Patient Name {}.",scriptname, patientName)
patientName = patientName.replaceAll('\\ \\^','\\^')
log.info("{}: Patient Name Fixed - if required {}.",scriptname, patientName)
set(PatientName, patientName)

// MENGLER: Studies with SOPClassUID 1.2.840.10008.5.1.4.1.1.88.67/X-Ray Radiation Dose SR Storage are not supported by DS/VV and cause send to fail.
if (sopClassUid == '1.2.840.10008.5.1.4.1.1.88.67') {
    log.debug("{}: Not forwarding SOP {} because it has SOP Class UID {}", scriptname, get(SOPInstanceUID), sopClassUid)
    return false
}


if (accessionNumber == null) {
        accessionNumber = get(StudyInstanceUID).reverse().substring(0,13)
} else if (accessionNumber.length() > 13) {
    accessionNumber = accessionNumber.substring(0,13)
}



def modalityOffset = 0
def modality = get(Modality)
 
if (modality == 'MR') {
        modalityOffset = 5
} else if (modality == 'CR' || modality == 'XA' || modality == 'DX') {
        modalityOffset = 10
} else if (modality == 'PT') {
        modalityOffset = 15
} else if (modality == 'US') {
        modalityOffset == 20
}
 
def procedureCodes = [
        ['B020ZZZ', 'I10P', 'Computerized Tomography (CT Scan) of Brain'],
        ['B02BZZZ', 'I10P', 'Computerized Tomography (CT Scan) of Spinal Cord'],
        ['B420ZZZ', 'I10P', 'Computerized Tomography (CT Scan) of Abdominal Aorta'],
        ['B52SZZZ', 'I10P', 'Computerized Tomography (CT Scan) of Bilateral Pulmonary Artery'],
        ['BF25ZZZ', 'I10P', 'Computerized Tomography (CT Scan) of Liver'],
        ['B2000ZZ', 'I10P', 'Plain Radiography of Single Coronary Artery using High Osmolar Contrast'],
        ['B205YZZ', 'I10P', 'Plain Radiography of Left Heart using Other Contrast'],
        ['B00BZZZ', 'I10P', 'Plain Radiography of Spinal Cord'],
        ['B300ZZZ', 'I10P', 'Plain Radiography of Thoracic Aorta'],
        ['B30SZZZ', 'I10P', 'Plain Radiography of Right Pulmonary Artery'],
        ['B030ZZZ', 'I10P', 'Magnetic Resonance Imaging (MRI) of Brain'],
        ['B434ZZZ', 'I10P', 'Magnetic Resonance Imaging (MRI) of Superior Mesenteric Artery'],
        ['B03BZZZ', 'I10P', 'Magnetic Resonance Imaging (MRI) of Spinal Cord'],
        ['B532ZZZ', 'I10P', 'Magnetic Resonance Imaging (MRI) of Intracranial Sinuses'],
        ['B231Y0Z', 'I10P', 'Magnetic Resonance Imaging (MRI) of Multiple Coronary Arteries using Other Contrast, Unenhanced and Enhanced'],
        ['C030BZZ', 'I10P', 'Positron Emission Tomographic (PET) Imaging of Brain using Carbon 11 (C-11)'],
        ['C03YYZZ', 'I10P', 'Positron Emission Tomographic (PET) Imaging of Central Nervous System using Other Radionuclide'],
        ['C23GMZZ', 'I10P', 'Positron Emission Tomographic (PET) Imaging of Myocardium using Oxygen 15 (O-15)'],
        ['C23YYZZ', 'I10P', 'Positron Emission Tomographic (PET) Imaging of Heart using Other Radionuclide'],
        ['CB32KZZ', 'I10P', 'Positron Emission Tomographic (PET) Imaging of Lungs and Bronchi using Fluorine 18 (F-18)'],
        ['B04BZZZ', 'I10P', 'Ultrasonography of Spinal Cord'],
        ['B24CZZZ', 'I10P', 'Ultrasonography of Pericardium'],
        ['B440ZZZ', 'I10P', 'Ultrasonography of Abdominal Aorta'],
        ['B24DYZZ', 'I10P', 'Ultrasonography of Pediatric Heart using Other Contrast'],
        ['B347ZZZ', 'I10P', 'Ultrasonography of Left Internal Carotid Artery']
]
 

def studyDescription = get(StudyDescription)
    if ((studyDescription == null) || (studyDescription.isEmpty())) {
        set(StudyDescription, "No Value Defined")
        log.error("{}: No Study Description Tag: Setting to {}.",scriptname, StudyDescription)
    }
def pCodeSequence = get(ProcedureCodeSequence)
remove(ProcedureCodeSequence)
if ((pCodeSequence == null)) {
    set('ProcedureCodeSequence/CodeValue', 'B030ZZZ')
    set('ProcedureCodeSequence/CodingSchemeDesignator', "I10P")
    set('ProcedureCodeSequence/CodeMeaning', '8675309')
}
if ((pCodeSequence[0] == null)) {
    set('ProcedureCodeSequence/CodeValue', 'B030ZZZ')
}
if ((pCodeSequence[1] == null)) {
    set('ProcedureCodeSequence/CodingSchemeDesignator', 'I10P')
}
if ((pCodeSequence[2] == null)) {
    set('ProcedureCodeSequence/CodeMeaning', '8675309')
}

if ((issuerOfPatientId == 'NotProvided')) {
   set(IssuerOfPatientID, issuerOfPatientId)
}

log.debug("{}: Final procedure code - CodeValue: {}, Designator: {}, Meaning: {}",scriptname, get('ProcedureCodeSequence/CodeValue'), get('ProcedureCodeSequence/CodingSchemeDesignator'), get('ProcedureCodeSequence/CodeMeaning'))

/*
set(StudyID, '')
set(AccessionNumber, 'OFX' + accessionNumber)
set(InstitutionName, 'External Hospital')
log.debug("externalExam.groovy: New Accession Number: {}", get(AccessionNumber))  */
