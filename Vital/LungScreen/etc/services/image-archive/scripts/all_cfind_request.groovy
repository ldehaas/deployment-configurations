log.debug("cfindRequest.groovy v01: Start inbound morpher")

def patientDomain = 'LSA'
def patientDomainUID = 'ad.lungscreen.com'
log.debug("cfindRequest.groovy: Called AE is {}.", getCallingAETitle())

if (get(PatientID)) { // Only adds domain if the query contains a patient ID
        set(IssuerOfPatientID, patientDomain)
        set("IssuerOfPatientIDQualifiersSequence/UniversalEntityID", patientDomainUID)
        set("IssuerOfPatientIDQualifiersSequence/UniversalEntityIDType",'ISO') 
}
