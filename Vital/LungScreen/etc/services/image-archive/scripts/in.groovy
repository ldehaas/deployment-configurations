import java.time.LocalDateTime
import java.time.temporal.ChronoUnit
import java.time.format.DateTimeFormatter
import java.time.format.DateTimeParseException

def scriptname = "image-archive/in.groovy"
log.debug("{} starting", scriptname)

def issuerOfPatientId = get(IssuerOfPatientID)
def universalEntityID = get("IssuerOfPatientIDQualifiersSequence/UniversalEntityID")
def universalEntityIDType = get("IssuerOfPatientIDQualifiersSequence/UniversalEntityIDType")

if (issuerOfPatientId == null) {
    issuerOfPatientId = 'LSA'
    set(IssuerOfPatientID, issuerOfPatientId)
}

log.debug("{}: IssuerOfPatientId is {} ", scriptname, issuerOfPatientId)

def (namespace, domain, type) = issuerOfPatientId.tokenize("&")
log.debug("{}: namespace = {}, domain = {}, type = {}", scriptname, namespace, domain, type)

if (namespace != "") {
    set(IssuerOfPatientID, namespace)
}

if (universalEntityID == null && domain == null) {
    if (get(IssuerOfPatientID) == "LSA") {
        set("IssuerOfPatientIDQualifiersSequence/UniversalEntityID", 'ad.lungscreen.com')
    } else {
        set("IssuerOfPatientIDQualifiersSequence/UniversalEntityID", 'ad.lungscreen.com')
    }
} else if (universalEntityID == null && domain != null){
    set("IssuerOfPatientIDQualifiersSequence/UniversalEntityID", domain)
}

if (universalEntityIDType == null && type == null) {
    set("IssuerOfPatientIDQualifiersSequence/UniversalEntityIDType", 'ISO')
} else if (universalEntityIDType == null && type != null) {
    set("IssuerOfPatientIDQualifiersSequence/UniversalEntityIDType", type)
}

def patientDateOfBirth = get(PatientBirthDate)
def patientTimeOfBirth = get(PatientBirthTime)
def studyDate = get(StudyDate)
def studyTime = get(StudyTime)
def patientAge = get(PatientAge)

try {
    if (patientAge == null) {
        set("PatientAge", calculateAge(patientDateOfBirth, patientTimeOfBirth, studyDate, studyTime))
    }
} catch (Exception e) {
    log.warn("{}: Failed to compute PatientAge. Proceeding without changing PatientAge. ", scriptname, e)
}

String calculateAge(String patientDateOfBirth, String patientTimeOfBirth, String studyDate, String studyTime){
    def TIPPING_WEEKS_TO_DAYS = 4
    def TIPPING_MONTHS_TO_WEEKS = 3
    def TIPPING_YEARS_TO_MONTHS = 2
    
    if (patientDateOfBirth == null || studyDate == null) {
        return null
    }

    patientDateOfBirth = patientDateOfBirth.replace(":", "")
    studyDate = studyDate.replace(":", "")

    def patientDateTimeOfBirth
    def studyDateTime

    try {
        def dtf = DateTimeFormatter.ofPattern("yyyyMMddHHmmss")
        patientDateTimeOfBirth = LocalDateTime.parse(patientDateOfBirth + cleanTime(patientTimeOfBirth), dtf)
        studyDateTime = LocalDateTime.parse(studyDate + cleanTime(studyTime), dtf)
    } catch (DateTimeParseException e) {
        log.warn("{}: Failed to parse PatientBirthDate or StudyDate.", scriptname, e)
        return null
    }

    if (!patientDateTimeOfBirth.isBefore(studyDateTime)) {
        return null
    }

    def years = ChronoUnit.YEARS.between(patientDateTimeOfBirth, studyDateTime)
    def months = ChronoUnit.MONTHS.between(patientDateTimeOfBirth, studyDateTime)
    def weeks = ChronoUnit.WEEKS.between(patientDateTimeOfBirth, studyDateTime)
    def days = ChronoUnit.DAYS.between(patientDateTimeOfBirth, studyDateTime)
    def age = ""

    if (years > 999) {
        return null
    }
    if (years >= TIPPING_YEARS_TO_MONTHS) {
        age = years + "Y"
    } else if (months >= TIPPING_MONTHS_TO_WEEKS) {
        age = months + "M"
    } else if (weeks >= TIPPING_WEEKS_TO_DAYS) {
        age = weeks + "W"
    } else {
        age = days + "D"
    }

    return age.padLeft(4, "0")
}

String cleanTime(String str) {
    if (str == null) {
        return "000000"
    }
    str = str.replace(":", "") 
    if (str.indexOf(".") == 6) {
        str = str.substring(0, 6)
    }
    if (str.contains(".") || str.length() > 6 || str.length()%2 == 1) {
        return "000000"
    }
    return str.padRight(6, "0")
}

log.debug("{} ending", scriptname)
