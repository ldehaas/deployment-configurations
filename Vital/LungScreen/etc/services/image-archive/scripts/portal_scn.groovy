import java.text.SimpleDateFormat

def scriptname="portal-scn.groovy"
log.debug("{} starting", scriptname)

def sdf = new SimpleDateFormat("yyyyMMddHHmmss")

initialize( 'ORM', 'O01', '2.3' );
//output.getMessage().addNonstandardSegment('IPC')

log.debug("input is {}", input);
log.debug("Calling AE Title is {}", getCallingAETitle());
//
//set('MSH-7', '20130827132217')
set('MSH-7', sdf.format(new Date()))
setPersonName('PID-5', input.get(PatientName));
set('PID-3-1', input.get(PatientID));

def namespace = ""
def universalId = ""
def universalIdType = ""
def issuerOfPatientID = input.get(IssuerOfPatientID)
log.debug("issuerOfPatientID = {}", issuerOfPatientID)

if (issuerOfPatientID != null && issuerOfPatientID.contains("&")) {
    parts = issuerOfPatientID.split('&')
    if (parts.length == 3) {
        domain = com.karos.rtk.common.Domain.parse(issuerOfPatientID)
        namespace = domain.namespaceID
        universalId = domain.domainUUID
        universalIdType = domain.domainUUIDtype
    }
} else {
    namespace = issuerOfPatientID
}

set('PID-3-4-1', namespace)
set('PID-3-4-2', universalId)
set('PID-3-4-3', universalIdType)

if (type != null && type.toLowerCase().contains("delete")) {
    set('ORC-1', 'DC')
} else {
    set('ORC-1', 'NW')
}

//
set('OBR-1', '1')
set('OBR-3-1', input.get(AccessionNumber))
set('OBR-3-2', namespace)
set('OBR-4-1', input.get(RequestedProcedureID))

def studyDescription = input.get(StudyDescription)
if (studyDescription != null) {
    studyDescription = studyDescription.replaceAll("\\^.*", "")
    set('OBR-4-2', studyDescription)
}

set('OBR-7', input.get(StudyDate))
set('OBR-25', 'I')
set('OBX-1', '1')
set('OBX-2', 'TX')
set('OBX-3-1-2', 'GDT')
set('OBX-5', 'Y')

set('OBX-9', input.get(StudyInstanceUID))

log.debug("{} ending", scriptname)
