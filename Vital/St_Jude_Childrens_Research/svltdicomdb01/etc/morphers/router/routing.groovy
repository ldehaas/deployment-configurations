def called_AET = getCalledAETitle().toUpperCase();

if (called_AET.equals("REVOLUTION")){
	
    def checkSlice = get(SliceThicknes)

    if (checkSlice > 0.625) {
        return "etc/dataflows/thickslice.xml"
    } else {
        return "etc/dataflows/thinslice.xml"
    }
}
