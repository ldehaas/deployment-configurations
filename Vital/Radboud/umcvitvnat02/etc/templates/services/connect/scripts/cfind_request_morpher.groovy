/*
 * cfind_request_morpher.groovy
 *
 * This script is used in the 
 * <prop name="Sources">/<dicom>/<cfindRequestMorpher> tag.
 *
 * This morpher can be configured on each DICOM source to customize the values 
 * of the C-FIND request object. This morpher is run on the C-FIND created in  
 * the <prop name="FetchPriors">/<hl7ToCFindMorpher> or for adhoc C-FIND 
 * reuqests. Use this morpher to either add or remove query parameters before 
 * sending the C-FIND to the source.
 *
 * To access the DICOM C-FIND object, use the "set()" method. In order to set a 
 * query parameter value in the DICOM C-FIND object set the value to what you 
 * wish to query by.
 * For example, to set the value of PatientId: 
 * 
 * set("PatientId","12345")
 * OR
 * set(PatientId,"12345")
 *
 * In order to request information be returned in the C-FIND response from 
 * the source, set the value to be blank or null.
 * For example, to request StudyDate from the source:
 * 
 * set("StudyDate",null)
 * OR
 * set(StudyDate,"")
 *
 * This script may optionally return a boolean value (either true or false). If
 * the return value of the script is false then no C-FIND will be sent to the
 * DICOM source that this morpher is configured for.
 */

log.info("cfind_request_morpher.groovy: Start HL7 to CFind Request morpher")

set(StudyDate, null)

log.info("cfind_request_morpher.groovy: Ending HL7 to CFind Request morpher")
