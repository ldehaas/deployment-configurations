log.info("Here's how the order message looks:]\n{}", input)
log.info("And here's how the initial Imaging Service Request looks like\n{}", imagingServiceRequest)

def patientIdentification = imagingServiceRequest.getPatientIdentification()

log.info("Issuer of PID is updating with \n{}", patientIdentification.getPatientIdUniversalId())

imagingServiceRequest.setPatientIdentification(patientIdentification)

def patientDemographics = imagingServiceRequest.getPatientDemographics()
//patientDemographics.setPatientBirthDate(get("PID-6"))
imagingServiceRequest.setPatientDemographics(patientDemographics)

def requestedProcedure = imagingServiceRequest.getRequestedProcedureSequence().get(0)
requestedProcedure.setRequestedProcedureDescription(get("OBR-5"))
requestedProcedure.setRequestedProcedureID(get("OBR-4-1"))
//requestedProcedure.setStudyInstanceUID(get("ZDS-1"))

def scheduledProcedureStep = requestedProcedure.getScheduledProcedureStepSequence().get(0)
//scheduledProcedureStep.setScheduledProcedureStepStatus(get("OBR-1"))
scheduledProcedureStep.setScheduledProcedureStepLocation(get("OBR-2"))
scheduledProcedureStep.setScheduledStationName(get("OBR-4"))
if (scheduledProcedureStep.getScheduledProcedureStepLocation() == null) {
    scheduledProcedureStep.setScheduledProcedureStepLocation("DEFAULT_LOCATION_CT");
}
if (get("ORC-1") == "CA") {
  scheduledProcedureStep.setScheduledProcedureStepStatus("CA")
}

scheduledProcedureStep.setModality(get("OBR-3"))
if (scheduledProcedureStep.getModality() == null) {
    scheduledProcedureStep.setModality("CT");
}

log.info("Finished fixing up the Imaging Service Request...Here it is\n{}", imagingServiceRequest)
