<?xml version="1.0"?>
<config>

    <server type="http" id="http-navigator">
        <port>2526</port>
    </server>

    <server type="http" id="http-admin-tools">
        <port>2527</port>
    </server>

    <!-- PLUGIN: External Viewer (Download) -->
    <var name="DownloadPlugin-Name" value="External Viewer (Download)" />
    <var name="DownloadPlugin-Description" value="External platform-specific viewer or download to local disk" />
    <service id="navigator-plugin-download" type="navigator.plugin.download">

        <device idref="pix" />
        <device idref="pdq" />
        <device idref="xdsrep"/>
        <device idref="xdsreg"/>
        <device idref="cassandra-dc1" />

        <config>
            <prop name="CrossAffinityDomainID" value="${System-AffinityDomain}"/>
            <prop name="ForcedMimeTypeFileExtension">
                <prop mimeType="application/vnd.ms-excel" fileExtension="xls"/>
            </prop>
            <prop name="CassandraConfiguration">
                <keyspaceName>${CassandraKeyspacePrefix}navigator</keyspaceName>
                <keyspaceReplicationStrategy>NetworkTopologyStrategy</keyspaceReplicationStrategy>
                <keyspaceReplicationStrategyOptions>${CassandraReplicationOption}</keyspaceReplicationStrategyOptions>
                <keyspaceCreateEnabled>true</keyspaceCreateEnabled>
                <dynamicConsistencyLevelEnabled>false</dynamicConsistencyLevelEnabled>
                <schemaUpdateEnabled>false</schemaUpdateEnabled>
            </prop>
            <prop name="web-api-path" value="/plugins/download"/>
            <prop name="UserManagementURL" value="${Usermanagement-AuthenticatedURL}"/>

            <include location="admin-tools/patientidentitydomains.xml"/>
            <include location="admin-tools/cacheStorage.xml"/>
        </config>
    </service>

    <!-- PLUGIN: CDA Viewer   -->
    <var name="CDAPlugin-Name" value="CDA Viewer" />
    <var name="CDAPlugin-Description" value="HL7 CDA (Clinical Document Architecture) Viewer" />
    <service id="navigator-plugin-cda" type="navigator.plugin.cda">
        <device idref="pix" />
        <device idref="pdq" />
        <device idref="xdsrep"/>
        <device idref="xdsreg"/>
        <device idref="cassandra-dc1" />

        <config>
            <prop name="CrossAffinityDomainID" value="${System-AffinityDomain}"/>
            <prop name="CassandraConfiguration">
                <keyspaceName>${CassandraKeyspacePrefix}navigator</keyspaceName>
                <keyspaceReplicationStrategy>NetworkTopologyStrategy</keyspaceReplicationStrategy>
                <keyspaceReplicationStrategyOptions>${CassandraReplicationOption}</keyspaceReplicationStrategyOptions>
                <keyspaceCreateEnabled>true</keyspaceCreateEnabled>
                <dynamicConsistencyLevelEnabled>false</dynamicConsistencyLevelEnabled>
                <schemaUpdateEnabled>false</schemaUpdateEnabled>
            </prop>
            <prop name="web-api-path" value="/plugins/cda"/>
            <prop name="DefaultStylesheet" value="${rialto.rootdir}/etc/services/admin-tools/plugins/cda/default.xsl"/>
            <prop name="UserManagementURL" value="${Usermanagement-AuthenticatedURL}"/>

            <include location="admin-tools/patientidentitydomains.xml"/>
            <include location="admin-tools/cacheStorage.xml"/>
        </config>
    </service>

    <service id="rialto-ui" type="rialto.ui">
        <device idref="pix" />
        <device idref="pdq" />

        <server idref="http-navigator" name="web-ui">
            <!-- Not configurable at this time -->
            <url>/</url>
        </server>

        <server idref="http-admin-tools" name="admin-tools">
            <!-- Not configurable at this time -->
            <url>/v2/</url>
        </server>

        <server idref="http-authenticated" name="web-api">
            <url>/public/*</url>
        </server>

        <config>
            <prop name="CrossAffinityDomainID" value="${System-AffinityDomain}"/>
            <prop name="ApiURL" value="${Navigator-HostProtocol}://${Navigator-Host}:${Navigator-APIPort}${Navigator-RootPath}/api${Navigator-APIBasePath}"/>
            <prop name="ArrURL" value="${Navigator-HostProtocol}://${Navigator-Host}:${ARR-GUIPort}"/>
            <prop name="StudyManagementURL" value="http://${HostIP}:8080${IA-StudyManagement-Path}"/>
            <prop name="WadoURL" value="http://${HostIP}:8080/vault/wado" />
            <prop name="MintApiURL" value="http://${HostIP}:8080/vault/mint" />
            <prop name="ElasticsearchRestApiURL" value="http://localhost:9200" />
            <prop name="DeviceRegistryURL" value="http://${HostIP}:8080/rialto/deviceregistry" />
            <prop name="ModalityWorklistURL" value="${Navigator-HostProtocol}://${Navigator-Host}:${Navigator-APIPort}/rest/api/mwl" />
            <prop name="DicomQcToolsURL" value="http://${HostIP}:8080/vault/qc" />
            <prop name="IlmURL" value="http://${HostIP}:8080/vault/ilm" />
            <prop name="EMPIManagementURL" value="http://${HostIP}:8080/rialto/empi" />
            <prop name="GlobalInactivitySessionTimeout" value="${IdleUserSessionTimeout}" />
            <prop name="ExternalPDQConfigured" value="false" />
            <prop name="RialtoAsACache" value="false" />

            <!--prop name="VIPConfidentialityCode" value="VIP" /-->

            <prop name="RialtoImageArchiveAETitle" value="${IA-AETitle}" />

            <prop name="ElasticsearchSchemaPrefix" value="${WorkflowSchemaPrefix}" />

            <!-- ConnectQueryRetrieveAETitle is used for query/retrieve of catalogued studies
                 This property should be enabled if you want to enable the import of catalogued studies in Rialto Admin tools
                 It's value should match a configured Connect device -->
            <!-- <prop name="ConnectQueryRetrieveAETitle" value="CONNECT" /> -->

            <prop name="SupportedThemes">
                rialto-light
                rialto-dark
                vitreaConnection
            </prop>
            <prop name="DefaultTheme" value="${Navigator-DefaultTheme}"/>

            <prop name="SupportedLocales">
                en
                fr
            </prop>
            <prop name="DefaultLocale" value="${Navigator-DefaultLocale}"/>

            <prop name="BreakTheGlassOptions">
                <BreakTheGlassOption>Emergency access required for patient care.</BreakTheGlassOption>
            </prop>

            <include location="admin-tools/patientidentitydomains.xml"/>

            <prop name="Plugins">
                <plugin>
                    <name>${CDAPlugin-Name}</name>
                    <description>${CDAPlugin-Description}</description>
                    <mimeTypes>text/xml</mimeTypes>
                    <url>${Navigator-HostProtocol}://${Navigator-Host}:${Navigator-APIPort}${Navigator-RootPath}/api/plugins/cda</url>
                    <embedded>true</embedded>
                </plugin>

                <plugin>
                    <name>${DownloadPlugin-Name}</name>
                    <description>${DownloadPlugin-Description}</description>
                    <mimeTypes>*</mimeTypes>
                    <url>${Navigator-HostProtocol}://${Navigator-Host}:${Navigator-APIPort}${Navigator-RootPath}/api/plugins/download</url>
                    <embedded>true</embedded>
                </plugin>
            </prop>

            <!--Un-comment the EnabledConfigurationFeatures property below to enable/disable desired configuration features-->
            <!--by including/not-including them in the property tag. Note: contexts will always be enabled whether-->
            <!--or not it is included in the property tag.-->
            <!--By default, if the EnabledConfigurationFeatures property does not exist-->
            <!--it will set contexts, devices, rules and scripts as enabled by default-->

            <prop name="EnabledConfigurationFeatures">
                devices
                <!-- rules -->
                scripts
                spaces
                routes
                mwlQueryRules
            </prop>

            <!-- Configure server name to display in UI -->
            <!-- prop name="ServerName" value="server" /-->


            <!-- Configure image caching for studies in the UI       -->
            <!-- The value can be one of:                            -->
            <!-- "true": enable the cache with default size (32 MBs) -->
            <!-- "false": disabled caching of images                 -->
            <!-- "number": The store size in megabytes               -->
            <!--
                <prop name="ImageCache">
                    <size>64</size>
                </prop>
            -->

            <!-- Configure eventCodeSchemes to support searching for documents by eventCode -->
            <!--prop name="EventCodeSchemes">
                <scheme schemeID="1.2.3" schemeName="Karos Health demo eventCodes" />
            </prop-->
            <prop name="ProductBranding">
                <productName>Vitrea Connection</productName>
                <companyName>Vital Images</companyName>
                <copyrightDates>2018</copyrightDates>
                <contactAddress>5850 Opus Parkway, Suite 300, Minnetonka, MN 55343, USA</contactAddress>
                <contactPhoneNumber>866.433.4624</contactPhoneNumber>
                <contactEmail>support@vitalimages.com</contactEmail>
                <contactWebsite>www.vitalimages.com</contactWebsite>
                <supportPhoneNumber>800.208.3005</supportPhoneNumber>
                <supportWebsite>www.vitalimages.com/about-us/contact-us</supportWebsite>
            </prop>
        </config>
    </service>
</config>
