log.info("ilm_policy.groovy - START")
import org.joda.time.DateTime
import org.joda.time.Duration

def cutoffDateTime = new org.joda.time.DateTime().minusDays(14)

// NOTE: If the production site contains studies used by the
// health check service, these should never be removed from the system.
// e.g. Check if this is a Test study that is used by HealthCheck service.
// StudyInstanceUIDs below are examples only. Change to match production requirements.
if (study.getStudyInstanceUID() == '1.2.392.200036.9125.2.12345679' || study.getStudyInstanceUID() == '1.2.392.200036.9125.2.987654321') {
    log.info(scriptName + "study with StudyInstanceUID {} is used by HealthCheck service, skipping...", study.getStudyInstanceUID());
    log.debug(scriptName + "END - Done looking at study with StudyInstanceUID {}", study.getStudyInstanceUID());
    return;
}

if (study.isOlderThan(Duration.standardDays(60)) && study.getLastUpdateTime() < cutoffDateTime) {
    log.info("ilm_policy.groovy - physically deleting study {}, with Accession Number {}, study date {}, last update date {}", study.getStudyInstanceUID(), study.get("AccessionNumber"), study.getStudyDateTime(), study.getLastUpdateTime())
    ops.scheduleStudyHardDelete();
}

log.info("ilm_policy.groovy - END")
