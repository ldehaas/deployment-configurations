<?xml version="1.0"?>
<config>

    <server id="hl7-ia-update" type="hl7v2">
        <port>${UpdateStudies-HL7v2Port}</port>
    </server>

    <!-- Image Archive -->
    <service id="image-archive" type="image-archive">

        <!-- Usage of spaces is disabled in favour of using standard file system storage -->
        <!--
        <device idref="space-index" name="space" />
        <device idref="elastic" />
        -->

        <server idref="dicom-main"      name="CStoreSCP" />
        <server idref="dicom-main"      name="StgCmtSCP" />
        <server idref="dicom-main"      name="StgCmtSCU" />
        <server idref="dicom-main"      name="CFindSCP" />
        <server idref="dicom-main"      name="CMoveSCP" />
        <server idref="hl7-ia-update"   name="update" />
        <server idref="hl7-ia-update"   name="merge" />
        <server idref="hl7-ia-update"   name="order" />
        <server idref="hl7-ia-update"   name="oru" />

        <server idref="http-rest" name="MINT">
            <url>/vault/mint/*</url>
        </server>

        <server idref="http-rest" name="ILM">
            <url>/vault/ilm/*</url>
        </server>

        <server idref="http-rest" name="QC">
            <url>/vault/qc/*</url>
        </server>

        <server idref="http-rest" name="WADO">
            <url>/vault/wado</url>
        </server>

        <server idref="http-rest" name="StudyManagement">
            <url>${IA-StudyManagement-Path}/*</url>
        </server>

        <server idref="http-rest" name="StowRS">
          <url>/vault/stowrs/*</url>
        </server>

        <device idref="xdsrep" />
        <device idref="xdsreg" />
        <device idref="pix" />
        <device idref="pdq" />
        <!-- pif is not needed
        <device idref="pif" />
        -->
        <device idref="cassandra-dc1" />

        <config>

            <prop name="StorerType" value="FILE" />
            <prop name="Cache" value="${rialto.rootdir}/var/ids1/cache" />
            <prop name="Inbox" value="${rialto.rootdir}/var/ids1/index" />
            <prop name="JobQueue" value="${rialto.rootdir}/var/ids1/jobs" />
            <prop name="QCWorkDirectory" value="${rialto.rootdir}/var/ids1/qc" />
            <prop name="CacheSize" value="10" />
            <prop name="AETitle" value="${IA-AETitle}" />

            <!-- DefaultDomain:  For incoming studies, if Issuer of Patient ID (0010,0021) is empty, the
                 study is assumed to be from this domain.  Format: namespace&domainUID&domainUIDType -->
            <prop name="DefaultDomain" value="${System-DefaultLocalFullyQualifiedDomain}" />
            <prop name="AffinityDomain" value="${System-AffinityFullyQualifiedDomain}" />

            <prop name="IndexerType" value="CQL3" />
            <prop name="IndexerRetryTime" value="1m" />

            <!-- An ILM configuration, for archive mode -->
            <prop name="ImagingLifeCycleManagementConfiguration">
                <Mode>ARCHIVE</Mode>
                <!-- if you change data center name ensure that you also add eligibility script -->
                <DataCenter>DC1</DataCenter>
                <EligibilityScript>${rialto.rootdir}/etc/services/image-archive/scripts/ilm_eligibility.groovy</EligibilityScript>
                <Policy>${rialto.rootdir}/etc/services/image-archive/scripts/ilm_policy.groovy</Policy>
                <FCMDSleepTime>1440m</FCMDSleepTime>
            </prop>

            <!-- An alternative ILM configuration, for cache mode -->
            <!-- For details, see: "Policy script" section of Admin Guide -->
            <!-- 
            <prop name="ImagingLifeCycleManagementConfiguration">
              <Mode>CACHE</Mode>

              <MinASCThresholdRatio>0.26</MinASCThresholdRatio>
              <MinLRRUSCRecoveryRatio>0.31</MinLRRUSCRecoveryRatio>
              <LRRUSCCleanUpMode>CATALOG</LRRUSCCleanUpMode>
              <FCMDSleepTime>2m</FCMDSleepTime>

              <LRRUSCSleepTime>1m</LRRUSCSleepTime>
              <StudyStabilityTime>5m</StudyStabilityTime>

              <StudyRecordBatchRetrieveSize>100</StudyRecordBatchRetrieveSize>
              <OperationExecutorThreadPoolSize>15</OperationExecutorThreadPoolSize>
              <Policy>${rialto.rootdir}/etc/services/image-archive/scripts/ilm_policy.groovy</Policy>
              <DataCenter>DC1</DataCenter>
              <EligibilityScript>${rialto.rootdir}/etc/services/image-archive/scripts/ilm_eligibility.groovy</EligibilityScript>
              <PrefetchedFilter>
                <callingAE>*</callingAE>
                <calledAE>PF_RIALTO</calledAE>
              </PrefetchedFilter>

              <StgCmtMaxAttemptsToForward>5</StgCmtMaxAttemptsToForward>
            </prop>
            -->

            <!--
             Property to reject studies that contain multiple patient ID's
            -->
            <prop name="RejectMixedPatientIDsInStudy">false</prop>

            <prop name="LinkManagerAE" value="RIALTO_MWL" />

            <prop name="CassandraConfiguration">
                <keyspaceName>${CassandraKeyspacePrefix}imagearchive</keyspaceName>
                <keyspaceReplicationStrategy>NetworkTopologyStrategy</keyspaceReplicationStrategy>
                <keyspaceReplicationStrategyOptions>${CassandraReplicationOption}</keyspaceReplicationStrategyOptions>
                <keyspaceCreateEnabled>true</keyspaceCreateEnabled>
                <dynamicConsistencyLevelEnabled>false</dynamicConsistencyLevelEnabled>
                <schemaUpdateEnabled>false</schemaUpdateEnabled>
            </prop>

            <prop name="DirectAccess" value="true" />
            <prop name="MintEnabled" value="true" />

            <prop name="FileSystemStorageConfiguration">
                <FileSystemStorageLocation>
                   <AbsoluteBasePath>${rialto.rootdir}/var/ids1/archive</AbsoluteBasePath>
                   <ReadOnly>false</ReadOnly>
                   <SourceDataCenter>DC1</SourceDataCenter>
                </FileSystemStorageLocation>
            </prop>

            <!-- HCP Configuration
            <prop name="HcpConfiguration">
                <HcpNodeConfiguration>
                    <HcpUrl>${HCPURL}</HcpUrl>
                    <HcpHost>${HCPHost}</HcpHost>
                    <HcpUsername>${HCPUsername}</HcpUsername>
                    <HcpPassword>${HCPPassword}</HcpPassword>
                    <HcpAuthHeader>${HcpAuthHeader}</HcpAuthHeader>
                </HcpNodeConfiguration>
                <HttpConnectionConfiguration>
                    <poolMaxConnectionsPerHost>${HCPPoolMaxConnectionsPerHost}</poolMaxConnectionsPerHost>
                    <poolMaxTotalConnections>${HCPPoolMaxTotalConnections}</poolMaxTotalConnections>
                </HttpConnectionConfiguration>
            </prop>
            -->

            <prop name="MWLReconciliationEnabled">true</prop>
            <prop name="QCToolsReconciliationScript">${rialto.rootdir}/etc/services/image-archive/scripts/qcreconciliation.groovy</prop>
            <prop name="ReconciliationScript">${rialto.rootdir}/etc/services/image-archive/scripts/qcreconciliation.groovy</prop>

            <prop name="PatientNameInHL7Location" value="/.PID-5" />

            <!-- Manifest Publish Configuration -->
            <prop name="PublisherType" value="BASIC" />
            <prop name="MetadataStudyInstanceUIDKey">studyInstanceUID</prop>
            <prop name="HealthCareFacilityCode" value="HealthCareFacilityCodeValue, HealthCareFaciltiyCodeScheme, HealthCareFaciltiyCodeDisplay" />
            <prop name="PracticeSettingCode" value="PracticeSettingCodeValue, PracticeSettingCodeScheme, PracticeSettingCodeSchemeDisplay" />
            <prop name="ClassCode" value="ClassCodeValue, ClassCodeScheme, ClassCodeDisplay" />
            <prop name="TypeCode" value="TypeCodeValue, TypeCodeScheme,TypeCodeDisplay" />
            <prop name="ContentTypeCode" value="ContentTypeCodeValue, ContentTypeCodeScheme, ContentTypeCodeDisplay" />
            <prop name="ConfidentialityCode" value="ConfidentialityCodeValue, ConfidentialityCodeScheme,ConfidentialityCodeDisplay" />
            <!-- Manifest metadata is now determined from the groovy script -->
            <prop name="DocumentMetadataMorpher">${rialto.rootdir}/etc/services/image-archive/scripts/document_metadata_morpher.groovy</prop>
            <prop name="FindPreviousManifestUsingReferenceID" value="false" />
            <!-- disable pif
            <prop name="PatientIdentityFeedMorpher">${rialto.rootdir}/etc/services/image-archive/scripts/pif_morpher.groovy</prop>
            -->
            <prop name="TagMorphers">
                <script direction="IN" file="${rialto.rootdir}/etc/services/image-archive/scripts/in.groovy" />
                <!-- <script direction="CFIND_REQUEST" file="${rialto.rootdir}/etc/services/image-archive/scripts/cfind.groovy" /> -->
            </prop>

            <!-- Study Content Notification -->
            <!-- Note: When configuring notification destinations, please ensure the receivingApplication
                 and receivingFacility match up with a configured device -->
            <!--prop name="StudyContentNotifierType" value="BASIC" />

            <prop name="NotificationDestinations">
                <destination name="TomAndJerry">
                    <receivingApplication>THOMAS</receivingApplication>
                    <receivingFacility>JERRY</receivingFacility>
                    <morphingScript>${rialto.rootdir}/etc/services/image-archive/scripts/daffy.groovy</morphingScript>
                </destination>
            </prop-->
        </config>
    </service>

</config>
