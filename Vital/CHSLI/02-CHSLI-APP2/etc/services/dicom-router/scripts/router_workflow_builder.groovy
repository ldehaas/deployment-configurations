import org.dcm4che2.data.DicomObject
import org.dcm4che2.data.Tag.*;

import com.karos.rialto.workflow.model.*;
import com.karos.rialto.workflow.common.tasks.*;
import com.karos.rialto.storage.file.*;
import java.time.*;
import com.karos.groovy.morph.dcm.*;

import com.karos.rialto.workflow.model.Task.Status;

class RouterWorkflowBuilder {                       

    def callingAe = "RIALTO"

    def wfb = null
    def receiveImageTaskBuilder = new ReceiveImageTask.Builder()
    def inboundMorpherBuilder = null
    def transcodeBuilder = null
    def cleanStorageLocationTaskBuilder = new CleanStorageLocationTask.Builder()
    def baseTask = null
    def transcodeBase = null

    RouterWorkflowBuilder(path, called, calling, studyInstanceUID) {

        callingAe = calling

        wfb = new GenericWorkflow.Builder("Route Images from " + calling)

        def currentDate = LocalDateTime.now()
        def fsRepo = new FileSystemStorageRepository('Router', path)
        String[] sopPath = [called,
                            calling,
                            currentDate.getYear(),
                            currentDate.getMonthValue(),
                            currentDate.getDayOfMonth(),
                            currentDate.getHour(),
                            currentDate.getMinute(),
                            currentDate.getSecond(),
                            studyInstanceUID
                            ]

        receiveImageTaskBuilder.setStorageRepository(fsRepo)
        receiveImageTaskBuilder.setStorageTags(sopPath)
        receiveImageTaskBuilder.setStatus(Status.RUNNING)
        baseTask = receiveImageTaskBuilder

        cleanStorageLocationTaskBuilder.addDataDependency(receiveImageTaskBuilder, Status.SUCCESS)

        wfb.addTaskBuilder(receiveImageTaskBuilder)
        wfb.addTaskBuilder(cleanStorageLocationTaskBuilder)
    }

    def setInboundMorpher(script) {
        inboundMorpherBuilder = new MorphStudyTask.Builder()
        inboundMorpherBuilder.addDataDependency(baseTask, Status.SUCCESS)
        inboundMorpherBuilder.setMorpherScriptName(script)

        cleanStorageLocationTaskBuilder.addDataDependency(inboundMorpherBuilder, Status.SUCCESS)
        wfb.addTaskBuilder(inboundMorpherBuilder)

        baseTask = inboundMorpherBuilder
    }

    def addDestination(params) {

        def calledAe = params.calledAe
        def lookupAe = params.lookupAe
        def transcodeDestinationTs = params.transcodeDestinationTs
        def priority = params.priority
        def morpher = params.morpher

        def baseForSend = baseTask

        if (morpher != null) {
            def morpherBuilder = new MorphStudyTask.Builder()
            morpherBuilder.addDataDependency(baseForSend, Status.SUCCESS)
            morpherBuilder.setMorpherScriptName(morpher)

            baseForSend = morpherBuilder

            cleanStorageLocationTaskBuilder.addDataDependency(morpherBuilder, Status.SUCCESS)
            wfb.addTaskBuilder(morpherBuilder)
        }

        def sendStudyTaskBuilder = new SendStudyTask.Builder()
        
        if (transcodeDestinationTs == null) {
            sendStudyTaskBuilder.addDataDependency(baseForSend, Status.SUCCESS)
        } else {
            // They want to transcode. Let's check if we already made a transcode task:
            if (transcodeBase == null) {
                // let's make a transcode task!
                transcodeBuilder = new TranscodeInstancesTask.Builder()
                transcodeBuilder.addDataDependency(baseForSend, Status.SUCCESS)
                transcodeBuilder.setSupportedSourceTs("[1.2.840.10008.1.2, 1.2.840.10008.1.2.1, 1.2.840.10008.1.2.2]")
                transcodeBuilder.setDestinationTs(transcodeDestinationTs)

                cleanStorageLocationTaskBuilder.addDataDependency(transcodeBuilder, Status.SUCCESS)
                wfb.addTaskBuilder(transcodeBuilder)

                transcodeBase = transcodeBuilder
            }

            sendStudyTaskBuilder.addDataDependency(transcodeBase, Status.SUCCESS, Status.FAILURE)
        }

        sendStudyTaskBuilder.setCallingAe(callingAe)
        sendStudyTaskBuilder.setDestination(calledAe)
        if (lookupAe != null) {
            sendStudyTaskBuilder.setLookupAe(lookupAe)
        }
        
        if (priority != null) {
            sendStudyTaskBuilder.setPriority(priority.getPriorityValue())
        }

        cleanStorageLocationTaskBuilder.addDataDependency(sendStudyTaskBuilder, Status.SUCCESS)
        wfb.addTaskBuilder(sendStudyTaskBuilder)
    }

    def build() {
        return wfb.build()
    }

}