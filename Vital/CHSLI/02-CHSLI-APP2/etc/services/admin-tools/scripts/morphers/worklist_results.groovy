/**
 * This script is executed once per DICOM modality worklist response
 * during the auto-reconcile workflow. The input 'xdsinputs' is a
 * collection of DocumentMetadataScriptingAPI each wrapping an XDS
 * document from the document-set (here the document-set comprises
 * all documents that have matching accession number within the
 * current patient domain).
 *
 * The aECG and PDF documents can be identified by their respective
 * MIME types, and can be interrogated for properties that will form
 * the basis for matching against DICOM modality worklist entries.
 *
 * The input 'dcminputs' is a collection of DicomScriptingAPI each
 * wrapping one result from the DICOM modality worklist response.
 * This script is responsible for selecting one matching DICOM
 * modality worklist entry (corresponding to an 'order') from the
 * collection of results so that downstream tasks in the workflow
 * might reconcile the document-set against that worklist entry.  
 *
 * Note: the script variables also include 'rawobjects' which is the
 * collection of DicomObject modality worklist entries that mirrors
 * the 'dcminputs' collection, so the matching worklist entry can be
 * returned as below:
 *
 * def match = findMatchingOrder(aecg.get(XDSCreationTime))
 * if (match == null) {
 *   log.error("Worklist match was not found")
 *   return null
 * }
 * return rawobjects[xdsinputs.indexOf(match)]
 *
 * This script should return null if no DICOM modality worklist entry
 * satisfies the required order-matching criteria. Note: DICOM modality
 * worklist entries that have already been reconciled against, will
 * have their ScheduledProcedureStepStatus set to "VERIFIED"
 */
import org.joda.time.*
import com.karos.rtk.common.HL7v2Date;
 
def findMatchingWorklistEntry(resultsDatetime) {
     
    if (resultsDatetime == null) {
        log.warn("The match target document does not know its creation time.")
        return null
    }
 
    log.debug("results datetime:{}", resultsDatetime)
 
    return dcminputs.find { dcmobject ->
         
        def issuedDatetime = new DateTime(dcmobject.getDate(
            IssueDateOfImagingServiceRequest,
            IssueTimeOfImagingServiceRequest))
 
        log.debug("issued datetime:{}", issuedDatetime)
 
        def scheduledProcedureList = dcmobject.get(ScheduledProcedureStepSequence)
        if (scheduledProcedureList.isEmpty()) {
            log.warn("The worklist entry does not contain scheduled procedures.")
            return false
        }
 
        def scheduledProcedure = scheduledProcedureList[0]
 
        def scheduledDatetime = new DateTime(
            scheduledProcedure.getDate(
                ScheduledProcedureStepStartDate,
                ScheduledProcedureStepStartTime))
         
        log.debug("scheduled datetime:{}", scheduledDatetime)
 
        def scheduledStatus = scheduledProcedure.get(ScheduledProcedureStepStatus)
 
        log.debug("scheduled status:{}", scheduledStatus)
 
        return scheduledStatus != "VILIFIED" &&
            resultsDatetime.isAfter(issuedDatetime) &&
            resultsDatetime.isBefore(scheduledDatetime.plusDays(3)) &&
            resultsDatetime.isAfter(scheduledDatetime.minusDays(3))
    }
}
 
def findECG(mimeType) {
    return xdsinputs.find { document ->
        def format = document.get(XDSFormatCode)
        return format.getCodeValue() == "93010" &&
               format.getSchemeName() == "CPT-4" &&
               mimeType.equals(document.get(XDSMimeType))
    }
}
 
def aecg = findECG("text/xml")
 
if (aecg == null) {
    aecg = findECG("application/xml")
}
 
if (aecg == null) {
    log.debug("aECG was not found - returning without match")
    return null
}
 
log.debug("Evaluating worklist candidates against ECG documents")
 
def match = findMatchingWorklistEntry(aecg.get(XDSCreationTime))
 
if (match == null) {
    log.debug("Worklist match was not found")
    return null
}
 
return rawobjects[xdsinputs.indexOf(match)]
