import org.joda.time.LocalDateTime;
import org.joda.time.DateTime;

def scriptName="etc/services/connect/script/cfind_response_filter.groovy"
log.debug("{} starting", scriptName)

def validPriorList = []
DateTime currentDateTime = new DateTime()
def earliestStudyDate = currentDateTime.minusYears(3)

log.debug("CurrentDateTime {}, earliestStudyDate {}", currentDateTime, earliestStudyDate);

log.debug("Complete list of studies being examinged:\n{}",
    inputs.collect({ it.get(StudyInstanceUID) + ": " + it.getDate(StudyDate, StudyTime) }).join("\n"));

inputs.each {
    if (it.getDate(StudyDate, StudyTime) < earliestStudyDate) {
        log.debug("Prior {} is older than '{}'. This study has a date/time of '{}'. Not going to prefetch study.", it.get(StudyInstanceUID), earliestStudyDate, it.getDate(StudyDate, StudyTime));
    } else {
        log.debug("Prior {} is younger than '{}'. This study has a date/time of '{}'. Going to prefetch study.", it.get(StudyInstanceUID), earliestStudyDate, it.getDate(StudyDate, StudyTime));

        validPriorList.add(it);

    }
}

log.debug("{} completed", scriptName)
return validPriorList
