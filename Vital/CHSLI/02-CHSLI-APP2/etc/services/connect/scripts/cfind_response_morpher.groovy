def scriptName = "etc/services/connect/scripts/cfind_response_morpher.groovy")
log.debug("{} starting", scriptName)

def patientId = get(PatientID)
def issuer = get(IssuerOfPatientID)
def retrieveAE = get(RetrieveAE)

log.debug("PatientID:{} IssuerOfPatientID:{} RetrieveAE:{}", patientID, issuer, retrieveAE)

log.debug("{} completed", scriptName)
