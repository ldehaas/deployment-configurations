def scriptName = "etc/services/connect/scripts/oru2oru_localizer.groovy"
log.debug("{} starting", scriptName)
LOAD("../../common.groovy")
def sendingFacility = prefetchOrder.get("MSH-4")
set("MSH-4", sendingFacility)
set("MSH-6", sendingFacility)
//def sourcePid = localDemographics.get(PatientID)
def acn = get("ORC-3")
//log.debug("Image PatientId:{} Accession:{}", sourcePid, acn)
def orderPatientId = prefetchOrder.get('PID-3')
def orderIssuer = prefetchOrder.get('PID-3-4')
log.debug("acn, orderPatientId, orderIssuer = {}, {}, {}", acn, orderPatientId, orderIssuer)
set("PID-3", orderPatientId)

if (sendingFacility == 'F') {
    set("OBR-3", 'SFD_' + acn)
    set("ORC-3", 'SFD_' + acn)
}

if (sendingFacility == 'SFD') {
    set("OBR-3", 'F_' + acn)
    set("ORC-3", 'F_' + acn)
}

set("PID-3-4", orderIssuer)
log.debug("{} completed", scriptName)
