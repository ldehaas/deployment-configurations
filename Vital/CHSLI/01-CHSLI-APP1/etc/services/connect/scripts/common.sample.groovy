/*Common classes for all groovy scripts*/

class Constants {
    static final LocalIssuerIDs = ["domain1", "domain2", "domain3"]
    static final PrimaryLocalIssuerID = LocalIssuerIDs[0]
}

class ProcedureCodes {
    static localize(modality, rawText) {
        if (rawText == null || rawText.trim().isEmpty()) {
            return null
        }

        if (modality == null) {
            modality = ""
                // still try to do the matching without modality
        }

        modality = modality.toUpperCase()
            def text = rawText.toLowerCase() 

            // This is used to handle the specific set of images that I downloaded
            // Allows for Comparison with a test script
            // Normally there would be far more regex or lookup tables than this, but this is sufficient for
            // testing purpopses
            if(text =~ /wrist/ ){
                text = "Wrist"
                    return modality+"_"+text
            }
            if(text =~ /cardi/ ){
                text = "Heart"
                    return modality+"_"+text
            }
        // example of a map
        def ret = lookup.get( [modality, text] )
            if (ret != null) {
                return ret
            }
        // can't localize, let the caller decide
        // This is handled by the caller, causes the workflow to crash
        return null
    }

    private static final lookup = [
        ['CT', 'abdomen']:'CT_Abdomen',
        ['CT', 'abdn']:'CT_Abdomen',
        ]
}

class Pids {
    /**
     *@return a two element array containing the localized pid and issuer
     *@param qualifiedSourcePid a two element array containing source
     *       pid and source issuer
     *@param qualifiedOtherPids an array of qualified pids
    */
    // If the SourcePID is contained in the list of LocalIssuerID's,
    // then we will use that as the source issuer.
    // Otherwise, we walk through the OtherIDsSequence and try to find one
    // that fits in the LocalIssuerIDs.
    // If that doesn't work, then we just use the primary localissuer ID.
    static localize(qualifiedSourcePid, qualifiedOtherPids) {
        def sourcePid = qualifiedSourcePid[0]
        def sourceIssuer = qualifiedSourcePid[1]

            if ( Constants.LocalIssuerIDs.contains(sourceIssuer) ) {
                // this image originated from the local PACS
                 return qualifiedSourcePid;
            }
        def foundPid = null
        for (def pid : qualifiedOtherPids) {
            if (Constants.LocalIssuerIDs.contains(pid[1]) ){
                foundPid=pid
                break
            }
        }
        // no pid found, create one by prefixing
        if (foundPid == null){
            foundPid=[sourceIssuer+"_"+sourcePid,Constants.PrimaryLocalIssuerID]
        }
        return foundPid
    }
}
