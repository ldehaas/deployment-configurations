def scriptName = "etc/services/connect/scripts/report_rank_filter.groovy"
log.debug("{} starting", scriptName)

final dateAndTimeField = 'ORC-7-4'
final verificationField = 'OBX-11'

log.debug("Original number of ORUs converted for this study: {}", reports.size())

def logDates = { statusFunc, description, reports ->
    def selected = reports.findAll { report -> statusFunc(report.get(verificationField)) }
    def dates = selected.collect { report -> report.get(dateAndTimeField) }
    log.debug("There are {} {} reports with dates: {}", dates.size(), description, dates)
}

logDates({ status -> status == "F" }, "final", reports)
logDates({ status -> status != "F" }, "preliminary", reports)

//Sort the reports by report time
def sortedReports = reports.sort { report ->
    report.get(dateAndTimeField) + (report.get(verificationField) =='F' ? 2 : 1)
}

//Pass only the latest report to the PACS
lastReport = sortedReports.last()
log.debug("Passing 1 report with date '{}' and study instance uid '{}'",
    lastReport.get(dateAndTimeField), lastReport.get("ZDS-1-1"))

log.debug("{} completed", scriptName)

return lastReport
