def scriptName = "etc/services/hl7-proxy/scripts/report_repo_forward_rules.groovy"

LOAD("../../common.groovy")

log.debug("{} starting", scriptName)

def messageType = get('/.MSH-9-1')
def triggerEvent = get('/.MSH-9-2')

log.debug("Report Repo Morpher: messageType is: {}", messageType)

Boolean rc = true
if (messageType != null && triggerEvent != null) {
    if ("ORU".equals(messageType)) {
        log.debug("message accepted {}", scriptName)

        def sendingFacility = get('/.MSH-4')
        CHSLI_Helper.updatePID2MSH4(sendingFacility, this)
        
        def patientId = get('/.PID-3')
        def IssuerOfPatientId = get('/.PID-3-4')
        def IssuerOfPatientIdUsingMSH4 = CHSLI_Helper.lookupUniversalEntityIdUsingMSH4(sendingFacility)

        def accessionNumber = get('/.OBR-3')
        def IssuerOfAccessionNumber = get('/.OBR-3-3')
        def IssuerOfAccessionUsingMSH4 = CHSLI_Helper.lookupUniversalEntityIdUsingMSH4(sendingFacility)

        log.debug("sendingFacility:{}", sendingFacility)
        log.debug("patientId:{}", patientId)
        log.debug("IssuerOfPatientId:{}", IssuerOfPatientId)

        log.debug("IssuerOfAccessionNumber:{}", IssuerOfAccessionNumber)
        log.debug("IssuerOfAccessionUsingSendingFacility:{}", IssuerOfAccessionUsingMSH4)
        log.debug("accessionNumber:{}", accessionNumber)

        set('/.PID-3-4-2', IssuerOfPatientIdUsingMSH4)    
        set('/.PID-3-4-3', 'ISO')

        log.debug("IssuerOfPatientId:{}^^{}^{}", get('/.PID-3-4-1'), get('/.PID-3-4-2'), get('/.OBR-3-4-3'))

        set('/.OBR-3-3', IssuerOfAccessionUsingMSH4) 
        set('/.OBR-3-4', 'ISO')
           
        log.debug("IssuerOfAccessionNumber:{}^^{}^{}", get('/.OBR-3-1'), get('/.OBR-3-3'), get('/.OBR-3-4'))

    } else {
        log.debug("message rejected {}", scriptName)
        rc = false
    }
}

log.debug("{} completed", scriptName)
return rc
