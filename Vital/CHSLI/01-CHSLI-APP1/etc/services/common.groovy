class CHSLI_Helper {

    static updatePID2MSH4(sendingFacility, hl7) {
        def keepMRN = ""
        def keepIssuer = ""
        def keepUniversalId = ""
        def keepUniversalIdType = ""
        def keepExtra = ""

        // determine which local MRN to keep
        // if SFHD keep the EPI (enterprise Id)
        // in most cases, this matches the Issuer, etc in the DICOM

        if (sendingFacility.equals("SFD")) {
            sendingFacility = "EPI"
        }
        
        def pidCount = 0
        hl7.get("PID-3").each {
            def localMRN = hl7.get("PID-3(" + pidCount + ")-1")
            def issuer = hl7.get("PID-3(" + pidCount + ")-4-1")
            def universalId = hl7.get("PID-3(" + pidCount + ")-4-2")
            def universalIdType = hl7.get("PID-3(" + pidCount + ")-4-3")
            def extra = hl7.get("PID-3(" + pidCount + ")-5")

            if (issuer == sendingFacility) {
                keepMRN = localMRN
                keepIssuer = issuer
                keepUniversalId = universalId
                keepUniversalIdType = universalIdType
                keepExtra = extra
            }
            pidCount++
        }

        pidCount = 0
        hl7.get("PID-3").each {
            hl7.set("PID-3(" + pidCount + ")-1", null)
            hl7.set("PID-3(" + pidCount + ")-4-1", null)
            hl7.set("PID-3(" + pidCount + ")-4-2", null)
            hl7.set("PID-3(" + pidCount + ")-4-3", null)
            hl7.set("PID-3(" + pidCount + ")-5", null)
            hl7.set("PID-3(" + pidCount + ")", null)
            pidCount++
        }
        
        hl7.set("PID-3-1", keepMRN)
        hl7.set("PID-3-4-1", keepIssuer)
        hl7.set("PID-3-4-2", keepUniversalId)
        hl7.set("PID-3-4-3", keepUniversalIdType)
        hl7.set("PID-3-5", keepExtra)

        return hl7
    }
    
    static lookupUniversalEntityIdUsingMSH4(facility) {

        def universalId = ""
        switch(facility) {
            case "EPI":
                universalId = "chsli.org"
                break
            case "F":
                universalId = "st.francis"
                break
            case "SFD":
                universalId = "chsli.org"
                break;
            case "S":
                universalId = "st.catherines"
                break
            case "M":
                universalId = "mercy.medical"
                break
            case "C":
                universalId = "st.charles"
                break
            case "J":
                universalId = "st.joseph"
                break
            case "G":
                universalId = "good.samaritan"
                break
            case "ST":
                universalId = "star.corporate"
                break
        }
        return universalId
    }

    static lookupFacilityUsingCallingAE(aetitle) {

        def facility = ""
        switch(aetitle) {
            case "QRF":
                facility = "F"
                break
            case "QRSFD":
                 facility = "SFD"
                break;
        }
        return facility 
    }

    static parseExistingPrefix(acnNum) {
        def acn = acnNum
        if(acn.startsWith("F_") || acn.startsWith("SFD_")) {
            def splitACN = acn.split("_")
            def newACN = ""
            for (int count = 0; count < splitACN.size(); ++count) {
                if (count != 0) {
                    newACN += splitACN[count]
                    newACN += (count != (splitACN.size() - 1) ? "_" : "")
                }
            }
            acn = newACN
        }
        return acn
    }

}

