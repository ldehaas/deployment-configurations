def scriptname = "etc/services/hl7-proxy/scripts/connect_forward_star.groovy"

log.info("{} starting", scriptname)

def messageType = get('/.MSH-9-1')
def triggerEvent = get('/.MSH-9-2')

def sendingFacility = get('/.MSH-4')

log.info("messageType is: {}", messageType)
log.info("triggerEvent is: {}", triggerEvent)
log.info("sendingFacility is: {}", sendingFacility)

boolean rc = true
if (messageType != null && triggerEvent != null) {
    if ((["ORM^O01"].contains(messageType + "^" + triggerEvent)) && (sendingFacility.equals("STAR"))) {
        log.info("message accepted {}", scriptname)
    }
    else {
        log.info("message rejected {}", scriptname)
        rc = false
    }
}

log.info("{} completed", scriptname)
return rc
