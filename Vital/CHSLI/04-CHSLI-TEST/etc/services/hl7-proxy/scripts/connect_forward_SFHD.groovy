def scriptname = "etc/services/hl7-proxy/scripts/connect_forward_SFHD.groovy"

LOAD("../../common.groovy")

log.info("{} starting", scriptname)

def messageType = get('/.MSH-9-1')
def triggerEvent = get('/.MSH-9-2')
def sendingFacility = get('/.MSH-4')

log.info("messageType is: {}", messageType)
log.info("triggerEvent is: {}", triggerEvent)
log.info("sendingFacility is: {}", sendingFacility)

boolean rc = true
if (messageType != null && triggerEvent != null) {
    if ((["ORM^O01"].contains(messageType + "^" + triggerEvent)) && (sendingFacility.equals("SFD"))) {

        CHSLI_Helper.updatePID2MSH4(sendingFacility, this)

        log.info("message accepted {}", scriptname)
    }
    else {
        log.info("message rejected {}", scriptname)
        rc = false
    }
}

log.info("{} completed", scriptname)
return rc
