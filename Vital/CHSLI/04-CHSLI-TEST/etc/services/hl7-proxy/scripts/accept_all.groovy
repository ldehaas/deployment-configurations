def scriptname = "etc/services/hl7-proxy/scripts/accept_all.groovy"

log.info("{} starting" + scriptname)

def messageType = get('/.MSH-9-1')
def triggerEvent = get('/.MSH-9-2')

log.info("messageType is: {}", messageType)
log.info("triggerEvent is: {}", triggerEvent)

log.info("{} completed", scriptname)
return true
