def scriptname = "etc/services/hl7-proxy/scripts/EMPI_forward_rules.groovy"

LOAD("../../common.groovy")

log.info("{} starting" + scriptname)

def messageType = get('/.MSH-9-1')
def triggerEvent = get('/.MSH-9-2')

log.info("messageType is: {}", messageType)
log.info("triggerEvent is: {}", triggerEvent)

//The following is to populate the EMPI from the historical reports that CHSLI confirmed will all have the latest patient info in them already.
//To be removed once all historical reports have been migrated in.
if (messageType == "ORU") {
    set("MSH-9-1","ADT")
    set("MSH-9-2", "A04")
}

boolean rc = true
if (messageType != null && triggerEvent != null) {
    if (["ADT^A01", "ADT^A04", "ADT^A05", "ADT^A08", "ADT^A28", "ADT^A31", "ADT^A49", "ADT^A39", "ADT^A40"].contains(messageType + "^" + triggerEvent)) {
        def sendingFacility = get('/.MSH-4')
        if (sendingFacility == null) {
            set('/.MSH-4', 'EPI')
        } else {
            if ((sendingFacility == "SFD") || (sendingFacility == "SSD")) {
                set('/.MSH-4', 'EPI')
            }
        }
       
        log.info("message accepted {}", scriptname)
    }
    else {
        log.info("message rejected {}", scriptname)
        rc = false
    }
}

log.info("{} completed", scriptname)
return rc
