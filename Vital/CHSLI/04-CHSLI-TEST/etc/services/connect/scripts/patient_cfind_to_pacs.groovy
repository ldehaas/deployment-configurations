import org.dcm4che2.data.VR;

def scriptName = "etc/services/connect/scripts/patient_cfind_to_pacs.groovy"
log.debug("{} starting", scriptName)

LOAD("../../common.groovy")


def callingAE = getCallingAETitle()
log.debug("Calling AE is {}:", callingAE)

if (callingAE == 'CONNECTSFD') {
    log.debug("Calling AE is CONNECTSFD")
    image.set(IssuerOfPatientID, 'F')
    image.set("IssuerOfPatientIDQualifiersSequence/UniversalEntityID", 'st.francis', VR.UT)
    image.set("IssuerOfPatientIDQualifiersSequence/UniversalEntityIDType", 'ISO', VR.CS)
    image.set("IssuerOfAccessionNumberSequence/LocalNamespaceEntityID", 'F', VR.UT)
    image.set("IssuerOfAccessionNumberSequence/UniversalEntityID", 'st.francis', VR.UT)
    image.set("IssuerOfAccessionNumberSequence/UniversalEntityIDType", 'ISO', VR.CS)
}

if (callingAE == 'CONNECTF') {
    log.debug("Calling AE is CONNECTF")
    image.set(IssuerOfPatientID, 'SFD')
    image.set("IssuerOfPatientIDQualifiersSequence/UniversalEntityID", 'chsli.org', VR.UT)
    image.set("IssuerOfPatientIDQualifiersSequence/UniversalEntityIDType", 'ISO', VR.CS)
    image.set("IssuerOfAccessionNumberSequence/LocalNamespaceEntityID", 'SFD', VR.UT)
    image.set("IssuerOfAccessionNumberSequence/UniversalEntityID", 'chsli.org', VR.UT)
    image.set("IssuerOfAccessionNumberSequence/UniversalEntityIDType", 'ISO', VR.CS)
}

if (callingAE == 'CONNECTSSD') {
    log.debug("Calling AE is CONNECTSSD")
    image.set(IssuerOfPatientID, 'SSD')
    image.set("IssuerOfPatientIDQualifiersSequence/UniversalEntityID", 'chsli.org', VR.UT)
    image.set("IssuerOfPatientIDQualifiersSequence/UniversalEntityIDType", 'ISO', VR.CS)
    image.set("IssuerOfAccessionNumberSequence/LocalNamespaceEntityID", 'SSD', VR.UT)
    image.set("IssuerOfAccessionNumberSequence/UniversalEntityID", 'chsli.org', VR.UT)
    image.set("IssuerOfAccessionNumberSequence/UniversalEntityIDType", 'ISO', VR.CS)
}

if (callingAE == 'CONNECTS') {
    log.debug("Calling AE is CONNECTS")
    image.set(IssuerOfPatientID, 'S')
    image.set("IssuerOfPatientIDQualifiersSequence/UniversalEntityID", 'st.catherines', VR.UT)
    image.set("IssuerOfPatientIDQualifiersSequence/UniversalEntityIDType", 'ISO', VR.CS)
    image.set("IssuerOfAccessionNumberSequence/LocalNamespaceEntityID", 'SSD', VR.UT)
    image.set("IssuerOfAccessionNumberSequence/UniversalEntityID", 'st.catherines', VR.UT)
    image.set("IssuerOfAccessionNumberSequence/UniversalEntityIDType", 'ISO', VR.CS)
}


log.debug("IMAGE IS " + image)

log.debug("{} completed", scriptName)
