def scriptName = "etc/services/connect/scripts/cfind_response_morpher.groovy"
log.debug("{} starting", scriptName)

LOAD("../../common.groovy")

def issuer = get(IssuerOfPatientID)
def facility = CHSLI_Helper.lookupUniversalEntityIdUsingMSH4("F")

if (issuer == null) {

    set(IssuerOfPatientID, "F")
    set("IssuerOfPatientIDQualifiersSequence/UniversalEntityID", facility)
    set("IssuerOfPatientIDQualifiersSequence/UniversalEntityIDType", 'ISO')

    log.debug("Updating response to have PatientID:{} Issuer:{}&{}&{}", get(PatientID), get(IssuerOfPatientID), get("IssuerOfPatientIDQualifiersSequence/UniversalEntityID"), get("IssuerOfPatientIDQualifiersSequence/UniversalEntityIDType"))

}

log.debug("{} completed", scriptName)
