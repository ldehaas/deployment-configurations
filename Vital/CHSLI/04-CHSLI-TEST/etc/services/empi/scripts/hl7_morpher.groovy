// This file is used to modify incoming messages

def scriptName = "etc/services/empi/scripts/hl7_morpher.groovy"
log.debug("STARTING " + scriptName)

LOAD("../../common.groovy")

def messageType = get('MSH-9-1')
def triggerEvent = get('MSH-9-2')

//The following is to populate the EMPI from the historical reports that CHSLI confirmed will all have the latest patient info in them already.
//To be removed once all historical reports have been migrated in.
if (messageType == "ORU") {
    set("MSH-9-1","ADT")
        set("MSH-9-2", "A04")
        }

if ("ADT^A08".equals(messageType + "^" + triggerEvent)) {
    //if a patient is admitted thru the ER; an enterprise number is generated ADT^A08 first
    //without this IF, we are returning an AE to interface team.
    //added 2017.06.26
    set("MSH-4", "EPI")
}

if (!("ADT^A04".equals(messageType + "^" + triggerEvent))) 
    return;

def sendingApplication = get("MSH-3")
def sendingFacility = get("MSH-4")

if (sendingApplication == null) {
    set("MSH-3", "EPIC")
}

if (sendingFacility == null) {
    set("MSH-4", "null")
}

def pidCount = 0
get("PID-3").each {
    def issuer = get("PID-3("+pidCount+")-4-1")
    def universalId = get("PID-3("+pidCount+")-4-2")
    def universalIdType = get("PID-3("+pidCount+")-4-3")

    if (issuer != null) {

        log.debug(pidCount + ")" + issuer + "&" + universalId + "&" + universalIdType)

        def tmp = CHSLI_Helper.lookupUniversalEntityIdUsingMSH4(issuer)
        if (tmp.length() <= 0) {
        tmp = universalId 
        }
        set("PID-3("+pidCount+")-4-2", tmp)
        set("PID-3("+pidCount+")-4-3", "ISO")
        
        log.debug(pidCount + ")" + get("PID-3("+pidCount+")-4-1") + "&" + get("PID-3("+pidCount+")-4-2") + "&" + get("PID-3("+pidCount+")-4-3"))

    }
    pidCount += 1
}

log.debug("FINISHED " + scriptName);
