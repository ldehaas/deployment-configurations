<?xml version="1.0"?>
<config>

    <server type="http" id="http-navigator">
        <port>${Navigator-GUIPort}</port>
        <!-- use the default TLS configuration in etc/variables/tls.xml -->
        <tls />
    </server>

    <!-- PLUGIN: External Viewer (Download) -->
    <var name="DownloadPlugin-Name" value="External Viewer (Download)" />
    <var name="DownloadPlugin-Description" value="External platform-specific viewer or download to local disk" />
    <service id="navigator-plugin-download" type="navigator.plugin.download">

        <device idref="pix" />
        <device idref="pdq" />
        <device idref="xdsrep"/>
        <device idref="xdsreg"/>
        <device idref="cassandra-dc1" />

        <config>
            <prop name="CrossAffinityDomainID" value="${System-AffinityDomain}"/>
            <prop name="ForcedMimeTypeFileExtension">
                <prop mimeType="application/vnd.ms-excel" fileExtension="xls"/>
            </prop>
            <prop name="CassandraConfiguration">
                <keyspaceName>${CassandraKeyspacePrefix}navigator</keyspaceName>
                <keyspaceReplicationStrategy>NetworkTopologyStrategy</keyspaceReplicationStrategy>
                <keyspaceReplicationStrategyOptions>${CassandraReplicationOption}</keyspaceReplicationStrategyOptions>
                <keyspaceCreateEnabled>true</keyspaceCreateEnabled>
                <dynamicConsistencyLevelEnabled>false</dynamicConsistencyLevelEnabled>
                <schemaUpdateEnabled>false</schemaUpdateEnabled>
            </prop>
            <prop name="web-api-path" value="/plugins/download"/>
            <prop name="UserManagementURL" value="${Usermanagement-AuthenticatedURL}"/>

            <include location="admin-tools/patientidentitydomains.xml"/>
            <include location="admin-tools/cacheStorage.xml"/>
        </config>
    </service>

    <!-- PLUGIN: CDA Viewer   -->
    <var name="CDAPlugin-Name" value="CDA Viewer" />
    <var name="CDAPlugin-Description" value="HL7 CDA (Clinical Document Architecture) Viewer" />
    <service id="navigator-plugin-cda" type="navigator.plugin.cda">
        <device idref="pix" />
        <device idref="pdq" />
        <device idref="xdsrep"/>
        <device idref="xdsreg"/>
        <device idref="cassandra-dc1" />

        <config>
            <prop name="CrossAffinityDomainID" value="${System-AffinityDomain}"/>
            <prop name="CassandraConfiguration">
                <keyspaceName>${CassandraKeyspacePrefix}navigator</keyspaceName>
                <keyspaceReplicationStrategy>NetworkTopologyStrategy</keyspaceReplicationStrategy>
                <keyspaceReplicationStrategyOptions>${CassandraReplicationOption}</keyspaceReplicationStrategyOptions>
                <keyspaceCreateEnabled>true</keyspaceCreateEnabled>
                <dynamicConsistencyLevelEnabled>false</dynamicConsistencyLevelEnabled>
                <schemaUpdateEnabled>false</schemaUpdateEnabled>
            </prop>
            <prop name="web-api-path" value="/plugins/cda"/>
            <prop name="DefaultStylesheet" value="${rialto.rootdir}/etc/services/admin-tools/plugins/cda/default.xsl"/>
            <prop name="UserManagementURL" value="${Usermanagement-AuthenticatedURL}"/>

            <include location="admin-tools/patientidentitydomains.xml"/>
            <include location="admin-tools/cacheStorage.xml"/>
        </config>
    </service>

    <service id="rialto-ui" type="rialto.ui">
        <device idref="pix" />
        <device idref="pdq" />

        <server idref="http-navigator" name="web-ui">
            <url>${Navigator-GUIBasePath}</url>
        </server>

        <server idref="http-authenticated" name="web-api">
            <url>/public/*</url>
        </server>

        <config>
            <prop name="ApiURL" value="${Navigator-HostProtocol}://${Navigator-Host}:${Navigator-APIPort}${Navigator-RootPath}/api${Navigator-APIBasePath}"/>
            <prop name="ArrURL" value="${Navigator-HostProtocol}://${Navigator-Host}:${ARR-GUIPort}"/>
            <prop name="UserManagementURL" value="${Usermanagement-URL}"/>
            <prop name="PublicURL" value="${Navigator-HostProtocol}://${Navigator-Host}:${Navigator-APIPort}/public"/>
            <prop name="StudyManagementURL" value="http://${HostIP}:8080${IA-StudyManagement-Path}"/>
            <prop name="WadoURL" value="http://${HostIP}:8080/vault/wado" />
            <prop name="MintApiURL" value="http://${HostIP}:8080/vault/mint" />
            <prop name="WorkflowApiURL" value="http://${HostIP}:8080/workflows" />
            <prop name="ElasticsearchRestApiURL" value="http://localhost:9200" />
            <prop name="DeviceRegistryURL" value="http://${HostIP}:8080/rialto/deviceregistry" />
            <prop name="ModalityWorklistURL" value="http://${HostIP}:8085/mwl" />
            <prop name="DicomQcToolsURL" value="http://${HostIP}:8080/vault/qc" />
            <prop name="IlmURL" value="http://${HostIP}:8080/vault/ilm" />
            <prop name="EMPIManagementURL" value="http://${HostIP}:8080/rialto/empi" />
            <prop name="GlobalInactivitySessionTimeout" value="${IdleUserSessionTimeout}" />
            <prop name="ExternalPDQConfigured" value="false" />
            <prop name="RialtoAsACache" value="false" />
            <!--prop name="VIPConfidentialityCode" value="VIP" /-->

            <prop name="RialtoImageArchiveAETitle" value="${Router-AETitle}" />

            <prop name="ElasticsearchSchemaPrefix" value="${WorkflowSchemaPrefix}" />

            <!-- ConnectQueryRetrieveAETitle is used for query/retrieve of catalogued studies
                 This property should be enabled if you want to enable the import of catalogued studies in Rialto Admin tools
                 It's value should match a configured Connect device -->
            <!-- <prop name="ConnectQueryRetrieveAETitle" value="CONNECT" /> -->

            <!-- NEWRELIC CONFIGURATION -->
            <!-- Enable the following properties in order to enable the NewRelic Insights dashboards in the Rialto Admin tools -->
            <!-- <prop name="NewRelicInsightsQueryKey" value="${NewRelic-InsightsQueryKey}" />
                 <prop name="NewRelicInsightsQueryURL" value="https://insights-api.newrelic.com/v1/accounts/${NewRelic-AccountID}/query" /> -->
            <!-- Enable the following properties in order to enable the Server, Applications and Alerts dashboards in the Rialto Admin tools -->
            <!-- <prop name="NewRelicRestApiKey" value="${NewRelic-RestApiKey}" />
                 <prop name="NewRelicRestApiURL" value="https://api.newrelic.com/v2/" /> -->

            <prop name="SupportedThemes">
                vital
            </prop>
            <prop name="DefaultTheme" value="${Navigator-DefaultTheme}"/>

            <prop name="SupportedLocales">
                en
                fr
            </prop>
            <prop name="DefaultLocale" value="${Navigator-DefaultLocale}"/>

            <prop name="BreakTheGlassOptions">
                <BreakTheGlassOption>Emergency access required for patient care.</BreakTheGlassOption>
            </prop>

            <include location="admin-tools/patientidentitydomains.xml"/>

            <prop name="Plugins">
                <plugin>
                    <name>${CDAPlugin-Name}</name>
                    <description>${CDAPlugin-Description}</description>
                    <mimeTypes>text/xml</mimeTypes>
                    <url>${Navigator-HostProtocol}://${Navigator-Host}:${Navigator-APIPort}${Navigator-RootPath}/api/plugins/cda</url>
                    <embedded>true</embedded>
                </plugin>

                <plugin>
                    <name>${DownloadPlugin-Name}</name>
                    <description>${DownloadPlugin-Description}</description>
                    <mimeTypes>*</mimeTypes>
                    <url>${Navigator-HostProtocol}://${Navigator-Host}:${Navigator-APIPort}${Navigator-RootPath}/api/plugins/download</url>
                    <embedded>true</embedded>
                </plugin>
            </prop>

            <!-- Configure eventCodeSchemes to support searching for documents by eventCode -->
            <!--prop name="EventCodeSchemes">
                <scheme schemeID="1.2.3" schemeName="Karos Health demo eventCodes" />
            </prop-->

        </config>
    </service>
</config>
