log.debug("in.groovy: Start inbound morpher")

def issuer = get(IssuerOfPatientID)

if (issuer == null) {

    set(IssuerOfPatientID, "VI")
    set("IssuerOfPatientIDQualifiersSequence/UniversalEntityID","vital.images.consulting") 
    set("IssuerOfPatientIDQualifiersSequence/UniversalEntityIDType", 'ISO')
    log.info("Issuer set to {}", get(IssuerOfPatientID))
}

log.debug("in.groovy: End inbound morpher")
