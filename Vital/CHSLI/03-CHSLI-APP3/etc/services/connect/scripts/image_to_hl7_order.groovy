def scriptName = "etc/services/connect/scripts/image_to_hl7_order.groovy"
log.debug("{} starting", scriptName)

initialize("ORM", "O01", "2.3")

set('PID-3', get(PatientID))
if (localDemographics != null && !localDemographics.isEmpty()) {
    set('PID-5', localDemographics.get(0).get(PatientName))
}
else {
    set('PID-5', get(PatientName))
}

def accessionNumber = get(AccessionNumber)
set('OBR-2', accessionNumber)
set('ORC-2', accessionNumber)
set('ORC-3', accessionNumber)

log.debug("{} completed", scriptName)
