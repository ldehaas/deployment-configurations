/*
 * HL7 to cfind morpher
 * Convert an order message into a cfind message.
 * get() works on the order message, while set() works on the 
 * cfind message.
 * 
 * The order is ignored if this script returns false,
 * e.g. no images will be fetched for the order.
 */
 
def scriptName = "etc/services/connect/scripts/hl7_to_cfind_morpher.groovy"

LOAD("../../common.groovy")

log.debug("{} starting", scriptName)

def pid = get("PID-3-1")
def sendingFacility = get("MSH-4")

def namespace = get("PID-3-4-1")
def universalID = get("PID-3-4-2")
def universalType = get("PID-3-4-3")

if (namespace == null) {
    namespace = ""
}
if (universalID == null) {
    universalID = CHSLI_Helper.lookupUniversalEntityIdUsingMSH4(sendingFacility) 
}
if (universalType == null) {
    universalType = "ISO" 
}

log.info("found pid '{}' and issuer '{}&{}&{}'", pid, namespace, universalID, universalType)
issuer = namespace+"&"+universalID+"&"+universalType
log.info("the issuer is '{}'", issuer)

// no need to check pid == null, it's handed earlier in the workflow

// construct the cfind
set(PatientID, pid)
set(IssuerOfPatientID, issuer)
set("NumberOfStudyRelatedSeries", null)
set(RetrieveAETitle, null)
//set ( StudyDescription, get( "OBR-4" ))

log.debug("{} completed", scriptName)
