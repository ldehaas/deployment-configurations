def scriptName = "etc/services/connect/scripts/foreign_image_morpher.groovy"
log.debug("{} starting", scriptName)

LOAD("../../common.groovy")

def getParameter(fieldName) {
    log.info("localDemographics=[{}]", localDemographics);
    if (localDemographics != null && !localDemographics.isEmpty()) {
        log.info("localDemographics has data: [{}] and is size of [{}]", localDemographics.get(0), localDemographics.size())
        return localDemographics.get(0).get(fieldName)
    } else {
        return get(fieldName)
    }
}

def acn = get(AccessionNumber)

def callingAE = getCallingAETitle()
def calledAE = getCalledAETitle()

log.debug("CallingAETitle:{} CalledAETitle:{}", callingAE, calledAE)
log.debug("Image Accession:{}", acn)

if (acn.startsWith("F_") || acn.startsWith("SFD_")){
    def acnsize = acn.split("_").size()
    for (def count = 0; count < acnsize; ++count){
        acn  = CHSLI_Helper.parseExistingPrefix(acn)
    }
}

log.debug("After removing leading Fs and SFDs from the accession number, the accession number is now: {}", acn)

def orderPatientId = prefetchOrder.get('PID-3')

def facility = CHSLI_Helper.lookupFacilityUsingCallingAE(callingAE)
def entityId = CHSLI_Helper.lookupUniversalEntityIdUsingMSH4(facility)

set(PatientID, orderPatientId)
set(AccessionNumber, facility + "_" + acn)
set(IssuerOfPatientID, facility)
set("IssuerOfPatientIDQualifiersSequence/UniversalEntityID", entityId)
set("IssuerOfPatientIDQualifiersSequence/UniversalEntityIDType", 'ISO')

log.debug("sending report...")
//log.debug("Updating CachedStudy to have PatientID:{} Issuer:{}&{}&{} and AccessionNumber:{}", get(PatientID), get(IssuerOfPatientID), get("IssuerOfPatientIDQualifiersSequence/UniversalEntityID"), get("IssuerOfPatientIDQualifiersSequence/UniversalEntityIDType"), get(AccessionNumber))
//log.debug("Updating CachedStudy to have PatientID:{} AccessionNumber:{}", get(PatientID), get(AccessionNumber))

log.debug("{} completed", scriptName)
