// Uses MINT metadata to find series descriptions that match the blacklist
// v01
import groovy.util.slurpersupport.GPathResult
import java.net.URL
import groovy.util.XmlSlurper

def java.lang.String baseMINTURL='http://127.0.0.1:8080/vault/mint/studies'
def java.lang.String queryLevel = 'SERIES'
def java.lang.String includeFieldString = 'includefield=SeriesDescription'
def studyInstanceUID = study.get(StudyInstanceUID)
if (studyInstanceUID != null) {
    def java.lang.String seriesUID = ''
    //noinspection GroovyAssignabilityCheck
    String mintString = baseMINTURL + '?studyInstanceUID=' + studyInstanceUID + '&queryLevel=' + queryLevel + '&' + includeFieldString
    URL mintURL = new URL(mintString)
    HttpURLConnection mintConnection = (HttpURLConnection) mintURL.openConnection()
    DataInputStream mintResponse = new DataInputStream(mintConnection.getInputStream())
    mintText = mintResponse.getText() // Gets XML metadata with study, series UIDs plus any other attributes specified in the includeFieldString
    mintResponse.close()
    mintConnection.disconnect()
 
    def GPathResult parsedMint = new XmlSlurper().parseText(mintText) // turns the MINT XML into JSON
 
    def java.util.Collection foundSeriesList = parsedMint.study.'**'.findAll { node -> // finds all Series
        node.name() == 'series'
    }
    log.debug("ilm.groovy: Number of Series: {}", foundSeriesList.size())

    foundSeriesList.each { series ->  // Finds all Series Descriptions
        def java.util.Collection seriesDescriptionList = series.'**'.findAll { node ->
            node.name() == 'attr' && node.@tag == '0008103e' 
            seriesUID = node.@val.toString() 
        }
        log.debug("ilm.groovy: Series UID {}", seriesUID)
        def java.util.ArrayList<java.lang.String> seriesBlackList = ['DOCUMENT', ' ', 'NULL']
           def java.lang.String 


            seriesDescriptionList.each { seriesDescription ->
            seriesBlackList.each { phiSeries ->
                if( seriesDescription.@val.toString().toUpperCase().contains(phiSeries.toUpperCase())) {
                    // Checks each series description against blacklist and write series ID in log file if it matches: KHC15262
                    log.info("ilm.groovy: Series found: {}", seriesDescription.@val.toString())
                    def java.lang.String ToBeDeletedMessage = '{ "type": "DELETE", "level":"SERIES", "seriesInstanceUID":"' + seriesUID + '"}'
                    log.debug("ilm.groovy: To Be Deleted String {}", ToBeDeletedMessage)
                    def java.io.File MyFile = new MyFile("/tmp/KHC15262/KHC15262.txt", true);
                    def java.io.BufferedWriter  Buffer = new Buffer (MyFile);
                    Buffer.write(ToBeDeletedMessage);
                    Buffer.write("\n".getBytes());
                    Buffer.flush();
                    Buffer.close();
                }
            }
        } 
   }
}
