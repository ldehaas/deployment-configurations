import com.karos.cql3.persistence.*;
import org.apache.commons.lang.builder.ToStringBuilder;
import java.util.*;
import java.lang.String;

/**
 * auto-momo-ized code by com.karos.cql3.maintenance.tablescriptor.TableScriptor
 * keyspace  = imagearchive
 * table  = new_study
 */
@Entity
@Table(name="new_study")
public class new_study {

  @Id(ordinal=0)
  @Basic
  @Column(name="study_instance_uid")
  public String study_instance_uid;

  @Basic
  @Column(wide=true, ordinal=0, name="study_version")
  public Integer study_version;

  @Basic
  @Column(name="aa_namespace_id")
  public String aa_namespace_id;

  @Basic
  @Column(name="aa_universal_id")
  public String aa_universal_id;

  @Basic
  @Column(name="aa_universal_id_type")
  public String aa_universal_id_type;

  @Basic
  @Column(name="accession_number")
  public String accession_number;

  @Basic
  @Column(name="binary_items_desc_location")
  public String binary_items_desc_location;

  @Basic
  @Column(name="change_record")
  public String change_record;

  @Basic
  @Column(name="dicom_sops_desc_location")
  public String dicom_sops_desc_location;

  @Basic
  @Column(name="instance_count")
  public Integer instance_count;

  @Basic
  @Column(name="is_cataloged")
  public Boolean is_cataloged;

  @Basic
  @Column(name="is_deleted")
  public Boolean is_deleted;

  @Basic
  @Column(name="is_verified")
  public Boolean is_verified;

  @Basic
  @Column(name="issuer_of_accession_number")
  public String issuer_of_accession_number;

  @Basic
  @Column(name="metadata_locations")
  public String metadata_locations;

  @Basic
  @Column(name="metadata_summary")
  public String metadata_summary;

  @Basic
  @Column(name="mint_study_version")
  public Integer mint_study_version;

  @Basic
  @Column(name="patient_birth_date")
  public String patient_birth_date;

  @Basic
  @Column(name="patient_id")
  public String patient_id;

  @Basic
  @Column(name="patient_name")
  public String patient_name;

  @Basic
  @Column(name="patient_sex")
  public String patient_sex;

  @Basic
  @Column(name="study_date_time")
  public String study_date_time;

  @Basic
  @Column(name="study_id")
  public String study_id;

  @Basic
  @Column(name="study_uuid")
  public UUID study_uuid;

  @Basic
  @Column(UUIDversion = 1, name="study_version_uuid")
  public UUID study_version_uuid;

  @Basic
  @Column(name="timeline_event")
  public String timeline_event;

  @Basic
  @Column(name="total_pixel_data_size")
  public Long total_pixel_data_size;

  public String toString() {
    return ToStringBuilder.reflectionToString(this);
  }

}

/*
 * sample operation on entity
 */
// Use this script to collect the series instance ID to delete, the corresponding study ID and the accession#
if (entity.metadata_locations.matches('DC1:2018.*')) {
      def study = new XmlSlurper().parseText(entity.metadata_summary)
          study.seriesList.series.each {
           def SerieDate = it.attributes.attr.find {it.@tag == "00080021"}.@val
           def SerieDescription = it.attributes.attr.find {it.@tag == "0008103E"}.@val
           def StudyUID = study.attributes.attr.find {it.@tag == "0020000D"}.@val
           def SeriesUID = it.@seriesInstanceUID
           if ((SerieDescription.toString() == "SCCANNED DOCUMENTS") || SerieDescription.toString().contains('2020') || SerieDescription.toString().contains('2019') || SerieDescription.toString().contains('2018') || SerieDescription.toString().contains('2017')) 
           {

              def S = new File("/tmp/KHC16965/studyUID.txt");
              def s = new File("/tmp/KHC16965/seriesUID.txt");
              def Ss = new File("/tmp/KHC16965/study-series.txt");
              S.append('\n'+StudyUID.toString());
              s.append('\n'+SeriesUID.toString());
              Ss.append("ACC#="+entity.accession_number+", StudyID="+StudyUID+", SerieID="+SeriesUID+'\n');
              println("ACC#="+entity.accession_number+", StudyID="+StudyUID+", SerieID="+SeriesUID);

            }
      }
}
