// All instances that do not have the patient domain used by the de-identifier script will be rejected
log.debug("in.groovy: Start inbound morpher")

def universalEntityID = get("IssuerOfPatientIDQualifiersSequence/UniversalEntityID")
def instanceUID = get(SOPInstanceUID)

if (universalEntityID != '1.3.6.1.4.1.40920') {
    log.debug("in.groovy: Patient domain found: {}. Instance {} is not de-identified and will not be stored.", universalEntityID, instanceUID)
}

log.debug("in.groovy: End inbound morpher")