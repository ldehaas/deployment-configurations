#!/bin/bash
# use this script to generate the commands of the series ID to delete and the corresponding study ID to be deleted.
fileStudy="/home/rialto/rialto/etc/services/image-archive/scripts/studyUID.txt"
fileSeries="/home/rialto/rialto/etc/services/image-archive/scripts/seriesUID.txt"

work() {
#  printf 'fileStudy: %s\n' "$1"
#  printf 'fileSeries: %s\n' "$2"
  echo "curl -s -X GET http://localhost:8080/vault/mint/studies/$1/DICOM/metadata.xml > $1.metadata.xml"
  echo "cp $1.metadata.xml $1.metadata.xml_orig"
  echo "curl http://localhost:8080/vault/mint/studies/$1 -X PUT -H \"Content-Type: application/json\" -v -d '{\"type\":\"DELETE\",\"level\":\"SERIES\",\"seriesInstanceUID\":\"$2\"}'"
  echo "curl -v --header \"Content-Type: application/xml\"  --upload-file \"$1.metadata.xml\" http://localhost:8080/vault/mint/studies/$1/DICOM/metadata.xml"
  echo "gzip $1.metadata.xml"
  echo "gzip $1.metadata.xml_orig"
  echo $" "

}

[ ! -f $fileStudy ] && { echo "$fileStudy file not found"; exit 99; }
while true
do
  read -r fileStudy <&3 || break
  read -r fileSeries <&4 || break
  work "$fileStudy" "$fileSeries"
done 3<$fileStudy 4<$fileSeries
