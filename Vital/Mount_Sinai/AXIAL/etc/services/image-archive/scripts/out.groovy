// Updates patient birthdate if older than 90 when beoutg sent out of archive
log.debug("out.groovy: Start outbound morpher")

def replacementYear = new Date().format("yyyy").toInteger() - 90 // current year - 90
def oldBirthDate = get(PatientBirthDate)
def newBirthDate
if (oldBirthDate != null){
	if (oldBirthDate.take(4).toInteger() < replacementYear) { // If year is earlier than (current year - 90) set year to (current year - 90)
		log.debug("out.groovy: Birth date found: {}. Removing birth date.", oldBirthDate)
		newBirthDate = '10660101'
		set(PatientBirthDate,newBirthDate) // (0010,0030)
	}	
}

log.debug("out.groovy: End outbound morpher")