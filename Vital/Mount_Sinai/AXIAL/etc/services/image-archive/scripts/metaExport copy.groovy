metaExport.groovy
// v01
// Gets MINT metadata, sends it to MRN database and forwards the study to METADATASCP
// Data sent to METADATASCP is not saved.
import groovy.sql.Sql
import java.sql.SQLException
import java.net.URL

def studyInstanceUID = study.getStudyInstanceUID()
log.debug("metaExport.groovy: Processing study {}.", studyInstanceUID)
return true
def patientID = study.get(PatientID)
def accessionNumber = study.get(AccessionNumber)
// TEST ONLY
def studyDate = study.get(StudyDate)
//
def baseMINTURL='http://127.0.0.1:8080/vault/mint/studies/'
// def baseMINTURL='http://127.0.0.1:8080/vault/mint/studies/'
def mappingDBURL = 'jdbc:oracle:thin:@sdwhetlprod01.mountsinai.org:1521:ETLPROD'
def mappingDBUser = 'vioarchive_lookup'
def mappingDBPass = 'msdw'
Class.forName('oracle.jdbc.driver.OracleDriver')

if  (!timeline.wasFullyForwardedTo("METADATASCP"))  { // remove second condition after test
        log.debug("metaExport.groovy: Exporting metadata for Study {}.", studyInstanceUID)

        String mintString = baseMINTURL + studyInstanceUID + '/DICOM/metadata'
        URL mintURL = new URL(mintString)
        HttpURLConnection mintConnection = (HttpURLConnection) mintURL.openConnection()
        def responseCode = mintConnection.getResponseCode()
        log.debug("metaExport.groovy: HTTP Response Code for study {}: {}", studyInstanceUID, responseCode)
        if (responseCode != 200) {
            log.error("metaExport.groovy: Failed to get MINT Metadata for study {}.", studyInstanceUID)
            mintConnection.disconnect()
            return false
        }
        DataInputStream mintResponse = new DataInputStream(mintConnection.getInputStream()) 
        metadataXML = mintResponse.getText()
        log.debug("metaExport.groovy: MINT Metadata retrieved for study {}.", studyInstanceUID)
        mintResponse.close()
        mintConnection.disconnect()

        // make database connection
        log.debug("metaExport.groovy: Initiating connection to {}", mappingDBURL)
        try {
            sql = Sql.newInstance(mappingDBURL, mappingDBUser, mappingDBPass)
        } catch (SQLException sqlConnectError) {
            log.error("metaExport.groovy: Failed to connect to {}: {}", mappingDBURL, sqlConnectError)
            return false
        }

        // upload metadata
        // send_nophi_xml_data(masked_mrn, masked_accession_no, file_name, xml_data)
        log.debug("metaExport.groovy: Sending metadata for Study {} to {}", studyInstanceUID, mappingDBURL)
        try {
            sql.call '{? = call send_nophi_xml_data(?,?,?,?)}', [Sql.NUMERIC, patientID,accessionNumber,null,metadataXML], {sqlResponse ->
                log.debug("metaExport.groovy: Database response for study {}:{}", studyInstanceUID, sqlResponse)
            }
        } catch (e) {
            log.error("metaExport.groovy: Failed database upload for study {}: {}",studyInstanceUID, e.getMessage())
            log.debug("metaExport.groovy: Closing database connection")
            sql.close()
            log.debug("metaExport.groovy: Database connection closed")
            return false
        }
    log.debug("metaExport.groovy: Closing database connection")
    sql.close()
    log.debug("metaExport.groovy: Database connection closed")

    log.info("metaExport.groovy: Study {} successfully processed", studyInstanceUID)
    ops.scheduleStudyForward("METADATASCP")
}
