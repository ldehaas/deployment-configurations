// Adds qualifiers to C-FIND requests
def requestPatientID = get(PatientID)
if (requestPatientID != null) {
    requestPatientID = requestPatientID + '^^^Research Warehouse&1.3.6.1.4.1.40920&ISO'
    set(PatientID,requestPatientID)
}
