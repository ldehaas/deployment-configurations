#!/bin/bash
#
# This script will retry all failed workflows automatically.
# This tool was built in order to fix the issues reported on
# the ticket KHC11662.
#
# Created by Victor Lauria <vlauria@vitalimages.com>
# Date: October 5, 2018
#
# Last Update: November 15, 2018
# Version 1.1
#
# Copyright Karos Health Incorporated 2018

# Check and create missing folders
bash /root/createFolders.sh

FILE_LIST="/tmp/failed.txt"

DATE="$(date +%Y-%m-%d --date='1 day ago')"
#echo $DATE

QUERY="select dataflowdocuments.dataflowid from ddprocessing, dataflowdocuments where ddprocessing.state=3 and ddprocessing.ddindex_id = dataflowdocuments.id and dataflowdocuments.lastupdated > '$DATE'"
#echo $QUERY

COMMAND="psql -t -d dicomrouter -c \"$QUERY\" > $FILE_LIST"
#echo $COMMAND

COUNTER=0

cd /tmp

sudo -H -u rialto bash -c "$COMMAND"

while read line
do
        if [ ! -z $line ]; then
                # Retry
                curl -v -X PUT "http://localhost:13337/monitoring/dataflowDocuments/$line/retry"
                COUNTER=$((COUNTER + 1))
        fi
done < $FILE_LIST

echo "Retried $COUNTER dataflow documents"

rm -f $FILE_LIST
