#!/bin/bash
#
# This script will create missing folders reported in the log files.
# This tool was built in order to fix the issues reported on
# the ticket KHC11662.
#
# Create_folderd by Victor Lauria <vlauria@vitalimages.com>
# Date: November 14, 2018
#
# Last Update: November 15, 2018
# Version: 1.0
#
# Copyright Karos Health Incorporated 2018

# Files
PATH_LIST="/tmp/nonExistingFolders.txt"
RIALTO_LOG="/home/rialto/rialto/log/rialto.log"

# Paths
ROUT_PATH="/home/rialto/rialto/etc/../var/router"

# Filter
FILTER_TAG="does not exist"

# Create Folder
create_folder () {
	if [ ! -d $1 ]; then
		mkdir $1
		echo "DEBUG: Created $1"
	fi
}

# Touch the temp tole
touch $PATH_LIST
rm -f $PATH_LIST
touch $PATH_LIST

# Dump missing paths to the tmp file
grep "$FILTER_TAG" $RIALTO_LOG | rev | awk '{print $4}' | rev | sort | uniq > $PATH_LIST


# For each path, parse it properly
FROM_AE=""
TO_AE=""
HASH_DW=""
SIUID=""

while read line; do
	FROM_AE=$(echo $line | rev | cut -d/ -f4 | rev)
	  TO_AE=$(echo $line | rev | cut -d/ -f3 | rev)
	HASH_DW=$(echo $line | rev | cut -d/ -f2 | rev)
	  SIUID=$(echo $line | rev | cut -d/ -f1 | rev)

	FOLDER_ONE="$ROUT_PATH/$FROM_AE/$TO_AE/$HASH_DW"
	FOLDER_TWO="$ROUT_PATH/$FROM_AE/$TO_AE/$HASH_DW/$SIUID"
 	
 	create_folder $FOLDER_ONE
 	create_folder $FOLDER_TWO

done < $PATH_LIST

# Remove temp file
rm -f $PATH_LIST

exit 0
