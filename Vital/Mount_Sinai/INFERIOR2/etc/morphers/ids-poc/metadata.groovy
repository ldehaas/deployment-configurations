
// use extended metadata to keep track of study instance uid
set(XDSExtendedMetadata('studyInstanceUid'), get('StudyInstanceUID'))
set(XDSCreationTime, new Date())

set(XDSLanguage, 'English')

// these codes are environment specific. 
set(XDSClassCode, 
    code('Test Class Code','Test Scheme'))
    
set(XDSHealthcareFacilityTypeCode, 
    code('Test Healthcare Facility Type Code', 'Test Scheme'))
    
set(XDSPracticeSettingCode, 
    code('Test Practice Setting Code', 'Test Scheme'))
    
set(XDSFormatCode, 
    code('1.2.840.10008.5.1.4.1.1.88.59', '1.2.840.10008.2.6.1'))
    
set(XDSConfidentialityCode,
    code('Test Confidentiality Code N', 'Test Scheme'))

