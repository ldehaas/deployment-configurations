// Mount Sinai Script v 0.81
//
// Supported SOP Classes:
//              - 1.2.840.10008.5.1.4.1.1.1.2 (Digital Mammography X-Ray Image Storage - For Presentation)
//              - 1.2.840.10008.5.1.4.1.1.1.2.1 (Digital Mammography X-Ray Image Storage - For Processing)
//              - 1.2.840.10008.5.1.4.1.1.2 (CT Image Storage)
//              - 1.2.840.10008.5.1.4.1.1.4 (MR Image Storage)
//              - 1.2.840.10008.5.1.4.1.1.1 (Computed Radiography Image Storage)
//              - 1.2.840.10008.5.1.4.1.1.20 (Nuclear Medicine Image Storage)
//
// Basic Application Level Confidentiality Profile options:
//              - Retain Longitudinal Temporal Information with Modified Dates
//              - Retain Patient Characteristics (some attributes cleaned)
//              - Retain UIDs
//              - Retain Safe Private
//
// Options _not_ currently supported:
//              - Clean Pixel Data
//              - Clean Graphics
//              - Clean Structured Content
//
// De-identification of non-human patients is not supported
//
//
//
import com.karos.connectxds.mc.UIDHasher
import groovy.sql.Sql
import java.sql.SQLException
def oldStudyInstanceUID = get(StudyInstanceUID)
def oldSOPInstanceUID = get(SOPInstanceUID)
log.info("deidLive.groovy v081: Now de-identifying Study:SOP Instance {}:{}", oldStudyInstanceUID, oldSOPInstanceUID)

// Exclude unsupported SOP Classes
def sopClassWhitelist = []
def flag = ''
def uidFlag = ''
new File('/home/rialto/rialto/etc/services/dicom-router/deid-config/sopClassWhitelist.txt').eachLine { sopClass ->
        sopClassWhitelist << sopClass
}

def instanceSOPClass = get(SOPClassUID)
if (!sopClassWhitelist.contains(instanceSOPClass)){
    log.info("deidLive.groovy: SOP Instance {}:{} was flagged. SOP Class UID {} unsupported.", oldStudyInstanceUID, oldSOPInstanceUID, instanceSOPClass)
    return false
    //  flag = 'Notsupported_'
    //  uidFlag = '.1'
}

// Exclude unknown devices
def stationNameWhitelist = []
new File('/home/rialto/rialto/etc/services/dicom-router/deid-config/stationNameWhitelist.txt').eachLine { stationName ->
    stationNameWhitelist << stationName
}

def phiSeries = []
new File('/home/rialto/rialto/etc/services/dicom-router/deid-config/seriesBlackList.txt').eachLine { flaggedSeries ->
    phiSeries << flaggedSeries
}

def phiManufacturers = []
new File('/home/rialto/rialto/etc/services/dicom-router/deid-config/manufacturerBlackList.txt').eachLine { manufacturer ->
    phiManufacturers << manufacturer
}

def phiConversionType = ['SD', 'SI', 'DF', 'DV']
def phiStrings = ['Hu', 'Du', 'Rm', 'Bh', 'BC', 'Kh', 'Qn', 'Bc', 'Ph']

// Filter out instances with burned in PHI
def modality = get(Modality) // (0008,0060)
def conversionType = get(ConversionType) // (0008,0064)
def manufacturer = get(Manufacturer) // (0008,0070)
def model = get(ManufacturerModelName) // (0008,1090)
def studyDescription = get(StudyDescription) // (0008,1030)
def seriesDescription = get(SeriesDescription) // (0008,103E)
def protocolName = get(ProtocolName) // (0018,1030)
def burnedInAnnotation = get(BurnedInAnnotation) // (0028,0301)

def instanceStationName = get(StationName)
log.debug("deidLive.groovy: Station Name: {}", instanceStationName)
def stationNameFile = new File('/home/rialto/rialto/etc/services/dicom-router/deid-config/foundStationNames.txt')
def stationNameList = []
stationNameFile.eachLine { foundStationName ->
    stationNameList << foundStationName
}
//update for KHC14868 - define a list of station names to reject
def stationNameToReject = ['MEDPC']

if (!stationNameWhitelist.contains(instanceStationName)) {
    log.info("deidLive.groovy: SOP Instance {}:{} was flagged. Station Name {} unrecognized.", oldStudyInstanceUID, oldSOPInstanceUID, instanceStationName)
    if ((instanceStationName != null) && (!stationNameList.contains(instanceStationName))) {
        //update for KHC14868
        if (stationNameToReject.contains(instanceStationName){
            log.info("deidLive.groovy: This Station Name {} is rejected.", instanceStationName)
        }
        else {
           log.debug("deidLive.groovy: Found unknown Station Name {}. Adding it to list.", instanceStationName)
           stationNameFile << manufacturer + ': ' + model + ': \n'
           stationNameFile << instanceStationName + '\n'
        }
        flag = 'AEFlag_'
        uidFlag = '.3'
    }  
}

if (burnedInAnnotation == 'YES') {
    log.debug("deidLive.groovy: SOP Instance {}:{} was flagged. Image contains burned in demographics", oldStudyInstanceUID, oldSOPInstanceUID)
    flag = 'PHIFlag_'
    uidFlag = '.2'
    //return false
}

if (manufacturer != null) {
    phiManufacturers.each { vendor ->
        if (manufacturer.toUpperCase().contains(vendor.toUpperCase())) {
            log.debug("deidLive.groovy: SOP Instance {}:{} was flagged. Images from manufacturer {} contain burned in demographics", oldStudyInstanceUID, oldSOPInstanceUID, manufacturer)
            flag = 'PHIFlag_'
            uidFlag = '.2'
            //return false
        }
    }
}

if (studyDescription != null) {
    phiSeries.each { study ->
        if (studyDescription.toUpperCase().contains(study.toUpperCase())) {
            log.debug("deidLive.groovy: SOP Instance {}:{} was not imported. Series of type {} contain burned in demographics", oldStudyInstanceUID, oldSOPInstanceUID, studyDescription)
            flag = 'PHIFlag_'
            uidFlag = '.2'
            //return false
        }
    }
}

if (seriesDescription != null) {
    phiSeries.each { series ->
        if (seriesDescription.toUpperCase().contains(series.toUpperCase())) {
            log.debug("deidLive.groovy: SOP Instance {}:{} was not imported. Series of type {} contain burned in demographics", oldStudyInstanceUID, oldSOPInstanceUID, seriesDescription)    
            flag = 'PHIFlag_'
            uidFlag = '.2'
            //return false    
        }
    }
}
else {
      log.debug("deidLive.groovy: SOP Instance {}:{} was not imported. Series description is empty (KHC15262)", oldStudyInstanceUID, oldSOPInstanceUID, seriesDescription)    
      flag = 'PHIFlag_'
      uidFlag = '.2'  
}


if (protocolName != null) {
    phiSeries.each { protocol ->
        if (protocolName.toUpperCase().contains(protocol.toUpperCase())) {
            log.debug("deidLive.groovy: SOP Instance {}:{} was not imported. Protocol type {} contains burned in demographics", oldStudyInstanceUID, oldSOPInstanceUID, protocolName)
            flag = 'PHIFlag_'
            uidFlag = '.2'
            // return false
        }
    }
}

if (phiConversionType.contains(conversionType)) {
    log.debug("deidLive.groovy: SOP Instance {}:{} was not imported. Conversion type {} contains burned in demographics", oldStudyInstanceUID, oldSOPInstanceUID, conversionType)
    flag = 'PHIFlag_'
    uidFlag = '.2'
    //return false
}

if ((modality == 'OT') && (conversionType == 'DI') && (manufacturer == 'Philips Medical Systems')) {
    log.debug("deidLive.groovy: SOP Instance {}:{} was not imported. {} image type contains burned in demographics", oldStudyInstanceUID, oldSOPInstanceUID, manufacturer)
    flag = 'PHIFlag_'
    uidFlag = '.2'
    //return false
}

if ((modality == 'XA') && (instanceSOPClass == '1.2.840.10008.5.1.4.1.1.7') && (manufacturer == 'Philips Medical Systems')) {
    log.debug("deidLive.groovy: SOP Instance {}:{} was not imported. Image type contains burned in demographics", oldStudyInstanceUID, oldSOPInstanceUID)
    flag = 'PHIFlag_'
    uidFlag = '.2'
    //return false
}

//  if (modality == 'US') {
//      log.debug("deidLive.groovy: SOP Instance {}:{} was not imported. {} image type contains burned in demographics", oldStudyInstanceUID, oldSOPInstanceUID, manufacturer)
//      flag = 'PHIFlag_'
//      uidFlag = '.2'
//      return false
//  }

if ((instanceSOPClass == '1.2.840.10008.5.1.4.1.1.7') && (manufacturer == 'TOSHIBA')) { // && (get(SeriesNumber) == '9000')) {
    log.debug("deidLive.groovy: SOP Instance {}:{} was flagged. {} image type contains burned in demographics", oldStudyInstanceUID, oldSOPInstanceUID, manufacturer)
    flag = 'PHIFlag_'
    uidFlag = '.2'
    //return false
}

def imageType = getList(ImageType)
log.debug("deidLive.groovy: Image Type: {}", imageType)
if (imageType != null) {
    if (imageType instanceof List) {
        imageType.each { imageTypeValue ->
            log.debug("deidLive.groovy: Image type value: {}", imageTypeValue)
            if ((imageTypeValue.contains('DOC')) || (imageTypeValue.contains('SCAN')) || (imageTypeValue.contains('REF'))) {
                log.debug("deidLive.groovy: SOP Instance {}:{} was flagged. {} image type contains burned in demographics", oldStudyInstanceUID, oldSOPInstanceUID, imageTypeValue)
                flag = 'PHIFlag_'
                uidFlag = '.2'
            }
        }
    } else if (imageType.contains('DI SCAN DOC')) {
            log.debug("deidLive.groovy: SOP Instance {}:{} was flagged. {} image type contains burned in demographics", oldStudyInstanceUID, oldSOPInstanceUID, imageType)
        flag = 'PHIFlag_'
        uidFlag = '.2'
    }
}

if (modality == 'US') {
        if (burnedInAnnotation == 'NO') {
            log.info("deidLive.groovy: SOP Instance {}:{} is a corrected ultrasound image." , oldStudyInstanceUID, oldSOPInstanceUID)
            flag = 'US_'
            uidFlag = '.5'
        } else {
            flag = 'PHIFlag_'
            uidFlag = '.2'
        }
}

if (flag == 'PHIFlag_') {
    log.info("deidLive.groovy: SOP Instance {}:{} was not imported due to burned in demographics." , oldStudyInstanceUID, oldSOPInstanceUID)
    set(BurnedInAnnotation, 'YES')
}









// UID Hash function
// see http://oid-info.com/get/1.3.6.1.4.1.40920 for Icahn School of Medicine at Mount Sinai OID registration
// alternative would be http://oid-info.com/get/2.16.840.1.113883.3.515 for Mount Sinai OID registration
def localOID = '1.3.6.1.4.1.40920' 
UIDHasher hasher = new UIDHasher(localOID)

// ** PatientMappingDBQuery **
// database parameters
// TEST
// def mappingDBURL = 'jdbc:oracle:thin:@sdwhodbqa01.mountsinai.org:1521:MSDWUSERS'
// def mappingDBUser = 'vioarchive_lookup'
// def mappingDBPass = 'msdw'
// Class.forName('oracle.jdbc.driver.OracleDriver')
// LIVE

// input fields mapped
def oldPatientID = get(PatientID)
def olduniversalEntityUID = get("IssuerOfPatientIDQualifiersSequence/UniversalEntityID")
log.debug("deidLive.groovy: Old Patient Domain: {}",olduniversalEntityUID)
def oldAccessionNumber = get(AccessionNumber)
if (oldAccessionNumber == null) {
        log.warn("deidLive.groovy: Missing accession number. Cannot be de-identified.")
        return false
}
def newPatientID 
def newAccessionNumber 
def studyOffset

def mappingFileName = '/home/rialto/rialto/var/tmp/' + oldStudyInstanceUID + '.qry'
log.debug("deidLive.groovy: Looking for mapping file: {}", mappingFileName)
def mappingFile = new File(mappingFileName)

if (!mappingFile.exists()) {
    log.warn("deidLive.groovy: Mapping file not found. Attempting to re-create.")
    
    def mappingDBURL = 'jdbc:oracle:thin:@sdwhetlprod01.mountsinai.org:1521:ETLPROD'
    def mappingDBUser = 'vioarchive_lookup'
    def mappingDBPass = 'msdw'
    Class.forName('oracle.jdbc.driver.OracleDriver')

    log.info("deidLive.groovy: Initiating connection to {}", mappingDBURL)
    try {
        sql = Sql.newInstance(mappingDBURL, mappingDBUser, mappingDBPass)
    } catch (SQLException sqlConnectError) {
        log.error("deidLive.groovy: Failed to connect to {}: {}", mappingDBURL, sqlConnectError)
        return false
    }
    log.debug("deidLive.groovy: Connected to database")

    // get new PatientID
    log.debug("deidLive.groovy: Getting New Patient ID")
    try {
        sql.call '{? = call get_deid(?,?,?)}', [Sql.LONGVARCHAR, oldPatientID, oldAccessionNumber,'MRN'], {row ->
            log.debug("deidLive.groovy: Returned {}", row)
            newPatientID = row
        }
        log.debug("deidLive.groovy: New Patient ID: {}",newPatientID)
    } catch (e) {
        log.warn("deidLive.groovy: Failed Patient ID Lookup for Study {}: {}", oldStudyInstanceUID, e.getMessage())
        log.debug("deidLive.groovy: Closing database connection")
        sql.close()
        log.info("deidLive.groovy: Database connection closed")
        return false
    } 

    // get new AccessionNumber
    log.debug("deidLive.groovy: Getting New Accession Number")
    try {
        sql.call '{? = call get_deid(?,?,?)}', [Sql.LONGVARCHAR, oldPatientID, oldAccessionNumber,'accession_no'], {row ->
            log.debug("deidLive.groovy: Returned {}", row)
            newAccessionNumber = row
        }
        log.debug("deidLive.groovy: New Accession Number: {}",newAccessionNumber)
    } catch (e) {
        log.warn("deidLive.groovy: Failed Accession Number lookup for Study {}: {}", oldStudyInstanceUID, e.getMessage())
        log.debug("deidLive.groovy: Closing database connection")
        sql.close()
        log.info("deidLive.groovy: Database connection closed")
        return false
    }

    // get studyOffset
    log.debug("deidLive.groovy: Getting Study Offset")
    try {
        sql.call '{? = call get_deid(?,?,?)}', [Sql.LONGVARCHAR, oldPatientID, oldAccessionNumber,'dateshift'], {row ->
            log.debug("deidLive.groovy: Returned {}", row)
                studyOffset = row
        }
        log.debug("deidLive.groovy: Study Offset: {}",studyOffset)
        studyOffset = studyOffset.toInteger()
    } catch (e) {
        log.warn("deidLive.groovy: Failed Study Offset lookup for Study {}: {}", oldStudyInstanceUID, e.getMessage())
        log.debug("deidLive.groovy: Closing database connection")
        sql.close()
        log.info("deidLive.groovy: Database connection closed")
        return false
    }

    log.debug("deidLive.groovy: Closing database connection")
    sql.close()
    log.info("deidLive.groovy: Database connection closed")

    mappingFile << newPatientID + '\n'
    mappingFile << newAccessionNumber + '\n'
    mappingFile << studyOffset

    mappingFile.deleteOnExit()

}

def mappingResults = []

mappingFile.eachLine { mappingItem ->
    mappingResults << mappingItem
}

newPatientID = mappingResults[0]

newAccessionNumber = mappingResults[1]

studyOffset = mappingResults[2].toInteger()

// set flags
newPatientID = flag + newPatientID
newAccessionNumber = flag + newAccessionNumber

// Other pre-defined values
def newUniversalEnityID = localOID
def newIssuerOfPatientID = 'Research Warehouse'
def newInstitutionName = 'Research Warehouse'
def replacementYear = new Date().format("yyyy").toInteger() - 90 // current year - 90

// ** GROUP 0002 **
def oldMediaStorageSOPInstanceUID = get(MediaStorageSOPInstanceUID)
if (oldMediaStorageSOPInstanceUID != null) {
        set(MediaStorageSOPInstanceUID, hasher.hashUID(oldMediaStorageSOPInstanceUID)) // (0008,0018)
}
// ** END OF GROUP 0002 **

// ** GROUP 0008 **
// add studyOffset to StudyDate (0008,0020)
def newStudyDate
def oldStudyDate = get(StudyDate)
log.debug("deidLive.groovy: Old Study Date: {}", oldStudyDate)
if (oldStudyDate == null) {
    newStudyDate = new Date()
}
else {
    newStudyDate = new Date().parse('yyyyMMdd', oldStudyDate)
}
def studyYear = newStudyDate.format('yyyy')
newStudyDate.setDate(newStudyDate.getDate() + studyOffset)
set(StudyDate,newStudyDate.format('yyyyMMdd'))
log.debug("deidLive.groovy: New Study Date: {}", get(StudyDate))

// add studyOffset to InstanceCreationDate (0008,0012)
def newInstanceCreationDate = new Date()
def oldInstanceCreationDate = get(InstanceCreationDate)
log.debug("deidLive.groovy: Old Instance Creation Date: {}", oldInstanceCreationDate)
if (oldInstanceCreationDate == null){
        newInstanceCreationDate = newStudyDate
}
else {
        newInstanceCreationDate = new Date().parse("yyyyMMdd",oldInstanceCreationDate)
        newInstanceCreationDate.setDate(newInstanceCreationDate.getDate() + studyOffset)
}
set(InstanceCreationDate,newInstanceCreationDate.format('yyyyMMdd'))
log.debug("deidLive.groovy: New Instance Creation Date: {}", get(InstanceCreationDate))

// ignore InstanceCreationTime (0008,0013)

def oldInstanceCreatorUID = get(InstanceCreatorUID) // (0008,0014)
if (oldInstanceCreatorUID != null) {
        set(InstanceCreatorUID, hasher.hashUID(oldInstanceCreatorUID))  
} else {
        remove(InstanceCreatorUID)
}

// add studyOffset to InstanceCoercionDateTime (0008,0015) !MISSING!

if (oldSOPInstanceUID != null) {
        set(SOPInstanceUID, hasher.hashUID(oldSOPInstanceUID)) // (0008,0018)
}

// add studyOffset to SeriesDate (0008,0021)
def newSeriesDate = new Date()
def oldSeriesDate = get(SeriesDate)
log.debug("deidLive.groovy: Old Series Date: {}", oldSeriesDate)
if (oldSeriesDate == null){
        newSeriesDate = newStudyDate
}
else {
        newSeriesDate = new Date().parse("yyyyMMdd",oldSeriesDate) 
        newSeriesDate.setDate(newSeriesDate.getDate() + studyOffset)
}
set(SeriesDate,newSeriesDate.format('yyyyMMdd'))
log.debug("deidLive.groovy: New Series Date: {}", get(SeriesDate))

// add studyOffset to AcquisitionDate (0008,0022)
def newAcquisitionDate = new Date()
def oldAcquisitionDate = get(AcquisitionDate)
log.debug("deidLive.groovy: Old Acquisition Date: {}", oldAcquisitionDate)
log.debug("deidLive.groovy: Old Acquisition Time: {}", get(AcquisitionTime))
if (oldAcquisitionDate == null){
        newAcquisitionDate = newStudyDate
}
else {
        newAcquisitionDate = new Date().parse("yyyyMMdd",oldAcquisitionDate)
        newAcquisitionDate.setDate(newAcquisitionDate.getDate() + studyOffset)
}
set(AcquisitionDate,newAcquisitionDate.format('yyyyMMdd'))
log.debug("deidLive.groovy: New Acquisition Date: {}", get(AcquisitionDate))

// add studyOffset to ContentDate (0008,0023)
def newContentDate = new Date()
def oldContentDate = get(ContentDate)
log.debug("deidLive.groovy: Old Content Date: {}", oldContentDate)
if (oldContentDate == null){
        newContentDate = newStudyDate
}
else {
        newContentDate = new Date().parse("yyyyMMdd",oldContentDate) 
        newContentDate.setDate(newContentDate.getDate() + studyOffset)
}
set(ContentDate,newContentDate.format('yyyyMMdd'))
log.debug("deidLive.groovy: New Content Date: {}", get(ContentDate))

remove(OverlayDate) // (0008,0024)
remove(CurveDate) // (0008,0025)

// add studyOffset to AcquisitionDateTime (0008,002A)
def newAcquisitionDateTime = new Date()
def oldAcquisitionDateTime = get(AcquisitionDateTime)
log.debug("deidLive.groovy: Old Acquisition Datetime: {}", oldAcquisitionDateTime)
def dateTimeFormat = 'yyyyMMddHHmmss.SSS'
if (oldAcquisitionDateTime == null){
        if (get(AcquisitionTime) != null){
                oldAcquisitionDateTime = get(AcquisitionDate) + get(AcquisitionTime)
        } else {
                oldAcquisitionDateTime = get(AcquisitionDate)
        }
        if (oldAcquisitionDateTime.length() > 18){
                oldAcquisitionDateTime = oldAcquisitionDateTime.take(18)
        }
        dateTimeFormat = dateTimeFormat.take(oldAcquisitionDateTime.length())
        newAcquisitionDateTime = new Date().parse(dateTimeFormat,oldAcquisitionDateTime)
}
else {
        dateTimeFormat = dateTimeFormat.take(oldAcquisitionDateTime.length())
    newAcquisitionDateTime = new Date().parse(dateTimeFormat,oldAcquisitionDateTime)
    newAcquisitionDateTime.setDate(newAcquisitionDateTime.getDate() + studyOffset)
}
set(AcquisitionDateTime,newAcquisitionDateTime.format(dateTimeFormat))
log.debug("deidLive.groovy: New Acquisition Datetime: {}", get(AcquisitionDateTime))

// ignore StudyTime (0008,0030)
// ignore SeriesTime (0008,0031)
// ignore AcquisitionTime (0008,0032)
// ignore ContentTime (0008,0033)
remove(OverlayTime) // (0008,0034)
remove(CurveTime) // (0008,0035)
set(AccessionNumber,newAccessionNumber) // (0008,0050)

remove(IssuerOfAccessionNumberSequence) // blank sequence (0008,0051)
//
//get(IssuerOfAccessionNumberSequence) // new sequence
//set("IssuerOfAccessionNumberSequence/LocalNamespaceEntityID",newIssuerOfPatientID) // (0040,0031)
//set("IssuerOfAccessionNumberSequence/UniversalEntityID",newUniversalEnityID) // (0040,0032)
//set("IssuerOfAccessionNumberSequence/UniversalEntityIDType",'ISO') // (0040,0033)

set(InstitutionName, newInstitutionName) // (0008,0080)
remove(InstitutionAddress) // (0008,0081)
set(ReferringPhysicianName, null) // (0008,0090)
remove(ReferringPhysicianIdentificationSequence) // (0008,0096)
// remove(ConsultingPhysicianName) // (0008,009C) !MISSING!
// remove(ConsultingPhysicianIdentificationSequence) // (0008,009D) !MISSING!
set(TimezoneOffsetFromUTC, '-0500') // (0008,0201)

// update ProcedureCodeSequence (0008,1032) and use code meanings to create new StudyDescription (0008,1030)
def today = new Date().format("yyyyMMdd.HHmmss")
def procedureCodes = get(ProcedureCodeSequence)
// studyDescription = today + ':' TEST ONLY
studyDescription = studyYear + ':'
procedureCodes.each {procedureCode ->
    studyDescription = studyDescription + ' ' + procedureCode.CodeMeaning
}
set(StudyDescription,studyDescription)

//if (seriesDescription != null) { // clean Series Description (0008,103E) of identified strings
//    phiStrings.each { phiString ->
//        seriesDescription = seriesDescription.minus(phiString)
//    }
//}
///set(SeriesDescription,seriesDescription)

// future option: update SeriesDescriptionCodeSequence (0008,103F) and use code meanings to create new SeriesDescription (0008,103F)
// def newSeriesDescription
// def seriesCodes = get(SeriesDescriptionCodeSequence) 
// seriesCodes.each { seriesCode ->
//      // map procedure code to new value if desired
//      if (newSeriesDescription == null) {
//              newSeriesDescription = seriesCode.get(CodeMeaning)
//      }
//      else {
//              newSeriesDescription = newSeriesDescription + ', ' + seriesCode.get(CodeMeaning)
//      }
// }
// set(SeriesDescription,newSeriesDescription)

remove(InstitutionalDepartmentName) // (0008,1040) 
remove(PhysiciansOfRecord) // (0008,1048)
remove(PhysiciansOfRecordIdentificationSequence) // (0008,1049)
remove(PerformingPhysicianName) // (0008,1050)
remove(PerformingPhysicianIdentificationSequence) // (0008,1052)
remove(NameOfPhysiciansReadingStudy) // (0008,1060)
remove(PhysiciansReadingStudyIdentificationSequence) // (0008,1062)
remove(OperatorsName) // (0008,1070)
remove(OperatorIdentificationSequence) // (0008,1072)

def newReferencedStudySequence = get(ReferencedStudySequence) // update UIDs in ReferencedStudySequence (0008,1110)
if (newReferencedStudySequence != []){
        newReferencedStudySequence.each { referencedSOPInstance ->
                // create new UID for referencedSOPInstance.ReferencedSOPInstanceUID (0008,1155)
                if (referencedSOPInstance.get(ReferencedSOPInstanceUID) != null){
                        referencedSOPInstance.set(ReferencedSOPInstanceUID, hasher.hashUID(referencedSOPInstance.get(ReferencedSOPInstanceUID))) // (0008,0018)
                }
        }
} else {
        remove(ReferencedStudySequence)
}

def newReferencedSeriesSequence = get(ReferencedSeriesSequence) // update UIDs in ReferencedSeriesSequence (0008,1115)
if (newReferencedSeriesSequence != []){
        newReferencedSeriesSequence.each { referencedSeries ->
                // create new UID for referencedSeries.SeriesInstanceUID (0020,000E)
                if (referencedSeries.get(SeriesInstanceUID) != null){
                        referencedSeries.set(SeriesInstanceUID, hasher.hashUID(referencedSeries.get(SeriesInstanceUID))) 
                }
                def newReferencedInstanceSequence = get(ReferencedInstanceSequence) // update UIDs in ReferencedInstanceSequence (0008,114A)
                newReferencedInstanceSequence.each { referencedSOPInstance ->
                        // create new UID for referencedSOPInstance.ReferencedSOPInstanceUID (0008,1155)
                        if (referencedSOPInstance.get(ReferencedSOPInstanceUID) != null){
                                referencedSOPInstance.set(ReferencedSOPInstanceUID, hasher.hashUID(referencedSOPInstance.get(ReferencedSOPInstanceUID))) // (0008,0018)
                        }
                }
        }
} else {
        remove(ReferencedSeriesSequence)
}

newReferencedSeriesSequence = get(ReferencedSeriesSequence) // update UIDs in ReferencedSeriesSequence (0008,1115)
if (newReferencedSeriesSequence != []){
        newReferencedSeriesSequence.each { referencedSeries ->
                // create new UID for referencedSeries.SeriesInstanceUID (0020,000E)
                if (referencedSeries.get(SeriesInstanceUID) != null){
                        referencedSeries.set(SeriesInstanceUID, hasher.hashUID(referencedSeries.get(SeriesInstanceUID))) 
                }
                def newReferencedInstanceSequence = get(ReferencedInstanceSequence) // update UIDs in ReferencedInstanceSequence (0008,114A)
                newReferencedInstanceSequence.each { referencedSOPInstance ->
                        // create new UID for referencedSOPInstance.ReferencedSOPInstanceUID (0008,1155)
                        if (referencedSOPInstance.get(ReferencedSOPInstanceUID) != null){
                                referencedSOPInstance.set(ReferencedSOPInstanceUID, hasher.hashUID(referencedSOPInstance.get(ReferencedSOPInstanceUID))) // (0008,0018)
                        }
                }
        }
} else {
        remove(ReferencedSeriesSequence)
}

def newStudiesContainingOtherReferencedInstancesSequence = get(StudiesContainingOtherReferencedInstancesSequence) // update UIDS in StudiesContainingOtherReferencedInstancesSequence (0008,1200)
if (newStudiesContainingOtherReferencedInstancesSequence != []){
        newStudiesContainingOtherReferencedInstancesSequence.each { referencedStudy ->
                // create new UID for referencedStudy.StudyInstanceUID (0020,000D)
                if (referencedStudy.get(StudyInstanceUID) != null){
                        referencedStudy.set(StudyInstanceUID, hasher.hashUID(referencedStudy.get(StudyInstanceUID))) 
                }
                newReferencedSeriesSequence = get(ReferencedSeriesSequence) // update UIDs in ReferencedSeriesSequence (0008,1115)
                newReferencedSeriesSequence.each { referencedSeries ->
                        // create new UID for referencedSeries.SeriesInstanceUID (0020,000E)
                        if (referencedSeries.get(SeriesInstanceUID) != null){
                                referencedSeries.set(SeriesInstanceUID, hasher.hashUID(referencedSeries.get(SeriesInstanceUID))) 
                        }
                        def newReferencedInstanceSequence = get(ReferencedInstanceSequence) // update UIDs in ReferencedInstanceSequence (0008,114A)
                        newReferencedInstanceSequence.each { referencedSOPInstance ->
                                // create new UID for referencedSOPInstance.ReferencedSOPInstanceUID (0008,1155)
                                if (referencedSOPInstance.get(ReferencedSOPInstanceUID) != null){
                                        referencedSOPInstance.set(ReferencedSOPInstanceUID, hasher.hashUID(referencedSOPInstance.get(ReferencedSOPInstanceUID))) // (0008,0018)
                                }
                        }
                }
        }
} else {
        remove(StudiesContainingOtherReferencedInstancesSequence)
}

remove(ReferencedPatientSequence) // (0008,1120)

def newReferencedImageSequence = get(ReferencedImageSequence) // update UIDs in ReferencedImageSequence (0008,1140)
if (newReferencedImageSequence != []){
        newReferencedImageSequence.each { referencedSOPInstance ->
                if (referencedSOPInstance.get(ReferencedSOPInstanceUID) != null) {
                        referencedSOPInstance.set(ReferencedSOPInstanceUID, hasher.hashUID(referencedSOPInstance.get(ReferencedSOPInstanceUID))) // (0008,0018)
                }
        }
} else {
        remove(ReferencedImageSequence)
}

def newRelatedSeriesSequence = get(RelatedSeriesSequence) // update UIDs in RelatedSeriesSequence (0008,1250)
if (newRelatedSeriesSequence != []) {
        newRelatedSeriesSequence.each { referencedSeries ->
                if (referencedSeries.get(StudyInstanceUID) != null){
                        referencedSeries.set(StudyInstanceUID, hasher.hashUID(referencedSeries.get(StudyInstanceUID))) // (0020,000D)
                }
                if (referencedSeries.get(SeriesInstanceUID) != null){
                        referencedSeries.set(SeriesInstanceUID, hasher.hashUID(referencedSeries.get(SeriesInstanceUID))) // (0020,000E)
                }
        }
} else {
        remove(RelatedSeriesSequence)
}

// ignore DerivationDescription (0008,2111)
// future option: update DerivationCodeSequence (0008,9215) and use code meanings to create new DerivationDescription (0008,2111)
// def newDerivationDescription
// def derivationCodes = get(DerivationCodeSequence) 
// derivationCodes.each { derivationCode ->
//      // map code to new value if desired
//      if (newDerivationDescription == null) {
//              newDerivationDescription = derivationCode.get(CodeMeaning)
//      }
//      else {
//              newDerivationDescription = newDerivationDescription + ', ' + derivationCode.get(CodeMeaning)
//      }
// }
// set(DerivationDescription,newDerivationDescription)

def newSourceImageSequence = get(SourceImageSequence) // update UIDs in ReferencedImageSequence (0008,2112)
if (newSourceImageSequence != []){
        newSourceImageSequence.each { sourceSOPInstance ->
                if (sourceSOPInstance.get(ReferencedSOPInstanceUID) != null) {
                        sourceSOPInstance.set(ReferencedSOPInstanceUID, hasher.hashUID(sourceSOPInstance.get(ReferencedSOPInstanceUID))) // (0008,0018)
                }
        }
} else {
        remove(SourceImageSequence)
}

// create new UIDs for multivalued IrradiationEventUID (0008,3010)
def newIrradiationEventUIDs = get(IrradiationEventUID)
if (newIrradiationEventUIDs != null){
        if (newIrradiationEventUIDs instanceof List){
                newIrradiationEventUIDs.each { irradiationUID ->
                        log.debug("deidLive.groovy: Old irradiationUID: {}", irradiationUID)
                        irradiationUID = hasher.hashUID(irradiationUID)
                }
        } else {
                log.debug("deidLive.groovy: Old irradiationUID: {}", newIrradiationEventUIDs)
                newIrradiationEventUIDs = hasher.hashUID(newIrradiationEventUIDs)
        }
        set(IrradiationEventUID,newIrradiationEventUIDs)
        log.debug("deidLive.groovy: New irradiationUID: {}", get(IrradiationEventUID))
} else {
        remove(IrradiationEventUID)
}
// ** END OF GROUP 0008 **

// ** GROUP 0010 **
set(PatientName,newPatientID) // (0010,0010)
set(PatientID,newPatientID) // (0010,0020)
set(IssuerOfPatientID,newIssuerOfPatientID) // (0010,0021)

remove(IssuerOfPatientIDQualifiersSequence) // blank sequence (0010,0024)
get(IssuerOfPatientIDQualifiersSequence) // new empty sequence
set("IssuerOfPatientIDQualifiersSequence/UniversalEntityID",newUniversalEnityID) // (0040,0032)
set("IssuerOfPatientIDQualifiersSequence/UniversalEntityIDType",'ISO') // (0040,0033)
log.debug("deidLive.groovy: New patient domain: {}", get("IssuerOfPatientIDQualifiersSequence/UniversalEntityID"))

// remove(SourcePatientGroupIdentificationSequence) // (0010,0026) !MISSING!
// remove(GroupOfPatientsIdentificationSequence) // (0010,0027) !MISSING!

def oldBirthDate = get(PatientBirthDate)
def newBirthDate
if (oldBirthDate != null){
        if (oldBirthDate.length() == 8) {
                newBirthDate = new Date().parse('yyyyMMdd', oldBirthDate)
        } else if (oldBirthDate.length() == 6) {
                newBirthDate = new Date().parse('yyyyMMdd', oldBirthDate + '01')
        } else if (oldBirthDate.length() == 4) {
                newBirthDate = new Date().parse('yyyyMMdd', oldBirthDate + '0101')
        }
        newBirthDate.setDate(newBirthDate.getDate() + studyOffset)
        newBirthDate = newBirthDate.format('yyyyMMdd')
        if (newBirthDate.take(4).toInteger() < replacementYear) { // If year is earlier than (current year - 90) set year to (current year - 90)
                newBirthDate = '10660101'
        }
        set(PatientBirthDate,newBirthDate) // (0010,0030)
}

remove(PatientBirthTime) // (0010,0032)
// keep PatientSex (0010,0040)
remove(PatientInsurancePlanCodeSequence) // (0010,0050)
remove(PatientPrimaryLanguageCodeSequence) // (0010,0101)
remove(PatientPrimaryLanguageCodeSequence) // (0010,0102)
remove(OtherPatientIDs) // (0010,1000)
remove(OtherPatientNames) // (0010,1001)
remove(OtherPatientIDsSequence) // (0010,1002)
remove(PatientBirthName) // (0010,1005)

// update PatientAge (0010,1010) if missing or > 90
def oldPatientAge = get(PatientAge)
if (oldPatientAge != null){
        if (oldPatientAge.charAt(oldPatientAge.length()-1) == 'Y') {
                def yearAge = oldPatientAge.take(oldPatientAge.length()-1).toInteger()
                if (yearAge > 90) {
                        set(PatientAge,'90Y')
                }
        }
}
// to be added: calculate date if missing

// ignore PatientSize (0010,1020)
// if (newPatientSize != null) {
//      if (newPatientSize.charAt(newPatientSize.length()-1) == '.') { // unless it is a '.'
//              newPatientSize = newPatientSize.take(newPatientSize.length()-2) + '0'
//      }
//      else {
//              newPatientSize = newPatientSize.take(newPatientSize.length()-1) + '0'
//      }
//      set(PatientSize,newPatientSize)
// }

// ignore PatientWeight (0010,1030)
// if (newPatientWeight != null) {
//      if (newPatientWeight.charAt(newPatientWeight.length()-1) == '.') { // unless it is a '.'
//              newPatientWeight = newPatientWeight.take(newPatientWeight.length()-2) + '0'
//      }
//      else {
//              newPatientWeight = newPatientWeight.take(newPatientWeight.length()-1) + '0'
//      }
//      set(PatientWeight,newPatientWeight)
// }

remove(PatientAddress) // (0010,1040)
remove(InsurancePlanIdentification) // (0010,1050)
remove(PatientMotherBirthName) // (0010,1060)
remove(MilitaryRank) // (0010,1080)
remove(BranchOfService) // (0010,1081)
remove(MedicalRecordLocator) // (0010,1090)
// remove(ReferencedPatientPhotoSequence) // (0010,1100) !MISSING!
remove(MedicalAlerts) // (0010,2000)
remove(Allergies) // (0010,2110)
remove(CountryOfResidence) // (0010,2150)
remove(RegionOfResidence) // (0010,2152)
remove(PatientTelephoneNumbers) // (0010,2154)
// remove(PatientTelecomInformation) // (0010,2155) !MISSING!
// ignore EthnicGroup (0010,2160)

// ignore Occupation (0010,2180)
// ignore SmokingStatus (0010,21A0)

remove(AdditionalPatientHistory)        // (0010,21B0)

remove(PregnancyStatus) // (0010,21C0)
remove(LastMenstrualDate) // (0010,21D0)
remove(PatientReligiousPreference) // (0010,21F0)
remove(PatientSexNeutered) // (0010,2203)
remove(ResponsiblePerson) // (0010,2297)
remove(ResponsibleOrganization) // (0010,2299)

remove(PatientComments) // (0010,4000)
// ** END OF GROUP 0010 **

// ** GROUP 0012 **
set(PatientIdentityRemoved, 'YES') // (0012,0062)
set(DeidentificationMethod, ['Basic Application Confidentiality Profile','Retain Longitudinal Temporal Information Modified Dates Option','Retain Patient Characteristics Option','Retain UIDs Option', 'Retain Safe Private Option']) // (0012,0063)
get(DeidentificationMethodCodeSequence) // (0012,0064) new empty sequence
set("DeidentificationMethodCodeSequence[1]/CodeValue", '113100') // (0008,0100)
set("DeidentificationMethodCodeSequence[1]/CodingSchemeDesignator", 'DCM') // (0008,0102)
set("DeidentificationMethodCodeSequence[1]/CodeMeaning", 'Basic Application Confidentiality Profile') // (0008,0104)
set("DeidentificationMethodCodeSequence[2]/CodeValue", '113107') // (0008,0100)
set("DeidentificationMethodCodeSequence[2]/CodingSchemeDesignator", 'DCM') // (0008,0102)
set("DeidentificationMethodCodeSequence[2]/CodeMeaning", 'Retain Longitudinal Temporal Information Modified Dates Option') // (0008,0104)
set("DeidentificationMethodCodeSequence[3]/CodeValue", '113108') // (0008,0100)
set("DeidentificationMethodCodeSequence[3]/CodingSchemeDesignator", 'DCM') // (0008,0102)
set("DeidentificationMethodCodeSequence[3]/CodeMeaning", 'Retain Patient Characteristics Option') // (0008,0104)
set("DeidentificationMethodCodeSequence[4]/CodeValue", '113110') // (0008,0100)
set("DeidentificationMethodCodeSequence[4]/CodingSchemeDesignator", 'DCM') // (0008,0102)
set("DeidentificationMethodCodeSequence[4]/CodeMeaning", 'Retain UIDs Option') // (0008,0104)
set("DeidentificationMethodCodeSequence[4]/CodeValue", '113111') // (0008,0100)
set("DeidentificationMethodCodeSequence[4]/CodingSchemeDesignator", 'DCM') // (0008,0102)
set("DeidentificationMethodCodeSequence[4]/CodeMeaning", 'Retain Safe Private Option') // (0008,0104)
// ** END GROUP 0012

// ** GROUP 0018 **
// ignore ContrastBolusAgent (0018,0010)
// future option: use ContrastBolusAgentSequence (0018,0012) code meanings to create new ContrastBolusAgent (0008,0010)
// def newContrastBolusAgent
// def contrastAgents = get(ContrastBolusAgentSequence) 
// contrastAgents.each { agent ->
//      // map procedure code to new value if desired
//      if (newContrastBolusAgent == null) {
//              newContrastBolusAgent = agent.get(CodeMeaning)
//      }
//      else {
//              newContrastBolusAgent = newContrastBolusAgent + ', ' + agent.get(CodeMeaning)
//      }
// }
// set(ContrastBolusAgent,newContrastBolusAgent)

// add studyOffset to DateofSecondaryCapture (0018,1012)
def newDateOfSecondaryCapture 
def oldDateOfSecondaryCapture = get(DateOfSecondaryCapture)
log.debug("deidLive.groovy: Old DateOfSecondaryCapture: {}", oldDateOfSecondaryCapture)
if (oldDateOfSecondaryCapture == null) {
    newDateOfSecondaryCapture = new Date()
}
else {
    newDateOfSecondaryCapture = new Date().parse('yyyyMMdd', oldDateOfSecondaryCapture)
}
newDateOfSecondaryCapture.setDate(newDateOfSecondaryCapture.getDate() + studyOffset)
set(DateOfSecondaryCapture,newDateOfSecondaryCapture.format('yyyyMMdd'))
log.debug("deidLive.groovy: New DateOfSecondaryCapture: {}", get(DateOfSecondaryCapture))

remove(DeviceSerialNumber) // (0018,1000)
remove(DeviceUID) // (0018,1002)
remove(PlateID) // (0018,1004)
remove(GeneratorID) // (0018,1005)
remove(CassetteID) // (0018,1007)
remove(GantryID) // (0018,1008)

//if (protocolName != null) { // clean Protocol Name (0018,1030) of identified strings
//    phiStrings.each { phiString ->
//        protocolName = protocolName.minus(phiString)
//    }
//}
//set(ProtocolName,protocolName)

// future option use PerformedProtocolCodeSequence (0040,0260) code meanings to create new ProtocolName (0018,1030)
// def newProtocolName
// def protocols = get(PerformedProtocolCodeSequence) 
//protocols.each { protocol ->
//      // map procedure code to new value if desired
//      if (newProtocolName == null) {
//              newProtocolName = protocol.get(CodeMeaning)
//      }
//      else {
//              newProtocolName = newProtocolName + ', ' + protocol.get(CodeMeaning)
//      }
//      def protocolContext = get(ProtocolContextSequence) // (0040,0440)
//      protocolContext.each { context ->
//              newProtocolName = newProtocolName + ' ' + context.get(CodeMeaning)
//              def contextModifiers = get(ContentItemModifierSequence) // (0040,0441)
//              contextModifiers.each {modifier ->
//                      newProtocolName = newProtocolName + '(' + modifier.get(CodeMeaning) + ')'
//              }
//      }
// }
// set(ProtocolName,newProtocolName)

// ignore AcquisitionDeviceProcessingDescription (0018,1400)

remove(DetectorID) // (0018,700A)

// ignore AcquisitionProtocolDescription (0018,9424)

// this section not needed for current SOP Class list
// def newXRay3DAcquisitionSequence = get(XRay3DAcquisitionSequence) // update datetimes in XRay3DAcquisitionSequence (0018,9507)
// newXRay3DAcquisitionSequence.each { xRayAquisition ->
//      add studyOffset to xRayAquisition.StartAcquisitionDateTime (0018,9516)
//      add studyOffset to xRayAquisition.EndAcquisitionDateTime (0018,9517)    
// }

remove(ContributingEquipmentSequence) // (0018,A001)
// ** END OF GROUP 0018 **

// ** GROUP 0020 **

if (oldStudyInstanceUID != null){
    set(StudyInstanceUID, hasher.hashUID(oldStudyInstanceUID) + uidFlag) // (0020,000D)
}

def oldSeriesInstanceUID = get(SeriesInstanceUID)
if (oldSeriesInstanceUID != null) {
        set(SeriesInstanceUID, hasher.hashUID(oldSeriesInstanceUID)) // (0020,000E)
}

set(StudyID, newAccessionNumber) // (0020,0010)

def oldFrameOfReferenceUID = get(FrameOfReferenceUID)
if (oldFrameOfReferenceUID != null){
        set(FrameOfReferenceUID, hasher.hashUID(oldFrameOfReferenceUID)) // (0020,0052) 
}

def oldSynchronizationFrameOfReferenceUID = get(SynchronizationFrameOfReferenceUID)
if (oldSynchronizationFrameOfReferenceUID != null){
        set(SynchronizationFrameOfReferenceUID, hasher.hashUID(oldSynchronizationFrameOfReferenceUID)) // (0020,0200)
} else {
        remove(SynchronizationFrameOfReferenceUID)
}

remove(ImageComments) // (0020,4000)

def oldConcatenationUID = get(ConcatenationUID)
if (oldConcatenationUID != null){
        set(ConcatenationUID, hasher.hashUID(oldConcatenationUID)) // (0020,9161)
} else {
        remove(ConcatenationUID)
}

def newDimensionOrganizationSequence = get(DimensionOrganizationSequence) // update UIDs in DimensionOrganizationSequence (0020,9221)
if (newDimensionOrganizationSequence != []) {
        newDimensionOrganizationSequence.each { dimensionOrganization ->
                // create new UID for DimensionOrganization.DimensionOrganizationUID (0020,9164)        
                if (dimensionOrganization.get(DimensionOrganizationUID) != null){
                        dimensionOrganization.set(DimensionOrganizationUID, hasher.hashUID(dimensionOrganization.get(DimensionOrganizationUID)))
                }
        }
} else {
        remove(DimensionOrganizationSequence)
}

def newDimensionIndexSequence = get(DimensionIndexSequence) // update UIDs in DimensionIndexSequence (0020,9222)
if (newDimensionIndexSequence != []){
        newDimensionIndexSequence.each { dimensionIndex ->
                // create new UID for dimensionIndex.DimensionOrganizationUID (0020,9164)
                if (dimensionIndex.get(DimensionOrganizationUID) != null) {
                        dimensionIndex.set(DimensionOrganizationUID, hasher.hashUID(dimensionIndex.get(DimensionOrganizationUID)))      
                }
        }
} else {
        remove(DimensionIndexSequence)
}
// ** END OF GROUP 0020 **

// ** GROUP 0028 **
/* Temporarily removed
def burnedInAnnotation = get(BurnedInAnnotation)
if (burnedInAnnotation == 'YES') {
        log.debug("deidLive.groovy: SOP Instance {}:{} was not imported. Image contains burned in demographics.", oldStudyInstanceUID, oldSOPInstanceUID)
        return false
}
*/
set(LongitudinalTemporalInformationModified, 'MODIFIED') // (0028,0303)
//      ** END OF GROUP 0028 **

// ** GROUP 0032 **
remove(RequestedProcedureDescription) // (0032,1060)
remove(RequestingPhysician) // (0032,1032)
remove(RequestingService) // 0032,1033
// ** END OF GROUP 0032

// ** GROUP 0038 **
remove(AdmissionID) // (0038,0010)
remove(IssuerOfAdmissionID) // (0038,0011)
remove(IssuerOfAdmissionIDSequence) // (0038,0014)
remove(ServiceEpisodeID) // (0038,0060)
remove(IssuerOfServiceEpisodeID) // (0038,0061)
remove(ServiceEpisodeDescription) // (0038,0062)
remove(IssuerOfServiceEpisodeIDSequence) // (0038,0064)
// ** END OF GROUP 0038 **

// ** GROUP 0040 **
// add studyOffset to ScheduledProcedureStepStartDate (0040,0002)
def newScheduledProcedureStepStartDate = new Date()
def oldScheduledProcedureStepStartDate = get(ScheduledProcedureStepStartDate)
if (oldScheduledProcedureStepStartDate == null){
    newScheduledProcedureStepStartDate = newStudyDate
}
else {
    newScheduledProcedureStepStartDate = new Date().parse("yyyyMMdd",oldScheduledProcedureStepStartDate) 
    newScheduledProcedureStepStartDate.setDate(newScheduledProcedureStepStartDate.getDate() + studyOffset)
}
set(ScheduledProcedureStepStartDate,newScheduledProcedureStepStartDate.format('yyyyMMdd'))

// add studyOffset to ScheduledProcedureStepEndDate (0040,0003)
def newScheduledProcedureStepEndDate = new Date()
def oldScheduledProcedureStepEndDate = get(ScheduledProcedureStepEndDate)
if (oldScheduledProcedureStepEndDate == null){
    newScheduledProcedureStepEndDate = newStudyDate
}
else {
    newScheduledProcedureStepEndDate = new Date().parse("yyyyMMdd",oldScheduledProcedureStepEndDate) 
    newScheduledProcedureStepEndDate.setDate(newScheduledProcedureStepEndDate.getDate() + studyOffset)
}
set(ScheduledProcedureStepEndDate,newScheduledProcedureStepEndDate.format('yyyyMMdd'))

// add studyOffset to PerformedProcedureStepStartDate (0040,0244)
def newPerformedProcedureStepStartDate = new Date()
def oldPerformedProcedureStepStartDate = get(PerformedProcedureStepStartDate)
if (oldPerformedProcedureStepStartDate == null){
    newPerformedProcedureStepStartDate = newStudyDate
}
else {
    newPerformedProcedureStepStartDate = new Date().parse("yyyyMMdd",oldPerformedProcedureStepStartDate) 
    newPerformedProcedureStepStartDate.setDate(newPerformedProcedureStepStartDate.getDate() + studyOffset)
}
set(PerformedProcedureStepStartDate,newPerformedProcedureStepStartDate.format('yyyyMMdd'))

// ignore PerformedProcedureStepStartTime (0040,0245)

// add studyOffset to PerformedProcedureStepEndDate (0040,0250)
def newPerformedProcedureStepEndDate = new Date()
def oldPerformedProcedureStepEndDate = get(PerformedProcedureStepEndDate)
if (oldPerformedProcedureStepEndDate == null){
    newPerformedProcedureStepEndDate = newStudyDate
}
else {
    newPerformedProcedureStepEndDate = new Date().parse("yyyyMMdd",oldPerformedProcedureStepEndDate) 
    newPerformedProcedureStepEndDate.setDate(newPerformedProcedureStepEndDate.getDate() + studyOffset)
}
set(PerformedProcedureStepEndDate,newPerformedProcedureStepEndDate.format('yyyyMMdd'))

// ignore PerformedProcedureStepEndTime (0040,0251)
remove(PerformedProcedureStepID) // (0040,0253)
remove(PerformedProcedureStepDescription) // (0040,0254)

def newRequestAttributeSequence = get(RequestAttributesSequence) // update RequestAttributesSequence (0040,0275)
if (newRequestAttributeSequence != []) {
    newRequestAttributeSequence.each { request ->
        // fix 
        request.remove(AccessionNumber) // (0008,0050)
        request.remove(IssuerOfAccessionNumberSequence) // (0008,0051)
        request.remove(RequestedProcedureID) // (0040,1001)
        def oldRequestStudyInstanceUID = request.get(StudyInstanceUID)
        if (oldRequestStudyInstanceUID != null) {
            request.set(StudyInstanceUID, hasher.hashUID(oldStudyInstanceUID)) // (0020,000D)
        }
        newReferencedStudySequence = request.get(ReferencedStudySequence) // update UIDs in ReferencedStudySequence (0008,1110)
        newReferencedStudySequence.each { referencedSOPInstance ->
            // create new UID for referencedSOPInstance.ReferencedSOPInstanceUID (0008,1155)
            referencedSOPInstance.set(ReferencedSOPInstanceUID, hasher.hashUID(referencedSOPInstance.get(ReferencedSOPInstanceUID))) // (0008,0018)
        }
    }
    set(RequestAttributesSequence,newRequestAttributeSequence)
} else {
    remove(RequestAttributesSequence)
}

remove(CommentsOnThePerformedProcedureStep) // (0040,0280)

// Consider morphing SpecimenUID (0040,0554), though it is not specified in PS3.15

remove(AcquisitionContextSequence) // removes existing sequence (0040,0555)
get(AcquisitionContextSequence) // creates blank sequence

// Consider transforming ReasonForRequestedProcedureCodeSequence (0040,1012) values

remove(FillerOrderNumberImagingServiceRequest) // (0040,2017)
//remove(RetrieveURI) // (0040,E010). Added by GE PACS?
//remove(0x0040e010) // backup removal

// ** END OF GROUP 0040

// ** GROUP 0050 **
def newDeviceSequence = get(DeviceSequence) // remove serial number from DeviceSequence (0050,0010)
if (newDeviceSequence != []) {
        newDeviceSequence.each { device ->
                device.remove(DeviceSerialNumber) // (0018,1000)
                device.remove(DeviceID) // (0018,1003)
        }
        set(DeviceSequence, newDeviceSequence)
} else {
        remove(DeviceSequence)
}
// ** END OF GROUP 0050

// ** GROUP 0088 **
remove(IconImageSequence) // (0088,0200)
// END OF GROUP 0088

// ** GROUP 0400 **
remove(OriginalAttributesSequence) // (0400,0561)

// ** GROUP 60XX **
remove(OverlayRows) // (60xx,0010)
remove(OverlayColumns) // (60xx,0011)
remove(OverlayDescription) // (60xx,0022)
remove(OverlayType) // (60xx,0040)
remove(OverlaySubtype) // (60xx,0045)
remove(OverlayOrigin) // (60xx,0050)
remove(OverlayBitsAllocated) // (60xx,0100)
remove(OverlayBitPosition) // (60xx,0102)
remove(OverlayLabel) // (60xx,1500)
remove(OverlayData) // (60XX,3000)
// ** END OF GROUP 60XX **

// ** GROUP FFFA **
remove(DigitalSignaturesSequence) // (FFFA,FFFA)
// ** END OF GROUP FFFA

// ** PRIVATE GROUPS **
def privateGroups = []
//  Add new private groups to be kept in the form:
//  [PrivateCreator,
//      [
//          0xggggeeee,
//          0xggggeeee,
//          ...
//      ]
//  ] 
//  ** SIEMENS **
//  (0029,00xx) SIEMENS CSA HEADER
//    ['SIEMENS CSA HEADER',
//        [
//            0x00290008,
//            0x00290009
//        ]
//    ]
//]

// ** PRIVATE GROUP REMOVAL CODE **
def rawDicom = input.getDicomObject()
def privateTagWhitelist = []
//def oldPrivateAttributes = []
//def newPrivateAttributes = []
// def privateAttributeFile = new File('/home/rialto/rialto/var/log/removedPrivateAttributes.txt')
// privateAttributeFile.eachLine { removedAttribute ->
//    oldPrivateAttributes << removedAttribute
// }
// log.debug("deidLive.groovy: Initial removed attribute list: {}", oldPrivateAttributes)
if (rawDicom != null){
    // GENERATE PRIVATE ELEMENT WHITELIST
    privateGroups.each {group ->
        privateCreator = group[0]
        privateTags = group[1]
        privateTags.each {privateAttribute ->
            privateTag = rawDicom.resolveTag(privateAttribute,privateCreator)
            if (rawDicom.get(privateTag) != null){
                log.debug("deidLive.groovy: Whitelisting {}:0x{}", privateCreator, Integer.toHexString(privateAttribute))
                privateTagWhitelist << Integer.toHexString(privateTag)
            }
        }
    }
    log.debug("deidLive.groovy: Whitelist:{}", privateTagWhitelist)
    // REMOVE NON-WHITELIST PRIVATE ELEMENTS
    def privateGroupNumber = 0x0005
    while (privateGroupNumber <= 0xFFFF) {
        def groupStart = (privateGroupNumber * 0x10000) + 0x0001
        def groupEnd = (privateGroupNumber * 0x10000) + 0xFFFF
        privateGroup = rawDicom.iterator(groupStart, groupEnd)
        privateGroup.each{ privateElement ->
            log.debug("deidLive.groovy: Private element found: {}", privateElement)
            if ( (privateElement.tag() % 0x10000) > 0x1000) {   
                if (!privateTagWhitelist.contains(privateElement.tag())){
                    privateTag = privateElement.tag()
                    privateCreator = (privateTag.intdiv(0x10000) * 0x10000) + (privateTag % 0x10000).intdiv(0x100)
                    // newPrivateAttributes << get(privateCreator) + ':' + Integer.toHexString(privateTag)
                    remove(privateElement.tag())
                }
            }
        }
        privateGroupNumber += 0x0002
    }
}

log.info("deidLive.groovy: Completed de-identification")
