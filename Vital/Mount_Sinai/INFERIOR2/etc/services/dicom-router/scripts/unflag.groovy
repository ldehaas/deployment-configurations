// Mount Sinai Unflagging Script v03
//
// Note: this script is only run if the router receives data that has already been de-identified but had previously-identified issues.
// The assumption is that this data has been manually fixed and is suitable for general consumption

log.info("unflag.groovy v03: Now un-flagging Study:SOP Instance {}:{}.", get(StudyInstanceUID), get(SOPInstanceUID))

def newPatientID = get(PatientID)
def newAccessionNumber = get(AccessionNumber)
def newStudyInstanceUID = get(StudyInstanceUID)
def flag = ''
def uidFlag = ''

if ( (newPatientID == null) || (newAccessionNumber == null) || (newStudyInstanceUID == null) ) {
    log.warn("unflag.groovy: Data is not flagged. Aborting script.")
    return false
}

if (newPatientID.startsWith('AEFlag_')) {
    log.debug("unflag.groovy: Found unknown Station Name.")
    flag = 'AEFlag_'
    uidFlag = '.3'
} else if (newPatientID.startsWith('PHIFlag_')) {
    log.debug("unflag.groovy: Found burned in demographics.")
    flag = 'PHIFlag_'
    uidFlag = '.2'
} else if (newPatientID.startsWith('US_')) {
    log.debug("unflag.groovy: Found burned in demographics.")
    flag = 'US_'
    uidFlag = '.5'
} else {
    log.debug("unflag.groovy: Data is not flagged. Aborting script.")
    return false
}

newPatientID = newPatientID - flag
// newPatientID = 'TEST_' + newStudyInstanceUID
newAccessionNumber = newAccessionNumber - flag
// newAccessionNumber = 'TEST_' + newStudyInstanceUID
newStudyInstanceUID = newStudyInstanceUID.take(newStudyInstanceUID.length() - uidFlag.length())

set(PatientID, newPatientID)
set(PatientName,newPatientID)
set(AccessionNumber,newAccessionNumber)
set(StudyInstanceUID,newStudyInstanceUID)

log.info("unflag.groovy: Completed un-flagging Study:SOP Instance {}:{}.", get(StudyInstanceUID), get(SOPInstanceUID))
