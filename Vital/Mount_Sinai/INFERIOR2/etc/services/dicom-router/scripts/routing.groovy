//      Mount Sinai Routing Script v.6.3-36
//      Routing script logic:
//      Data with Mount Sinai patient domain:
//              - Ultrasound:
//                      - Sent from PACS
//                              - Send to CTP Tool
//              - Try MRN lookup for Patient ID
//                  - If database down, enter retry
//                  - If no DB entry, save metadata (optional) and delete
//              - De-identifier / flagging script
//              - Send to ROUTER
//              - Send to VIOARCHIVE1 to save metadata
//      Data with Research Warehouse patient domain
//              - Unflagged 
//                      - Send to VioArchive main storage
//              - Flagged 
//                      - Sent from ROUTER
//                              - Send to VioArchive flagged storage
//                      - Sent from other source with 'Burned In Annotation' set to 'NO'
//                              - Unflag
//                              - Send to VioArchive main storage
import groovy.sql.Sql
import java.sql.SQLException

log.info("routing.groovy v6.3-36: Begin routing script.")

def sinaiDomain = '2.16.840.1.113883.3.515'
def researchDomain = '1.3.6.1.4.1.40920'
def routerAE = 'ROUTER'
def archiveAE = 'VIOARCHIVE'
def flaggedAE = 'VIOARCHIVE1'
def metadataAE = 'METADATA'
def ultrasoundAE = 'USANONPIXEL'
def flag = null
def flagList = ['PHIFlag_', 'AEFlag_', 'US_']
def saveMetadata = false
def callingAE = getCallingAETitle()
log.debug("routing.groovy: Calling AE Title is {}", callingAE)

def oldPatientID = input.get(PatientID)
def oldAccessionNumber = input.get(AccessionNumber)
def burnedInPHI = input.get(BurnedInAnnotation)
def patientDomain = input.get("IssuerOfPatientIDQualifiersSequence/UniversalEntityID")
def studyUID = input.get(StudyInstanceUID)
log.debug("routing.groovy: Patient Domain is {}", patientDomain)


flagList.each { flagItem ->
    if (oldPatientID.startsWith(flagItem)) {
        log.debug("routing.groovy: Flag {} found in study.", flagItem)
        flag = flagItem
    }
}

if (patientDomain == researchDomain) { // Study has been de-identified
    log.debug("routing.groovy: Research Warehouse domain found.")
    if (flag == null) { // Study is unflagged

        log.info("routing.groovy: Sending to VioArchive main storage.")
		return 'etc/dataflows/main.xml'
	}
	if (callingAE == routerAE) { // Study is flagged but is being sent from router
		log.info("routing.groovy: Sending to VioArchive flagged storage.")
		return 'etc/dataflows/flagged.xml'
	}
	if (burnedInPHI != 'YES') { // Study has been corrected and is being sent back into system
		log.info("routing.groovy: Unflagging corrected data and sending to VioArchive main storage.")
		return 'etc/dataflows/unflag.xml'
	}
	log.warn("routing.groovy: Flagged data has not been corrected. Aborting script.")
	return 'etc/dataflows/null.xml'
} 

if ( (get(Modality) == 'US') && (callingAE != ultrasoundAE) ) { 
    // Non-anonymized US study from PACS sent to pixel data tool

    log.info("routing.groovy: Sending to ultrasound pixel data tool.")
    return 'etc/dataflows/ultrasound.xml'

}

// Study has not been de-identified and if it is ultrasound it has had burned-in PHI removed 
// Lookup patient
def mappingDBURL = 'jdbc:oracle:thin:@sdwhetlprod01.mountsinai.org:1521:ETLPROD'
def mappingDBUser = 'vioarchive_lookup'
def mappingDBPass = 'msdw'
Class.forName('oracle.jdbc.driver.OracleDriver')

def newPatientID
def newAccessionNumber
def studyOffset 

log.debug("routing.groovy: Initiating connection to {}", mappingDBURL)
try {
    sql = Sql.newInstance(mappingDBURL, mappingDBUser, mappingDBPass)
} catch (SQLException sqlConnectError) {
    // Database unavailable. Put data in a queue until database is alive again.
    log.warn("routing.groovy: Failed to connect to {}: \n{}. Putting in queue.", mappingDBURL, sqlConnectError)
    log.debug("routing.groovy: Closing database connection")
    sql.close()
    log.debug("routing.groovy: Database connection closed")
        
    log.info("routing.groovy: Could not connect to database. Will re-try later.")
    return 'etc/dataflows/retry.xml'         
}

// Database is alive
log.debug("routing.groovy: Getting New Patient ID")
try {
    sql.call '{? = call get_deid(?,?,?)}', [Sql.LONGVARCHAR, oldPatientID, oldAccessionNumber,'MRN'], {row ->
        log.debug("routing.groovy: Returned {}", row)
        newPatientID = row
        log.info("routing.groovy: New Patient ID: {}",newPatientID)
    }
} catch (e) {
    // Patient not in MRN database. Optionally save metadata then throw away.
    log.warn("routing.groovy: Unable to map Patient ID for Study {}. Aborting.\n{}", studyUID, e.getMessage())
    log.debug("routing.groovy: Closing database connection")
    sql.close()
    log.debug("routing.groovy: Database connection closed")
    
    return 'etc/dataflows/null.xml'
}

// get new AccessionNumber
log.debug("routing.groovy: Getting New Accession Number")
try {
    sql.call '{? = call get_deid(?,?,?)}', [Sql.LONGVARCHAR, oldPatientID, oldAccessionNumber,'accession_no'], {row ->
        log.debug("routing.groovy: Returned {}", row)
        newAccessionNumber = row
    }
    log.debug("routing.groovy: New Accession Number: {}",newAccessionNumber)
} catch (e) {
    // Study not in MRN database. Optionally save metadata then throw away.
    log.warn("routing.groovy: Failed Accession Number lookup for Study {}: {}", studyUID, e.getMessage())
    log.debug("routing.groovy: Closing database connection")
    sql.close()
    log.debug("routing.groovy: Database connection closed")

    return 'etc/dataflows/null.xml'
}

// get studyOffset
log.debug("routing.groovy: Getting Study Offset")
try {
    sql.call '{? = call get_deid(?,?,?)}', [Sql.LONGVARCHAR, oldPatientID, oldAccessionNumber,'dateshift'], {row ->
        log.debug("routing.groovy: Returned {}", row)
            studyOffset = row
    }
    log.debug("routing.groovy: Study Offset: {}",studyOffset)
    studyOffset = studyOffset.toInteger()
} catch (e) {
    // Patient not in MRN database. Optionally save metadata then throw away.
    log.warn("routing.groovy: Failed Study Offset lookup for Study {}: {}", studyUID, e.getMessage())
    log.debug("routing.groovy: Closing database connection")
    sql.close()
    log.debug("routing.groovy: Database connection closed")

    return 'etc/dataflows/null.xml'
} 

// Patient / Study is in MRN database
log.debug("routing.groovy: Closing database connection")
sql.close()
log.debug("routing.groovy: Database connection closed")

// Morph study
File mappingFile = new File('/home/rialto/rialto/var/tmp/' + get(StudyInstanceUID) + '.qry')

if(!mappingFile.exists()) {
    // Mapping file will never exist at this stage except in error cases
    mappingFile.deleteOnExit()

    log.debug("routingTest.groovy: New values: {}/{}/{}", newPatientID, newAccessionNumber, studyOffset)
    log.debug("routingTest.groovy: Temp file: {}", mappingFile.getAbsolutePath())

    mappingFile << newPatientID + '\n'
    mappingFile << newAccessionNumber + '\n'
    mappingFile << studyOffset + '\n'
}

log.info("routing.groovy: Sending to de-identifier.")
return 'etc/dataflows/deid.xml'

log.warn("routing.groovy: Ending routing script. This message should not appear")
