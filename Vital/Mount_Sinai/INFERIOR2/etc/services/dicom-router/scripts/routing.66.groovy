//      Mount Sinai Routing Script v.36
//      Routing script logic:
//      Data with Mount Sinai patient domain:
//              - Ultrasound:
//                      - Sent from PACS
//                              - Send to CTP Tool
//              - Try MRN lookup for Patient ID
//                  - If database down, enter dbTest
//                  - If no DB entry, save metadata (optional) and delete
//              - De-identifier / flagging script
//              - Send to ROUTER
//              - Send to VIOARCHIVE1 to save metadata
//      Data with Research Warehouse patient domain
//              - Unflagged 
//                      - Send to VioArchive main storage
//              - Flagged 
//                      - Sent from ROUTER
//                              - Send to VioArchive flagged storage
//                      - Sent from other source with 'Burned In Annotation' set to 'NO'
//                              - Unflag
//                              - Send to VioArchive main storage
log.info("routing.groovy v36: Begin routing script.")

import org.dcm4che2.data.DicomObject
import org.dcm4che2.data.Tag.*

import com.karos.rialto.workflow.model.GenericWorkflow
import com.karos.rialto.workflow.common.tasks.*
import com.karos.rialto.storage.StorageRepository
import com.karos.rialto.storage.file.FileSystemStorageRepository

import com.karos.rialto.workflow.model.Task.Status
import com.karos.rialto.workflow.model.Task.TaskPriority

import java.time.LocalDateTime

import groovy.sql.Sql
import java.sql.SQLException

def currentDate = LocalDateTime.now()

def fsRepo = new FileSystemStorageRepository('Router','/home/rialto/var/ids1/router')
String[] sopPath = [
    getCallingAETitle(),
    getCalledAETitle(),
    currentDate.getYear(),
    currentDate.getMonthValue(),
    currentDate.getDayOfMonth(),
    currentDate.getHour(),
    currentDate.getMinute(),
    currentDate.getSecond(),
    input.get(StudyInstanceUID)
]

def sinaiDomain = '2.16.840.1.113883.3.515'
def researchDomain = '1.3.6.1.4.1.40920'
def routerAE = 'ROUTER'
def archiveAE = 'VIOARCHIVE'
def flaggedAE = 'VIOARCHIVE1'
def metadataAE = 'METADATA'
def ultrasoundAE = 'USANONPIXEL'
def flag = null
def flagList = ['PHIFlag_', 'AEFlag_', 'US_']
def saveMetadata = false // should be enabled once software update applied

// Receive images
def receiveImageTaskBuilder = new ReceiveImageTask.Builder()
receiveImageTaskBuilder.setStorageRepository(fsRepo)                 
receiveImageTaskBuilder.setStorageTags(sopPath)

def oldPatientID = input.get(PatientID)
def oldAccessionNumber = input.get(AccessionNumber)
def burnedInPHI = input.get(BurnedInAnnotation)

def patientDomain = input.get("IssuerOfPatientIDQualifiersSequence/UniversalEntityID")
log.debug("routing.groovy: Patient Domain is {}", patientDomain)

def callingAE = sopPath[0]

log.debug("routing.groovy: Calling AE Title is {}", callingAE)

flagList.each { flagItem ->
    if (oldPatientID.startsWith(flagItem)) {
        log.debug("routing.groovy: Flag {} found in study.", flagItem)
        flag = flagItem
    }
}

if (saveMetadata) { 
    def qualifyStudyTaskBuilder = new MorphStudyTask.Builder()
    qualifyStudyTaskBuilder.setMorpherScriptName("/home/rialto/rialto/etc/services/dicom-router/scripts/in.groovy")
    qualifyStudyTaskBuilder.addDataDependency(receiveImageTaskBuilder,Status.SUCCESS)

    def sendMetaDataTaskBuilder = new SendStudyTask.Builder()
    sendMetaDataTaskBuilder.addDataDependency(qualifyStudyTaskBuilder, Status.SUCCESS)
    sendMetaDataTaskBuilder.setDestination(metadataAE)
}

def testDBTaskBuilder = new MorphStudyTask.Builder()
def morphStudyTaskBuilder = new MorphStudyTask.Builder()
def sendStudyTaskBuilder = new SendStudyTask.Builder()
sendStudyTaskBuilder.setMaxRetries(5)
sendStudyTaskBuilder.setCallingAe(callingAE)

def cleanStorageLocationTaskBuilder = new CleanStorageLocationTask.Builder()

if (patientDomain == researchDomain) { // Study has been de-identified
    log.debug("routing.groovy: Research Warehouse domain found.")
    if (flag == null) { // Study is unflagged

        log.info("routing.groovy: Sending to VioArchive main storage.")
        
        // Send study
        sendStudyTaskBuilder.setDestination(archiveAE)
        sendStudyTaskBuilder.addDataDependency(receiveImageTaskBuilder, Status.SUCCESS)
        cleanStorageLocationTaskBuilder.addDataDependency(sendStudyTaskBuilder, Status.SUCCESS)

        // Run workflow
        def raStore = new GenericWorkflow.Builder("Send To Archive")
        raStore.addTaskBuilder(receiveImageTaskBuilder)
        raStore.addTaskBuilder(sendStudyTaskBuilder)
        raStore.addTaskBuilder(cleanStorageLocationTaskBuilder)
        return raStore.build()

    } 
    if (callingAE == routerAE) { // Study is flagged but is being sent from router

        log.info("routing.groovy: Sending to VioArchive flagged storage.")
        // Send study
        sendStudyTaskBuilder.setDestination(flaggedAE)
        sendStudyTaskBuilder.addDataDependency(receiveImageTaskBuilder, Status.SUCCESS)
        cleanStorageLocationTaskBuilder.addDataDependency(sendStudyTaskBuilder, Status.SUCCESS)

        // Run workflow
        def flagStore = new GenericWorkflow.Builder("Send To Flagged Storage")
        flagStore.addTaskBuilder(receiveImageTaskBuilder)
        flagStore.addTaskBuilder(sendStudyTaskBuilder)
        flagStore.addTaskBuilder(cleanStorageLocationTaskBuilder)
        return flagStore.build()

    } 
    if (burnedInPHI != 'YES') { // Study has been corrected and is being sent back into system

        log.info("routing.groovy: Unflagging corrected data and sending to VioArchive main storage.")

        // Unflag study
        morphStudyTaskBuilder.setMorpherScriptName("/home/rialto/rialto/etc/services/dicom-router/scripts/unflag.groovy")
        morphStudyTaskBuilder.addDataDependency(receiveImageTaskBuilder,Status.SUCCESS)

        // Send study
        sendStudyTaskBuilder.addDataDependency(morphStudyTaskBuilder, Status.SUCCESS)
        sendStudyTaskBuilder.setDestination(archiveAE)

        // Clean storage
        cleanStorageLocationTaskBuilder.addDataDependency(sendStudyTaskBuilder, Status.SUCCESS)

        // Run workflow
        def unflagStore = new GenericWorkflow.Builder("Unflag And Send To Archive")
        unflagStore.addTaskBuilder(receiveImageTaskBuilder)
        unflagStore.addTaskBuilder(morphStudyTaskBuilder)
        unflagStore.addTaskBuilder(sendStudyTaskBuilder)
        unflagStore.addTaskBuilder(cleanStorageLocationTaskBuilder)
        return unflagStore.build()

    } 

    // Study has not been corrected
    log.warn("routing.groovy: Flagged data has not been corrected. Data not being routed.")
    cleanStorageLocationTaskBuilder.addDataDependency(receiveImageTaskBuilder, Status.SUCCESS)
    def routingFail = new GenericWorkflow.Builder("Uncorrected flagged data")
    routingFail.addTaskBuilder(receiveImageTaskBuilder)
    routingFail.addTaskBuilder(cleanStorageLocationTaskBuilder)
    return routingFail.build()

}
if ( (get(Modality) == 'US') && (callingAE != ultrasoundAE) ) { 
    // Non-anonymized US study from PACS sent to pixel data tool

    log.info("routing.groovy: Sending to ultrasound pixel data tool.")

    // Send study
    sendStudyTaskBuilder.setDestination(ultrasoundAE)
    sendStudyTaskBuilder.addDataDependency(receiveImageTaskBuilder, Status.SUCCESS)
    cleanStorageLocationTaskBuilder.addDataDependency(sendStudyTaskBuilder, Status.SUCCESS)

    // Run workflow
    def ultrasoundWF = new GenericWorkflow.Builder("Sending ultrasound data to pixel de-identifier")
    ultrasoundWF.addTaskBuilder(receiveImageTaskBuilder)
    ultrasoundWF.addTaskBuilder(sendStudyTaskBuilder)
    ultrasoundWF.addTaskBuilder(cleanStorageLocationTaskBuilder)
    return ultrasoundWF.build()
}

// Study has not been de-identified and if it is ultrasound it has had burned-in PHI removed 
// Lookup patient
def mappingDBURL = 'jdbc:oracle:thin:@sdwhetlprod01.mountsinai.org:1521:ETLPROD'
def mappingDBUser = 'vioarchive_lookup'
def mappingDBPass = 'msdw'
Class.forName('oracle.jdbc.driver.OracleDriver')

def newPatientID
def newAccessionNumber
def studyOffset    

log.debug("routing.groovy: Initiating connection to {}", mappingDBURL)
try {
    sql = Sql.newInstance(mappingDBURL, mappingDBUser, mappingDBPass)
} catch (SQLException sqlConnectError) {
    // Database unavailable. Put data in a queue until database is alive again.
    log.warn("routing.groovy: Failed to connect to {}: \n{}. Putting in queue.", mappingDBURL, sqlConnectError)
    log.debug("routing.groovy: Closing database connection")
    sql.close()
    log.debug("routing.groovy: Database connection closed")
        
    // Morph study
    log.info("routing.groovy: Could not connect to database. Will re-try later.")
    testDBTaskBuilder.setMorpherScriptName("/home/rialto/rialto/etc/services/dicom-router/scripts/dbTest.groovy")
    testDBTaskBuilder.addDataDependency(receiveImageTaskBuilder,Status.SUCCESS)
    testDBTaskBuilder.setPriority(90)
    testDBTaskBuilder.setMaxRetries(5)

    // Send study back to router
    sendStudyTaskBuilder.addDataDependency(testDBTaskBuilder, Status.SUCCESS)
    sendStudyTaskBuilder.setCallingAe(routerAE)
    sendStudyTaskBuilder.setDestination(routerAE)

    // Clean storage
    cleanStorageLocationTaskBuilder.addDataDependency(sendStudyTaskBuilder, Status.SUCCESS)

    // Run workflow
    def deidTry = new GenericWorkflow.Builder("Waiting for database connection.")
    deidTry.addTaskBuilder(receiveImageTaskBuilder)
    deidTry.addTaskBuilder(testDBTaskBuilder)
    deidTry.addTaskBuilder(sendStudyTaskBuilder)
    deidTry.addTaskBuilder(cleanStorageLocationTaskBuilder)
    return deidTry.build()          
}
// Database is alive
log.debug("routing.groovy: Getting New Patient ID")
try {
    sql.call '{? = call get_deid(?,?,?)}', [Sql.LONGVARCHAR, oldPatientID, oldAccessionNumber,'MRN'], {row ->
        log.debug("routing.groovy: Returned {}", row)
        newPatientID = row
        log.info("routing.groovy: New Patient ID: {}",newPatientID)
    }
} catch (e) {
    // Patient not in MRN database. Optionally save metadata then throw away.
    log.warn("routing.groovy: Unable to map Patient ID for Study {}. Aborting.\n{}", sopPath[8], e.getMessage())
    log.debug("routing.groovy: Closing database connection")
    sql.close()
    log.debug("routing.groovy: Database connection closed")
    
    cleanStorageLocationTaskBuilder.addDataDependency(receiveImageTaskBuilder, Status.SUCCESS)
    if (saveMetadata) {
        cleanStorageLocationTaskBuilder.addDataDependency(sendMetaDataTaskBuilder, Status.SUCCESS)
    }
    // Run workflow
    def mrnFail = new GenericWorkflow.Builder("De-identifer Failed MRN DB Lookup")
    mrnFail.addTaskBuilder(receiveImageTaskBuilder)
    if (saveMetadata) {
        mrnFail.addTaskBuilder(qualifyStudyTaskBuilder)
        mrnFail.addTaskBuilder(sendMetaDataTaskBuilder)
    }
    mrnFail.addTaskBuilder(cleanStorageLocationTaskBuilder)
    return mrnFail.build()
}

// get new AccessionNumber
log.debug("routing.groovy: Getting New Accession Number")
try {
    sql.call '{? = call get_deid(?,?,?)}', [Sql.LONGVARCHAR, oldPatientID, oldAccessionNumber,'accession_no'], {row ->
        log.debug("routing.groovy: Returned {}", row)
        newAccessionNumber = row
    }
    log.debug("routing.groovy: New Accession Number: {}",newAccessionNumber)
} catch (e) {
    // Study not in MRN database. Optionally save metadata then throw away.
    log.warn("routing.groovy: Failed Accession Number lookup for Study {}: {}", sopPath[8], e.getMessage())
    log.debug("routing.groovy: Closing database connection")
    sql.close()
    log.debug("routing.groovy: Database connection closed")

    cleanStorageLocationTaskBuilder.addDataDependency(receiveImageTaskBuilder, Status.SUCCESS)
    if (saveMetadata) {
        cleanStorageLocationTaskBuilder.addDataDependency(sendMetaDataTaskBuilder, Status.SUCCESS)
    }
    // Run workflow
    def mrnFail2 = new GenericWorkflow.Builder("De-identifer Failed Accession Number Lookup")
    mrnFail2.addTaskBuilder(receiveImageTaskBuilder)
    if (saveMetadata) {
        mrnFail2.addTaskBuilder(qualifyStudyTaskBuilder)
        mrnFail2.addTaskBuilder(sendMetaDataTaskBuilder)
    }
    mrnFail2.addTaskBuilder(cleanStorageLocationTaskBuilder)
    return mrnFail2.build()

}

// get studyOffset
log.debug("routing.groovy: Getting Study Offset")
try {
    sql.call '{? = call get_deid(?,?,?)}', [Sql.LONGVARCHAR, oldPatientID, oldAccessionNumber,'dateshift'], {row ->
        log.debug("routing.groovy: Returned {}", row)
            studyOffset = row
    }
    log.debug("routing.groovy: Study Offset: {}",studyOffset)
    studyOffset = studyOffset.toInteger()
} catch (e) {
    // Patient not in MRN database. Optionally save metadata then throw away.
    log.warn("routing.groovy: Failed Study Offset lookup for Study {}: {}", sopPath[8], e.getMessage())
    log.debug("routing.groovy: Closing database connection")
    sql.close()
    log.debug("routing.groovy: Database connection closed")
    cleanStorageLocationTaskBuilder.addDataDependency(receiveImageTaskBuilder, Status.SUCCESS)
    if (saveMetadata) {
        cleanStorageLocationTaskBuilder.addDataDependency(sendMetaDataTaskBuilder, Status.SUCCESS)
    }
    // Run workflow
    def mrnFail3 = new GenericWorkflow.Builder("De-identifer Failed Study Offset Lookup")
    mrnFail3.addTaskBuilder(receiveImageTaskBuilder)
    if (saveMetadata) {
        mrnFail3.addTaskBuilder(qualifyStudyTaskBuilder)
        mrnFail3.addTaskBuilder(sendMetaDataTaskBuilder)
    }
    mrnFail3.addTaskBuilder(cleanStorageLocationTaskBuilder)
    return mrnFail3.build()
}    

// Patient / Study is in MRN database
log.debug("routing.groovy: Closing database connection")
sql.close()
log.debug("routing.groovy: Database connection closed")

// Morph study
File mappingFile = new File('/home/rialto/rialto/var/tmp/' + get(StudyInstanceUID) + '.qry')

if(!mappingFile.exists()) {
    // Mapping file will never exist at this stage except in error cases
    mappingFile.deleteOnExit()

    log.debug("routingTest.groovy: New values: {}/{}/{}", newPatientID, newAccessionNumber, studyOffset)
    log.debug("routingTest.groovy: Temp file: {}", mappingFile.getAbsolutePath())

    mappingFile << newPatientID + '\n'
    mappingFile << newAccessionNumber + '\n'
    mappingFile << studyOffset + '\n'
}

log.info("routing.groovy: Sending to de-identifier.")

morphStudyTaskBuilder.setMorpherScriptName("/home/rialto/rialto/etc/services/dicom-router/scripts/deidLive.groovy")
morphStudyTaskBuilder.addDataDependency(receiveImageTaskBuilder,Status.SUCCESS)

// Send study
sendStudyTaskBuilder.addDataDependency(morphStudyTaskBuilder, Status.SUCCESS)
sendStudyTaskBuilder.setCallingAe(routerAE)
sendStudyTaskBuilder.setDestination(routerAE)

// Clean storage
if (saveMetadata) {
    cleanStorageLocationTaskBuilder.addDataDependency(sendMetaDataTaskBuilder, Status.SUCCESS)
}
cleanStorageLocationTaskBuilder.addDataDependency(sendStudyTaskBuilder, Status.SUCCESS)

// Run workflow
def deidwf = new GenericWorkflow.Builder("De-identify " + oldPatientID + " to " + newPatientID)
deidwf.addTaskBuilder(receiveImageTaskBuilder)
if (saveMetadata) {
    deidwf.addTaskBuilder(qualifyStudyTaskBuilder)
    deidwf.addTaskBuilder(sendMetaDataTaskBuilder)
}
deidwf.addTaskBuilder(morphStudyTaskBuilder)
deidwf.addTaskBuilder(sendStudyTaskBuilder)
deidwf.addTaskBuilder(cleanStorageLocationTaskBuilder)
return deidwf.build() 

log.info("routing.groovy: Ending routing script. This message should not appear")
