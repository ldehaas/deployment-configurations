// log.debug("dbTest.groovy v05")

import groovy.sql.Sql
import java.sql.SQLException

if (get(InstanceNumber) != '1') {
        // Should only test DB ` once per study
        return true
}

def mappingDBURL = 'jdbc:oracle:thin:@sdwhetlprod01.mountsinai.org:1521:ETLPROD'
def mappingDBUser = 'vioarchive_lookup'
def mappingDBPass = 'msdw'
Class.forName('oracle.jdbc.driver.OracleDriver')

log.debug("dbTest.groovy v05: Initiating connection to {}", mappingDBURL)

sql = Sql.newInstance(mappingDBURL, mappingDBUser, mappingDBPass)
// If the database connection is unavailable this will throw an exception and nothing below is run

log.debug("dbTest.groovy: Closing database connection")
sql.close()
log.debug("dbTest.groovy: Database connection closed")
