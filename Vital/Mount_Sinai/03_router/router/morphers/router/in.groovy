log.debug("in.groovy: Start inbound morpher")

def procedureCode = get("RequestedProcedureCodeSequence/CodeValue")
log.debug("Found Requested Procedure Code: {}.", procedureCode)
if (procedureCode == null) {
     log.debug("No Procedure Code -- will continue.")
//    log.debug("No Procedure Code.")
//    return false
} 
//else if ((procedureCode.substring(0,2) == 'OP') || (procedureCode.substring(0,2) == 'PU')) {
//    log.debug("Procedure Code not supported")
//    return false
//}

def issuerOfPatientId = get(IssuerOfPatientID)
def universalEntityID = get("IssuerOfPatientIDQualifiersSequence/UniversalEntityID")
def universalEntityIDType = get("IssuerOfPatientIDQualifiersSequence/UniversalEntityIDType")
log.debug("Existing domain: {}&{}&{}", issuerOfPatientId, universalEntityID, universalEntityIDType)

if (issuerOfPatientId == null) {
    issuerOfPatientId = 'Mount Sinai'
    set(IssuerOfPatientID, issuerOfPatientId)
}

log.debug("in.groovy: IssuerOfPatientId is {} ", issuerOfPatientId)

def (namespace, domain, type) = issuerOfPatientId.tokenize("&")
log.debug("in.groovy: namespace = {}, domain = {}, type = {}", namespace, domain, type)

if (namespace != "") {
    set(IssuerOfPatientID, namespace)
}

if (universalEntityID == null && domain == null) {
   set("IssuerOfPatientIDQualifiersSequence/UniversalEntityID", '2.16.840.1.113883.3.515')
} else if (universalEntityID == null && domain != null){
    set("IssuerOfPatientIDQualifiersSequence/UniversalEntityID", domain)
}

if (universalEntityIDType == null && type == null) {
    set("IssuerOfPatientIDQualifiersSequence/UniversalEntityIDType", 'ISO')
} else if (universalEntityIDType == null && type != null) {
    set("IssuerOfPatientIDQualifiersSequence/UniversalEntityIDType", type)
}
log.debug("New domain: {}&{}&{}", get(IssuerOfPatientID), get("IssuerOfPatientIDQualifiersSequence/UniversalEntityID"), get("IssuerOfPatientIDQualifiersSequence/UniversalEntityIDType"))
log.debug("in.groovy: End inbound morpher")
