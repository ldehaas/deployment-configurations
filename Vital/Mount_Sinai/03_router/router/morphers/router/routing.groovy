// 	Mount Sinai Routing Script v.04
// 	Routing script logic:
// 	Data with Mount Sinai patient domain:
//		- De-identifier / flagging script
//		- Send to ROUTER
//	Data with Research Warehouse patient domain
//		- Unflagged 
//			- Send to VioArchive main storage
//		- Flagged 
//			- Sent from ROUTER
//				- Send to VioArchive flagged storage
//			- Sent from other source with 'Burned In Annotation' set to 'NO'
//				- Unflag
//				- Send to VioArchive main storage
log.info("routing.groovy: Begin routing script.")

def patientDomain = get("IssuerOfPatientIDQualifiersSequence/UniversalEntityID")
def sinaiDomain = '2.16.840.1.113883.3.515'
def researchDomain = '1.3.6.1.4.1.40920'
def routerAE = 'ROUTER'
log.debug("routing.groovy: Patient Domain is {}", patientDomain)

def callingAE = getCallingAETitle()
log.debug("routing.groovy: Calling AE Title is {}", callingAE)

def patientID = get(PatientID)
def burnedInPHI = get(BurnedInAnnotation)
def flag = null

if (patientID.startsWith('PHIFlag_')) {
    log.debug("routing.groovy: Flagged due to burned in PHI.")
    flag = 'PHI'
} else if (patientID.startsWith('AEFlag_')) {
    log.debug("routing.groovy: Flagged due to unknown AE.")
    flag = 'AE'
} else {
	log.debug("routing.groovy: Study not flagged.")
}

if (patientDomain == researchDomain) {
	log.debug("routing.groovy: Research Warehouse domain found.")
	if (flag == null) {
		log.info("routing.groovy: Sending to VioArchive main storage.")
		return 'etc/dataflows/main.xml'
	}
	if (callingAE == routerAE) {
		log.info("routing.groovy: Sending to VioArchive flagged storage.")
		return 'etc/dataflows/flagged.xml'
	}
	if (burnedInPHI != 'YES') {
		log.info("routing.groovy: Unflagging corrected data and sending to VioArchive main storage.")
		return 'etc/dataflows/unflag.xml'
	}
	log.warn("routing.groovy: Flagged data has not been corrected. Aborting script.")
	return 'etc/dataflows/null.xml'
} else if (patientDomain == sinaiDomain){
	log.debug("routing.groovy: Mount Sinai domain found.")
	log.info("routing.groovy: Sending to de-identifier.")
	return 'etc/dataflows/deidMeta.xml'
}

log.warn("routing.groovy: Unexpected domain found. Aborting script.")
return 'etc/dataflows/null.xml'

log.warn("routing.groovy: Ending routing script. This message should not appear")
