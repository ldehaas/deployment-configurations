//      Mount Sinai Routing Script v.04
//      Routing script logic:
//      Data with Mount Sinai patient domain:
//              - Sent from CACHE
//                      - De-identifier / flagging script
//                      - Send to ROUTER
//              - Not sent from cache
//                      - send to cache
//      Data with Research Warehouse patient domain
//              - Unflagged 
//                      - Send to VioArchive main storage
//              - Flagged 
//                      - Sent from ROUTER
//                              - Send to VioArchive flagged storage
//                      - Sent from other source with 'Burned In Annotation' set to 'NO'
//                              - Unflag
//                              - Send to VioArchive main storage
def studyInstanceUID = get(StudyInstanceUID)
log.info("routing.groovy: Begin routing study {}.", studyInstanceUID)

def patientDomain = get("IssuerOfPatientIDQualifiersSequence/UniversalEntityID")
def sinaiDomain = '2.16.840.1.113883.3.515'
def researchDomain = '1.3.6.1.4.1.40920'
def routerAE = 'ROUTER'
def cacheAE = 'ROUTERCACHE'
log.debug("routing.groovy: Patient Domain for Study {} is {}", studyInstanceUID, patientDomain)

def callingAE = getCallingAETitle()
log.debug("routing.groovy: Calling AE Title for Study {} is {}", studyInstanceUID, callingAE)

def patientID = get(PatientID)
def burnedInPHI = get(BurnedInAnnotation)
def flag = null

if (patientID.startsWith('SOPError_')) {
    log.debug("routing.groovy: Study {} flagged due to unsupported SOP Class.", studyInstanceUID)
    flag = 'SOP'
} else if (patientID.startsWith('PHIError_')) {
    log.debug("routing.groovy: Study {} flagged due to burned in PHI.", studyInstanceUID)
    flag = 'PHI'
} else {
        log.debug("routing.groovy: Study {} not flagged.", studyInstanceUID)
}

if (patientDomain == researchDomain) {
        log.debug("routing.groovy: Research Warehouse domain found for Study {}.", studyInstanceUID)
        if (flag == null) {
                log.info("routing.groovy: Sending Study {} to VioArchive main storage.", studyInstanceUID)
                return 'etc/dataflows/main.xml'
        }
        if (callingAE == routerAE) {
                log.info("routing.groovy: Sending Study {} to VioArchive flagged storage.", studyInstanceUID)
                return 'etc/dataflows/flagged.xml'
        }
        if (burnedInPHI != 'YES') {
                log.info("routing.groovy: Unflagging corrected Study {} and sending to VioArchive main storage.", studyInstanceUID)
                return 'etc/dataflows/unflag.xml'
        }
        log.warn("routing.groovy: Flagged Study {} has not been corrected. Aborting script.", studyInstanceUID)
        return 'etc/dataflows/null.xml'
} else if (patientDomain == sinaiDomain){
        log.debug("routing.groovy: Mount Sinai domain found for Study {}.", studyInstanceUID)
        if (callingAE != cacheAE) {
                log.info("routing.groovy: Sending Study {} to Router Cache for two days.", studyInstanceUID)
                return 'etc/dataflows/cache.xml'
        }
        log.info("routing.groovy: Sending Study {} to de-identifier.", studyInstanceUID)
        return 'etc/dataflows/deid.xml'
}

log.warn("routing.groovy: Unexpected domain found in Study {}. Aborting script.", studyInstanceUID)
return 'etc/dataflows/null.xml'

log.warn("routing.groovy: Ending routing script. This message should not appear")
