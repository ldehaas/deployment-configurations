// metaExport2.groovy
// v01
// For non-deidentified metadata
// Gets MINT metadata, sends it to MRN database, forwards the study to ROUTER and then deletes it
import groovy.sql.Sql
import java.sql.SQLException
import java.net.URL
import org.joda.time.*

def studyInstanceUID = study.getStudyInstanceUID()
def patientID = study.get(PatientID)
def accessionNumber = study.get(AccessionNumber)
def studyDateTime = study.getStudyDateTime()

def baseMINTURL='http://127.0.0.1:8080/vault/mint/studies/'
def vnaBaseMINTURL='http://10.95.15.52:8080/vault/mint/studies/'
def mappingDBURL = 'jdbc:oracle:thin:@sdwhodbqa01.mountsinai.org:1521:MSDWUSERS'
def mappingDBUser = 'vioarchive_lookup'
def mappingDBPass = 'msdw'
Class.forName('oracle.jdbc.driver.OracleDriver')

log.info("metaExport.groovy: Parsing Study {}.", studyInstanceUID)

if (studyDateTime == null) {
        log.info("metaExport.groovy: Missing Study Date / Time")
        log.info("metaExport.groovy: Study {} will be deleted", studyInstanceUID)
        ops.scheduleStudyHardDelete()
        return false
}

if  (!timeline.wasFullyForwardedTo("ROUTER") && (study.isOlderThan(Duration.standardDays(2))))  {
        log.debug("metaExport.groovy: Exporting metadata for Study {}.", studyInstanceUID)

        String mintString = baseMINTURL + studyInstanceUID + '/DICOM/metadata'
        URL mintURL = new URL(mintString)
        HttpURLConnection mintConnection = (HttpURLConnection) mintURL.openConnection()
        def responseCode = mintConnection.getResponseCode()
        log.debug("metaExport.groovy: HTTP Response Code for study {}: {}", studyInstanceUID, responseCode)
        if (responseCode != 200) {
                log.error("metaExport.groovy: Failed to get MINT Metadata for study {}.", studyInstanceUID)
                mintConnection.disconnect()
                return false
        }
        DataInputStream mintResponse = new DataInputStream(mintConnection.getInputStream()) 
        metadataXML = mintResponse.getText()
        log.debug("metaExport.groovy: MINT Metadata retrieved for study {}.", studyInstanceUID)
        mintResponse.close()
        mintConnection.disconnect()

        // make database connection
        log.debug("metaExport.groovy: Initiating connection to {}", mappingDBURL)
        try {
                sql = Sql.newInstance(mappingDBURL, mappingDBUser, mappingDBPass)
        } catch (SQLException sqlConnectError) {
                log.error("Failed to connect to {}: {}", mappingDBURL, sqlConnectError)
                return false
        }

        // upload metadata
        // send_phi_xml_data(mrn, accession_no, file_name, xml_data)
        log.debug("metaExport.groovy: Sending metadata for Study {} to {}", studyInstanceUID, mappingDBURL)
        try {
                sql.call '{? = call send_phi_xml_data(?,?,?,?)}', [Sql.NUMERIC, patientID,accessionNumber,null,metadataXML], {sqlResponse ->
                        log.debug("metaExport.groovy: Database response for study {}:{}", studyInstanceUID, sqlResponse)
                }
        } catch (e) {
                log.error("metaExport.groovy: Failed database upload for study {}: {}",studyInstanceUID, e.getMessage())
        log.debug("metaExport.groovy: Closing database connection")
        sql.close()
        log.debug("metaExport.groovy: Database connection closed")
                return false
        }
    log.debug("metaExport.groovy: Closing database connection")
    sql.close()
    log.debug("metaExport.groovy: Database connection closed")

        log.info("metaExport.groovy: Study {} successfully processed. Forwarding study to ROUTER.", studyInstanceUID)
    ops.scheduleStudyForward("ROUTER")

}

if (timeline.wasFullyForwardedTo("ROUTER")) {
        // Verifies data is in VNA before deleting from cache
        log.debug("Study {} is a candidate for deletion.", studyInstanceUID)
        def newStudyInstanceUID = hasher.hashUID(studyInstanceUID)
        
        // Check for hashed study UID and flagged variants
        mintString = vnaBaseMINTURL + newStudyInstanceUID
        mintURL = new URL (mintString)
        HttpURLConnection mintConnection = (HttpURLConnection) mintURL.openConnection()
        mintConnection.setRequestMethod("HEAD")
        responseCode = mintConnection.getResponseCode()
        if (responseCode == 200) {
                log.info("Study {} found in Imaging Research Warehouse and will be deleted from cache.", studyInstanceUID)
                ops.scheduleStudyHardDelete()
                mintConnection.disconnect()
                return true
        }

        mintString = vnaBaseMINTURL + newStudyInstanceUID + '.1'
        mintURL = new URL (mintString)
        HttpURLConnection mintConnection = (HttpURLConnection) mintURL.openConnection()
        mintConnection.setRequestMethod("HEAD")
        responseCode = mintConnection.getResponseCode()
        if (responseCode == 200) {
                log.info("Study {} found in Imaging Research Warehouse and will be deleted from cache.", studyInstanceUID)
                ops.scheduleStudyHardDelete()
                mintConnection.disconnect()
                return true
        }

        mintString = vnaBaseMINTURL + newStudyInstanceUID + '.2'
        mintURL = new URL (mintString)
        HttpURLConnection mintConnection = (HttpURLConnection) mintURL.openConnection()
        mintConnection.setRequestMethod("HEAD")
        responseCode = mintConnection.getResponseCode()
        if (responseCode == 200) {
                log.info("Study {} found in Imaging Research Warehouse and will be deleted from cache.", studyInstanceUID)
                ops.scheduleStudyHardDelete()
                mintConnection.disconnect()
                return true
        }

        mintString = vnaBaseMINTURL + newStudyInstanceUID + '.3'
        mintURL = new URL (mintString)
        HttpURLConnection mintConnection = (HttpURLConnection) mintURL.openConnection()
        mintConnection.setRequestMethod("HEAD")
        responseCode = mintConnection.getResponseCode()
        if (responseCode == 200) {
                log.info("Study {} found in Imaging Research Warehouse and will be deleted from cache.", studyInstanceUID)
                ops.scheduleStudyHardDelete()
                mintConnection.disconnect()
                return true
        }       

}
