log.info("in.groovy v05: Start inbound morpher")

remove(IssuerOfPatientID)
set(IssuerOfPatientID, 'Mount Sinai')
set("IssuerOfPatientIDQualifiersSequence/UniversalEntityID", '2.16.840.1.113883.3.515')
set("IssuerOfPatientIDQualifiersSequence/UniversalEntityIDType", 'ISO')
