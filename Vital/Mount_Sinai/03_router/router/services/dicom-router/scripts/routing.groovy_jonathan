//      Mount Sinai Routing Script v.29
//      Routing script logic:
//      Data with Mount Sinai patient domain:
//              - Try MRN lookup for Patient ID
//              - Ultrasound:
//                      - Sent from PACS
//                              - Send to CTP Tool
//              - De-identifier / flagging script
//              - Send to ROUTER
//              - Send to VIOARCHIVE1 to save metadata
//      Data with Research Warehouse patient domain
//              - Unflagged 
//                      - Send to VioArchive main storage
//              - Flagged 
//                      - Sent from ROUTER
//                              - Send to VioArchive flagged storage
//                      - Sent from other source with 'Burned In Annotation' set to 'NO'
//                              - Unflag
//                              - Send to VioArchive main storage
log.info("routing.groovy v27: Begin routing script.")

import org.dcm4che2.data.DicomObject
import org.dcm4che2.data.Tag.
import com.karos.rialto.workflow.model.GenericWorkflow
import com.karos.rialto.workflow.common.tasks.*
import com.karos.rialto.storage.StorageRepository
import com.karos.rialto.storage.file.FileSystemStorageRepository

import com.karos.rialto.workflow.model.Task.Status
import com.karos.rialto.workflow.model.Task.TaskPriority

import java.time.LocalDateTime

import groovy.sql.Sql
import java.sql.SQLException

def currentDate = LocalDateTime.now()

def fsRepo = new FileSystemStorageRepository('Router','/home/rialto/var/ids1/router')
String[] sopPath = [
    getCallingAETitle(),
    getCalledAETitle(),
    currentDate.getYear(),
    currentDate.getMonthValue(),
    currentDate.getDayOfMonth(),
    currentDate.getHour(),
    currentDate.getMinute(),
    currentDate.getSecond(),
    input.get(StudyInstanceUID)
]

def sinaiDomain = '2.16.840.1.113883.3.515'
def researchDomain = '1.3.6.1.4.1.40920'
def routerAE = 'ROUTER'
def archiveAE = 'VIOARCHIVE'
def flaggedAE = 'VIOARCHIVE1'
def metadataAE = 'METADATA'
def ultrasoundAE = 'USANONPIXEL'
def flag = null
def flagList = ['PHIFlag_', 'AEFlag_', 'US_']
// change this flag to true to save Metadata
def saveMetadata = false

// Receive images
def receiveImageTaskBuilder = new ReceiveImageTask.Builder()
// receiveImageTaskBuilder.setStorageRepository(fsRepo) (using default)
receiveImageTaskBuilder.setStorageTags(sopPath)

def patientID = input.get(PatientID)
def burnedInPHI = input.get(BurnedInAnnotation)

def patientDomain = input.get("IssuerOfPatientIDQualifiersSequence/UniversalEntityID")
log.debug("routing.groovy: Patient Domain is {}", patientDomain)

def callingAE = getCallingAETitle()
log.debug("routing.groovy: Calling AE Title is {}", callingAE)

flagList.each { flagItem ->
    if (patientID.startsWith(flagItem)) {
        log.debug("routing.groovy: Flag {} found in study.", flagItem)
        flag = flagItem
    }
}

if (saveMetadata) {
    def qualifyStudyTaskBuilder = new MorphStudyTask.Builder()
    def sendMetaDataTaskBuilder = new SendStudyTask.Builder()
}
def testDBTaskBuilder = new MorphStudyTask.Builder()
def morphStudyTaskBuilder = new MorphStudyTask.Builder()
def sendStudyTaskBuilder = new SendStudyTask.Builder()
sendStudyTaskBuilder.setMaxRetries(5)
sendStudyTaskBuilder.setRetryDelays("1m", "5m", "30m", "120m", "480m" )
def cleanStorageLocationTaskBuilder = new CleanStorageLocationTask.Builder()

if (patientDomain == researchDomain) {
    log.debug("routing.groovy: Research Warehouse domain found.")
    if (flag == null) {

        log.info("routing.groovy: Sending to VioArchive main storage.")
        
        // Send study
        sendStudyTaskBuilder.addDataDependency(receiveImageTaskBuilder, Status.SUCCESS)
        sendStudyTaskBuilder.setCallingAe(callingAE)
        sendStudyTaskBuilder.setDestination(archiveAE)
        cleanStorageLocationTaskBuilder.addDataDependency(sendStudyTaskBuilder, Status.SUCCESS)

        // Run workflow
        def raStore = new GenericWorkflow.Builder("Send To Archive")
        raStore.addTaskBuilder(receiveImageTaskBuilder)
        raStore.addTaskBuilder(sendStudyTaskBuilder)
        raStore.addTaskBuilder(cleanStorageLocationTaskBuilder)
        return raStore.build()

    } else if (callingAE == routerAE) {

        log.info("routing.groovy: Sending to VioArchive flagged storage.")
        // Send study
        sendStudyTaskBuilder.addDataDependency(receiveImageTaskBuilder, Status.SUCCESS)
        sendStudyTaskBuilder.setCallingAe(callingAE)
        sendStudyTaskBuilder.setDestination(flaggedAE)
        cleanStorageLocationTaskBuilder.addDataDependency(sendStudyTaskBuilder, Status.SUCCESS)

        // Run workflow
        def flagStore = new GenericWorkflow.Builder("Send To Flagged Storage")
        flagStore.addTaskBuilder(receiveImageTaskBuilder)
        flagStore.addTaskBuilder(sendStudyTaskBuilder)
        flagStore.addTaskBuilder(cleanStorageLocationTaskBuilder)
        return flagStore.build()

    } else if (burnedInPHI != 'YES') {

        log.info("routing.groovy: Unflagging corrected data and sending to VioArchive main storage.")

        // Unflag study
        morphStudyTaskBuilder.setMorpherScriptName("/home/rialto/rialto/etc/services/dicom-router/scripts/unflag.groovy")
        morphStudyTaskBuilder.addDataDependency(receiveImageTaskBuilder,Status.SUCCESS)

        // Send study
        sendStudyTaskBuilder.addDataDependency(morphStudyTaskBuilder, Status.SUCCESS)
        sendStudyTaskBuilder.setCallingAe(getCallingAETitle())
        sendStudyTaskBuilder.setDestination(archiveAE)

        // Clean storage
        cleanStorageLocationTaskBuilder.addDataDependency(sendStudyTaskBuilder, Status.SUCCESS)

        // Run workflow
        def unflagStore = new GenericWorkflow.Builder("Unflag And Send To Archive")
        unflagStore.addTaskBuilder(receiveImageTaskBuilder)
        unflagStore.addTaskBuilder(morphStudyTaskBuilder)
        unflagStore.addTaskBuilder(sendStudyTaskBuilder)
        unflagStore.addTaskBuilder(cleanStorageLocationTaskBuilder)
        return unflagStore.build()

    } else {

        log.warn("routing.groovy: Flagged data has not been corrected. Data not being routed.")
        cleanStorageLocationTaskBuilder.addDataDependency(receiveImageTaskBuilder, Status.SUCCESS)
        def routingFail = new GenericWorkflow.Builder("Uncorrected flagged data")
        routingFail.addTaskBuilder(receiveImageTaskBuilder)
        routingFail.addTaskBuilder(cleanStorageLocationTaskBuilder)
        return routingFail.build()
    }
} else {
    if ( (get(Modality) == 'US') && (callingAE != ultrasoundAE) ) {

        log.info("routing.groovy: Sending to ultrasound pixel data tool.")

        // Send study
        sendStudyTaskBuilder.addDataDependency(receiveImageTaskBuilder, Status.SUCCESS)
        sendStudyTaskBuilder.setCallingAe(getCallingAETitle())
        sendStudyTaskBuilder.setDestination(ultrasoundAE)

        // Clean storage
        cleanStorageLocationTaskBuilder.addDataDependency(sendStudyTaskBuilder, Status.SUCCESS)

        // Run workflow
        def ultrasoundWF = new GenericWorkflow.Builder("Sending ultrasound data to pixel de-identifier")
        ultrasoundWF.addTaskBuilder(receiveImageTaskBuilder)
        ultrasoundWF.addTaskBuilder(sendStudyTaskBuilder)
        ultrasoundWF.addTaskBuilder(cleanStorageLocationTaskBuilder)
        return ultrasoundWF.build()
    }

    if (saveMetadata) {

        // Qualify study
        log.debug("routing.groovy: Adding Mount Sinai domain.")
        qualifyStudyTaskBuilder.setMorpherScriptName("/home/rialto/rialto/etc/services/dicom-router/scripts/in.groovy")
        qualifyStudyTaskBuilder.addDataDependency(receiveImageTaskBuilder,Status.SUCCESS)
        qualifyStudyTaskBuilder.setPriority(90)

        // Send metadata
        log.debug("routing.groovy: Sending to metadata storage.")
        sendMetaDataTaskBuilder.addDataDependency(qualifyStudyTaskBuilder, Status.SUCCESS)
        sendMetaDataTaskBuilder.setCallingAe(getCallingAETitle())
        sendMetaDataTaskBuilder.setDestination(metadataAE)
        sendMetaDataTaskBuilder.setPriority(90)
    }

    // Lookup patient
    def mappingDBURL = 'jdbc:oracle:thin:@sdwhetlprod01.mountsinai.org:1521:ETLPROD'
    def mappingDBUser = 'vioarchive_lookup'
    def mappingDBPass = 'msdw'
    Class.forName('oracle.jdbc.driver.OracleDriver')

    def oldPatientID = get(PatientID)
    def oldAccessionNumber = get(AccessionNumber)
    def newPatientID
    def sqlError = null

    log.debug("routing.groovy: Initiating connection to {}", mappingDBURL)
    try {
        sql = Sql.newInstance(mappingDBURL, mappingDBUser, mappingDBPass)
    } catch (SQLException sqlConnectError) {
        log.warn("routing.groovy: Failed to connect to {}: {}. Initiating de-identifier anyway.", mappingDBURL, sqlConnectError)
        log.debug("routing.groovy: Closing database connection")
        sql.close()
        log.debug("routing.groovy: Database connection closed")
            
        // Morph study
        log.info("routing.groovy: Sending to de-identifier.")
        testDBTaskBuilder.setMorpherScriptName("/home/rialto/rialto/etc/services/dicom-router/scripts/dbTest.groovy")
        testDBTaskBuilder.addDataDependency(receiveImageTaskBuilder,Status.SUCCESS)

        morphStudyTaskBuilder.setMorpherScriptName("/home/rialto/rialto/etc/services/dicom-router/scripts/deidLive.groovy")
        morphStudyTaskBuilder.addDataDependency(testDBTaskBuilder,Status.SUCCESS)

        // Send study
        sendStudyTaskBuilder.addDataDependency(morphStudyTaskBuilder, Status.SUCCESS)
        sendStudyTaskBuilder.setCallingAe(routerAE)
        sendStudyTaskBuilder.setDestination(routerAE)

        // Clean storage
        if (saveMetadata) {
            cleanStorageLocationTaskBuilder.addDataDependency(sendMetaDataTaskBuilder, Status.SUCCESS)
        }
        cleanStorageLocationTaskBuilder.addDataDependency(sendStudyTaskBuilder, Status.SUCCESS)

        // Run workflow
        def deidTry = new GenericWorkflow.Builder("De-identify " + oldPatientID +" to (unknown)")
        deidTry.addTaskBuilder(receiveImageTaskBuilder)
        if (saveMetadata) {
            deidTry.addTaskBuilder(qualifyStudyTaskBuilder)
            deidTry.addTaskBuilder(sendMetaDataTaskBuilder)
        }
        deidTry.addTaskBuilder(testDBTaskBuilder)
        deidTry.addTaskBuilder(morphStudyTaskBuilder)
        deidTry.addTaskBuilder(sendStudyTaskBuilder)
        deidTry.addTaskBuilder(cleanStorageLocationTaskBuilder)
        return deidTry.build()          
    }
    log.debug("routing.groovy: Getting New Patient ID")
    try {
        sql.call '{? = call get_deid(?,?,?)}', [Sql.LONGVARCHAR, oldPatientID, oldAccessionNumber,'MRN'], {row ->
            log.debug("deidLive.groovy: Returned {}", row)
            newPatientID = row
            log.info("routing.groovy: New Patient ID: {}",newPatientID)
        }
    } catch (e) {
        log.warn("routing.groovy: Unable to map Patient ID for Study {}. Aborting.\n{}", get(StudyInstanceUID), e.getMessage())
        log.debug("routing.groovy: Closing database connection")
        sql.close()
        log.debug("routing.groovy: Database connection closed")
        
        cleanStorageLocationTaskBuilder.addDataDependency(receiveImageTaskBuilder, Status.SUCCESS)
        if (saveMetadata) {
            cleanStorageLocationTaskBuilder.addDataDependency(sendMetaDataTaskBuilder, Status.SUCCESS)
        }
        // Run workflow
        def mrnFail = new GenericWorkflow.Builder("De-identifer Failed MRN DB Lookup")
        mrnFail.addTaskBuilder(receiveImageTaskBuilder)
        if (saveMetadata) {
            mrnFail.addTaskBuilder(qualifyStudyTaskBuilder)
            mrnFail.addTaskBuilder(sendMetaDataTaskBuilder)
        }
        mrnFail.addTaskBuilder(cleanStorageLocationTaskBuilder)
        return mrnFail.build()
    }
    log.debug("routing.groovy: Closing database connection")
    sql.close()
    log.debug("routing.groovy: Database connection closed")

    // Morph study
    log.info("routing.groovy: Sending to de-identifier.")
    testDBTaskBuilder.setMorpherScriptName("/home/rialto/rialto/etc/services/dicom-router/scripts/dbTest.groovy")
    testDBTaskBuilder.addDataDependency(receiveImageTaskBuilder,Status.SUCCESS)

    morphStudyTaskBuilder.setMorpherScriptName("/home/rialto/rialto/etc/services/dicom-router/scripts/deidLive.groovy")
    morphStudyTaskBuilder.addDataDependency(testDBTaskBuilder,Status.SUCCESS)

    // Send study
    sendStudyTaskBuilder.addDataDependency(morphStudyTaskBuilder, Status.SUCCESS)
    sendStudyTaskBuilder.setCallingAe(routerAE)
    sendStudyTaskBuilder.setDestination(routerAE)

    // Clean storage
    if (saveMetadata) {
        cleanStorageLocationTaskBuilder.addDataDependency(sendMetaDataTaskBuilder, Status.SUCCESS)
    }
    cleanStorageLocationTaskBuilder.addDataDependency(sendStudyTaskBuilder, Status.SUCCESS)

    // Run workflow
    def deidwf = new GenericWorkflow.Builder("De-identify " + oldPatientID + " to " + newPatientID)
    deidwf.addTaskBuilder(receiveImageTaskBuilder)
    if (saveMetadata) {
        deidwf.addTaskBuilder(qualifyStudyTaskBuilder)
        deidwf.addTaskBuilder(sendMetaDataTaskBuilder)
    }
    deidwf.addTaskBuilder(testDBTaskBuilder)
    deidwf.addTaskBuilder(morphStudyTaskBuilder)
    deidwf.addTaskBuilder(sendStudyTaskBuilder)
    deidwf.addTaskBuilder(cleanStorageLocationTaskBuilder)
    return deidwf.build()
} 

log.info("routing.groovy: Ending routing script. This message should not appear")
