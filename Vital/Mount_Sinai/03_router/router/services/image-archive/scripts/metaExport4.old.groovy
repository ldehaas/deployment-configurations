// metaExport4.groovy
// v03
// For non-deidentified metadata
// Gets MINT metadata, sends it to MRN database and then deletes it
import groovy.sql.Sql
import java.sql.SQLException
import java.net.URL

def sinaiDomain = '2.16.840.1.113883.3.515'
def studyInstanceUID = study.getStudyInstanceUID()
def patientID = study.get(PatientID)
def accessionNumber = study.get(AccessionNumber)
// def patientDomain = study.get("IssuerOfPatientIDQualifiersSequence/UniversalEntityID")
def baseMINTURL='http://127.0.0.1:8080/vault/mint/studies/'
def mappingDBURL = 'jdbc:oracle:thin:@sdwhetlprod01.mountsinai.org:1521:ETLPROD'
def mappingDBUser = 'vioarchive_lookup'
def mappingDBPass = 'msdw'
Class.forName('oracle.jdbc.driver.OracleDriver')

log.info("metaExport.groovy: Parsing Study {}.", studyInstanceUID)

log.debug("metaExport.groovy: Exporting metadata for Study {}.", studyInstanceUID)

String mintString = baseMINTURL + studyInstanceUID + '/DICOM/metadata'
URL mintURL = new URL(mintString)
HttpURLConnection mintConnection = (HttpURLConnection) mintURL.openConnection()
def responseCode = mintConnection.getResponseCode()
log.debug("metaExport.groovy: HTTP Response Code for study {}: {}", studyInstanceUID, responseCode)
if (responseCode != 200) {
	log.error("metaExport.groovy: Failed to get MINT Metadata for study {}.", studyInstanceUID)
	mintConnection.disconnect()
	return false
}
DataInputStream mintResponse = new DataInputStream(mintConnection.getInputStream()) 
metadataXML = mintResponse.getText()
log.debug("metaExport.groovy: MINT Metadata retrieved for study {}.", studyInstanceUID)
mintResponse.close()
mintConnection.disconnect()

// make database connection
log.debug("metaExport.groovy: Initiating connection to {}", mappingDBURL)
try {
	sql = Sql.newInstance(mappingDBURL, mappingDBUser, mappingDBPass)
} catch (SQLException sqlConnectError) {
	log.error("Failed to connect to {}: {}", mappingDBURL, sqlConnectError)
	return false
}

// upload metadata
// send_phi_xml_data(mrn, accession_no, file_name, xml_data)
log.debug("metaExport.groovy: Sending metadata for Study {} to {}", studyInstanceUID, mappingDBURL)
try {
	sql.call '{? = call send_phi_xml_data(?,?,?,?)}', [Sql.NUMERIC, patientID,accessionNumber,null,metadataXML], {sqlResponse ->
		log.debug("metaExport.groovy: Database response for study {}:{}", studyInstanceUID, sqlResponse)
	}
} catch (e) {
	log.error("metaExport.groovy: Failed database upload for study {}: {}",studyInstanceUID, e.getMessage())
    log.debug("metaExport.groovy: Closing database connection")
    sql.close()
    log.debug("metaExport.groovy: Database connection closed")
	return false
}
log.debug("metaExport.groovy: Closing database connection")
sql.close()
log.debug("metaExport.groovy: Database connection closed")

log.info("metaExport.groovy: Study {} successfully processed. Deleting study.", studyInstanceUID)
ops.scheduleStudyHardDelete()
