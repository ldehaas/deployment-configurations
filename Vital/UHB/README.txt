UHB = University Hospitals Birmingham

On 2020-04-18, this part of "Deployment Configurations" was moved to a 
new customer specific repository:
https://bitbucket.org/karoshealth/uhb/src/8fa4bce249ce3f20080783970615c69290ca2cbe/config-mirror/ 

At that time, https://bitbucket.org/karoshealth/deployment-configurations/src/master/Vital/UHB/ had the following files and timestamps:

production/server_uhbvna1ez 2020‑03‑13
test/server_paxton          2020‑03‑13
README.txt                  2019‑12‑13
