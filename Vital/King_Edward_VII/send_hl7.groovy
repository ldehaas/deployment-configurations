/**
* Usage: rtkgrv send_hl7.groovy localhost 13337 directory-with-hl7 [error-directory]
*
* where the files in <directory-with-hl7> are legal HL7 messages. 
*
* If an error-directory is specified, HL7 messages which get negative acknowledgments (AR/AE) will be moved to the error
* directory instead of being retried indefinitely. If we don't get a clear response from the HL7 
* destination, we will leave the HL7 messages in their existing directory for a later retry.
*
* @author Oleksi Derkatch
* @since June 9, 2016
*/
import com.karos.rtk.common.*;
import ca.uhn.hl7v2.parser.*;

def destinationClient;
def errorDirectory = null;

def sendHl7 = { dest, msg ->
    // for pretty-printing multi-line strings:
    msg = msg.replaceAll("\n\\s*", "\n")
    println "Sending request to $dest:\n$msg\n"
    msg = PipeParser.getInstanceWithNoValidation().parse(msg.replaceAll("\n", "\r"))
    def rsp = dest.send(msg)
    println "Got response from $dest:\n${rsp.toString().replaceAll('\r', '\n')}"
    return rsp
}

def removeFile = { fileName ->
    fileName.delete()
}

def sendAndClearHl7 = { hl7FileName ->

    try {
        println "\n\nSending HL7 file: " + hl7FileName.name
        def response = sendHl7(destinationClient, hl7FileName.getText())

        def terser = new Terser(response)
        def responseCode = terser.get("/.MSA-1")

        if ("AA" == responseCode) {
            println("Removing HL7 file (" + hl7FileName + ") because response successfully received.")
            removeFile(hl7FileName)
        } else {
            if (errorDirectory == null) {
                println("Received negative response: " + responseCode + ". Not removing file")
            } else {
                println("Received negative response: " + responseCode + ". Moving file to error directory: " + errorDirectory)
                hl7FileName.renameTo(new File(errorDirectory, hl7FileName.getName()));  
            }
        }
    } catch (Exception e) {
        println("Encountered exception while sending HL7. Not removing file. Error was: " + e.getMessage())
    }
}

def main = { -> 
    def destinationHost = args[0]
    def destinationPort = args[1]
    destinationClient = new HL7v2Client(destinationHost, destinationPort.toInteger())

    def directory = args[2]

    if (args.length == 4 && args[3] != null) {
        errorDirectory = new File(args[3])
        
        if( !errorDirectory.exists() ) {
          errorDirectory.mkdirs()
        }
    }

    new File(directory).eachFile {
        sendAndClearHl7(it)
    }
}

main()