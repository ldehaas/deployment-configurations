<?xml version="1.0"?>
<config>
    <!-- Default Connect config -->

    <!-- For connect to receive cfind/cmove requests -->
    <server type="dicom" id="dicom-connect">
        <port>4109</port>
    </server>

    <!-- For connect to receive fps triggers -->
    <server type="hl7v2" id="hl7-connect">
        <port>2395</port>
    </server>

    <service type="connect" id="connect">
        <server idref="dicom-connect" name="cfind" />
        <server idref="dicom-connect" name="cmove" />
        <server idref="dicom-connect" name="cstore" />
        <server idref="hl7-connect" name="fps" />

        <!-- Reference to an empi for translating patient ids to the domains of
             the sources of studies. -->

        <device idref="pix" />

        <!-- Reference to an hl7 device for the Move Destination in the cache
        and forward workflow. -->

        <device idref="PACSHL7" name="PACSHL7"/>

        <config>
            <!-- Where to search for studies. -->
            <prop name="Sources">
                <dicom>
                    <!-- Required. Must reference a device defined below. The device must have
                         a domain defined. Patient ids in the query will be translated to the
                         domain of the source. Sources with no known domains for a given patient
                         will not be queried. -->
                    <ae>DCM4CHEE</ae>
                    <domain crossReferencing="false">PPTH&amp;princeton.plainsboro&amp;ISO</domain>
                    <domain crossReferencing="false">RH&amp;royal.hospital&amp;ISO</domain>

                    <!-- Optional. -->
                    <cfindRequestMorpher>{rialto.rootdir}/etc/services/connect/scripts/cfind_request_morpher.groovy</cfindRequestMorpher>

                    <!-- Optional. -->
                    <!--cfindResponseMorpher>{rialto.rootdir}/etc/services/connect/scripts/cfind_response_morpher.groovy</cfindResponseMorpher-->
                </dicom>
                <dicom>
                    <ae>DCM4CHEE2</ae>
                    <domain crossReferencing="false">PPTH&amp;princeton.plainsboro&amp;ISO</domain>
                    <cfindRequestMorpher>{rialto.rootdir}/etc/services/connect/scripts/cfind_request_morpher.groovy</cfindRequestMorpher>
                    <!--<cfindResponseMorpher>{rialto.rootdir}/etc/services/connect/scripts/cfind_response_morpher.groovy</cfindResponseMorpher>-->
                </dicom>
                <CMoveProxyMaxPause>45s</CMoveProxyMaxPause>
                <CMoveProxyTimeout>45s</CMoveProxyTimeout>
                <AdHocCFindRequestMorpher>{rialto.rootdir}/etc/services/connect/scripts/adhoc_cfind_request_morpher.groovy</AdHocCFindRequestMorpher>
            </prop>

            <prop name="FetchPriors">
                <trigger>
                    <!-- Optional. For normalization of inbound hl7 messages.
                         Could be used to drop uninteresting messages or normalize pids/domains. -->
                    <!--morpher>{rialto.rootdir}/etc/services/connect/scripts/fps_trigger_morpher.groovy</morpher-->

                    <!-- Optional. For dropping of chatty messages. -->
                    <!--duplicateFilter-->
                        <!-- A message is considered duplicate if all of the values at the
                             given locations are identical.
                             Optional, defaults to Quebec-specific values. -->
<!--
                        <field>/.PID-2</field>
                        <field>/.PID-2-4-2</field>
-->
                        <!-- After this period, a message will be forgotten and a duplicate
                             will be processed again. Optional, default 24h. -->
<!--
                        <age>24h</age>
                    </duplicateFilter>
-->
                    <!-- Optional. Enables queueing of prefetch triggers for retry after failure.
                         Same format as OrderRetry property for connect-dicom and connect-xds. -->
                    <retry>
                        <!-- Required. -->
                        <storageDir>${rialto.rootdir}/var/connect/orders</storageDir>

                        <!-- Optional. -->
                        <scanInterval>1s</scanInterval>

                        <!-- Optional. -->
                        <retryAfter>5s</retryAfter>

                        <!-- Optional. Defines concurrency of prefetch processing. -->
                        <poolSize>1</poolSize>

                        <!-- Optional. Number of times to try a single message. -->
                        <retries>4</retries>
                    </retry>
                </trigger>

                <!-- Required. Converts fps trigger to a cfind request. Compatible with
                     existing connect scripts. Currently, must set PatientID and IssuerOfPatientID. -->
                <hl7ToCFindMorpher>{rialto.rootdir}/etc/services/connect/scripts/hl7_to_cfind_morpher.groovy</hl7ToCFindMorpher>

                <!-- Optional. Compatible with existing connect CFindResponseRankFilter scripts. -->
                <cfindResponseFilter>{rialto.rootdir}/etc/services/connect/scripts/cfind_response_rank_filter.groovy</cfindResponseFilter>

                <!-- Enables querying of move destination to filter out studies that it already has. -->
                <instanceAvailabilityFilter>
                    <!-- Optional.  Defines how to query the destination.  MULTI_SUID or SINGLE_SUID.
                         SINGLE_SUID results in multiple queries. -->
                    <queryStrategy>MULTI_SUID</queryStrategy>
		    <evaluation>
                <inspectorScript>{rialto.rootdir}/etc/services/connect/scripts/instance_availability_evaluation_morpher.groovy</inspectorScript>
		    </evaluation>

                    <!-- Optional. Not commonly needed. -->
                    <cfindMorpher>{rialto.rootdir}/etc/services/connect/scripts/instance_availability_morpher.groovy</cfindMorpher>
                </instanceAvailabilityFilter>
            </prop>

            <prop name="StudyInfoCache">
                <timeout>30m</timeout>
                <checkFrequency>5m</checkFrequency>
            </prop>

            <!-- AE title to send studies to. Currently sources will be asked to move the studies
                 directly here. In future, will also support proxying the studies through connect
                 for localization.
                 For instance availability queries to work (see above), should match the ae title
                 of a device configured below. -->
            <prop name="MoveDestination">
                <destinationAE>${IA-AETitle-PF}</destinationAE>
                <moveProgressMonitoring>
                    <cfindPoll>
                        <pollDestinationAE>${IA-AETitle-PF}</pollDestinationAE>
                        <pollFrequency>1s</pollFrequency>
                        <maxIdlePeriod>10m</maxIdlePeriod>
                    </cfindPoll>
        		<reportSourceCompletions />
                </moveProgressMonitoring>
            </prop>


           <!--
                This property enables the caching of studies before forwarding to the Move Destination
                in both the Adhoc C-MOVE and Fetch Prior Studies workflow.
                With this enabled, Connect can localize images as well as retrieve/localize HL7 ORU
                reports from the report repository before forwarding on to the MoveDestination.
            -->
            <prop name="CacheAndForward">
                <ImageDelivery>
                  <imageCache>
                    <cacheDir>{rialto.rootdir}/var/connect/images</cacheDir>
                    <maxRetries>4</maxRetries>
                    <retryWaitTime>2m</retryWaitTime>
                    <maxConcurrentCStores>5</maxConcurrentCStores>
                  </imageCache>
                </ImageDelivery>

                <FilterDuplicateAccessionNumbers/>
                <PACSQueryAE>DCM4CHEE</PACSQueryAE>
                <DelayAfterSendingOrder>15s</DelayAfterSendingOrder>

                <ReportQueryParams>PatientID,AccessionNumber</ReportQueryParams>

                <ReportQueryParamMorpher>${rialto.rootdir}/etc/services/connect/scripts/report_query_param_morpher.groovy</ReportQueryParamMorpher>

                <ReportRetrievalURL>http://10.0.1.215:8080/reports/fhir/</ReportRetrievalURL>
                <ReportSendingTiming>BEFORE_STUDY</ReportSendingTiming>

                <PatientCFindToPACSRequestMorpher>
                    {rialto.rootdir}/etc/services/connect/scripts/patient_cfind_to_pacs.groovy
                </PatientCFindToPACSRequestMorpher>

                <ForeignImageMorpher>
                    {rialto.rootdir}/etc/services/connect/scripts/foreign_image_morpher.groovy
                </ForeignImageMorpher>

                <ForeignReportMorpher>
                    {rialto.rootdir}/etc/services/connect/scripts/oru2oru_localizer.groovy
                </ForeignReportMorpher>

                <DICOMSRToORUMorpher>
                    {rialto.rootdir}/etc/services/connect/scripts/sr_to_oru_morpher.groovy
                </DICOMSRToORUMorpher>

                <!--
                <ReportRankFilter>
                    {rialto.rootdir}/etc/services/connect/scripts/report_rank_filter.groovy
                </ReportRankFilter>

                <ImageToHL7OrderMorpher>
                    {rialto.rootdir}/etc/services/connect/scripts/image_to_hl7_order.groovy
                </ImageToHL7OrderMorpher>
                -->

            </prop>

            <!-- AE title connect uses when talking to other systems -->
            <prop name="LocalAETitle" value="${CONNECT-AETitle}" />
        </config>
    </service>

    <device type="hl7v2" id="PACSHL7">
        <host>localhost</host>
        <port>9000</port>
    </device>

    <device type="dicom" id="CONNECT">
        <host>${System-Tests-IP}</host>
        <port>4109</port>
        <aetitle>${CONNECT-AETitle}</aetitle>
        <!-- although not needed for connect, this is useful for testing -->
        <domain>PPTH&amp;princeton.plainsboro&amp;ISO</domain>
    </device>

    <!-- Where we're sending the studies (see MoveDestination above).
         Optional, but highly recommended for supporting instance availability queries. -->
    <device type="dicom" id="PF_RIALTO">
        <host>localhost</host>
        <port>4104</port>
        <aetitle>${IA-AETitle-PF}</aetitle>
        <!-- although not needed for connect, this is useful for testing -->
        <domain>MASH&amp;mobile.army&amp;ISO</domain>
    </device>

    <device type="dicom" id="PF_RIALTO_TWO">
        <host>localhost</host>
        <port>4106</port>
        <aetitle>${IA-AETitle-PF}_2</aetitle>
    </device>

    <!-- Where to search for studies. Referenced from <prop name="Sources"> above. -->

     <device type="dicom" id="DCM4CHEE">
        <host>10.0.1.63</host>
        <port>11112</port>

        <!-- Required. Queries will be translated into this domain. -->
        <domain>PPTH&amp;princeton.plainsboro&amp;ISO</domain>
        <domain>RH&amp;royal.hospital&amp;ISO</domain>

         <!-- Must match <ae> in Sources config above. -->
       <aetitle>DCM4CHEE</aetitle>
    </device>

    <device type="dicom" id="DCM4CHEE2">
        <host>10.0.1.83</host>
        <port>11112</port>

        <!-- Required. Queries will be translated into this domain. -->
        <domain>PPTH&amp;princeton.plainsboro&amp;ISO</domain>

        <!-- Must match <ae> in Sources config above. -->
        <aetitle>DCM4CHEE2</aetitle>
    </device>

    <!-- for connect ha -->
    <device type="dicom" id="native-dc">
        <host>10.0.1.227</host>
        <port>${IA-DicomPort}</port>
        <aetitle>NATIVE_DC</aetitle>
    </device>

    <device type="dicom" id="other-dc">
        <host>10.0.1.238</host>
        <port>${IA-DicomPort}</port>
        <aetitle>OTHER_DC</aetitle>
    </device>

</config>
