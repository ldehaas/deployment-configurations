def scriptName = "image-archive/scripts/qcreconciliation.groovy: "
log.debug(scriptName + "Reconciling SOP {} with the matching MWL entry.", get(SOPInstanceUID));

log.debug(scriptName + "MWL dump:\n{}", mwlEntry)

set('AccessionNumber', mwlEntry.get('AccessionNumber'))
//set('IssuerOfAccessionNumberSequence/LocalNamespaceEntityID', mwlEntry.get('IssuerOfAccessionNumberSequence/LocalNamespaceEntityID'))
//set('IssuerOfAccessionNumberSequence/UniversalEntityID', mwlEntry.get('IssuerOfAccessionNumberSequence/UniversalEntityID'))
//set('IssuerOfAccessionNumberSequence/UniversalEntityIDType', mwlEntry.get('IssuerOfAccessionNumberSequence/UniversalEntityIDType'))

//set('StudyInstanceUID', mwlEntry.get('StudyInstanceUID'));

//set('ReferringPhysicianName', mwlEntry.get('ReferringPhysicianName'))

set('PatientID', mwlEntry.get('PatientID'))
//set('IssuerOfPatientID', mwlEntry.get('IssuerOfPatientID'))
//set('IssuerOfPatientIDQualifiersSequence/UniversalEntityID', mwlEntry.get('IssuerOfPatientIDQualifiersSequence/UniversalEntityID'))
//set('IssuerOfPatientIDQualifiersSequence/UniversalEntityIDType', mwlEntry.get('IssuerOfPatientIDQualifiersSequence/UniversalEntityIDType'))

set('PatientName', mwlEntry.get('PatientName'))
set('PatientSex', mwlEntry.get('PatientSex'))
set('PatientBirthDate', mwlEntry.get('PatientBirthDate'))
//set('PatientWeight', mwlEntry.get('PatientWeight'))
//set('PatientState', mwlEntry.get('PatientState'))

//set('MedicalAlerts', mwlEntry.get('MedicalAlerts'))
//set('RequestingPhysician', mwlEntry.get('RequestingPhysician'))

set('RequestedProcedureID', mwlEntry.get('RequestedProcedureID'))
//set('RequestedProcedurePriority', mwlEntry.get('RequestedProcedurePriority'))

//set('OrderEnteredBy', mwlEntry.get('OrderEnteredBy'))
//set('OrderEntererLocation', mwlEntry.get('OrderEntererLocation'))
//set('OrderCallbackPhoneNumber', mwlEntry.get('OrderCallbackPhoneNumber'))

//set('AdmissionID', mwlEntry.get('AdmissionID'))
//set('IssuerOfAdmissionID', mwlEntry.get('IssuerOfAdmissionID'))

// added KEVII
set('StudyDescription', mwlEntry.get('RequestedProcedureDescription'))

set('RequestedProcedureDescription', mwlEntry.get('RequestedProcedureDescription'))
//set('RequestedProcedureCodeSequence/CodeValue', mwlEntry.get('RequestedProcedureCodeSequence/CodeValue'))
//set('RequestedProcedureCodeSequence/CodingSchemeDesignator', mwlEntry.get('RequestedProcedureCodeSequence/CodingSchemeDesignator'))
//set('RequestedProcedureCodeSequence/CodeMeaning', mwlEntry.get('RequestedProcedureCodeSequence/CodeMeaning'))

log.debug(scriptName + "MWL ConfidentialityConstraintOnPatientDataDescription = {}", mwlEntry.get('ConfidentialityConstraintOnPatientDataDescription'))
set('ConfidentialityCode', mwlEntry.get('ConfidentialityConstraintOnPatientDataDescription'))

log.debug(scriptName + "Finished reconciling SOP {}", get(SOPInstanceUID));
