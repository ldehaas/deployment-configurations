log.info("IA Inbound CFind Morpher - START")

set(ConfidentialityCode, null)
set(ConfidentialityConstraintOnPatientDataDescription, null)

if (get('PatientID') != null) {
    set('IssuerOfPatientID', 'KE')
    set('IssuerOfPatientIDQualifiersSequence/UniversalEntityID', '2.16.124.113638.7.1.1.1')
    set('IssuerOfPatientIDQualifiersSequence/UniversalEntityIDType', 'ISO')
}

log.info("CFind Req: {}", input)

log.info("IA Inbound CFind Morpher - End")
