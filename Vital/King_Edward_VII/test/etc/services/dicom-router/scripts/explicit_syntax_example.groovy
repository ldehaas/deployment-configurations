import org.dcm4che2.data.DicomObject
import org.dcm4che2.data.Tag.*

import com.karos.rialto.workflow.model.GenericWorkflow
import com.karos.rialto.workflow.common.tasks.*
import com.karos.rialto.storage.StorageRepository
import com.karos.rialto.storage.file.FileSystemStorageRepository

import com.karos.rialto.workflow.model.Task.Status
import com.karos.rialto.workflow.model.Task.TaskPriority

import java.time.LocalDateTime


// Create the Storage Repository and initial 'path'
def currentDate = LocalDateTime.now()
def fsRepo = new FileSystemStorageRepository('Router', 'var/router')
String[] sopPath = [getCallingAETitle(),
                    getCalledAETitle(),
                    currentDate.getYear(),
                    currentDate.getMonthValue(),
                    currentDate.getDayOfMonth(),
                    currentDate.getHour(),
                    currentDate.getMinute(),
                    currentDate.getSecond(),
                    input.get(StudyInstanceUID)
                    ]

// Receive images
// Need this Task so the listener can add the events as the images come in.
def receiveImageTaskBuilder = new ReceiveImageTask.Builder()
// Set the location to store the images as they move through the Router.
receiveImageTaskBuilder.setStorageRepository(fsRepo)
receiveImageTaskBuilder.setStorageTags(sopPath)

// Task to morph the studies, if required.
def morphStudyTaskBuilder = new MorphStudyTask.Builder()
// set morphing script name
morphStudyTaskBuilder.setMorpherScriptName("etc/services/dicom-router/scripts/in.groovy")
// Set the 2nd task to be dependant on the success of the first.
morphStudyTaskBuilder.addDataDependency(receiveImageTaskBuilder,Status.SUCCESS)

// Transcode study
def transcodeInstancesTaskBuilder = new TranscodeInstancesTask.Builder()
// set the inbound and outbound Transfer Syntax
transcodeInstancesTaskBuilder.setSupportedSourceTs("[1.2.840.10008.1.2, 1.2.840.10008.1.2.1, 1.2.840.10008.1.2.2]")
transcodeInstancesTaskBuilder.setDestinationTs("1.2.840.10008.1.2.4.80")
// Set the 3rd task to be dependant on the success of the second.
transcodeInstancesTaskBuilder.addDataDependency(morphStudyTaskBuilder,Status.SUCCESS)
    
// Send Study  to the first destination
def sendStudyTaskBuilder1 = new SendStudyTask.Builder()
// set source for the study
sendStudyTaskBuilder1.setCallingAe(getCallingAETitle())
// set destination for the study, in this case, a device from the Rialto Device Registry
sendStudyTaskBuilder1.setDestination("RIALTO_SOUTH")
// set the priority of the task
sendStudyTaskBuilder1.setPriority(TaskPriority.HIGH .getPriorityValue())

// set the task dependancy
sendStudyTaskBuilder1.addDataDependency(transcodeInstancesTaskBuilder,Status.SUCCESS)

// Send Study to a 2nd destination
def sendStudyTaskBuilder2 = new SendStudyTask.Builder()
// set source for the study
sendStudyTaskBuilder2.setCallingAe(getCallingAETitle())
// set the destination for the study
sendStudyTaskBuilder2.setDestination("DELL_INDEX_LINK")
// set the priority of the task
sendStudyTaskBuilder2.setPriority(TaskPriority.LOW.getPriorityValue())

// set the task dependancy
sendStudyTaskBuilder2.addDataDependency(transcodeInstancesTaskBuilder,Status.SUCCESS,Status.FAILURE)

// Clean up the temp files on the Router
def cleanStorageLocationTaskBuilder = new CleanStorageLocationTask.Builder()
// The task will only run when all the Tasks have completed successfully
// We also need to reference all the Tasks, so all the images will be removed from the system.
cleanStorageLocationTaskBuilder.addDataDependency(sendStudyTaskBuilder1,Status.SUCCESS)
cleanStorageLocationTaskBuilder.addDataDependency(sendStudyTaskBuilder2,Status.SUCCESS)
cleanStorageLocationTaskBuilder.addDataDependency(transcodeInstancesTaskBuilder,Status.SUCCESS)
cleanStorageLocationTaskBuilder.addDataDependency(morphStudyTaskBuilder,Status.SUCCESS)
cleanStorageLocationTaskBuilder.addDataDependency(receiveImageTaskBuilder,Status.SUCCESS)

// define workflow and add tasks
def wfb = new GenericWorkflow.Builder("Route Images from " + getCallingAETitle())
wfb.addTaskBuilder(receiveImageTaskBuilder)
wfb.addTaskBuilder(morphStudyTaskBuilder)
wfb.addTaskBuilder(transcodeInstancesTaskBuilder)
wfb.addTaskBuilder(sendStudyTaskBuilder1)
wfb.addTaskBuilder(sendStudyTaskBuilder2)
wfb.addTaskBuilder(cleanStorageLocationTaskBuilder)

return wfb.build()
