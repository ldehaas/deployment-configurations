log.debug("in cfind_request_wildcards.groovy")

def pid = get(PatientID)
//patientid = 12345^a^^KE&2.16.124.113638.7.1.1.1&ISO
log.debug("cfind_request_wildcards.groovy: patientid is ${pid}")

if (pid == null)
    return

if (pid.endsWith("*")) {
    pid = pid.substring(0, pid.length()-1)
}

def (patientid, fqpid, tmp1, tmp2) = pid.tokenize("^")
if (tmp1) fqpid = tmp1
if (tmp2) fqpid = tmp2

if (fqpid == null) {
    //either its provided or its not; not handling partial fqpid
    fqpid = "KE&2.16.124.113638.7.1.1.1&ISO"
}

def new_pid = ""
if ((patientid) && (patientid.length() > 0)) {
    new_pid = patientid + "^^^" + fqpid
}

log.debug("cfind_request_wildcards.groovy: new patientid is ${new_pid}")
set(PatientID, new_pid) 

log.debug("end cfind_request_wildcards.groovy")
