def scriptName='MWL Imaging Service Morpher - '
log.debug(scriptName + "START")

log.info(scriptName + "Here's how the order message looks:]\n{}", input)
log.info(scriptName + "And here's how the initial Imaging Service Request looks like\n{}", imagingServiceRequest)

imagingServiceRequest.setAccessionNumberNamespaceId("KE")
imagingServiceRequest.setAccessionNumberUniversalId("2.16.124.113638.7.1.1.1")
imagingServiceRequest.setAccessionNumberUniversalIdType("ISO")

def requestedProcedure = imagingServiceRequest.getRequestedProcedureSequence().get(0)
requestedProcedure.setRequestedProcedureID(imagingServiceRequest.getAccessionNumber())
def scheduledProcedureStep = requestedProcedure.getScheduledProcedureStepSequence().get(0)
scheduledProcedureStep.setScheduledProcedureStepIDString(imagingServiceRequest.getAccessionNumber())

if (get("ORC-5") == "CA") {
  scheduledProcedureStep.setScheduledProcedureStepStatus("DISCONTINUED")
}
if (get("ORC-5") == "EX") {
    scheduledProcedureStep.setScheduledProcedureStepStatus("COMPLETED")
}
if (get("ORC-5") == "PR") {
    scheduledProcedureStep.setScheduledProcedureStepStatus("INPROGRESS")
}
if (get("ORC-5") == "SD") {
    scheduledProcedureStep.setScheduledProcedureStepStatus("SCHEDULED")
}

log.debug(scriptName + "setting ConfidentialityConstraintOnPatient...")

def confCode = get("PD1-12")
if (confCode != null) {
    log.debug(scriptName + "PD1-12 = {}", confCode)
    imagingServiceRequest.getPatientDemographics().setConfidentialityConstraintOnPatient(confCode)
} else {
    log.debug(scriptName + "PD1-12 appears to be null...")
}

def confCode2 = get("PV1-16")
if (confCode2 != null) {
    log.debug(scriptName + "PV1-16 = {}", confCode2)
    imagingServiceRequest.getPatientDemographics().setConfidentialityConstraintOnPatient(confCode2)
} else {
    log.debug(scriptName + "PV1-16 appears to be null...")
}


//imagingServiceRequest.getPatientDemographics().setConfidentialityConstraintOnPatient(get("PD1-12"))

log.info(scriptName + "Finished fixing up the Imaging Service Request...Here it is\n{}", imagingServiceRequest)

log.debug(scriptName + "END")
