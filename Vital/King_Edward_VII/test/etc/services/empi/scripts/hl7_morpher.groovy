// This file is used to modify incoming messages
def scriptName = "empi/scripts/hl7_morpher.groovy: "

log.info(scriptName + "START...")

log.debug(scriptName + "input:\n{}", input)

def messageType = get('MSH-9-1')
def triggerEvent = get('MSH-9-2')


def pdConfCode = ''

try { 
    pdConfCode = get('PD1-12')

    if (pdConfCode == null || ''.equals(pdConfCode)) {

        log.debug(scriptName + "PD1-12 appears to be null or empty, will try to use PV1-16 to setup Confidentiality tag...")

        def pvConfCode = get('PV1-16')
        if (pvConfCode != null && !''.equals(pvConfCode))  {
            log.debug(scriptName + "setting PD1-12 segment with a value from PV1-16...")
            set('PD1-12', get('PV1-16'))
        } else {
            log.debug(scriptName + "PV1-16 appear to be null or empty, will NOT set PD1-12 segment...")
        }
    }
} catch(Exception ex) {
    log.warn(scriptName + "Unable to parse PD1-12 segment:", ex);
}

log.debug(scriptName + "output:\n{}", output)

log.info(scriptName + "END")
