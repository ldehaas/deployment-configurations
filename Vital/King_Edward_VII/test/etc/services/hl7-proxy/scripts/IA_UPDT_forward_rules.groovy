def scriptName = "hl7-proxy/scripts/IA_UPDT_forward_rules.groovy: "

log.info(scriptName + "START")

log.debug(scriptName + "input:\n{}", input)

def messageType = get('/.MSH-9-1')
def triggerEvent = get('/.MSH-9-2')

log.info(scriptName + "messageType is: {}", messageType)
log.info(scriptName + "triggerEvent is: {}", triggerEvent)

if (messageType != null && triggerEvent != null) {
    if (("ADT".equals(messageType) && ("A04".equals(triggerEvent) || "A08".equals(triggerEvent) || "A40".equals(triggerEvent) || "A31".equals(triggerEvent) || "A39".equals(triggerEvent))) || ("ORU".equals(messageType) && ("R01".equals(triggerEvent)))) {
        log.info(scriptName + "will forward message to IA Update")
    } else {
        log.info(scriptName + "will NOT forward to IA Update")
        return false
    }
}

def pdConfCode = get('PD1-12')

if (pdConfCode == null || ''.equals(pdConfCode)) {

    log.debug(scriptName + "PD1-12 appears to be null or empty, will try to use PV1-16 to setup Confidentiality tag...")

    def pvConfCode = get('PV1-16')
    if (pvConfCode != null && !''.equals(pvConfCode))  {
        log.debug(scriptName + "setting PD1-12 segment with a value from PV1-16...")
        set('PD1-12', get('PV1-16'))
    } else {
        log.debug(scriptName + "PV1-16 appear to be null or empty, will NOT set PD1-12 segment...")
    }
}

log.debug(scriptName + "output:\n{}", output)

log.info(scriptName + "END")
