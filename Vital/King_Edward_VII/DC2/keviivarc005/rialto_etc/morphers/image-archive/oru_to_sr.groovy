/*
 HL7 ORU to a DICOM SR object
 * many of the HL7 locations are potentially different in the field. this
 * script is simply a starting point.
 */
import org.dcm4che2.util.UIDUtils
import org.joda.time.DateTime

LOAD("common.groovy")

log.debug('starting oru2sr.groovy')

set(SpecificCharacterSet, 'ISO_IR 100')
set(InstanceCreationDate, new DateTime())
set(InstanceCreationDate, new DateTime())
set(InstanceCreatorUID, '1.2.3.4.5.6.7.8.9')
set(Modality, 'SR')
set(SOPClassUID, '1.2.840.10008.5.1.4.1.1.88.11') // basic SR
set(StudyInstanceUID, get('ZDS-1'))
// These UIDs are injected from the Java code.
//set(SeriesInstanceUID, UIDUtils.createUID())
//set(SOPInstanceUID, UIDUtils.createUID())
set(StudyDescription, get('OBR-4-2'))
set(AccessionNumber, get('OBR-3'))
set(StudyID, get('OBR-3'))
set(SeriesDescription, 'Diagnostic Imaging Report')
log.debug('AccessionNumber={}', get('OBR-3'))


def studyDate = DICOMTime.parseDate(get('OBR-7'))
def studyTime = DICOMTime.parseTime(get('OBR-7'))
log.debug('OBR-7={}', get('OBR-7'))
log.debug('studyDate={}', studyDate)
log.debug('studyTime={}', studyTime)

set(StudyDate, studyDate)
set(StudyTime, studyTime)
set(SeriesDate, studyDate)
set(SeriesTime, studyTime)

def contentDate = get('OBR-22') != null ? DICOMTime.parseDate(get('OBR-22')) : studyDate
def contentTime = get('OBR-22') != null ? DICOMTime.parseTime(get('OBR-22')) : studyTime

set(ContentDate, contentDate)
set(ContentTime, contentTime)

set('ReferringPhysicianName', 'OBR-16-2')
log.debug('ReferringPhysicianName={}', get('OBR-16-2'))
set('ReferringPhysicianIdentificationSequence/InstitutionName', get('MSH-4'))
def seq = 'ReferringPhysicianIdentificationSequence/PersonIdentificationCodeSequence';
set(seq+'/CodeValue', get('OBR-16-1'))
set(seq+'/CodingSchemeDesignator', get('MSH-4'))
set(seq+'/CodeMeaning', 'Referring Physician ID')
set(StationName, get('ORC-13'))
set(NameOfPhysiciansReadingStudy, get('OBR-32-2'))
set('AnatomicRegionSequence/CodeValue', get('OBR-15-1-1'))
set('AnatomicRegionSequence/CodingSchemeDesignator', get('OBR-15-1-3'))
set('AnatomicRegionSequence/CodeValue', get('OBR-15-1-2'))
setPersonName(PatientName, 'PID-5')
set(PatientID, get('PID-3-1'))
set(IssuerOfPatientID, 'KE')
set('IssuerOfPatientIDQualifiersSequence/UniversalEntityID', '2.16.124.113638.7.1.1.1')
set('IssuerOfPatientIDQualifiersSequence/UniversalEntityIDType', 'ISO')
set(PatientBirthDate, get('PID-7'))
set(PatientSex, get('PID-8'))
set(InstanceNumber, '1')
set(StudyStatusID, get('OBR-25'))
set(ReasonForStudy, get('ORC-16'))
set(RequestingPhysician, get('ORC-12'))
set(RequestingService, get('MSH-4'))
set('RequestedProcedureCodeSequence/CodeValue', get('OBR-4-1'))
set('RequestedProcedureCodeSequence/CodingSchemeDesignator', 'King Edwards VII')
set('RequestedProcedureCodeSequence/CodeMeaning', get('OBR-4-2'))
//set(RequestedProcedureID, acc_num)
set(ValueType, 'CONTAINER')
set('ConceptNameCodeSequence/CodeValue', '18748-4')
set('ConceptNameCodeSequence/CodingSchemeDesignator', 'LN')
set('ConceptNameCodeSequence/CodeMeaning', 'Diagnostic Imaging Report')
set(ContinuityOfContent, 'CONTINUOUS')
set('VerifyingObserverSequence/VerifyingOrganization', get('MSH-4'))
set('VerifyingObserverSequence/VerificationDateTime', get('OBR-7'))
set('VerifyingObserverSequence/VerifyingObserverName', get('OBR-32-2'))
seq = 'VerifyingObserverSequence/VerifyingObserverIdentificationCodeSequence'
set(seq+'/CodeValue', get('OBR-32-1'))
set(seq+'/CodingSchemeDesignator', get('MSH-4'))
set(seq+'/CodeMeaning', get('OBR-32-2'))
set('ReferencedRequestSequence/AccessionNumber', get('OBR-3'))
set('ReferencedRequestSequence/StudyInstanceUID', get('ZDS-1'))
set('ReferencedRequestSequence/RequestedProcedureDescription', get('OBR-4-2'))
set('ReferencedRequestSequence/RequestedProcedureID', RequestedProcedureID)
set('ReferencedRequestSequence/PlacerOrderNumberProcedure', get('OBR-2'))
set('ReferencedRequestSequence/FillerOrderNumberProcedure', get('OBR-3'))
seq = 'ReferencedRequestSequence/RequestedProcedureCodeSequence'
set(seq+'/CodeValue', get('OBR-4-1'))
set(seq+'/CodeMeaning', get('OBR-4-2'))
set(CompletionFlag, 'COMPLETE')
if (get('OBR-25') == 'F') {
    set(VerificationFlag, 'VERIFIED')
} else {
    set(VerificationFlag, 'UNVERIFIED')
}
set('ContentSequence[1]/RelationshipType', 'HAS CONCEPT MOD')
set('ContentSequence[1]/ValueType', 'CODE')
set('ContentSequence[1]/ConceptNameCodeSequence/CodeValue', '121058')
set('ContentSequence[1]/ConceptNameCodeSequence/CodingSchemeDesignator', 'DCM')
set('ContentSequence[1]/ConceptNameCodeSequence/CodeMeaning', 'Procedure Reported')
set('ContentSequence[1]/ConceptCodeSequence/CodeValue', get('OBR-4-1'))
set('ContentSequence[1]/ConceptCodeSequence/CodingSchemeDesignator', 'RP')
set('ContentSequence[1]/ConceptCodeSequence/CodeMeaning', get('OBR-4-2'))
set('ContentSequence[2]/RelationshipType', 'HAS CONCEPT MOD')
set('ContentSequence[2]/ValueType', 'CODE')
set('ContentSequence[2]/ConceptNameCodeSequence/CodeValue', '121049')
set('ContentSequence[2]/ConceptNameCodeSequence/CodingSchemeDesignator', 'DCM')
set('ContentSequence[2]/ConceptNameCodeSequence/CodeMeaning', 'Language of Content Item and Descendants')
set('ContentSequence[2]/ConceptCodeSequence/CodeValue', 'en') // or fr?
set('ContentSequence[2]/ConceptCodeSequence/CodingSchemeDesignator', 'RFC3066')
set('ContentSequence[2]/ConceptCodeSequence/CodeMeaning', 'English') // or french
 
set('ContentSequence[3]/RelationshipType', 'HAS OBS CONTEXT')
set('ContentSequence[3]/ValueType', 'CODE')
set('ContentSequence[3]/ConceptNameCodeSequence/CodeValue', '121005')
set('ContentSequence[3]/ConceptNameCodeSequence/CodingSchemeDesignator', 'DCM')
set('ContentSequence[3]/ConceptNameCodeSequence/CodeMeaning', 'Observer Type')
set('ContentSequence[3]/ConceptCodeSequence/CodeValue', '121006')
set('ContentSequence[3]/ConceptCodeSequence/CodingSchemeDesignator', 'DCM')
set('ContentSequence[3]/ConceptCodeSequence/CodeMeaning', 'Person')
set('ContentSequence[4]/RelationshipType', 'HAS OBS CONTEXT')
set('ContentSequence[4]/ValueType', 'CODE')
set('ContentSequence[4]/ConceptNameCodeSequence/CodeValue', '121011')
set('ContentSequence[4]/ConceptNameCodeSequence/CodingSchemeDesignator', 'DCM')
set('ContentSequence[4]/ConceptNameCodeSequence/CodeMeaning', 'Person Observer\'s Role in this Procedure')
set('ContentSequence[4]/ConceptCodeSequence/CodeValue', '121097')
set('ContentSequence[4]/ConceptCodeSequence/CodingSchemeDesignator', 'DCM')
set('ContentSequence[4]/ConceptCodeSequence/CodeMeaning', 'Recording')
 
set('ContentSequence[5]/RelationshipType', 'CONTAINS')
set('ContentSequence[5]/ValueType', 'CONTAINER')
set('ContentSequence[5]/ConceptNameCodeSequence/CodeValue', '121064')
set('ContentSequence[5]/ConceptNameCodeSequence/CodingSchemeDesignator', 'DCM')
set('ContentSequence[5]/ConceptNameCodeSequence/CodeMeaning', 'Current Procedure Description')
set('ContentSequence[5]/ContinuityOfContent', 'CONTINUOUS')
seq = 'ContentSequence[5]/ContentSequence'
set(seq+'/RelationshipType', 'CONTAINS')
set(seq+'/ValueType', 'TEXT')
set(seq+'/ConceptNameCodeSequence/CodeValue', '121065')
set(seq+'/ConceptNameCodeSequence/CodingSchemeDesignator', 'DCM')
set(seq+'/ConceptNameCodeSequence/CodeMeaning', 'Procedure Description')
set(seq+'/TextValue', get('OBR-4-1') + '^' + get('OBR-4-2'))
set('ContentSequence[6]/RelationshipType', 'CONTAINS')
set('ContentSequence[6]/ValueType', 'CONTAINER')
set('ContentSequence[6]/ConceptNameCodeSequence/CodeValue', '121070')
set('ContentSequence[6]/ConceptNameCodeSequence/CodingSchemeDesignator', 'DCM')
set('ContentSequence[6]/ConceptNameCodeSequence/CodeMeaning', 'Findings')

set('ContentSequence[6]/ContinuityOfContent', 'CONTINUOUS')

seq = 'ContentSequence[6]/ContentSequence'
set(seq+'/ValueType', 'TEXT')

set(seq+'/RelationshipType', 'CONTAINS')

set(seq+'/ConceptNameCodeSequence/CodeValue', '121071')
set(seq+'/ConceptNameCodeSequence/CodingSchemeDesignator', 'DCM')
set(seq+'/ConceptNameCodeSequence/CodeMeaning', 'Finding')
def observations = getList('ORCOBRNTEOBXNTECTI(*)/OBXNTE/OBX-5(*)')
def validObservations = observations.findAll{ it != null }
set(seq+'/TextValue', validObservations.join('\n'))
log.debug("OBSERVATIONS: {}", validObservations)
log.debug("OBS: {}", observations)

log.debug('ending oru2sr.groovy')

