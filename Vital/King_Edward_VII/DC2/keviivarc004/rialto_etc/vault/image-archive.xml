<config>

    <!-- Image Archive -->
    <!-- Most functionality on the same DICOM port as router, but CStore on secondary -->
    <service id="imagingArchive" type="image-archive">
        <server idref="ia-store-dicom"  name="CStoreSCP" />
        <server idref="main-dicom"      name="StgCmtSCP" />
        <server idref="main-dicom"      name="CFindSCP" />
        <server idref="main-dicom"      name="CMoveSCP" />
        <server idref="ia-update-hl7"   name="update" />
        <server idref="ia-update-hl7"   name="merge" />
        <server idref="ia-update-hl7"   name="order" />
	<server idref="ia-update-hl7"   name="oru" />

        <server idref="http8080" name="MINT">
            <url>/vault/mint/*</url>
        </server>

        <server idref="http8080" name="ILM">
            <url>/vault/ilm/*</url>
        </server>

        <server idref="http8080" name="QC">
            <url>/vault/qc/*</url>
        </server>

        <server idref="http8080" name="WADO">
            <url>/vault/wado</url>
        </server>

        <server idref="http8080" name="StudyManagement">
            <url>${IA-StudyManagement-Path}/*</url>
        </server>

        <!-- Study Validation web service
        <server idref="http8080" name="StudyValidation">
            <url>/vault/utils/*</url>
        </server>
        -->

        <device idref="xdsrep" />
        <device idref="xdsreg" />
        <device idref="pix" />
        <device idref="pdq" />
        <!-- <device idref="pif" /> -->
        <device idref="WorkflowEngine" />

        <config>

            <prop name="StorerType" value="FILE" />
            <prop name="Cache" value="${rialto.rootdir}/var/ids1/cache" />
            <prop name="Inbox" value="${rialto.rootdir}/var/ids1/index" />
            <prop name="JobQueue" value="${rialto.rootdir}/var/ids1/jobs" />
            <prop name="QCWorkDirectory" value="${rialto.rootdir}/var/ids1/qc" />
            <prop name="CacheSize" value="10" />
            <prop name="AETitle" value="${IA-AETitle}" />

            <!-- DefaultDomain:  For incoming studies, if Issuer of Patient ID (0010,0021) is empty, the
                 study is assumed to be from this domain.  Format: namespace&domainUID&domainUIDType -->
            <prop name="DefaultDomain" value="${System-DefaultLocalFullyQualifiedDomain}" />
            <prop name="AffinityDomain" value="${System-AffinityFullyQualifiedDomain}" />

            <prop name="IndexerType" value="CQL3" />
            <prop name="IndexerRetryTime" value="1m" />

<!--
            <prop name="ImagingLifeCycleManagementConfiguration">
                <Mode>ARCHIVE</Mode>
                <StudyStabilityTime>1m</StudyStabilityTime>
                <FCMDSleepTime>1m</FCMDSleepTime>
                <Policy>${rialto.rootdir}/etc/morphers/image-archive/ilm_policy.groovy</Policy>
            </prop>
-->

            <prop name="CassandraConfiguration">
                <clusterHosts>${CassandraClusterHosts}</clusterHosts>
                <clusterDatacenterName>${CassandraClusterDatacenterName}</clusterDatacenterName>
                <keyspaceName>${CassandraKeyspacePrefix}imagearchive</keyspaceName>
                <keyspaceReplicationStrategy>NetworkTopologyStrategy</keyspaceReplicationStrategy>
                <keyspaceReplicationStrategyOptions>${CassandraReplicationOption}</keyspaceReplicationStrategyOptions>
                <consistencyLevel>LOCAL_QUORUM</consistencyLevel>
                <serialConsistencyLevel>LOCAL_SERIAL</serialConsistencyLevel>
                <keyspaceCreateEnabled>true</keyspaceCreateEnabled>
                <dynamicConsistencyLevelEnabled>false</dynamicConsistencyLevelEnabled>
                <schemaUpdateEnabled>false</schemaUpdateEnabled>
            </prop>

            <prop name="DirectAccess" value="true" />
            <prop name="MintEnabled" value="true" />

            <prop name="FileSystemStorageConfiguration">

                <FileSystemStorageLocation>
                   <AbsoluteBasePath>${rialto.rootdir}/var/ids1/archive</AbsoluteBasePath>
                   <ReadOnly>false</ReadOnly>
                   <SourceDataCenter>DC2</SourceDataCenter>
                </FileSystemStorageLocation>

                <FileSystemStorageLocation>
                   <AbsoluteBasePath>${rialto.rootdir}/var/ids1/archive</AbsoluteBasePath>
                   <ReadOnly>false</ReadOnly>
                   <SourceDataCenter>DC1</SourceDataCenter>
                </FileSystemStorageLocation>

            </prop>

            <prop name="MWLReconciliationEnabled">true</prop>
            <prop name="QcToolsReconciliationScript">${rialto.rootdir}/etc/morphers/image-archive/qcreconciliation.groovy</prop>
            <prop name="ReconciliationScript">${rialto.rootdir}/etc/morphers/image-archive/qcreconciliation.groovy</prop>

            <prop name="PatientNameInHL7Location" value="/.PID-5" />

            <!-- Manifest Publish Configuration -->
            <prop name="PublisherType" value="DISCARD" />
            <prop name="MetadataStudyInstanceUIDKey">studyInstanceUid</prop>
            <prop name="HealthCareFacilityCode" value="HealthCareFacilityCodeValue, HealthCareFaciltiyCodeScheme, HealthCareFaciltiyCodeDisplay" />
            <prop name="PracticeSettingCode" value="PracticeSettingCodeValue, PracticeSettingCodeScheme, PracticeSettingCodeSchemeDisplay" />
            <prop name="ClassCode" value="ClassCodeValue, ClassCodeScheme, ClassCodeDisplay" />
            <prop name="TypeCode" value="TypeCodeValue, TypeCodeScheme,TypeCodeDisplay" />
            <prop name="ContentTypeCode" value="ContentTypeCodeValue, ContentTypeCodeScheme, ContentTypeCodeDisplay" />
            <prop name="ConfidentialityCode" value="ConfidentialityCodeValue, ConfidentialityCodeScheme,ConfidentialityCodeDisplay" />
            <!-- Manifest metadata is now determined from the groovy script -->
            <prop name="DocumentMetadataMorpher">${rialto.rootdir}/etc/morphers/image-archive/document_metadata_morpher.groovy</prop>

            <prop name="PatientIdentityFeedMorpher">${rialto.rootdir}/etc/morphers/image-archive/pif_morpher.groovy</prop>

            <prop name="OruToSrMorpher">${rialto.rootdir}/etc/morphers/image-archive/oru_to_sr.groovy</prop>

            <prop name="TagMorphers">
                <script direction="IN" file="${rialto.rootdir}/etc/morphers/router/in.groovy">
                    <callingAE>*</callingAE>
                    <calledAE>*</calledAE>
                </script>
                <script direction="CFIND_REQUEST" file="${rialto.rootdir}/etc/morphers/image-archive/cfind_req.groovy">
                    <callingAE>*</callingAE>
                    <calledAE>*</calledAE>
                </script>
            </prop>

            <!-- Study Content Notification -->
            <!-- Note: When configuring notification destinations, please ensure the receivingApplication
                 and receivingFacility match up with a configured device -->
            <prop name="StudyContentNotifierType" value="DISCARD" />

            <prop name="UpdatePatientDemographicsOnIngestion">true</prop>

            <prop name="VIPConfidentialityCode" value="3" />

            <prop name="FullAccessAE" value="VIPAETitle" />

            <prop name="WorkflowCreatorType" value="BASIC" />

            <prop name="WorkflowSOPFilterScript" value="${rialto.rootdir}/etc/morphers/vault/workflow_sop_filter.groovy" />

            <prop name="WorkflowTemplateCreatorScript" value="${rialto.rootdir}/etc/morphers/vault/workflow_creator.groovy" />

            <!--Fix for CAPA-19 per KHC-8427 -->
            <prop name="CStoreSCPTransferCapabilities">
                <disable>
                    <transferSyntax uid="1.2.840.10008.1.2.2" />
                </disable>
            </prop>

<!--
            <prop name="NotificationDestinations">
                <destination name="TomAndJerry">
                    <receivingApplication>THOMAS</receivingApplication>
                    <receivingFacility>JERRY</receivingFacility>
                    <morphingScript>${rialto.rootdir}/etc/morphers/image-archive/daffy.groovy</morphingScript>
                </destination>
            </prop>
-->
        </config>
    </service>

</config>
