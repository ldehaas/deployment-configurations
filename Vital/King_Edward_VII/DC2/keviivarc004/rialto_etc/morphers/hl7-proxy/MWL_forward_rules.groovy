log.info("MWL_forward_rules START")

def messageType = get('/.MSH-9-1')
def triggerEvent = get('/.MSH-9-2')

log.info("messageType is: {}", messageType)
log.info("triggerEvent is: {}", triggerEvent)

if (messageType != null && triggerEvent != null) {
    if (["ADT^A04", "ADT^A08", "ADT^A31", "ADT^A39", "ADT^A40", "ORM^O01"].contains(messageType + "^" + triggerEvent)) {
        log.info("MWL_forward_rules - forward to MWL")
    }
    else {
        log.info("MWL_forward_rules - do not forward to MWL")
        return false
    }
}

log.info("MWL_forward_rules FALSE")
