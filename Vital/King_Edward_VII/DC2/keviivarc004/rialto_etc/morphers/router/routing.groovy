//return "etc/dataflows/pass-through-and-compress.xml"
//return "etc/dataflows/pass-through.xml"

def modality=get("00080060")

if (modality.toUpperCase().contains("CT")) {
    return "etc/dataflows/vims-and-vioarchive.xml"
}
else {
    return "etc/dataflows/vioarchive-only.xml"
}

//return "etc/dataflows/decompress.xml"
//return "etc/dataflows/multiple-destinations.xml"
