log.info("Start EMPI Morpher")

def messageType = get('/.MSH-9-1')
def triggerEvent = get('/.MSH-9-2')

log.info("messageType is: {}", messageType)
log.info("triggerEvent is: {}", triggerEvent)

if (!"ADT".equalsIgnoreCase(messageType)) {
    log.debug('EMPI Morpher - An unactionable message was provided, taking no action on message {}^{}', messageType, triggerEvent);
    return false
}

log.info("End EMPI Morpher")
