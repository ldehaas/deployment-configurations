log.debug("in.groovy: Start inbound morpher")

def issuerOfPatientId = get(IssuerOfPatientID)
def universalEntityID = get("IssuerOfPatientIDQualifiersSequence/UniversalEntityID")
def universalEntityIDType = get("IssuerOfPatientIDQualifiersSequence/UniversalEntityIDType")

log.debug("in.groovy: IssuerOfPatientId is {}", issuerOfPatientId)
log.debug("in.groovy: universalEntityID is {}", universalEntityID)
log.debug("in.groovy: universalEntityIDType is {}", universalEntityIDType)

set(IssuerOfPatientID, 'KE')
set("IssuerOfPatientIDQualifiersSequence/UniversalEntityID", '2.16.124.113638.7.1.1.1')
set("IssuerOfPatientIDQualifiersSequence/UniversalEntityIDType", 'ISO')

def patientId = get(PatientID)
def focusPatients = [ "8015353", "8008158", "7001501", "8026649", "7000401", "5001859", "8051068", "8043826", "8008382",
                        "2001547", "2001379", "5002843", "0003246", "6000714", "8014610", "2002563", "8071120", "8073473" ]

if (focusPatients.contains(patientId)) {
    log.debug("in.groovy: patientid {} - 0x00401008 set to 3", patientId)
    set(0x00401008, "3")
}

log.debug("in.groovy: End inbound morpher")
