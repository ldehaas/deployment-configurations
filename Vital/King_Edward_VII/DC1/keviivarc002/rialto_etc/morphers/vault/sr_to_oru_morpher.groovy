/* SR to ORU Morpher
*
*   (Shared in both the Ad-Hoc Query and Fetch Prior Studies workflow)
*
*   This script creates an ORU report message from a DICOM Structured Report.  
*       Some PACS (HMI PACS in particular) have Ad Hoc deployments of a reporting system, which needs reports as an HL7 ORU.
* 
*       This script is optional.
*       It is optional.  If not configured, SRs will be passed directly to the PACS.
*       TODO Is this true??  What about the SOP count??  If configured, the SRs will not be delivered as SOPs, but the generated ORU will be.
*       TODO if so, what would happen to an SR if the script returns false?
*
*       NOTE:  The input Structured Report will have already been localized by the foreign image localizer script.
*
*   Currently, for HSC, the script is doing the following:
*       1)
*       2)
*   
*   TODO  look fot the different TODO throughout the script
*/

def scriptName = "SR to ORU morpher - "

/* ========= Shared Files Loading ============= */

 /* Load the shared methods that allow:
        a) Using either a Patient ID Local to the PACS, or a prefixed foreign Patient when the Patient does not exist in the PACS
        b) The Procedure Codes tables to normalize the hanging protocols at the local PACS      */
LOAD("/home/rialto/rialto/etc/morphers/common/common_localization_routines.groovy")

/* =========  Constant Declaration  ================ */
 def verifyingObserverNameSplittingStrategy = "commaOnly"
 // Possible options: commaOnly, commaFirst, caretOnly, caretFirst

/* ======== Worker Functions ========== */
/**
 * Some SRs that we have seen in production have invalid escape sequences in
 * their report content.  For example, the string "\X09\" instead of a tab
 * character.  This causes visual problems in the reports as displayed by
 * PACSs so we should replace them with what is likely the correct character...
 */
def replaceInvalidEscapeSequences(content) {

    content = content.replaceAll("\\\\X(\\p{XDigit}{2})\\\\") {
        (char) Integer.parseInt(it[1], 16)
    }

    // now replace all carriage returns with newlines, since carriage
    // returns get escaped by us and then usually misinterpreted by a PACS
    return content.replaceAll("\r", "\n");
}



/* ================  Script  ================  */

 /* Create a new, blank ORU message with the Default MSH segment codes specified in the SAD   */
 // TODO check if the whole MSH segment is being correctly generated
 initialize("ORU", "R01", "2.3")


 // Explicitly setting the Char Set for ORUs that don't have them, assuming ISO IR 100
 // This is to change the defaul Char set mask that the message has to evaluate non-ASCII characters
 //  not setting this could result in garbled characters
 log.debug(scriptName + "Study Description code points (BEFORE setting specific char set): {}", get(StudyDescription).collect { it.codePointAt(0) })
 if (input.get(SpecificCharacterSet) == null) {
    input.set(SpecificCharacterSet, "ISO_IR 100")
 }
 log.debug(scriptName + "Study Description code points (AFTER setting specific char set): {}", get(StudyDescription).collect { it.codePointAt(0) })


 // [debug] Printing Tracing Information
 log.debug(scriptName + "\n\n\n*** Starting DICOM SR to HL7 ORU Report Conversion Script *** :\n")
 log.debug(scriptName + "This is the object received from the DICOM Study (after modifying char set):\n {}", input)

 // Setting Character Set supported by French HMI PACS
 set('MSH-18', '8859/1')

 // TODO  Why are we needing to localize if the incoming object is supposedly localized already??  Should test with this part of the code turned off.
 // Adding localization of PatientID for the ORU - Rodrigo
 def sourcePID = get(PatientID)

 /*  MIGHT GET AWAY WITH BLOWING AWAY FROM HERE  */
 def sourceIssuer = get(IssuerOfPatientID)
 def localPid = Pids.localize([sourcePID, sourceIssuer], getAllPidsQualified())
        // "localPid" will get an array of Patient IDs, whose array value 0 will always be the local patient, or a prefixed foreign patient
        // "Pids" is a variable obtained from common_localization_routines.groovy
        // "localize" is a method that returns either a local patient or a prefixed foreign patient

 // TODO Now I understand. The above code was a check for Sam's bug fix.  I assume he wasn't localizing before passing to this script, so I
 //     hacked it manually.  If it works, blow this whole code segment away.  If it doesn't, get Jeremy to fix it, and
 //     in the meantime prefix the Accession number too by taking the code from the lcoalizer morpher.
 log.debug(scriptName + "Checking the Patient Number with old method: {}", localPid[0])
 log.debug(scriptName + "Patient with the new method: {}", get(PatientID))
 /*  TO HERE!! */


 /* Set the expected PID Segments */
  //TODO def ramq = get()
  
  set("PID-1", "1")

  // Set the localized Patient ID in the ORU Report
  if(null != sourcePID) {
        set('PID-3', sourcePID)
  } else {
        log.error(scriptName + "The Patient Localization failed for SR to ORU Creation!!  The localized Patient is null")
        throw new Exception("The Patient Localization failed for ORU Creation!!  The localized Patient is null")
  }

  // Set the name components in PID-5 and PID-6
  /**
   * Copies up to three components of a name from the SR into the hl7 message.
   * DICOM person name elements are supposed to be of the form
   * family^given^middle^prefix^suffix.  HL7 names are sometimes of the form
   * family^given^middle^suffix^prefix but some components are actually
   * id^family^given^...
   *
   * In the case where the id is there, pass startingComponent=2 to offset where
   * we put the name.  However, in our sample SRs, the values of the person names
   * actually include the id (they are in DICOM formath instead of HL7).  In this
   * case, just leave the default startingComponent.
   *
   * @param dicomTag place to get name from in SR
   * @param hl7prefix place to put name in hl7
   * @param startingComponent if name doesn't line up at beginning of
   * @return
   */
   // TODO Check how the names actually are in DICOM

   def setName(dicomTag, hl7prefix, startingComponent=1) {
        nameParts = split(dicomTag)
        if (null == nameParts) {
        return
        }

        // loop far enough to get the id if present, plus the family and given names
        int maxPartsToCopy = 3

        for (int i = 0; i < maxPartsToCopy && i < nameParts.size(); i++) {
            set(hl7prefix + "-" + (startingComponent++), nameParts[i])
        }
   }

  // Set the Patient Name with up to 3 components from DICOM
  setName(PatientName, "PID-5")

  // Set Mother's Maiden Name, if present in the DICOM SR
  set("PID-6", get(PatientMotherBirthName))

  set("PID-7", get(PatientBirthDate))
  
  set("PID-8", get(PatientSex))
  
  set("PID-11", get(PatientAddress))

  set("PID-13", get(PatientTelephoneNumbers))



 /* Set the expected ORC Segments */
 set("ORC-1", "RE")

 set("ORC-3", get(AccessionNumber))



 /* Set the expected OBR Segments */
 set("OBR-1", "1")

 set("OBR-3", get(AccessionNumber))

 set("OBR-4-1", get(CodeValue))
    // This is the Top-Level CodeValue
    // This is currently being populated as the Normalizaed Procedure Code in the Foreign Image Localizer script

 set("OBR-4-2", get(StudyDescription))

 set("OBR-4-4", get(AccessoryCode))
    // Foreign Procedure Code

 set("OBR-4-6", get(AcquisitionComments))
    // Foreign Procedure Description

// set("OBR-4", get(StudyDescription))



 /* Handle the Report date and time correctly */

 def fullReportDateTimeinSTUDYdateTime = false
 def fullReportDateTimeinCONENTDateTime = false
 def reportDate = null
 def reportTime = null
 def studyDate = get(StudyDate)
 def studyTime = get(StudyTime)
 def contentDate = get(ContentDate)
 def contentTime = get(ContentTime)


 if((null != studyDate) && (null != studyTime)) {
 // Check for best-case scenarios first

 // neither of the Study Date/Times are null
    if((! studyDate.isEmpty() ) && ( ! studyTime.isEmpty() ) ) {
    // both not null; both populated  -- Best case scenario
        reportDate = studyDate
        reportTime = studyTime
        fullReportDateTimeinSTUDYdateTime = true

    }

 } else if( (null != contentDate ) && (null != contentTime ) ) {
 // neither of the Cotent Date/Times are null

    if((! contentDate.isEmpty() ) && ( ! contentTime.isEmpty() ) ) {
    // both not null; both populated  -- 2nd Best case scenario
        reportDate = contentDate
        reportTime = contentTime
        fullReportDateTimeinCONENTDateTime = true
        log.debug(scriptName + "Study Date and Time incomplete or blank.  Using Content Date and Time to populate HL7 Observation Date and Time")
    }
 }

 if(fullReportDateTimeinSTUDYdateTime == true || fullReportDateTimeinCONENTDateTime == true) {
 // set the Obs. date and time if coming from a single source
    set("OBR-7", reportDate + reportTime)

 } else {
 // Salvage the date and times 


    // Salvage date and times from available sources

    if((null != studyDate) && (! studyDate.isEmpty() ) ) {
    // Study Date is valid.  Takes priority in salvaging
        reportDate = studyDate 

    } else if((null != contentDate) && (! contentDate.isEmpty() ) ) { 
        reportDate = contentDate   
    }

    if((null != studyTime) && (! studyTime.isEmpty() ) ) {
    // Study Time is valid.  Takes priority
        reportTime = studyTime

    } else if((null != contentTime) && (! contentTime.isEmpty() ) ) {
        reportTime = contentTime
    }

    // Set Obs. Date / Time from salvaged fields 

    if(reportDate != null && reportTime != null) {
        log.warn(scriptName + "WARNING! - Setting HL7 ORU Observation Date and Time from a composite source.")
        set("OBR-7", reportDate + reportTime)

    } else if(reportDate == null) {
        log.warn(scriptName + "Report Date is non-existent.  Leaving HL7 ORU Observation Date and Time blank!!")
        // A report with a time and no date does not make much sense, and it confuses the sorting mechanism downstream
        //  The report body is still passed.  Not interrupting the conversion.
    }

 }



 // Populate the Anatomic Region
 anatomicRegionSeq = get(AnatomicRegionSequence)
 if (!anatomicRegionSeq.isEmpty()) {
    anatomicRegion = anatomicRegionSeq.first()
    set("OBR-15-1-1", anatomicRegion.get(CodeValue))
    set("OBR-15-1-2", anatomicRegion.get(CodeMeaning))
    set("OBR-15-1-3", anatomicRegion.get(CodingSchemeDesignator))
 }

 // our sample SR includes the physician id (non-standard)
 setName(ReferringPhysicianName, "OBR-16")

 set("OBR-19", get(AccessionNumber))

 set("OBR-20", get(StudyID))

 set("OBR-22", get(ContentDate) + get(ContentTime))

 set("OBR-24", get(ModalitiesInStudy))
 log.debug(scriptName + "The modality of this report is [{}]", get(Modality))
 log.debug(scriptName + "The Modalities of this Study are [{}]", get(Modality))

// set("OBR-24", get(Modality))

completionFlag = get(CompletionFlag)
log.debug(scriptName + "CompletionFlag = {}", completionFlag)

 //performedProcedureSeq = get(PerformedProcedureCodeSequence)
 //if (!performedProcedureSeq.isEmpty()) {
    //performedProcedure = performedProcedureSeq.first()
    //completionFlag = performedProcedure.get(CompletionFlag)
 //}

 //set("OBR-25", completionFlag)
if ( completionFlag != null ) {
    if ("PARTIAL".equals(completionFlag.toUpperCase()) ) {
        log.debug(scriptName + "CompletionFlag is set to {},  setting OBR-25 to P (Provisional)", completionFlag)
        set("OBR-25", "P") 
    }
    else if ("COMPLETE".equals(completionFlag.toUpperCase()) ) {
        log.debug(scriptName + "CompletionFlag is set to {},  setting OBR-25 to F (Final)", completionFlag)
        set("OBR-25", "F") 
    }
    else {
        log.debug(scriptName + "CompletionFlag is set to {},  setting OBR-25 to P (Provisional)", completionFlag)
        set("OBR-25", "P")     
    }
}
else {
    log.debug(scriptName + "CompletionFlag appears to be null, setting OBR-25 to P (Provisional)")
    set("OBR-25", "P")
}

// PredecessorDocumentsSequence (0040,A360)
predecessorSeq = get(PredecessorDocumentsSequence)

def predecessorSeqSize = 0
if ( !predecessorSeq.isEmpty() ) {
    predecessorSeqSize = predecessorSeq.size()
    log.debug(scriptName + "PredecessorDocumentsSequence size is: {}", predecessorSeqSize)
}
else {
    log.debug(scriptName + "PredecessorDocumentsSequence appears to be empty...")
}

if (predecessorSeqSize > 0 && "COMPLETE".equals(completionFlag.toUpperCase()) ) {
//if (predecessorSeqSize > 0 ) {
    log.debug(scriptName + "PredecessorDocumentsSequence reffers to another SR, setting OBR-25 to A (Addendum)")
    set("OBR-25", "A")
}

 // Setting the Study Date to populate the Scheduled date / time - Same as the ORM - if not present use content date/time
 def timeBack = null
 if((null != get(StudyDate)) && (null != get(StudyTime))) {
    timeBack =  get(StudyDate) + get(StudyTime)
    set("OBR-27-4", timeBack)
 } else if ((null != get(ContentDate)) && (null !=get (ContentTime))) {
    timeBack = get(ContentDate) + get(ContentTime)
    set("OBR-27-4",timeBack)
 } else {
    log.warn(scriptName + "SR to ORU Conversion: Leaving OBR-27 -Quantity/Timing- empty.  No appropriate time source available")
 }
 log.debug(scriptName + "SR to ORU Workflow: Setting the OBR-27-4 TO {}", get(StudyDate))


 //Setting Verifying Observer ID and Names (Lastname,Firstname)into OBR-32 sub-segments

    def verifyingObserverName = null
    def vObsIdCode = null
    def verifyingObserverSeq = get(VerifyingObserverSequence)

    if(null != verifyingObserverSeq){

        if (! verifyingObserverSeq.isEmpty() ) {

            def verifyingObserver = verifyingObserverSeq.first()
            verifyingObserverName = verifyingObserver.get(VerifyingObserverName)    // Have to use a get method in a DICOM Sequence
            log.debug(scriptName + "Verifying Observer Name as received from the DIR in this SR: [{}]", verifyingObserverName)

            def verifyingObserverIDCodeSeq = verifyingObserver.get(VerifyingObserverIdentificationCodeSequence)
            if(!verifyingObserverIDCodeSeq.isEmpty()) {
                def vOIDSeq = verifyingObserverIDCodeSeq.first()
                vObsIdCode = vOIDSeq.get(CodeValue)
                log.debug(scriptName + "Verifying Observer ID Code as received from DIR is [{}]", vObsIdCode)
            }
        }
    }



 //Setting Verifying Observer ID

    if(null != vObsIdCode) {
        set("OBR-32-1", vObsIdCode)
    }


    if(null != verifyingObserverName) {
        if(! verifyingObserverName.isEmpty() ) {
            log.debug(scriptName + "\n\n Rechecking what the Verifying Observer Name is \"{}\"", verifyingObserverName)

            // Split the name into the expected values
            verifyingObserverName = Demographics.parseName(verifyingObserverName)

            // Use the comma format expected at the Report Reader (RADReport)
            verifyingObserverName = verifyingObserverName.join(", ")
        }
    }

    //Set the verifying observer name in the correct place
    log.debug(scriptName + "This is the final verifying observer name (Report Author??) to be used: [{}]", verifyingObserverName)
    set("OBR-32-2", verifyingObserverName)

    //TODO just for debugging
    if(verifyingObserverName instanceof String){
        log.debug(scriptName + "verifyingObserverName is a String")
    } else if(verifyingObserverName instanceof List){
        log.debug(scriptName + "verifyingObserverName is a List")
    } else if(verifyingObserverName instanceof Collection){
        log.debug(scriptName + "verifyingObserverName is a Collection")
    } else {
        log.debug(scriptName + "verifyingObserverName is nothing that's expected")
    }
/*
 if(null != verifyingObserverName) {

    if(! verifyingObserverName.isEmpty() ) {
        log.debug(scriptName + "\n\n Rechecking what the Verifying Observer Name is \"{}\"", verifyingObserverName)

        // Split the Verifying Observer Name according to the site-specific scheme
        //TODO Only commaOnly and caretFirst implemented so far
        if(verifyingObserverNameSplittingStrategy == "commaOnly"){

            if(verifyingObserverName.contains(',')) {
                verifyingObserverNameArray = verifyingObserverName.split(',')
            } else {
                log.warn(scriptName + "Verifying Observer Name does not contain a caret (^) nor a comma (,)")
                useNameAsPassed = true
            }


        } else if(verifyingObserverNameSplittingStrategy == "caretFirst"){
            if(verifyingObserverName.contains('^')) {  
                verifyingObserverNameArray = verifyingObserverName.split('\\^')
            } else if(verifyingObserverName.contains(',')) {
                verifyingObserverNameArray = verifyingObserverName.split(',')
            } else {
                log.debug(scriptName + "Verifying Observer Name does not contain a caret (^) nor a comma (,)")
                useNameAsPassed = true
            }
        }

        // Error-checking logic
        if(null != verifyingObserverNameArray ) {
            def arrayLength = verifyingObserverNameArray.length
            log.debug(scriptName + "Verifying Observer *Name* Array (i.e. how many strings) length is \"{}\"", arrayLength)

            if(2 >= arrayLength) {
                log.debug(scriptName + "Length of Verifying Observer Name is 2 or more strings")
                finalverifyingObserverName = verifyingObserverName[0]+","+verifyingObserverName[1]
            } else if (1 >= arrayLength) {
                log.debug(scriptName + "Length of Verifying Observer Name is 1 string")
                finalverifyingObserverName = verifyingObserverName[0]+","
            } else {
                log.debug(scriptName + "Sanity Check: Length of Verifying Observer Name Array is less than 1")
                finalverifyingObserverName = null
            }
        } else if(useNameAsPassed) {
            finalverifyingObserverName = verifyingObserverName
            log.debug(scriptName + "Non-delimited name passed.  Will write the original DICOM Verifying Observer Name in HL7 OBR-32-2")

        } else {
            log.debug(scriptName + "Length of Verifying Observer Name is NULL")
            finalverifyingObserverName = null
        }

        log.debug(scriptName + "This is the final verifying observer name (Report Author??) to be used: [{}]", finalverifyingObserverName)
        set("OBR-32-2", finalverifyingObserverName)
    }
 } else {
    log.debug(scriptName + "No verifing observer name was passed from the SR.  Leaving it blank")
 }
*/

 /* Populate the Expected OBX Segments */

 // Preliminary setup
 def gatherTextContent(contentSeq, output) {
    contentSeq.each { content ->
        if ("TEXT".equals(content.get(ValueType))) {
            meaning = null
            conceptNameCodeSeq = content.get(ConceptNameCodeSequence)
            if (!conceptNameCodeSeq.isEmpty()) {
                meaning = conceptNameCodeSeq.first().get(CodeMeaning)
            }

            output.add([meaning, content.get(TextValue)])

        } else if ("CONTAINER".equals(content.get(ValueType))) {
            gatherTextContent(content.get(ContentSequence), output)
        }
    }
 }
 reportTextItems = []
 gatherTextContent(get(ContentSequence), reportTextItems)

 // Set OBX Segments
 set("OBX-1", "1")

 set("OBX-2", "TX")

 // Set actual Report contents in OBX-5
 textRepetition = 0
 reportTextItems.each { reportTextItem ->
    title = reportTextItem[0] == null ? "___" : reportTextItem[0].toUpperCase()
    text = replaceInvalidEscapeSequences(reportTextItem[1])

    set("OBX-5(" + textRepetition++ + ")", title)
    // blank line after title
    set("OBX-5(" + textRepetition++ + ")", "")

    text.split("\n").each { line ->
        set("OBX-5(" + textRepetition++ + ")", line.trim())
    }

    // 2 blank lines after each section
    set("OBX-5(" + textRepetition++ + ")", "")
    set("OBX-5(" + textRepetition++ + ")", "")
 }

 def verificationFlag = get(VerificationFlag)
 log.debug(scriptName + "VerificationFlag = {}", verificationFlag)
 if("VERIFIED" == verificationFlag) {
    verificationFlag = "F"
 } else if("UNVERIFIED" == verificationFlag) {
    verificationFlag = "P"
 }


 // McKesson needs this for their PACS report-displaying mechanism
 /* "The current logic is setup where ‘F’ and ‘A’ in OBX-11 will be reported and everything else is transcribed." */
 set("OBX-11", verificationFlag)
 log.debug(scriptName + "DICOM SR to HL7 ORU Report creation: Setting Verification Flag in OBX-11 as {}", verificationFlag)


 set("OBX-14", get(ContentDate) + get(ContentTime))
 // TODO: split name and use subcomponents of OBX-16, starting at OBX-16-2
 //set("OBX-16", verifyingObserverName)
 // TODO got to check what it actually looks like in an SR


 /* Populate the Expected ZDS Segments  */
 output.getMessage().addNonstandardSegment('ZDS')
 
 set('ZDS-1-1', get(StudyInstanceUID))
 
 set('ZDS-1-2', 'RadImage')
 
 set('ZDS-1-3', 'Application')

 set('ZDS-1-4', 'DICOM')



 /* Output the Outgoing Report to Log */
 log.debug(scriptName + "This is the Report that will get sent: \n {}", output)

