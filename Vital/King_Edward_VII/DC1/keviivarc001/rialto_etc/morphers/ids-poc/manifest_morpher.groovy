/*
*   Manifest Morpher - This morpher runs every time the service receives a DICOM Study or Internally-generated SR to create an XDS Manifest object for all the SOPs in the study.
*       If it is an internally-created SR object (created through the ORU to SR converter), the Manifest will only contain a reference to that SR Object.
*       If it a DICOM Study moved in through the DCM Server, the Manifest will contain reference to all the SOPs that were sent to us.
*       Later on the underlying workflow (not accessible to scripting) Vault IDS will merge Manifests for studies that already have them.
*       Currently for  Report Manifests, we simply create all received reports and store them in our XDS Repo (see the ORU_to_SR morpher), and we generate Report Manifest that get merged
*       with whatever other Manifests there might exist for that given study.  This means that we could currently be storing several copies of the same report.
*
*   IMPORTANT NOTE:  This morpher deals with traffic received from 2 very different workflows.  You *must* account for both of them to not get errors, duplicate log messages or incorrect
*       contexts.
*       The 2 workflows it deals with are:
*           1. Full DICOM Studies pushes in from the DIR (HMI EIR) through the "dicomForManifestFeed" DICOM Server
*           2. Internally-created DICOM SR (Report) Objects, passed directly through the VaultIDS Service without going through a DICOM Server
*
*           Because in Workflow 1, a DIR can have many contexts for the different PACS it services, you might need to infer the context that the EIR is sending the study from, 
*            and populate an IssuerOfPatientID value.
*
*           In Workflow 2, you might already have the IssuerOfPatientID set, but you need to handle this traffic in a different manner, as not to confuse your logging.  Also, you have to be
*            careful to not end up with a wrong context because of the Workflow 1 logic, or inadvertently set off alarms for unknown contexts, etc.
*
*
*   FUNCTIONALITY SUMMARY:  The script is currently doing the following:
*       1. 
*/

// Include Common Functions File
LOAD("context_mapping_common.groovy")


log.trace("MANIFEST Creation: raw object before being modified by the script:\n {} \n\n", input)

// Maintain the original StudyInstanceUID
 def originalStudyInstanceUID = get('StudyInstanceUID')
 set('StudyInstanceUID', originalStudyInstanceUID)

 log.debug("Creating Manifest for Study Instance UID: {}", originalStudyInstanceUID)







/* Logic to Tell whether the Received Object is an in-house created SR Report, or a full DICOM Study from the DIR */

 def issuerOfPID = get(IssuerOfPatientID)
 def knownIssuer = Context_Mapping.knownIssuersOfPID[issuerOfPID]
 log.debug("\n Issuer Of Patient ID as received from the incoming object is \"{}\"", issuerOfPID)
 log.debug("This maps to \"{}\" in our known values table", knownIssuer) 


 // Check to see if the received IssuerOfPatientID is valid.  If it is it is likely our SR (the DIR sends bogus values in that field)
 if(null != knownIssuer) { 
    
    // Double check to see if it is our basic SR
    if('1.2.840.10008.5.1.4.1.1.88.11' == get(SOPClassUID) ){

        log.debug("This is an internally-created SR Report from " + knownIssuer )
    } else {
    
        log.info("Unexpected last SOP as a basic SR, correctly populated, known IssuerOfPatientID from " + knownIssuer )
    }



 } else {    // It is likely a DICOM Study pushed in from the DIR
    
    log.debug("This is a study moved in from the DIR")

    /* Infer and Populate the IssuerOfPatientID based on the Combination of (Calling + Called AETitle) and Instution Name
    *   NOTE: This is a crucial part of the Manifest Creation workflow.  If the Issuer Of Patient ID must be correctly populated
    *       for the PIX lookup to work.  A working PIX lookup is needed to publish the Manifest against the correct patient
    */

    def institutionNameTopLevel = get(InstitutionName)

    def callingAE = getCallingAETitle()
    def calledAE = getCalledAETitle()
    log.debug("Calling AETitle was \"{}\", and Called AETitle was \"{}\".",callingAE, calledAE)

    
    def mapped_value = Context_Mapping.Resolve_Context_by_AETitle(callingAE, calledAE)
    log.debug("Current Value of \"mapped_value\" is = {}", mapped_value)
    def inferredIssuerOfPID = null
    def inferredInstitution = null

    if ( ! mapped_value.isEmpty() || mapped_value != null) {
    
        inferredIssuerOfPID = mapped_value[0]
        inferredInstitution = mapped_value[1]    
    }


    // It is crucial to set the correct inferred Issuer Of Patient ID to publish the Manifest against the correct patient in the XDS Registry
    if (inferredIssuerOfPID != null ){
        set('IssuerOfPatientID', inferredIssuerOfPID)
        log.debug("Assuming this Study is from {}.", inferredInstitution)


    } else {
        // Something else unexpected has happened.
        // Falling back to 2nd line of inference -- previously-populated InstitutionName field
        log.warn("HMI EIR Pushing studies to Vault is using a different Called / Calling AETitle combination than expected.\n" +
            "Will try to infer the Issuer Of Patient ID of this study based on the Institution Name field")
        
        log.info("Institution Name in this SOP is \"{}\"", institutionNameTopLevel)

        inferredIssuerOfPID = Context_Mapping.Issuer_from_Institution_Name[institutionNameTopLevel]

        if (inferredIssuerOfPID != null) {
            set('IssuerOfPatientID', inferredIssuerOfPID)
            log.info("Explicitly setting the IssuerOfPatientID from a known populated Institution.  Setting to the Quebec normalized IssuerOfPID for {}",Context_Mapping.knownIssuersOfPID[inferredIssuerOfPID])

        } else {
         // We don't know what context to assign to this study
            log.error("Unable to determine to what Context this Study should be assigned!!  Aborting Publishing for this Study!!")
            return false
        }

    } // end of last 'else' statement inside DICOM studies pushed from DIR  
 
 } // end of top-level 'else' statement





/* Manifest-Creation Logic Starts here */

// Explicitly set the "Retrieve Location UID" inside the "Referenced Series Sequence" to the  Repo's OID
 set('CurrentRequestedProcedureEvidenceSequence/ReferencedSeriesSequence/RetrieveLocationUID', '2.16.124.10.101.1.60.2.81')
 set('CurrentRequestedProcedureEvidenceSequence/ReferencedSeriesSequence/RetrieveAETitle', 'ALI_QUERY_EIRSCP')


// Set Study Date and Time
 def modality = get(Modality)

 modality = modality.toUpperCase()
 log.info("\nManifest Creation:  Current SOP Modality is \"{}\"",modality)

 if(null != modality) {
    // Set study date and time for non-null modalities (i.e. actual SOPs for modalities)

    log.info("\nManifest Creation:  Current Study Date is: \"{}\"", get(StudyDate))
    if('SR' != modality) {
      // If the DICOM object is NOT an SR, use StudyDateTime to set Manifest "Study Date / Time".  If StudyDate is not available, use the SOP Date/Time      
        if(null != get(StudyDate)) {
            set(StudyDate, get(StudyDate))
            set(StudyTime, get(StudyTime))
        } else {
            // DONT KNOW WHAT THE TAG FOR SOP DATE TIME IS
        }
    
    } else {
      // This could actually be null.  It's better to set an empty field and fail submission than to put in a wrong date
        set(StudyDate, get(StudyDate))
        set(StudyTime, get(StudyTime))
    }
 }


// Explictly set top-level institution name in an SR publshing (i.e. besides the Referring Physician Identification Sequence)
 def institutionName = get('ReferringPhysicianIdentificationSequence/InstitutionName')
 if(null != modality) {
    if('SR' == modality) {
        // SRs don't have institution name explicitly in their structure.  Set it to empty if an SR comes in. (Just to have the tag, it shouldn't overwrite what's in the SOPs)
        if(null != institutionName){
            set(InstitutionName, institutionName)
            log.info("Expliclitly setting top level institution name for an SR publishing.  Setting to  \"{}\"", institutionName)
        } else {
            log.info("Institution name is blank.")
        }
    } /*else {
       // Temporarily SETTING a value for Testing
        set(InstitutionName, 'LakeShore1') 
    }

 } else {
    // Temporarily SETTING a value for Testing Institution in non-modality SOPs
    set(InstitutionName, 'LakeShore1')  
 */
 }


// Temporarily setting the Code Meaning to null
// set('AnatomicRegionSequence/CodeMeaning', null)

// NEED TO FINISH THIS, MAPS, ETC
// Explicitly set the Anatomic Region Sequence Code Meaning if not Populated
/*if(null == get('AnatomicRegionSequence/CodeMeaning')) {
    set('AnatomicRegionSequence/CodeMeaning', 'Autre')
    //set('AnatomicRegionSequence/CodeMeaning', 'Anatomic Region Code')
    log.info("Manifest (DICOM KOS) Creation: Explicitly setting the Anatomic Region Code, as it was NULL in the Study or SR Report")
}
*/


// Explicitly setting the Additional Patient History field
 set(AdditionalPatientHistory, get(AdditionalPatientHistory))

// Explicitly set the StudyPriorityID
 set(StudyPriorityID, get(StudyPriorityID))


// Explicitly set the ConfidentialityCode
 set(ConfidentialityCode, get(ConfidentialityCode))



/* Logging the entrie DICOM object for debugging purposes */
log.trace("This is the current manifest being created:\n {}",output)

