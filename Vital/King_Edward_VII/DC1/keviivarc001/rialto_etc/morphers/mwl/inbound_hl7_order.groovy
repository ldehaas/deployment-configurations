def scriptName = 'MWL ORDER Morpher - ';

log.info(scriptName + "START...")
log.debug(scriptName + "HL7 Input:\n{}", input)

def pvConfCode = get("PV1-16")

if (pvConfCode == null || ''.equals(pvConfCode)) {

    log.debug(scriptName + "PV1-16 appears to be null or empty, will try to use PD1-12 to setup Confidentiality tag...")
    
    def pdConfCode = get("PD1-12")
    if (pdConfCode != null && !''.equals(pdConfCode))  {
        log.debug(scriptName + "setting PV1-16 segment with a value from PD1-12...")
        set("PV1-16", get("PD1-12"))
    } else {
        log.debug(scriptName + "PD1-12 appear to be null or empty, will NOT set PV1-16 segment...")
    }
}

log.debug(scriptName + "HL7 Output:\n{}", output)

log.info(scriptName + "END...")
