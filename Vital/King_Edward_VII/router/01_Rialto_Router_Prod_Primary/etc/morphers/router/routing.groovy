/**
* This is the script that decides which dataflow template to use.
*/

log.debug('starting routing.groovy')

def workflow_vioarchive  = "etc/dataflows/vioarchive-only.xml";
def workflow_viovims = "etc/dataflows/vims-and-vioarchive.xml";
def workflow_viovimscd = "etc/dataflows/vims-vio-cd.xml";
def workflow_viocd = "etc/dataflows/vio-cd.xml";
def workflow_cd = "etc/dataflows/cd-only.xml";

def dataflow = workflow_vioarchive;
def modality = get(Modality)

log.info('Modality = {}', modality)

if (modality != null && modality.toUpperCase().contains("CT")) {
    dataflow = workflow_viovimscd
    log.info("Modality is a CT - Using " + dataflow + " as the dataflow document.");
}
else if (modality != null && modality.toUpperCase().contains("MR")) {
    dataflow = workflow_viovimscd
    log.info("Modality is a MR - Using " + dataflow + " as the dataflow document.");
}

log.info('Dataflow = {}', dataflow)

return dataflow;
