# compatibility: whitetank
aisexec {
  # Run as root - it is necessary to be able to manage resources with Pacemaker
  user: root
  group: root
}
totem {
  version: 2

  # How long before declaring a token lost (ms)
  token: 5000
  
  # How many token retransmits before forming a new configuration
  token_retransmits_before_loss_const: 20
  
  # How long to wait for join messages in the membership protocol (ms)
  join: 1000
  
  # How long to wait for consensus to be achieved before starting a new round of membership configuration (ms)
  consensus: 7500

  # With secauth enabled, Corosync nodes mutually authenticate using the shared secret stored in /etc/corosync/authkey,
  # When using secauth, cluster communications are also encrypted.
  secauth: on
  threads: 0
  interface {
    member {
      memberaddr: @@NET_ADDRESS1
    }
    member {
      memberaddr: @@NET_ADDRESS2
    }
    ringnumber: 0
    # bind address is unique per node in the cluster
    bindnetaddr: @@BINDNET_ADDRESS
    mcastport: @@MULTICAST_PORT
    ttl: 1
  }
  transport: udpu
}
logging {
  fileline: off
  to_stderr: no
  to_logfile: yes
  to_syslog: yes
  logfile: /var/log/cluster/corosync.log
  debug: off
  timestamp: on
  logger_subsys {
    subsys: AMF
    debug: off
  }
}
amf {
  mode: disabled
}

service {
  # Load the Pacemaker Cluster Resource Manager
  name: pacemaker
  ver: 1
}
