// environment specific variables should be placed here. This file should not be copied across prod/test without modification

class env_var {
   static final hmc_agfa_store_scp = 'NWG2AOFH'
   static final hmc_agfa_store_scu = 'AS2AOFH'
   static final Default_OID = '2.16.124.113638.7.6.1.5104'
   static final Default_Unknown_OID = '2.16.124.113638.7.6.1.5100'
   static final DDOHSR_OID = '2.16.124.113638.7.6.1.5105'
   static final DDSTPH_OID = '2.16.124.113638.7.6.1.5106'
   static final DDNSSLIDELL_OID = '2.16.124.113638.7.6.1.5107'
   static final DDOHSCN_OID = '2.16.124.113638.7.6.1.5109'
   static final DDOHSCI_OID = '2.16.124.113638.7.6.1.5110'
   static final MHM_OID = '2.16.124.113638.7.6.1.5111'
   static final DDOHSOP_OID = '2.16.124.113638.7.6.1.5113'
   static final DDOHSCPN_OID = '2.16.124.113638.7.6.1.5115'
   static final DDOHSOB_OID = '2.16.124.113638.7.6.1.5117'
   static final DDOHSPOCUS_OID = '2.16.124.113638.7.6.1.5119'
}
