LOAD('/opt/rialto/etc/services/common/mwl_mapping.groovy')
LOAD('/opt/rialto/etc/services/common/common_ohs.groovy')

def scriptName = "MWL cfind response morpher - CallingAET " + getCallingAETitle() + " "
log.info(scriptName + "START")

log.info(scriptName + "start examining the order with patient ID {} and accession number {}", get("PatientID"), get("AccessionNumber"))

// Characterset is handled in the request morpher.
// Response morpher only has the ability to rename the character set tag. It cannot change the actual character storage.

// pass the value for placer and filler numbers
set(PlacerOrderNumberImagingServiceRequest,imagingServiceRequest.getPlacerIssuerAndNumber())
set(FillerOrderNumberImagingServiceRequest,imagingServiceRequest.getFillerIssuerAndNumber())
set(CurrentPatientLocation,imagingServiceRequest.getVisitStatus().getCurrentPatientLocation())
set("ScheduledProcedureStepSequence/ScheduledProcedureStepDescription", get("RequestedProcedureDescription"))

set("BodyPartExamined",imagingServiceRequest.getRequestedProcedureSequence().get(0).getBodyPart())
set("Laterality",imagingServiceRequest.getRequestedProcedureSequence().get(0).getLaterality())


def callingAET =  getCallingAETitle()
def calledAET = getCalledAETitle()

if (OHSCommon.IsCleanAE(callingAET) || OHSMWL.IsWhiteListAET(callingAET)) {
    // Including specialty code in MWL result
    if (imagingServiceRequest.getRequestedProcedureSequence().get(0).getRequestedProcedureCodeSequence().size() > 1) {
        if (imagingServiceRequest.getRequestedProcedureSequence().get(0).getRequestedProcedureCodeSequence().get(1).getCodingSchemeDesignator() == 'OHS_SPECIALTY'){
            set(tag(0x0035,0x9999), imagingServiceRequest.getRequestedProcedureSequence().get(0).getRequestedProcedureCodeSequence().get(1).getCodeValue(), VR.SH)
        }
    }
    // no need to set 0035,9997 and 0035,9998 here because requested body part and requested laterality have their native tags in MWL.
    // future reconciliation will pull them up from their native nags so the next two lines are commented out

    // When calling AET is internal, do not interfere with the query result
    log.info(scriptName + "is recognized as internal. Will NOT interfere with the original search results")

} else {
    // Note: if calling AET is not recognized in group mapping, then the cfind request should have been discarded and this branch should never be executed.
    // Therefore, no need to check group assignment again here.
    def myOrderStatus = get("ScheduledProcedureStepSequence/ScheduledProcedureStepStatus");
    if (OHSMWL.IsPACSSCAN(callingAET)){
        // if calling AET is PACS scan, it should not exclude completed status
        if (myOrderStatus == null || myOrderStatus == 'DISCONTINUED') {
            log.info(scriptName + "Dropping this result from DMWL C-Find response, Calling AET is a PACSSCAN and OrderStatus={}", myOrderStatus)
            return false
        } else {
            log.info(scriptName + "Retaining this result in DMWL C-Find response, Calling AET is a PACSSCAN and OrderStatus={}", myOrderStatus)
        }
    } else {
        // if calling AET is not PACS scan, it must be a physical modality, both completed and cancelled are excluded.
        if (myOrderStatus == null || myOrderStatus == 'DISCONTINUED' || myOrderStatus == 'COMPLETED') {
            log.info(scriptName + "Dropping this result from DMWL C-Find response, Calling AET is a Modality and OrderStatus={}", myOrderStatus)
            return false
        } else {
            log.info(scriptName + "Retaining this result in DMWL C-Find response, Calling AET is a Modality and OrderStatus={}", myOrderStatus)
        }
    }

    // Remove Epic ID from physician name tag
    def ref_phys_name_orig = get(ReferringPhysicianName)
    if (ref_phys_name_orig) { set(ReferringPhysicianName, ref_phys_name_orig.substring(ref_phys_name_orig.indexOf("^")+1).replaceAll("\\^", ", ")) }

    def req_phys_name_orig = get(RequestingPhysician)
    if (req_phys_name_orig) { set(RequestingPhysician, req_phys_name_orig.substring(req_phys_name_orig.indexOf("^")+1).replaceAll("\\^", ", ")) }

    def perf_phys_name_orig = get(PerformingPhysicianName)
    if (perf_phys_name_orig) { set(PerformingPhysicianName, perf_phys_name_orig.substring(perf_phys_name_orig.indexOf("^")+1).replaceAll("\\^", ", ")) }

    // The following tag manipulations are only apply to mwl cfind requested by physical modalities.
    output.remove(tag(0x0010,0x0024))      // issuer of patient id qualifier sequence
    output.remove(tag(0x0008,0x0051))      // issuer of accession numbers

    // RequestedProcedureCodeSequence (Procedure code and specialty) are both implicitly copied. However we do not want to present specialty code to modalities. So we first clean out the entire sequence and re-populate just procedure code and description. This is because we can't selectively delete one code.
    output.remove(tag(0x0032,0x1064))      // remove the entire sequence for requested procedure code
    set("RequestedProcedureCodeSequence/CodeValue",imagingServiceRequest.getRequestedProcedureSequence().get(0).getRequestedProcedureCodeSequence().get(0).getCodeValue())
    set("RequestedProcedureCodeSequence/CodingSchemeDesignator",imagingServiceRequest.getRequestedProcedureSequence().get(0).getRequestedProcedureCodeSequence().get(0).getCodingSchemeDesignator())
    set("RequestedProcedureCodeSequence/CodeMeaning",imagingServiceRequest.getRequestedProcedureSequence().get(0).getRequestedProcedureCodeSequence().get(0).getCodeMeaning())

    if (OHSMWL.IsLegacyMWLSCP(callingAET)) {
        log.info(scriptName + "is a legacy modality and requires certain fields to be removed")
        // The following tags are removed from the responses to legacy modalities on the assumption that they don't like these fields
        // because they never received them from their legacy MWL SCP.
        output.remove(tag(0x0010,0x0021))      // issuer of patient id
        output.remove(tag(0x0032,0x1064))      // requested procedure code sequence
        output.remove(tag(0x0040,0x0006))      // scheduled performing physician's name
        output.remove(tag(0x0040,0x2004))      // issue date of imaging service request
        output.remove(tag(0x0040,0x2005))      // issue time of imaging service request
        output.remove(tag(0x0040,0x2008))      // order entered by

    }

    if (OHSMWL.IsGEMWLSCP(callingAET)) {
        output.remove(tag(0x0040,0x2010))     // OrderCallbackPhoneNumber
        output.remove(tag(0x0040,0x2016))     // PlacerOrderNum-ImagingServiceReq
        output.remove(tag(0x0040,0x2017))     // FillerOrderNumber-ImagingServiceReq
        set("ScheduledProcedureStepSequence/ScheduledStationAETitle", callingAET)
        set("ScheduledProcedureStepSequence/ScheduledStationName", callingAET)
    }
}

if (OHSMWL.IsForceSCSIR100(callingAET)) {
    set("SpecificCharacterSet", 'ISO_IR 100')
}

if (OHSMWL.IsGESENOES(callingAET)) {
    log.info("testing123")
    set("ScheduledProcedureStepSequence/ScheduledPerformingPhysicianName", get(ReferringPhysicianName))
}


log.info(scriptName + "END")
