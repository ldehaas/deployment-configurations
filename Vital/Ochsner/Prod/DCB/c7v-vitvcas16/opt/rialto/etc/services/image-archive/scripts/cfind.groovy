/* cfind.groovy
 *
 * This script is intended to morph the IssuerOfPatientID tag of an inbound
 * C-FIND request when this tag is not present.
 *
 */

log.debug("cfind.groovy: Start c-find request morpher")


log.debug("cfind.groovy: Checking IssuerOfPatientID")

def issuerOfPatientId = get("IssuerOfPatientID")

if (issuerOfPatientId == null) {
	set("IssuerOfPatientID", "CG")
	set("IssuerOfPatientIDQualifiersSequence/UniversalEntityID", 'county.general')
	set("IssuerOfPatientIDQualifiersSequence/UniversalEntityIDType", 'ISO')
	log.debug("cfind.groovy: Morphed IssuerOfPatientID to be ")
	log.debug("cfind.groovy: namespace = {}, domain = {}, type = {}", 
		get("IssuerOfPatientID"), 
		get("IssuerOfPatientIDQualifiersSequence/UniversalEntityID"), 
		get("IssuerOfPatientIDQualifiersSequence/UniversalEntityIDType"))
}

log.debug("cfind.groovy: End c-find request morpher")