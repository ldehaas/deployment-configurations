import java.time.LocalDateTime
import java.time.temporal.ChronoUnit
import java.time.format.DateTimeParseException
import java.time.format.DateTimeFormatter

class DataHandlerCommon {

    static String calculateAge(String patientDateOfBirth, String patientTimeOfBirth, String studyDate, String studyTime){
        def TIPPING_WEEKS_TO_DAYS = 4
        def TIPPING_MONTHS_TO_WEEKS = 3
        def TIPPING_YEARS_TO_MONTHS = 2
        
        if (patientDateOfBirth == null || studyDate == null) {
            return null
        }

        patientDateOfBirth = patientDateOfBirth.replace(":", "")
        studyDate = studyDate.replace(":", "")

        def patientDateTimeOfBirth
        def studyDateTime

        try {
            def dtf = DateTimeFormatter.ofPattern("yyyyMMddHHmmss")
            patientDateTimeOfBirth = LocalDateTime.parse(patientDateOfBirth + cleanTime(patientTimeOfBirth), dtf)
            studyDateTime = LocalDateTime.parse(studyDate + cleanTime(studyTime), dtf)
        } catch (DateTimeParseException e) {
            throw e
            return null
        }

        if (!patientDateTimeOfBirth.isBefore(studyDateTime)) {
            return null
        }

        def years = ChronoUnit.YEARS.between(patientDateTimeOfBirth, studyDateTime)
        def months = ChronoUnit.MONTHS.between(patientDateTimeOfBirth, studyDateTime)
        def weeks = ChronoUnit.WEEKS.between(patientDateTimeOfBirth, studyDateTime)
        def days = ChronoUnit.DAYS.between(patientDateTimeOfBirth, studyDateTime)
        def age = ""

        if (years > 999) {
            return null
        }
        if (years >= TIPPING_YEARS_TO_MONTHS) {
            age = years + "Y"
        } else if (months >= TIPPING_MONTHS_TO_WEEKS) {
            age = months + "M"
        } else if (weeks >= TIPPING_WEEKS_TO_DAYS) {
            age = weeks + "W"
        } else {
            age = days + "D"
        }

        return age.padLeft(4, "0")
    }

    static String cleanTime(String str) {
        if (str == null) {
            return "000000"
        }
        str = str.replace(":", "") 
        if (str.indexOf(".") == 6) {
            str = str.substring(0, 6)
        }
        if (str.contains(".") || str.length() > 6 || str.length()%2 == 1) {
            return "000000"
        }
        return str.padRight(6, "0")
    }

    static String genIUID() {
        def uuidString = UUID.randomUUID().toString()
        def uidString = ''
        def uidRoot = '1.2.840.113747.20080222' // This is the Vital UID root. You could use this or a site-specific one.

        uuidString.each { digit ->
            if (digit == '-') {
                uidString += '.'
            }
            else {
                uidString += Integer.parseInt(digit, 16).toString()
            }
        }
        def newUID = uidRoot + '.' + uidString 
        return newUID
    }
}


