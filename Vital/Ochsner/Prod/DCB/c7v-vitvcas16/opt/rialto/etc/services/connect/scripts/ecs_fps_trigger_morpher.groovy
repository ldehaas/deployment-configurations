def scriptName = "Connect - ecs_fps_trigger_morpher.groovy "

log.info(scriptName + "START")

def messageType = get('/.MSH-9-1')
def triggerEvent = get('/.MSH-9-2')
def controlCode = get('/.ORC-1')
def orderStatus = get('/.ORC-5')

if ("ORM".equalsIgnoreCase(messageType) && "O01".equalsIgnoreCase(triggerEvent) && "NW".equalsIgnoreCase(controlCode) && "Scheduled".equalsIgnoreCase(orderStatus)) {
    log.info(scriptName + "Received ORM indicating a scheduled order, assuming the patient has studies to pre-warm.")
} else {
    log.warn(scriptName + "An unactionable message was privided. Expected ORM^O01 with ORC-1=NW and ORC-5=Scheduled. Receiving {}^{} with ORC-1={} and ORC-5={}", messageType, triggerEvent, controlCode, orderStatus)
    return false
}

log.info(scriptName + "END")
