String scriptName = "Connect - fwd_fps_trigger_morpher.groovy"
String scriptVersion = 'v04'

// **v04 note** The logic that was this script is now in the routing rules
log.info(scriptName + " START " + scriptVersion)
log.debug(scriptName + " Input\n" + input) // Trigger order

String studyDesc = (get('/.OBR-4-2') ?: '').toUpperCase()
log.info(scriptName + " Order will be processed for DICOM forwarding. StudyDescription={}", studyDesc)