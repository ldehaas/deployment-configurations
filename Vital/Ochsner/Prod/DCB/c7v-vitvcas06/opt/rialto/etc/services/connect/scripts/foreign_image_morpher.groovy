String scriptName = "Connect - fwd_foreign_image_morpher.groovy"
String scriptVersion = "v02"

log.info('{} {}: Starting script', scriptName, scriptVersion)
Integer thinSliceTheshold = 50
Boolean keepNonImages = true // Set variable to determine if instances without slice thickness are kept

String sliceThickness = get(SliceThickness)
if (!sliceThickness) {
    return keepNonImages
}
if (Float.parseFloat(sliceThickness) > thinSliceTheshold) {
    log.warn('{}: SOP Instance {} is not thin -- Slice Thickness = {} -- and will not be sent.', scriptName, sliceThickness, get(SOPInstanceUID))
    return false
}
log.info('{}: SOP Instance {} is thin -- Slice Thickness = {} -- and will be sent.', scriptName, sliceThickness, get(SOPInstanceUID))
