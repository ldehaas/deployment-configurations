LOAD('/opt/rialto/etc/services/connect/scripts/AVCodes.groovy')
//import AVCodes
String scriptName = "Connect - fwd_cfind_response_filter.groovy"
String scriptVersion = "v02"

Map<String,List<String>> procedureCodeMap = [:]
// Currently map is only 1:1 but this could be modified to retrieve multiple matching codes from a source code
AVCodes.priorCodes.each { procedureCode ->
	procedureCodeMap[procedureCode] = [procedureCode]
}
log.info(scriptName + " " + scriptVersion + ": Start")

def validPriorList = []
String orderProcedure = order.get('OBR-4-2')
log.info("{}: Order procedure is {}.",scriptName, orderProcedure)

log.info("{}: tdr Start", scriptName)
inputs.each {
    study ->
    log.info("{}: accession {}", scriptName, study.get(AccessionNumber))
}
log.info("{}: tdr End", scriptName)

if (procedureCodeMap[orderProcedure]) {
	inputs.each { priorStudy -> // check Study Description of each input
		log.info("{}: Input study is {}", scriptName, priorStudy)
	    String studyDescription = priorStudy.get(StudyDescription)
	    if (procedureCodeMap[orderProcedure].contains(studyDescription)) {
	        log.info("{}: Study with description {} will be pre-fetched.",scriptName, studyDescription)
	        validPriorList.add(priorStudy)
	    } else {
	    	log.info("{}: Study description {} not in {}", scriptName, studyDescription, procedureCodeMap[orderProcedure])
	    }
	}
} else { // C-FIND was for specific study only so all studies should be added
	log.info("{}: Order procedure {} not in {}. Fetching only current study.", scriptName, orderProcedure, procedureCodeMap)
	inputs.each { priorStudy ->
		validPriorList.add(priorStudy)
	}
}

log.info("{}: Will pre-fetch {} studies.", scriptName, validPriorList.size())
return first(validPriorList, validPriorList.size())
