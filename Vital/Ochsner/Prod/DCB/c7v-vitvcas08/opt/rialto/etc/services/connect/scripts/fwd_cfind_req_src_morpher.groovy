def scriptName = "Connect - fwd_cfind_req_src_morpher.groovy "

if (getCalledAETitle()){
    scriptName = "Connect - fwd_cfind_req_src_morpher.groovy " + getCalledAETitle().toUpperCase() + " "
}

log.debug(scriptName + "Start")
def in_StudyInstanceUID = get('StudyInstanceUID')
log.debug(scriptName + in_StudyInstanceUID)
if (in_StudyInstanceUID == 'PLACE_HOLDER'){
    remove(tag(0x0020,0x000D))
}
log.debug(scriptName + "End")
