// pass the value for placer and filler numbers
LOAD('/opt/rialto/etc/services/common/mwl_mapping.groovy')
LOAD('/opt/rialto/etc/services/common/common_ohs.groovy')

def scriptName = "MWL cfind request morpher - CallingAET " + getCallingAETitle() + " "
log.info(scriptName + "START")

log.debug(scriptName + "Input:\n{}", input)

// if specific character set isn't supplied in the query, default it to Latin1. Without this,
// the system default IR192 (unicode) will be used, which isn't supported by some antique modalities.
def specifiedCharSet = get("SpecificCharacterSet")
if (!specifiedCharSet){
    set("SpecificCharacterSet", 'ISO_IR 100')
}

def callingAET = getCallingAETitle()
if (callingAET != null) {
    callingAET = callingAET.toUpperCase()
}

if (OHSCommon.IsCleanAE(callingAET) || OHSMWL.IsWhiteListAET(callingAET)) {
    log.info(scriptName + "is recognized as internal. Will NOT interfere with original search keys")
}else{
    def locationGroup = OHSMWL.GetLocGrpByAET(callingAET)

    if (locationGroup == null){
        // if calling AET is not recognized in group mapping, then do not return anything in the result.
        //log.info(scriptName + "does not belong to any location group specified. MWL will drop the Query")
        //return false
        locationGroup = "DROPTHISREQUEST"
        set("ScheduledProcedureStepSequence/ScheduledProcedureStepLocation",locationGroup)
    }else{
        log.info(scriptName + "is mapped to Location Group {}", locationGroup)
        def AccNum = get("AccessionNumber")
        def PID = get("PatientID")
        def SIUID = get("StudyInstanceUID")
        def SPSStartDate = get("ScheduledProcedureStepSequence/ScheduledProcedureStepStartDate")
        def SPSEndDate = get("ScheduledProcedureStepSequence/ScheduledProcedureStepEndDate")

        def patientName = get("PatientName")
        if (patientName != null && "*".equalsIgnoreCase(patientName.trim())) {
            log.debug(scriptName + "PatientName provided by modality is a wildcard, will strip it out")
            set("PatientName", "")
        }

        if (OHSMWL.StripModalityTagByAET(callingAET)) {
            log.info(scriptName + "CallingAET {} is on the list of modalities to get Modality Type stripped from DMWL request", callingAET)
            set("ScheduledProcedureStepSequence/Modality","")
        }

        if (OHSMWL.StripModalityTagByLocGroup(locationGroup)) {
            log.info(scriptName + "Location Group {} for CallingAET {} is on the list of location groups to get Modality Type stripped from DMWL request", locationGroup, callingAET)
            set("ScheduledProcedureStepSequence/Modality","")
        }

        if (SPSStartDate != null && SPSStartDate.contains('-')){
            log.debug(scriptName + "ScheduledProcecureStepStartDate is provided by modality as {}", SPSStartDate )
            def splitDate = SPSStartDate.split('-')
            SPSStartDate = splitDate[0]
            if (splitDate.size() > 1) {
                SPSEndDate = splitDate[1]
            }

            log.debug(scriptName + "splitting ScheduledProcecureStepStartDate into StartDate {} and EndDate {}", SPSStartDate, SPSEndDate)
        }

        // None of these keys are provided, the querying modality is polling for daily task. In this case, we do infer some search keys based on AE title.
        if (!AccNum && !PID && !SIUID) {
            // If the code falls in this bracket, the modality does not provide AccNum, PID or SUIUD. This indicates the modality doesn't know what it wants.
            // In this case we take over and dictates its search conditions.
            def MWL_Day_Window = 0
            def todayDate = new Date()
            if (OHSMWL.IsPACSSCAN(callingAET)) {
                // if calling AET is recognized as PACSSCAN, use +- 2 day as range, without location group as filter
                MWL_Day_Window = 2
                log.info(scriptName + "is a PACS scan device. It does not appear to search for specific patient or study. We dictates search condition for it.")

                log.info(scriptName + "Normalized search condition for PACSSCAN: Modality=''")
                set("ScheduledProcedureStepSequence/Modality","")

                // def SiteCode = locationGroup.split('_')[0]
                // log.info(scriptName + "Normalized search condition for PACSSCAN: ScheduledProcedureStepLocation={}", SiteCode)
                // set("ScheduledProcedureStepSequence/ScheduledProcedureStepLocation",SiteCode)
            } else {
                MWL_Day_Window = 1
                // if calling AET is recognized as regular modality use +-1 day as range, with location group as filter
                log.info(scriptName + "is a modality. It does not appear to search for specific patient or study. We dictates search condition for it.")

                log.info(scriptName + "Normalized search condition: ScheduledProcedureStepLocation={}", locationGroup)
                set("ScheduledProcedureStepSequence/ScheduledProcedureStepLocation",locationGroup)
            }

            if (!SPSStartDate && !SPSEndDate) {
                // neither start or end is provided, we force a three day window (from yesterday to tomorrow)
                SPSStartDate = (todayDate - MWL_Day_Window).format('yyyyMMdd')
                SPSEndDate   = (todayDate + MWL_Day_Window).format('yyyyMMdd')
            }else if (!SPSEndDate){
                // only start date was provided, we force a date range (startdate, startdate + x day) and fill in the end date
                def StartDate = new Date().parse('yyyyMMdd', SPSStartDate)
                SPSEndDate = (StartDate + MWL_Day_Window).format('yyyyMMdd')
            }else if (!SPSStartDate){
                // only end date is provided, we force a date range (enddate -x day, enddate) and fill in the start date
                def EndDate = new Date().parse('yyyyMMdd', SPSEndDate)
                SPSStartDate = (EndDate - MWL_Day_Window).format('yyyyMMdd')
            }else{
                // both start and end dates are provided, do nothing
                log.debug(scriptName + "Both start and end dates are provided. Will respect the date condition.")
            }
            set("ScheduledProcedureStepSequence/ScheduledProcedureStepStartDate", SPSStartDate + "-" + SPSEndDate)
            set("ScheduledProcedureStepSequence/ScheduledProcedureStepEndDate", null)
            set("ScheduledProcedureStepSequence/ScheduledProcedureStepStartTime", null)
            set("ScheduledProcedureStepSequence/ScheduledProcedureStepEndTime", null)
            set("ScheduledProcedureStepSequence/ScheduledStationName", null)

            log.info(scriptName + "Normalized search condition: ScheduledProcedureStepDate between {} and {}", SPSStartDate, SPSEndDate)

            log.info(scriptName + "Implied filter on ScheduledProcedureStep applied: set Start and End Date to {} and {}, and removed Start and End Time", SPSStartDate, SPSEndDate)
        }else{
            // WARNING: This branch is purposefully left blank. We do not meddle with modality's original search condition! Do not add code here!!!!!!
            log.debug(scriptName + "appears to be looking for a specific patient or study. We back out and respect its original search condition.")
        }
    }
}

log.info(scriptName + "Output:\n{}", output)
log.debug(scriptName + "ScheduledProcedureStep filter:\n StartDateTime: {} {}\n EndDateTime: {} {}",get("ScheduledProcedureStepSequence/ScheduledProcedureStepStartDate"),get("ScheduledProcedureStepSequence/ScheduledProcedureStepStartTime"),get("ScheduledProcedureStepSequence/ScheduledProcedureStepEndDate"),get("ScheduledProcedureStepSequence/ScheduledProcedureStepEndTime"))

log.info(scriptName + "END")

