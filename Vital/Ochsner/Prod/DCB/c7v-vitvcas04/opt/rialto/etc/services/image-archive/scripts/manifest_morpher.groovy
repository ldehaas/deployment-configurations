log.debug("manifest_morpher.groovy: Start morpher")

// Modality Logic
def dcmModality = get(ModalitiesInStudy)
if (dcmModality) {
    set(ModalitiesInStudy, dcmModality.toUpperCase())
}

log.debug("manifest_morpher.groovy: Modality: {}, ReceivingAE: {}", dcmModality, get(ReceivingAE))

log.debug("manifest_morpher.groovy: End morpher")
