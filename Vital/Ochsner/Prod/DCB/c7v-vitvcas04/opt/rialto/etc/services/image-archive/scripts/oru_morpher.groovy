// This is the morpher that modifies ORU message before IA converts them into SR
// We want to remove ZDS-1 to allow IA to assign study instance UID based on PID and Accession Number
//ZDS-1 is provided originally from Epic and might have fallen out of date because the study instance UID might have changed during split and merge.
//remove("ZDS-1")
set("ZDS-1",null)
