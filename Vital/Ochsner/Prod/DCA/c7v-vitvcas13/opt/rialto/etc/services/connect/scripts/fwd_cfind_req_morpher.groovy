def scriptName="Connect - fwd_cfind_req_morpher.groovy "

LOAD('/opt/rialto/etc/services/common/common_ohs.groovy')

log.debug(scriptName + "Start")


def in_PatientID = get('PatientID')

if (in_PatientID != null) {
    def out_issuerOfPatientID = OHSCommon.GetDefiPid()
    def out_universalEntityID = OHSCommon.GetDefiPidUnivId()
    def out_universalEntityType = ('ISO')

    set('IssuerOfPatientID', out_issuerOfPatientID)
    set('IssuerOfPatientIDQualifiersSequence/UniversalEntityID', out_universalEntityID)
    set('IssuerOfPatientIDQualifiersSequence/UniversalEntityIDType', out_universalEntityType)

    log.debug(scriptName + "cfind request is now stamped with fully Qualified Issuer of PatientID {}", out_universalEntityID)
}else{
    log.info(scriptName + " C-Find request does not contain PatientID")
}


// C-Find-Rq to spread across pools requires at least one of study instance UID and patient ID, if both are null, we fill up study instance UID to bypass this requirement.
// Then in the c-find request morphers for each source, we remove the fake value
def in_StudyInstanceUID = get('StudyInstanceUID')
if (!(in_StudyInstanceUID) && !(in_PatientID)) {
    set('StudyInstanceUID','PLACE_HOLDER') 
}

log.debug(scriptName + "End")

