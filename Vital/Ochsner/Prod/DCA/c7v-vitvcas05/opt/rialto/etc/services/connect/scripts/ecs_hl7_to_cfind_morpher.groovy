/*
 * HL7 to cfind morpher
 * Convert an order message into a cfind message.
 * get() works on the order message, while set() works on the 
 * cfind message.
 * 
 * The order is ignored if this script returns false,
 * e.g. no images will be fetched for the order.
 */

LOAD('/opt/rialto/etc/services/common/common_ohs.groovy')

def scriptName = "Connect - ecs_hl7_to_cfind_morpher.groovy"

log.info(scriptName + "Start")

def orig_PID = get('/.PID-3-1')

if (orig_PID){
 
    def out_issuerOfPatientID = OHSCommon.GetDefiPid()

    set("PatientID", orig_PID)
    set('IssuerOfPatientID', out_issuerOfPatientID)

    set("StudyTime", null)
    def cfEndDate = new Date()
    def cfStartDate = cfEndDate - 5*365
    set("StudyDate", cfStartDate.format('yyyyMMdd') + "-" + cfEndDate.format('yyyyMMdd'))


    log.info(scriptName + "converting HL7 to cfind query with patient id {}, study date start {} and end {}.",orig_PID,cfStartDate.format('yyyyMMdd'),cfEndDate.format('yyyyMMdd'))

    //def hl7BodyPart = get('/.OBR-15-4')
    //if (hl7BodyPart) {
    //    set(BodyPartExamined, hl7BodyPart)
    //}
    set(StudyInstanceUID, null)
    set(AccessionNumber, null)

    set(NumberOfStudyRelatedSeries, null)
    set(StudyDescription, null)
    set(BodyPartExamined, null)

}else{
    return false
}


log.info(scriptName + "End")
