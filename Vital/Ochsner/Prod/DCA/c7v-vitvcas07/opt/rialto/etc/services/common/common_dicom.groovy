LOAD('/opt/rialto/etc/services/common/dicom_tag_mapping.groovy')

class DICOM_Common {
    def log = null
    def sop = null
    def mapper = null

    def DICOM_Common(log, sop) {
        this.log = log
        this.sop = sop
        this.mapper = new DICOM_Mappings()         
    }

    def evalSOP(pm) {
        def sourceValue
        def foundValue
        def sequenceItems

        //iterate over the pattern matching array list
        for (def i = 0; i < pm.size(); i++) {
            if (pm[i].sequenceTag == 0) { //0 is the root of the object.
                log.debug("looking in the root of the object for tag " + pm[i].tag)
                sourceValue = sop.get(pm[i].tag)
                foundValue = evaluateValue(sourceValue, pm[i].processingExpression, pm[i].resultString)
                if (foundValue != null) {
                    log.debug("found a match (" + foundValue + "), getting out.")
                    return foundValue
                }
            }
            else {
                log.debug("the value we are to look for is within a sequence. cracking opening the sequence...")
                sequenceItems = sop.get(pm[i].sequenceTag)
                if (sequenceItems != null) {
                    log.debug("got all the items within the sequence. starting to iterate...")
                    for (def k = 0; k < sequenceItems.size(); k++) {
                        foundValue = evaluateValue(sequenceItems[k].get(pm[i].tag), pm[i].processingExpression, pm[i].resultString)
                        if (foundValue != null) {
                            log.debug("found a match (" + foundValue + "), getting out.")
                            return foundValue
                        }
                        else {
                            log.debug("not a match, moving to the next item within the sequence if there is one.")
                        }
                    }
                } else {
                    log.debug("did not get any items within the sequence...")
                }
            }
        }
    }

    def evaluateValue(valueIn, expressionIn, toReturn) {
        def f

        log.debug("looking at '" + valueIn + "' using " + expressionIn)

        //perform RegEx evaluation on the sourceValue
        f = valueIn =~ expressionIn

        //ensure it returned a value Matcher data type
        //assert f instanceof java.util.regex.Matcher
        if (f) {
            if (f.matches()) {
                log.debug("match found, returning '" + toReturn +"'.")
                return toReturn
            }
        }
    }

    def processSOPBodyPart() {
        def foundBodyPart

        foundBodyPart = evalSOP(mapper.bodyParts)

        if (foundBodyPart != null) {
            log.info("setting body part '" + foundBodyPart + "' into object.")
            sop.set(BodyPartExamined, foundBodyPart)
        }
        else {
            log.info("not setting body part into object since no suitable mappings could be found for accession number " + sop.get(AccessionNumber))
        }

    }

    def processSOPLaterality() {
        def foundLaterality

        foundLaterality = evalSOP(mapper.lateralities)

        if (foundLaterality != null) {
            log.info("setting laterality '" + foundLaterality + "' into object.")
            sop.set(Laterality, foundLaterality)
        }
        else {
            log.info("not setting laterality into object since no suitable mappings could be found for accession number " + sop.get(AccessionNumber))
        }
    }

}
