/**
* This groovy script will output the MWL c-find request as it passes
* through the dicom proxy. The output will also include the calling and called AET
* and will be written to the rialto.log file.
*
* All of the output will be on one line so that it can be exported and parsed. The
* c-find request is actual a multi line output however it was modified
* with the newline characters replaced with a bar (|). You
* can save this c-find request and then convert the bar character to new line by:
* tr "|" "\n" < input_file   > output_file
**/

def scriptName = "DicomProxy Logging MWL Morpher - ";
def callingAET = getCallingAETitle().toUpperCase();
def calledAET = getCalledAETitle().toUpperCase();

log.info(scriptName + "CallingAET: {} CalledAET: {} MWL query:{}", callingAET, calledAET, input.toString().replaceAll("[\\n]","|"))
