log.debug("document_metadata_morpher.groovy: Start morpher")
// The implementation below is based on minimal requirement.

def calledAET = get(ReceivingAE)
def dcmStudyDate=get(StudyDate)
def dcmStudyTime=get(StudyTime)
def dcmStudyInstanceUID=get(StudyInstanceUID)
def dcmPatientID=get(PatientID)
def (dcmNameSpace,dcmIPIDDomain,dcmIPIDType)=get(IssuerOfPatientID).tokenize("&")
def dcmPatientFullName = get(PatientName)
if (dcmPatientFullName == null) {
   log.debug("document_metadata_morpher.groovy: patient Name is null, setting to blank")
   dcmPatientFullName = ""
}
def (dcmPatientFamilyName, dcmPatientFirstName) = dcmPatientFullName.tokenize("^")
def dcmModality=get(ModalitiesInStudy)
def dcmPatientDOB = get(PatientBirthDate)
def dcmPatientSex = get(PatientSex)
def dcmAccessionNumber = get(AccessionNumber)

if (!dcmIPIDDomain) {
    dcmIPIDDomain = get("IssuerOfPatientIDQualifiersSequence/UniversalEntityID")
    dcmIPIDType = get("IssuerOfPatientIDQualifiersSequence/UniversalEntityIDType")
}

def fq_pid = new Pid(dcmPatientID,dcmNameSpace,dcmIPIDDomain,dcmIPIDType)
log.debug("document_metadata_morpher.groovy: {},{},{},{}", dcmPatientID,dcmNameSpace,dcmIPIDDomain,dcmIPIDType)

if (calledAET == 'VCIAP8') {
    set(XDSClassCode, code('26441-6 ','2.16.840.1.113883.3.88.12.80.46','Cardiology studies (set)'))
}else{
    set(XDSClassCode, code('18726-0 ','2.16.840.1.113883.3.88.12.80.46','Radiology studies (set)'))
}

set(XDSTypeCode,code('55113-5','2.16.840.1.113883.6.1','Key Images'))

set(XDSConfidentialityCode,code('N', '2.16.840.1.113883.4.642.3.47', 'Normal'))
if (dcmModality){
    set(XDSEventCode,code(dcmModality,'1.2.840.10008.2.16.4', dcmModality))
}else{
    return false
}

// Body parts will be skipped until mapping between procedure and body part is defined further.
set(XDSFormatCode,code('1.2.840.10008.5.1.4.1.1.88.59', '1.2.840.10008.2.6.1', 'Key Object Selection Document'))

// Leaving Facility Type code unknown for now.
set(XDSHealthcareFacilityTypeCode, code('0', '2.16.840.1.113883.3.88.12.80.67', 'Unknown'))

set(XDSLanguage, 'en-US')


set(XDSPatientID, fq_pid)
set(XDSSourcePatientID, fq_pid)

if(null == dcmPatientFamilyName) {
    log.warn("XDS Metadata creation, manifest submission:\nPatient Family Name was NULL.  Manifest submission will likely fail!!")
} else {
    set(XDSSourcePatientFamilyName, dcmPatientFamilyName)   
}
if(null == dcmPatientFirstName) {
    log.warn("XDS Metadata creation, manifest submission:\nPatient First Name was NULL.  Manifest submission will likely fail!!")
} else {
    set(XDSSourcePatientGivenName, dcmPatientFirstName)
}
if(null == dcmPatientDOB) {
    log.warn("XDS Metadata creation, manifest submission:\nPatient Date Of Birth was NULL.")
} else {
    set(XDSSourcePatientBirthDate, dcmPatientDOB)
}
if(null == dcmPatientSex) {
    log.warn("XDS Metadata creation, manifest submission:\nPatient Sex was NULL.  Manifest submission will likely fail!!")
} else {
    set(XDSSourcePatientSex, dcmPatientSex)
}


if (calledAET == 'VCIAP8') {
    set(XDSPracticeSettingCode,code('R-30248', '1.2.840.10008.2.16.4', 'Cardiology'))
} else{
    set(XDSPracticeSettingCode,code('R-3027B', '1.2.840.10008.2.16.4', 'Radiology'))
}

if (get(StudyDate) != null) {
    set(XDSServiceStartTime, dcmStudyDate, dcmStudyTime)
    set(XDSServiceStopTime, dcmStudyDate, dcmStudyTime)
    set(XDSCreationTime, dcmStudyDate, dcmStudyTime)
} else { 
    set(XDSServiceStartTime, new java.util.Date())
    set(XDSServiceStopTime, new java.util.Date())
    set(XDSCreationTime, new java.util.Date())
}

set(XDSTitle, 'DICOM Study')
set(XDSExtendedMetadata('studyInstanceUID'), dcmStudyInstanceUID)

if(!dcmAccessionNumber) {
    log.warn("XDS Metadata creation, manifest submission:\nAccession Number was NULL.")
} else {
    set(XDSExtendedMetadata('accessionNumber'), dcmAccessionNumber)
}

log.debug("document_metadata_morpher.groovy: End morpher")
