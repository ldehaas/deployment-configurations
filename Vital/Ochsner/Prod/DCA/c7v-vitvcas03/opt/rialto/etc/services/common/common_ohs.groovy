// to reference this class, put LOAD('/opt/rialto/etc/services/common/common_ohs.groovy')
import java.security.MessageDigest

LOAD('/opt/rialto/etc/variables/env_var.groovy')
LOAD('/opt/rialto/etc/services/common/dicomProxy_mapping.groovy')
class OHSCommon {

    private static Def_iPid = 'OHSEPIC'
    private static Def_iPid_UnivId = env_var.Default_OID
    private static Def_UnivEntType = 'ISO'
    private static Ukn_iPid = 'UNKNOWN'
    private static Ukn_iPid_UnivId = env_var.Default_Unknown_OID

    private static HMC_AGFA_STORE_SCP= env_var.hmc_agfa_store_scp
    private static HMC_AGFA_STORE_SCU= env_var.hmc_agfa_store_scu

    private static final aet_to_ipid_map = [
        'VCIAP4':(Def_iPid),
        'VCIAP5':'DDOHSR',
        'VCIAP6':'DDSTPH',
        'VCIAP7':'DDNSSLIDELL',
        'VCIAP8':(Def_iPid),
        'VCIAP9':'DDOHSCN',
        'VCIAP10':'DDOHSCI',
        'VCIAP11':'MHM',
        'VCIAP12':(Def_iPid),
        'VCIAP13':'DDOHSOP',
        'VCIAP14':(Def_iPid),
        'VCIAP15':'DDOHSCPN',
        'VCIAP16':(Def_iPid),
        'VCIAP17':'DDOHSOB',
        'VCIAP18':(Def_iPid),
        'VCIAP19':'DDOHSPOCUS'
    ]

    private static final ipid_to_univId_map = [
        (Def_iPid):(Def_iPid_UnivId),
        'DDOHSR':env_var.DDOHSR_OID,
        'DDSTPH':env_var.DDSTPH_OID,
        'DDNSSLIDELL':env_var.DDNSSLIDELL_OID,
        'DDOHSCN':env_var.DDOHSCN_OID,
        'DDOHSCI':env_var.DDOHSCI_OID,
        'MHM':env_var.MHM_OID,
        'DDOHSOP':env_var.DDOHSOP_OID,
        'DDOHSCPN':env_var.DDOHSCPN_OID,
        'DDOHSOB':env_var.DDOHSOB_OID,
        'DDOHSPOCUS':env_var.DDOHSPOCUS_OID,
        (Ukn_iPid):(Ukn_iPid_UnivId)
    ]

    private static final valid_aet_list = [
        'VCIAP4',
        'VCIAP5',
        'VCIAP6',
        'VCIAP7',
        'VCIAP8',
        'VCIAP9',
        'VCIAP10',
        'VCIAP11',
        'VCIAP12',
        'VCIAP13',
        'VCIAP14',
        'VCIAP15',
        'VCIAP16',
        'VCIAP17',
        'VCIAP18',
        'VCIAP19'
    ]

    private static final clean_aet_list = [
        'VCIAP4',
        'VCIAP8',
        'VCIAP11',
        'VCIAP12',
        'VCIAP14',
        'VCIAP16',
        'VCIAP18'
    ]

    private static final auto_verify_aet_list = [
        'VER4',
        'VER8',
        'VER11',
        'VER12',
        'VER14',
        'VER16',
        'VER18'
    ]

    private static final aet_to_port_map = [
        'VCIAP4':4104,
        'VCIAP8':4108,
        'VCIAP11':4111,
        'VCIAP12':4112,
        'VCIAP14':4114,
        'VCIAP16':4116,
        'VCIAP18':4118
    ]

    private static final remove_privateTag_aet_list = [
        'HMSHCDSO01',
        'CD1NMCH',
        'MEDIAWRITER4',
        'CONMED1NSMH',
        'MEDIAWRITERBRMH'
    ]

    static GetiPIDbyAET(AET){
        if (OHSDP.isAgfaCalledAET(AET)) {
            AET = "VCIAP4"
        }
        def iPid = aet_to_ipid_map[AET]
        if (iPid == null) {
            iPid = Ukn_iPid
        }
        return iPid
    }

    static IsValidiPid(iPid){
        if (iPid == Ukn_iPid){
            return false
        }else{
            def UnivID = ipid_to_univId_map[iPid]
            if (UnivID == null || UnivID == ''){
                return false
            }else{
                return true
            }
        }
    }

    static GetunivIdbyiPID(iPid){
        def UnivID = ipid_to_univId_map[iPid]
        if (UnivID == null) {
            UnivID = ipid_to_univId_map[Ukn_iPid]
        } 
        return UnivID
    }

    static GetDefiPid(){
        return Def_iPid
    }

    static GetDefiPidUnivId(){
        return Def_iPid_UnivId
    }

    static IsValidAE(AET){
        return valid_aet_list.contains(AET)
    }

    static IsCleanAE(AET){
        return clean_aet_list.contains(AET)
    }

    static IsDirtyAE(AET){
        return IsValidAE(AET) && !IsCleanAE(AET)
    }

    static GetPortByAET(AET){
        def dcmPort = aet_to_port_map[AET]
        if (dcmPort == null){
            dcmPort = 0
        }
        return dcmPort
    }

    static GetHMCAgfaStoreSCU(){
        return HMC_AGFA_STORE_SCU
    }

    static GetHMCAgfaStoreSCP(){
        return HMC_AGFA_STORE_SCP
    }

    static IsRemovePrivateTag(AET){
        return remove_privateTag_aet_list.contains(AET)
    }

    static IsAutoVerifyAET(AET){
        return auto_verify_aet_list.contains(AET)
    }

}

class ValueHasher {
    static String hashValue(String sourceValue, Integer length = 32) {
        if (!sourceValue) {
            return null
        }
        Byte[] outputValue = MessageDigest.getInstance("MD5").digest(sourceValue.getBytes())

        StringBuilder sb = new StringBuilder()
        for (byte b : outputValue) {
            sb.append(String.format("%02X", b))
        }
        return sb.toString().take(length)
    }
}

