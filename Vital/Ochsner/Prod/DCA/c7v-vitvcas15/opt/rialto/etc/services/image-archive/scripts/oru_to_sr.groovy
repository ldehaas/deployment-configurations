/**
 * converts HL7 ORU to a DICOM SR object for Medseek
 * draft according to the SAD v1.5
 *
 * many of the HL7 locations are potentially different in the field. this
 * script is simply a starting point.
 */
import org.dcm4che2.util.UIDUtils
import org.joda.time.DateTime

log.debug("oru-2-sr.groovy: Start morpher")
//log.debug("oru-2-sr: input = {}", input)


LOAD("common.groovy")

set(SpecificCharacterSet, 'ISO_IR 100')
set(InstanceCreationDate, new DateTime())
set(InstanceCreationDate, new DateTime())
set(InstanceCreatorUID, '1.2.3.4.5.6.7.8.9')
set(Modality, 'SR')
set(SOPClassUID, '1.2.840.10008.5.1.4.1.1.88.11') // basic SR
set(ValueType, 'CONTAINER')
set(ContinuityOfContent, 'CONTINUOUS')
set('ConceptNameCodeSequence/CodeValue', '18748-4')
set('ConceptNameCodeSequence/CodingSchemeDesignator', 'LN')
set('ConceptNameCodeSequence/CodeMeaning', 'Diagnostic Imaging Report')
set(InstanceNumber, '1')
set(SeriesNumber, '1')

/* PID SEGMENT */
setPersonName(PatientName, 'PID-5') //
setPersonName('PatientMotherBirthName', 'PID-6')
set(PatientBirthDate, get('PID-7')) //
set(PatientSex, get('PID-8')) //
set('PatientAddress', get('PID-11'))
set('PatientTelephoneNumbers', get('PID-13'))
set(PatientID, get('PID-3-1'))
set(IssuerOfPatientID, get('PID-3-4-1')) // or inferred

//log.debug("oru-2-sr: PatientID = {}", get('PID-3-1'))

/* OBR SEGMENT */
set(AccessionNumber, get('ORC-3')) //

//4-1, 4-2 present, 4-4, 4-6 missing
set(StudyDescription, get('OBR-4-1')+'^'+get('OBR-4-2')) //+'^'+get('OBR-4-4')+'^'+get('OBR-4-6'))

def studyDate = DICOMTime.parseDate(get('OBR-6'))
def studyTime = DICOMTime.parseTime(get('OBR-6'))
set(StudyDate, studyDate)
set(StudyTime, studyTime)
set(SeriesDate, studyDate)
set(SeriesTime, studyTime)

def contentDate = get('OBR-22') != null ? DICOMTime.parseDate(get('OBR-22')) : studyDate
def contentTime = get('OBR-22') != null ? DICOMTime.parseTime(get('OBR-22')) : studyTime

set(ContentDate, contentDate)
set(ContentTime, contentTime)

set('AnatomicRegionSequence/CodeValue', get('OBR-15-1-1'))
set('AnatomicRegionSequence/CodeMeaning', get('OBR-15-1-2'))
set('AnatomicRegionSequence/CodingSchemeDesignator', get('OBR-15-1-3'))
setPersonName(ReferringPhysicianName, 'OBR-16') //
set(StudyID, get('OBR-20'))

set('VerifyingObserverSequence/VerifyingOrganization', get('MSH-4'))
set('VerifyingObserverSequence/VerificationDateTime', get('OBR-7'))
set('VerifyingObserverSequence/VerifyingObserverName', get('OBR-32-2')+ '^' + get('OBR-32-3'))
seq = 'VerifyingObserverSequence/VerifyingObserverIdentificationCodeSequence'
set(seq+'/CodeValue', get('OBR-32-1'))
set(seq+'/CodingSchemeDesignator', get('MSH-4'))
set(seq+'/CodeMeaning', get('OBR-32-2')+ '^' + get('OBR-32-3'))


set('PerformedProcedureCodeSequence/CodeValue', get('OBR-15-1-1'))

/**
 * use obr-25 to see if this is a final report or not
 * if final then set both the completionFlag and the preliminaryFlag accordingly. 
*/
def completionFlag = get('OBR-25')
if( completionFlag != null ) {
    if( completionFlag.toUpperCase().equals('FINAL') ) {
        //supposedly this is invalid
        //set('PerformedProcedureCodeSequence/CompletionFlag', 'COMPLETE')
        set('CompletionFlag', 'COMPLETE')
        set('PreliminaryFlag', 'FINAL')
        set('ContentLabel', 'FINAL')
    } else if( completionFlag.toUpperCase().equals('PARTIAL') ) {
        //supposedly this is invalid
        //set('PerformedProcedureCodeSequence/CompletionFlag', 'PARTIAL')
        set('CompletionFlag', 'PARTIAL')
        set('PreliminaryFlag', 'PRELIMINARY')
        set('ContentLabel','PRELIMINARY')
    } else {
        set('ContentLabel','UNSPECIFIED')
        log.warn('Completion flag unrecognized')
    }
}

set('VerifyingObserverIdentificationCodeSequence/CodeValue', get('OBR-32-1')) //
setPersonName('VerifyingObserverIdentificationCodeSequence/VerifyingObserverName', 'OBR-32-2') //Looks like this should join across OBR-32-2+


set('ReferencedPerformedProcedureStepSequence', [])

/* OBX SEGMENT */
def verificationFlag = get('OBX-11')
if( verificationFlag != null ) {
    if( verificationFlag.equals('F') ) {
        set('VerificationFlag', 'VERIFIED')
    } else if( verificationFlag.equals('P') ) {
        set('VerificationFlag', 'UNVERIFIED')
    } else {
        log.warn('Verification flag unrecognized');
    }
}
set('ContentSequence[1]/RelationshipType', 'HAS CONCEPT MOD')
set('ContentSequence[1]/ValueType', 'CODE')
set('ContentSequence[1]/ConceptNameCodeSequence/CodeValue', '121058')
set('ContentSequence[1]/ConceptNameCodeSequence/CodingSchemeDesignator', 'DCM')
set('ContentSequence[1]/ConceptNameCodeSequence/CodeMeaning', 'Procedure Reported')

set('ContentSequence[1]/ConceptCodeSequence/CodeValue', get('OBR-4-1'))
set('ContentSequence[1]/ConceptCodeSequence/CodingSchemeDesignator', 'RP')
set('ContentSequence[1]/ConceptCodeSequence/CodeMeaning', get('OBR-4-2'))

set('ContentSequence[2]/RelationshipType', 'HAS CONCEPT MOD')
set('ContentSequence[2]/ValueType', 'CODE')
set('ContentSequence[2]/ConceptNameCodeSequence/CodeValue', '121049')
set('ContentSequence[2]/ConceptNameCodeSequence/CodingSchemeDesignator', 'DCM')
set('ContentSequence[2]/ConceptNameCodeSequence/CodeMeaning', 'Language of Content Item and Descendants')

set('ContentSequence[2]/ConceptCodeSequence/CodeValue', 'en') // or fr?
set('ContentSequence[2]/ConceptCodeSequence/CodingSchemeDesignator', 'RFC3066')
set('ContentSequence[2]/ConceptCodeSequence/CodeMeaning', 'English') // or french


set('ContentSequence[3]/RelationshipType', 'HAS OBS CONTEXT')
set('ContentSequence[3]/ValueType', 'CODE')
set('ContentSequence[3]/ConceptNameCodeSequence/CodeValue', '121005')
set('ContentSequence[3]/ConceptNameCodeSequence/CodingSchemeDesignator', 'DCM')
set('ContentSequence[3]/ConceptNameCodeSequence/CodeMeaning', 'Observer Type')

set('ContentSequence[3]/ConceptCodeSequence/CodeValue', '121006')
set('ContentSequence[3]/ConceptCodeSequence/CodingSchemeDesignator', 'DCM')
set('ContentSequence[3]/ConceptCodeSequence/CodeMeaning', 'Person')

set('ContentSequence[4]/RelationshipType', 'HAS OBS CONTEXT')
set('ContentSequence[4]/ValueType', 'CODE')
set('ContentSequence[4]/ConceptNameCodeSequence/CodeValue', '121011')
set('ContentSequence[4]/ConceptNameCodeSequence/CodingSchemeDesignator', 'DCM')
set('ContentSequence[4]/ConceptNameCodeSequence/CodeMeaning', 'Person Observer\'s Role in this Procedure')

set('ContentSequence[4]/ConceptCodeSequence/CodeValue', '121097')
set('ContentSequence[4]/ConceptCodeSequence/CodingSchemeDesignator', 'DCM')
set('ContentSequence[4]/ConceptCodeSequence/CodeMeaning', 'Recording')


set('ContentSequence[5]/RelationshipType', 'CONTAINS')
set('ContentSequence[5]/ValueType', 'CONTAINER')
set('ContentSequence[5]/ConceptNameCodeSequence/CodeValue', '121064')
set('ContentSequence[5]/ConceptNameCodeSequence/CodingSchemeDesignator', 'DCM')
set('ContentSequence[5]/ConceptNameCodeSequence/CodeMeaning', 'Current Procedure Description')
set('ContentSequence[5]/ContinuityOfContent', 'CONTINUOUS')

seq = 'ContentSequence[5]/ContentSequence'

set(seq+'/RelationshipType', 'CONTAINS')
set(seq+'/ValueType', 'TEXT')
set(seq+'/ConceptNameCodeSequence/CodeValue', '121065')
set(seq+'/ConceptNameCodeSequence/CodingSchemeDesignator', 'DCM')
set(seq+'/ConceptNameCodeSequence/CodeMeaning', 'Procedure Description')
// KHC11965 - Change Study Description to only use OBR-4-2
// set(seq+'/TextValue', get('OBR-4-1') + '^' + get('OBR-4-2'))
set(seq+'/TextValue', get('OBR-4-2'))

set('ContentSequence[6]/RelationshipType', 'CONTAINS')
set('ContentSequence[6]/ValueType', 'CONTAINER')
set('ContentSequence[6]/ConceptNameCodeSequence/CodeValue', '121070')
set('ContentSequence[6]/ConceptNameCodeSequence/CodingSchemeDesignator', 'DCM')
set('ContentSequence[6]/ConceptNameCodeSequence/CodeMeaning', 'Findings')
set('ContentSequence[6]/ContinuityOfContent', 'CONTINUOUS')

seq = 'ContentSequence[6]/ContentSequence'
set(seq+'/ValueType', 'TEXT')
set(seq+'/RelationshipType', 'CONTAINS')
set(seq+'/ConceptNameCodeSequence/CodeValue', '121071')
set(seq+'/ConceptNameCodeSequence/CodingSchemeDesignator', 'DCM')
set(seq+'/ConceptNameCodeSequence/CodeMeaning', 'Finding')
set(seq+'/ContinuityOfContent', 'CONTINUOUS')
def reportTextSegments = getList("OBX(*)-5")
Collections.replaceAll(reportTextSegments, null, "\r\n")
def reportText = reportTextSegments.join('\n')
reportText = DICOMText.replaceEscapedText(reportText)
set(seq+'/TextValue', reportText)
log.debug("oru-2-sr.groovy: End morpher")
