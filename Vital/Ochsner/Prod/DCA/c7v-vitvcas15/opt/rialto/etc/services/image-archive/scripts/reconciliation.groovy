// This scripts is currently for both manual and automatic reconciliation.
// if MWLCFindQueryMorpher isn't specified for image archive, then AccessionNumber, PatientID and issuer of PatientID are 
// used to determine whether a matching MWL is determined. In this script, mwlEntry represents the matching MWL order.

// Note: in this script if you want to set value to a DICOM tag by specifying tag number, you need to do one of these:
//  set("00080015", "Arm and Leg")
//  set("00359999", "Arm and Leg", VR.SH)
//  Referencing the tag by tag(0x0035,0x9999) or 0x00359999 would not work because this script uses a different set method com.karos.groovy.morph.mint.MintScriptingAPI.set() 
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
def studycoercion = LoggerFactory.getLogger("studycoercion")

def scriptName = "Reconciliation Script - "

log.debug(scriptName + "Starting reconciling SOP {} with the matching MWL entry.", get('SOPInstanceUID'));

set('AccessionNumber', mwlEntry.get('AccessionNumber'))
set('IssuerOfAccessionNumberSequence/LocalNamespaceEntityID', mwlEntry.get('IssuerOfAccessionNumberSequence/LocalNamespaceEntityID'))
set('IssuerOfAccessionNumberSequence/UniversalEntityID', mwlEntry.get('IssuerOfAccessionNumberSequence/UniversalEntityID'))
set('IssuerOfAccessionNumberSequence/UniversalEntityIDType', mwlEntry.get('IssuerOfAccessionNumberSequence/UniversalEntityIDType'))

set('ReferringPhysicianName', mwlEntry.get('ReferringPhysicianName'))

set('PatientID', mwlEntry.get('PatientID'))
set('IssuerOfPatientID', mwlEntry.get('IssuerOfPatientID'))
set('IssuerOfPatientIDQualifiersSequence/UniversalEntityID', mwlEntry.get('IssuerOfPatientIDQualifiersSequence/UniversalEntityID'))
set('IssuerOfPatientIDQualifiersSequence/UniversalEntityIDType', mwlEntry.get('IssuerOfPatientIDQualifiersSequence/UniversalEntityIDType'))

set('PatientName', mwlEntry.get('PatientName'))
set('PatientSex', mwlEntry.get('PatientSex'))
set('PatientBirthDate', mwlEntry.get('PatientBirthDate'))
//KHC14775 - remove the following line so PatientWeight from modality is not overwritten
//set('PatientWeight', mwlEntry.get('PatientWeight'))
set('PatientState', mwlEntry.get('PatientState'))

set('MedicalAlerts', mwlEntry.get('MedicalAlerts'))
set('RequestingPhysician', mwlEntry.get('RequestingPhysician'))

set('RequestedProcedureID', mwlEntry.get('RequestedProcedureID'))
set('RequestedProcedurePriority', mwlEntry.get('RequestedProcedurePriority'))

set('OrderEnteredBy', mwlEntry.get('OrderEnteredBy'))
set('OrderEntererLocation', mwlEntry.get('OrderEntererLocation'))
set('OrderCallbackPhoneNumber', mwlEntry.get('OrderCallbackPhoneNumber'))

set('AdmissionID', mwlEntry.get('AdmissionID'))
set('IssuerOfAdmissionID', mwlEntry.get('IssuerOfAdmissionID'))

set('RequestedProcedureDescription', mwlEntry.get('RequestedProcedureDescription'))
//set('RequestedProcedureCodeSequence/CodeValue', mwlEntry.get('RequestedProcedureCodeSequence/CodeValue'))
//set('RequestedProcedureCodeSequence/CodingSchemeDesignator', mwlEntry.get('RequestedProcedureCodeSequence/CodingSchemeDesignator'))
//set('RequestedProcedureCodeSequence/CodeMeaning', mwlEntry.get('RequestedProcedureCodeSequence/CodeMeaning'))

//set('BodyPartExamined', mwlEntry.get('BodyPartExamined'))
//set('Laterality', mwlEntry.get('Laterality'))

// preserving requested body part, requested procedure 
log.debug(scriptName + "Preserving order information into private tag {},{},{}", mwlEntry.get("00359999"), mwlEntry.get('Laterality'), mwlEntry.get('BodyPartExamined'))
set("00359999", mwlEntry.get("00359999"), VR.SH)   // specialty code
if (mwlEntry.get('Laterality')){
    set("00359998", mwlEntry.get('Laterality'), VR.SH)   // laterality requested
}else{
    log.debug(scriptName + "laterality is empty")
}
set("00359997", mwlEntry.get('BodyPartExamined'), VR.SH)   // body part requested

// This is a work-around to put the CPL value and place it in both the CPL dicom field and
// the ConfidentialityCode dicom field. This is to allow cfind queries to find study by a value
// that comes in as a CPL but we can't search correctly by CPL so we have to use CC for now.
// this is a work-around and will be removed when a fix is released.
set('CurrentPatientLocation',mwlEntry.get('CurrentPatientLocation'))
log.debug(scriptName + "PatientLocation is {}",mwlEntry.get('CurrentPatientLocation'))
// a workaround to allow EV query by location. 
set('ConfidentialityCode',mwlEntry.get('CurrentPatientLocation'))
set('InstitutionalDepartmentName',mwlEntry.get('CurrentPatientLocation'))


// In this script, a matching MWL is already found, so order rules.
// to keep the values from OBR-2 and OBR-4. Note we are overwriting study description
// 0040,2016
set('PlacerOrderNumberImagingServiceRequest', mwlEntry.get('PlacerOrderNumberImagingServiceRequest'))
// 0008,1030
set('StudyDescription', mwlEntry.get('RequestedProcedureDescription'))
// 0020,000D Study Instance UID

def MWLStudyInstUID = mwlEntry.get('StudyInstanceUID')
def dcmStudyInstUID = get('StudyInstanceUID')

if (MWLStudyInstUID == null){
    log.warn(scriptName + "No study instance UID is defined in MWL entry. Preserving study instance UID from original DICOM study.")
}else{
    if (MWLStudyInstUID != dcmStudyInstUID) {
        log.info(scriptName + "Study instance UID in the original DICOM study is found to be different than the study instanced UID assigned by the MWL order.")
        //log.info(scriptName + "overwriting the original study instance UID {} to {}", dcmStudyInstUID, MWLStudyInstUID)
        // Store native StudyInstanceUID in 0035,9989 before coercing the study instane UID
        //set("00359989", dcmStudyInstUID, VR.UI);
        //set('StudyInstanceUID', MWLStudyInstUID);
        studycoercion.debug("STUDY_TO_COERCE: {}|{}|{}|{}", dcmStudyInstUID, MWLStudyInstUID, mwlEntry.get('PatientID'), mwlEntry.get('AccessionNumber'))
    }
}


log.debug(scriptName + "Finished reconciling SOP {}", get('SOPInstanceUID'));
