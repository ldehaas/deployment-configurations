<?php

        // this is an array of userid's used for the userservice command
        // KHC15248 - easyvizsvc user account added
	$domains = array("svc.easyvizadmin:wXnJ7c_NfuzX", "vitaltestadmin:Hospital1", "easyvizsvc:C2JM5sWa8CxgOLfVRFeZ3p4w65k65r");
        // assign hostname to a variable
        $hostName = gethostname();
        // create curl resource
        $ch = curl_init();
        // build url for the restful call
        $builtUrl = "https://" . $hostName . "/vdb/rest/evz?";
        $url = getURLParams();
        $parts = parse_url($url);
        $foo = parse_str($parts["query"], $parsed);
        $accessionNumber = $parsed["accession_number"];
        $username = $parsed["username"];
        $password = $parsed["password"];
        $outputType = $parsed["output_type"];
        $authzname = $parsed["authzname"];
        $msgId = $parsed["msg_id"];
        $patientId = $parsed["patient_id"];
        $domain = getUserDomain($authzname);
        if ($domain == '') {
                 echo "Unknown userid (" . $authzname . ") provided. Unable to connect to EasyViz";
                 exit;
        }
        if ($patientId != '') {
                $issuerOfPID = "OHSEPIC";
        }
        if (($username == '') || ($password == '')) {
                $username = "admin";
                $password = "Vital123";
        }
        if ($outputType == '') {
                $outputType = "web";
        }
        if ($msgId == '') {
                $msgId = "EV_WORKLIST_SHOW";
        }
        $username = $username . $domain;
        $builtUrl .= "accession_number=" . $accessionNumber;
        $builtUrl .= "&username=" . $username;
        $builtUrl .= "&password=" . $password;
        $builtUrl .= "&output_type=" . $outputType;
        $builtUrl .= "&msg_id=" . $msgId;
        $builtUrl .= "&patient_id_issuer=" . $issuerOfPID;
        $builtUrl .= "&patient_id=" . $patientId;
        $builtUrl .= "&authzuser=" . $authzname;
//echo($builtUrl);
//echo("<br>");
        // set url
        curl_setopt($ch, CURLOPT_URL, $builtUrl);
        //return the transfer as a string
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
        // set the UA
        curl_setopt($ch, CURLOPT_USERAGENT, 'EasyViz Restful Launcher');
        // Alternatively, lie, and pretend to be a browser
        // curl_setopt($ch, CURLOPT_USERAGENT, 'Mozilla/4.0 (compatible; MSIE 7.0; Windows NT 5.1)');
        // $output contains the output string
        $output = curl_exec($ch);
        // close curl resource to free up system resources
        curl_close($ch);
        //redirect to where the restful service responded with.
        //usleep(2000000);
// echo($output);
// echo("<br>");
        header("Location: " . $output);
// echo("<a href='".$output."'>here</a>");
// echo("<br>");
// echo($output);
        function getURLParams() {
                $temp = $_SERVER["REQUEST_URI"];
                return strstr($temp, '?');
        }
        function getURLRoot() {
                $pageURL = 'http';
                if ($_SERVER["HTTPS"] == "on") {
                        $pageURL .= "s";
                }
                $pageURL .= "://";
                if ($_SERVER["SERVER_PORT"] != "80") {
                        $pageURL .= $_SERVER["SERVER_NAME"].":".$_SERVER["SERVER_PORT"];
                } else {
                        $pageURL .= $_SERVER["SERVER_NAME"];
                }
                return $pageURL;
        }

        function getUserDomain($userid) {
                global $domains, $hostName;
		$atSign = '@';
		$evBackend_pre = "EV_BACKEND=http://";
		$evBackend_suffix = $atSign . $hostName . "/vdb";
		$evBackend_export = "";
		$domainName = "";

		foreach ($domains as $login) {
		    $evBackend_export = $evBackend_pre . $login . $evBackend_suffix;
		    putenv($evBackend_export);
		    exec("userservice -u " . $userid . " 2> /tmp/php.txt2", $output);
		    if (count($output) > 0) {
		       $pos = strpos($output[1], $atSign);
		       $domainName = substr($output[1], strpos($output[1], $atSign));
		       break;
		    }
		    unset($output);
		}

		return $domainName;

        }
?>
</body>
</html>
