- type: log
  enabled: true
  paths:
  - /var/log/rialto/metrics*.log*
  json.keys_under_root: true
  json.overwrite_keys: true
  exclude_lines: ['database.read', 'database.write']
  fields:
    product:
      name: vitrea_connection
    log:
      type: rialto_metrics
      content: metric
- type: log
  enabled: true
  paths:
  - /var/log/rialto/rialto.log
  - /var/log/rialto/rialto_java_stderr.log
  - /var/log/rialto/rialto_java_stdout.log
  multiline.pattern: '^[0-9]{4}-[0-9]{2}-[0-9]{2} [0-9]{2}:[0-9]{2}:[0-9]{2},[0-9]{3}'
  multiline.negate: true
  multiline.match: after
  fields:
    product:
      name: vitrea_connection
    log:
      type: rialto_log
      content: diagnostic
