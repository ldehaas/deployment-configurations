def scriptName = "Connect - agfa_cfind_rsp_filter.groovy "

log.info(scriptName + "START")
def studyYears = 5
def numberOfPriorsToReturn = 3
def validPriorList = []

def earliestStudyDate =  new org.joda.time.DateTime().minusYears(studyYears)
log.debug(scriptName + "Calculated earliestStudyDate is '{}'.", earliestStudyDate)

log.debug("Complete list of studies being examined:\n{}",
inputs.collect({
    it.get(StudyInstanceUID) + ": " + it.getDate(StudyDate, StudyTime)
}).join("\n"))

inputs.each {
    if (it.getDate(StudyDate, StudyTime) < earliestStudyDate) {
        log.trace("Prior falls outside of maximum age '{}' and is dropped.", earliestStudyDate)
    } else {
        log.trace("Prior falls inside of maximum age '{}' and is retained.", earliestStudyDate)
        validPriorList.add(it)
    }
}

validPriorList.sort(byRecency)

log.debug("Studies after removing ones that are too old:\n{}",
validPriorList.collect({
    it.get(StudyInstanceUID) + ": " + it.getDate(StudyDate, StudyTime)
}).join("\n"))

// Returns the correct number of priors based on prior age
return first(validPriorList, numberOfPriorsToReturn)

log.info(scriptName + "END")
