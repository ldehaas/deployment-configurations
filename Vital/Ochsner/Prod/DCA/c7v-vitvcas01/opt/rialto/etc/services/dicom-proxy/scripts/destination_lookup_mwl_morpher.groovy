/**
 * This groovy script is used by the dicomProxy service.
 *  It will obtain the calling and Called AETs from the request
 *  If the calling AET is a known AET then we will set the destination to VC
 *  If the calling AET is not known, then we will set the destination to the called AET
 */
LOAD('/opt/rialto/etc/services/common/dicomProxy_mapping.groovy')

def scriptName = "DicomProxy Destination MWL Morpher - CallingAET=" + getCallingAETitle() + ", calledAET=" + getCalledAETitle() + ":"

log.info(scriptName + "START")

def callingAET = getCallingAETitle().toUpperCase();
def calledAET = getCalledAETitle().toUpperCase();
// set a default destionation to one of the VC pools AET
def defaultDestination = "VCIA_MWL"
def destinationAET;

log.debug(scriptName + "CalledAET {}", calledAET);

// use the isMwlCallingAET method to see if the calling AET is known
if ( OHSDP.isMwlCallingAET(callingAET) ) {
    destinationAET = defaultDestination;
    log.info(scriptName + "CallingAET is recognized in dp_mwl_calling_aet, destination will be {}", destinationAET)

} else if ( OHSDP.isAgfaCalledAET(calledAET) ) {
    destinationAET = calledAET;
    log.info(scriptName + "CallingAET is NOT recognized in dp_mwl_calling_aet. Use CalledAET in dp_called_aet to proxy, destination will be {}", destinationAET)
} else {
    log.info(scriptName + "unable to proxy this request. Both CalledAET {} and CallingAET {} are not defined", calledAET, callingAET)
    return null;
}

log.info(scriptName + "END");

return destinationAET

