/*
 * HL7 to cfind morpher
 * Convert an order message into a cfind message.
 * If Procedure Code is in AVCodes.txt it will search based on Patient ID to find priors
 * Otherwise it will only search for current study based on Accession Number / Study UID
 */
LOAD('/opt/rialto/etc/services/connect/scripts/AVCodes.groovy')
//import AVCodes
String scriptName = "Connect - fwd_hl7_to_cfind_morpher.groovy"
String scriptVersion = "v04"

log.info(scriptName + " " + scriptVersion + ": Start")

String studyInstUID = get('/.ZDS-1')
studyInstUID = ''

String studyProcedure = get('OBR-4-2')
// Due to a defect in Connect service query must contain Patient ID so PID always part of search
String patientID = get('PID-3-1')
set(PatientID,patientID)
set(IssuerOfPatientID, get('PID-3-4-1'))
set('IssuerOfPatientIDQualifiersSequence/UniversalEntityID', get('PID-3-4-2'))
set('IssuerOfPatientIDQualifiersSequence/UniversalEntityIDType', get('PID-3-4-3'))
log.info(scriptName + "Setting Patient ID to {} in C-Find-Rq", patientID)
set(StudyDescription, '')

if (AVCodes.priorCodes.contains(studyProcedure)) { // Study should trigger fetching of priors
    // No additional conditions beyond Patient ID
} else if (studyInstUID) { // Only current study retrieved based on Study UID
        set(StudyInstanceUID,studyInstUID)
        log.info(scriptName + "Setting StudyInstanceUID to {} in C-Find-Rq", studyInstUID)
} else {
    String accNum = get('/.ORC-3')
    if (accNum != null){ // Only current study retrieved based on Accession Number
        set(AccessionNumber,accNum)
        set(StudyInstanceUID,'')
        log.warn(scriptName + "cannot find study instance UID in ZDS-1. Setting AccessionNumber to {} in C-Find-Rq", accNum)
    } else {
        log.error(scriptName + "cannot find study instance UID or AccessionNumber. The ORM must be invalid!!")
        return false
    }
}

log.info(scriptName + "End")
