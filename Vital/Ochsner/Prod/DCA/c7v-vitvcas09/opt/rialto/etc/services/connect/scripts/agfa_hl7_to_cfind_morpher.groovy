/*
 * HL7 to cfind morpher
 * Convert an order message into a cfind message.
 * get() works on the order message, while set() works on the 
 * cfind message.
 * 
 * The order is ignored if this script returns false,
 * e.g. no images will be fetched for the order.
 */
LOAD('/opt/rialto/etc/services/common/common_ohs.groovy')

def scriptName = "Connect - agfa_hl7_to_cfind_morpher.groovy "

log.info(scriptName + "Start")

def orig_PID = get('/.PID-3-1')
if (orig_PID){
    def out_PID
    if (orig_PID.length() > 0 && orig_PID.length() < 7) {
        // Pad leading 0 to PID to match Agfa's format
        out_PID = orig_PID.padLeft(7,'0')
    }else{
        out_PID = orig_PID
    }
    set("PatientID", out_PID)

    def out_issuerOfPatientID = OHSCommon.GetDefiPid()
    def out_universalEntityID = OHSCommon.GetDefiPidUnivId()
    def out_universalEntityType = ('ISO')

    set('IssuerOfPatientID', out_issuerOfPatientID)
// IssuerOfPatientIDQualifiersSequence is not mandatory for this C-Find-RQ to pass to the source.
//    set('IssuerOfPatientIDQualifiersSequence/UniversalEntityID', out_universalEntityID)
//    set('IssuerOfPatientIDQualifiersSequence/UniversalEntityIDType', out_universalEntityType)

    set("StudyTime", null)
    def cfEndDate = new Date()
    set("StudyDate", "20181220" + "-" + cfEndDate.format('yyyyMMdd'))

    log.info(scriptName + "converting HL7 to cfind query with patient id {}, study date start {} and end {}.",out_PID,cfStartDate.format('yyyyMMdd'),cfEndDate.format('yyyyMMdd'))

    set(StudyInstanceUID, null)
    set(AccessionNumber, null)

    set(NumberOfStudyRelatedSeries, null)
    set(StudyDescription, null)
    set(BodyPartExamined, null)

}else{
    return false
}

log.info(scriptName + "End")
