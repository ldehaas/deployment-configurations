import java.time.LocalDateTime
import java.time.temporal.ChronoUnit
import java.time.format.DateTimeFormatter
import java.time.format.DateTimeParseException


// NOTE: Please be aware that this script is referenced from multiple image archive services. 
// Every scenario should work for multiple image archives.
// NOTE: IPID stamping is exclusively based on called AE title! The IPID in the original incoming DICOM tag is ignored.

log.debug("in.groovy: Start inbound morpher")

def callingAET = getCallingAETitle().toUpperCase();
def calledAET = getCalledAETitle().toUpperCase();
def sopIUID = get("SOPInstanceUID")
if (sopIUID == null) {return false}


def Def_iPid = 'OHSEPIC'
def Def_UnivId = '2.16.124.113638.7.6.1.5104'

def scriptName = "in.groovy [" + calledAET + "]:"

log.info(scriptName + "SOP inst UID: {}, callingAET is {} and calledAET is {}", sopIUID, callingAET, calledAET)
set(ReceivingAE, calledAET)

def new_ipid = ''
def new_univId = ''

def old_issuerOfPatientId = get(IssuerOfPatientID)
def old_universalEntityID = get("IssuerOfPatientIDQualifiersSequence/UniversalEntityID")
def old_universalEntityIDType = get("IssuerOfPatientIDQualifiersSequence/UniversalEntityIDType")

log.debug(scriptName + "original IssuerOfPatientId is {} ", old_issuerOfPatientId)

if (old_issuerOfPatientId == Def_iPid){
    if (old_universalEntityID == Def_UnivId){
        log.info(scriptName + "No need to morph the original iPid!")
    }else{
        new_ipid = old_issuerOfPatientId
        new_univId = Def_UnivId
    }
}else{
    // Data from modality or sources other than dirty pool
    new_ipid = Def_iPid
    new_univId = Def_UnivId
}

if (new_ipid != '' || new_univId != ''){
    // Preserving exisitn IPID info
    if ((old_issuerOfPatientId != null && old_issuerOfPatientId != Def_UnivId) || old_universalEntityID != null || old_universalEntityIDType != null) {
        log.info(scriptName + "SOP Inst UID " + sopIUID + ": moving existing issuer information [IssuerOfPatientID: " + old_issuerOfPatientId + ", UniversalEntityID: " + old_universalEntityID + ", old_universalEntityIDType: " + old_universalEntityIDType + "] to private tag.")

        set(0x00350010, "VITAL IMAGES PRESERVED INFO 1.0", VR.LO)
        set(0x00351010, old_issuerOfPatientId, VR.LO)
        set(0x00351011, old_universalEntityID, VR.LO)
        set(0x00351012, old_universalEntityIDType, VR.LO)
    }

    log.info(scriptName + "SOP Inst UID " + sopIUID + ":: setting namespace to:{}", new_ipid)
    set("IssuerOfPatientID", new_ipid)

    log.info(scriptName + "SOP Inst UID " + sopIUID + ":: setting universal id to:{}", new_univId)
    set("IssuerOfPatientIDQualifiersSequence/UniversalEntityID", new_univId)

    log.info(scriptName + "SOP Inst UID " + sopIUID + ":: setting universal id type to: ISO")
    set("IssuerOfPatientIDQualifiersSequence/UniversalEntityIDType", "ISO")

}


def orig_PID = get("PatientID")
def out_PID = null

// This will check the PID of the incoming image. If it has leading zeros
// then this code will remove them. 
if (orig_PID != null && orig_PID.substring(0,1) == '0') {
    out_PID = orig_PID.replaceFirst("^0+(?!\$)","")
    set("PatientID", out_PID)
    set(0x00351111, orig_PID, VR.LO)
    log.info(scriptName + "Patient ID has leading zeros, PID updated to {}",out_PID)
}
      

def patientDateOfBirth = get(PatientBirthDate)
def patientTimeOfBirth = get(PatientBirthTime)
def studyDate = get(StudyDate)
def studyTime = get(StudyTime)
def patientAge = get(PatientAge)

// Patient weight is blank (RIALTO-11950)
if ((get(PatientWeight) == '') || (get(PatientWeight) == null)) {
    remove(PatientWeight)
}

try {
    if (patientAge == null) {
        set("PatientAge", calculateAge(patientDateOfBirth, patientTimeOfBirth, studyDate, studyTime))
    }
} catch (Exception e) {
    log.warn("Failed to compute PatientAge. Proceeding without changing PatientAge. ", e)
}

// remove the Instance  availability tag, this should not be sent in but
// if it does we will remove it.
remove(InstanceAvailability)


String calculateAge(String patientDateOfBirth, String patientTimeOfBirth, String studyDate, String studyTime){
    def TIPPING_WEEKS_TO_DAYS = 4
    def TIPPING_MONTHS_TO_WEEKS = 3
    def TIPPING_YEARS_TO_MONTHS = 2
    
    if (patientDateOfBirth == null || studyDate == null) {
        return null
    }

    patientDateOfBirth = patientDateOfBirth.replace(":", "")
    studyDate = studyDate.replace(":", "")

    def patientDateTimeOfBirth
    def studyDateTime

    try {
        def dtf = DateTimeFormatter.ofPattern("yyyyMMddHHmmss")
        patientDateTimeOfBirth = LocalDateTime.parse(patientDateOfBirth + cleanTime(patientTimeOfBirth), dtf)
        studyDateTime = LocalDateTime.parse(studyDate + cleanTime(studyTime), dtf)
    } catch (DateTimeParseException e) {
        log.warn("in.groovy: Failed to parse PatientBirthDate or StudyDate.", e)
        return null
    }

    if (!patientDateTimeOfBirth.isBefore(studyDateTime)) {
        return null
    }

    def years = ChronoUnit.YEARS.between(patientDateTimeOfBirth, studyDateTime)
    def months = ChronoUnit.MONTHS.between(patientDateTimeOfBirth, studyDateTime)
    def weeks = ChronoUnit.WEEKS.between(patientDateTimeOfBirth, studyDateTime)
    def days = ChronoUnit.DAYS.between(patientDateTimeOfBirth, studyDateTime)
    def age = ""

    if (years > 999) {
        return null
    }
    if (years >= TIPPING_YEARS_TO_MONTHS) {
        age = years + "Y"
    } else if (months >= TIPPING_MONTHS_TO_WEEKS) {
        age = months + "M"
    } else if (weeks >= TIPPING_WEEKS_TO_DAYS) {
        age = weeks + "W"
    } else {
        age = days + "D"
    }

    return age.padLeft(4, "0")
}

String cleanTime(String str) {
    if (str == null) {
        return "000000"
    }
    str = str.replace(":", "") 
    if (str.indexOf(".") == 6) {
        str = str.substring(0, 6)
    }
    if (str.contains(".") || str.length() > 6 || str.length()%2 == 1) {
        return "000000"
    }
    return str.padRight(6, "0")
}

String genIUID() {
    def uuidString = UUID.randomUUID().toString()
    def uidString = ''
    def uidRoot = '1.2.840.113747.20080222' // This is the Vital UID root. You could use this or a site-specific one.

    uuidString.each { digit ->
        if (digit == '-') {
            uidString += '.'
        }
        else {
            uidString += Integer.parseInt(digit, 16).toString()
        }
    }
    def newUID = uidRoot + '.' + uidString 
    return newUID
}

log.debug("in.groovy: End inbound morpher")

