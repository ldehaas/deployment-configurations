#!/bin/bash
#
###############################################################################################
# Use this script along with the following input files to spawn rialto instance configurations:
# * Template file template-image-archive.xml for the following services  
#       - report-repo
#       - xds-repo
#       - image-archive
# * Template file template-admin-tools.xml for the following services
#       - navigator.server 
#       - navigator.plugin.download
#       - navigator.plugin.cda
#       - rialto.ui
# * Nginx configuration template default.conf. This script will generate default.conf with
#   all entries and ports required in nginx configuration
#
##############################################################################################
# IMPORTANT NOTES
#       1. When generating new xml files, this script will overwrite existing ones.
#       2. Avoid making any manual changes to the files generated! 
#       3. When copying configs from test to prod or migration, follow the instruction below!
##############################################################################################
## UPDATE HISTORY
##      2018.05.01      Yi      Created script
##      2018.05.14      Yi      Differentiate configuration parameters based on clean/dirty pool
##      2018.05.28      Yi      Separated out ipid from fq_pid and build fq_pid based on mapping
##      2018.06.15      Yi      Corrected IA AE title for navigator
##      2018.06.18      Yi      Include ServerName in admintools to display pool name
##      2018.06.20      Yi      Support deployment type [P]roduction,[T]est or [M]igration
##      2018.06.21      Yi      Remove MWL and SCN for all pools in Migration deployment
##      2018.06.27      Yi      Auto generating PID domain property for admin tools
##      2018.07.10      Yi      Including SiteOID and DomainUUID correction to match production/test
##      2018.07.24      Yi      Remove report repo and include ORU2SR
##      2017.07.31      Yi      Add Agfa AE title manipulation in groovy for prod and test
##      2018.08.24      Yi      Add section to comment/uncomment devices in xml for prod and test
##      2018.09.04      Yi      Add section for migration to reflect production path and migration path
##############################################################################################

################
dep_mode="M"
################
#
# Instruction for promoting configuration from Test Environment to Production/Migration
#
# 1. Keep the local version of rialto.lic, ./variables/cassandra.xml and ./variables/elastic.xml
# 2. Run this script with correct deployment mode! P=Production, M=Migration and T=Test
# 3. Confirm the followings:
#    a - ./devices/common.xml and ./services/health-check.xml reflect the correct environment
#    b - context-manager.xml and authentication.xml have correct root OID reference
#        Prod OID = 2.16.124.113638.7.6.1.1              Test OID = 2.25.65066631204490692481020178991382140296
#    c - ./variables/common.xml and ./services/common/common_ohs.groovy have correct Domain UUIDs
#        Prod UUID = 2.16.124.113638.7.6.1.51xx          Test UUID = 2.16.124.113638.7.6.0.51xx
#    d - (Best Practise) Run grep -r with OID and UUID to ensure that they are correct everywhere else in the configuration.
# 4. For migration ONLY, manually turn off modality-worklist, health-check, connect services and empi


if [[ ! -f template-image-archive.xml ]] ; then
    echo "Template file template-image-archive.xml does not exist! Exiting..."
    exit
fi

if [[ ! -f template-admin-tools.xml ]] ; then
    echo "Template file template-admin-tools.xml does not exist! Exiting..."
    exit
fi

if [[ ! -f template-default.conf ]] ; then
    echo "Nginx configuration template-default.conf does not exist! Exiting..."
    exit
fi


echo "*** Automatic configuration for Ochsner Health ***"
echo "# open ports for each pool instances" > firewall_update.txt
echo "# Add these lines to the bottom of /usr/local/etc/firewall.cfg " >> firewall_update.txt
echo "# Nginx config for multiple admin tools instances" > default.conf

declare -A Domain_UID

if [ $dep_mode == "M" -o $dep_mode == "Migration" -o $dep_mode == "P" -o $dep_mode == "Production" ]; then
    Domain_UID[UNKNOWN]=2.16.124.113638.7.6.1.5100
    Domain_UID[OHSEPIC]=2.16.124.113638.7.6.1.5104
    Domain_UID[DDOHSR]=2.16.124.113638.7.6.1.5105
    Domain_UID[DDSTPH]=2.16.124.113638.7.6.1.5106
    Domain_UID[DDNSSLIDELL]=2.16.124.113638.7.6.1.5107
    Domain_UID[DDOHSCN]=2.16.124.113638.7.6.1.5109
    Domain_UID[DDOHSCI]=2.16.124.113638.7.6.1.5110
    Domain_UID[MHM]=2.16.124.113638.7.6.1.5111
    Domain_UID[DDOHSOP]=2.16.124.113638.7.6.1.5113
    Domain_UID[DDOHSCPN]=2.16.124.113638.7.6.1.5115
    Domain_UID[DDOHSOB]=2.16.124.113638.7.6.1.5117
    Domain_UID[DDOHSPOCUS]=2.16.124.113638.7.6.1.5119

    ## Correct site OID and Domain UID references in xml and groovy to production
    sed -i -e "s/2\.25\.65066631204490692481020178991382140296/2\.16\.124\.113638\.7\.6\.1\.1/g" ./services/authentication.xml
    sed -i -e "s/2\.25\.65066631204490692481020178991382140296/2\.16\.124\.113638\.7\.6\.1\.1/g" ./services/context-manager.xml
    sed -i -e "s/2\.16\.124\.113638\.7\.6\.0\.51/2\.16\.124\.113638\.7\.6\.1\.51/g" ./variables/common.xml
    sed -i -e "s/2\.16\.124\.113638\.7\.6\.0\.51/2\.16\.124\.113638\.7\.6\.1\.51/g" ./services/common/common_ohs.groovy
    sed -i -e "s/HMC_AGFA_STORE_SCP='VAP-PACSTSTOHSM'/HMC_AGFA_STORE_SCP='NWG2AOFH'/g" ./services/common/common_ohs.groovy
    sed -i -e "s/HMC_AGFA_STORE_SCU='VAP-PACSTSTOHSM'/HMC_AGFA_STORE_SCU='AS2AOFH'/g" ./services/common/common_ohs.groovy

    ## Comment and Uncomment device declarations for prod
    sed -i -e "s/<!-- Test Section Begin -->/<!-- Test Section Begin ###/g" \
           -e "s/<!-- Test Section End -->/#### Test Section End -->/g" \
           -e "s/<!-- Production Section Begin ###/<!-- Production Section Begin -->/g" \
           -e "s/#### Production Section End -->/<!-- Production Section End -->/g" \
        ./devices/common.xml

elif [ $dep_mode == "T" -o $dep_mode == "Test" ] 
then
    Domain_UID[UNKNOWN]=2.16.124.113638.7.6.0.5100
    Domain_UID[OHSEPIC]=2.16.124.113638.7.6.0.5104
    Domain_UID[DDOHSR]=2.16.124.113638.7.6.0.5105
    Domain_UID[DDSTPH]=2.16.124.113638.7.6.0.5106
    Domain_UID[DDNSSLIDELL]=2.16.124.113638.7.6.0.5107
    Domain_UID[DDOHSCN]=2.16.124.113638.7.6.0.5109
    Domain_UID[DDOHSCI]=2.16.124.113638.7.6.0.5110
    Domain_UID[MHM]=2.16.124.113638.7.6.0.5111
    Domain_UID[DDOHSOP]=2.16.124.113638.7.6.0.5113
    Domain_UID[DDOHSCPN]=2.16.124.113638.7.6.0.5115
    Domain_UID[DDOHSOB]=2.16.124.113638.7.6.0.5117
    Domain_UID[DDOHSPOCUS]=2.16.124.113638.7.6.0.5119

    ## Correct site OID and Domain UUID references in xml
    sed -i -e "s/2\.16\.124\.113638\.7\.6\.1\.1/2\.25\.65066631204490692481020178991382140296/g" ./services/authentication.xml
    sed -i -e "s/2\.16\.124\.113638\.7\.6\.1\.1/2\.25\.65066631204490692481020178991382140296/g" ./services/context-manager.xml
    sed -i -e "s/2\.16\.124\.113638\.7\.6\.1\.51/2\.16\.124\.113638\.7\.6\.0\.51/g" ./variables/common.xml
    sed -i -e "s/2\.16\.124\.113638\.7\.6\.1\.51/2\.16\.124\.113638\.7\.6\.0\.51/g" ./services/common/common_ohs.groovy
    sed -i -e "s/HMC_AGFA_STORE_SCP='NWG2AOFH'/HMC_AGFA_STORE_SCP='VAP-PACSTSTOHSM'/g" ./services/common/common_ohs.groovy
    sed -i -e "s/HMC_AGFA_STORE_SCU='AS2AOFH'/HMC_AGFA_STORE_SCU='VAP-PACSTSTOHSM'/g" ./services/common/common_ohs.groovy

    ## Comment and Uncomment device declarations for prod
    sed -i -e "s/<!-- Test Section Begin ###/<!-- Test Section Begin -->/g" \
           -e "s/#### Test Section End -->/<!-- Test Section End -->/g" \
           -e "s/<!-- Production Section Begin -->/<!-- Production Section Begin ###/g" \
           -e "s/<!-- Production Section End -->/#### Production Section End -->/g" \
        ./devices/common.xml
else
    echo "Incorrect deployment mode. Set deployment mode to M, P or T"
    exit
fi


PIDDomain_prop_header="<?xml version=\"1.0\"?>\n<config>\n\t<prop name=\"PatientIdentityDomains\">\n\t\t<domain name=\"Ochsner Health System\">\n\t\t\t<id>"${Domain_UID["OHSEPIC"]}"</id>\n\t\t\t<type>Organization</type>\n\t\t\t<code>OHS</code>\n\t\t\t<ae>VCIAP4</ae>\n\t\t\t<facility>OHS_VC</facility>\n\t\t</domain>\n"
PIDDomain_prop_body="<!--This configuration file is automatically generated by script.-->\n"

## Add new domain here ONLY for DIRTY pool

for iaNum in {1..16}; do
    chmod +w image-archive-${iaNum}.xml
    chmod +w admin-tools-${iaNum}.xml

    ## Pool ID starts with 4
    let poolid=3+${iaNum}
    let dicomport=4103+${iaNum}
    let hl7iaport=5103+${iaNum}
    let hl7rrport=5203+${iaNum}
    let hl7oru2srport=5303+${iaNum}
    let xdsrepoport=9103+${iaNum}
    let httpauthenticatedport=2400
    let httpnavigatorport=2603+${iaNum}
    let httpadmintoolsport=2703+${iaNum}
    let httpnavapiport=2503+${iaNum}

    if [ $dep_mode == "M" -o $dep_mode == "Migration" -o $dep_mode == "P" -o $dep_mode == "Production" ]; then
        xdsrepuid="2.16.124.113638.7.6.1."${xdsrepoport}
        imgarcuid="2.16.124.113638.7.6.1."${dicomport}
    else
        xdsrepuid="2.16.124.113638.7.6.0."${xdsrepoport}
        imgarcuid="2.16.124.113638.7.6.0."${dicomport}
    fi   

    case "$iaNum" in
    1 ) poolname="OHS Radiology-Clean"
        ipid="OHSEPIC";;
    2 ) poolname="Agfa Radiology-Dirty"
        ipid="DDOHSR";;
    3 ) poolname="STPH Radiology-Dirty"
        ipid="DDSTPH";;
    4 ) poolname="OHS-NSSlidell-Dirty"
        ipid="DDNSSLIDELL";;
    5 ) poolname="OHS Cardiology-Clean"
        #let dicomport=100+$dicomport
        ipid="OHSEPIC";;
    6 ) poolname="Agfa Card Echo-Dirty"
        ipid="DDOHSCN";;
    7 ) poolname="Agfa Card Cath-Dirty"
        ipid="DDOHSCI";;
    8 ) poolname="MHM Radiology-Dirty"
        ipid="MHM";;
    9 ) poolname="OHS Ophthalmology-Clean"
        ipid="OHSEPIC";;
    10 ) poolname="OHS Ophthalmology-Dirty"
        ipid="DDOHSOP";;
    11 ) poolname="OHS Peds Echo-Clean"
        ipid="OHSEPIC";;
    12 ) poolname="OHS Peds Echo-Dirty"
        ipid="DDOHSCPN";;
    13 ) poolname="OHS OBGYN-Clean"
        ipid="OHSEPIC";;
    14 ) poolname="OHS OBGYN-Dirty"
        ipid="DDOHSOB";;
    15 ) poolname="OHS POCUS-Clean"
        ipid="OHSEPIC";;
    16 ) poolname="OHS POCUS-Dirty"
        ipid="DDOHSPOCUS";;
    * ) poolname="Unknown"
        ipid="UNKNOWN";;
    ## Add new poolname here when configuring new pools.
    esac
    poolname=${dep_mode}"-"${poolname}

    if [ ! ${Domain_UID[${ipid}]+_} ]; then
        echo "..... Cannot recognize Poolname "${poolname}". Please correct configuration script. Exiting..."
        exit
    fi

    ## Construct fully qualified ipid based on predefined mapping
    fq_ipid=${ipid}"\&amp;"${Domain_UID[${ipid}]}"\&amp;ISO"
    echo "->INSTANCE:"$poolname"|"${ipid}"|"${Domain_UID[${ipid}]}

    if [ $ipid == "OHSEPIC"  ]; then
        #echo "..Applying configurations for clean pool."
        ## Customizations for clean pool
        ## 1. Enable MWL Reconciliation
        ## 2. Enable SCN
        ## 3. Use customized inbound morpher c_in.groovy
        sed -e "s/iaX/p${poolid}/g" \
            -e "s/AET_IAX/VCIAP${poolid}/g" \
            -e "s/dicomportX/${dicomport}/g" \
            -e "s/hl7iaportX/${hl7iaport}/g" \
            -e "s/hl7rrportX/${hl7rrport}/g" \
            -e "s/hl7oru2srportX/${hl7oru2srport}/g" \
            -e "s/xdsrepoportX/${xdsrepoport}/g" \
            -e "s/IAHL7RECAPPX/VCIA${poolid}/g" \
            -e "s/XDSRepoIDX/${xdsrepuid}/g" \
            -e "s/IAUIDX/${imgarcuid}/g" \
            -e "s/IAFQIPIDX/${fq_ipid}/g" \
            -e "s/IAHL7RECFACX/DC/g" \
            -e "s/<prop name=\"MWLReconciliationEnabled\">false<\/prop>/<prop name=\"MWLReconciliationEnabled\">true<\/prop>/g" \
            -e "s/<!-- SCN starting flag/<!-- SCN starting flag -->/g" \
            -e "s/SCN ending flag -->/<!-- SCN ending flag -->/g" \
            -e "s/\/etc\/services\/image-archive\/scripts\/in\.groovy/\/etc\/services\/image-archive\/scripts\/c_in\.groovy/g" \
            template-image-archive.xml > image-archive-${iaNum}.xml    
    else
        #echo "..Applying configuration for dirty pool."
        ## Customizations for dirty pool
        ## 1. Use customized inbound morpher d_in.groovy
        sed -e "s/iaX/p${poolid}/g" \
            -e "s/AET_IAX/VCIAP${poolid}/g" \
            -e "s/dicomportX/${dicomport}/g" \
            -e "s/hl7iaportX/${hl7iaport}/g" \
            -e "s/hl7rrportX/${hl7rrport}/g" \
            -e "s/hl7oru2srportX/${hl7oru2srport}/g" \
            -e "s/xdsrepoportX/${xdsrepoport}/g" \
            -e "s/IAHL7RECAPPX/VCIA${poolid}/g" \
            -e "s/XDSRepoIDX/${xdsrepuid}/g" \
            -e "s/IAUIDX/${imgarcuid}/g" \
            -e "s/IAFQIPIDX/${fq_ipid}/g" \
            -e "s/IAHL7RECFACX/DC/g" \
            -e "s/\/etc\/services\/image-archive\/scripts\/in\.groovy/\/etc\/services\/image-archive\/scripts\/d_in\.groovy/g" \
            template-image-archive.xml > image-archive-${iaNum}.xml

        PIDDomain_prop_body=${PIDDomain_prop_body}"\t\t<domain name=\""${ipid}"\">\n\t\t\t<id>"${Domain_UID[${ipid}]}"</id>\n\t\t\t<type>Organization</type>\n\t\t\t<code>OHS</code>\n\t\t\t<ae>VCIAP"${poolid}"</ae>\n\t\t\t<facility>OHS_VC</facility>\n\t\t</domain>\n"
    fi

    ## Customizations for Migration App - these customizations override previous ones for clean pool
    ## 1. Turn on ORU2SR for radiology and cardiology clean pool
    ## 2. Disable MWL reconciliation for all pools
    ## 3. Turn off SCN for all pools
    ## 4. Set production path to readonly and migration path to read-write

    if [ $dep_mode == "M" -o $dep_mode == "Migration" ]; then
        # if this is a migration deployment, then we still turn off SCN or MWL reconciliation even for clean pools, keep ORU2SR default to off
        sed -i -e "s/<prop name=\"MWLReconciliationEnabled\">true<\/prop>/<prop name=\"MWLReconciliationEnabled\">false<\/prop>/g" \
               -e "s/<!-- SCN starting flag -->/<!-- SCN starting flag/g" \
               -e "s/<!-- SCN ending flag -->/SCN ending flag -->/g" \
               -e "s/<!-- Production Storage Locations starting flag -->/<!-- Production Storage Locations starting flag ###/g" \
               -e "s/<!-- Production Storage Locations ending flag -->/#### Production Storage Locations ending flag -->/g" \
               -e "s/<!-- Migration Storage Locations starting flag ###/<!-- Migration Storage Locations starting flag -->/g" \
               -e "s/#### Migration Storage Locations ending flag -->/<!-- Migration Storage Locations ending flag -->/g" \
            image-archive-${iaNum}.xml
    else
        # if this is production or test, and radiology or cardiology clean pool, then we enable ORU2SR
        if [ ${iaNum} == 1 -o ${iaNum} == 5 ]; then
            sed -i -e "s/<!-- ORU2SR starting flag/<!-- ORU2SR starting flag -->/g" \
                   -e "s/ORU2SR ending flag -->/<!-- ORU2SR ending flag -->/g" \
                image-archive-${iaNum}.xml
        fi
    fi

    ## Admin tools configuration

#    PIDDomain_prop="<prop name=\"PatientIdentityDomains\"><domain name=\""${ipid}"\"><id>"${Domain_UID[${ipid}]}"<\/id><type>Organization<\/type><code>OHS<\/code><ae>VCIAP"${poolid}"<\/ae><facility>OHS_VC<\/facility><\/domain><\/prop>"
    
    sed -e "s/iaX/p${poolid}/g" \
        -e "s/AET_IAX/VCIAP${poolid}/g" \
        -e "s/httpnavigatorportX/${httpnavigatorport}/g" \
        -e "s/httpadmintoolsportX/${httpadmintoolsport}/g" \
        -e "s/httpnavapiportX/${httpnavapiport}/g" \
        -e "s/IAIPIDX/${Domain_UID[${ipid}]}/g" \
        -e "s/navigatorservernameX/${poolname}/g" \
        template-admin-tools.xml > admin-tools-${iaNum}.xml

    echo "open "${dicomport} >> firewall_update.txt
    echo "open "${hl7iaport} >> firewall_update.txt
    echo "open "${hl7rrport} >> firewall_update.txt
    echo "open "${httpnavapiport} >> firewall_update.txt
    echo "open "${xdsrepoport} >> firewall_update.txt
    #echo "forward "${dicomport}+100" "${dicomport} >> firewall_update.txt

    ## Nginx configuration
    sed -e "s/2525 ssl/${httpnavapiport} ssl/g" \
        -e "s/localhost:2524/localhost:${httpauthenticatedport}/g" \
        -e "s/localhost:2526/localhost:${httpnavigatorport}/g" \
        -e "s/localhost:2527/localhost:${httpadmintoolsport}/g" \
        template-default.conf >> default.conf

    chmod -w image-archive-${iaNum}.xml
    chmod -w admin-tools-${iaNum}.xml

done

PIDDomain_prop_footer="\t</prop>\n</config>"
echo -e ${PIDDomain_prop_header}${PIDDomain_prop_body}${PIDDomain_prop_footer} > services/admin-tools/patientidentitydomains.xml


echo "Files generated. Please update rialto.cfg.xml, firewall exception and change http authenticated port to "${httpauthenticatedport}" in authentication.xml"

