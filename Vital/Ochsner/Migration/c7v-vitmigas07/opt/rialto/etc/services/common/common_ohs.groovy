// to reference this class, put LOAD('/opt/rialto/etc/services/common/common_ohs.groovy')

class OHSCommon {

    private static Def_iPid = 'OHSEPIC'
    private static Def_iPid_UnivId = '2.16.124.113638.7.6.1.5104'
    private static Def_UnivEntType = 'ISO'
    private static Ukn_iPid = 'UNKNOWN'
    private static Ukn_iPid_UnivId = '2.16.124.113638.7.6.1.5100'

    private static HMC_AGFA_STORE_SCP='NWG2AOFH'
    private static HMC_AGFA_STORE_SCU='AS2AOFH'

    private static final aet_to_ipid_map = [
        'VCIAP4':(Def_iPid),
        'VCIAP5':'DDOHSR',
        'VCIAP6':'DDSTPH',
        'VCIAP7':'DDNSSLIDELL',
        'VCIAP8':(Def_iPid),
        'VCIAP9':'DDOHSCN',
        'VCIAP10':'DDOHSCI',
        'VCIAP11':'MHM',
        'VCIAP12':(Def_iPid),
        'VCIAP13':'DDOHSOP',
        'VCIAP14':(Def_iPid),
        'VCIAP15':'DDOHSCPN',
        'VCIAP16':(Def_iPid),
        'VCIAP17':'DDOHSOB',
        'VCIAP18':(Def_iPid),
        'VCIAP19':'DDOHSPOCUS'
    ]

    private static final ipid_to_univId_map = [
        (Def_iPid):(Def_iPid_UnivId),
        'DDOHSR':'2.16.124.113638.7.6.1.5105',
        'DDSTPH':'2.16.124.113638.7.6.1.5106',
        'DDNSSLIDELL':'2.16.124.113638.7.6.1.5107',
        'DDOHSCN':'2.16.124.113638.7.6.1.5109',
        'DDOHSCI':'2.16.124.113638.7.6.1.5110',
        'MHM':'2.16.124.113638.7.6.1.5111',
        'DDOHSOP':'2.16.124.113638.7.6.1.5113',
        'DDOHSCPN':'2.16.124.113638.7.6.1.5115',
        'DDOHSOB':'2.16.124.113638.7.6.1.5117',
        'DDOHSPOCUS':'2.16.124.113638.7.6.1.5119',
        (Ukn_iPid):(Ukn_iPid_UnivId)
    ]

    private static final valid_aet_list = [
        'VCIAP4',
        'VCIAP5',
        'VCIAP6',
        'VCIAP7',
        'VCIAP8',
        'VCIAP9',
        'VCIAP10',
        'VCIAP11',
        'VCIAP12',
        'VCIAP13',
        'VCIAP14',
        'VCIAP15',
        'VCIAP16',
        'VCIAP17',
        'VCIAP18',
        'VCIAP19'
    ]

    private static final clean_aet_list = [
        'VCIAP4',
        'VCIAP8',
        'VCIAP12',
        'VCIAP14',
        'VCIAP16',
        'VCIAP18'
    ]

    private static final aet_to_port_map = [
        'VCIAP4':4104,
        'VCIAP8':4108,
        'VCIAP12':4112,
        'VCIAP14':4114,
        'VCIAP16':4116,
        'VCIAP18':4118
    ]

    static GetiPIDbyAET(AET){
        def iPid = aet_to_ipid_map[AET]
        if (iPid == null) {
            iPid = Ukn_iPid
        }
        return iPid
    }

    static IsValidiPid(iPid){
        if (iPid == Ukn_iPid){
            return false
        }else{
            def UnivID = ipid_to_univId_map[iPid]
            if (UnivID == null || UnivID == ''){
                return false
            }else{
                return true
            }
        }
    }

    static GetunivIdbyiPID(iPid){
        def UnivID = ipid_to_univId_map[iPid]
        if (UnivID == null) {
            UnivID = ipid_to_univId_map[Ukn_iPid]
        } 
        return UnivID
    }

    static GetDefiPid(){
        return Def_iPid
    }

    static GetDefiPidUnivId(){
        return Def_iPid_UnivId
    }

    static IsValidAE(AET){
        return valid_aet_list.contains(AET)
    }

    static IsCleanAE(AET){
        return clean_aet_list.contains(AET)
    }

    static IsDirtyAE(AET){
        return IsValidAE(AET) && !IsCleanAE(AET)
    }

    static GetPortByAET(AET){
        def dcmPort = aet_to_port_map[AET]
        if (dcmPort == null){
            dcmPort = 0
        }
        return dcmPort
    }

    static GetHMCAgfaStoreSCU(){
        return HMC_AGFA_STORE_SCU
    }

    static GetHMCAgfaStoreSCP(){
        return HMC_AGFA_STORE_SCP
    }

}

