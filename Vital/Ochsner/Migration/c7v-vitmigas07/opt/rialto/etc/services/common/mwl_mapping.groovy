class OHSMWL {
/*
    Modality Worklist Implementation logics:
    1. When ORM drops in, PV1-3-1 is converted to location group and stored in MWL
    2. When MWL-RQ comes in, inbound morpher converts AET to AET group and then to location group
    3. MWL query based on location group matching
*/

    //n to 1 mapping from AET to AET group
    // No duplicates in the left column.
    private static final aet_to_aet_grp_map = [
        'HMSHDXFU01':'HMSH_XRAY_AEG',
        'HMSHDXFU02':'HMSH_XRAY_AEG',
        'HMSHRFHO02':'HMSH_XRAY_AEG',
        'HMSHRFPH03':'HMSH_XRAY_AEG',
        'HMSHOTPG01':'HMSH_XRAY_AEG',
        'HMSHOTPG02':'HMSH_XRAY_AEG',
        'HMSHRFSI01':'HMSH_XRAY_AEG',
        'HMSHCTTO01':'HMSH_CT_AEG',
        'HMSHMGHO01':'HMSH_MG_AEG',
        'HMSHMGHO02':'HMSH_MG_AEG',
        'HMSHUSPH01':'HMSH_US_AEG',
        'HMSHUSPH02':'HMSH_US_AEG',
        'HMSHUSPH03':'HMSH_US_AEG',
        'MATT'      :'HMSH_US_AEG',
        'ECHO1'     :'HMSH_US_AEG',
        'HMSHCRGE01':'HMSH_DEXA_AEG',
        'HMSHNMGE01':'HMSH_NM_AEG',
        'HMSHMRPH01':'HMSH_MR_AEG',
        'HMSHUSPH04':'HMSH_ECHO_AEG',
        'PBVHDXAG01':'PBVH_XRAY_AEG',
        'HMSCDXFU01':'HMSC_XRAY_AEG',
        'DHSCDXFU01':'DHSC_DX_AEG',
        'DHCHDXTO01':'DHCH_DX_AEG',
        'DHCHCTGE01':'DHCH_CT_AEG',
        'DHCHMRTO01':'DHCH_MR_AEG',
        'HMSHFLGE01':'HMSH_XRAY_AEG',
        'HMGENIEACQ':'HMSH_NM_AEG',
        'HMSHRFPH03':'HMSH_XRAY_AEG',
        'HMSHCADHO01':'HMSH_MG_AEG'
    ]

    // n to 1 mapping from location to location group
    // No duplicates in the left column.
    private static final loc_to_loc_grp_map = [
        "HMSH XRAY":'HMSH_DX_LG',
        "HMSH CT":'HMSH_CT_LG',
        "HMSH MAMMO":'HMSH_MG_LG',
        "HMSH US":'HMSH_US_LG',
        "HMSH DEXA":'HMSH_BMD_LG',
        "HMSH NUCMED":'HMSH_NM_LG',
        "DHSH XRAY":'DHSH_DX_LG',
        "HMSH MRI":'HMSH_MR_LG',
        "PBVH XRAY":'PBVH_DX_LG',
        "HMSH ED":'HMSH_CT_LG',
	"HMSH XRAYPOD":'HMSH_DXPOD_LG',
        "HMSH NIC":'HMSH_NM_LG'
    ]
    // 1 to 1 mapping between AET group and location group
    // No duplicate in either column. 
    private static final aet_grp_to_loc_grp_map = [
        'HMSC_XRAY_AEG':'HMSH_DXPOD_LG',
        'HMSH_XRAY_AEG':'HMSH_DX_LG',
        'HMSH_CT_AEG':'HMSH_CT_LG',
        'HMSH_MG_AEG':'HMSH_MG_LG',
        'HMSH_US_AEG':'HMSH_US_LG',
        'DHSC_DX_AEG':'DHSH_DX_LG',
        'HMSH_MR_AEG':'HMSH_MR_LG',
        'PBVH_XRAY_AEG':'PBVH_DX_LG',
        'HMSH_NM_AEG':'HMSH_NM_LG',
        'HMSH_ECHO_AEG':'HMSH_ECHO_LG',
        'HMSH_DEXA_AEG':'HMSH_BMD_LG'

    ]

    private static final mwl_whitelist_aet = [
        'RIALTO',
        'ORU2SR',
        'ALL'
    ]

    private static final mwl_pacsscan_aet = [
        'HMSHOTPG01',
        'HMSHOTPG02'
    ]

    private static final mwl_rm_mod_tag_aet = [
        'HMSHRFPH03'
    ]

    private static final invasive_loc = [
        'CATH500':'CARDIAC CATHETERIZATION',
        'CATH545':'TAVR',
        'EP521':'ELECTROPHYSIOLOGY PROCEDURE',
        'IR5000':'INTERVENTIONAL RADIOLOGY PROCEDURE'
    ]

    static GetLocGrpByLoc(Location){
        return loc_to_loc_grp_map[Location]
    }

    static GetLocGrpByLocModality(Location, Modality){
        switch (Location) {
           case 'HMSH NIC':
              return Modality.equals("NM") ? "HMSH_NM_LG":"HMSH_ECHO_LG"
           //DO NOT IMPLEMENT FOR HMSH NUCMED YET; ONLY HMSH NIC - PER MATT H 2018-10-02
           //case 'HMSH NUCMED':
           //   return Modality.equals("NM") ? "HMSH_NM_LG":"HMSH_ECHO_LG"
           default:
              return GetLocGrpByLoc(Location)
        }
        //return GetLocGrpByLoc(Location)
    }
    
    static GetAETGrpByAET(AET){
        return aet_to_aet_grp_map[AET]
    }

    static GetLocGrpByAETGrp(AETGroup){
        return aet_grp_to_loc_grp_map[AETGroup]
    }

    static IsNonInvasive(LocCode){
        // If the cardiology exam location is not on invasive location list, then it is non-invasive
        return (invasive_loc[LocCode] == null)
    }

    static IsPACSSCAN(AET){
        return mwl_pacsscan_aet.contains(AET)
    }

    static IsWhiteListAET(AET){
        return mwl_whitelist_aet.contains(AET)
    }

}
