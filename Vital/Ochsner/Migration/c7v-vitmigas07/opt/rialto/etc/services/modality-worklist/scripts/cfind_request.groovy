// pass the value for placer and filler numbers
LOAD('/opt/rialto/etc/services/common/mwl_mapping.groovy')
LOAD('/opt/rialto/etc/services/common/common_ohs.groovy')

def scriptName = "MWL cfind request morpher - "
log.info(scriptName + "start")

// if specific character set isn't supplied in the query, default it to Latin1. Without this,
// the system default IR192 (unicode) will be used, which isn't supported by some antique modalities.
def specifiedCharSet = get("SpecificCharacterSet")
if (specifiedCharSet == null || specifiedCharSet == ""){
    set("SpecificCharacterSet", 'ISO_IR100')
}


def callingAET =  getCallingAETitle()
if (OHSCommon.IsCleanAE(callingAET) || OHSMWL.IsWhiteListAET(callingAET)) {
    log.info(scriptName + "calling AET {} is recognized as internal. Will NOT interfere with original search keys", callingAET)
}else{
    def callingAETGroup = OHSMWL.GetAETGrpByAET(callingAET)
    def locationGroup = OHSMWL.GetLocGrpByAETGrp(callingAETGroup) 

    if (callingAETGroup == null || locationGroup == null){
        // if calling AET is not recognized in group mapping, then do not return anything in the result.
        log.info(scriptName + "calling AET {} does not belong to any AET group, or belongs to an AET group without location group specified. MWL will drop the Query", callingAET)
        return false
    }else{
        log.info(scriptName + "calling AET {} belongs to AET group {}, which is mapped to Location Group {}", callingAET, callingAETGroup, locationGroup)
        def AccNum = get("AccessionNumber")
        def PID = get("PatientID")
        def SIUID = get("StudyInstanceUID")
        def SPSStartDate = get("ScheduledProcedureStepSequence/ScheduledProcedureStepStartDate")
        def SPSEndDate = get("ScheduledProcedureStepSequence/ScheduledProcedureStepEndDate")
        // None of these keys are provided, the querying modality is polling for daily task. In this case, we do infer some search keys based on AE title.
        if ((AccNum==null||AccNum=="") && (PID==null||PID=="") && (SIUID==null||SIUID=="") && (SPSStartDate == null && SPSEndDate == null)) {
            def MWL_Day_Window = 0
            if (OHSMWL.IsPACSSCAN(callingAET)) {
                // if calling AET is recognized as PACSSCAN, use +- 2 day as range, without location group as filter
                if (get("PatientName") == "*") { 
                    set("PatientName","") 
                }
                set("Modality","")
                MWL_Day_Window = 2
            } else {
                // if calling AET is recognized as regular modality use +-1 day as range, with location group as filter
                MWL_Day_Window = 1
                set("ScheduledProcedureStepSequence/ScheduledProcedureStepLocation",locationGroup)
            }

            def todayDate = new Date()
            SPSStartDate = (todayDate - MWL_Day_Window).format('yyyyMMdd')
            SPSEndDate = (todayDate + MWL_Day_Window).format('yyyyMMdd')

            set("ScheduledProcedureStepSequence/ScheduledProcedureStepStartDate", SPSStartDate + "-" + SPSEndDate)
            set("ScheduledProcedureStepSequence/ScheduledProcedureStepStartTime", null)
            set("ScheduledProcedureStepSequence/ScheduledProcedureStepEndDate", null)
            set("ScheduledProcedureStepSequence/ScheduledProcedureStepEndTime", null)

            log.info(scriptName + "Implied filter on ScheduledProcedureStep applied: set Start and End Date to {} and {}, and removed Start and End Time", SPSStartDate, SPSEndDate)        
        }else{
            log.debug(scriptName + "Script decided to respect the original search keys.")
        }
    }
}

log.debug(scriptName + "{}", output)
log.debug(scriptName + "ScheduledProcedureStep filter:\n StartDateTime: {} {}\n EndDateTime: {} {}",get("ScheduledProcedureStepSequence/ScheduledProcedureStepStartDate"),get("ScheduledProcedureStepSequence/ScheduledProcedureStepStartTime"),get("ScheduledProcedureStepSequence/ScheduledProcedureStepEndDate"),get("ScheduledProcedureStepSequence/ScheduledProcedureStepEndTime"))

