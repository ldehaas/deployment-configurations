/*
 * HL7 to cfind morpher
 * Convert an order message into a cfind message.
 * get() works on the order message, while set() works on the 
 * cfind message.
 * 
 * The order is ignored if this script returns false,
 * e.g. no images will be fetched for the order.
 */
def scriptName = "Connect-forward HL7 to CFind Moprher - "

log.info(scriptName + "Start")

def StudyInstUID = get('/.ZDS-1')

if (StudyInstUID != null) {
    set(StudyInstanceUID,StudyInstUID)
    log.info(scriptName + "Setting StudyInstanceUID to {} in C-Find-Rq", StudyInstUID)
}else{
    def AccNum = get('/.ORC-3')
    if (AccNum != null){
        set(AccessionNumber,AccNum)
        log.warn(scriptName + "cannot find study instance UID in ZDS-1. Setting AccessionNumber to {} in C-Find-Rq", AccNum)
    }else{
        log.error(scriptName + "cannot find study instance UID or AccessionNumber. The ORM must be invalid!!")
        return false
    }
}

log.info(scriptName + "End")
