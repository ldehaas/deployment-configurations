def scriptName="Adhoc CFind Request Morpher - "

LOAD('/opt/rialto/etc/services/common/common_ohs.groovy')

log.debug(scriptName + "Start")


def in_PatientID = get('PatientID')

if (in_PatientID != null) {
    def out_issuerOfPatientID = OHSCommon.GetDefiPid()
    def out_universalEntityID = OHSCommon.GetDefiPidUnivId()
    def out_universalEntityType = ('ISO')

    set('IssuerOfPatientID', out_issuerOfPatientID)
    set('IssuerOfPatientIDQualifiersSequence/UniversalEntityID', out_universalEntityID)
    set('IssuerOfPatientIDQualifiersSequence/UniversalEntityIDType', out_universalEntityType)

    log.debug(scriptName + "cfind request is now stamped with fully Qualified Issuer of PatientID {}", out_universalEntityID)
}else{
    log.info(scriptName + " C-Find request does not contain PatientID")
}

log.debug(scriptName + "End")

