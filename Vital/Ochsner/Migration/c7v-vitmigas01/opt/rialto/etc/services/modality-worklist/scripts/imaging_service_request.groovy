import com.karos.rtk.common.HL7v2Date;
import org.joda.time.DateTimeZone;


LOAD('/opt/rialto/etc/services/common/mwl_mapping.groovy')

def scriptName = "MWL Imaging Service Request Morpher - "

log.info(scriptName + "Received order message:\n{}", input)
log.info("And here's how the initial Imaging Service Request looks like\n{}", imagingServiceRequest)



// ************************************
// *     Imaging Service Request      *
// ************************************

def patientIdentification = imagingServiceRequest.getPatientIdentification()
patientIdentification.setPatientID(get("PID-3-1"))
if (patientIdentification.getPatientIdUniversalId() == null) {
    patientIdentification.setPatientIdNamespaceId(get("PID-3-4-1"))
    patientIdentification.setPatientIdUniversalId(get("PID-3-4-2"))
    patientIdentification.setPatientIdUniversalIdType("ISO")
}
//patientIdentification.setPatientName(get("PID-5"))
imagingServiceRequest.setPatientIdentification(patientIdentification)


def patientDemographics = imagingServiceRequest.getPatientDemographics()
patientDemographics.setPatientBirthDate(get("PID-7"))
patientDemographics.setPatientSex(get("PID-8"))
imagingServiceRequest.setPatientDemographics(patientDemographics)

if (get("ORC-3") != null) {
    def Identifier = get("ORC-3-1")
    def NamespaceID = get("ORC-3-2")
   
    log.info(scriptName + "creating imagingServiceRequest with accession number {} in namespace {}", Identifier, NamespaceID)
    imagingServiceRequest.setAccessionNumber(Identifier)
} else {
    log.error(scriptName + "ORC-3 is NULL. Unable to create imagingServiceRequest with accession number!")
}


def visitIdentification = imagingServiceRequest.getVisitIdentification()
visitIdentification.setAdmissionID(get("PV1-19"))


def visitStatus = imagingServiceRequest.getVisitStatus()
visitStatus.setPatientClass(get("PV1-2"))

def LocGrp = OHSMWL.GetLocGrpByLocModality(get("OBR-18"),get("OBR-24"))
log.debug(scriptName + " location group set to {}", LocGrp)
//visitStatus.setCurrentPatientLocation(get("OBR-18"))
def scheduledProcedureStep1 = imagingServiceRequest.getRequestedProcedureSequence().get(0).getScheduledProcedureStepSequence().get(0)
scheduledProcedureStep1.setScheduledProcedureStepLocation(LocGrp);
/*
if (get("OBR-18") != null){
    visitStatus.setCurrentPatientLocation(get("OBR-18"))
}
*/


// PlacerNumber is needed to fill in Placer Order Number to DICOM study being reconciled at the step of verification
def PlacerNumber = get("OBR-2")
if (PlacerNumber != null) {
    imagingServiceRequest.setPlacerIssuerAndNumber(PlacerNumber)
    log.info(scriptName + "Placer Number = {}", PlacerNumber)
} else {
    log.warn(scriptName + "OBR-2 is NULL. Unsble to map it to PlacerOrderNumber in ImagingServiceRequest")
}

imagingServiceRequest.setFillerIssuerAndNumber(get("OBR-3"))



if (imagingServiceRequest.getAccessionNumberUniversalId() == null) {
    imagingServiceRequest.setAccessionNumberNamespaceId("UNKNOWN")
    imagingServiceRequest.setAccessionNumberUniversalId("2.16.124.113638.1.2.1.1")
    imagingServiceRequest.setAccessionNumberUniversalIdType("ISO")
}

imagingServiceRequest.setAttendingPhysician(get("PV1-7-1") + "^" + get("PV1-7-2") + "^" + get("PV1-7-3"))
//Referring Physician from Epic is OBR-16; Encounter Physician is PV1-8; changed 20180809
//imagingServiceRequest.setReferringPhysician(get("PV1-8-1") + "^" + get("PV1-8-2") + "^" + get("PV1-8-3"))
//imagingServiceRequest.setRequestingPhysician(get("OBR-16-1") + "^" + get("OBR-16-2") + "^" + get("OBR-16-3"))
imagingServiceRequest.setReferringPhysician(get("OBR-16-1") + "^" + get("OBR-16-2") + "^" + get("OBR-16-3"))
imagingServiceRequest.setRequestingPhysician(get("PV1-8-1") + "^" + get("PV1-8-2") + "^" + get("PV1-8-3"))

imagingServiceRequest.setOrderCallbackPhoneNumber(get("OBR-17"))
imagingServiceRequest.setReasonForImagingServiceRequest(get("OBR-31"))



// *******************************
// *     Requested Procedure     *
// *******************************

def requestedProcedure = imagingServiceRequest.getRequestedProcedureSequence().get(0)
if (get("ZDS-1") == null || get("ZDS-1") == ""){
    log.warn(scriptName + "Study Instance UID is not provided in ZDS segment.")
}else{
    requestedProcedure.setStudyInstanceUID(get("ZDS-1"))
}


requestedProcedure.setRequestedProcedureID(imagingServiceRequest.getAccessionNumber())
requestedProcedure.setRequestedProcedurePriority(get("OBR-5"))
requestedProcedure.setReasonForTheRequestedProcedure(get("OBR-31"))

//def ReqProcCode = get("OBR-4-1")
def ReqProcDesc = get("OBR-4-2")

if (ReqProcDesc != null) {
    requestedProcedure.setRequestedProcedureDescription(ReqProcDesc)
    log.info(scriptName + "Requested Procedure Description: {}", ReqProcDesc)
} else {
    log.warn(scriptName + "OBR-4-2 is NULL. Unable to map it to RequesteProcedureDescription in ImagingServiceRequest")
}

// ***********************************
// *     Scheduled Procedure Step    *
// ***********************************

def scheduledProcedureStep = requestedProcedure.getScheduledProcedureStepSequence().get(0)
scheduledProcedureStep.setScheduledProcedureStepIDString(imagingServiceRequest.getAccessionNumber())
scheduledProcedureStep.setScheduledProcedureStepStatus(get("ORC-1"))

// Order Control Code from ORC-1 not being used. Order Status from ORC-5 used to populate ScheduledProcedureStep status
// For Cardiology (MSH-3=CUPID) non-invasive(depending on study description) studies, the logic will be a little differnt
// "Tech completed" instead of "Exam Ended" will mark the study completed.
def orderStatus = get("ORC-5")
if (orderStatus == null || orderStatus == ''){
    log.warn(scriptName + "ORC-5 is blank. This ORM is not used to control ScheduledProcedureStepStatus")
}else{
    if (get("MSH-3") == "CUPID" && OHSMWL.IsNonInvasive(get("OBR-4-1"))) {
        log.info(scriptName + "A cardiology non-invasive exam order with status {} in ORC-5. This message was groomed by HL7Validator Script", orderStatus)
    }else{
        log.info(scriptName + "A regular exam order with status {} in ORC-5", orderStatus)
    }
    switch(orderStatus){
//        case 'Scheduled':
//            log.info(scriptName + "setting Scheduled Procedure Step Status to SCHEDULED")
//            scheduledProcedureStep.setScheduledProcedureStepStatus('SCHEDULED')
//            break

// if 'Tech Comp' is received, then it must be a cardio non-invasive. 
// if 'Exam Ended' is received, then it should be other study type'
        case 'Arrived':
            log.info(scriptName + "setting Scheduled Procedure Step Status to SCHEDULED")
            scheduledProcedureStep.setScheduledProcedureStepStatus('SCHEDULED')
            break
        case 'Exam Begun':
            log.info(scriptName + "setting Scheduled Procedure Step Status to INPROGRESS")
            scheduledProcedureStep.setScheduledProcedureStepStatus('INPROGRESS')
            break
        case 'Exam Ended':
            log.info(scriptName + "setting Scheduled Procedure Step Status to COMPLETED")
            scheduledProcedureStep.setScheduledProcedureStepStatus('COMPLETED')
            break
        case 'Tech Comp':
            log.info(scriptName + "setting Scheduled Procedure Step Status to COMPLETED")
            scheduledProcedureStep.setScheduledProcedureStepStatus('COMPLETED')
            break
        case 'Cancelled':
            log.info(scriptName + "setting Scheduled Procedure Step Status to DISCONTINUED")
            scheduledProcedureStep.setScheduledProcedureStepStatus('DISCONTINUED')
            break
    }
}

scheduledProcedureStep.setModality(get("OBR-24"))
if (scheduledProcedureStep.getModality() == null) {
    scheduledProcedureStep.setModality("UNKNOWN");
}

if (scheduledProcedureStep.getScheduledProcedureStepLocation() == null) {
    def DeptCode = get("OBR-18")
    //def DeptCode = get("OBR-18") //change at Hancock go-live 2018/08/05
    log.debug(scriptName + " DeptCode {}", DeptCode)
    if (DeptCode != null) {
        def LocationGroup = OHSMWL.GetLocGrpByLocModality(DeptCode, get("OBR-24"))
        
        if (LocationGroup == null || LocationGroup == ''){
            log.info(scriptName + "Location {} does not belong to any Location Group. Leaving Scheduled Procedure Step Location field blank.")
        }else{
            log.info(scriptName + "Location {} belongs to Location Group {}. Setting Scheduled Procedure Step Location to the Location Group", DeptCode, LocationGroup)
            scheduledProcedureStep.setScheduledProcedureStepLocation(LocationGroup)
        }
    }else{
        log.warn(scriptName + "about to set Scheduled Procedure Step Location to UNKNOWN because PV1-3-1 is null");
        scheduledProcedureStep.setScheduledProcedureStepLocation("UNKNOWN")
    }
}

if (get("OBR-36") != null){
    scheduledProcedureStep.setScheduledProcedureStepStartDateTime(HL7v2Date.parse(get("OBR-36").toString(), DateTimeZone.getDefault()))
} else {
    log.warn(scriptName + "OBR-36 is null. Unknown scheduled date time.")
}

log.info("Finished fixing up the Imaging Service Request...Here it is\n{}", imagingServiceRequest)
