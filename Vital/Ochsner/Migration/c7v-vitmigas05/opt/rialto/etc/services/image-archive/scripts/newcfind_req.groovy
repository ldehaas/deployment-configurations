LOAD('/opt/rialto/etc/services/common/common_ohs.groovy')

def scriptName = "IA C-Find Req Morpher - ";

log.info(scriptName + "START")

def callingAET = getCallingAETitle().toUpperCase();
def calledAET = getCalledAETitle().toUpperCase();

def in_PatientID = input.get('PatientID')
def in_issuerOfPatientID = input.get('IssuerOfPatientID')
def in_CurrentPatientLocation = input.get('CurrentPatientLocation')

def out_issuerOfPatientID
def out_universalEntityID
def out_universalEntityType = 'ISO'

if (in_PatientID != null) {
    // Manipulate issuer only if PatientID is part of the C-Find query
    out_issuerOfPatientID = in_issuerOfPatientID

    if (in_issuerOfPatientID == null) {
       out_issuerOfPatientID = null
    } else {
       out_universalEntityID = OHSCommon.GetunivIdbyiPID(in_issuerOfPatientID)
    }
    
    if (out_issuerOfPatientID == null || out_universalEntityID == null || out_issuerOfPatientID == 'UNKNOWN'){
        log.info(scriptName + " original issuer is null or unknown, drop original issuer and determine issuer based on called AET: {}", calledAET)
        out_issuerOfPatientID = OHSCommon.GetiPIDbyAET(calledAET)

        if (out_issuerOfPatientID == null){
            log.info(scriptName + "No valid issuer or calledAET, fail the C-Find")
            return false;
        }else{
            out_universalEntityID = OHSCommon.GetunivIdbyiPID(out_issuerOfPatientID)
            log.info(scriptName + "valid called AET found, fill in the corresponding issuer {} and univ id {}.", out_issuerOfPatientID, out_universalEntityID)
            set('IssuerOfPatientID', out_issuerOfPatientID)
            set('IssuerOfPatientIDQualifiersSequence/UniversalEntityID', out_universalEntityID)
            set('IssuerOfPatientIDQualifiersSequence/UniversalEntityIDType', out_universalEntityType)
        }
    } else {
        log.info(scriptName + "Original issuer is valid, repect the original and fill in the corresponding univ id")
        out_universalEntityID = OHSCommon.GetunivIdbyiPID(out_issuerOfPatientID)
        set('IssuerOfPatientIDQualifiersSequence/UniversalEntityID', out_universalEntityID)
        set('IssuerOfPatientIDQualifiersSequence/UniversalEntityIDType', out_universalEntityType)
    }

}

// check to see if the incoming request has a CPL value if so we need to morph this into a rialto search-able field
// for now we will copy it over to ConfidentialityCode
if (in_CurrentPatientLocation != null) {
   set(ConfidentialityCode,get(CurrentPatientLocation)) 
}


log.info(scriptName + "CFind Req: {}", input)

log.info(scriptName + "END")
