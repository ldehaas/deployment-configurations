log.debug("document_metadata_morpher.groovy: Start morpher")

// -----------------------------------------------
// The implementation below is based on our best guess of what Ochsner might need

set(XDSClassCode, code('18726-0 ','2.16.124.113638.7.6.9.1001','Radiology studies (set)'))
set(XDSConfidentialityCode,code('N', '2.16.840.1.113883.5.25', 'Normal'))

if (dcmBodyPartExamined == null){
   log.warn("Body Part Examined field is blank. Anatomic region is unknown.")
   set(XDSEventCode,code(dcmModality,'1.2.840.10008.2.16.4',dcmModality))
}else{
   set(XDSEventCode,code(dcmModality,'1.2.840.10008.2.16.4',dcmModality),
                    code(dcmBodyPartExamined,'2.16.840.1.113883.6.51',dcmBodyPartExamined))  
}

set(XDSFormatCode,code('1.2.840.10008.5.1.4.1.1.88.59', '1.2.840.10008.2.6.1', 'Key Object Selection Document'))


set(XDSLanguage, 'en-US')

set(XDSPatientID, dcmPatientID, issuerOfLocalPID)
if(null == dcmPatientFamilyName) {
    log.warn("XDS Metadata creation, manifest submission:\nPatient Family Name was NULL.  Manifest submission will likely fail!!")
} else {
    set(XDSSourcePatientFamilyName, dcmPatientFamilyName)   
}
if(null == dcmPatientFirstName) {
    log.warn("XDS Metadata creation, manifest submission:\nPatient First Name was NULL.  Manifest submission will likely fail!!")
} else {
    set(XDSSourcePatientGivenName, dcmPatientFirstName)
}
if(null == dcmPatientDOB) {
    log.warn("XDS Metadata creation, manifest submission:\nPatient Date Of Birth was NULL.")
} else {
    set(XDSSourcePatientBirthDate, dcmPatientDOB)
}
if(null == dcmPatientSex) {
    log.warn("XDS Metadata creation, manifest submission:\nPatient Sex was NULL.  Manifest submission will likely fail!!")
} else {
    set(XDSSourcePatientSex, dcmPatientSex)
}

set(XDSSourcePatientID, dcmPatientID, issuerOfLocalPID)

set(XDSPracticeSettingCode,code('NI', '1.2.826.0.1.3680043.2.1537.7.0.1.0.1.3', 'No Information'))

if (get(StudyDate) != null) {
    set(XDSServiceStartTime, get(StudyDate), get(StudyTime))
    set(XDSServiceStopTime, get(StudyDate), get(StudyTime))
} else { 
    set(XDSServiceStartTime, new java.util.Date())
    set(XDSServiceStopTime, new java.util.Date())
}

set(XDSTitle, 'DICOM Study')
set(XDSExtendedMetadata('studyInstanceUID'), dcmStudyInstanceUID)

// -----------------------------------------------


// use extended metadata to keep track of study instance uid
set(XDSExtendedMetadata('studyInstanceUid'), get('StudyInstanceUID'))

//set(XDSExtendedMetadata('accessionNumber'), get('AccessionNumber'))
def accessionNumber = get(AccessionNumber)
// Sanity Check
if(null == accessionNumber) {
    log.warn("XDS Metadata creation, manifest submission:\nAccession Number was NULL.")
} else {
    set(XDSExtendedMetadata('accessionNumber'), accessionNumber)
}

//set(XDSCreationTime, get('StudyDate'))
//set XDSCreationTime to InstanceCreationTime, StudyDate or CurrentDate.
if (get(InstanceCreationDate) != null) {
    set(XDSCreationTime,get(InstanceCreationDate),get(InstanceCreationTime))
} else if (get(StudyDate) != null) {
    set(XDSCreationTime,get(StudyDate),get(StudyTime))
} else {
    set(XDSCreationTime, new java.util.Date())
}

set(XDSLanguage, 'English')

// these codes are environment specific.
set(XDSTypeCode, code('Test Type Code', 'Test Scheme', 'Test Type Code'))

set(XDSClassCode,
    code('DICOM Study Manifest', 'Karos Health demo classCodes', 'DICOM Study Manifest'))

set(XDSHealthcareFacilityTypeCode,
    code('Hospital Unit', 'Karos Health demo healthcareFacilityTypeCodes', 'Hospital Unit'))

set(XDSPracticeSettingCode,
    code('Test Practice Setting Code', 'Test Scheme', 'Test Practice Setting Code'))

set(XDSFormatCode,
    code('DICOM', 'Karos Health demo formatCodes', 'DICOM'))

set(XDSConfidentialityCode,
    code('N', 'Karos Health demo confidentialityCodes', 'Normal'))

set(XDSEventCode, code('Test', 'Test', 'Test'))

def localPID = get(PatientID)
if(null == localPID) {
    log.warn("XDS Metadata creation, manifest submission:\nLocal Patient ID was NULL.  Manifest submission will likely fail!!")
}

def issuerOfLocalPID = null
def (namespace, domain, type) = get(IssuerOfPatientID).tokenize("&")
if (null == domain) {
    issuerOfLocalPID = get("IssuerOfPatientIDQualifiersSequence/UniversalEntityID")
} else {
    issuerOfLocalPID = domain
}

// Sanity Check
if(null == issuerOfLocalPID) {
    log.warn("XDS Metadata creation, manifest submission:\nLocal Domain was NULL.  Manifest submission will likely fail!!")
}

def qualifiedLocalPID = localPID + issuerOfLocalPID
// Sanity Check
if(null == qualifiedLocalPID) {
    log.warn("XDS Metadata creation, manifest submission:\nLocal Patient ID and Issuer were *both*  NULL.  Manifest submission will likely fail!!")
}

def referringPhysicianName = get(tag(0x0008,0x0090))
// Sanity Check
if(null == referringPhysicianName) {
    log.warn("XDS Metadata creation, manifest submission:\nReferring Physician Name is NULL. Not including referring physician name in manifest")
} else {
    set(XDSExtendedMetadata('referringPhysician'), referringPhysicianName)
}

log.debug("document_metadata_morpher.groovy: Debugging XDS Metadata creation, manifest submission: \nLocal Patient ID is \"{}\", and its Issuer is: \"{}\"", localPID, issuerOfLocalPID)
set(XDSSourcePatientID, localPID, issuerOfLocalPID)

//set(XDSTitle, get(StudyDescription))
def studyDescription = get(StudyDescription)
// Sanity Check
if(null == studyDescription) {
    log.debug("XDS Metadata creation, manifest submission:\nStudy Description is NULL. Setting XDSTitle to default DICOM Study")
    set(XDSTitle, 'DICOM Study')
} else {
    set(XDSTitle, studyDescription)
}

set(XDSAuthorGivenName, ' ')

log.debug("document_metadata_morpher.groovy: End morpher")
