import org.joda.time.DateTime
import org.joda.time.format.DateTimeFormat
import java.text.SimpleDateFormat

import org.dcm4che2.data.BasicDicomObject
import org.dcm4che2.data.DicomObject
import org.dcm4che2.data.Tag
import org.dcm4che2.data.UID
import org.dcm4che2.net.Status

import com.karos.dcm.cfind.CFindResult
import com.karos.dcm.cfind.CFindSCU
import com.karos.groovy.morph.dcm.VR
import com.karos.rialto.dcm.CFindCallback
import com.karos.rialto.dcm.CFindStatus
import com.karos.rialto.platform.dcm.DicomDevice
import com.karos.rtk.common.Domain

LOAD('/opt/rialto/etc/services/common/common_ohs.groovy')
// NOTE: script files scn_epic.groovy and scn_ph.groovy are supposed to be identical. 
// Some code logic proceed are based on file name

//def scriptName = "IA SCN morpher - "
def scriptFile = this.class.getName()
def scriptName = "IA SCN Morpher " + scriptFile + ":" 
log.debug(scriptName + "Start")
// This script is referenced by multiple instances of image archive services;
// Values assigned to MSH-3 to MSH-6 will be overwritten on the way out. So don't attempt to set them here.

if (scriptFile == 'scn_epic' && type != null && type.toUpperCase() != 'ARCHIVE') {
    // if the script name is scn_epic, then this is used for epic and we do not send anything other than ARCHIVE to epic
    log.debug(scriptName + "skipping SCN type " + type + " for EPIC")
    return false
}

initialize( 'ORM', 'O01', '2.3' );
output.getMessage().addNonstandardSegment('ZDS')
log.debug(scriptName + "input is {}", input);

def CallingAET = getCallingAETitle();
def CalledAET = getCalledAETitle();

log.debug(scriptName + "Calling AE Title is {} and Called AE Title is {}", CallingAET,  CalledAET);

sdf = new SimpleDateFormat("yyyyMMddHHmmss")
set('MSH-7', sdf.format(new Date()))

set('PID-3-1', input.get(PatientID));
set('PID-3-4-1', input.get(IssuerOfPatientID))
set('PID-3-4-2', input.get("IssuerOfPatientIDQualifiersSequence/UniversalEntityID"))
set('PID-3-4-3', input.get("IssuerOfPatientIDQualifiersSequence/UniversalEntityIDType"))
setPersonName('PID-5', input.get(PatientName));

set('ORC-1', 'XO')

set('ORC-17','VC_IA')
if (CalledAET != null){
    // Called AET should indicate pool when the study is sent in
    set('ORC-18', CalledAET)
}else{
    // If CalledAET is null, then this is most likely SCN from study deletion. Use Calling AET instead, which should indicate pool.
    set('ORC-18', CallingAET)
}
//
set('OBR-1', '1')

def in_AccNum = input.get(AccessionNumber)
def in_StudyInstUID = input.get(StudyInstanceUID)

set('OBR-2', input.get(PlacerOrderNumberImagingServiceRequest))
set('OBR-3', in_AccNum)
//
// Set the Procedure Code depending on the Calling AE Title
    
//set('OBR-4-1',  input.get(RequestedProcedureID))
set('OBR-4', input.get(StudyDescription))
set('OBR-7', input.get(StudyDate))

def ReqPhysician = input.get(RequestingPhysician) 
if (ReqPhysician != null && ReqPhysician != '') {
    if (ReqPhysician.contains("\\^")) {
        def name_parts = ReqPhysician.split("\\^")
        if (name_parts.length == 3) {
            // assuming requesting physician name follows "code^lastname^firstname format"
            set('OBR-16-1', name_parts[0])
            set('OBR-16-2', name_parts[1])
            set('OBR-16-3', name_parts[2])
            }else{
            set('OBR-16', ReqPhysician)
            }
    }else{
        set('OBR-16', ReqPhysician)
    }
}

set('OBR-25', 'I')
set('OBX-1', '1')
set('OBX-2', 'TX')
set('OBX-3-1-2', 'GDT')
set('OBX-5', 'Y')
set('ZDS-1', in_StudyInstUID)

// Perform final SR search only when all of the following conditions are met
// 1) scn is destined to ph (scriptFile = scn_ph) 
// 2) scn type is ARCHIVE (only when ingesting new SOP instance)
// 3) called AET is a clean AET (only when thrid party send to the clean pool)
// 4) only when SOP inst is an image (with valid integer value in Rows and Columns)

boolean  DoSearchFinalSR = false
if (scriptFile == 'scn_ph' && type != null && type.toUpperCase() == 'ARCHIVE' && OHSCommon.IsCleanAE(CalledAET)) {
    DoSearchFinalSR = true
}


if (DoSearchFinalSR) {
    log.debug(scriptName + "Querying for final reports")
    Domain domain = null
    String issuerOfPatientID = input.get(IssuerOfPatientID)
    if (issuerOfPatientID != null) {
        domain = new Domain(issuerOfPatientID, input.get("IssuerOfPatientIDQualifiersSequence/UniversalEntityID"), input.get("IssuerOfPatientIDQualifiersSequence/UniversalEntityIDType"))
        DicomObject query = new BasicDicomObject()

        // query for an SR at the image level, then check if any of the results have the PreliminaryFlag set to "FINAL". If Accession Number is valid, check by Accession Number, or check by Study Instance UID...
        // The last argument of DicomDevice constructor is TLS encryption parameters, which should be added if TLS is enabled.

        if (in_AccNum == null || in_AccNum == "") {
            query.putString(Tag.QueryRetrieveLevel, VR.CS, "IMAGE")
            query.putString(Tag.StudyInstanceUID, VR.UI, in_StudyInstUID)
            query.putString(Tag.SOPClassUID, VR.UI, UID.BasicTextSRStorage)
            query.putNull(Tag.ContentLabel, VR.CS)
            log.debug(scriptName + "Building device: AET: " + CalledAET + ", port number" + OHSCommon.GetPortByAET(CalledAET) + ", domain:" + domain + ", query by study instance uid as accession number is blank or null.")
        }else{
            query.putString(Tag.QueryRetrieveLevel, VR.CS, "IMAGE")
            query.putString(Tag.AccessionNumber, VR.SH, in_AccNum)
            query.putString(Tag.SOPClassUID, VR.UI, UID.BasicTextSRStorage)
            query.putNull(Tag.ContentLabel, VR.CS)
            query.putNull(Tag.StudyInstanceUID, VR.UI)
            log.debug(scriptName + "Building device: AET: " + CalledAET + ", port number" + OHSCommon.GetPortByAET(CalledAET) + ", domain:" + domain + ", query by accession number as accession number appears to be valid.")
        }
       
        DicomDevice device = new DicomDevice(CalledAET, domain as Domain, "127.0.0.1", OHSCommon.GetPortByAET(CalledAET), null)

        log.debug(scriptName + "Building cfind SCU")
        CFindSCU scu = new CFindSCU(device, "SCN_MORPHER_AE")

        log.debug(scriptName + "Building cfind callback")
        CFindCallback callback = new CFindCallback() {
            List<DicomObject> dicomObjects = new ArrayList();
            @Override
            void send(DicomObject result, boolean warning) {
                dicomObjects.add(result)
            }
            @Override
            boolean valid() {
                return true
            }
        }
        boolean isFinal = false
        String out_StudyInstUID = ''
        try {
            log.debug(scriptName + "Sending CFind request for study instance {}, sop class {}, at IMAGE level", input.get(StudyInstanceUID), UID.BasicTextSRStorage);
            CFindResult response = scu.cfind(query, callback)

            log.debug(scriptName + "CFind finished with " + callback.dicomObjects.size() + " result(s), checking if they have FINAL tag")

            if (response.getStatusCode() == Status.Success && !callback.dicomObjects.isEmpty()) {
                for (DicomObject result : callback.dicomObjects) {
                    if ("FINAL" == result.getString(Tag.ContentLabel)) {
                        isFinal = true
                        out_StudyInstUID = result.getString(Tag.StudyInstanceUID)
                        log.debug(scriptName + "Found a TextSR with FINAL status in study instance {}", out_StudyInstUID)
                        break
                    }
                }
            }
        } catch (InterruptedException | IOException e) {
            log.error("Error executing cfind for final report: " + e.getMessage())
        }
        set("ZDS-2", out_StudyInstUID)
        set("ZDS-3", isFinal ? "Y" : "N")
    }else{
        log.debug(scriptName + "Domain incorrect")
    }
}else{
log.debug(scriptName + "decided to skip checking for Final SR")
}

log.debug(scriptName + "End")
