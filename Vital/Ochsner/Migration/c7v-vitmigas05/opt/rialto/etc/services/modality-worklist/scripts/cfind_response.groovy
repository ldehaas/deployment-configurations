LOAD('/opt/rialto/etc/services/common/mwl_mapping.groovy')
LOAD('/opt/rialto/etc/services/common/common_ohs.groovy')

def scriptName = "MWL cfind response morpher - "
log.info(scriptName + "start examining the order with patient ID {} and  accession number {}", get("PatientID"), get("AccessionNumber"))

// Characterset is handled in the request morpher. 
// Response morpher only has the ability to rename the character set tag. It cannot change the actual character storage.

// pass the value for placer and filler numbers
set(PlacerOrderNumberImagingServiceRequest,imagingServiceRequest.getPlacerIssuerAndNumber())
set(FillerOrderNumberImagingServiceRequest,imagingServiceRequest.getFillerIssuerAndNumber())
set(CurrentPatientLocation,imagingServiceRequest.getVisitStatus().getCurrentPatientLocation())
set("ScheduledProcedureStepSequence/ScheduledProcedureStepDescription", get("RequestedProcedureDescription"))

def callingAET =  getCallingAETitle()
if (OHSCommon.IsCleanAE(callingAET) || OHSMWL.IsWhiteListAET(callingAET)) {
    // When calling AET is internal, do not interfere with the query result
    log.debug(scriptName + "calling AET {} is recognized as internal. Will NOT interfere with the original search results", callingAET)
}else{
    // Note: if calling AET is not recognized in group mapping, then the cfind request should have been discarded and this branch should never be executed.
    // Therefore, no need to check group assignment again here.
    def myOrderStatus = get("ScheduledProcedureStepSequence/ScheduledProcedureStepStatus");
    if (OHSMWL.IsPACSSCAN(callingAET)){
        // if calling AET is PACS scan, it should not exclude completed status
        if (myOrderStatus == null || myOrderStatus == 'DISCONTINUED') {
            log.debug(scriptName + "Dropping this result from DMWL C-Find response, calling AET={} (PACSSCAN) and OrderStatus={}",callingAET,myOrderStatus);
            return false;
        }else{
            log.debug(scriptName + "Retaining this result in DMWL C-Find response, calling AET={} (PACSSCAN) and OrderStatus={}",callingAET,myOrderStatus);
        }
    }else{
        // if calling AET is not PACS scan, it must be a physical modality, both completed and cancelled are excluded.
        if (myOrderStatus == null || myOrderStatus == 'DISCONTINUED' || myOrderStatus == 'COMPLETED') {
            log.debug(scriptName + "Dropping this result from DMWL C-Find response, calling AET={} (Modality) and OrderStatus={}",callingAET,myOrderStatus);
            return false;
        }else{
            log.debug(scriptName + "Retaining this result in DMWL C-Find response, calling AET={} (Modality) and OrderStatus={}",callingAET,myOrderStatus);
        }
    }

    // The following tag manipulations are only apply to mwl cfind requested by physical modalities.
    // Remove issuer of patient id qualifier sequence in mwl cfind response. 
    remove(tag(0x0010,0x0024))
    // Remove issuer of accession numbers in mwl cfind response.    
    remove(tag(0x0008,0x0051))

}
