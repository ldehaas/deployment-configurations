// This scripts is currently for both manual and automatic reconciliation.
// if MWLCFindQueryMorpher isn't specified for image archive, then AccessionNumber, PatientID and issuer of PatientID are 
// used to determine whether a matching MWL is determined. In this script, mwlEntry represents the matching MWL order.

def scriptName = "Reconciliation Script - "

log.debug(scriptName + "Starting reconciling SOP {} with the matching MWL entry.", get(SOPInstanceUID));

set('AccessionNumber', mwlEntry.get('AccessionNumber'))
set('IssuerOfAccessionNumberSequence/LocalNamespaceEntityID', mwlEntry.get('IssuerOfAccessionNumberSequence/LocalNamespaceEntityID'))
set('IssuerOfAccessionNumberSequence/UniversalEntityID', mwlEntry.get('IssuerOfAccessionNumberSequence/UniversalEntityID'))
set('IssuerOfAccessionNumberSequence/UniversalEntityIDType', mwlEntry.get('IssuerOfAccessionNumberSequence/UniversalEntityIDType'))

set('ReferringPhysicianName', mwlEntry.get('ReferringPhysicianName'))

set('PatientID', mwlEntry.get('PatientID'))
set('IssuerOfPatientID', mwlEntry.get('IssuerOfPatientID'))
set('IssuerOfPatientIDQualifiersSequence/UniversalEntityID', mwlEntry.get('IssuerOfPatientIDQualifiersSequence/UniversalEntityID'))
set('IssuerOfPatientIDQualifiersSequence/UniversalEntityIDType', mwlEntry.get('IssuerOfPatientIDQualifiersSequence/UniversalEntityIDType'))

set('PatientName', mwlEntry.get('PatientName'))
set('PatientSex', mwlEntry.get('PatientSex'))
set('PatientBirthDate', mwlEntry.get('PatientBirthDate'))
set('PatientWeight', mwlEntry.get('PatientWeight'))
set('PatientState', mwlEntry.get('PatientState'))

set('MedicalAlerts', mwlEntry.get('MedicalAlerts'))
set('RequestingPhysician', mwlEntry.get('RequestingPhysician'))

set('RequestedProcedureID', mwlEntry.get('RequestedProcedureID'))
set('RequestedProcedurePriority', mwlEntry.get('RequestedProcedurePriority'))

set('OrderEnteredBy', mwlEntry.get('OrderEnteredBy'))
set('OrderEntererLocation', mwlEntry.get('OrderEntererLocation'))
set('OrderCallbackPhoneNumber', mwlEntry.get('OrderCallbackPhoneNumber'))

set('AdmissionID', mwlEntry.get('AdmissionID'))
set('IssuerOfAdmissionID', mwlEntry.get('IssuerOfAdmissionID'))

set('RequestedProcedureDescription', mwlEntry.get('RequestedProcedureDescription'))
set('RequestedProcedureCodeSequence/CodeValue', mwlEntry.get('RequestedProcedureCodeSequence/CodeValue'))
set('RequestedProcedureCodeSequence/CodingSchemeDesignator', mwlEntry.get('RequestedProcedureCodeSequence/CodingSchemeDesignator'))
set('RequestedProcedureCodeSequence/CodeMeaning', mwlEntry.get('RequestedProcedureCodeSequence/CodeMeaning'))



set('CurrentPatientLocation',mwlEntry.get('CurrentPatientLocation'))
log.debug(scriptName + "PatientLocation is {}",mwlEntry.get('CurrentPatientLocation'))
// a workaround to allow EV query by location. to be refactored in 7.3
set('ConfidentialityCode',mwlEntry.get('CurrentPatientLocation'))

// In this script, a matching MWL is already found, so order rules.
// to keep the values from OBR-2 and OBR-4. Note we are overwriting study description
// 0040,2016
set('PlacerOrderNumberImagingServiceRequest', mwlEntry.get('PlacerOrderNumberImagingServiceRequest'))
// 0008,1030
set('StudyDescription', mwlEntry.get('RequestedProcedureDescription'))
// 0020,000D Study Instance UID

def MWLStudyInstUID = mwlEntry.get('StudyInstanceUID')
def dcmStudyInstUID = get('StudyInstanceUID')

if (MWLStudyInstUID == null){
    log.warn(scriptName + "No study instance UID is defined in MWL entry. Preserving study instance UID from original DICOM study.")
}else{
    if (MWLStudyInstUID != dcmStudyInstUID) {
        log.info(scriptName + "Study instance UID in the original DICOM study is found to be different than the study instanced UID assigned by the MWL order.")
        log.info(scriptName + "overwriting the original study instance UID {} to {}", dcmStudyInstUID, MWLStudyInstUID)
        set('StudyInstanceUID', MWLStudyInstUID);
    }
}


log.debug(scriptName + "Finished reconciling SOP {}", get(SOPInstanceUID));
