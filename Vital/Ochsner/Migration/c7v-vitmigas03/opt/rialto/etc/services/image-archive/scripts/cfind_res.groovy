def scriptName = "IA - cfind response morpher - ";
def callingAET = getCallingAETitle().toUpperCase()

if (callingAET.startsWith("VITRENAS")){
	log.info(scriptName + "Calling AET is identified to be EasyViz, modify referring physian with a user friendly format.")
	// Gets the DICOM tag ReferringPhysician that contains EPIC^LASTNAME^FIRSTNAME
	def ref_phys_name_orig = get(ReferringPhysicianName)
        if (ref_phys_name_orig) {
	   // Removes the EpicID and leaves format as LASTNAME^FIRSTNAME and replaces ^ with ", " and changes format to requested LASTNAME, FIRSTNAME
	   def ref_phys_name_modified = ref_phys_name_orig.substring(ref_phys_name_orig.indexOf("^")+1).replaceAll("\\^", ", ")
    	   // def noCaratReferringPhysician = noEpicReferringPhysician.replaceAll("\\^", ", ")
	   set(ReferringPhysicianName, ref_phys_name_modified)
        }
}
