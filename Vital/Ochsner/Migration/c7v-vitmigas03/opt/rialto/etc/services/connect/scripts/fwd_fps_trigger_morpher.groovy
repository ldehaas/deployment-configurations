def scriptName = "Connect-forward - FPS trigger morpher - "

log.info(scriptName + "START")

def messageType = get('/.MSH-9-1')
def triggerEvent = get('/.MSH-9-2')
def controlCode = get('/.ORC-1')
def orderStatus = get('/.ORC-5')

def ValidModalityList = ["CT","MR","NM"]
def ValidStudyDescList = ["TRANSTHORACIC ECHO (TTE) LIMITED",
                          "TRANSTHORACIC ECHO (TTE) COMPLETE",
                          "ECHOCARDIOGRAM STRESS TEST",
                          "ECHOCARDIOGRAM STRESS TEST WITH COLOR FLOW DOPPLER",
                          "TRANSESOPHAGEAL ECHO (TEE) W/ POSSIBLE CARDIOVERSION",
                          "TRANSESOPHAGEAL ECHO (TEE)"
                          ]


if ("ORM".equalsIgnoreCase(messageType) && "O01".equalsIgnoreCase(triggerEvent) && "SC".equalsIgnoreCase(controlCode) && "Exam Ended".equalsIgnoreCase(orderStatus)) {
    log.info(scriptName + "Received ORM indicating QC completeness")
    def Modality = get('/.OBR-24').toUpperCase()
    def StudyDesc = get('/.OBR-4-2').toUpperCase()

    if (((Modality != null) && ValidModalityList.contains(Modality)) || ((StudyDesc != null) && ValidStudyDescList.contains(StudyDesc))) {
        log.info(scriptName + "Order will be processed for DICOM forwarding. Modality={}, StudyDescription={}", Modality, StudyDesc)
        return true
    } else {
        log.warn(scriptName + "Order NOT being process for DICOM forwarding. Modality={}, StudyDescription={}", Modality, StudyDesc)
        return false
    }
} else {
    log.warn(scriptName + "An unactionable message was privided. Expected ORM^O01 with ORC-1=SC and ORC-5=Exam Ended. Receiving {}^{} with ORC-1={} and ORC-5={}", messageType, triggerEvent, controlCode, orderStatus)
    return false
}

log.info(scriptName + "END")
