// pass the value for placer and filler numbers
def scriptName = "MWL cfind response morpher - "
log.info(scriptName + "start")

set(PlacerOrderNumberImagingServiceRequest,imagingServiceRequest.getPlacerIssuerAndNumber())
set(FillerOrderNumberImagingServiceRequest,imagingServiceRequest.getFillerIssuerAndNumber())

log.info(scriptName + "finish")

