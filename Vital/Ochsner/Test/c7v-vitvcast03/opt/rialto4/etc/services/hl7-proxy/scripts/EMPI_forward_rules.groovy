log.info("Start EMPI Morpher")

def messageType = get('/.MSH-9-1')
def triggerEvent = get('/.MSH-9-2')

log.info("messageType is: {}", messageType)
log.info("triggerEvent is: {}", triggerEvent)


set('MSH-5', 'OHSV4');
set('MSH-6', 'OCHSNER');

if (messageType != null && triggerEvent != null) {
    if ("ADT".equals(messageType)) {
        log.info("will forward ADT message to EMPI")
    } else {
        log.info("will not forward non-ADT message to EMPI")
        return false
    }
}

log.info("EMPI Morpher - Setting Sender OID into PID-3");

set('/.PID-3-2','');
set('/.PID-3-3','');
set('/.PID-3-4-1','OHSV4');
set('/.PID-3-4-2', '2.16.124.113638.7.6.2.5104');
set('/.PID-3-4-3', 'ISO');

log.info("End EMPI Morpher")
