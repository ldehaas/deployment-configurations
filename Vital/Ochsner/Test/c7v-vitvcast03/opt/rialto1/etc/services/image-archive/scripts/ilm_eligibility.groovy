log.info("ilm_eligibility.groovy START")

// If the given study is from a domain that "belongs" to the local data center,
// the script returns true, and the ILM will operate on the study:
if (localPid != null && localPid.domainUUID == "DC1") {
    return true;
} else {
    return false;
}

log.info("ilm_eligibility.groovy END")
