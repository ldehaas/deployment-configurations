def scriptName = "IA C-Find Req Morpher - ";

   def ipid_to_univId_map = [
     'VACE1':'2.16.124.113638.7.6.2.5101',
     'VACE2':'2.16.124.113638.7.6.2.5102',
     'VACE3':'2.16.124.113638.7.6.2.5103',
     'VACE4':'2.16.124.113638.7.6.2.5104',
     'VACE5':'2.16.124.113638.7.6.2.5105',
     'VACE6':'2.16.124.113638.7.6.2.5106'
   ]

log.info(scriptName + "START")

def calledAET = getCalledAETitle().toUpperCase();
def sopUID = get("SOPInstanceUID")

def patientID = input.get('PatientID')
def issuerOfPatientID = input.get('IssuerOfPatientID')

if ((patientID) && (ipid_to_univId_map.containsKey(issuerOfPatientID))) {
        log.debug("PID and valid IPID specified, will use this for cfind")
	setIPID(sopUID, issuerOfPatientID, ipid_to_univId_map.get(issuerOfPatientID))

} else {

	if (ipid_to_univId_map.containsKey(calledAET)) {
           log.debug("PID but no IPID specified, will use calledAET to find correct IPID")
           setIPID(sopUID, calledAET, ipid_to_univId_map.get(calledAET))

	} else {
	   log.info(scriptName + "No valid IPID or valid calledAET, C-Find request failed")	
	   return false;
	}
}



def setIPID(sopUID, namespace, universalid) {

        log.debug("SOP: " + sopUID + ", setting namespace to:" + namespace);
        set("IssuerOfPatientID", namespace)

        log.debug("SOP: " + sopUID + ", setting universal id to: " + universalid);
        set("IssuerOfPatientIDQualifiersSequence/UniversalEntityID", universalid, VR.LO)

        log.debug("SOP: " + sopUID + ", setting universal id type to: ISO");
        set("IssuerOfPatientIDQualifiersSequence/UniversalEntityIDType", "ISO", VR.LO)
}

log.debug(scriptName + "CFind Req: {}", input)

log.info(scriptName + "END")
