log.info("Start PH ORM/ORU Morpher")

def messageType = get('/.MSH-9-1')
def triggerEvent = get('/.MSH-9-2')

log.info("messageType is: {}", messageType)
log.info("triggerEvent is: {}", triggerEvent)

if (messageType != null && triggerEvent != null) {
    if ("ORM".equals(messageType) && ("O01".equals(triggerEvent))) {
        log.info("will forward ORM message to PH ORM interface")
    } else if ("ORU".equals(messageType) && ("R01".equals(triggerEvent))) {
        log.info("will forward ORU message to PH ORU interface")
    } else {
        log.info("will not forward this type of message to PH")
        return false
    }
}

set('/.MSH-4','VACE1')

log.info("PH ORM/ORU Morpher - Setting Sender OID into PID-3");

set('/.PID-3-2','');
set('/.PID-3-3','');
set('/.PID-3-4-1','OHSV1');
set('/.PID-3-4-2', '2.16.124.113638.7.6.2.5101');
set('/.PID-3-4-3', 'ISO');

log.info("End PH ORM/ORU Morpher")
