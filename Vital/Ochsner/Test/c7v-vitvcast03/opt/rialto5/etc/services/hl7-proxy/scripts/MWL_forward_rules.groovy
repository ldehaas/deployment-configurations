log.info("Start MWL Update Morpher")

def messageType = get('/.MSH-9-1')
def triggerEvent = get('/.MSH-9-2')

log.info("messageType is: {}", messageType)
log.info("triggerEvent is: {}", triggerEvent)

if (messageType != null && triggerEvent != null) {
    if ("ORM".equals(messageType) && ("O01".equals(triggerEvent))) {
        log.info("will forward this ORM message to MWL")
    } else {
        log.info("will not forward this type of message to MWL")
        return false
    }
}

log.info("MWL  Morpher - Setting Sender OID into PID-3");

set('/.PID-3-2','');
set('/.PID-3-3','');
set('/.PID-3-4-1','OHSV5');
set('/.PID-3-4-2', '2.16.124.113638.7.6.2.5105');
set('/.PID-3-4-3', 'ISO');

log.info("End MWL Update Morpher")
