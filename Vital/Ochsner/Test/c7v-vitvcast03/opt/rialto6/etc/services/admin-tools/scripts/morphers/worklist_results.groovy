/**
 * This script is executed once per DICOM modality worklist response
 * during the auto-reconcile workflow. The input 'xdsinputs' is a
 * collection of DocumentMetadataScriptingAPI each wrapping an XDS
 * document from the document-set (here the document-set comprises
 * all documents that have matching accession number within the
 * current patient domain).
 *
 * The aECG and PDF documents can be identified by their respective
 * MIME types, and can be interrogated for properties that will form
 * the basis for matching against DICOM modality worklist entries.
 *
 * The input 'dcminputs' is a collection of DicomScriptingAPI each
 * wrapping one result from the DICOM modality worklist response.
 * This script is responsible for selecting one matching DICOM
 * modality worklist entry (corresponding to an 'order') from the
 * collection of results so that downstream tasks in the workflow
 * might reconcile the document-set against that worklist entry.  
 *
 * Note: the script variables also include 'rawobjects' which is the
 * collection of DicomObject modality worklist entries that mirrors
 * the 'dcminputs' collection, so the matching worklist entry can be
 * returned as below:
 *
 * def match = findMatchingOrder(aecg.get(XDSCreationTime))
 * if (match == null) {
 *   log.error("Worklist match was not found")
 *   return null
 * }
 * return rawobjects[xdsinputs.indexOf(match)]
 *
 * This script should return null if no DICOM match was found. If a match was
 * found and the scheduled status indicates it is the final document in the
 * set of ECG documents uploaded, set the variable "documentStatus" to be not
 * null. EG. (documentStatus = "APPROVED") indicates to update the modality
 * worklist entry status to be "VERIFIED".
 * Note: DICOM modality worklist entries that have already been reconciled
 * against will have their ScheduledProcedureStepStatus set to "VERIFIED" only
 * if the scheduledStatus of the matched DICOM object has a status of "APPROVED"
 */
import org.joda.time.*
import com.karos.rtk.common.HL7v2Date;

def findMatchingWorklistEntry(serviceStartDatetime) {

    if (serviceStartDatetime == null) {
        log.warn("The match target document does not know its service start datetime.")
        return null
    }

    log.debug("service start datetime:{}", serviceStartDatetime)

    return dcminputs.find { dcmobject ->
        
        def issuedDatetime = new DateTime(dcmobject.getDate(
            IssueDateOfImagingServiceRequest,
            IssueTimeOfImagingServiceRequest))

        log.debug("issued datetime:{}", issuedDatetime)

        def scheduledProcedureList = dcmobject.get(ScheduledProcedureStepSequence)
        if (scheduledProcedureList.isEmpty()) {
            log.warn("The worklist entry does not contain scheduled procedures.")
            return false
        }

        def scheduledProcedure = scheduledProcedureList[0]

        def scheduledDatetime = new DateTime(
            scheduledProcedure.getDate(
                ScheduledProcedureStepStartDate,
                ScheduledProcedureStepStartTime))
        
        log.debug("scheduled datetime:{}", scheduledDatetime)

        def scheduledStatus = scheduledProcedure.get(ScheduledProcedureStepStatus)

        log.debug("scheduled status:{}", scheduledStatus)


        def placerordernumber = dcmobject.get(PlacerOrderNumberImagingServiceRequest)

        if (placerordernumber != null && placerordernumber.contains("RHEL")) {
            return serviceStartDatetime.isAfter(issuedDatetime) &&
                serviceStartDatetime.isBefore(scheduledDatetime.plusWeeks(3)) &&
                serviceStartDatetime.isAfter(scheduledDatetime.minusWeeks(3))
        }
        else {
            return scheduledStatus != "VERIFIED" &&
                scheduledStatus != "CANCELLED" &&
                serviceStartDatetime.isAfter(issuedDatetime) &&
                serviceStartDatetime.isBefore(scheduledDatetime.plusDays(3)) &&
                serviceStartDatetime.isAfter(scheduledDatetime.minusDays(3))
        }
    }
}

def findECG(mimeType) {
    return xdsinputs.find { document ->
        def format = document.get(XDSFormatCode)
        return format.getCodeValue() == "93010" &&
               format.getSchemeName() == "CPT-4" &&
               mimeType.equals(document.get(XDSMimeType))
    }
}

def getDocumentStatus(match) {

    def scheduledProcedureList = dcmOutput.get(ScheduledProcedureStepSequence)
    def scheduledProcedure = scheduledProcedureList[0]
    def scheduledStatus = scheduledProcedure.get(ScheduledProcedureStepStatus)

    // document status is a script variable that indicates whether or not we
    // want to update the WorklistEntryStatus to verified
    //only set this value if you wish to do that.
    if (scheduledStatus == "APPROVED") {
        return "APPROVED"
    }

    return null
}

def aecg = findECG("text/xml")

if (aecg == null) {
    aecg = findECG("application/xml")
}

if (aecg == null) {
    log.debug("aECG was not found - returning without match")
    return null
}

def orderingID = aecg.get(XDSExtendedMetadata("orderingID"))

if (orderingID != null && !orderingID.isEmpty()) {
    log.debug("AECG already matched")
    return null
}

log.debug("Evaluating worklist candidates against ECG documents")

def match = findMatchingWorklistEntry(aecg.get(XDSServiceStartTime))

if (match == null) {
    log.debug("Worklist match was not found")
    return null
}

documentStatus = getDocumentStatus(match)

return rawobjects[dcminputs.indexOf(match)]