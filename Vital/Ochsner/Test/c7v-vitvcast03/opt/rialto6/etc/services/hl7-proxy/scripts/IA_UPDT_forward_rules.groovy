log.info("Start IA Update Morpher")

def messageType = get('/.MSH-9-1')
def triggerEvent = get('/.MSH-9-2')

log.info("messageType is: {}", messageType)
log.info("triggerEvent is: {}", triggerEvent)

if (messageType != null && triggerEvent != null) {
    if ("ADT".equals(messageType) && ("A08".equals(triggerEvent) || "A40".equals(triggerEvent) || "A31".equals(triggerEvent) || "A39".equals(triggerEvent))) {
        log.info("will forward this ADT message to IA Update")
    } else if ("ORM".equals(messageType) && ("O01".equals(triggerEvent))) {
        log.info("will forward this ORM message to IA Update")
    } else {
        log.info("will not forward this type of message to IA Update")
        return false
    }
}



log.info("IA Update Morpher - Setting Sender OID into PID-3")
set('/.PID-3-2','');
set('/.PID-3-3','');
set('/.PID-3-4-1','OHSV6');
set('/.PID-3-4-2', '2.16.124.113638.7.6.2.5106');
set('/.PID-3-4-3', 'ISO');


log.info("End IA Update Morpher")
