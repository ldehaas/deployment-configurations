import com.karos.rialto.workflow.model.GenericWorkflow
import com.karos.rialto.workflow.model.AttemptEvent
import com.karos.rialto.workflow.model.Task.Status
import com.karos.rialto.workflow.common.tasks.*
import com.karos.rtk.xds.DocumentMetadata
import com.karos.xds.workflow.tasks.*
import com.karos.xds.workflow.events.common.*
import com.karos.rialto.workflow.events.xds.reconcile.*
import java.time.Duration

log.debug("Starting morpher [XDS Worklist Reconciliation Factory]")
log.debug("Morpher input contains {} XDS documents", xdsinputs.size())

if (xdsinputs.size() < 2) {
    log.debug("missing related document(s) - skipping workflow creation")
    return null
}

def findECG(mimeType) {
    return xdsinputs.find { document ->
        def format = document.get(XDSFormatCode)
        return format.getCodeValue() == "93010" &&
               format.getSchemeName() == "CPT-4" &&
               mimeType.equals(document.get(XDSMimeType))
    }
}

def aecg = findECG("text/xml")

if (aecg == null) {
    aecg = findECG("application/xml")
}

if (aecg == null) {
    log.debug("aECG not found - skipping workflow creation")
    return null
}

def pdf = findECG("application/pdf")

if (pdf == null) {
    log.debug("PDF not found - skipping workflow creation")
    return null
}

def automatic = (patientId == null || patientDomain == null || requestedProcedureId == null)

def registerDocumentsTaskBuilder = new RegisterDocumentsTask.Builder()
def claimWorklistEntryTaskBuilder = null
if (automatic) {
    claimWorklistEntryTaskBuilder = new ClaimWorklistEntryTask.Builder()
    claimWorklistEntryTaskBuilder.setWorklistRequestScript("${rialto.rootdir}/etc/services/admin-tools/scripts/morphers/worklist_request.groovy")
    claimWorklistEntryTaskBuilder.setWorklistResultsScript("${rialto.rootdir}/etc/services/admin-tools/scripts/morphers/worklist_results.groovy")
} else {
    claimWorklistEntryTaskBuilder = new UserClaimWorklistEntryTask.Builder()
    claimWorklistEntryTaskBuilder.setRequestedProcedureId(requestedProcedureId)
    claimWorklistEntryTaskBuilder.setIssuerOfAccessionNumber(issuerOfAccessionNumber)
    claimWorklistEntryTaskBuilder.setAccessionNumber(accessionNumber)
    claimWorklistEntryTaskBuilder.setPatientDomain(patientDomain)
    claimWorklistEntryTaskBuilder.setPatientId(patientId)
}
def reconcileDocumentsTaskBuilder = new ReconcileDocumentsTask.Builder()
def createHL7FromDocumentsTaskBuilder = new CreateHL7FromDocumentsTask.Builder()
def sendHL7TaskBuilder = new SendHL7Task.Builder()

def workflowTitle = automatic ? "Reconcile XDS Documents with Imaging Service Request (Automatic)"
                              : "Reconcile XDS Documents with Imaging Service Request (Manual)"

def workflowBuilder = new GenericWorkflow.Builder(workflowTitle)

for (DocumentMetadata document : documents) {
    registerDocumentsTaskBuilder.addInitialEvent(new DocumentRegisteredEvent(document,
        registerDocumentsTaskBuilder.getId(), AttemptEvent.Status.SUCCESS))
}

registerDocumentsTaskBuilder.setInitialStatus(Status.SUCCESS)

claimWorklistEntryTaskBuilder.addDependency(registerDocumentsTaskBuilder, true, Status.SUCCESS)
reconcileDocumentsTaskBuilder.addDependency(registerDocumentsTaskBuilder, true, Status.SUCCESS)
reconcileDocumentsTaskBuilder.addDependency(claimWorklistEntryTaskBuilder, true, Status.SUCCESS)
createHL7FromDocumentsTaskBuilder.addDependency(registerDocumentsTaskBuilder, true, Status.SUCCESS)
createHL7FromDocumentsTaskBuilder.addDependency(reconcileDocumentsTaskBuilder, true, Status.SUCCESS)
createHL7FromDocumentsTaskBuilder.addDependency(claimWorklistEntryTaskBuilder, true, Status.SUCCESS)
sendHL7TaskBuilder.addDependency(createHL7FromDocumentsTaskBuilder, true, Status.SUCCESS)

workflowBuilder.addTaskBuilder(registerDocumentsTaskBuilder)
workflowBuilder.addTaskBuilder(claimWorklistEntryTaskBuilder)
workflowBuilder.addTaskBuilder(reconcileDocumentsTaskBuilder)
workflowBuilder.addTaskBuilder(createHL7FromDocumentsTaskBuilder)
workflowBuilder.addTaskBuilder(sendHL7TaskBuilder)

def pid = aecg.get(XDSSourcePatientID)
def acn = aecg.get(XDSAccessionNumber)

if (acn != null && acn.size() == 1){
    workflowBuilder.setAccessionNumber(acn[0].getId())
}

workflowBuilder.setUniversalId(pid.domainUUID)
workflowBuilder.setPatientId(pid.pid)

claimWorklistEntryTaskBuilder.setWorklistAETitle(worklistAE)
claimWorklistEntryTaskBuilder.setWorklistHost(worklistHost)
claimWorklistEntryTaskBuilder.setWorklistPort(worklistPort)
claimWorklistEntryTaskBuilder.setLocalAETitle(localAE)
claimWorklistEntryTaskBuilder.setWorklistResource("http://localhost:8086/rialto/mwl/mwl/update")

reconcileDocumentsTaskBuilder.setReconcileDocumentScript("${rialto.rootdir}/etc/services/admin-tools/scripts/morphers/reconcile_document.groovy")
reconcileDocumentsTaskBuilder.setXDSRegistryResource("http://localhost:9106/registry")

createHL7FromDocumentsTaskBuilder.setXdsToHl7ConversionScript("${rialto.rootdir}/etc/services/admin-tools/scripts/morphers/convert_xds2scn.groovy")

sendHL7TaskBuilder.setMorpher("${rialto.rootdir}/etc/services/admin-tools/scripts/morphers/morph_scn.groovy")
sendHL7TaskBuilder.setDestinationHost("localhost")
sendHL7TaskBuilder.setDestinationPort(2575)

return workflowBuilder.build()
