import org.joda.time.DateTime
import org.joda.time.format.DateTimeFormat
import java.text.SimpleDateFormat


// This script is referenced by multiple instances of image archive services;

// Values assigned to the following fields will be overwritten on the way out. So don't attempt to set them here.
// MSH-3 Sending Application    - from the properties of device id PH-SCN (VC_IA)
// MSH-4 Sending Facility       - from the properties of device id PH-SCN (OHS_EI)
// MSH-5 Receiving Application  - from the properties of device id PH-SCN (PH_SCN)
// MSH-6 Receiving Facility     - from the properties of device id PH-SCN (OHS_EI)


initialize( 'ORM', 'O01', '2.3' );
output.getMessage().addNonstandardSegment('IPC')
log.debug("input is {}", input);
def CallingAET = getCallingAETitle();
def CalledAET = getCalledAETitle();

log.debug("Calling AE Title is {} and Called AE Title is {}", CallingAET,  CalledAET);


sdf = new SimpleDateFormat("yyyyMMddHHmmss")

//set('MSH-3','OHS_EI')
//set('MSH-4',CalledAET)
//set('MSH-5','OHS_EI')
//set('MSH-6','PH_SCN')

set('MSH-7', sdf.format(new Date()))

set('PID-3-1', input.get(PatientID));
set('PID-3-4-1', input.get(IssuerOfPatientID))
set('PID-3-4-2', input.get("IssuerOfPatientIDQualifiersSequence/UniversalEntityID"))
set('PID-3-4-3', get("IssuerOfPatientIDQualifiersSequence/UniversalEntityIDType"))
//set('PID-3-5', 'MPI')
setPersonName('PID-5', input.get(PatientName));

set('ORC-1', 'XO')

set('ORC-17','VC_IA')
if (CalledAET != null){
    // Called AET should indicate pool when the study is sent in
    set('ORC-18', CalledAET)
}else{
    // If CalledAET is null, then this is most likely SCN from study deletion. Use Calling AET instead, which should indicate pool.
    set('ORC-18', CallingAET)
}
//
set('OBR-1', '1')
set('OBR-2', input.get(PlacerOrderNumberImagingServiceRequest))
set('OBR-3', input.get(AccessionNumber))
//
// Set the Procedure Code depending on the Calling AE Title

//set('OBR-4-1',  input.get(RequestedProcedureID))
set('OBR-4', input.get(StudyDescription))
set('OBR-7', input.get(StudyDate))
//set('OBR-16-9', 'SMART')
//set('OBR-16-13', 'PROV')
set('OBR-25', 'I')

set('OBX-1', '1')
set('OBX-2', 'TX')
set('OBX-3-1-2', 'GDT')
set('OBX-5', 'Y')
set('IPC-3', input.get(StudyInstanceUID))
