import java.time.LocalDateTime
import java.time.temporal.ChronoUnit
import java.time.format.DateTimeFormatter
import java.time.format.DateTimeParseException

log.debug("in.groovy: Start inbound morpher")


def issuerOfPatientID = get("IssuerOfPatientID")
def universalEntityID = get("IssuerOfPatientIDQualifiersSequence/UniversalEntityID")
def universalEntityIDType = get("IssuerOfPatientIDQualifiersSequence/UniversalEntityIDType")
def sopUID = get("SOPInstanceUID")
def patientDateOfBirth = get(PatientBirthDate)
def patientTimeOfBirth = get(PatientBirthTime)
def studyDate = get(StudyDate)
def studyTime = get(StudyTime)
def patientAge = get(PatientAge)


log.debug("in.groovy: Original IPID is {}, original UID is {}, and original UID type is {}", issuerOfPatientID, universalEntityID, universalEntityIDType)

setIPID(sopUID, issuerOfPatientID, universalEntityID, universalEntityIDType)


try {
    if (patientAge == null) {
        set("PatientAge", calculateAge(patientDateOfBirth, patientTimeOfBirth, studyDate, studyTime))
    }
} catch (Exception e) {
    log.warn("Failed to compute PatientAge. Proceeding without changing PatientAge. ", e)
}



def setIPID(sopUID, onamespace, ouniversalid, ouniversalidtype) {

        if (onamespace != null || ouniversalid != null || ouniversalidtype != null) {

            log.debug("SOP: " + sopUID + ", : Going to move existing issuer information [IussuerOfPatientID:  " + onamespace + ", UniversalEntityID: " + ouniversalid + ", UniversalEntityIDType: " + ouniversalidtype + "] to a VitalImages specific private tag")

            set(0x00350010, "VITAL IMAGES PRESERVED INFO 1.0", VR.LO)
            set(0x00351010, onamespace, VR.LO)
            set(0x00351011, ouniversalid, VR.LO)
            set(0x00351012, ouniversalidtype, VR.LO)
        }

        log.debug("SOP: " + sopUID + ", setting namespace to: ");
        set("IssuerOfPatientID", "OHSV2")

        log.debug("SOP: " + sopUID + ", setting universal id to: 2.16.124.113638.7.6.2.5102");
        set("IssuerOfPatientIDQualifiersSequence/UniversalEntityID", "2.16.124.113638.7.6.2.5102", VR.LO)

        log.debug("SOP: " + sopUID + ", setting universal id type to: ISO");
        set("IssuerOfPatientIDQualifiersSequence/UniversalEntityIDType", "ISO", VR.LO)
}


String calculateAge(String patientDateOfBirth, String patientTimeOfBirth, String studyDate, String studyTime){
    def TIPPING_WEEKS_TO_DAYS = 4
    def TIPPING_MONTHS_TO_WEEKS = 3
    def TIPPING_YEARS_TO_MONTHS = 2
    
    if (patientDateOfBirth == null || studyDate == null) {
        return null
    }

    patientDateOfBirth = patientDateOfBirth.replace(":", "")
    studyDate = studyDate.replace(":", "")

    def patientDateTimeOfBirth
    def studyDateTime

    try {
        def dtf = DateTimeFormatter.ofPattern("yyyyMMddHHmmss")
        patientDateTimeOfBirth = LocalDateTime.parse(patientDateOfBirth + cleanTime(patientTimeOfBirth), dtf)
        studyDateTime = LocalDateTime.parse(studyDate + cleanTime(studyTime), dtf)
    } catch (DateTimeParseException e) {
        log.warn("in.groovy: Failed to parse PatientBirthDate or StudyDate.", e)
        return null
    }

    if (!patientDateTimeOfBirth.isBefore(studyDateTime)) {
        return null
    }

    def years = ChronoUnit.YEARS.between(patientDateTimeOfBirth, studyDateTime)
    def months = ChronoUnit.MONTHS.between(patientDateTimeOfBirth, studyDateTime)
    def weeks = ChronoUnit.WEEKS.between(patientDateTimeOfBirth, studyDateTime)
    def days = ChronoUnit.DAYS.between(patientDateTimeOfBirth, studyDateTime)
    def age = ""

    if (years > 999) {
        return null
    }
    if (years >= TIPPING_YEARS_TO_MONTHS) {
        age = years + "Y"
    } else if (months >= TIPPING_MONTHS_TO_WEEKS) {
        age = months + "M"
    } else if (weeks >= TIPPING_WEEKS_TO_DAYS) {
        age = weeks + "W"
    } else {
        age = days + "D"
    }

    return age.padLeft(4, "0")
}

String cleanTime(String str) {
    if (str == null) {
        return "000000"
    }
    str = str.replace(":", "") 
    if (str.indexOf(".") == 6) {
        str = str.substring(0, 6)
    }
    if (str.contains(".") || str.length() > 6 || str.length()%2 == 1) {
        return "000000"
    }
    return str.padRight(6, "0")
}

log.debug("in.groovy: End inbound morpher")
