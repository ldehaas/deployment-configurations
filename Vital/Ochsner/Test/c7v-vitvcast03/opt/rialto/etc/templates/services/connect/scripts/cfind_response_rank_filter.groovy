/*
 * cfind_response_filter.groovy
 *
 * This script is used in the <prop name="FetchPriors">/<cfindResponseFilter> 
 * tag.
 *
 * Use this script to determine which priors discovered from the queried DICOM
 * AE's are relevant to fetch for this workflow. Return a list of relevant 
 * priors to fetch at the end of this script that you would like to be sent
 * to your MoveDestination as configured in the Connect service.
 *
 * To access the list of DICOM C-FIND objects returned, use the variable name
 * "inputs". This represents a list of C-FIND responses. Each object in this
 * list can be accessed similarly to how you may access the DICOM C-FIND request
 * in the "hl7_to_cfind_morpher.groovy" file. Both the "set()" and "get()"
 * method will access details of the DICOM object. For example, to set the value
 * of the first DICOM C-FIND response within this list:
 * 
 * inputs[0].set("PatientId","12345")
 * OR
 * inputs[0].set(PatientId,"12345")
 *
 * To access the PatientId value within the first DICOM C-FIND response within 
 * this list:
 * inputs[0].get("PatientId")
 * OR
 * inputs[0].get(PatientId)
 *
 * An example of how to iterate over the list of C-FIND responses can be found
 * in the script below.
 *
 * You may also access the prefetch order in this script by using the "order"
 * variable. You can access fields in this by using the "get()" method. For
 * example, to access the original PatientId found in the order:
 *
 * def pid = get("PID-3-1")
 *
 */

log.debug("cfind_response_filter.groovy: Starting cfind_response_rank_filter")


/*
 * We can configure this script to sort studies that are older than a specified
 * threshold. For this example, any studies that are older than 2 weeks.
 */

def validPriorList = []

/*
 * We use the org.joda.time.DateTime() class to evaluate time. When 
 * instantiating this object, it uses the current time. The minusWeeks()
 * method allows us to substract a configurable number of weeks from the current
 * time, allowing us to fetch priors that were created within the past 2 weeks.
 * Other subtraction methods may be used. For example:
 * 
 * - minusYears(value): Subtract the [value] number of years
 * - minusDays(value): Subtract the [value] number of days
 */
def earliestStudyDate =  new org.joda.time.DateTime().minusWeeks(2)

/*
 * This will iterate over the list of C-FIND responses and evaluate the 
 * StudyDate and StudyTime to determine whether this prior is relevent to us. To
 * access each response in this loop, we use the variable "it" to represent the
 * iterator variable while inside this loop. If we deem a C-FIND response to be
 * relevant, we will add it to a list variable called vallidPriorList that we 
 * created above. 
 *
 * We may also use this loop to evaluate each C-FIND response for other
 * information that was reqiested om tje response. For example, we may wish to filter
 * relevant priors based on modality as well as StudyDate/Time.
 */
inputs.each {

    if (it.getDate(StudyDate, StudyTime) < earliestStudyDate) {

        log.trace("cfind_response_filter.groovy: Prior falls outside of maximum age {}", earliestStudyDate)

    } else {

        // Add it to the list of studies that are within the correct date range
        log.trace("cfind_response_filter.groovy: Prior falls inside of maximum age {}", earliestStudyDate)
        validPriorList.add(it)
    }
}

log.debug("cfind_response_filter.groovy: Identified priors to prefetch:\n {}",
        validPriorList)

log.debug("cfind_response_filter.groovy: Ending cfind_response_rank_filter")

/*
 * Always return the list of priors we wish to prefetch. If an empty list is
 * returned, no priors will be fetched.
 */
return validPriorList 
