/*
 * sr_to_oru_morpher.groovy
 *
 * This script creates an ORU report message from a DICOM Structured Report (SR).
 *
 * This script is optional. If not configured, SRs will be passed directly to 
 * the PACS. If configured, the SRs will not be delivered, but the generated ORU
 * will be. The input SR will have already been localized by the foreign image
 * localizer script.
 *
 * To access values from the DICOM SR object, use the "get()" method. For 
 * example, to retrieve the PatientId from the object: 
 * 
 * def patientId = get("PatientId")
 * OR
 * def patientId = get(PatientId)
 *
 * In order to set values in the ORU message, use the "set()" method. For
 * example, to set a segment in the HL7 message:
 * 
 * set("PID-2-1", get(PatientID))
 * 
 * This script may optionally return a boolean value (either true or false). If
 * the return value of the script is false then the DICOM SR will be dropped and
 * no ORU will be sent.
 */

 /*
  * This method will loop through and gather the report information from the SR
  * and write it to the ORU message. The method reserveOBX() returns
  * a new obx segment that is then represented by the $obx variable in the loop.
  */

def gatherTextContent(contentSeq, completionIndicator, observationTime, output) {
    meaning = null
    contentSeq.each { content ->
        if ("TEXT".equals(content.get(ValueType))) {
            def obx = reserveOBX()
            def codeValue = ""
            def meaning = ""
            def conceptName = ""

            conceptNameCodeSeq = content.get(ConceptNameCodeSequence)
            if (!conceptNameCodeSeq.isEmpty()) {
                meaning = conceptNameCodeSeq.first().get(CodeMeaning)
                codeValue = conceptNameCodeSeq.first().get(CodeValue)
                conceptName = conceptNameCodeSeq.first().get(CodingSchemeDesignator)
            }

            set("$obx-2", "TX")

            set("$obx-3-1",codeValue)
            set("$obx-3-2",meaning)
            set("$obx-3-3",conceptName)
            
            set("$obx-5", content.get(TextValue))

            set("$obx-11", completionIndicator)
            set("$obx-14", observationTime)

        } else if ("CONTAINER".equals(content.get(ValueType))) {
            gatherTextContent(content.get(ContentSequence), completionIndicator, observationTime, output)
        } else if ("NUM".equals(content.get(ValueType))) {
            def obx = reserveOBX()
            def codeValue = ""
            def meaning = ""
            def conceptName = ""
            conceptNameCodeSeq = content.get(ConceptNameCodeSequence)

            if (!conceptNameCodeSeq.isEmpty()) {
                meaning = conceptNameCodeSeq.first().get(CodeMeaning)
                codeValue = conceptNameCodeSeq.first().get(CodeValue)
                conceptName = conceptNameCodeSeq.first().get(CodingSchemeDesignator)
            }

            set("$obx-2", "NM")

            set("$obx-3-1",codeValue)
            set("$obx-3-2",meaning)
            set("$obx-3-3",conceptName)
            
            set("$obx-5", content.get(MeasuredValueSequence).first().get(NumericValue))

            set("$obx-11", completionIndicator)
            set("$obx-14", observationTime)
        }
    }
}

/*
 * Some SRs have invalid escape sequences in their report content. For example,
 * the string "\X09\" instead of a tab character.  This causes visual problems
 * in the reports as displayed by PACSs so we should replace them with what is
 * likely the correct character.
 */
def replaceInvalidEscapeSequences(content) {
    // It appears that the escape sequence is \X??\ where ?? is a hex number
    // representing the actual ascii code for the character.  So we parse that
    // to a number and cast it to a char to get the intended string value.
    return content.replaceAll("\\\\X(\\p{XDigit}{2})\\\\") {
        (char) Integer.parseInt(it[1], 16)
    }
}

/*
 * We must initialize the HL7 message first to be the correct message type and
 * represent the correct HL7 version we wish our message to represent.
 */
initialize("ORU", "R01", "2.5")

/*
 * Because this is an HL7v2.5 message, we must initialize all three parts of the
 * MSH-9 segment to represent the proper message type.
 */
set("MSH-9-1","ORU")
set("MSH-9-2","R01")
set("MSH-9-3","ORU_R01")


/*********************/
/*    PID SEGMENT    */
/*********************/

/*
 * Here we will set the PID segment to represent the patient information we can
 * retrieve from the DICOM SR
 */
set("PID-2-1", get(PatientID))
set("PID-3-1", get(PatientID))
set("PID-3-5-1", "TST")
set("PID-3-5-2", "test.test")
set("PID-3-5-3", "ISO")

/*
 * The method setPersonName() takes two arguments, the first is the HL7 field we
 * wish to set. The second is the patient name, delimited by the '^' character.
 * For example, to set the name Joe Smith, we would use the following:
 *
 * setPersonName("PID-5", "Joe^Smith")
 *
 */
setPersonName("PID-5", get(PatientName))

set("PID-7", get(PatientBirthDate))
set("PID-8", get(PatientSex))

set("ORC-1", "OK")
set("ORC-3", get(AccessionNumber))

set("OBR-1", "1")
set("OBR-3", get(AccessionNumber))
set("OBR-4", get(StudyDescription))
set("OBR-7", get(StudyDate) + get(StudyTime))

anatomicRegionSeq = get(AnatomicRegionSequence)
if (!anatomicRegionSeq.isEmpty()) {
    anatomicRegion = anatomicRegionSeq.first()
    set("OBR-15-1-1", anatomicRegion.get(CodeValue))
    set("OBR-15-1-2", anatomicRegion.get(CodeMeaning))
    set("OBR-15-1-3", anatomicRegion.get(CodingSchemeDesignator))
}

setPersonName("OBR-16", get(ReferringPhysicianName))

set("OBR-19", get(AccessionNumber))
set("OBR-20", get(StudyID))
set("OBR-22", get(ContentDate) + get(ContentTime))
set("OBR-24", get(Modality))

def completionFlag = 'P'


if (get(VerificationFlag) == 'VERIFIED') {
    completionFlag = 'F'
}

if (get(CompletionFlag) != 'COMPLETE') {
    completionFlag = 'S'
}

set("OBR-25", completionFlag)

reportTextItems = []
gatherTextContent(get(ContentSequence),completionFlag,get(ContentDate) + get(ContentTime), reportTextItems)

set("ZDS-1", get(StudyInstanceUID))