cd /opt/rialto/etc/templates
namespace=ohsv1
for index in cfindinfo context device event osr priorruleset routeruleset script space
do
    curl -s -XPUT "http://localhost:9200/_template/$namespace-$index" -d"$(sed 's/default-/'$namespace'-/g' $index.template.json)"
done
