#!/bin/bash
#
# Start and Stop Rialto with Automatic Restart Loop
#
# This is the "init.d" script for Rialto.
#
# To enable automatic start and stop at boot time and shutdown,
# enter the command "chkconfig <app_instance_name> on"
# e.g. chkconfig rialto on
#
# To disable automatic start and stop at boot and shutdown enter
# the command "chkconfig <app_instance_name> off"
# e.g. chkconfig rialto off
#
# Copyright Karos Health Incorporated 2018
# Rewritten with auto-restart September 2015.
#
# Configuration for chkconfig(8):
# chkconfig: 5 99 01
# description: Rialto - a product of Karos Health
#

#######################
# General Configuration
#######################
APP_NAME=Rialto3                     # Used in log messages
APP_INSTANCE_NAME=rialto3            # e.g. rialto, rialto2, rialto3, ...
APP_USERID=rialto                   # Non-root userid that app runs as
APP_HOME=/opt/$APP_INSTANCE_NAME
export RIALTO_HOME=$APP_HOME        # Used by Rialto JRuby script

APP_JAR=$APP_HOME/lib/jar/jruby-complete-1.7.27.jar
APP_RUBY_SCRIPT=$APP_HOME/lib/rubybin/runrialto.rb
APP_TEMP_DIR=$APP_HOME/var/tmp

# Note: Location of App PID file is also known to the install-rialto script
PID_DIR=/var/run/$APP_USERID
APP_PID_FILE=$PID_DIR/$APP_INSTANCE_NAME.pid                # Application pid
RESTART_PID_FILE=$PID_DIR/${APP_INSTANCE_NAME}_restart.pid  # Restart loop pid
APP_EXIT_STATUS_FILE=$PID_DIR/$APP_INSTANCE_NAME.exit_status
SERVICE_START_FAILURE=222

LOG_DIR=/var/log/$APP_USERID
LOG_FILE=$LOG_DIR/${APP_INSTANCE_NAME}_initd.log
STDOUT_FILE=$LOG_DIR/${APP_INSTANCE_NAME}_start_stdout.log
STDERR_FILE=$LOG_DIR/${APP_INSTANCE_NAME}_start_stderr.log
JAVA_STDOUT_FILE=$LOG_DIR/${APP_INSTANCE_NAME}_java_stdout.log
JAVA_STDERR_FILE=$LOG_DIR/${APP_INSTANCE_NAME}_java_stderr.log

# Environment variables for use in rialto.cfg.xml
# IP_ADDR_NIC1, IP_ADDR_NIC2, IP_ADDR_NIC3, etc.
# IP_ADDR_ETH0, IP_ADDR_ETH1 (deprecated)
declare -i i=0
for ip in $(hostname -I); do
    eval "IP_ADDR_ETH$i=$ip" # Deprecated (there is no eth0 on Red Hat)
    let ++i
    eval "IP_ADDR_NIC$i=$ip" # Deprecated (there could be 2+ IPs on a NIC)
    eval "IP_ADDR_$i=$ip"    # Works for both CentOS and Red Hat
done

####################
# Java Configuration
####################

# Get definitions for JAVA and JAVA_HOME
. /etc/profile.d/rialto.sh

# Determine how much memory is available (in MB)
declare -i systemMemory=$(free -m | awk '/Mem:/ {print $2}')

declare -i reserveMemory=384 # Reserve for OS
if [ -f /etc/init.d/cassandra ]; then
    let reserveMemory+=3800 # Reserve for Cassandra
fi

declare -i availableMemory=$((systemMemory - reserveMemory))

# Choose Java heap size (in MB)
declare -i HeapSize
declare -i MAX_JVM_HEAP=8192
if [[ $availableMemory -lt 1024 ]]; then
    HeapSize=1024
elif [[ $availableMemory -lt $MAX_JVM_HEAP ]]; then
    HeapSize=$availableMemory
else
    HeapSize=$MAX_JVM_HEAP
fi

JAVA_OPTS="
-Xms512M
-Xmx${HeapSize}M
-XX:+HeapDumpOnOutOfMemoryError
-Dfile.encoding=UTF-8
-Djava.net.preferIPv4Stack=true
-Djava.io.tmpdir=$APP_TEMP_DIR
-server
"
# to debug ssl handshakesa, add the following to JAVA_OPTS
# -Djavax.net.debug=all

# to monitor rialto via jvisualvm, add the following to JAVA_OPTS
# -Dcom.sun.management.jmxremote

# to remotely attach a debugger, add the following to JAVA_OPTS
# -Xdebug -Xrunjdwp:transport=dt_socket,server=y,suspend=n,address=1044
# and then in eclipse, debug configurations, add a remote java application
# to this host on port 1044


#======== START TEMPORARY (until JVM bug fixed) ==============================
# Disable compilation of run method NewRialtoDefaultWebSecurityManager
# For more information, see RIALTO-6700
#
file='/opt/rialto/etc/hotspot_compiler'
if [ ! -f "$file" ]; then # Create if file doesn't exist
    classPath='com/karos/rialto/security/userauthentication/mgt'
    class='NewRialtoDefaultWebSecurityManager'
    echo "exclude $classPath/$class\$1 run" >$file
    chown $APP_USERID: $file
fi
JAVA_OPTS="$JAVA_OPTS -XX:CompileCommandFile=$file"
#======== END TEMPORARY (until JVM bug fixed) ================================


#############
# Subroutines
#############
. /etc/rc.d/init.d/functions

log() {
    echo $(date) $* >>$LOG_FILE
}

die() {
    echo 'ERROR:' $* 1>&2
    log "ERROR: $*"
    exit 1
}

quitIfNotRoot() {
    if [ $UID -ne 0 ]; then
        echo 'Sorry, must be root'
        exit 1
    fi
}

isAppAlive() {
    test -f $APP_PID_FILE && checkpid $(cat $APP_PID_FILE)
}

isRestartAlive() {
    test -f $RESTART_PID_FILE && checkpid $(cat $RESTART_PID_FILE)
}

getUptime() {
    local file="$1"
    ps --no-headers -p $(cat $file) -o etime
}

createLogFile() {
    if [ $UID -ne 0 ]; then
        return
    fi
    local file
    for file in $* ; do
        if [ ! -f $file ]; then
            touch $file
        fi
        chown $APP_USERID: $file
    done
}


doStart() {
    # Sanity checks
    if [ ! -r $APP_JAR ]; then
        die "Application JAR file not found: $APP_JAR"
    fi
    if [ ! -r $APP_RUBY_SCRIPT ]; then
        die "Application jruby script not found: $APP_RUBY_SCRIPT"
    fi

    # Prevent starting two copies of the application.
    if isRestartAlive; then
        echo "$APP_NAME auto restart shell is already running"
    fi
    if isAppAlive; then
        echo "$APP_NAME is already running"
        if ! isRestartAlive; then
            echo "$APP_NAME auto restart shell is not running"
        fi
        exit 0
    fi
    if isRestartAlive; then
        echo "$APP_NAME is not running but will restart automatically"
        exit 0
    fi

    # Put a shell into background that will automatically restart
    # the application in case it should die.
    log 'Starting auto restart shell'
    echo -n "Starting $APP_NAME auto restart shell: "
    (
        echo $BASHPID >$RESTART_PID_FILE # PID of current shell

        while [ 'true' ]; do
            log "Starting $APP_NAME"

            su $APP_USERID -c "
                cd $APP_HOME
                export IP_ADDR_ETH0='$IP_ADDR_ETH0' # Deprecated
                export IP_ADDR_ETH1='$IP_ADDR_ETH1' # Deprecated
                export IP_ADDR_NIC1='$IP_ADDR_NIC1'
                export IP_ADDR_NIC2='$IP_ADDR_NIC2'
                export LD_LIBRARY_PATH='$APP_HOME/lib/native'

                exec $JAVA $(echo $JAVA_OPTS) \\
                    -jar $APP_JAR --1.9 $APP_RUBY_SCRIPT \\
                    >>$JAVA_STDOUT_FILE \\
                    2>>$JAVA_STDERR_FILE &

                echo \$! >$APP_PID_FILE
                wait \$!
                echo \$? >$APP_EXIT_STATUS_FILE

            " >>$STDOUT_FILE 2>>$STDERR_FILE

            # Poll the application until Java dies
            # Note: Although this seems redundant after the "wait",
            #       Oana saw some weirdness while testing that went
            #       away after this extra check was restored.
            pid=$(cat $APP_PID_FILE)
            while [ -e /proc/$pid ]; do
                sleep 30
            done

            # Exit auto restart shell on fatal error
            status=$(cat $APP_EXIT_STATUS_FILE)
            if [ "$status" == "$SERVICE_START_FAILURE" ]; then
                log "$APP_NAME start failed (status=$status), disabling restart"
                break
            fi
            log "$APP_NAME stopped unexpectedly (status=$status), will restart"

            # Don't be in a rush to restart.  If the application cannot start,
            # we don't want to fill the hard drive with log files.
            sleep 25
        done
    ) &

    # Report whether the auto restart shell started.
    if [ $? -eq 0 ]; then
        echo_success
    else
        echo_failure
    fi
    echo

    # Report whether the application started.
    echo -n "Starting $APP_NAME: "
    sleep 1
    if isAppAlive; then
        ExitValue=0
        echo_success
    else
        ExitValue=1
        echo_failure
    fi
    echo
}


doStop() {
    # Stop the auto restart shell
    if isRestartAlive; then
        log 'Stopping auto restart shell'
        echo -n "Stopping $APP_NAME auto restart shell: "
        if kill $(cat $RESTART_PID_FILE); then
            ExitValue=0
            echo_success
        else
            ExitValue=1
            echo_failure
        fi
        echo
    else
        echo "$APP_NAME auto restart shell is already stopped"
    fi

    # Make sure it's really stopped
    while isRestartAlive; do
        echo 'Waiting for auto restart shell to stop ...'
        sleep 2
    done

    # Stop the application
    if isAppAlive; then
        log "Stopping $APP_NAME"
        echo -n "Stopping $APP_NAME: "
        if kill $(cat $APP_PID_FILE); then
            sleep 1
            ExitValue=0
            echo_success
        else
            ExitValue=1
            echo_failure
        fi
        echo
    else
        echo "$APP_NAME is already stopped"
    fi

    # Make sure it's really stopped
    while isAppAlive; do
        echo "Waiting for $APP_NAME to stop ..."
        sleep 2
    done
}


doStatus() {
    # Report auto restart shell status
    if isRestartAlive; then
        echo "$APP_NAME auto restart shell is running, uptime" \
            $(getUptime $RESTART_PID_FILE)
    else
        echo "$APP_NAME auto restart shell is not running"
    fi

    # Report app status
    if isAppAlive; then
        echo "$APP_NAME is running, uptime" $(getUptime $APP_PID_FILE)
        exit 0
    elif isRestartAlive; then
        echo "$APP_NAME is not running (but will restart automatically)"
    else
        echo "$APP_NAME is not running"
    fi

    # Report last exit status if any.
    if [ -f $APP_EXIT_STATUS_FILE ]; then
        status=$(cat $APP_EXIT_STATUS_FILE)
        if [ "$status" == "$SERVICE_START_FAILURE" ]; then
            echo "$APP_NAME last exit status = $status " \
                "(a service failed to start)"
        else
            echo "$APP_NAME last exit status was $status"
        fi
    fi
}


# Create folders and log files
if [ $UID -eq 0 ]; then
    mkdir -m 0755 -p $PID_DIR ; chown $APP_USERID: $PID_DIR
    mkdir -m 0755 -p $LOG_DIR ; chown $APP_USERID: $LOG_DIR
fi
createLogFile $LOG_FILE $STDOUT_FILE $STDERR_FILE \
    $JAVA_STDOUT_FILE $JAVA_STDERR_FILE


ExitValue=0
case "$1" in
    start)
        quitIfNotRoot
        doStart
        ;;
    stop)
        quitIfNotRoot
        doStop
        ;;
    status)
        doStatus
        ;;
    restart)
        quitIfNotRoot
        doStop
        doStart
        ;;
    *)
    echo 'Usage: $0 {start|stop|restart|status}'
    ExitValue=3
esac

exit $ExitValue
