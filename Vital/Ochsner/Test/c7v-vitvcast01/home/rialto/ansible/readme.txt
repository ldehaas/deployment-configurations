Quick Examples:

ansible-playbook -l [pattern] [playbook]
* patterns should match a host in ~/ansible/inventories/ochsner.yml
* playbook file are in ~/ansible
* common tasks:


--- Test connectivity to all production servers
# ansible-playbook -l prod_dc*_app vc_test_connection.yml


--- Rolling restart of nifi and rialto on the target server
# ansible-playbook -l test_dc1_app vc_restart_services.yml


--- Rolling restart of nifi on the target server
# ansible-playbook -l test_dc1_app vc_restart_nifi.yml


--- Sync an app node config from localhost
# ansible-playbook -l test_dc1_app vc_sync_config.yml


--- Redeploy an app node config using config from localhost
# ansible-playbook -l test_dc1_app vc_redeploy_config.yml


--- Copy a single file (ad hoc command) from this server:
# ansible-playbook -l test_dc1_app vc_copy_file.yml
# ansible test_dc1_app -e "fn=/home/rialto/test.txt" -m copy -a "src={{fn}} dest={{fn}}"


--- Measure mint retrieval time per server
# ansible-playbook -l prod_dc*_app vc_test_mint.yml



For more instructions, visit:
https://karoshealth.atlassian.net/wiki/spaces/~yi.lu/pages/737084035/Automation+with+Ansible
