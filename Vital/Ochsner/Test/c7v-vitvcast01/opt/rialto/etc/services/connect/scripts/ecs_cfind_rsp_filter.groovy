def scriptName = "Connect - ecs_cfind_rsp_filter.groovy "

log.debug(scriptName + "Start")

def modalitiesInStudy = order.get("OBR-24")
def studyYears = 5
def numberOfPriorsToReturn = 3
def validPriorList = []
if (modalitiesInStudy != null  && modalitiesInStudy.contains("MG")) {
    studyYears = 6
    numberOfPriorsToReturn = 5
}

def earliestStudyDate =  new org.joda.time.DateTime().minusYears(studyYears)
log.debug(scriptName + "Calculated earliestStudyDate is '{}'.", earliestStudyDate)

log.info("Complete list of studies being examined:\n{}",
inputs.collect({
    it.get(StudyInstanceUID) + ": " + it.getDate(StudyDate, StudyTime)
}).join("\n"))

inputs.each {
    if (it.getDate(StudyDate, StudyTime) < earliestStudyDate) {
        log.trace("Prior falls outside of maximum age '{}' and is dropped.", earliestStudyDate)
    } else {
        log.trace("Prior falls inside of maximum age '{}' and is retained.", earliestStudyDate)
        validPriorList.add(it)
    }
}

validPriorList.sort(byRecency)

log.info("Studies after removing ones that are too old:\n{}",
validPriorList.collect({
    it.get(StudyInstanceUID) + ": " + it.getDate(StudyDate, StudyTime)
}).join("\n"))

// Returns the correct number of priors based on prior age
return first(validPriorList, numberOfPriorsToReturn)


log.debug(scriptName + "End")
