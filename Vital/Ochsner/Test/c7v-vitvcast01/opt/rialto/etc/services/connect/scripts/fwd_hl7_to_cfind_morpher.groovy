/*
 * HL7 to cfind morpher
 * Convert an order message into a cfind message.
 * get() works on the order message, while set() works on the 
 * cfind message.
 * 
 * The order is ignored if this script returns false,
 * e.g. no images will be fetched for the order.
 */
def scriptName = "Connect - fwd_hl7_to_cfind_morpher.groovy"

log.info(scriptName + "Start")

def PatId = get('/.PID-3-1')
def ReqStudyInstUID = get('/.ZDS-1')
def AccNum = get('/.ORC-3-1')

if (AccNum && PatId) {
   set(AccessionNumber, AccNum)
   set(PatientID, PatId)

   set('IssuerOfPatientID', get('/.PID-3-4-1'))
   set('IssuerOfPatientIDQualifiersSequence/UniversalEntityID', get('/.PID-3-4-2'))
   set('IssuerOfPatientIDQualifiersSequence/UniversalEntityIDType', 'ISO')

   log.info(scriptName + "Setting PatientID to {} and AccessionNumber to {} in C-Find-Rq", PatId, AccNum)
}else if (ReqStudyInstUID) {
   set(StudyInstanceUID, ReqStudyInstUID)
   log.warn(scriptName + "Unable to find AccessionNumber, setting StudyInstanceUID to {} in C-Find-Rq", ReqStudyInstUID)
}else{
   log.error(scriptName + "cannot find either AccessionNumber or requested study instance UID. Dropping this request.")
   return false
}

log.info(scriptName + "End")
