def scriptName = "Connect - fwd_fps_trigger_morpher.groovy"

log.info(scriptName + "START")

def messageType = get('/.MSH-9-1')
def triggerEvent = get('/.MSH-9-2')
def controlCode = get('/.ORC-1')
def orderStatus = get('/.ORC-5')
def accNumber = get('/.ORC-3-1')

def ValidModalityList = ["CT","MR","NM","PT"]
def ValidStudyDescList = ["TRANSTHORACIC ECHO (TTE) LIMITED",
                          "TRANSTHORACIC ECHO (TTE) COMPLETE",
                          "ECHOCARDIOGRAM STRESS TEST",
                          "ECHOCARDIOGRAM STRESS TEST WITH COLOR FLOW DOPPLER",
                          "TRANSESOPHAGEAL ECHO (TEE) W/ POSSIBLE CARDIOVERSION",
                          "TRANSESOPHAGEAL ECHO (TEE)"
                          ]
def StudyDesc = ""
def Modality = ""

if ("ORM".equalsIgnoreCase(messageType) && "O01".equalsIgnoreCase(triggerEvent) && "SC".equalsIgnoreCase(controlCode) && "Exam Ended".equalsIgnoreCase(orderStatus) && (get('/.OBR-24')) && (get('/.OBR-4-2'))) {
    log.info(scriptName + "Received ORM indicating QC completeness")
    if (get('/.OBR-24')){
        Modality = get('/.OBR-24').toUpperCase()
        StudyDesc = (get('/.OBR-4-2'))?(get('/.OBR-4-2').toUpperCase()):""
    }else{
        log.warn(scriptName + "Order NOT being processed because Modality value is invalid.")
        return false
    }

    if (((Modality) && ValidModalityList.contains(Modality)) || ((StudyDesc) && ValidStudyDescList.contains(StudyDesc))) {
        log.info(scriptName + "Order {} will be processed for DICOM forwarding. Modality={}, StudyDescription={}", accNumber, Modality, StudyDesc)
        return true
    } else {
        log.warn(scriptName + "Order {} NOT being process for DICOM forwarding. Modality={}, StudyDescription={}", accNumber, Modality, StudyDesc)
        return false
    }
} else {
    log.warn(scriptName + "An unactionable message was privided. Expected ORM^O01 with ORC-1=SC and ORC-5=Exam Ended. Receiving {}^{} with ORC-1={} and ORC-5={}", messageType, triggerEvent, controlCode, orderStatus)
    return false
}

log.info(scriptName + "END")
