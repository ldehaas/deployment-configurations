# Nginx config for multiple admin tools instances
server {
    listen       2500 ssl http2;
    server_name 147.206.236.189;

    include /etc/nginx/ssl/default.conf;

    add_header Strict-Transport-Security max-age=15768000;
    add_header X-Frame-Options DENY;
    add_header X-Content-Type-Options nosniff;

    # Enable gzipping of responses.
    gzip on;
    gzip_types text/plain application/xml application/json application/javascript application/x-javascript text/css;

    # Enable checking the existence of precompressed files.
    gzip_static on;

    location ~* ^/(rest|public) {
        proxy_pass http://localhost:2400;
        proxy_set_header Origin $http_origin;
        proxy_http_version 1.1;
    }

    location ^~ /docs/ {
        root /var/www/;
        index Default.htm;
        add_header X-Frame-Options SAMEORIGIN;
    }

    location ^~ /v2 {
        rewrite /v2/?$ /v2/en/ permanent;
        # Rewrite anything that looks like a page (no file extension) to /v2/{locale} for Angular
        rewrite /v2/([^/]*/)[^.]+$ /v2/$1 break;
        proxy_pass http://localhost:2700;
        proxy_redirect default;
        proxy_http_version 1.1;
    }

    location / {
        proxy_pass http://localhost:2600;
        proxy_redirect http:// https://;
        proxy_set_header Host $host:$server_port;
        proxy_http_version 1.1;
        # Double proxy_read_timeout to 120 for loading large studies in AdminTools
        proxy_read_timeout 120;
    }
}

# https->http, gzip on
server {
    listen		8079 	ssl http2;
    server_name   	147.206.236.189;
    include /etc/nginx/ssl/default.conf;
    
    gzip on;
    gzip_types application/octet-stream text/plain application/xml application/json;
    gzip_static on;

    location / {
	proxy_pass http://localhost:8080;
	proxy_redirect http:// https://;
	proxy_http_version 1.1;
    } 
}


