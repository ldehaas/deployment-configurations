#!/bin/bash
#
# This script applies the workaround for KHC14343
# Apply this workaround to production only after running spawn-instance.sh and DO NOT reapply
# To remove the workaround, simply run spawn-instance.sh again
# This workaround should be removed after the KHC is address and clean-up work is completed

if [[ ! -f ia1-workaround.txt ]] || [[ ! -f ia5-workaround.txt ]] ; then
    echo " Expecting file ia1-workaround.txt and ia5-workaround.txt"
    exit
fi

chmod +w image-archive-1.xml
chmod +w image-archive-5.xml

linenum=$(awk '/<!-- Production Storage Locations ending flag -->/{ print NR; exit }' image-archive-1.xml)
echo inserting ia1-workaround.txt after line $linenum in image-archive-1.xml
sed -n -i -e '/Migration Storage Locations starting flag/r ia1-workaround.txt' -e 1x -e '2,${x;p}' -e '${x;p}' image-archive-1.xml

linenum=$(awk '/<!-- Production Storage Locations ending flag -->/{ print NR; exit }' image-archive-5.xml)
echo inserting ia1-workaround.txt after line $linenum in image-archive-5.xml
sed -n -i -e '/Migration Storage Locations starting flag/r ia5-workaround.txt' -e 1x -e '2,${x;p}' -e '${x;p}' image-archive-5.xml

chmod -w image-archive-1.xml
chmod -w image-archive-5.xml
