import java.time.LocalDateTime
import java.time.temporal.ChronoUnit
import java.time.format.DateTimeFormatter
import java.time.format.DateTimeParseException

LOAD('/opt/rialto/etc/services/common/common_ohs.groovy')

// NOTE: Please be aware that this script is referenced from multiple image archive services. 
// Every scenario should work for multiple image archives.
// NOTE: IPID stamping is exclusively based on called AE title! The IPID in the original incoming DICOM tag is ignored.

log.debug("in.groovy: Start inbound morpher")

def callingAET = getCallingAETitle().toUpperCase();
def calledAET = getCalledAETitle().toUpperCase();
def sopIUID = get("SOPInstanceUID")
if (sopIUID == null) {return false}

def scriptName = "IA in.groovy [" + calledAET + "]:"

log.info(scriptName + "SOP inst UID: {}, callingAET is {} and calledAET is {}", sopIUID, callingAET, calledAET)
set(ReceivingAE, calledAET)

def new_ipid = ''
def new_univId = ''

def old_issuerOfPatientId = get(IssuerOfPatientID)
def old_universalEntityID = get("IssuerOfPatientIDQualifiersSequence/UniversalEntityID")
def old_universalEntityIDType = get("IssuerOfPatientIDQualifiersSequence/UniversalEntityIDType")
log.debug(scriptName + "original IssuerOfPatientId is {} ", old_issuerOfPatientId)

// special requirement for image availability workflow:
// when agfa sending images to Peds dirty pool, set iPID to OHSEPIC, strip leading 0s from Pid,
if (calledAET == 'VCIAP15' && callingAET == OHSCommon.GetHMCAgfaStoreSCU()) {
    // Morph PID
    def orig_PID = get("PatientID")
    def out_PID = null
    def orig_AccNum = get("AccessionNumber")
    def out_AccNum = null

    if (orig_PID.substring(0,1) == '0'){
        out_PID = orig_PID.replaceFirst("^0+(?!\$)","")
        set("PatientID", out_PID)
        set(0x00351111, orig_PID, VR.LO)
    }

    if (orig_AccNum.length() >= 0 && orig_AccNum.length() <= 15 && orig_AccNum.substring(0,1) != 'Z'){
        out_AccNum = "Z" + orig_AccNum
        set("AccessionNumber", out_AccNum)
        set(0x00351112, orig_AccNum, VR.LO)
    }

    if (out_PID != null || out_AccNum != null){
        log.debug(scriptName + "Modified PatientID={}, modified AccessionNumber={}", out_PID, out_AccNum)
        set(0x00351010, "VITAL IMAGES PRESERVED INFO 1.0", VR.LO)
    }else{
        log.debug(scriptName + "No changes to PatientID or AccessionNumber")
    }

    // Morph iPid
    old_issuerOfPatientId = OHSCommon.GetDefiPid()
}


if (OHSCommon.IsValidiPid(old_issuerOfPatientId)) {
    if (old_universalEntityID == OHSCommon.GetunivIdbyiPID(old_issuerOfPatientId)){
        log.info(scriptName + "No need to morph the original iPid!")
    }else{
        new_ipid = old_issuerOfPatientId
        new_univId = OHSCommon.GetunivIdbyiPID(old_issuerOfPatientId)
    }
}else{
    if (OHSCommon.IsDirtyAE(calledAET)){
        new_ipid = OHSCommon.GetiPIDbyAET(calledAET)
        new_univId = OHSCommon.GetunivIdbyiPID(new_ipid)
    }else{
        // There is no way to know what iPid domain the incoming study is intended for
        new_ipid = 'UNKNOWN'
        new_univId = OHSCommon.GetunivIdbyiPID(new_ipid)
    }
}

if (new_ipid != '' || new_univId != ''){
    // Preserving exisitn IPID info
    if ((old_issuerOfPatientId != null && old_issuerOfPatientId != OHSCommon.GetDefiPidUnivId()) || old_universalEntityID != null || old_universalEntityIDType != null) {
        log.info(scriptName + "SOP Inst UID " + sopIUID + ": moving existing issuer information [IssuerOfPatientID: " + old_issuerOfPatientId + ", UniversalEntityID: " + old_universalEntityID + ", old_universalEntityIDType: " + old_universalEntityIDType + "] to private tag.")

        set(0x00350010, "VITAL IMAGES PRESERVED INFO 1.0", VR.LO)
        set(0x00351010, old_issuerOfPatientId, VR.LO)
        set(0x00351011, old_universalEntityID, VR.LO)
        set(0x00351012, old_universalEntityIDType, VR.LO)
    }

    log.info(scriptName + "SOP Inst UID " + sopIUID + ":: setting namespace to:{}", new_ipid)
    set("IssuerOfPatientID", new_ipid)

    log.info(scriptName + "SOP Inst UID " + sopIUID + ":: setting universal id to:{}", new_univId)
    set("IssuerOfPatientIDQualifiersSequence/UniversalEntityID", new_univId)

    log.info(scriptName + "SOP Inst UID " + sopIUID + ":: setting universal id type to: ISO")
    set("IssuerOfPatientIDQualifiersSequence/UniversalEntityIDType", "ISO")

}


// In this situation, we want to force the study to be VERIFIED
//  study is being stored in clean pool, but is from DesAcc's migration AE
def MigrationAETList = ['DESACCDCE']
if ((OHSCommon.IsCleanAE(calledAET) && MigrationAETList.contains(callingAET))){
    //set('VerificationFlag', 'VERIFIED')
    log.debug(scriptName + "migrated study from AET DESACCDCE, this will be marked as Verified")
    set('StudyStatusID','VER')
}


def patientDateOfBirth = get(PatientBirthDate)
def patientTimeOfBirth = get(PatientBirthTime)
def studyDate = get(StudyDate)
def studyTime = get(StudyTime)
def patientAge = get(PatientAge)

try {
    if (patientAge == null) {
        set("PatientAge", calculateAge(patientDateOfBirth, patientTimeOfBirth, studyDate, studyTime))
    }
} catch (Exception e) {
    log.warn("Failed to compute PatientAge. Proceeding without changing PatientAge. ", e)
}

String calculateAge(String patientDateOfBirth, String patientTimeOfBirth, String studyDate, String studyTime){
    def TIPPING_WEEKS_TO_DAYS = 4
    def TIPPING_MONTHS_TO_WEEKS = 3
    def TIPPING_YEARS_TO_MONTHS = 2
    
    if (patientDateOfBirth == null || studyDate == null) {
        return null
    }

    patientDateOfBirth = patientDateOfBirth.replace(":", "")
    studyDate = studyDate.replace(":", "")

    def patientDateTimeOfBirth
    def studyDateTime

    try {
        def dtf = DateTimeFormatter.ofPattern("yyyyMMddHHmmss")
        patientDateTimeOfBirth = LocalDateTime.parse(patientDateOfBirth + cleanTime(patientTimeOfBirth), dtf)
        studyDateTime = LocalDateTime.parse(studyDate + cleanTime(studyTime), dtf)
    } catch (DateTimeParseException e) {
        log.warn("in.groovy: Failed to parse PatientBirthDate or StudyDate.", e)
        return null
    }

    if (!patientDateTimeOfBirth.isBefore(studyDateTime)) {
        return null
    }

    def years = ChronoUnit.YEARS.between(patientDateTimeOfBirth, studyDateTime)
    def months = ChronoUnit.MONTHS.between(patientDateTimeOfBirth, studyDateTime)
    def weeks = ChronoUnit.WEEKS.between(patientDateTimeOfBirth, studyDateTime)
    def days = ChronoUnit.DAYS.between(patientDateTimeOfBirth, studyDateTime)
    def age = ""

    if (years > 999) {
        return null
    }
    if (years >= TIPPING_YEARS_TO_MONTHS) {
        age = years + "Y"
    } else if (months >= TIPPING_MONTHS_TO_WEEKS) {
        age = months + "M"
    } else if (weeks >= TIPPING_WEEKS_TO_DAYS) {
        age = weeks + "W"
    } else {
        age = days + "D"
    }

    return age.padLeft(4, "0")
}

String cleanTime(String str) {
    if (str == null) {
        return "000000"
    }
    str = str.replace(":", "") 
    if (str.indexOf(".") == 6) {
        str = str.substring(0, 6)
    }
    if (str.contains(".") || str.length() > 6 || str.length()%2 == 1) {
        return "000000"
    }
    return str.padRight(6, "0")
}

String genIUID() {
    def uuidString = UUID.randomUUID().toString()
    def uidString = ''
    def uidRoot = '1.2.840.113747.20080222' // This is the Vital UID root. You could use this or a site-specific one.

    uuidString.each { digit ->
        if (digit == '-') {
            uidString += '.'
        }
        else {
            uidString += Integer.parseInt(digit, 16).toString()
        }
    }
    def newUID = uidRoot + '.' + uidString 
    return newUID
}

log.debug("in.groovy: End inbound morpher")
