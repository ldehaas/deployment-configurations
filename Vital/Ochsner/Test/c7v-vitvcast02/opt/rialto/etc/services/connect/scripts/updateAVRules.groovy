import groovy.json.JsonOutput
import groovy.json.JsonSlurper
import javax.net.ssl.HostnameVerifier
import javax.net.ssl.HttpsURLConnection
import javax.net.ssl.SSLContext
import javax.net.ssl.TrustManager
import javax.net.ssl.X509TrustManager
import java.security.SecureRandom
import java.security.cert.CertificateException
import java.security.cert.X509Certificate

// updateAVRules.groovy v11
String scriptName = 'updateAVRules.groovy'
// LOAD('/opt/rialto/etc/services/connect/scripts/AVCodes.groovy')
// import AVCodes

// get commandline arguments
Map<String,String> inputArgs = [:]
args.each { argument ->
    String[] argArray = argument.tokenize('=')
    if (argArray.size() == 2 ){
        inputArgs[argArray[0]] = argArray[1]
    }
}
if (inputArgs.size() == 0) {
    println 'Usage: rtkgrv ' + scriptName + ' rialtoHost={rialtoHost} rialtoPort={rialtoPort} password={system.root password}'
}
String rialtoHost = inputArgs['rialtoHost'] ?: '147.206.236.189' //TEST B
String rialtoPort = inputArgs['rialtoPort'] ?: '2500'
String password = inputArgs['password'] ?: 'removed'
// String entityId = inputArgs['entityId']

String facility = 'OHS_EI'
String connectApplication = 'VC_CONN_FWD'
String vimsAE = 'VIMS_AETB'
String cardiacVimsAE = 'VIMS_AETB' //change to CVIMS_AE for production; using same AV in TEST
String cardiacUltrasoundAE = 'TTASRV'
String creatorHost = 'c7v-vitvcast02.ochsner.org'
String ownerContext = '2.25.65066631204490692481020178991382140296'

Map avHL7Ruleset = [
        'name': 'HL7 - Vitrea Advanced Orders',
        'description': 'Orders that trigger sending to Vitrea Advanced',
        'defaultRuleSet': false,
        'inputType': 'HL7',
        'matchingCriteria': new RadHL7MatchingCriteria(AVCodes.procedureCodes).getMatchingCriteria(),
        'actions': [
                [
                        'facility': facility,
                        'application': connectApplication,
                        'actionType': 'SEND_HL7'
                ]
        ],
        'signature': [
                'createdWhere': creatorHost,
                'createdWhen': new Date().toInstant().toString(),
                'createdWhy': 'Update trigger procedure codes',
                'createdBy': 'system.root@DefaultSystemRealm',
                'ownedBy': ownerContext
        ]
]

Map avDICOMRuleset = [
        'name': 'DICOM - Vitrea Advanced Studies',
        'description': 'Studies that trigger sending to Vitrea Advanced',
        'defaultRuleSet': false,
        'inputType': 'DICOM',
        'matchingCriteria': new RadDICOMMatchingCriteria(AVCodes.procedureCodes).getMatchingCriteria(),
        'actions': [
                [
                        'destinationAETitle': vimsAE,
                        'actionType': 'SEND_STUDY'
                ]
        ],
        'signature': [
                'createdWhere': creatorHost,
                'createdWhen': new Date().toInstant().toString(),
                'createdWhy': 'Update routing procedure codes',
                'createdBy': 'system.root@DefaultSystemRealm',
                'ownedBy': ownerContext
        ]
]

Map cardiacHL7Ruleset = [
        'name': 'HL7 - Vitrea Advanced Cardiac Orders',
        'description': 'Orders that trigger sending to Vitrea Advanced Cardiac',
        'defaultRuleSet': false,
        'inputType': 'HL7',
        'matchingCriteria': new CardHL7MatchingCriteria(AVCodes.cardiacProcedureCodes).getMatchingCriteria(),
        'actions': [
                [
                        'facility': facility,
                        'application': connectApplication,
                        'actionType': 'SEND_HL7'
                ]
        ],
        'signature': [
                'createdWhere': creatorHost,
                'createdWhen': new Date().toInstant().toString(),
                'createdWhy': 'Update trigger procedure codes',
                'createdBy': 'system.root@DefaultSystemRealm',
                'ownedBy': ownerContext
        ]
]
Map cardiacDICOMRuleset = [
        'name': 'DICOM - Vitrea Advanced Cardiac Studies',
        'description': 'Studies that trigger sending to Vitrea Advanced Cardiac',
        'defaultRuleSet': false,
        'inputType': 'DICOM',
        'matchingCriteria': new CardDICOMMatchingCriteria(AVCodes.cardiacProcedureCodes).getMatchingCriteria(),
        'actions': [
                [
                        'destinationAETitle': cardiacVimsAE,
                        'actionType': 'SEND_STUDY'
                ]
        ],
        'signature': [
                'createdWhere': creatorHost,
                'createdWhen': new Date().toInstant().toString(),
                'createdWhy': 'Update routing procedure codes',
                'createdBy': 'system.root@DefaultSystemRealm',
                'ownedBy': ownerContext
        ]
]
Map cardiacUSRuleSet =  [
        'name': 'DICOM - Cardiac Measurement Reports',
        'description': 'Studies that trigger sending to Cardiac Measurement Report System',
        'defaultRuleSet': false,
        'inputType': 'DICOM',
        'matchingCriteria': new CardDICOMMatchingCriteria(AVCodes.cardiacUSProcedureCodes, true).getMatchingCriteria(),
        'actions': [
                [
                        'destinationAETitle': cardiacUltrasoundAE,
                        'actionType': 'SEND_STUDY'
                ]
        ],
        'signature': [
                'createdWhere': creatorHost,
                'createdWhen': new Date().toInstant().toString(),
                'createdWhy': 'Update routing procedure codes',
                'createdBy': 'system.root@DefaultSystemRealm',
                'ownedBy': ownerContext
        ]
]

VCAuthorizer vcAuthorizer = new VCAuthorizer()
String rialtoAuthURL = 'https://' + rialtoHost + ':' + rialtoPort + '/rest/login'
String authToken = vcAuthorizer.getAuthToken(rialtoAuthURL,'system.root', password)

String rialtoRulesetURLString = 'https://' + rialtoHost + ':' + rialtoPort + '/rest/api/routingrules/routingRuleSets'

String avHL7RuleJson = JsonOutput.toJson(avHL7Ruleset)
String avDICOMRuleJson = JsonOutput.toJson(avDICOMRuleset)
String cardiacHL7RuleJson = JsonOutput.toJson(cardiacHL7Ruleset)
String cardiacDICOMRuleJson = JsonOutput.toJson(cardiacDICOMRuleset)
String cardiacUSRuleJson = JsonOutput.toJson(cardiacUSRuleSet)

URL rialtoRulesetURL = new URL(rialtoRulesetURLString)
// Write AV HL7 Rules

HttpsURLConnection rulesetConnection = (HttpsURLConnection) rialtoRulesetURL.openConnection()
rulesetConnection.setRequestProperty('Authorization', authToken)
rulesetConnection.setDoOutput(true)
rulesetConnection.setRequestProperty("Content-Type", "application/json")
OutputStreamWriter jsonOutput = new OutputStreamWriter(rulesetConnection.getOutputStream())
jsonOutput.write(avHL7RuleJson)
jsonOutput.close()
Integer responseCode = rulesetConnection.getResponseCode()
println(scriptName + ' HL7 AV Rules HTTP Response code = ' + responseCode.toString())


// Write AV DICOM Rules

rulesetConnection = (HttpsURLConnection) rialtoRulesetURL.openConnection()
rulesetConnection.setRequestProperty('Authorization', authToken)
rulesetConnection.setDoOutput(true)
rulesetConnection.setRequestProperty("Content-Type", "application/json")
jsonOutput = new OutputStreamWriter(rulesetConnection.getOutputStream())
jsonOutput.write(avDICOMRuleJson)
jsonOutput.close()
rulesetConnection.disconnect()
responseCode = rulesetConnection.getResponseCode()
println(scriptName + ' DICOM AV Rules HTTP Response code = ' + responseCode.toString())


// Write Cardiac HL7 Rules

rulesetConnection = (HttpsURLConnection) rialtoRulesetURL.openConnection()
rulesetConnection.setRequestProperty('Authorization', authToken)
rulesetConnection.setDoOutput(true)
rulesetConnection.setRequestProperty("Content-Type", "application/json")
jsonOutput = new OutputStreamWriter(rulesetConnection.getOutputStream())
jsonOutput.write(cardiacHL7RuleJson)
jsonOutput.close()
rulesetConnection.disconnect()
responseCode = rulesetConnection.getResponseCode()
println(scriptName + ' HL7 Cardiac Rules HTTP Response code = ' + responseCode.toString())


// Write Cardiac DICOM Rules

rulesetConnection = (HttpsURLConnection) rialtoRulesetURL.openConnection()
rulesetConnection.setRequestProperty('Authorization', authToken)
rulesetConnection.setDoOutput(true)
rulesetConnection.setRequestProperty("Content-Type", "application/json")
jsonOutput = new OutputStreamWriter(rulesetConnection.getOutputStream())
jsonOutput.write(cardiacDICOMRuleJson)
jsonOutput.close()
rulesetConnection.disconnect()
responseCode = rulesetConnection.getResponseCode()
println(scriptName + ' DICOM Cardiac Rules HTTP Response code = ' + responseCode.toString())


// Write Cardiac US DICOM Rules
rulesetConnection = (HttpsURLConnection) rialtoRulesetURL.openConnection()
rulesetConnection.setRequestProperty('Authorization', authToken)
rulesetConnection.setDoOutput(true)
rulesetConnection.setRequestProperty("Content-Type", "application/json")
jsonOutput = new OutputStreamWriter(rulesetConnection.getOutputStream())
jsonOutput.write(cardiacUSRuleJson)
jsonOutput.close()
rulesetConnection.disconnect()
responseCode = rulesetConnection.getResponseCode()
println(scriptName + ' DICOM Cardiac US Rules HTTP Response code = ' + responseCode.toString())

rulesetConnection.disconnect()

//println avHL7RuleJson
//println avDICOMRuleJson
//println cardiacHL7RuleJson
//println cardiacDICOMRuleJson
//println cardiacUSRuleJson

class VCAuthorizer {
    private def trustAllCerts = [
            new X509TrustManager() {
                X509Certificate[] getAcceptedIssuers() {
                    return null
                }

                void checkServerTrusted(X509Certificate[] certs, String authType) throws CertificateException {
                }

                void checkClientTrusted(X509Certificate[] certs, String authType) throws CertificateException {
                }
            }
    ] as TrustManager[]

    private def trustAllHosts = [
            verify: { hostname, session -> true }
    ]

    String getAuthToken(String authURLString, String username='system.root', String password, String authDomain = 'DefaultSystemRealm') {
        try {
            SSLContext sc = SSLContext.getInstance("SSL")
            sc.init(null, trustAllCerts, new SecureRandom())
            HttpsURLConnection.setDefaultSSLSocketFactory(sc.getSocketFactory())
            HttpsURLConnection.setDefaultHostnameVerifier(trustAllHosts as HostnameVerifier)
        } catch (Exception ignored) {
            return null
        }
        try {
            URL authURL = new URL(authURLString)
            HttpsURLConnection authConnection = (HttpsURLConnection) authURL.openConnection()
            authConnection.setRequestMethod('POST')
            authConnection.setRequestProperty('X-Userid', username)
            authConnection.setRequestProperty('X-Password', password)
            authConnection.setRequestProperty('X-Realm', authDomain)
            Integer authResponseCode = authConnection.getResponseCode()
            if (authResponseCode != 200) {
                authConnection.disconnect()
                return null
            }
            DataInputStream authResponse = new DataInputStream(authConnection.getInputStream())
            JsonSlurper jsonSlurper = new JsonSlurper()
            Object authJSON = jsonSlurper.parseText(authResponse.getText())
            authResponse.close()
            authConnection.disconnect()
            return 'Bearer ' + (authJSON.access_token as String)
        } catch (MalformedURLException ignored) {
            return null
        }
    }
}

class ConditionDetail {
    Map conditionDetails = [:]

    ConditionDetail (String matchVariable, String matchType, List<String> matchValues) {
        conditionDetails['matchVariable'] = matchVariable
        conditionDetails['matchType'] = matchType
        conditionDetails['matchValues'] = matchValues
    }

    ConditionDetail (String matchVariable, String matchType, String matchValue) {
        conditionDetails['matchVariable'] = matchVariable
        conditionDetails['matchType'] = matchType
        conditionDetails['matchValues'] = [matchValue]
    }

    Map getConditions() {
        return conditionDetails
    }
}

class HL7Condition {
    Map condition = [:]

    HL7Condition (String matchVariable, String matchType, def matchValues) {
        condition['conditionType'] = 'HL7_FIELD'
        condition['conditionDetails'] = new ConditionDetail(matchVariable, matchType, matchValues).getConditionDetails()
    }

    Map getCondition() {
        return condition
    }
}

class DICOMCondition {
    Map condition = [:]

    DICOMCondition (String matchVariable, String matchType, def matchValues) {
        condition['conditionType'] = 'DICOM_TAG'
        condition['conditionDetails'] = new ConditionDetail(matchVariable, matchType, matchValues).getConditionDetails()
    }

    Map getCondition() {
        return condition
    }
}

class AECondition {
    Map condition = [:]

    AECondition (String aeDirection, String matchValue) {
        String conditionType
        if (aeDirection == 'CALLED') {
            conditionType = 'MATCHING_CALLED_AE_TITLE'
        } else if (aeDirection == 'CALLING') {
            conditionType = 'MATCHING_CALLING_AE_TITLE'
        }
        if (conditionType && matchValue) {
            condition['conditionType'] = conditionType
            condition['conditionDetails'] = new ConditionDetail('','EQUALS', matchValue).getConditionDetails()
        }
    }

    Map getCondition() {
        return condition
    }
}

class RadHL7MatchingCriteria {
    List matchingCriteria = []

    RadHL7MatchingCriteria (File procedureCodeFile) {
        procedureCodeFile.eachLine { procedureCode ->
            matchingCriteria << ['conditions': [
                    new HL7Condition('/.MSH-9-1', 'EQUALS', 'ORM').getCondition(),
                    new HL7Condition('/.MSH-9-2', 'EQUALS', 'O01').getCondition(),
                    new HL7Condition('/.ORC-1', 'EQUALS', 'SC').getCondition(),
                    new HL7Condition('/.ORC-5', 'EQUALS', 'EXAM ENDED').getCondition(),
                    new HL7Condition('/.OBR-4-2', 'EQUALS', procedureCode).getCondition()
            ]]
        }
    }

    RadHL7MatchingCriteria (List<String> procedureCodeList) {
        procedureCodeList.each { procedureCode ->
            matchingCriteria << ['conditions': [
                    new HL7Condition('/.MSH-9-1', 'EQUALS', 'ORM').getCondition(),
                    new HL7Condition('/.MSH-9-2', 'EQUALS', 'O01').getCondition(),
                    new HL7Condition('/.ORC-1', 'EQUALS', 'SC').getCondition(),
                    new HL7Condition('/.ORC-5', 'EQUALS', 'EXAM ENDED').getCondition(),
                    new HL7Condition('/.OBR-4-2', 'EQUALS', procedureCode).getCondition()
            ]]
        }
    }

    List getMatchingCriteria () {
        return matchingCriteria
    }
}

class CardHL7MatchingCriteria {
    List matchingCriteria = []

    CardHL7MatchingCriteria (File procedureCodeFile) {
        procedureCodeFile.eachLine { procedureCode ->
            matchingCriteria << ['conditions': [
                    new HL7Condition('/.MSH-9-1', 'EQUALS', 'ORM').getCondition(),
                    new HL7Condition('/.MSH-9-2', 'EQUALS', 'O01').getCondition(),
                    new HL7Condition('/.ORC-1', 'EQUALS', 'SC').getCondition(),
                    new HL7Condition('/.ORC-5', 'EQUALS', 'EXAM ENDED').getCondition(),
                    new HL7Condition('/.OBR-4-2', 'EQUALS', procedureCode).getCondition()
            ]]
        }
    }

    CardHL7MatchingCriteria (List<String> procedureCodeList) {
        procedureCodeList.each { procedureCode ->
            matchingCriteria << ['conditions': [
                    new HL7Condition('/.MSH-9-1', 'EQUALS', 'ORM').getCondition(),
                    new HL7Condition('/.MSH-9-2', 'EQUALS', 'O01').getCondition(),
                    new HL7Condition('/.ORC-1', 'EQUALS', 'SC').getCondition(),
                    new HL7Condition('/.ORC-5', 'EQUALS', 'EXAM ENDED').getCondition(),
                    new HL7Condition('/.OBR-4-2', 'EQUALS', procedureCode).getCondition()
            ]]
        }
    }

    List getMatchingCriteria () {
        return matchingCriteria
    }
}

class RadDICOMMatchingCriteria {
    List matchingCriteria = []

    RadDICOMMatchingCriteria (File procedureCodeFile, Boolean srOnly = false) {
        procedureCodeFile.eachLine { procedureCode ->
            List conditions = [
                    new DICOMCondition('0008,1030', 'EQUALS', procedureCode).getCondition(),
                    new AECondition('CALLED', 'ROUTER_FWD').getCondition()
		    
                ]
            if (srOnly) {
                conditions << new DICOMCondition('0008,0060', 'EQUALS', 'SR').getCondition()
            }
            matchingCriteria << ['conditions': conditions]
        }
    }

    RadDICOMMatchingCriteria (List<String> procedureCodeList, Boolean srOnly = false) {
        procedureCodeList.each{ procedureCode ->
            List conditions = [
                    new DICOMCondition('0008,1030', 'EQUALS', procedureCode).getCondition(),
                    new AECondition('CALLED', 'ROUTER_FWD').getCondition()
            ]
            if (srOnly) {
                conditions << new DICOMCondition('0008,0060', 'EQUALS', 'SR').getCondition()
            }
            matchingCriteria << ['conditions': conditions]
        }
    }

    List getMatchingCriteria () {
        return matchingCriteria
    }
}

class CardDICOMMatchingCriteria {
    List matchingCriteria = []

    CardDICOMMatchingCriteria (File procedureCodeFile, Boolean srOnly = false) {
        procedureCodeFile.eachLine { procedureCode ->
            List conditions = [
                    new DICOMCondition('0008,1030', 'EQUALS', procedureCode).getCondition(),
                    new AECondition('CALLED', 'ROUTER_FWD').getCondition()
                ]
            if (srOnly) {
                //conditions << new DICOMCondition('0008,0060', 'EQUALS', 'SR').getCondition()
                conditions << new DICOMCondition('0008,0016', 'EQUALS', '1.2.840.10008.5.1.4.1.1.88.33').getCondition()
            }
            matchingCriteria << ['conditions': conditions]
        }
    }

    CardDICOMMatchingCriteria (List<String> procedureCodeList, Boolean srOnly = false) {
        procedureCodeList.each{ procedureCode ->
            List conditions = [
                    new DICOMCondition('0008,1030', 'EQUALS', procedureCode).getCondition(),
                    new AECondition('CALLED', 'ROUTER_FWD').getCondition()
            ]
            if (srOnly) {
                //conditions << new DICOMCondition('0008,0060', 'EQUALS', 'SR').getCondition()
                conditions << new DICOMCondition('0008,0016', 'EQUALS', '1.2.840.10008.5.1.4.1.1.88.33').getCondition()
            }
            matchingCriteria << ['conditions': conditions]
        }
    }

    List getMatchingCriteria () {
        return matchingCriteria
    }
}
