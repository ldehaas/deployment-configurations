<?xml version="1.0"?>
<config>

    <var name="http.rest.port" value="8080" />
    <var name="Navigator-Host" value="${HostIP}"/>
    <var name="Navigator-HostProtocol" value="https" />
    <!-- remove this after configuration, var name="Navigator-GUIPort" value="2525" / -->

    <!-- var name="Navigator-APIPort" value="2525" / this is renamed to fit multi-tenancy naming convention-->   
    <var name="http-navigator-API.port.p0" value="2500" />          <!--default 2525-->
    <var name="http-navigator.port.p0" value="2600" />           <!--default 2526-->
    <var name="http-admin-tools.port.p0" value="2700" />        <!--default 2527-->

    <var name="Navigator-RootPath" value="/rest" />
    <var name="Navigator-APIBasePath.p0" value="/rialto" />

    <var name="Navigator-KeyspaceName.p0" value="${CassandraKeyspacePrefix}navigator" />

    <var name="Usermanagement-APIBasePath" value="/usermanagement" />
    <!-- variable Usermanagement-URL and Usermanagement-AuthenticatedURL are not used as they are referenced only once per instance -->
    <!-- var name="Usermanagement-URL" value="${Navigator-HostProtocol}://${Navigator-Host}:${http-navigator-API.port}${Navigator-RootPath}" / -->
    <!-- var name="Usermanagement-AuthenticatedURL" value="${Navigator-HostProtocol}://${Navigator-Host}:${http-navigator-API.port}${Navigator-RootPath}/api${Usermanagement-APIBasePath}" / -->

    <!-- variables for front-end (navigator.plugin.download and navigator.plugin.cda) -->
    <var name="DownloadPlugin-Name" value="External Viewer (Download)" />
    <var name="DownloadPlugin-Description" value="External platform-specific viewer or download to local disk" />
    <var name="DownloadPlugin-APIBasePath.p0" value="/plugins/download" />

    <var name="CDAPlugin-Name" value="CDA Viewer" />
    <var name="CDAPlugin-Description" value="HL7 CDA (Clinical Document Architecture) Viewer" />
    <var name="CDAPlugin-APIBasePath.p0" value="/plugins/cda" />
    <!-- http servers for frontend (rialto.ui) -->
    <server type="http" id="http-navigator.p0">
        <port>${http-navigator.port.p0}</port>
    </server>

    <server type="http" id="http-admin-tools.p0">
        <port>${http-admin-tools.port.p0}</port>
    </server>


    <!-- Backend -->
    <service id="navigator-server.p0" type="navigator.server">
        <device idref="pix" />
        <device idref="pdq" />
        <device idref="xdsrep.p4"/>
        <device idref="xdsreg"/>
        <device idref="workflow-engine" />
        <device idref="cassandra-dc1" />

        <config>
            <prop name="CrossAffinityDomainID" value="${System-AffinityDomain}"/>
            <prop name="CassandraConfiguration">
                <keyspaceName>${Navigator-KeyspaceName.p0}</keyspaceName>
                <keyspaceReplicationStrategy>NetworkTopologyStrategy</keyspaceReplicationStrategy>
                <keyspaceReplicationStrategyOptions>${CassandraReplicationOption}</keyspaceReplicationStrategyOptions>
                <keyspaceCreateEnabled>true</keyspaceCreateEnabled>
                <dynamicConsistencyLevelEnabled>false</dynamicConsistencyLevelEnabled>
                <schemaUpdateEnabled>false</schemaUpdateEnabled>
            </prop>
            <!-- Cluster to be removed
            <prop name="Cluster">
                <name>NavigatorServerClusterGroupId</name>
                <udpstack>
                    <bindAddress>${Navigator-Server-Cluster-Bind-IP}</bindAddress>
                    <multicastAddress>${Navigator-Server-Cluster-Multicast-IP}</multicastAddress>
                    <multicastPort>${Navigator-Server-Cluster-Multicast-PORT}</multicastPort>
                </udpstack>
            </prop>
            -->

            <!-- Rialto XDS Registry cannot search by extended metadata -->
            <prop name="ExtendedMetadataAttributes">
                <ExtendedMetadataAttribute displayName="ReferringPhysician" searchKey="ReferringPhysician"/>
                <ExtendedMetadataAttribute displayName="AccessionNumber" searchKey="AccessionNumber"/>
            </prop>
            <prop name="SystemDefaultPermissions">
                <permission name="rialto.navigator.patients.search.internal" value="true"/>
                <permission name="rialto.navigator.patients.search.external" value="false"/>
                <permission name="rialto.navigator.patients.admin" value="false"/>
                <permission name="rialto.navigator.study.metadata.view" value="true" />
                <permission name="rialto.navigator.study.move" value="true" />
                <permission name="rialto.navigator.study.changeavailability" value="false" />
                <permission name="rialto.navigator.study.physical.delete" value="false" />
                <permission name="rialto.navigator.study.qc.merge" value="false" />
                <permission name="rialto.navigator.study.qc.split" value="false" />
                <permission name="rialto.navigator.study.qc.edit" value="false" />
                <permission name="rialto.navigator.study.reconciliation" value="false"/>
                <permission name="rialto.navigator.study.version.restore" value="false" />
                <permission name="rialto.navigator.docs.metadata.view.unknown.internal" value="true"/>
                <permission name="rialto.navigator.docs.metadata.view.unknown.external" value="true"/>
                <permission name="rialto.navigator.docs.metadata.edit.unknown.internal" value="false"/>
                <permission name="rialto.navigator.docs.metadata.edit.unknown.external" value="false"/>
                <permission name="rialto.navigator.docs.move.unknown.internal" value="false" />
                <permission name="rialto.navigator.docs.view.unknown.internal.always" value="false"/>
                <permission name="rialto.navigator.docs.view.unknown.external.always" value="false"/>
                <permission name="rialto.navigator.docs.view.unknown.internal.onbreakglass" value="true"/>
                <permission name="rialto.navigator.docs.view.unknown.external.onbreakglass" value="true"/>
                <permission name="rialto.navigator.docs.upload.internal" value="false" />
                <permission name="rialto.navigator.docs.codes.edit" value="false" />
                <permission name="rialto.vip.access" value="false" />
            </prop>

            <prop name="DefaultConfidentialityCodeSchemeID" value="1.2.3.4.5.6.1" />
            <prop name="DefaultConfidentialityCodeSchemeName" value="Karos Health demo confidentialityCodes" />
            <prop name="MaximumPatientResultSetSize" value="${Navigator-MaxPatientSearchResults}"/>
            <prop name="MaximumDocumentResultSetSize" value="${Navigator-MaxDocumentSearchResults}"/>
            <prop name="AllowMetadataUpdates">${XDSRegistry-MetadataUpdateEnabled}</prop>
            <prop name="ConfidentialityCodeSchemaName">TEST</prop>
            <prop name="web-api-path" value="${Navigator-APIBasePath.p0}"/>
            <prop name="UseTargetDomainForPDQ">true</prop>

            <include location="admin-tools/cacheStorage.xml"/>
            <include location="admin-tools/xdsdocumentcodes.xml"/>

        </config>
    </service>



    <!-- Frontend -->

    <!-- PLUGIN: External Viewer (Download) -->
    <service id="navigator-plugin-download.p0" type="navigator.plugin.download">

        <device idref="pix" />
        <device idref="pdq" />
        <device idref="xdsrep.p4"/>
        <device idref="xdsreg"/>
        <device idref="cassandra-dc1" />

        <config>
            <prop name="CrossAffinityDomainID" value="${System-AffinityDomain}"/>
            <prop name="ForcedMimeTypeFileExtension">
                <prop mimeType="application/vnd.ms-excel" fileExtension="xls"/>
            </prop>
            <prop name="CassandraConfiguration">
                <keyspaceName>${Navigator-KeyspaceName.p0}</keyspaceName>
                <keyspaceReplicationStrategy>NetworkTopologyStrategy</keyspaceReplicationStrategy>
                <keyspaceReplicationStrategyOptions>${CassandraReplicationOption}</keyspaceReplicationStrategyOptions>
                <keyspaceCreateEnabled>true</keyspaceCreateEnabled>
                <dynamicConsistencyLevelEnabled>false</dynamicConsistencyLevelEnabled>
                <schemaUpdateEnabled>false</schemaUpdateEnabled>
            </prop>
            <prop name="web-api-path" value="${DownloadPlugin-APIBasePath.p0}" />
            <prop name="UserManagementURL" value="${Navigator-HostProtocol}://${Navigator-Host}:${http-navigator-API.port.p0}${Navigator-RootPath}/api${Usermanagement-APIBasePath}"/>

            <include location="admin-tools/cacheStorage.xml"/>
        </config>
    </service>

    <!-- PLUGIN: CDA Viewer   -->
    <service id="navigator-plugin-cda.p0" type="navigator.plugin.cda">
        <device idref="pix" />
        <device idref="pdq" />
        <device idref="xdsrep.p4"/>
        <device idref="xdsreg"/>
        <device idref="cassandra-dc1" />

        <config>
            <prop name="CrossAffinityDomainID" value="${System-AffinityDomain}"/>
            <prop name="CassandraConfiguration">
                <keyspaceName>${Navigator-KeyspaceName.p0}</keyspaceName>
                <keyspaceReplicationStrategy>NetworkTopologyStrategy</keyspaceReplicationStrategy>
                <keyspaceReplicationStrategyOptions>${CassandraReplicationOption}</keyspaceReplicationStrategyOptions>
                <keyspaceCreateEnabled>true</keyspaceCreateEnabled>
                <dynamicConsistencyLevelEnabled>false</dynamicConsistencyLevelEnabled>
                <schemaUpdateEnabled>false</schemaUpdateEnabled>
            </prop>
            <prop name="web-api-path" value="${CDAPlugin-APIBasePath.p0}"/>
            <prop name="DefaultStylesheet" value="${rialto.rootdir}/etc/services/admin-tools/plugins/cda/default.xsl"/>
            <prop name="UserManagementURL" value="${Navigator-HostProtocol}://${Navigator-Host}:${http-navigator-API.port.p0}${Navigator-RootPath}/api${Usermanagement-APIBasePath}"/>

            <include location="admin-tools/cacheStorage.xml"/>
        </config>
    </service>

    <service id="rialto-ui.p0" type="rialto.ui">
        <device idref="pix" />
        <device idref="pdq" />

        <server idref="http-navigator.p0" name="web-ui">
            <!-- Not configurable at this time -->
            <url>/</url>
        </server>

        <server idref="http-admin-tools.p0" name="admin-tools">
            <!-- Not configurable at this time -->
            <url>/v2/</url>
        </server>

        <server idref="http-authenticated" name="web-api">
            <url>/public/*</url>
        </server>

        <config>
            <prop name="ApiURL" value="${Navigator-HostProtocol}://${Navigator-Host}:${http-navigator-API.port.p0}${Navigator-RootPath}/api${Navigator-APIBasePath.p0}"/>
            <prop name="ArrURL" value="${Navigator-HostProtocol}://${Navigator-Host}:${ARR-GUIPort}"/>
            <prop name="UserManagementURL" value="${Navigator-HostProtocol}://${Navigator-Host}:${http-navigator-API.port.p0}${Navigator-RootPath}"/>
            <prop name="PublicURL" value="http://${Navigator-Host}:${http-authenticated.port}/public"/>
            <prop name="ElasticsearchRestApiURL" value="http://localhost:9200" />
            <prop name="ModalityWorklistURL" value="${Navigator-HostProtocol}://${Navigator-Host}:${http-navigator-API.port.p0}/rest/api/mwl" />
            <prop name="EMPIManagementURL" value="http://${HostIP}:${http.rest.port}/rialto/empi" />
            <prop name="GlobalInactivitySessionTimeout" value="${IdleUserSessionTimeout}" />
            <prop name="ExternalPDQConfigured" value="false" />
            <prop name="RialtoAsACache" value="false" />

            <prop name="DeviceManagerURL" value="${Navigator-HostProtocol}://${Navigator-Host}:${http-navigator-API.port.p0}/rest/api/devices" />
            <prop name="ContextManagerURL" value="${Navigator-HostProtocol}://${Navigator-Host}:${http-navigator-API.port.p0}/rest/api/contexts" />
            <!-- prop name="PriorRulesManagerURL" value="${Navigator-HostProtocol}://${Navigator-Host}:${http-navigator-API.port.p0}/rest/api/priorrules" / -->
            <prop name="ScriptManagerURL" value="${Navigator-HostProtocol}://${Navigator-Host}:${http-navigator-API.port.p0}/rest/api/scripts" />
            <prop name="EventViewerURL" value="${Navigator-HostProtocol}://${Navigator-Host}:${http-navigator-API.port.p0}/rest/api/events" />
            <prop name="RouteManagerURL" value="${Navigator-HostProtocol}://${Navigator-Host}:${http-navigator-API.port.p0}/rest/api/routingrules" />
            <!-- prop name="SpaceManagerURL" value="${Navigator-HostProtocol}://${Navigator-Host}:${http-navigator-API.port.p0}/rest/api/spaces" / -->
            <!--prop name="VIPConfidentialityCode" value="VIP" /-->

            <!-- StudyMetadataSizeLimit set to 10 times the default as Echo studies can be very large-->
            <prop name="StudyMetadataSizeLimit" value="104857600" />
            <prop name="ElasticsearchSchemaPrefix" value="${ElasticNamespace}-" />

            <prop name="RialtoApiEndpoints">
                <EndpointBlock name="P4-OHSRadiology">
                    <MintApiURL>http://${HostIP}:8080/vault_p4/mint/</MintApiURL>
                    <WadoURL>http://${HostIP}:8080/vault_p4/wado/</WadoURL>
                    <DicomQcToolsURL>http://${HostIP}:8080/vault_p4/qc/</DicomQcToolsURL>
                    <IlmURL>http://${HostIP}:8080/vault_p4/ilm/</IlmURL>
                    <StudyManagementURL>http://${HostIP}:8080/vault_p4/studymanagement/</StudyManagementURL>
                    <RialtoImageArchiveAETitle>VCIAP4</RialtoImageArchiveAETitle>
                </EndpointBlock>
                <EndpointBlock name="P5-AgfaRadiologyD">
                    <MintApiURL>http://${HostIP}:8080/vault_p5/mint/</MintApiURL>
                    <WadoURL>http://${HostIP}:8080/vault_p5/wado/</WadoURL>
                    <DicomQcToolsURL>http://${HostIP}:8080/vault_p5/qc/</DicomQcToolsURL>
                    <IlmURL>http://${HostIP}:8080/vault_p5/ilm/</IlmURL>
                    <StudyManagementURL>http://${HostIP}:8080/vault_p5/studymanagement/</StudyManagementURL>
                    <RialtoImageArchiveAETitle>VCIAP5</RialtoImageArchiveAETitle>
                </EndpointBlock>
                  <EndpointBlock name="P6-ImageUpload">
                    <MintApiURL>http://${HostIP}:8080/vault_p6/mint/</MintApiURL>
                    <WadoURL>http://${HostIP}:8080/vault_p6/wado/</WadoURL>
                    <DicomQcToolsURL>http://${HostIP}:8080/vault_p6/qc/</DicomQcToolsURL>
                    <IlmURL>http://${HostIP}:8080/vault_p6/ilm/</IlmURL>
                    <StudyManagementURL>http://${HostIP}:8080/vault_p6/studymanagement/</StudyManagementURL>
                    <RialtoImageArchiveAETitle>VCIAP6</RialtoImageArchiveAETitle>
                </EndpointBlock>
                <EndpointBlock name="P7-OHSNSSlidellD">
                    <MintApiURL>http://${HostIP}:8080/vault_p7/mint/</MintApiURL>
                    <WadoURL>http://${HostIP}:8080/vault_p7/wado/</WadoURL>
                    <DicomQcToolsURL>http://${HostIP}:8080/vault_p7/qc/</DicomQcToolsURL>
                    <IlmURL>http://${HostIP}:8080/vault_p7/ilm/</IlmURL>
                    <StudyManagementURL>http://${HostIP}:8080/vault_p7/studymanagement/</StudyManagementURL>
                    <RialtoImageArchiveAETitle>VCIAP7</RialtoImageArchiveAETitle>
                </EndpointBlock>
                <EndpointBlock name="P8-OHSCardiology">
                    <MintApiURL>http://${HostIP}:8080/vault_p8/mint/</MintApiURL>
                    <WadoURL>http://${HostIP}:8080/vault_p8/wado/</WadoURL>
                    <DicomQcToolsURL>http://${HostIP}:8080/vault_p8/qc/</DicomQcToolsURL>
                    <IlmURL>http://${HostIP}:8080/vault_p8/ilm/</IlmURL>
                    <StudyManagementURL>http://${HostIP}:8080/vault_p8/studymanagement/</StudyManagementURL>
                    <RialtoImageArchiveAETitle>VCIAP8</RialtoImageArchiveAETitle>
                </EndpointBlock>
                <EndpointBlock name="P9-AgfaCardEchoD">
                    <MintApiURL>http://${HostIP}:8080/vault_p9/mint/</MintApiURL>
                    <WadoURL>http://${HostIP}:8080/vault_p9/wado/</WadoURL>
                    <DicomQcToolsURL>http://${HostIP}:8080/vault_p9/qc/</DicomQcToolsURL>
                    <IlmURL>http://${HostIP}:8080/vault_p9/ilm/</IlmURL>
                    <StudyManagementURL>http://${HostIP}:8080/vault_p9/studymanagement/</StudyManagementURL>
                    <RialtoImageArchiveAETitle>VCIAP9</RialtoImageArchiveAETitle>
                </EndpointBlock>
                     <EndpointBlock name="P10-AgfaCardCathD">
                    <MintApiURL>http://${HostIP}:8080/vault_p10/mint/</MintApiURL>
                    <WadoURL>http://${HostIP}:8080/vault_p10/wado/</WadoURL>
                    <DicomQcToolsURL>http://${HostIP}:8080/vault_p10/qc/</DicomQcToolsURL>
                    <IlmURL>http://${HostIP}:8080/vault_p10/ilm/</IlmURL>
                    <StudyManagementURL>http://${HostIP}:8080/vault_p10/studymanagement/</StudyManagementURL>
                    <RialtoImageArchiveAETitle>VCIAP10</RialtoImageArchiveAETitle>
                </EndpointBlock>
                <EndpointBlock name="P11-MHMRadiologyD">
                    <MintApiURL>http://${HostIP}:8080/vault_p11/mint/</MintApiURL>
                    <WadoURL>http://${HostIP}:8080/vault_p11/wado/</WadoURL>
                    <DicomQcToolsURL>http://${HostIP}:8080/vault_p11/qc/</DicomQcToolsURL>
                    <IlmURL>http://${HostIP}:8080/vault_p11/ilm/</IlmURL>
                    <StudyManagementURL>http://${HostIP}:8080/vault_p11/studymanagement/</StudyManagementURL>
                    <RialtoImageArchiveAETitle>VCIAP11</RialtoImageArchiveAETitle>
                </EndpointBlock>
                <EndpointBlock name="P12-OHSOpthalmology">
                    <MintApiURL>http://${HostIP}:8080/vault_p12/mint/</MintApiURL>
                    <WadoURL>http://${HostIP}:8080/vault_p12/wado/</WadoURL>
                    <DicomQcToolsURL>http://${HostIP}:8080/vault_p12/qc/</DicomQcToolsURL>
                    <IlmURL>http://${HostIP}:8080/vault_p12/ilm/</IlmURL>
                    <StudyManagementURL>http://${HostIP}:8080/vault_p12/studymanagement/</StudyManagementURL>
                    <RialtoImageArchiveAETitle>VCIAP12</RialtoImageArchiveAETitle>
                </EndpointBlock>
                <EndpointBlock name="P13-OHSOpthalmologyD">
                    <MintApiURL>http://${HostIP}:8080/vault_p13/mint/</MintApiURL>
                    <WadoURL>http://${HostIP}:8080/vault_p13/wado/</WadoURL>
                    <DicomQcToolsURL>http://${HostIP}:8080/vault_p13/qc/</DicomQcToolsURL>
                    <IlmURL>http://${HostIP}:8080/vault_p13/ilm/</IlmURL>
                    <StudyManagementURL>http://${HostIP}:8080/vault_p13/studymanagement/</StudyManagementURL>
                    <RialtoImageArchiveAETitle>VCIAP13</RialtoImageArchiveAETitle>
                </EndpointBlock>
                <EndpointBlock name="P14-OHSPedsEcho">
                    <MintApiURL>http://${HostIP}:8080/vault_p14/mint/</MintApiURL>
                    <WadoURL>http://${HostIP}:8080/vault_p14/wado/</WadoURL>
                    <DicomQcToolsURL>http://${HostIP}:8080/vault_p14/qc/</DicomQcToolsURL>
                    <IlmURL>http://${HostIP}:8080/vault_p14/ilm/</IlmURL>
                    <StudyManagementURL>http://${HostIP}:8080/vault_p14/studymanagement/</StudyManagementURL>
                    <RialtoImageArchiveAETitle>VCIAP14</RialtoImageArchiveAETitle>
                </EndpointBlock>
                <EndpointBlock name="P15-OHSPedsEchoD">
                    <MintApiURL>http://${HostIP}:8080/vault_p15/mint/</MintApiURL>
                    <WadoURL>http://${HostIP}:8080/vault_p15/wado/</WadoURL>
                    <DicomQcToolsURL>http://${HostIP}:8080/vault_p15/qc/</DicomQcToolsURL>
                    <IlmURL>http://${HostIP}:8080/vault_p15/ilm/</IlmURL>
                    <StudyManagementURL>http://${HostIP}:8080/vault_p15/studymanagement/</StudyManagementURL>
                    <RialtoImageArchiveAETitle>VCIAP15</RialtoImageArchiveAETitle>
                </EndpointBlock>
                <EndpointBlock name="P16-OHSOBGYN">
                    <MintApiURL>http://${HostIP}:8080/vault_p16/mint/</MintApiURL>
                    <WadoURL>http://${HostIP}:8080/vault_p16/wado/</WadoURL>
                    <DicomQcToolsURL>http://${HostIP}:8080/vault_p16/qc/</DicomQcToolsURL>
                    <IlmURL>http://${HostIP}:8080/vault_p16/ilm/</IlmURL>
                    <StudyManagementURL>http://${HostIP}:8080/vault_p16/studymanagement/</StudyManagementURL>
                    <RialtoImageArchiveAETitle>VCIAP16</RialtoImageArchiveAETitle>
                </EndpointBlock>
                <EndpointBlock name="P17-OHSOBGYND">
                    <MintApiURL>http://${HostIP}:8080/vault_p17/mint/</MintApiURL>
                    <WadoURL>http://${HostIP}:8080/vault_p17/wado/</WadoURL>
                    <DicomQcToolsURL>http://${HostIP}:8080/vault_p17/qc/</DicomQcToolsURL>
                    <IlmURL>http://${HostIP}:8080/vault_p17/ilm/</IlmURL>
                    <StudyManagementURL>http://${HostIP}:8080/vault_p17/studymanagement/</StudyManagementURL>
                    <RialtoImageArchiveAETitle>VCIAP17</RialtoImageArchiveAETitle>
                </EndpointBlock>
                <EndpointBlock name="P18-OHSPOCUS">
                    <MintApiURL>http://${HostIP}:8080/vault_p18/mint/</MintApiURL>
                    <WadoURL>http://${HostIP}:8080/vault_p18/wado/</WadoURL>
                    <DicomQcToolsURL>http://${HostIP}:8080/vault_p18/qc/</DicomQcToolsURL>
                    <IlmURL>http://${HostIP}:8080/vault_p18/ilm/</IlmURL>
                    <StudyManagementURL>http://${HostIP}:8080/vault_p18/studymanagement/</StudyManagementURL>
                    <RialtoImageArchiveAETitle>VCIAP18</RialtoImageArchiveAETitle>
                </EndpointBlock>
                <EndpointBlock name="P19-OHSPOCUSD">
                    <MintApiURL>http://${HostIP}:8080/vault_p19/mint/</MintApiURL>
                    <WadoURL>http://${HostIP}:8080/vault_p19/wado/</WadoURL>
                    <DicomQcToolsURL>http://${HostIP}:8080/vault_p19/qc/</DicomQcToolsURL>
                    <IlmURL>http://${HostIP}:8080/vault_p19/ilm/</IlmURL>
                    <StudyManagementURL>http://${HostIP}:8080/vault_p19/studymanagement/</StudyManagementURL>
                    <RialtoImageArchiveAETitle>VCIAP19</RialtoImageArchiveAETitle>
                </EndpointBlock>
        </prop>

            <prop name="SupportedThemes">
                rialto-light
                rialto-dark
                vitreaConnection
            </prop>
            <prop name="DefaultTheme" value="${Navigator-DefaultTheme}"/>

            <prop name="SupportedLocales">
                en
                fr
            </prop>
            <prop name="DefaultLocale" value="${Navigator-DefaultLocale}"/>

            <prop name="BreakTheGlassOptions">
                <BreakTheGlassOption>Emergency access required for patient care.</BreakTheGlassOption>
            </prop>

            <include location="admin-tools/patientidentitydomains.xml"/>

            <prop name="Plugins">
                <plugin>
                    <name>${CDAPlugin-Name}</name>
                    <description>${CDAPlugin-Description}</description>
                    <mimeTypes>text/xml</mimeTypes>
                    <url>${Navigator-HostProtocol}://${Navigator-Host}:${http-navigator-API.port.p0}${Navigator-RootPath}/api/plugins/cda</url>
                    <embedded>true</embedded>
                </plugin>

                <plugin>
                    <name>${DownloadPlugin-Name}</name>
                    <description>${DownloadPlugin-Description}</description>
                    <mimeTypes>*</mimeTypes>
                    <url>${Navigator-HostProtocol}://${Navigator-Host}:${http-navigator-API.port.p0}${Navigator-RootPath}/api/plugins/download</url>
                    <embedded>true</embedded>
                </plugin>
            </prop>

            <prop name="EnabledConfigurationFeatures">
                devices
                scripts
                routes
                mwlQueryRules
            </prop>


            <!-- Configure server name to display in UI -->
            <prop name="ServerName" value="${ServerTag}" />


            <!-- Configure image caching for studies in the UI       -->
            <!-- The value can be one of:                            -->
            <!-- "true": enable the cache with default size (32 MBs) -->
            <!-- "false": disabled caching of images                 -->
            <!-- "number": The store size in megabytes               -->
            <!--
                <prop name="ImageCache">
                    <size>64</size>
                </prop>
            -->

            <!-- Configure eventCodeSchemes to support searching for documents by eventCode -->
            <prop name="EventCodeSchemes">
                <!--scheme schemeID="1.2.3" schemeName="Karos Health demo eventCodes" /-->
                <scheme schemeID="2.16.840.1.113883.6.5" schemeName="SRT" />
                <scheme schemeID="deviceCodes" schemeName="deviceCodes" />
                <scheme schemeID="manufacturerName" schemeName="manufacturerName" />
            </prop>

            <prop name="ProductBranding">
                <productName>Vitrea Connection</productName>
                <companyName>Vital Images</companyName>
                <copyrightDates>2018</copyrightDates>
                <contactAddress>5850 Opus Parkway, Suite 300, Minnetonka, MN 55343, USA</contactAddress>
                <contactPhoneNumber>866.433.4624</contactPhoneNumber>
                <contactEmail>support@vitalimages.com</contactEmail>
                <contactWebsite>www.vitalimages.com</contactWebsite>
                <supportPhoneNumber>800.208.3005</supportPhoneNumber>
                <supportWebsite>www.vitalimages.com/about-us/contact-us</supportWebsite>
            </prop>

        </config>
    </service>
</config>
