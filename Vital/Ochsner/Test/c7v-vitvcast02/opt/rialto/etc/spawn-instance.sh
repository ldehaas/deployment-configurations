#!/bin/bash
#
###############################################################################################
# Use this script along with the following input files to spawn rialto instance configurations:
# This script uses template file template-image-archive.xml to configure xds-repo and image-archive 
# Do not make manual changes to the files generated.
##############################################################################################
## UPDATE HISTORY
##      2018.11.08      Yi      Revamped script for Rialto 7.4
##      2019.01.04      Yi      Dynamically determine source AET for connect-agfa.xml
##      2019.02.14      Yi      Removed connect-agfa.xml, added OID mapping for space-manager.xml, routing-rule-manager.xml,
##                              patientidentitydomains.xml and variables/common.xml; refactored domain UID replacement
##      2019.05.17      Yi      Removed oru2sr services for all pools.
##      2019.05.23      Yi      Removed some sections to simplify
##      2019.05.24      Yi      Further simplification towards automatability
##      2019.06.13      Yi      Simplification to script so it becomes fully environment agnostic
##############################################################################################

################
#dep_mode="T"
################
# Instruction for promoting configuration from Test Environment to Production/Migration
# 1. Keep the local version of rialto.lic, ./variables/cassandra.xml and ./variables/elastic.xml
# 2. Run this script with correct deployment mode! P=Production, M=Migration and T=Test
# 3. Confirm the followings:
#    a - ./devices/common.xml and ./services/health-check.xml reflect the correct environment
#    b - context-manager.xml, authentication.xml,routing-rules-manage.xml and space-manager.xml have correct root OID reference
#        Prod OID = 2.16.124.113638.7.6.1.1              Test OID = 2.25.65066631204490692481020178991382140296
#    c - ./variables/common.xml and ./services/common/common_ohs.groovy have correct Domain UUIDs
#        Prod UUID = 2.16.124.113638.7.6.1.51xx          Test UUID = 2.16.124.113638.7.6.0.51xx
#    d - (Best Practise) Run grep -r with OID and UUID to ensure that they are correct everywhere else in the configuration in
#        case this script is not up to date.
# 4. For migration ONLY, manually turn off modality-worklist, health-check, connect services and empi


#case "$1" in
#    T)
#    dep_mode="Test"
#    ;;
#    M)
#    dep_mode="Migration"
#    ;;
#    P)
#    dep_mode="Production"
#    ;;
#    *)
#    echo 'Usage: spawn-instance.sh {P|M|T}'
#    echo 'P for production'
#    echo 'M for migration'
#    echo 'T for test'

#    exit -1
#esac

read -p "This is going to overwrite image-archive-x.xml files. Press Y to continue. " -n 1 -r
if [[ ! $REPLY =~ ^[Yy]$ ]]
then
    echo "You chose to quit."    
    [[ "$0" = "$BASH_SOURCE" ]] && exit 1 || return 1 # Exitting
fi



if [[ ! -f template-image-archive.xml ]] ; then
    echo "Template file template-image-archive.xml does not exist! Exiting..."
    exit
fi

echo "*** Automatic configuration for Ochsner Health ***"

#Domain_UID_Root_Prod="2.16.124.113638.7.6.1."
#Domain_UID_Root_Test="2.16.124.113638.7.6.0."

#declare -A Domain_UID


#if [ $dep_mode == "Migration" -o $dep_mode == "Production" ]; then
#    Domain_UID[UNKNOWN]=${Domain_UID_Root_Prod}"5100"
#    Domain_UID[OHSEPIC]=${Domain_UID_Root_Prod}"5104"
#    Domain_UID[DDOHSR]=${Domain_UID_Root_Prod}"5105"
#    Domain_UID[DDSTPH]=${Domain_UID_Root_Prod}"5106"
#    Domain_UID[DDNSSLIDELL]=${Domain_UID_Root_Prod}"5107"
#    Domain_UID[DDOHSCN]=${Domain_UID_Root_Prod}"5109"
#    Domain_UID[DDOHSCI]=${Domain_UID_Root_Prod}"5110"
#    Domain_UID[MHM]=${Domain_UID_Root_Prod}"5111"
#    Domain_UID[DDOHSOP]=${Domain_UID_Root_Prod}"5113"
#    Domain_UID[DDOHSCPN]=${Domain_UID_Root_Prod}"5115"
#    Domain_UID[DDOHSOB]=${Domain_UID_Root_Prod}"5117"
#    Domain_UID[DDOHSPOCUS]=${Domain_UID_Root_Prod}"5119"

#elif [ $dep_mode == "Test" ] 
#then
#    Domain_UID[UNKNOWN]=${Domain_UID_Root_Test}"5100"
#    Domain_UID[OHSEPIC]=${Domain_UID_Root_Test}"5104"
#    Domain_UID[DDOHSR]=${Domain_UID_Root_Test}"5105"
#    Domain_UID[DDSTPH]=${Domain_UID_Root_Test}"5106"
#    Domain_UID[DDNSSLIDELL]=${Domain_UID_Root_Test}"5107"
#    Domain_UID[DDOHSCN]=${Domain_UID_Root_Test}"5109"
#    Domain_UID[DDOHSCI]=${Domain_UID_Root_Test}"5110"
#    Domain_UID[MHM]=${Domain_UID_Root_Test}"5111"
#    Domain_UID[DDOHSOP]=${Domain_UID_Root_Test}"5113"
#    Domain_UID[DDOHSCPN]=${Domain_UID_Root_Test}"5115"
#    Domain_UID[DDOHSOB]=${Domain_UID_Root_Test}"5117"
#    Domain_UID[DDOHSPOCUS]=${Domain_UID_Root_Test}"5119"

#else
#    echo "Incorrect deployment mode. Set deployment mode to M, P or T"
#    exit
#fi

## Add new domain here ONLY for DIRTY pool

for iaNum in {1..16}; do
    chmod +w image-archive-${iaNum}.xml

    ## Pool ID starts with 4
    let poolid=3+${iaNum}
    let dicomport=4103+${iaNum}
    let hl7iaport=5103+${iaNum}
    let hl7rrport=5203+${iaNum}
    let xdsrepoport=9103+${iaNum}
    let httpauthenticatedport=2400
    let httpnavigatorport=2603+${iaNum}
    let httpadmintoolsport=2703+${iaNum}
    let httpnavapiport=2503+${iaNum}

    case "$iaNum" in
    1 ) poolname="OHS Radiology-Clean"
        ipid="OHSEPIC";;
    2 ) poolname="Agfa Radiology-Dirty"
        ipid="DDOHSR";;
    3 ) poolname="STPH Radiology-Dirty"
        ipid="DDSTPH";;
    4 ) poolname="OHS-NSSlidell-Dirty"
        ipid="DDNSSLIDELL";;
    5 ) poolname="OHS Cardiology-Clean"
        ipid="OHSEPIC";;
    6 ) poolname="Agfa Card Echo-Dirty"
        ipid="DDOHSCN";;
    7 ) poolname="Agfa Card Cath-Dirty"
        ipid="DDOHSCI";;
    8 ) poolname="MHM Radiology-Dirty"
        ipid="MHM";;
    9 ) poolname="OHS Ophthalmology-Clean"
        ipid="OHSEPIC";;
    10 ) poolname="OHS Ophthalmology-Dirty"
        ipid="DDOHSOP";;
    11 ) poolname="OHS Peds Echo-Clean"
        ipid="OHSEPIC";;
    12 ) poolname="OHS Peds Echo-Dirty"
        ipid="DDOHSCPN";;
    13 ) poolname="OHS OBGYN-Clean"
        ipid="OHSEPIC";;
    14 ) poolname="OHS OBGYN-Dirty"
        ipid="DDOHSOB";;
    15 ) poolname="OHS POCUS-Clean"
        ipid="OHSEPIC";;
    16 ) poolname="OHS POCUS-Dirty"
        ipid="DDOHSPOCUS";;
    * ) poolname="Unknown"
        ipid="UNKNOWN";;
    ## Add new poolname here when configuring new pools.
    esac
#    poolname=${dep_mode}"-"${poolname}
    poolname=${poolname}

#    if [ ! ${Domain_UID[${ipid}]+_} ]; then
#        echo "..... Cannot recognize Poolname "${poolname}". Please correct configuration script. Exiting..."
#        exit
#    fi

    ## Construct fully qualified ipid based on predefined mapping
 #   fq_ipid=${ipid}"\&amp;"${Domain_UID[${ipid}]}"\&amp;ISO"
 #   echo "->INSTANCE:"$poolname"|"${ipid}"|"${Domain_UID[${ipid}]}
    echo "-> INSTANCE:"$poolname"|"${ipid}

    if [ $ipid == "OHSEPIC"  ]; then
        #echo "..Applying configurations for clean pool."
        ## Customizations for clean pool
        ## 1. Enable MWL Reconciliation
        ## 2. Use customized inbound morpher c_in.groovy
        sed -e "s/iaX/p${poolid}/g" \
            -e "s/AET_IAX/VCIAP${poolid}/g" \
            -e "s/dicomportX/${dicomport}/g" \
            -e "s/hl7iaportX/${hl7iaport}/g" \
            -e "s/xdsrepoportX/${xdsrepoport}/g" \
            -e "s/IAHL7RECAPPX/VCIA${poolid}/g" \
            -e "s/IAHL7RECFACX/DC/g" \
            -e "s/<prop name=\"MWLReconciliationEnabled\">false<\/prop>/<prop name=\"MWLReconciliationEnabled\">true<\/prop>/g" \
            -e "s/\/etc\/services\/image-archive\/scripts\/in\.groovy/\/etc\/services\/image-archive\/scripts\/c_in\.groovy/g" \
            -e "s/<prop name=\"PublisherType\" value=\"DISCARD\" \/>/<prop name=\"PublisherType\" value=\"BASIC\" \/>/g" \
            template-image-archive.xml > image-archive-${iaNum}.xml    
    else
        #echo "..Applying configuration for dirty pool."
        ## Customizations for dirty pool
        ## 1. Use customized inbound morpher d_in.groovy
        sed -e "s/iaX/p${poolid}/g" \
            -e "s/AET_IAX/VCIAP${poolid}/g" \
            -e "s/dicomportX/${dicomport}/g" \
            -e "s/hl7iaportX/${hl7iaport}/g" \
            -e "s/xdsrepoportX/${xdsrepoport}/g" \
            -e "s/IAHL7RECAPPX/VCIA${poolid}/g" \
            -e "s/IAHL7RECFACX/DC/g" \
            -e "s/\/etc\/services\/image-archive\/scripts\/in\.groovy/\/etc\/services\/image-archive\/scripts\/d_in\.groovy/g" \
            template-image-archive.xml > image-archive-${iaNum}.xml
    fi

    ## Customizations for Migration App - these customizations override previous ones for clean pool
    ## 1. Disable MWL reconciliation for all pools
    ## 2. Set production path to readonly and migration path to read-write

# Migration Servers are not target of this script any more. Remember to apply the following scripts manually to migration servers
# if they ever need to be update
#    if [ $dep_mode == "Migration" ]; then
#        # if this is a migration deployment, then we still turn off MWL reconciliation even for clean pools
#        sed -i -e "s/<prop name=\"MWLReconciliationEnabled\">true<\/prop>/<prop name=\"MWLReconciliationEnabled\">false<\/prop>/g" \
#               -e "s/<!-- Production Storage Locations starting flag -->/<!-- Production Storage Locations starting flag ###/g" \
#               -e "s/<!-- Production Storage Locations ending flag -->/#### Production Storage Locations ending flag -->/g" \
#               -e "s/<!-- Migration Storage Locations starting flag ###/<!-- Migration Storage Locations starting flag -->/g" \
#               -e "s/#### Migration Storage Locations ending flag -->/<!-- Migration Storage Locations ending flag -->/g" \
#            image-archive-${iaNum}.xml
#    fi

    chmod -w image-archive-${iaNum}.xml
done

echo "Configuration files image-archive-x.xml generated."

