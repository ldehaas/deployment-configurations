
<!-- The following three FileSystem props are a mitigation, re: khc-14343-->
               <FileSystemStorageLocation>
                   <AbsoluteBasePath>${rialto.rootdir}${IA-WorkDir.p8}/archive</AbsoluteBasePath>
                   <ReadOnly>true</ReadOnly>
                   <SourceDataCenter>DC1</SourceDataCenter>
                </FileSystemStorageLocation>

                <FileSystemStorageLocation>
                   <AbsoluteBasePath>${rialto.rootdir}${IA-WorkDir.p4}/migration</AbsoluteBasePath>
                   <ReadOnly>true</ReadOnly>
                   <SourceDataCenter>DC1</SourceDataCenter>
                </FileSystemStorageLocation>

                <FileSystemStorageLocation>
                   <AbsoluteBasePath>${rialto.rootdir}${IA-WorkDir.p8}/migration</AbsoluteBasePath>
                   <ReadOnly>true</ReadOnly>
                   <SourceDataCenter>DC1</SourceDataCenter>
                </FileSystemStorageLocation>
<!-- End of mitigation, re: khc-14343-->

