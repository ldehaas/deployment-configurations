; External user name. Default value is external @ domain . com.
; WARNING: Avoid using this configuration as it is global.
;          We need configuration per OU. It is likely this configuration
;          is going to be moved to groups.conf in the near future.
; can-authorize-foreign-users="external@example.com"


; If this variable is set to the canonical name (that is an username
; with account and domain, fx, external@domain.com) of external user,
; then session authenticated as that external user will be run
; as the authzuser instead. Authz user must be defined in the user
; backend (DB or AD).
;
; Comma separated list of EV external users.
;
; SECURITY WARNING: enable this feature only if the 3rd party system
;                   that uses a particular EV external user for authentication
;                   can be trusted. Use separate EV external users
;                   per integration. See FD169 and EV-2146.R5
;
; can-authorize-known-users="external1@example.com,external2@example.com"


; Public hostname (not IP address) of vdbserver host. This is going to be
; used to get kerberos tickets. If not setup, vdbserver will use machine's
; hostname for that. In HA setups, normally machine's hostname is setup
; to its private name. Please make sure this name can be resolved to a FQDN.
;
; hostname="evbe1.example.com"

; Local domain configuration - with Active Directory.
;   When BE shall query AD for information for a domain (for instance "customer.com")
;   it will try to obtain a kerberos ticket for that domain using its stored keys
;   in keytab (this is to support for single AD setup). If that fails it will also
;   try to obtain a kerberos ticket for EV domain expecting that there is a EV KDC
;   and having a kerberos ticket from EV's KDC to obtain a ticket for user's domain.
;   (This is to support the setup for multi AD). By default EV will expect EV's KDC
;   to be created for the domain of BE's server. If EV's KDC has been setup for
;   another domain that can be setup with local-domain keyword.
;
; Domain of EV's KDC server. By default that is the domain name of BE's server.
; local-domain="ev.customer.com"

; Configuration of preferred LDAP server (supported since EV 4.0.1)
; - Ldap servers will be obtained with DNS query like "_ldap._tcp.EXAMPLE.COM" for SRV records.
; - Servers for a domain can be reconfigured in the ldap-servers section - one line per AD domain.
; - An ldap server can be removed from the query list by appending ':disabled' to the server name.
; - All ldap servers returned by dns can be removed by adding "all-dns-results:disabled" to the list.
; - Extra ldap servers can be provided and the query order controlled by listing the AD servers. They
;   will be queried in the listed order.

;
; NOTE: domain names and hostnames must be in LOWER CASE with no whitespace around.
;
[ldap-servers]
;example.com="dc1.example.com,dc2.example.com,dc3.example.com:disabled"
;foobar.com="dc1.foobar.com,dc2.foobar.com,all-dns-results:disabled"
