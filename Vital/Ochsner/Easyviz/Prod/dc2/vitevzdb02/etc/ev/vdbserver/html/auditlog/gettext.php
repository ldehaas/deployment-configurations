<?php
require('env.php');

// If LANG or locale is "C" gettext will not translate anything
setlocale(LC_ALL, "en_US"); // Not "C"
$lang = getenv("LANG"); // get the LANG set in e.g. /etc/ev/auditlogweb.env
if (!$lang || $lang != "C") {
# The LANGUAGE variable takes precedence over the locale set with setlocale (if not set to 'C')
# It also allows us to search a list of locales, which we want as the site translations are
# in the english locale
    putenv("LANGUAGE=" . $lang . ":en_US");
} else {
    putenv("LANGUAGE=en_US");
}
putenv("LANG=en_US"); // Just different from "C"
// // Specify location of translation tables
bindtextdomain("auditlogweb", "./gettext");
$sitetransdir = getenv("SITE_TRANSLATIONSDIR");
if ($sitetransdir == "") {
    $sitetransdir = "/usr/share/ev/translations/gettext";
}
bindtextdomain("site", $sitetransdir);
// Choose domain
textdomain("auditlogweb");
?>
