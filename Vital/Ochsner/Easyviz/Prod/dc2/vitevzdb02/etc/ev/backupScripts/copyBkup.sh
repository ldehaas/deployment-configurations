#!/bin/bash

#  this script will take the current DB2 backup file, or files if more than one,
#   from the default backup location and then copy these files to the other backend
#  it will connect to the other system as the userid in the backupDestID value
#  public keys need to be setup to allow non-password connection.

# if needed you can update these variables
backupSourceDir="/home/backup/EVVDB"
backupDestHost="vitevzdb01.ochsner.org"
backupDestDirPrefix="/home/backup/EVVDB/"
backupDestDirSuffix="/dbbackup/"
backupDestID="root"
dns_VIP_Name="eidiagnosticview"

# do not change these variables
colon=":"
atSign="@"
hostName=`hostname`

# 
# Function to determine if this host is the current active system.
# it uses the nslookup command against the name of eidiagnosticview to determine which system
# is current assigned to this name, the one that is assigned is the active system
# function will return 0 if the passive system and 1 if the active
#
function is_ActiveSystem () {
    local result=0
    local activeHost=$( nslookup $dns_VIP_Name | grep 'Name' | cut -d ":" -f 2 | sed -e 's/^[[:space:]]*//' )
    if [ "$activeHost" == "$hostName" ]
    then
      result=1
    fi
    echo $result

}


#
# Function to determine the destination directory where the backup file
#  will be copied to
#
function getBackupDestLocation {

   local backupDest="$backupDestHost$colon$backupDestDirPrefix$hostName$backupDestDirSuffix"
   echo $backupDest
}

#
# Function to figure out the file name of the backup to copy
#
function getSourceName {

   local filename=$(find $backupSourceDir -maxdepth 1 -type f -mtime 0 )
   echo $filename
}



#
# wrapper function for the file copy
#
function sendFile {

	#  local result=$(scp $1 $backupDestID$atSign$2$2)
	`scp $1 $backupDestID$atSign$2`
	if [ $? -eq 0 ];
	then
	    echo "OK"
	else
	    echo "NOK"
	fi

}

# 
# This function will set the permissions on the backup files copied to the other system. If the other system needs to use these for a 
# restore then the files need to be owned by user db2inst1, hence we set these file to the right owner
# 
function setFileOwner {
 tmpFile=$(echo $1 | awk -F "/" '{print $5}' )
 `ssh $backupDestID$atSign$backupDestHost chown -R  db2inst1:db2inst1 $backupDestDirPrefix$hostName$backupDestDirSuffix$tmpFile`

}

#
#  mainline code --------
#

if [ "$(is_ActiveSystem)" !=  1 ]
then
  echo "This system ($hostName) is currently not the active Easyviz Backend system, backup file not copied -- $(date +"%Y%m%d%H%M%S")"
  exit
fi 

backupTo=$(getBackupDestLocation) 

backupFile=$(getSourceName)

echo "DB2 backup file(s) $backupFile  will be copied to $backupTo -- start time $(date +"%Y%m%d%H%M%S")"

# there is a chance that more than 1 db2 backup is done in a day so make sure
#  all files are copied to the backup location
for file in $backupFile
do
  
   sendRC=$(sendFile $file $backupTo)

   if [[ $sendRC == OK ]]; then
      setFileOwner $file
      echo "DB2 backup file $file  successfully copied -- end time $(date +"%Y%m%d%H%M%S") "
   else
      echo "DB2 backup file $file did NOT copy -- end time $(date +"%Y%m%d%H%M%S") "
   fi

done

echo " "
