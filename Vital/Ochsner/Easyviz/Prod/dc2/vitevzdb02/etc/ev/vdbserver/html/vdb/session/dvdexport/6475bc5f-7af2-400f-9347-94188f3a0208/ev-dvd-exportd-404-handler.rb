#!/usr/bin/env ruby
puts 'Content-Type: text/plain; charset=utf-8'
puts 'Status: 404 Not Found'
puts ''
if ENV['REQUEST_URI'] =~ /^\/vdb\/session\/dvdexport\/[[:xdigit:]]{8}-[[:xdigit:]]{4}-[[:xdigit:]]{4}-[[:xdigit:]]{4}-[[:xdigit:]]{12}\/status$/ && File.exist?('status.error') then
puts File.read('status.error')
else
puts '404 - The requested resource could not be found'
end
