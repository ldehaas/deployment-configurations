<?php
$EVENT_TOOLTIP = _("<b>Event:</b><br/>An audit log record consists of one event and zero or more participants.");

$ACTIVE_PARTICIPANTS_TOOLTIP = _("<b>Active Participants:</b><br/>An auditlog record consists of one event and zero or more participants. Active Participants are participants that have been actively involved in the process that lead to the event occurring. Examples of Active Participants are users and software systems. Patients and studies, on the other hand, are examples of (passive) Participant Objects.");

$PARTICIPANT_OBJECTS_TOOLTIP = _("<b>Participant Objects:</b><br/>An auditlog record consists of one event and zero or more participants. Participant Objects are passive participants that have not been directly involved in the process that lead to the event ocurring. Patients and studies are examples of (passive) Participant Objects. Users and software components, on the other hand, are examples of Active Participants.");

$AUDIT_EVENT_ID_TOOLTIP = _("<b>Event ID</b><br/>. Specifies the kind of event.");

$AUDIT_EVENT_TYPE_CODE_TOOLTIP = _("<b>Event Type</b><br/>The Event Type specifies a subtype of the event, ie. an event with ID <b>User Authentication</b> may have type <b>Login</b> or <b>Logout</b>.");

$ACTIVE_PARTICIPANT_ROLE_ID_CODE_TOOLTIP = _("<b>Role:</b><br/>Specifies the Active Participants Role in the event.");

$AUDIT_ACTION_CODE_TOOLTIP = _("<b>Action:</b><br/>Action is commonly used to specify whether the event was generated as a result of creating, reading, updating or deleting records. Events that are not generated as a result of manipulating records usually specify the action <b>Execute</b>.");

$AUDIT_EVENT_OUTCOME_TOOLTIP = _("<b>Result:</b><br/>The result code specifies whether the event was generated for a successful operation or whether an error occurred.");

$AP_USER_ID_TOOLTIP = _("<b>ID:</b><br/>The contents of this field depends on the participants role. If the role is a user (<b>Application Launcher</b>) the <b>ID</b> is the users user-id. If the participant is a process or a media the <b>ID</b> will be a process or media ID, respectively.");

$AP_ALTERNATIVE_ID_TOOLTIP = _("<b>Alternate ID:</b><br/>The alternate ID may be an SID or a kerberos-ID for a user, or a DICOM Application Entity title for an application. For a media it could be a bar code.");

$EVENT_DATETIME_TOOLTIP = _("<b>Datetime::</b><br/>Use these fields to search for events that occurred within a specific time frame. The format is DD-MM-YYYY hh:mm");

$AP_USER_NAME_TOOLTIP = _("<b>User Name:</b><br/>User Name is the name of the active Participant, e.g. John Doe if the participant is a user.");

$AUDIT_SRC_ID_TOOLTIP = _("<b>Audit Source:</b><br/>The source of the event. The Workflow manager, for instance, specifies WFM.");

$XML_TOOLTIP = _("<b>Free text:</b><br/>Search for any substring in the actual (xml) audit message");

$PARTICIPANT_OBJECT_ID_TYPE_CODE_TOOLTIP = _("<b>ID Type:</b><br/>The ID-type field is useful to find audit records that involve e.g a specific study (<b>Study Instance UID</b>).");

$PO_OID_TOOLTIP = _("<b>ID:</b><br/>The ID depends on the type of participant. For a patient it may be the social security number, while it will be a Study Instance UID for studies.");

$PARTICIPANT_OBJECT_TYPE_CODE_TOOLTIP = _("<b>Type:</b><br/>The type of the participant object. A patient is a <b>Person</b> with Role Type <b>Patient</b>, while a study is a <b>System Object</b> with role type <b>Report</B>.");

$PARTICIPANT_OBJECT_TYPE_CODE_ROLE_TOOLTIP = _("<b>Role Type:</b><br/>Specifies a role in combination with the <b>Type</b> field. A patient is a <b>Person</b> with Role Type <b>Patient</b>, while a study is a <b>System Object</b> with role type <b>Report</B>.");

$TIMEZONE_TOOLTIP = _("<b>Choose which timezone you want the times converted to for display</b>");
?>
