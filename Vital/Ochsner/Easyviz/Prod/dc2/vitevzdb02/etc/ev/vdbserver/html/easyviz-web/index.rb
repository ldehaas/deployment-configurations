#!/usr/bin/env ruby
require 'cgi'
require 'erb'
include ERB::Util               # has html_escape and url_encode

$cgi = CGI.new
puts $cgi.header({"type" => "text/html"})

# Parse Accept-Language header field
# "en-US,en;q=0.8,da-DK;q=0.5,da;q=0.3" =>
# [["en", 0.1], ["da", 0.3], ["da", 0.5], ["en", 1.0]]
def parse(parm)
    return [] if parm.nil?
    accepted = parm.split(",")
    accepted = accepted.map { |l| l.strip.split(";") }
    accepted = accepted.map { |l|
        lang = l[0].split("-")[0].downcase
        (l.size == 2) ? [ lang, l[1].sub(/^q=/, "").to_f ] : [ lang, 1.0 ]
    }
    # sort by quality
    accepted.sort { |l1, l2| l1[1] <=> l2[1] }
end

languages = parse(ENV['HTTP_ACCEPT_LANGUAGE']);

lang = 'en'
# Select supported language with highest quality
languages.each {|pair| lang = pair[0] unless ["da", "en", "sv", "fr", "de", "it"].index(pair[0]).nil? }

data = {
    "BUILD_DATE" => "BUILD_DATE",
    "EVVERSION"  => "EVVERSION"
}
data.default = "(key not in data hash)"

File.open('index.data').each_line do |s|
    fields = s.chomp.split /\s/, 2
    data[fields[0]] = fields[1]
end rescue nil

begin
    filename = "index."+lang+".erb"
    data.update(data) { |k, v| html_escape(v) }
    bind = binding # To prevent the erb script to have access to "f"
    puts File.open(filename, "r") { |f| ERB.new(f.read).result(bind) }
rescue Exception => ex
    puts "<pre>Error: "+html_escape(ex.message+"\n\n"+ex.backtrace.join("\n"))+"</pre>";
end
