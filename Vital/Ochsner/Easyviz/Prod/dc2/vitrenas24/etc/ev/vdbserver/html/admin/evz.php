<?php
#########################################################################################
#### This file is deprecated and will no longer be maintained!
####
#### EVZ integration should now be done via vdbserver's new EVZ service.
#### New integrations should call $BACKEND/vdb/rest/evz instead of $BACKEND/admin/evz.php
#### Configuration of this is found in /etc/ev/vdbserver/evz.conf
####
#### Apache will redirect from /admin/evz.php to /vdb/rest/evz by default.
#### To disable the redirect and use this .php file instead,
#### remove the RewriteRule directives in /etc/ev/vdbserver/conf/httpd.conf
#########################################################################################

# script can be invoked directly as:
# easyviz.php?username=admin&password=admin%patient_id=1234567890

# Contains configuration
$cfg = "evz-cfg.php";
$lang = "en_US"; # default
$pacs_urls = "";
$logout_user = 0;
$msg = "";
$uuid = "";

set_include_path(get_include_path() . ":../../..");

// First import easyviz server environment
include "common.php";
import_ev_env();

// evz-cfg.dev.php is used on dev machines to customize config for testing.
if (!include "evz-cfg.dev.php")
    if (!include "$cfg")
        if (!include $cfg)
            error_exit("Internal error: Cannot read configuration file \"$cfg\".\n");

if (!$evz_enabled)
    error_exit("Error: EVZ interface is disabled by configuration.");

function add_criteria($criteria, $value)
{
    if ($value == "")
        return $criteria;
    else
        if ($criteria == "")
            return $value;
        else
            return "$criteria and $value";
}

function full_url()
{
    $s = empty($_SERVER["HTTPS"]) ? '' : ($_SERVER["HTTPS"] == "on") ? "s" : "";
    $protocol = substr(strtolower($_SERVER["SERVER_PROTOCOL"]), 0,
                       strpos(strtolower($_SERVER["SERVER_PROTOCOL"]), "/")) . $s;
    $port = ($_SERVER["SERVER_PORT"] == "80") ? "" : (":".$_SERVER["SERVER_PORT"]);
    $full = $protocol . "://" . $_SERVER['SERVER_NAME'] . $port . $_SERVER['REQUEST_URI'];
    return preg_replace("/password=[^&]*/", "password=<redacted>", $full);
}

log_msg(9, "called url: " . full_url());

setup_lang();
$output = "";

# Support for CLI. Useful for development and testing
if (isset($argc) && $argc > 0) {
    for ($i = 1; $i < $argc; $i++) {
        parse_str($argv[$i],$tmp);
        $_REQUEST = array_merge($_REQUEST, $tmp);
    }
    if (array_key_exists('enable_otp', $_REQUEST))
        $enable_otp = $_REQUEST['enable_otp'];
}

$patient_id = get_if_exists('patient_id');
$patient_id_issuer = get_if_exists('patient_id_issuer');
$study_iuid = get_if_exists('study_iuid');
$accession_number = get_if_exists('accession_number');
$series_iuid = get_if_exists('series_iuid');

$ignore_default_auth = get_if_exists('ignore_default_auth', '0');

if ($ignore_default_auth == "1") {
    $default_user = NULL;
    $default_user_pwd = NULL;
}

$username = get_if_exists('username', $default_user);
$authzuser = get_if_exists('authzuser', $username);

$password_key = get_if_exists('password', "");
$password = "";
if (array_key_exists($password_key, $userpasswordmapping)){
    $password = $userpasswordmapping[$password_key];
} else {
    if ($password_key == "" && $default_user_pwd != NULL)
        $password = $default_user_pwd;
    else
        $password = $password_key;
}

$clientip = get_if_exists('clientip', "");
$presentation = get_if_exists('presentation', "");
$msg_id = get_if_exists('msg_id', $msg_id);
$output_type = get_if_exists("output_type", $output_type);

$collabtype = "";
$sessionid = "";

if (array_key_exists('collab_join', $_REQUEST)) {
    $collabtype = "join";
    $sessionid = $_REQUEST['collab_join'];
}

$collab_join = get_if_exists('collab_join',"");

if (array_key_exists('logoutuser',$_REQUEST)) {
    $logout_user = 1;
}

log_msg(5, "evweb_ext: " . externalEvWeb());
log_msg(5, "backend_ext: " . externalBackend());
log_msg(5, "destination_app: $destination_app");
log_msg(5, "msg_id: $msg_id");
log_msg(5, "collabtype: $collabtype");
log_msg(5, "sessionid: $sessionid");
log_msg(5, "presentation name: $presentation");

$log_content = "patient_id: " . ($patient_id == "" ? "N/A" : "$patient_id");
$log_content .= " (pat_id_issuer: " . ($patient_id_issuer == "" ? "N/A" : "$patient_id_issuer") . "), ";
$log_content .= " acc_no: " . ($accession_number == "" ? "N/A" : "$accession_number") . ", ";
$log_content .= " study_iuid: " . ($study_iuid == "" ? "N/A" : "$study_iuid") . ", ";
$log_content .= " series_iuid: " . ($series_iuid == "" ? "N/A" : "$series_iuid");
$remoteip = $_SERVER['REMOTE_ADDR'];
$log_content .= ", opened by  ip: $remoteip, username: $username (extuser: "
    . ($authzuser == "" ? "N/A" : "$authzuser") . ")";

log_msg(3, $log_content);

if ($logout_user) {
    logout_user($username, $authzuser, $password);
    exit;
}

if ($patient_id == "" && $study_iuid == "" && $series_iuid == "" && $accession_number == "" && $collab_join = "")
    error_exit ("$output Error: At least one of patient_id, study_iuid, series_iuid, accession_number or collab_join must be provided");

if (array_key_exists('worklist_title', $_REQUEST)) {
    $title = $_REQUEST['worklist_title'];
} else {
    $criteria = "";
    $criteria = add_criteria($criteria, ($patient_id   != "" ? "patient id '$patient_id'" : ""));
    $criteria = add_criteria($criteria, ($patient_id_issuer   != "" ? "issuer of patient id  '$patient_id_issuer'" : ""));
    $criteria = add_criteria($criteria, ($accession_number != "" ? "accession number '$accession_number'" : ""));
    $criteria = add_criteria($criteria, ($study_iuid != "" ? "study iuid '$study_iuid'" : ""));
    $criteria = add_criteria($criteria, ($series_iuid != "" ? "series iuid '$series_iuid'" : ""));

    $criteria = str_replace("'", "&quot;", $criteria);
    $title = $criteria == ""
        ? "All studies from PACS"
        : "Studies from PACS with $criteria";
}

$pacs_host = get_if_exists('pacs_host', $pacs_host);
$pacs_port = get_if_exists('pacs_port', $pacs_port);
$pacs_aet = get_if_exists('pacs_aet', $pacs_aet);
$pacs_urls = get_if_exists('archives', $pacs_urls);
$get_otp = get_if_exists('get_otp');

$auth_info_supplied = $username && $password;
$otp = $auth_info_supplied  && ($enable_otp || $get_otp)
    ? get_otp($username, $authzuser, $password, $clientip)
    : $password;

if ($get_otp) {
    echo "$otp";
    exit;
}

if ($patient_id != "" || $study_iuid != "" || $series_iuid != "" || $accession_number != "") {
    $msg = "<easyviz-activate version='1.2'>\n";

    $sources = "";
    if ($pacs_urls != "") {
        $urls = $pacs_urls;
        while ($urls != '') {
            if (preg_match("/^dicom:\/\/([^:@,;]+):([^:@,;]+)@([a-zA-Z0-9_.-]+):(\d+)(,|;)?/", $urls, $m)) {
                $sources .= $m[1] . (count($m) >= 6 ? $m[5] : "");
                $msg .= " <archive hostname='$m[3]' port='$m[4]' aetitle='$m[1]' calling_aetitle='$m[2]'";
                if (array_key_exists('patient_history', $_REQUEST)) {
                    $msg .= " patient_history='$_REQUEST[patient_history]'";
                }
                $msg .= "/>\n";

                $urls = substr($urls, strlen($m[0]));
                if (strpos($urls, " ") !== false) {
                    $urls = substr($urls, strpos($urls, " ") + 1);
                }
                if (strpos($urls, "dicom:") === false) {
                    $urls = "";
                }
            } else {
                error_exit("Error: Cannot parse URL specified in the archives parameter: URL=" . $_REQUEST['archives'] . ", msg=$msg, urls=$urls");
            }
        }
        $sources = "sources='$sources'";
    } else {
        $msg .= " <archive hostname='$pacs_host' port='$pacs_port' aetitle='$pacs_aet'" .
            (array_key_exists('patient_history',$_REQUEST) ? " patient_history='$_REQUEST[patient_history]'" : "") .
            "/>\n";
    }

    $activate_presentation = "";

    if ($presentation != "")
        $activate_presentation="activate_presentation='$presentation'";

    $msg .= " <worklist mode='clear' title='$title' $sources $activate_presentation>\n";
    $msg .= "<patient"
        . ($patient_id != "" ? " patient_id='$patient_id'" : "")
        . ($patient_id_issuer != "" ? " patient_id_issuer='$patient_id_issuer'" : "")
        . ">\n";

    if ($study_iuid != "" || $series_iuid != "" || $accession_number != "") {
        $msg .= "  <study "
            . ($study_iuid != "" ? " uid='$study_iuid'" : "")
            . ($accession_number != "" ? " accession_number='$accession_number'" : "")
            . ">\n";
        if ($series_iuid != "")
            $msg .= "    <series uid='$series_iuid'/>\n";
        $msg .= "  </study>\n";
    }
    $msg .= "</patient>\n";
    $msg .= "</worklist>\n";
    $msg .= "</easyviz-activate>\n";
}

$username_encoded = rawurlencode($username);
$authzuser_encoded = rawurlencode($authzuser);
$otp_encoded = rawurlencode($otp);

// for debugging: allow the generated activation message to be displayed, instead of storing it to the database.
if ($output_type == "dump-activation") {
    header('Content-Type: text/plain');
    echo $msg;
} else if ($output_type == "evz-protocol") {
    header('Content-Type: text/plain');
    header('Cache-Control: private, must-revalidate, max-age=0');

    $sessionid = daemonLogin();
    if ($msg != "") {
        $uuid = storeActivation($sessionid, $msg);
    }
    backendLogout($sessionid);
    $activation = ($uuid != "" ? "&activation_key=$uuid&activation_msg_id=$msg_id" : "");
    global $host_ext;
    global $port_ext;
    global $https_ext;
    global $legacy_cbserver_ext;
    $urlmsg = "evz://$username_encoded:$otp_encoded@$host_ext:$port_ext/vdb"
        ."?authzname=$authzuser_encoded$activation&https=" . ($https_ext ? "yes" : "no");
    if (isset($legacy_cbserver_ext)) {
        $urlmsg = $urlmsg . "&loginserver=$legacy_cbserver_ext";
    }
    $urlmsg = $urlmsg . "&encoding_hint=%c3%b8";
    echo $urlmsg;

} else if ($output_type == "web") {
    header('Content-Type: text/plain');
    header('Cache-Control: private, must-revalidate, max-age=0');

    $sessionid = daemonLogin();
    if ($msg != "") {
        $uuid = storeActivation($sessionid, $msg);
    }
    backendLogout($sessionid);

    $append = "";
    if ($collabtype == "join")
        $append = "&collab_join=" . $sessionid;
    $activation = ($uuid ? "&activation_key=$uuid&activation_msg_id=$msg_id" : "");
    echo externalEvWeb() . "?username=$username_encoded&password=$otp_encoded&authzname=$authzuser_encoded$activation$append";
} else if ($output_type == "local") {
    shell_exec("DISPLAY=:0 xprop -f $msg_id 8s -name 'PACS WFM' -set $msg_id \"$msg\"");
} else {
    $user_output = $auth_info_supplied ? "   <user name='$username' authzname='$authzuser' password='$otp'/>\n" : "";

    $legacy_string = '';
    if (isset($legacy_cbserver_ext)) {
        $legacy_string = "   <server hostname='$legacy_cbserver_ext'/>\n";
    }

    $output .=
        "<?xml version='1.0' encoding='utf-8'?>\n" .
        " <easyviz-msg version='1.0'>\n" .
        "  <header>\n" .
        "   <backend url='" . externalBackend() . "'/>\n" .
        $legacy_string .
        $user_output .
        "   <destination application='$destination_app' msg-id='$msg_id'/>\n" .
        ( ($collabtype != "") ? "   <collaboration type='$collabtype' session-id='$sessionid' />\n" : "") .
        "  </header>\n";

    if ($msg != "" ) {
        $sessionid = daemonLogin();
        $uuid = storeActivation($sessionid, $msg);
        backendLogout($sessionid);
        $output .= "<body type='text'>";
        $output .= "activation_key:" . $uuid;
        $output .= "</body>";
    } else {
        $output .= "<body type='text' />";
    }

    $output .= "</easyviz-msg>\n";

    header('Content-Type: application/x-easyviz');
    header("Content-Disposition: attachment; filename=\"worklist.evz\";");
    header('Content-Length: ' . strlen($output));
    header('Cache-Control: private, must-revalidate, max-age=0');

    echo "$output";
}
?>
