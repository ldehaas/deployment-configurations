<?php
#########################################################################################
#### This file is deprecated and will no longer be maintained!
####
#### EVZ integration should now be done via vdbserver's new EVZ service.
#### New integrations should call $BACKEND/vdb/rest/evz instead of $BACKEND/admin/otp.php
#### Configuration of this is found in /etc/ev/vdbserver/evz.conf
####
#### Apache will redirect from /admin/otp.php to /vdb/rest/evz?get_otp=1 by default.
#### To disable the redirect and use this .php file instead,
#### remove the RewriteRule directives in /etc/ev/vdbserver/conf/httpd.conf
#########################################################################################

# script can be invoked directly as:
# otp.php?username=admin&password=admin

# Contains configuration
$cfg = "evz-cfg.php";
$lang = "en_US"; # default

set_include_path(get_include_path() . ":/etc/ev/");

// First import easyviz server environment
include "common.php";
import_ev_env();

if (!include "$cfg")
    if (!include $cfg)
        error_exit("Internal error: Cannot read configuration file \"$cfg\".\n");

if (!$evz_enabled)
    error_exit("Error: EVZ interface is disabled by configuration.");

setup_lang();

# Support for CLI. Useful for development and testing
if (isset($argc) && $argc > 0) {
    for ($i = 1; $i < $argc; $i++) {
        parse_str($argv[$i], $tmp);
        $_REQUEST = array_merge($_REQUEST, $tmp);
    }
    if (array_key_exists('enable_otp',$_REQUEST))
        $enable_otp = $_REQUEST['enable_otp'];
}

$username = get_if_exists('username',$default_user);
$authzuser = get_if_exists('authzuser', $username);
$password = get_if_exists('password', $default_user_pwd);
$clientip = get_if_exists('clientip', "");

if (!isset($password)) {
    error_exit("Error: No user password passed.");
}

log_msg(5, "username: $username");
log_msg(5, "authzuser: $authzuser");

log_msg(5, "host_ext: $host_ext");

$otp = get_otp($username, $authzuser, $password, $clientip);

header('Content-Type: text/plain');
header('Content-Length: ' . strlen($otp));
header('Cache-Control: private, must-revalidate, max-age=0');

echo "$otp";
?>
