<?php
#########################################################################################
#### This file is deprecated and will no longer be maintained!
####
#### EVZ integration should now be done via vdbserver's new EVZ service.
#### New integrations should call $BACKEND/vdb/rest/evz instead of $BACKEND/admin/evz.php
#### Configuration of this is found in /etc/ev/vdbserver/evz.conf
####
#### Apache will redirect from /admin/evz.php to /vdb/rest/evz by default.
#### To disable the redirect and use this .php file instead,
#### remove the RewriteRule directives in /etc/ev/vdbserver/conf/httpd.conf
#########################################################################################

# Please adjust for your setup.
$evz_enabled = 1;

# External vdbserver access
# use https for external access? true/false
$https_ext = true;
# external backend hostname
$host_ext = "localhost";
# external backend port
$port_ext = 443;

# External cbserver access (for 5.x clients only - can be commented out otherwise)
$legacy_cbserver_ext = "$host_ext:7500";

# these are only defaults, caller of php script
# should provide username/password
$default_user = "external";

# !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
# WARNING: Configuring default_user_pwd is a DEPRECATED feature and is highly discouraged.
#          Setting default_user_pwd will make the system widely open which is a MAJOR SERCURITY ISSUE!!!
#          Don't do this unless you have consulted with site's administrators about their security policies.
#          If default_user_pwd is set, you'd better restrict the access to evz.php by using .htaccess file.
# !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
$default_user_pwd = NULL;

# Enable or disable One Time Passwords
$enable_otp = 1;

# pacs
$pacs_host = "hest.fisk.com";
$pacs_port = 11112;
$archives = "";
# Only used if $archives is empty
$pacs_aet = "QR_AET";

# logging
$log_level = 1;
$log_file = "/var/log/ev/web/easyviz.php.log"; # Must be writable by vdbserver user
$log_date_format = 'Y-m-d\TH:i:sO'; # DATE_ISO8601

# message
$destination_app = "wfm";
$msg_id = "EV_WORKLIST_SHOW";

// The url of your backend. If you use ldap for authentication you must use
// the fully qualified hostname. In any case the url must not have a trailing '/'
$vdb_backend = getenv("EV_BACKEND");

// Extra options for talking to the backend. Normally used for providing certificates
// (--cacert or --capath) or to turn of certificate checking (-k). This is only necessary
// if the certificate is not trusted by the CA certificates installed on the machine.
//
// If the backend uses certificates issued by the Vital Images CA use the following value
// below: "--capath /usr/share/ev/vdbclient/certs/"
// See curl(1) for details.
$vdb_extra_opts = "";

// Valid options: "evz-file", "evz-protocol", "web", "local"
$output_type = "evz-file";

// Some sites map from one incoming password to another. If needed this can be configured here.
// Example:
//    $userpasswordmapping = array('PasswordKey1' => 'xyzzy',
//                                 'PasswordKey2' => 'secret');
$userpasswordmapping = array();
?>
