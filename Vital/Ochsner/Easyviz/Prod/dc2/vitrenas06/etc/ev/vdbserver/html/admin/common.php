<?php
#########################################################################################
#### This file is deprecated and will no longer be maintained!
####
#### EVZ integration should now be done via vdbserver's new EVZ service.
#### New integrations should call $BACKEND/vdb/rest/evz instead of $BACKEND/admin/evz.php
#### Configuration of this is found in /etc/ev/vdbserver/evz.conf
####
#### Apache will redirect from /admin/evz.php to /vdb/rest/evz by default.
#### To disable the redirect and use this .php file instead,
#### remove the RewriteRule directives in /etc/ev/vdbserver/conf/httpd.conf
#########################################################################################

// strip out username/password from backend url
function getBackend()
{
    global $vdb_backend;

    $parts = parse_url($vdb_backend);
    $port = array_key_exists('port', $parts) ? (':' . $parts['port']) : '';
    return $parts['scheme'] . '://' . $parts['host'] . $port . $parts['path'];

}

function getUser()
{
    global $vdb_backend;

    $parts = parse_url($vdb_backend);
    return urldecode($parts['user']);
}

function getPassword()
{
    global $vdb_backend;

    $parts = parse_url($vdb_backend);
    return urldecode($parts['pass']);
}

function externalBase()
{
    global $https_ext;
    global $host_ext;
    global $port_ext;

    return ($https_ext ? "https" : "http") . "://$host_ext:$port_ext";
}

function externalBackend()
{
    return externalBase() . '/vdb';
}

function externalEvWeb()
{
    return externalBase() . '/easyviz-web';
}

function get_if_exists($index_var, $default_val = "")
{
    if (array_key_exists($index_var,$_REQUEST))
        return $_REQUEST[$index_var];

    return $default_val;
}

function setup_lang()
{
    global $lang;

    $langs = array();

    # read all available locales
    $locales = explode("\n", run_command("locale -a", "", $rc, $error));
    asort($locales);

    if (isset($_SERVER['HTTP_ACCEPT_LANGUAGE'])) {
        // break up string into pieces (languages and q factors)
        preg_match_all('/([a-z]{1,8}(-[a-z]{1,8})?)\s*(;\s*q\s*=\s*(1|0\.[0-9]+))?/i', $_SERVER['HTTP_ACCEPT_LANGUAGE'], $lang_parse);

        if (count($lang_parse[1])) {
            // create a list like "en" => 0.8
            $langs = array_combine($lang_parse[1], $lang_parse[4]);

            // set default to 1 for any without q factor
            foreach ($langs as $lang => $val) {
                if ($val === '')
                    $langs[$lang] = 1;
            }

            // sort list based on value
            arsort($langs, SORT_NUMERIC);
        }
    }

    // Take the first lang from the list
    foreach ($langs as $l => $v) {
        $l1 = substr($l, 0, 2);
        $l2 = strtoupper(substr($l, 3, 2));

        // Try exact match
        if (in_array($l1 . "_" . $l2, $locales)) {
            $lang = $l1 . "_" . $l2;
            return;
        }

        // Try taking first existing locale starting with current lang prefix
        foreach ($locales as $loc) {
            if (substr($loc, 0, 2) == $l1) {
                $lang = $loc;
                return;
            }
        }
    }
}

function ie_pad($msg)
{
    $padding = "<!-- This comment is here in order to work around a glitch in certain versions of Microsoft Internet Explorer, which,   " .
        "makes it ignore responses less that 1024 characters.                                                                              " .
        str_repeat(' ', 1000) . "--> ";
    return $padding . $msg;
}

function error_exit($msg)
{
    $padded_msg = ie_pad($msg);
    header("HTTP/1.1 500 Internal error");
    echo $padded_msg;
    exit(1);
}

function log_msg($lvl, $msg)
{
    global $log_level, $log_file, $log_date_format;
    if ($lvl < $log_level)
        error_log(@date($log_date_format) . " Debug $lvl: " . rtrim($msg) . "\n", 3, $log_file);
}

function run_command($cmd, $to_stdin, &$return_value, &$error_msg)
{
    global $log_file, $lang;

    putenv("DISPLAY=");
    putenv("XAUTHORITY");
    putenv("HOME");
    putenv("EV_DEBUG_LEVEL=0");

    $cmd = "LANG=$lang $cmd";
    $result = "";
    $descriptorspec = array(0 => array("pipe", "r"),  // stdin is a pipe that the child will read from
                            1 => array("pipe", "w"),  // stdout is a pipe that the child will write to
                            2 => array("pipe", "w")   // stderr is a pipe that the child will write to
                            );

    $error_msg = "";

    log_msg(6, "starting cmd");
    $process = proc_open($cmd, $descriptorspec, $pipes);

    if (is_resource($process)) {
        // $pipes now looks like this:
        // 0 => writeable handle connected to child stdin
        // 1 => readable handle connected to child stdout
        // Any error output will be appended to /tmp/error-output.txt
        if ($to_stdin)
            fwrite($pipes[0], $to_stdin);
        fclose($pipes[0]);

        while (!feof($pipes[1]))
            $result .= fread($pipes[1], 8192);
        fclose($pipes[1]);

        while (!feof($pipes[2]))
            $error_msg .= fread($pipes[2], 8192);
        fclose($pipes[2]);

        if ($error_msg != "")
            log_msg(6, $error_msg);

        // It is important that you close any pipes before calling
        // proc_close in order to avoid a deadlock
        $return_value = proc_close($process);
        log_msg(6, "command finished with exit code: $return_value");
    } else {
        log_msg(6, "failed to start command");
        $result = undefined;
        $args = explode(" ", $cmd);
        $error_msg = "Failed to start command $args[0]";
    }
#    echo "result: $result\n";
    return $result;
}

function backendLogin($username, $authzuser, $password)
{
    $obj;
    $obj["username"] = $username;
    $obj["password"] = $password;
    if ($authzuser) {
        $obj["external"] = $authzuser;
    }
    $obj["language"] = "en_US";
    $obj["client_type"] = "daemon";

    $msg = json_encode($obj);

    $result = vdb_request("/rest/session", "PUT", $msg, "application/json");
    $out_obj = json_decode($result, true);
    if ($out_obj['session-id']) {
        return $out_obj['session-id'];
    }
    error_exit ("Backend login failed");
}

function daemonLogin()
{
    return backendLogin(getUser(), '', getPassword());
}

function backendLogout($session_id)
{
    $result = vdb_request("/rest/session/" . $session_id, "DELETE");
}

function get_otp($username, $authzuser, $password, $clientip = "")
{
    setlocale(LC_CTYPE, "UTF8", "en_US.UTF-8");

    $sessionid = backendLogin($username, $authzuser, $password);
    if ($clientip) {
        $clientip = '/' . $clientip;
    }
    $otp = vdb_request("/rest/session/$sessionid/otp$clientip", 'GET');
    $otp = rtrim($otp);
    backendLogout($sessionid);
    log_msg(6, "returned otp: $otp");
    return $otp;
}

function logout_user($username, $authzuser, $password)
{
    $session = backendLogin($username, $authzuser, $password);
    vdb_request("/rest/session/" . $session . "/vnc/sameuser", "DELETE");
    backendLogout($session);
}

function import_ev_env()
{
    // Is there a better way to read the environment configuration /etc/ev/ev.env
    // that does not require us to change /etc/init.d/httpd to source it than this

    if (file_exists("/etc/ev/ev.env"))
        $evEnv = file("/etc/ev/ev.env");
    else
        return;

    foreach ($evEnv as $value) {
        // Match unquoted or quoted with " or '
        if (preg_match('/^\s*export\s+(\w+)=([^\'"][^\s]*)(?:\s.*)?$/', $value, $matches) ||
            preg_match('/^\s*export\s+(\w+)="((?:[^"\\\\]|\\\\.)*)"(?:\s.*)?$/', $value, $matches) ||
            preg_match('/^\s*export\s+(\w+)=\'((?:[^\'\\\\]|\\\\.)*)\'(?:\s.*)?$/', $value, $matches)) {
            putenv($matches[1] . "=" . $matches[2]);
        }
    }
}

// For communicating with the vdb backend
function vdb_request($path, $method = "GET", $body = "", $content_type = "text/plain")
{
    global $vdb_extra_opts;

    $cmd = "/usr/bin/curl" .
        " -s " . $vdb_extra_opts .
        ($body ? " -d " . escapeshellarg($body) : "") .
        " -H 'Content-Type: $content_type; charset=utf-8' -X " . $method . " " . escapeshellarg(getBackend() . $path);

    $response = run_command($cmd, "", $rc, $error);

    if (!isset($rc) || $rc != 0)
        error_exit("Error: Request to vdbserver $path failed. Reason: $error");

    return $response;
}

function storeActivation($session, $activation)
{
    return vdb_request("/rest/session/" . $session . "/activation", "PUT", $activation);
}
?>
