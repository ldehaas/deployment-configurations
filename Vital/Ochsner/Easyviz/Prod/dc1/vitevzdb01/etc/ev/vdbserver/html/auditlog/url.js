function URL(aString) {
  if (!aString.match(/^http(s?):\/\//i))
    throw "Only http URLs supported";
  var components = aString.split("?", 2);

  this.baseurl = components[0];

  var keyvalStrings = components[1].split("&");

  this.arguments = new Object;
  var i
  for (i=0; i<keyvalStrings.length; ++i) {
    var kv = keyvalStrings[i].split("=", 2);
//    alert(kv[0]);
    this.arguments[kv[0]] = kv[1];
  }
}

URL.prototype.toString = function() {
  paramString = "";
  for (key in this.arguments) {
    if (paramString.length > 0)
      paramString += "&";
    paramString += key+"="+ (this.arguments[key] == undefined ? "" : this.arguments[key]);
  }

  return this.baseurl+"?"+paramString;
}

URL.prototype.lookup = function(key) {
  return this.arguments[key];
}
