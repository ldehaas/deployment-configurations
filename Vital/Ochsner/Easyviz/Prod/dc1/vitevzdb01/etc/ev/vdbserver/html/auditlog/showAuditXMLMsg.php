<?php
require('gettext.php');
require('dbconn.php');
if (!$dbconn)
    exit(_("Could not connect to database."));

$id = $_GET['id'];
$sql = "select xml from auditlog where id = $id";
$rs = odbc_exec($dbconn, $sql);
odbc_longreadlen($rs, 32768);
odbc_fetch_row($rs);

$xml = odbc_result($rs, "xml");
# Fix for bug 15542. With DB2 at least we get a hex string back and need to
# convert that to an ascii string. No amount of php_binmode() use and other
# fiddling has been able to convince php to give me a string directly.
if (substr($xml, 0, 2) != "<?")
    $xml = pack('H*', $xml);
if ($xml) {
    header("Content-Type: text/xml");
    echo $xml;
} else {
?>
<html>
<head><title><?php echo _("Error: Empty message"); ?></title>
<body><h3><?php echo _("Error: Empty message!"); ?></h3></body>
</html>
<?php
}
?>
