# This is the configuration file defining mapping from internal to external group names
#
# The format is:
#   [OU]
#   internal group name1=external group name1
#   internal group name1=external group name2
#   ...
#
# Where OU is an Organization Unit name. It has the format local-name@domain. The domain part is optional
# but if present allows local-name part not to be unique. Section names (OUs) must be unique
# in this file. Which OU section will be used from groups.conf depends on OU of currently logged
# in user. It is determined in the following way:
#
# * in case of DB user backend
#   User's OU stored in db in field users.ou must exactly case insensitively section's name
#
# * in case of LDAP/AD user backend
#   All OU sections of user's DN are checked local name of groups.conf section. If there is a match
#   then matching OU from groups.conf is found. If OU section name contains the optional domain part,
#   then it must match (case insesitively) the domain of user's account. Here is an example of a
#   matching OU section to an user with account test-user@EXAMPLE.COM having a DN in LDAP
#   CN=test-user,OU=ABC,OU=DEF,OU=GHI,DC=EXAMPLE,DC=COM
#
#   [def@example.com]
#   EasyViz Users=DEF EasyViz Users
#   ...
#
# Semantic notes:
#   - an empty section name [], is always read as [DEFAULT], as empty OU is not allowed
#   - a section called DEFAULT does not match all!
#   - in case of LDAP:
#     * EV_GROUP_BASE_DN is a special internal group name which is used to defines OUs base DN,
#       that is, all groups must be fined exactly at the point in the LDAP tree!
#     * external group names have CN= prefix prepended and the value of EV_GROUP_BASE_DN appended.
#       For instance if you have a map etries:
#
#           EV_GROUP_BASE_DN=OU=ABC,DC=EXAMPLE,DC=COM
#           EasyViz Users=Department A Users
#
#       The effective external group name for EasyViz Users for that OU would be:
#           CN=Department A Users,OU=ABC,DC=EXAMPLE,DC=COM
#
#       If a group is located not at EV_GROUP_BASE_DN in the tree, then use DN starting with CN
#       section. In that case EV_GROUP_BASE_DN will not be appended. For instance:
#
#           EasyViz Users=CN=Department A Users,OU=ou1,OU=ABC,DC=EXAMPLE,DC=COM
#
# Formatting notes:
#   1. Do not put extra spaces between words
#   2. Group names are case sensitive
#   3. If you do not need to map the internal groups to external names you'd better remove/rename this
#      config file so that it will not be loaded.
#
[DEFAULT@stph.org]
EV_GROUP_BASE_DN=OU=Vital,OU=Cross-Trust Groups,DC=stph,DC=org
EasyViz Users=EasyViz PROD Users
EasyViz Internal Users=EasyViz PROD Internal
EasyViz External Users=EasyViz PROD External
EasyViz Groups=EasyViz PROD Groups
EasyViz Administrators=EasyViz PROD Administrators

[DEFAULT@network.ad.tgmc.com]
EV_GROUP_BASE_DN=OU=Vital,OU=EPIC,DC=network,DC=ad,DC=tgmc,DC=com
EasyViz Users=EasyViz PROD Users
EasyViz Internal Users=EasyViz PROD Internal
EasyViz External Users=EasyViz PROD External
EasyViz Groups=EasyViz PROD Groups
EasyViz Administrators=EasyViz PROD Administrators
