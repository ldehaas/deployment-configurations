#/usr/bin/perl -w
# -*- mode: perl -*-

package analyze;

BEGIN {
      use Exporter;

      @ISA         = (Exporter);
      @EXPORT	=  qw( $debug $print_update $disable_command_running %aet
                       $default_aet $vps_aet $our_aet
                       $tiani_db $tiani_user $tiani_password
                       $vdb_db $vdb_user $vdb_password
                       $storage_class_uids
                      );
     @EXPORT_OK   =  qw();
}

our $debug=0;
our $print_update = 1;
our $disable_command_running=1;

# AETs
our %aet = ("QR_01_SCP"=>"aasdcm01",
           "QR_02_SCP"=>"aasdcm02",
           "QR_03_SCP"=>"aasdcm03",
           "QR_04_SCP"=>"aasdcm04",
           "QR_05_SCP"=>"aasdcm05",
           "QR_06_SCP"=>"aasdcm06",
           "QR_07_SCP"=>"aasdcm07",
           "QR_08_SCP"=>"aasdcm08",
           "QR_09_SCP"=>"aasdcm09",
           "QR_10_SCP"=>"aasdcm10");

our $default_aet='QR_01_SCP';
our $vps_aet='VPS_AAS';
our $our_aet="ANALYZE";

# DB users
our $tiani_db='tiani';
our $tiani_user='tiani';
our $tiani_password='letmework';

our $vdb_db='VDB_AAS';
our $vdb_user='db2inst1';
our $vdb_password='letmework';

our $storage_class_uids =
"(    '1.2.840.10008.5.1.4.1.1.1',
      '1.2.840.10008.5.1.4.1.1.1.2',
      '1.2.840.10008.5.1.4.1.1.1.1',
      '1.2.840.10008.5.1.4.1.1.2',
      '1.2.840.10008.5.1.4.1.1.3',
      '1.2.840.10008.5.1.4.1.1.4',
      '1.2.840.10008.5.1.4.1.1.7',
      '1.2.840.10008.5.1.4.1.1.6.1',
      '1.2.840.10008.5.1.4.1.1.6',
      '1.2.840.10008.5.1.4.1.1.3.1',
      '1.2.840.10008.5.1.4.1.1.12.1',
      '1.2.840.10008.5.1.4.1.1.12.2',
      '1.2.840.10008.5.1.4.1.1.88.59',
      '1.2.840.10008.5.1.4.1.1.11.1',
      '1.2.840.10008.5.1.4.1.1.128',
      '1.2.840.10008.5.1.4.1.1.1.3',
      '1.2.840.10008.5.1.4.1.1.77.1.5.1',
      '1.2.840.10008.5.1.4.1.1.77.1.5.2');
1;
