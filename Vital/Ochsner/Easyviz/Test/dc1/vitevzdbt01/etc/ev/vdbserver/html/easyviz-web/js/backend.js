function text(node) {
    if (!node) {
        return null;
    }
    return node.textContent ? node.textContent : node.text;
}

function vncreq(cookie, xpix, ypix, xmm, ymm, tls, no_wfm, session_mode, shared_session_id, botmode_uri, lang, session_log_id)
{
    if (!lang)
        throw new Error("lang not set in vncreq");

    var obj = {
        log_session_id: session_log_id,
        cams_only: no_wfm,
        tls: tls,
        language: lang,
        session_type: session_mode,
        screens: [{
            width_pix: xpix,
            height_pix: ypix,
            width_mm: xmm,
            height_mm: ymm,
            position_x: 0,
            position_y: 0,
            bits_per_pixel: 32
        }]
    };

    if (obj.session_type === "JOIN") {
        obj.join_session_id = shared_session_id;
    }

    if (botmode_uri) {
        obj.botmode = {
            uri: botmode_uri
        };
    }

    return JSON.stringify(obj);
}

function BackendLogin(next) {
    return function(context, pass) {
        context.func = "backendLogin";
        if (!context.generic_error_callback) {
            context.generic_error_callback = generic_error_callback;
        }
        if (!context.xhr_error_callback) {
            context.xhr_error_callback = xhr_error_callback;
        }
        $("#ev_connect_status").html(tr("LOGGING_IN")+"..");

        var authz = context.authz ? context.authz : "";
        var user = (context.user.indexOf('@') == -1 && context.realm != "") ? context.user + '@' + context.realm : context.user;

        evlog.debug(1, "specified: " + context.user + ", authz: " + context.authz + ", realm: " + context.realm);
        evlog.debug(1, "actual user: " + user + ", authz: " + authz);

        var term_id = $.cookie('term_id');
        if (!term_id) {
            term_id = 'term_web_' + Number(new Date().getTime()
            + '' + Math.round(Math.random() * 1000000)).toString(16);
            evlog.debug(5, "Generating new terminal id: " + term_id);
        }
        $.cookie('term_id', term_id, { expires: 365 });

        var login_data = {
            "username": user,
            "password": pass,
            "integration_type": context.is_integration ? "evz" : "none",
            "language": context.lang,
            "web_client": true,
            "client_type": "rendernode",
            "terminal_id": term_id
        };

        if (authz !== "") {
            login_data.external = authz;
        }

        $.ajax("/vdb/rest/session", {
            cache : false,
            contentType : 'application/json; charset=utf-8',
            data : JSON.stringify(login_data),
            type : "PUT",
            error : context.xhr_error_callback,
            success : function (data, status, rsp) {
                next(context, data, status, rsp);
            }
        });
    };
}

function HandleBackendSession(next) {
    return function(context, data, status, rsp) {
        context.func = "handleBackendSession";
        // we're logged in, so make endSession do something
        endSession = endActualSession;
        context.user = data.canonical_name;
        evlog.debug(0, "Logged in as user " + context.user);
        context['session-id'] = data['session-id'];
        next(context);
    };
}

function RequestVncSession(next) {
    return function(context) {
        var session_id = context['session-id'];
        try {
            context.func = "RequestVncSession";

            var px = window.innerWidth - 5;
            var py = window.innerHeight - 5;
            evlog.debug(7, "Assuming screen area to be " + px + "x" + py + " pixels");
            // Assume 100 dpi
            var w = Math.round(px / 100 * 25.4);
            var h = Math.round(py / 100 * 25.4);
            evlog.debug(7, "Assuming 100 DPI, that gives us a physical size of " + w + "x" + h + " mm");
            var tls = is_secure();
            evlog.debug(7, "TLS flag for getDisplay request: " + tls);
            evlog.debug(1, "Requesting VNC session");

            $.ajax("/vdb/rest/session/" + session_id + "/vnc", {
                cache : false,
                contentType : 'application/json; charset=utf-8',
                dataType: 'json',
                processData: false, // send as json string, don't transform to query url
                data : vncreq(session_id, px, py, w, h, tls, context.no_wfm,
                              context.session_mode, context.shared_session_id,
                              window.BOTMODE_URI, context.lang, evlog.logsessionid()),
                type : "POST",
                error : context.xhr_error_callback,
                success : function (data, status, rsp) {
                    next(context, data, status, rsp);
                }
            });
        } catch (err) {
            evlog.error("Error: " + err);
            context.generic_error_callback("Login: "+err);
        }
    };
}

function HandleVncSession(next) {
    return function(context, data, status, rsp) {
        try {
            evlog.debug(9, "Got getDisplay response");
            context.func = "handleVncSession";
            if (!"displays" in data || !"tls" in data || !"secret" in data) {
                evlog.error("Invalid response to vnc request: " + typeof data);
            }
            evlog.debug(5, "Rendernode is located at " + data.displays[0].host + ":" + data.displays[0].port);
            if (data.displays[0].muxer_port) {
                evlog.debug(5, "Rendernode is using muxer on port " + data.displays[0].muxer_port);
            }
            evlog.debug(1, "Using rendernode " + data.displays[0].host + ":" + data.displays[0].port);
            evlog.debug(1, "TLS status for VNC connection: " + data.tls);
            next(context, data.displays[0].host, data.displays[0].port, data.displays[0].muxer_port, data.secret, data.tls, data.displays[0].internal_host);
        } catch (err) {
            evlog.error("Error handling vnc response");
            context.generic_error_callback("VncSession: "+err);
        }
    };
}
