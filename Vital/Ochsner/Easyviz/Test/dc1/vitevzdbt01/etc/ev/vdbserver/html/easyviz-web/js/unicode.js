var unicode = {
    fromString: function(s) {
        var cps = [];
        for (var i = 0; i < s.length; ++i) {
            var cu = s.charCodeAt(i);
            if (cu >= 0xd800 && cu <= 0xdbff) {
                var cu2 = s.charCodeAt(++i);
                if (cu2 >= 0xdc00 && cu2 <= 0xdfff) {
                    // valid surrogate pair
                    cps.push((cu << 10) + (cu2 - 0x35fdc00));
                } else {
                    Util.Error("Invalid string data. Mismatched surrogate pair");
                }
            } else {
                cps.push(cu);
            }
        }
        return cps;
    },

    toString: function(cps) {
        var s = '';
        for (var i = 0; i < cps.length; ++i) {
            var cp = cps[i];
            if (cp < 0x10000) {
                console.log("cp1", String.fromCharCode(cp));
                s += String.fromCharCode(cp);
            } else if (cp <= 0x10ffff) {
                console.log("cp2", String.fromCharCode((cp >> 10) + 0xd7c0, (cp & 0x3ff) + 0xdc00));
                s += String.fromCharCode((cp >> 10) + 0xd7c0, (cp & 0x3ff) + 0xdc00);
            } else {
                Util.Error("Invalid code point");
            }
        }
        return s;
    },

    toUtf8: function(cps) {
        var out = [];

        for (var i = 0; i < cps.length; ++i) {
            var c = cps[i];
            if (c < 0x80) {
                out.push(c);
            } else if (c < 0x0800) {
                out.push(((c >> 6) & 0x1f) | 0xc0);
                out.push((c & 0x3f) | 0x80);
            } else if (c < 0x010000) {
                out.push(((c >> 12) & 0x0f) | 0xe0);
                out.push(((c >> 6) & 0x3f) | 0x80);
                out.push((c & 0x3f) | 0x80);
            } else if (c < 0x110000) {
                out.push(((c >> 18) & 0x07) | 0xf0);
                out.push(((c >> 12) & 0x3f) | 0x80);
                out.push(((c >> 6) & 0x3f) | 0x80);
                out.push((c & 0x3f) | 0x80);
            }
        }
        return out;
    },

    fromUtf8: function(cus) {
        var cps = [];
        for (var i = 0; i < cus.length; ++i) {
            var cu = cus[i];
            if (cu < 0x80) {
                cps.push(cu);
            } else if (cu < 0xe0) {
                var cu2 = cus[++i];
                cps.push((cu << 6) + (cu2 - 0x3080));
            } else if (cu< 0xf0) {
                var cu2 = cus[++i];
                var cu3 = cus[++i];
                cps.push((cu << 12) + (cu2 << 6) + (cu3 - 0xe2080));
            } else if (cu< 0xf5) {
                var cu2 = cus[++i];
                var cu3 = cus[++i];
                var cu4 = cus[++i];
                cps.push((cu << 18) + (cu2 << 12) + (cu3 << 6) + (cu4 - 0x3c82080));
            }
        }
        return cps;
    }
}
