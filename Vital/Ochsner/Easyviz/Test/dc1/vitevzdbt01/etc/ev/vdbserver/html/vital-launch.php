<?php
        //
        // This php script handles the incoming url call from epic
        // by default EV just sends back a new url with a one-time-password 
        // to actually get the reqested study/images. However, this script
        // handles the OTP and re-invokes this so that the caller (epic) actually
        // gets the requested study displaying. Also, this script by default looks
        // for the requested study via the 'default_archives' conf setting in evz.conf.
        // Here is an example of requesting a specific study be opened:
        // https://vitevzdbt01.ochsner.org/vital-launch.php?username=svc.ezvepicuser&password=XXXXXX&authzname=v-dsmith&accession_number=987ABCD 
        //
        //
        // The user can request that the study be obtained from a specific VACE
        // environment by passing the vace parm on the url, here is an example:
        //
        // https://vitevzdbt01.ochsner.org/vital-launch.php?username=svc.ezvepicuser&password=XXXXXX&authzname=v-dgover&accession_number=26092225&vace=VACE2
        //

        // create curl resource
        $ch = curl_init();
        // build url for the restful call
        //  $builtUrl = getURLRoot();
        $builtUrl = "https://vitevzdbt01.ochsner.org";
        $builtUrl .= "/vdb/rest/evz?";
	$vace_part1 = "dicom://";
	$vace_part2 = "c7v-vitvcast03.ochsner.org";
        $url = getURLParams();
        $parts = parse_url($url);
        $foo = parse_str($parts["query"], $parsed);
        $accessionNumber = $parsed["accession_number"];
        $username = $parsed["username"];
        $password = $parsed["password"];
        $outputType = $parsed["output_type"];
        $authzname = $parsed["authzname"];
        $msgId = $parsed["msg_id"];
        $patientId = $parsed["patient_id"];
        $vace = $parsed["vace"];
        if ($patientId != '') {
                $issuerOfPID = "OHSEPIC";
        }
        if (($username == '') || ($password == '')) {
                $username = "admin";
                $password = "Vital123";
        }
        if ($outputType == '') {
                $outputType = "web";
        }
        if ($msgId == '') {
                $msgId = "EV_WORKLIST_SHOW";
        }
	if ($vace == '') {
		$vace = "";
	}
        $builtUrl .= "accession_number=" . $accessionNumber;
        $builtUrl .= "&username=" . $username;
        $builtUrl .= "&password=" . $password;
        $builtUrl .= "&output_type=" . $outputType;
        $builtUrl .= "&msg_id=" . $msgId;
        $builtUrl .= "&patient_id_issuer=" . $issuerOfPID;
        $builtUrl .= "&patient_id=" . $patientId;
        $builtUrl .= "&authzuser=" . $authzname;
        if ($vace != '') {
	        $builtUrl .= "&archives=" . getVace($vace, $vace_part1, $vace_part2); 
        }
// echo($builtUrl);
// echo("<br>");
// exit;
        // set url
        curl_setopt($ch, CURLOPT_URL, $builtUrl);
        //return the transfer as a string
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
        // set the UA
        curl_setopt($ch, CURLOPT_USERAGENT, 'EasyViz Restful Launcher');
        // Alternatively, lie, and pretend to be a browser
        // curl_setopt($ch, CURLOPT_USERAGENT, 'Mozilla/4.0 (compatible; MSIE 7.0; Windows NT 5.1)');
        // $output contains the output string
echo($ch);
echo("<br>");
//$ch .= "&confirm_on_close=0";
        $output = curl_exec($ch);
        // close curl resource to free up system resources
        curl_close($ch);
        //redirect to where the restful service responded with.
        //usleep(2000000);
// echo($output);
// echo("<br>");
        header("Location: " . $output);
// echo("<a href='".$output."'>here</a>");
// echo("<br>");
// echo($output);
        function getURLParams() {
                $temp = $_SERVER["REQUEST_URI"];
                return strstr($temp, '?');
        }

 	function getVace($vace, $vace_part1, $vace_part2) {
		switch ($vace) {

			case "VACE1":
				$output = $vace_part1 . $vace . "@" . $vace_part2 . ":4301";
				break;
			case "VACE2":
				$output = $vace_part1 . $vace . "@" . $vace_part2 . ":4302";
				break;
			case "VACE3":
				$output = $vace_part1 . $vace . "@" . $vace_part2 . ":4303";
				break;
			case "VACE4":
				$output = $vace_part1 . $vace . "@" . $vace_part2 . ":4304";
				break;
			case "VACE5":
				$output = $vace_part1 . $vace . "@" . $vace_part2 . ":4305";
				break;
			case "VACE6":
				$output = $vace_part1 . $vace . "@" . $vace_part2 . ":4306";
				break;
			default:
			$output = "dicom://NOVACE@10.0.0.9:9999";	
				
		}
		return $output;
	}

        function getURLRoot() {
                $pageURL = 'http';
                if ($_SERVER["HTTPS"] == "on") {
                        $pageURL .= "s";
                }
                $pageURL .= "://";
                if ($_SERVER["SERVER_PORT"] != "80") {
                        $pageURL .= $_SERVER["SERVER_NAME"].":".$_SERVER["SERVER_PORT"];
                } else {
                        $pageURL .= $_SERVER["SERVER_NAME"];
                }
                return $pageURL;
        }
?>
</body>
</html>
