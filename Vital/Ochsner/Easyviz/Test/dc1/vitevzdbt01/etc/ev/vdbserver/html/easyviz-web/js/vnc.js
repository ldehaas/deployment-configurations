// kind of hackish, but I don't see a better way around it
// need to work around old browsers which claim to have websockets, but which we know implement old draft versions
function hasWebsocketSupport(ua) {
    var webkit = /AppleWebKit\/(\d+).(\d+)/;
    var firefox = /Firefox\/(\d+).(\d+).(\d+)/;

    var ff_ver = firefox.exec(ua);
    var wk_ver = webkit.exec(ua);

    // oldest known good webkit version is 536.11
    if (wk_ver && wk_ver.length == 3 && (parseInt(wk_ver[1], 10) < 536 || parseInt(wk_ver[1], 10) == 536 && parseInt(wk_ver[1], 10) < 11)) {
        return false;
    }

    // oldest known good Firefox version is 7
    if (ff_ver && ff_ver.length == 4 && parseInt(ff_ver[1], 10) < 7) {
        return false;
    }

    return true;
}

function wsConnectTimeout() {
    var val = 30;
    evlog.debug(5, "Setting websocket timeout to " + val + " seconds");
    return val;
}

function handleApplicationMsg(rfb, recipient, msg_id, msg)
{
    evlog.debug(1, "handleApplicationMsg: recipient:" + recipient +
               " msg_id: " + msg_id + " msg: " + msg.substr(0,40) + "...");
    if (msg_id === "open-uri") {
        var uri = msg.trim();
        evlog.debug(1, "openuri('" + uri + "')");
        function openSilent() {
            evlog.debug(5, "openUri: opening as iframe: " + uri);
            $("#ev_openuris").html("<iframe src='" + uri + "'></iframe>");
        }
        function openVisible() {
            evlog.debug(5, "openUri: opening as new window: " + uri);
            window.open(uri);
        }

        var is_http = uri.indexOf("http://") === 0 ||
            uri.indexOf("https://") === 0 ||
            uri.indexOf('/') === 0;
        if (!is_http) {
            // if it's not http, we assume it's some kind of app launch
            evlog.debug(5, "openUri: Non-http protocol");
            openSilent();
            return;
        }
        $.ajax({
            cache: false,
            type: "HEAD",
            url: uri,
            success : function(data, status, xhr) {
                // do we have a Content-Disposition?
                var disp = xhr.getResponseHeader("Content-Disposition");
                if (disp) {
                    if (disp.indexOf("attachment") === 0) {
                        // this should be downloaded
                        evlog.debug(5, "openUri: attachment hint");
                        openSilent();
                    } else {
                        // We want it shown in the browser
                        evlog.debug(5, "openUri: inline hint");
                        openVisible();
                    }
                } else {
                    // otherwise, it should probably be shown in the browser. Probably...
                    evlog.debug(5, "openUri: No hints available");
                    openVisible();
                }
            },
            error: function (rsp, status, error)
            {
                // if HEAD request fails, we don't know what we're dealing with. Show it to the user
                evlog.debug(5, "openUri: HEAD failed");
                openVisible();
            }
        });
    }
}

function mi_encode_int(arr, val)
{
    arr.push(val>>24);
    arr.push(val>>16 & 0xFF);
    arr.push(val>>8 & 0xFF);
    arr.push(val & 0xFF);
}

function mi_encode_string(arr, str)
{
    mi_encode_int(arr, str.length);
    for (var i=0; i <str.length; i++)
        arr.push(str.charCodeAt(i));
}

// Send MIClientIdentification message (code 250)
// See also evncserver2/common/rfb/SMsgReaderMI.cxx
function send_mi_client_identification(websocket, user)
{
    var user_enc = unicode.toUtf8(unicode.fromString(user));
    websocket.send([250, 0,0,0,user_enc.length]);
    websocket.send(user_enc);
    var arr = [];
    mi_encode_int(arr, Math.floor(Math.pow(2,31)*Math.random()));
    websocket.send(arr);
}

// Send MISendApplicationMsg message (code 254)
// See also evncserver2/common/rfb/SMsgReaderMI.cxx
function send_mi_send_application_msg(websocket, activation_msg_id, activation_key)
{
    var data = [254];
    mi_encode_string(data, "evsm");
    mi_encode_string(data, activation_msg_id);
    mi_encode_string(data, activation_key);
    websocket.send(data);
}

function ConnectFunc(context, host, port, muxer_port, secret, tls, preconnect_host) {
    return function(rfb) {
        var session_id = context['session-id'];
        if (muxer_port) {
            evlog.debug(3, "Attempting muxer preconnect against port " + muxer_port + " for display " + port);
            $("#ev_connect_status").html(tr("PRECONNECTING")+"....");
            $.ajax({
                cache: false,
                type: "POST",
                data: " ",
                dataType: "text",
                url: "/vdb/rest/session/" + session_id + "/vnc/preconnect?" +
                    "host=" + preconnect_host + "&displaynum=" + port
                    + "&logsessionid="+evlog.logsessionid(),
                success : function() {
                    evlog.debug(3, "Muxer preconnect successful");
                    rfb.connect(host, muxer_port, secret, 'vnc');
                },
                error: function (rsp, status, error)
                {
                    evlog.error("Muxer preconnect failed");
                    xhr_error_callback(rsp, status, "vncmuxer preconnect: "+error);
                }
            });
        } else {
            Util.Info("Got port " + port);
            evlog.debug(3, "Connecting to port: " + port);
            rfb.connect(host, port, secret, 'vnc');
        }

        enableLogin(false);

        $("#ev_connect_status").html(tr("LAUNCHING_VNC"));
    };
}

function askToLeave(e)
{
    var msg = "Leaving the page will disconnect from the EasyViz session. "
        + "Are you sure you want to leave?";
    (e || window.event).returnValue = msg;
    return msg;
}

function StateHandler(context, connectFunc, secret, confirm_on_close) {
    return function(rfb, state, oldstate, msg) {
        if (state === oldstate) {
            return;
        }

        if (oldstate == 'normal') {
            window.removeEventListener('beforeunload', askToLeave, false);
        }
        switch (state) {
            case 'failed':
                evlog.debug(3, "Entering 'failed' state");
            case 'fatal':
                evlog.debug(3, "Entering 'fatal' state");
                // error
                if (msg == "Server disconnected (code: 1005)") {
                    Util.Info("VNC Client disconnected normally: " + msg);
                    // We do not want to show a code after the user logs out
                    $("#ev_connect_status").text(tr("DISCONNECTED"));
                } else {
                    Util.Error("VNC Client: "+state+" "+msg);
                    $("#ev_connect_status").text(tr("VNC_CLIENT")+": "+msg);
                }
                displayLogin(true);
                break;
            case 'normal': // is connected
                evlog.debug(3, "Entering 'normal' state: VNC is running");
                if (confirm_on_close) {
                    window.addEventListener('beforeunload', askToLeave, false);
                }
                displayLogin(false);
                if (context && context.onConnected) {
                    context.onConnected(rfb, secret);
                }
                // normal
                break;
            case 'disconnected':
                evlog.debug(3, "Disconnected from session");
                displayLogin(true);
                // normal
                break;
            case 'loaded':
                // normal
                break;
            case 'password': // vnc auth requests password
                evlog.warning("VNC Auth requesting password. This should never happen");
                break;
            default:
                evlog.debug(5, "Entering state " + state);
                break;
        }
    };
}

function createRfb(context, qualifiedUser, statehandler, tls) {
    var has_evz = context.activation_msg_id && context.activation_key;
    function custom_send(websocket)
    {
        send_mi_client_identification(websocket, qualifiedUser);
        if (has_evz)
            send_mi_send_application_msg(websocket,
                                         context.activation_msg_id,
                                         "activation_key:" + context.activation_key);
        try {
            var metric = {
                eventType: 'web_system_info',
                timestamp: new Date().getTime(),
                host: window.location.host,
                userAgent: navigator.userAgent
            };
            send_mi_send_application_msg(websocket, "EV_CLIENT_METRICS", JSON.stringify(metric));
        } catch (ex) {
            evlog.error("Could not send system info metric");
        }
    };
    var print_clipboard_copy = false;
    var onClipboard = function(rfb, text) {
        if (print_clipboard_copy) {
            console.log("Server-side clipboard copy: " + text);
        }
    };

    var rfb = new RFB({'target': $('#ev_canvas')[0],
                   'onUpdateState': statehandler,
                   'onClipboard': onClipboard,
                   'onApplicationMsg': handleApplicationMsg,
                   'connectTimeout': wsConnectTimeout(),
                   'default_cursor': 'url("./images/ie_dot.cur"), auto',
                   'encrypt': !!tls,
                   'onPostClientInit': custom_send,
                   'acceptTouch': false
                  });

    window.evwebPaste = function(text) {
        rfb.clipboardPasteFrom(text);
    }
    window.evwebCopyEnabled = function(enabled) {
        print_clipboard_copy = enabled;
    }

    var local_cursor_supported = !!rfb.get_display().get_cursor_uri();
    rfb.set_local_cursor(local_cursor_supported);
    rfb.set_shared(true);
    rfb.set_true_color(true);
    evlog.debug(3, "Creating RFB object. Local cursor status: " + local_cursor_supported);
    return rfb;
}

function VncConnect(confirm_on_close) {
    return function(context, host, port, muxer_port, secret, tls, preconnect_host) {
        if (confirm_on_close) {
            evlog.debug(1, "confirm_on_close is set: User will be prompted for confirmation before closing the window");
        } else {
            evlog.debug(1, "confirm_on_close is not set: User will not be prompted for confirmation before closing the window");
        }
        evlog.debug(1, "Connecting to rendernode. Source page URL is " + window.location.href);
        var user = context.user;

        $("#ev_connect_status").html(tr("LOGGING_IN")+"..");
        Util.Info("vncConnect username="+user+" lang="+context.lang);
        if (!context.lang)
            throw new Error("lang not set in VncConnect");

        var realm = context.realm;
        $.cookie('realm', realm, { expires: 365 });
        $.cookie('lang', context.lang, { expires: 365 });

        $("#ev_connect_status").html(tr("LOGGING_IN")+"..");
        Util.Info("vncConnect username="+user+" lang="+context.lang);

        var connectFunc = ConnectFunc(context, host, port, muxer_port, secret, tls, preconnect_host);
        var stateHandler = StateHandler(context, connectFunc, secret, confirm_on_close);
        var rfb = createRfb(context, user, stateHandler, tls);
        $("#ev_connect_status").html(tr("LAUNCHING_VNC"));
        connectFunc(rfb);
    };
}
