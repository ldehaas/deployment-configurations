<?php
require_once("vars.php");
require_once('gettext.php');

$debug = 0;
$color1 = '#f0f0f0';
$color2 = '#ffffff';
$color_title = '#d1d1ff';
$bgcolor = $color1;
$auditsessiontimeout = 60*30; // 30 min
$source_conf_cmd = "test -f /etc/ev/ev.env && source /etc/ev/ev.env; test -f /etc/ev/auditlog.env &&  source /etc/ev/auditlog.env ";

function read_cmd_output($cmd)
{
  global $debug;

  $handle = popen($cmd, "r");

  $returnarray = array();

  $i = 0;
  while (($data = fgetcsv($handle, 1000, ",")) != FALSE) {
    $returnarray[$i++] = $data;
  }

  fclose($handle);

  if ($debug) {
    print "<pre>";
    var_dump($cmd);
    var_dump($returnarray);
    print "</pre>";
  }

  return $returnarray;
}

function getuserlist()
{
  global $source_conf_cmd, $bin_dir;
  return read_cmd_output("$source_conf_cmd; $bin_dir/vdb_show_users -t");
}

function getgrouplist()
{
  global $source_conf_cmd, $bin_dir;
  return read_cmd_output("$source_conf_cmd; $bin_dir/vdb_show_groups -t");
}

function get_ev_env($v)
{
    global $source_conf_cmd;
    $a = read_cmd_output("$source_conf_cmd; echo \$$v");

    foreach ($a as $key=>$value) {
        return $value[0];
    }
}

function getusermember($account)
{
  global $source_conf_cmd, $bin_dir;
  return read_cmd_output("$source_conf_cmd; $bin_dir/vdb_show_user_membership -t ".ereg_replace("[^ a-zA-Z@0-9\*\._-]", "", $account));
}

class Session {
    var $logged_in, $username, $password, $failed_login, $ldap;

    function Session()
    {
        global $debug, $auditsessiontimeout, $sysconf_dir;

        session_start();
        $this->failed_login = false;
        $this->ldap = false;

        $cmd = "/bin/grep '^EasyViz Administrators=' $sysconf_dir/ev/groups.conf|/bin/cut -d= -f2";
        if ($debug) {
            print "<pre>";
            var_dump($cmd);
            print "</pre>";
        }

        $h = popen($cmd,"r");
        while ($l = fgets($h, 10000)) {
            $this->admingroups[] = trim($l);
        }
        pclose($h);
        if ($debug) {
            print "<pre>";
            var_dump("Admin groups are: (".implode(",", $this->admingroups).")");
            print "</pre>";
        }

        if (count($this->admingroups) == 0) {
            if ($debug) {
                print "<pre>";
                var_dump("Defaulting to 'EasyViz Administrators'");
                print "</pre>";
            }
            $this->admingroups[0] = 'EasyViz Administrators';
        }

        $user_backend = get_ev_env("EV_VDB_USER_BE");

        if (strncmp($user_backend, "LDAP_BE", 7) == 0) {
            $this->ldap = true;
        }

        if ($debug) {
            print "<pre>";
            var_dump("Backend is ".($this->ldap == true) ? "LDAP" : "DB");
            print "</pre>";
        }
    }

    function Logout()
    {
        unset($_SESSION['username']);
        unset($_SESSION['password']);
        unset($_SESSION['timeout']);
    }

    function ldap_login($username, $password)
    {
        global $debug, $auditsessiontimeout, $source_conf_cmd;
        $username = ereg_replace("[^ a-zA-Z@0-9\*\._-]","", $username);
        $password = ereg_replace("[|&]","", $password);

        $username_proc_enc = urlencode($username);
        $password_proc_enc = urlencode($password);

        $cmd = "$source_conf_cmd; " .
          # Strip any password in EV_BACKEND
          # and fill in user's credentials
          'EV_BACKEND=$(echo $EV_BACKEND | sed "s/\/\/.*@/\/\/' . "$username_proc_enc:$password_proc_enc" .'@/g") ' .
          'userservice --is-admin > /dev/null 2>&1';
        system($cmd, $retval);
        if ($retval == 0) {
            $_SESSION['username'] = stripslashes($username);
            $_SESSION['password'] = md5($username);
            $_SESSION['timeout'] = time()+$auditsessiontimeout;
            $this->logged_in = 1;

            if ($debug) {
                print "<pre>";
                var_dump("User is authenticated");
                print "</pre>";
            }

            return true;
        } else {
            if ($debug) {
                print "<pre>";
                var_dump($cmd);
                var_dump("Returncode $retval");
                print "</pre>";
            }
        }

        unset($_SESSION['username']);
        unset($_SESSION['password']);
        unset($_SESSION['timeout']);

        $this->failed_login = true;
        return false;
    }

    function Login($username, $upassword)
    {
        global $auditsessiontimeout;
        $cryptpassword = '';

        $userlist = getuserlist();
        foreach ($userlist as $key=>$value) {
            $id = $value[0];
            $sid = $value[1];
            $account = $value[2];
            $password = $value[3];
            $title = $value[4];
            $fullname = $value[5];

            if ($account == $username) {
                $cryptpassword = $password;
                break;
            }
        }

        if ($cryptpassword == '') {
            unset($_SESSION['username']);
            unset($_SESSION['password']);
            unset($_SESSION['timeout']);
            $this->failed_login = true;
            exit(_("Invalid username or password. Please try again or contact your administrator for assistance."));
        }

        $cryptpassword = trim($cryptpassword);
        $salt = '';

        if (substr($cryptpassword, 0, 3) == '$1$') {
            // MD5
            $salt = substr($cryptpassword, 0, 12);
        } elseif (substr($cryptpassword, 0, 4) == '$2a$') {
            // Blowfish
            $salt = substr($cryptpassword, 0, strrpos($cryptpassword, '$'));
        } else {
            // Default to DES (Not extented DES)
            $salt = substr($cryptpassword, 0, 2);
        }

        $newp = crypt($upassword, $salt);
        if ($cryptpassword == $newp) {

            $list = getusermember($account);

            $i = 0;
            $inadmingroup = false;
            foreach ($list as $key=>$value) {
                $group_name = $value[0];
                if (in_array($group_name, $this->admingroups)) {
                    $inadmingroup = true;
                }
            }

            if ($inadmingroup == true) {
                $_SESSION['username'] = stripslashes($username);
                $_SESSION['password'] = md5($cryptpassword);
                $_SESSION['timeout'] = time()+$auditsessiontimeout;
            } else {
                $this->failed_login = true;
                unset($_SESSION['username']);
                unset($_SESSION['password']);
                unset($_SESSION['timeout']);
            }
        } else {
            $this->failed_login = true;
            unset($_SESSION['username']);
            unset($_SESSION['password']);
            unset($_SESSION['timeout']);
        }
    }

    function Authenticate()
    {
        global $auditsessiontimeout;

        if (isset($_SESSION['timeout'])) {
            if ($_SESSION['timeout']<time()) {
                // Session expired
                return false;
            }
        }

        if ($this->ldap == false) {
            if (!isset($_SESSION['username'])||!isset($_SESSION['password'])) {
                $this->logged_in = 0;
                return false;
            } else {
                if (!get_magic_quotes_gpc()) {
                    $_SESSION['username'] = addslashes($_SESSION['username']);
                }

                $cryptpassword = '';
                $userlist = getuserlist();
                foreach ($userlist as $key=>$value) {
                    $id = $value[0];
                    $sid = $value[1];
                    $account = $value[2];
                    $password = $value[3];
                    $title = $value[4];
                    $fullname = $value[5];

                    if ($account == $_SESSION['username']) {
                        $cryptpassword = $password;
                        break;
                    }
                }

                if ($cryptpassword == '') {
                    unset($_SESSION['username']);
                    unset($_SESSION['password']);
                    unset($_SESSION['timeout']);
                    exit("Unable to verify login");
                }

                $dbpassword = md5(trim($cryptpassword));

                $_SESSION['password'] = stripslashes($_SESSION['password']);

                if ($dbpassword == $_SESSION['password']) {
                    $_SESSION['timeout'] = time()+$auditsessiontimeout;
                    $logged_in = 1;
                    return true;
                } else {
                    $logged_in = 0;
                    unset($_SESSION['username']);
                    unset($_SESSION['password']);
                    unset($_SESSION['sessionview']);
                    unset($_SESSION['timeout']);
                }
                return false;
            }
        } else {
            if (!isset($_SESSION['username']) || !isset($_SESSION['password'])) {
                $this->logged_in = 0;
                return false;
            } else {
                if (!get_magic_quotes_gpc()) {
                    $_SESSION['username'] = addslashes($_SESSION['username']);
                }

                if (md5($_SESSION['username']) == $_SESSION['password']) {
                    $_SESSION['timeout'] = time() + $auditsessiontimeout;
                    return true;
                }

                return false;
            }
            // LDAP
        }
    }

    function htmllogin()
    {
        global $color_title;
        $html = '<html><body>
<form action="'.$_SERVER["PHP_SELF"].'" method="post">
<table bgcolor="#ffffff" border="0" cellspacing="0" cellpadding="0" width="100%">
  <tr><td align="center">
  <small>Login required</small>
  <table bgcolor="#ffffff" border="0" width="350">
    <tr>
      <td bgcolor="'.$color_title.'" align="center"><b>Login</b></td>
    </tr>
    <tr>
      <td bgcolor="#ffffff" align="left">
        <table bgcolor="#ffffff" align="center" border="0" width="100%">
          <tr>
            <td align="right" width="30%">'._("Username").':</td>
            <td align="left" width="*"><input type="text" name="login_username" value="" /></td>
          </tr>
          <tr>
            <td align="right" width="30%">'._("Password").':</td>
            <td align="left" width="*">
              <input type="password" name="secretkey" />
            </td>
          </tr>
        </table>
      </td>
    </tr>
    <tr>
      <td align="left"><center><input type="submit" name="Login" value="'._("Login").'" /></center></td>
    </tr>';

        if ($this->failed_login) {
            $html .= "<tr><td><center><b>"._("Login failed!")."</b></center></td></tr>";
        }
        $html .= '</table></center></td>
</tr>
</table>
</form>
</body>
</html>';
        return $html;
    }
}
?>
