var EV_LOG_INTERVAL = 200;

var evlog = {
    debug : function() {},
    warning : function() {},
    error : function() {}
};

function logsession() {
    "use strict";
    function pad(n, l) {
        var s = String(n);
        var padding = "00000000";
        return padding.substring(0, l - s.length) + s;
    }
    var d = new Date();
    var datestring = pad(d.getFullYear(), 4) + pad((d.getMonth()+1), 2) + pad(d.getDate(), 2) +
        pad(d.getHours(), 2) + pad(d.getMinutes(), 2) + pad(d.getSeconds(), 2);
    var rand = Math.floor(Math.random() * 1000000);
    return datestring + rand;
}

function Log() {
    "use strict";

    var sessionid = logsession();
    var log_url = '/vdb/rest/log/' + sessionid;
    var loglevel = null;
    var pending = [];

    function query() {
        return '?client_name=browser&log_section=Web';
    }

    function addMessage(obj) {
        if (!obj.filename) {
            obj.filename = "";
        }
        if (!obj.line) {
            obj.line = 0;
        }
        obj.timestamp = new Date().getTime();
        pending.push(obj);
    }

    var logMessage = null;
    var flush;
    // checks if there are pending log messages to send to server
    // if so, send them (after a moment's pause)
    // if not, make logMessage() flush the next time it is called
    function onSent() {
        setTimeout(function() {
            if (pending.length === 0) {
                // If nothing to log, flush the next time a message is added
                logMessage = function(obj) {
                    addMessage(obj);
                    flush();
                };
            }
            else {
                // if queued messages, flush them immediately
                flush();
            }
        }, EV_LOG_INTERVAL);
    }

    function initLog() {
        var url = log_url + query();
        $.ajax(url, {
            cache : false,
            contentType : 'application/json; charset=utf-8',
            data : "dummy",
            type : "PUT",
            error : function() {
                // retry
                setTimeout(initLog, 5000);
            },
            success : function(data, status, rsp) {
                loglevel = JSON.parse(data).debuglevel;
                // once we've initialized, please send this as well
                if (document.docMode) {
                    evlog.debug(0, "Document mode (IE is rendering as version): " + document.documentMode);
                }
                else {
                    evlog.debug(7, "document.documentMode not set");
                }
                if (document.compatMode) {
                    evlog.debug(0, "Compatibility mode: " + document.compatMode + (document.compatMode == "CSS1Compat" ? "(standards)" : "(quirks)"));
                }
                else {
                    evlog.debug(7, "document.compatMode not set");
                }

                function getClientSize() {
                    if (window.innerHeight) {
                        return window.innerWidth + "x" + window.innerHeight;
                    }
                    if (document.body.clientHeight) {
                        return document.body.clientWidth + "x" + document.body.clientHeight;
                    }
                    if (document.documentElement.clientHeight) {
                        return document.documentElement.clientWidth + "x" + document.documentElement.clientHeight;
                    }
                    return "<unknown>";
                }
                evlog.debug(0, "Screen resolution: " + window.screen.width + "x" + window.screen.height);
                evlog.debug(0, "Screen color depth: " + window.screen.pixelDepth);
                evlog.debug(0, "Client window area: " + getClientSize());
                onSent();
            }
        });
    }

    flush = function() {
        logMessage = addMessage;

        var toSend = pending;
        pending = [];
        var url = log_url + query();
        $.ajax(url, {
            cache : false,
            contentType : 'application/json; charset=utf-8',
            data : JSON.stringify(toSend),
            type : "POST",
            error : function(xhr, status, errormsg) {
                evlog.debug(0, "Could not send " + toSend.length + " log messages. Status code: " + status + ", error message: " + errormsg);
            },
            success : onSent
        });
    };

    function lazyInit(obj) {
        // the next logMessage call should just push onto the queue
        logMessage = addMessage;

        // push the message we just got onto the queue
        addMessage(obj);
        // initialize log so we can start sending
        initLog();
    }

    // first log message should initialize
    logMessage = lazyInit;

    var log = {
        debug: function(level, msg) {
            if (level > loglevel && loglevel !== null) { return; }
            if (console && console.log) { console.log(msg); }
            logMessage({level : level, type : 'debug', message : msg});
        },
        warning: function(msg) {
            if (console && console.warn) { console.log(msg); }
            logMessage({type : 'warning', message : msg});
        },
        info: function(msg) {
            if (console && console.info) { console.info(msg); }
            logMessage({type : 'info', message : msg});
        },
        error: function(msg) {
            if (console && console.error) { console.log(msg); }
            logMessage({type : 'error', message : msg});
        },
        initialized : true,
        reset : function() {
            flush();
            sessionid = logsession();
            logMessage = lazyInit;
        },
        debuglevel : function() { return loglevel; },
        logsessionid : function() { return sessionid; }
    };

    window.onerror = function(errmsg, url, line) {
        logMessage({type : 'error', message : errmsg, filename : url, line : line});
    };
    return log;
}
