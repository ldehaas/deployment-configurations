// Use this for translating texts. It requires a global g_tr object.
function tr(key)
{
    if (typeof(g_tr) === "undefined") {
        $("#ev_connect_status").text("no translations (g_tr)");
        $("#ev_login_screen").append("<h1>No translations</h1>");
    }
    if (key in g_tr)
        return g_tr[key];
    return "[TRANSLATION] "+key;
}

function getHost() {
    var host = document.location.host;
    var p = host.indexOf(':');
    if (p !== -1) {
        host = host.substr(0, p);
    }
    return host;
}

// Use it when setting session_mode in the vncreq call
SessionMode = {
    Create: "CREATE",
    Resume: "RESUME",
    Join: "JOIN"
};

// if we're not logged in, this does nothing
var endSession = function(){};

// once we're logged in, endSession points to this
function endActualSession() {
    try {
        if (evlog.initialized) {
            evlog.debug(9, "Closing browser-side session");
            evlog.reset();
        }
    } catch(ex) {}
    document.cookie = "ev-session=deleted; expires=Thu, 01 Jan 1970 00:00:01 GMT;";
    endSession = function(){};
}

function displayLogin(visible) {
    if (!visible) {
        $('#ev_password')[0].value = "";
    }
    $('#ev_login_screen').css("display", visible ? "block" : "none");
    $('#ev_screen').css("display", visible ? "none" : "block");
    enableLogin(visible);
    if (visible) {
        $('#ev_login_form').show();
    }
}

function enableLogin(enabled) {
    if (enabled) {
        $('#ev_password')[0].value = "";
        endSession();
    }
    $('#ev_login_form input').each(function(idx, node) { node.disabled = !enabled; });
}

function generic_error_callback(text)
{
    evlog.error("Generic error handler: " + text);
    // setTimeout is needed when an exception is thrown directly in vdbLogin
    // (also it gives a good visual effect)
    setTimeout(function () { enableLogin(true); }, 700);
    $("#ev_connect_status").text(text);
}

function xhr_error_callback(rsp, status, error)
{
    evlog.error("AJAX call failed: " + status + " " + error + ": " + rsp.responseText);
    enableLogin(true);
    // We use 412 as an alternative "401 Not authorized" to avoid login dialog from browser
    if (rsp.status == 412)
        $("#ev_connect_status").text(tr("INCORRECT_USERNAME_OR_PASSWORD"));
    else
        $("#ev_connect_status").text(error+" ("+tr("CODE")+" "+rsp.status+")");
}

// Read statistics from the RFB object and append them as text in data
// log format to the div element ev_performance_result.
function dataLog(time_step, rfb, secret, begin_time)
{
    if (document.title == "done") // test has finished
        return;

    var date = new Date();
    var text = "DATA name=perf, t=("+(Math.floor(date.getTime()/1000))+";"+(1000*date.getMilliseconds())+")";
    var jpg_stats = rfb.get_stats("TIGHT");
    var png_stats = rfb.get_stats("TIGHT_PNG");
    var time_drift = date.getTime() - begin_time - time_step*2000;

    text += ", jpg_rects=" + jpg_stats[0] +", jpg_bytes=" + jpg_stats[2];
    text += ", png_rects=" + png_stats[0] +", png_bytes=" + png_stats[2];
    text += ", time_drift="+time_drift;

    // Append to element
    $("#ev_performance_result").html($("#ev_performance_result").text() + "\n"+ text);

    time_step++;
    // Calculate how long to wait to compensate for time drifting
    var delay = time_step*2*1000 - (date.getTime() - begin_time);
    //console.log("step",time_step, "drift",time_drift,"delay", delay);

    setTimeout(function() { dataLog(time_step, rfb, secret, begin_time); }, delay);
}

function is_secure() {
    return document.location.protocol === "https:";
}

function base64_encode_string(str)
{
    var chars = [];
    for (var i= 0; i<str.length; i++)
        chars.push(str.charCodeAt(i));

    return Base64.encode(chars);
}

function getPreferredLangs()
{
    var langs = lang.split(",");
    return langs.map(function(l) { return l.split(";")[0]});
}

function indexOfMatchingLangOption(lang, elem)
{
    var options = Array.prototype.map.call(elem.children, function(child) {
        return child.value;
    });
    for (var idx in options) {
        if (options[idx].indexOf(lang) === 0) {
            return idx;
        }
    }
    return -1;
}

function getDefaultLangIndex(elem) {
    // If a lang is specified in the URL, that takes precedence
    var url_lang = WebUtil.getQueryVar("url_lang");
    if (url_lang !== null && url_lang !== "") {
        var idx = indexOfMatchingLangOption(url_lang, elem);
        if (idx !== -1) {
            evlog.debug(3, "Defaulting to language " + url_lang + " (" + idx + ") selected from URL Query param");
            return idx;
        }
    }

    // is the cookie set?
    var cookie_lang = $.cookie('lang');
    if (cookie_lang) {
        var idx = indexOfMatchingLangOption(cookie_lang, elem);
        if (idx !== -1) {
            evlog.debug(3, "Defaulting to language " + cookie_lang + " (" + idx + ") selected from cookie");
            return idx;
        }
    }

    // finally, run through the languages the browser specifies in the Accept-Language header
    var preferred = getPreferredLangs();
    for (var idx in preferred) {
        var browser_lang = preferred[idx];
        var idx = indexOfMatchingLangOption(browser_lang, elem);
        if (idx !== -1) {
            evlog.debug(3, "Defaulting to language " + browser_lang + " (" + idx + ") selected from browser locale");
            return idx;
        }
    }
    var idx = indexOfMatchingLangOption('en', elem);
    if (idx !== -1) {
        evlog.debug(3, "Defaulting to language en (" + idx + ") as a fallback");
        return idx;
    }
    evlog.debug(3, "Could not select a language");
    return 0;
}
