<?php

/*
*  This is the main integration script that accepts parameters from the caller. These parameters
*  are checked and any missing ones, such as IPID, as added. Further more, the userid passed in 
*  (this is the authzname parameter) is passed to the getUserDomain function to determine
*  what domain the user is a member of. User ids can be a member of a number of different domains
*  and inorder for this script to work we need the domain, the caller of this script (Epic) is
*  unable to pass us the domain hence we have to figure it out. The getUserDomain function uses
*  the easyviz userservice command against each of the known domains until it finds a match for 
*  the provided userid. Take note; as new domains are added to EV you will need to add that domains
*  service userid/password to the $domains variable below.
*
*
*/

        // this is an array of userid's used for the userservice command. Add new domains here... 
	$domains = array("svc.easyvizadmin:wXnJ7c_NfuzX", 
                         "easyvizsvc:C2JM5sWa8CxgOLfVRFeZ3p4w65k65r",
                         "stphvitaltest:Vit%40l123%21",
			 "tst.ezvuser2:HelloNurse1%21",
			 "vital-rad-test:P%40%24%24word2468");
        // assign hostname to a variable
        $hostName = gethostname();
        // create curl resource
        $ch = curl_init();
        // build url for the restful call
        $builtUrl = "https://" . $hostName . "/vdb/rest/evz?";
        $url = getURLParams();
        $parts = parse_url($url);
        $foo = parse_str($parts["query"], $parsed);

        /* this are options parms that may have been passed in */
        if (isset($parsed['accession_number'])) {
           $accessionNumber = $parsed["accession_number"];
        } else {
           $accessionNumber = "";
        }
        if (isset($parsed['output_type'])) {
           $outputType = $parsed["output_type"];
        } else {
           $outputType = "";
        }
        if (isset($parsed['msg_id'])) {
           $msgId = $parsed["msg_id"];
        } else {
           $msgId = "";
        }
        if (isset($parsed['patient_id'])) {
           $patientId = $parsed["patient_id"];
           $issuerOfPID = "OHSEPIC";
        } else {
           $patientId = "";
        }

        /* these are required parms that need to be passed in */
        if (isset($parsed['username'])) {
           $username = $parsed["username"];
        } else {
           errorMsg("required parameter username not found");
        }
        if (isset($parsed['password'])) {
           $password = $parsed["password"];
        } else {
           errorMsg("required parameter password not found");
        }
        if (isset($parsed['authzname'])) {
           $authzname = $parsed["authzname"];
        } else {
           errorMsg("required parameter authzname not found");
        }
        $domain = getUserDomain($authzname);
        if ($domain == '') {
                 errorMsg("Unknown userid (" . $authzname . ") provided. Unable to connect to EasyViz");
                 exit;
        }
        if ($outputType == '') {
                $outputType = "web";
        }
        if ($msgId == '') {
                $msgId = "EV_WORKLIST_SHOW";
        }

        $username = $username . $domain;
        $builtUrl .= "username=" . $username;
        $builtUrl .= "&password=" . $password;
        $builtUrl .= "&authzuser=" . $authzname;
        $builtUrl .= "&output_type=" . $outputType;
        $builtUrl .= "&msg_id=" . $msgId;
        if ($patientId != '') {
           $builtUrl .= "&patient_id=" . $patientId;
           $builtUrl .= "&patient_id_issuer=" . $issuerOfPID;
        }
        if ($accessionNumber != '') {
           $builtUrl .= "&accession_number=" . $accessionNumber;
        }
//echo($builtUrl);
//echo("<br>");
//exit;
        // set url
        curl_setopt($ch, CURLOPT_URL, $builtUrl);
        //return the transfer as a string
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
        // set the UA
        curl_setopt($ch, CURLOPT_USERAGENT, 'EasyViz Restful Launcher');
        // Alternatively, lie, and pretend to be a browser
        // curl_setopt($ch, CURLOPT_USERAGENT, 'Mozilla/4.0 (compatible; MSIE 7.0; Windows NT 5.1)');
        // $output contains the output string
        $output = curl_exec($ch);
        // close curl resource to free up system resources
        curl_close($ch);
        //redirect to where the restful service responded with.
        //usleep(2000000);
// echo($output);
// echo("<br>");
        header("Location: " . $output);
// echo("<a href='".$output."'>here</a>");
// echo("<br>");
// echo($output);


        /*
        *  This function will output a html error message to the user if the parameters send in are missing or invalid
        */
        function errorMsg($message) {
           echo("<b>Invalid URL: " . $message . "</b>");
           echo("<br>");
           exit;
        }

        /*
        * this function will obtain the parameters from the URL and return them
        *
        */
        function getURLParams() {
                $temp = $_SERVER["REQUEST_URI"];
                return strstr($temp, '?');
        }

        /*
        * this function figures out what domain the userid is a member of,
        * each known domain is checked and the first one that has an entry for the userid
        * is returned. The EV userservice command is used and connects to the various domains
        * based on the environment variable EV_BACKEND. We need to reassign this environment
        * variable for each domain.
        */
        function getUserDomain($userid) {
                global $domains, $hostName;
		$atSign = '@';
		$evBackend_pre = "EV_BACKEND=http://";
		$evBackend_suffix = $atSign . $hostName . "/vdb";
		$evBackend_export = "";
		$domainName = "";

		foreach ($domains as $login) {
		    $evBackend_export = $evBackend_pre . $login . $evBackend_suffix;
		    putenv($evBackend_export);
		    exec("userservice -u " . $userid . " 2> /tmp/php.txt2", $output);
		    if (count($output) > 0) {
		       $pos = strpos($output[1], $atSign);
		       $domainName = substr($output[1], strpos($output[1], $atSign));
		       break;
		    }
		    unset($output);
		}

		return $domainName;

        }
?>
</body>
</html>
