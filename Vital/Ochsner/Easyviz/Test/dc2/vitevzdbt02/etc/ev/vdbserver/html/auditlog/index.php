<?php
require_once("vars.php");
require_once("env.php");
require_once("auditsession.php");
$session = New Session();
$onloadAction = "";
$vscroll = "";
$CURRENT_TIMEZONE = date("Z") / 3600;

if (isset($_REQUEST['Logout'])) {
    $session->Logout();
}

if (isset($_REQUEST['login_username']) && $_REQUEST['login_username'] != '') {
    if ($session->ldap == true) {
        $session->ldap_login($_REQUEST['login_username'], $_REQUEST['secretkey']);
    } else {
        $session->Login($_REQUEST['login_username'], $_REQUEST['secretkey']);
    }
}

if ($session->Authenticate() == false) {
    print $session->htmllogin();
    exit(1);
}

require_once('gettext.php');
?>
<html>
<head>
<title><?php echo dgettext("site", "EasyViz") . " " . _("Audit Message Repository"); ?></title>
<link rel="stylesheet" href="style.css">
<link rel="stylesheet" href="calstyle.css">
</head>
<?php
$startTime = (float)array_sum(explode(' ', microtime()));
require('ihedomains.php');
require('tooltips.php');
require('auditlogused.php');

if (isset($_GET['verticalScroll'])) {
    $vscroll = $_GET['verticalScroll'];
    $onloadAction = " onload='window.scrollTo(0, $vscroll);' ";
}

function dropdown($label, $fieldname, $domain, $tooltip = null, $default = '')
{
    $val = isset($_GET[$fieldname]) ? $_GET[$fieldname] : $default;
    if ($tooltip)
        $retval = "<td><span onmouseover='return escape(\"$tooltip\");'>$label:</span></td>";
    else
        $retval = "<td>$label:</td>";

    $retval = $retval .
        "<td>" .
        "<select single name='$fieldname' STYLE='width: 257'>";

    foreach ($domain as $key => $value) {
        $selected = $val == "".$key ? 'selected' : '';
        $retval = $retval . "<OPTION $selected value='$key'>$value</OPTION>";
    }

    return $retval . "</select></td>";
}

function textbox($label, $fieldname, $tooltip = null)
{
    $val = isset($_GET[$fieldname]) ? $_GET[$fieldname] : '';
    if ($tooltip)
        $retval = "<td><span onmouseover='return escape(\"$tooltip\");'>$label:</span></td>";
    else
        $retval = "<td>$label:</td>";

    return $retval .
        "<td><input type='text' name='$fieldname' value='$val' style='width: 257'></td>";
}

function columnHeader($columnId, $columnName, $sortColumn, $desc)
{
    $si = "";
    if ($sortColumn == $columnId) {
        $si = "<img src='images/".($desc ? "down" : "up")."arrow.gif'/>";
    }
    return "<td style='cursor: pointer; cursor: hand' onclick=\"sortOn('$columnId');\">$columnName $si</td>";
}

function navLink($linkName, $offset, $current)
{
    return $current ?
        "<font color='red'>$linkName</font>" :
        "<a href='javascript:jumpTo($offset)'>$linkName</a>";
}

function navList($recordCount, $offset, $limit)
{
    $s = "";
    if ($offset > 0)
        $s = $s . navLink("&laquo;", max($offset-$limit, 0), false);

    $c = 1;
    $i = 1;
    while ($i < $recordCount) {
        $s = $s . " " . navLink($c, $i, $i >= $offset && $i < $offset + $limit);
        $c++;
        $i += $limit;
    }

    if ($offset + $limit < $recordCount - 1)
        $s = $s . " ". navLink("&raquo;", $offset + $limit, false);
    return $s;
}

class WhereClauseBuilder {
    function addClause($s)
    {
        $this->str = $this->str . (strlen($this->str) > 0 ? " and " : " ") . $s;
    }

    function addBasicClause($fieldname, $val)
    {
        if ($val != '')
            $this->addClause("$fieldname = '$val'");
    }

    function addBasicUnquotedClause($fieldname, $val)
    {
        if ($val != '')
            $this->addClause("$fieldname = $val");
    }

    function addBasicLikeClause($fieldname, $val)
    {
        if ($val != '')
            $this->addClause("$fieldname like '%%$val%%'");
    }

    function addCodedValueTypeClause($codingSchemeFieldname,
                                     $codeFieldname,
                                     $val)
    {
        $v = explode(",", $val);
        if (count($v) == 2)
            $this->addClause("$codingSchemeFieldname = '$v[0]' and " .
                             "$codeFieldname = '$v[1]'");
    }

    function getWhereClause()
    {
        return $this->str == '' ? '' : ("where ".$this->str);
    }

    var $str = "";
};

function fetchColumn($dbconn, $columnId, $selectStatement)
{
    $rs = odbc_exec($dbconn, $selectStatement);
    $out = array();
    while ($rs && odbc_fetch_row($rs))
        array_push($out, odbc_result($rs, $columnId));
    return $out;
}
?>
<script language="JavaScript" src="js_extras.js"></script>
<script language="JavaScript" src="url.js"></script>
<script language="JavaScript" src="CalendarPopup.js"></script>
<script language="JavaScript" src="datetimevalidation.js"></script>
<script language="javascript">

function sortOn(column)
{
    var url = new URL(document.URL);
    url.arguments['verticalScroll'] = document.body.scrollTop; // Doesn't work in IE: window.pageYOffset;

    if (url.arguments['orderby'] == column)
        url.arguments['desc'] = url.arguments['desc'] == "true" ? "false" : "true";

    url.arguments['orderby']=column;
    window.location.href = url.toString();
//    searchForm.verticalScroll.value = window.pageYOffset;
//    if (searchForm.orderby.value == column) {
//        searchForm.desc.value = searchForm.desc.value == "true" ? "false" : "true";
//    }
//    searchForm.orderby.value = column;
//    searchForm.submit();
}

function jumpTo(offset)
{
    var url = new URL(document.URL);
    url.arguments['verticalScroll'] = window.pageYOffset;
    url.arguments['offset'] = offset;
    window.location.href = url.toString();
//    searchForm.offset.value = offset;
//    searchForm.submit();
}

function submitSearchForm()
{
    if (checkDateTimeField(document.all.event_datetime_begin) &&
        checkDateTimeField(document.all.event_datetime_end)) {
        document.all.event_datetime_begin_db2.value =
            toDb2Timestampfrom(document.all.event_datetime_begin.value);
        document.all.event_datetime_end_db2.value =
            toDb2Timestampto(document.all.event_datetime_end.value);
        searchForm.submit();
    }
}

var cal = new CalendarPopup("calendar_div");
cal.setCssPrefix("TEST");
</script>
<body <?php echo $onloadAction ?> >
<form method="GET" action="index.php" id='searchForm'>
  <table align="center" width="100%">
    <tr style="background:#CCCCCC">
      <td align="center"><span class="pagetitle"><?php echo dgettext("site", "EasyViz") . " " . _("Audit Message Repository");?></span></td>
      <td align="right"><input type=submit name="Logout" value="<?php echo _("Logout"); ?>"></td>
    </tr>
  </table><br>
  <table align="center" rules="groups" border=0>
    <tr>
      <td colspan="2"><span onmouseover='return escape("<?php echo $EVENT_TOOLTIP?>");'><?php echo _("Event"); ?>:</span></td>
      <td width="40"></td>
      <td><span onmouseover='return escape("<?php echo $ACTIVE_PARTICIPANTS_TOOLTIP?>");'><?php echo _("Active Participants:"); ?></span></td>
    </tr>
    <tr>
      <?php echo dropdown(_('Event ID'), 'event_id_code_cvt', $AUDIT_EVENT_ID_DOMAIN, $AUDIT_EVENT_ID_TOOLTIP) ?>
      <td/>
      <?php echo dropdown(_('Role'), 'ap_role_id_cvt', $ACTIVE_PARTICIPANT_ROLE_ID_CODE_DOMAIN, $ACTIVE_PARTICIPANT_ROLE_ID_CODE_TOOLTIP) ?>
    </tr>

    <tr>
      <?php echo dropdown(_('Event Type'), 'event_typecode_code_cvt', $AUDIT_EVENT_TYPE_CODE_DOMAIN, $AUDIT_EVENT_TYPE_CODE_TOOLTIP) ?>
      <td/>
      <?php echo textbox(_('ID'), 'ap_user_id', $AP_USER_ID_TOOLTIP) ?>
    </tr>

    <tr>
      <?php echo dropdown(_('Action'), 'event_action_code', $AUDIT_ACTION_CODE_DOMAIN, $AUDIT_ACTION_CODE_TOOLTIP) ?>
      <td/>
      <?php echo textbox(_('Alternate ID'), 'ap_alternative_id', $AP_ALTERNATIVE_ID_TOOLTIP) ?>
    </tr>

    <tr>
      <?php echo dropdown(_('Result'), 'event_outcome', $AUDIT_EVENT_OUTCOME_DOMAIN, $AUDIT_EVENT_OUTCOME_TOOLTIP) ?>
      <td/>
      <?php echo textbox(_('User Name'), 'ap_user_name', $AP_USER_NAME_TOOLTIP) ?>
    </tr>

    <tr>
      <td><span onmouseover='return escape("<?php echo $EVENT_DATETIME_TOOLTIP?>");'><?php echo _("Datetime:"); ?></span></td>
      <td>
        <table width="100%"><tr><td>
<input type="text"
                             style='width: 103'
                             name="event_datetime_begin"
                             id="event_datetime_begin"
                             value="<?php echo isset($_GET["event_datetime_begin"]) ? $_GET["event_datetime_begin"] : '' ; ?>"><img src="images/cal.gif" style='cursor: pointer; cursor: hand' onClick="cal.select(document.forms[0].event_datetime_begin,'event_datetime_begin_anchor','dd-MM-yyyy'); return false;" TITLE="cal.select(document.forms[0].event_datetime_begin,'event_datetime_begin_anchor','dd-MM-yyyy'); return false;" NAME="event_datetime_begin_anchor" ID="event_datetime_begin_anchor"></td><td align="center">-</td><td align="right"><input type="text"
                             style='width: 103'
                             name="event_datetime_end"
                             id="event_datetime_end"
                             value="<?php echo isset($_GET["event_datetime_end"]) ?  $_GET["event_datetime_end"] : ''; ?>"><img src="images/cal.gif" style='cursor: pointer; cursor: hand' onClick="cal.select(document.forms[0].event_datetime_end,'event_datetime_end_anchor','dd-MM-yyyy'); return false;" TITLE="cal.select(document.forms[0].event_datetime_end,'event_datetime_end_anchor','dd-MM-yyyy'); return false;" NAME="event_datetime_end_anchor" ID="event_datetime_end_anchor"></td>
        </table>
        <input type="hidden" name="event_datetime_begin_db2" id="event_datetime_begin_db2"/>
        <input type="hidden" name="event_datetime_end_db2"   id="event_datetime_end_db2"/>
     </td>
      <td/>
    </tr>

    <tr>
      <?php echo textbox(_('Audit Source'), 'audit_src_id', $AUDIT_SRC_ID_TOOLTIP) ?>
    </tr>

    <tr>
      <td colspan="2"><span onmouseover='return escape("<?php echo $XML_TOOLTIP?>");'><?php echo _("Free text:"); ?></span></td>
      <td/>
      <td><span onmouseover='return escape("<?php echo $PARTICIPANT_OBJECTS_TOOLTIP?>");'><?php echo _("Participant Objects:"); ?></span></td>
    </tr>

    <tr>
      <td colspan="2" align="right"><input type="text" name="xml" STYLE='width: 310'
                             value="<?php echo isset($_GET["xml"]) ? $_GET["xml"] : ''; ?>"></td>
      <td/>
      <?php echo dropdown(_('ID Type'), 'po_oid_typecode_cvt', $PARTICIPANT_OBJECT_ID_TYPE_CODE_DOMAIN, $PARTICIPANT_OBJECT_ID_TYPE_CODE_TOOLTIP) ?>
    </tr>

    <tr>
      <td colspan="3"/>
      <?php echo textbox(_('ID'), 'po_oid', $PO_OID_TOOLTIP) ?>
    </tr>

    <tr>
      <?php echo dropdown(_('Time Zone'), 'timezone', $TIMEZONE, $TIMEZONE_TOOLTIP, $CURRENT_TIMEZONE) ?>
      <td/>
      <?php echo dropdown(_('Type'), 'po_typecode', $PARTICIPANT_OBJECT_TYPE_CODE_DOMAIN, $PARTICIPANT_OBJECT_TYPE_CODE_TOOLTIP) ?>
    </tr>

    <tr>
      <td colspan="3"/>
      <?php echo  dropdown(_('Role Type'), 'po_typecoderole', $PARTICIPANT_OBJECT_TYPE_CODE_ROLE_DOMAIN, $PARTICIPANT_OBJECT_TYPE_CODE_ROLE_TOOLTIP) ?>
    </tr>

    <tr>
      <td colspan="3" style="font-size:9px"><?php echo _("Note: Hover mouse cursor above labels for additional information (tool tip) "); ?></td>
      <td colspan="2" align="right">
        <input type="button" onclick="window.location.href='index.php';" value="<?php echo _("Clear"); ?>">
        <input type="button" onclick="submitSearchForm();" value="<?php echo _("Find"); ?>">
      </td>
    </tr>
  </table>
  <input id="desc" type="hidden" name="desc"
                       value="<?php echo isset($_GET["desc"]) ? $_GET["desc"] : ''  ; ?>"/>
  <input type="hidden" name="orderby"
                       value="<?php echo isset($_GET["orderby"]) ? $_GET["orderby"] : ''; ?>"/>
  <input id="offset" type="hidden" name="offset"/>
  <input id="verticalScroll" type="hidden" name="verticalScroll"/>
</form>
<div id="calendar_div" style="position:absolute;visibility:hidden;background-color:white;layer-background-color:white;"></DIV>
<script language="JavaScript" type="text/javascript" src="wz_tooltip.js"></script><?php
$offset = isset($_GET["offset"]) ? $_GET["offset"] : '';
$desc = "";
if (isset($_GET["desc"]) && $_GET["desc"] == "true")
    $desc = "desc";
if (!$offset)
    $offset = 1;
$limit = 25;
$orderby_column = (isset($_GET["orderby"]) && $_GET["orderby"] != "") ? $_GET["orderby"] : "event_datetime";
$timezone = isset($_GET["timezone"]) ? $_GET["timezone"] : '';
$builder = new WhereClauseBuilder;

$builder->addCodedValueTypeClause('event_id_coding_scheme',
                                  'event_id_code',
                                  isset($_GET['event_id_code_cvt']) ? $_GET['event_id_code_cvt'] : '' );

$builder->addCodedValueTypeClause('event_typecode_cs',
                                  'event_typecode_code',
                                  isset($_GET['event_typecode_code_cvt']) ? $_GET['event_typecode_code_cvt'] : '');

$builder->addBasicClause('event_action_code',
                         isset($_GET['event_action_code']) ? $_GET['event_action_code'] : '');

$builder->addBasicUnquotedClause('event_outcome',
                                 isset($_GET['event_outcome']) ? $_GET['event_outcome'] : '');

if (isset($_GET['event_datetime_begin_db2']) && $_GET['event_datetime_begin_db2']  != '')
    $builder->addClause("event_datetime >= '" . $_GET['event_datetime_begin_db2'] . "'" );
if (isset($_GET['event_datetime_end_db2']) && $_GET['event_datetime_end_db2'] != '')
    $builder->addClause("event_datetime <= '" . $_GET['event_datetime_end_db2'] . "'" );

$builder->addBasicLikeClause('audit_src_id',
                             isset($_GET['audit_src_id']) ? $_GET['audit_src_id'] : '' );

$builder->addCodedValueTypeClause('role_id_coding_scheme',
                                  'role_id_code',
                                  isset($_GET['ap_role_id_cvt']) ? $_GET['ap_role_id_cvt'] :'' );

$builder->addBasicLikeClause('user_id', isset($_GET['ap_user_id']) ? $_GET['ap_user_id'] : '' );

$builder->addBasicLikeClause('alternative_id', isset($_GET['ap_alternative_id']) ? $_GET['ap_alternative_id'] : '' );

$builder->addBasicLikeClause('user_name', isset($_GET['ap_user_name']) ? $_GET['ap_user_name'] : '' );

$builder->addCodedValueTypeClause('oid_typecode_coding_scheme',
                                  'oid_typecode',
                                  isset($_GET['po_oid_typecode_cvt']) ? $_GET['po_oid_typecode_cvt'] : '' );

$builder->addBasicLikeClause('oid', isset($_GET['po_oid']) ? $_GET['po_oid'] : '' );

$builder->addBasicUnquotedClause('typecode',
                                 isset($_GET['po_typecode']) ? $_GET['po_typecode'] : '' );

$builder->addBasicUnquotedClause('typecoderole',
                                 isset($_GET['po_typecoderole']) ? $_GET['po_typecoderole'] : '' );

$builder->addBasicLikeClause('xml', isset($_GET['xml']) ? $_GET['xml'] : '' );

require('dbconn.php');
if (!$dbconn)
    exit("Could not connect to database.");

//   $builder->addClause('a.id = ap.auditlog_id and a.id = po.auditlog_id');

$where_clause = $builder->getWhereClause();

if ($where_clause == '')
    exit();

$sql = "select count(distinct id) as count from auditlog as a left join auditlog_active_participant as ap on a.id = ap.auditlog_id left join auditlog_participant_object as po on a.id = po.auditlog_id $where_clause;";
#  echo "countsql: " . $sql;
$rs = odbc_exec($dbconn, $sql);
odbc_fetch_row($rs);
$resultCount = odbc_result($rs, "count");

$rs = odbc_exec($dbconn, "select count(*) as count from auditlog");
odbc_fetch_row($rs);
$allCount = odbc_result($rs, "count");

// This subselect returns the id's now we just needed the right
// columns and the right order
$endOffset = $offset + $limit -1;

$subselect = "select distinct id from auditlog as a left join auditlog_active_participant as ap on a.id = ap.auditlog_id left join auditlog_participant_object as po on a.id = po.auditlog_id $where_clause order by id";

# Note: The coalesce(event_typecode_domain.displayName, '') is a workaround for
# this bug: https://bugs.php.net/bug.php?id=54007. See also bug 14959.
$sql = "select * from (select rownumber() over (order by $orderby_column $desc) as rn, s.*, action_domain.displayName as actionDisplayName, " .
    " eventid_domain.displayName as eventidDisplayName, " .
    " coalesce(event_typecode_domain.displayName, '') as eventtypeDisplayname," .
    " (event_datetime + $timezone hours) as localTimestamp,  ".
    " event_outcome_domain.displayName as eventOutcomeDisplayName from " .
    "($subselect) as ids join auditlog as s on ids.id = s.id left join audit_domain_codes as action_domain on action_domain.coding_scheme = 'RFC3881_ACTION' and s.event_action_code = cast(action_domain.code as char(1))" .
    " left join audit_domain_codes as eventid_domain on eventid_domain.coding_scheme = s.event_id_coding_scheme and s.event_id_code = eventid_domain.code" .
    " left join audit_domain_codes as event_typecode_domain on event_typecode_domain.coding_scheme = s.event_typecode_cs and s.event_typecode_code = event_typecode_domain.code" .
    " left join audit_domain_codes as event_outcome_domain on event_outcome_domain.coding_scheme = 'RFC3881_OUTCOME' and s.event_outcome = cast(event_outcome_domain.code as integer)" .
    " order by $orderby_column $desc) as rownumbered where rn between $offset and $endOffset";

#echo $sql;
$rs = odbc_exec($dbconn, $sql);
if (!$rs) {
    auditLogUsed(4, $_SESSION['username']);
    exit ("Error in SQL!");
}
auditLogUsed(0, $_SESSION['username']);

$first = $offset;
$last = min($resultCount, $endOffset);
?>
<?php
if ($resultCount == 0) { ?>
  <table align="center">
   <tr><td>
  <?php echo _("Your query didn't match any records"); ?><br><br>
  <small>
  <?php echo _("Suggestions:"); ?><br>
  <?php echo _("- Check spelling"); ?><br>
  <?php echo _("- Try other search criterias"); ?><br>
  <?php echo _("- Simplify your query"); ?>
  </small>
  </td></tr>
  </table>
  <?php } else { ?>
<table align="center" border="1"  frame="void" width="100%">
  <thead>
  <tr>
   <td colspan="8" align="right"><?php echo sprintf ("%d-%d of %d found from %d records", $first, $last, $resultCount, $allCount); ?> <span id="timeConsumption"></span></td>
  </tr>
  <tr>
   <?php echo columnHeader('eventid_domain.displayName',_('Event ID'),
                    $orderby_column, $desc) ?>
   <?php echo columnHeader('event_typecode_domain.displayName',_('Event Type'),
                    $orderby_column, $desc) ?>
   <?php echo columnHeader('action_domain.displayName',_('Action'),
                    $orderby_column, $desc) ?>
   <?php echo columnHeader('event_outcome_domain.displayName',_('Result'),
                    $orderby_column, $desc) ?>
   <?php echo columnHeader('event_datetime',_('Datetime'),
                    $orderby_column, $desc) ?>
   <td><?php echo _("Active Participant ID"); ?></td>
   <td><?php echo _("Participant Object ID"); ?></td>
   <?php echo columnHeader('audit_src_id',_('Source'),
                    $orderby_column, $desc) ?>
   <td></td>
   </tr>
  </thead>
  <tbody>
  <?php while (odbc_fetch_row($rs)) { ?>
    <tr  valign="top">
      <td><?php echo odbc_result($rs, "eventidDisplayName") ?>&nbsp;</td>
      <td><?php echo odbc_result($rs, "eventtypeDisplayName") ?>&nbsp;</td>
      <td><?php echo odbc_result($rs, "actionDisplayName") ?>&nbsp;</td>
      <td><?php echo odbc_result($rs, "eventOutcomeDisplayName") ?>&nbsp;</td>
      <td><?php echo ereg_replace("\.[0-9]{6}$", "", ereg_replace("^([0-9]+)-([0-9]+)-([0-9]+)", "\\3-\\2-\\1", (odbc_result($rs, "localTimestamp")))) ?>&nbsp;</td>
      <td>
         <?php echo implode("<br>",
                            fetchColumn($dbconn, 'user_id',
                                        "select user_id from auditlog_active_participant where auditlog_id = " . odbc_result($rs, "id")));
         ?>
      </td>
      <td>
         <?php $oids = fetchColumn($dbconn, 'oid',
                                "select oid from auditlog_participant_object where auditlog_id = " . odbc_result($rs, "id"));

        for ($i=0; $i < count($oids); $i++)
            if (strlen($oids[$i]) > 60)
                $oids[$i] = substr($oids[$i], 0, 57) . "...";
         ?>
         <?php echo implode("<br>", $oids);
         ?>
      </td>
      <td><?php echo odbc_result($rs, "audit_src_id") ?>&nbsp;</td>
      <td><a href="showAuditXMLMsg.php?id=<?php echo odbc_result($rs, "id") ?>">xml</a></td>
    </tr>
  <?php } ?>
  </tbody>
  <?php if (true || $resultCount > $limit && $offset != 0) { ?>
    <tfoot>
      <tr>
        <td colspan="8" align="center">
          <?php echo navList($resultCount, $offset, $limit) ?>
        </td>
      </tr>
    </tfoot>
  <?php } ?>
</table>
<?php
  }
  odbc_close($dbconn);
  $endTime = (float) array_sum(explode(' ', microtime()));
  $duration = number_format($endTime-$startTime, 3);
  echo "<script language='javascript'>var myspan = document.getElementById('timeConsumption'); if (myspan) myspan.innerHTML = '($duration " . _("seconds") . ")';</script>";
?>
</body>
</html>
