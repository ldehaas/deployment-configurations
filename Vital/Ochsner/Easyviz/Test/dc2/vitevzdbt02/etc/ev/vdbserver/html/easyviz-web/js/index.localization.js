i18next
.use(i18nextXHRBackend)
.use(i18nextBrowserLanguageDetector)
.init({
  fallbackLng: 'en',
  debug: true,
  detection: {
    caches: []
  },
  backend: {
    loadPath: "translations/{{lng}}.json"
  },
}, function(err, t) {
  // init set content
  updateContent();
});
function updateContent() {
  var element_ids = [
              "username_text",
              "password_text",
              "domain_text",
              "language_text",
              "join_text",
              "resume_text",
              "da_DK_text",
              "sv_SE_text",
              "en_US_text",
              "en_GB_text",
              "en_CA_text",
              "en_AU_text",
              "de_DE_text",
              "de_CH_text",
              "de_AT_text",
              "fr_FR_text",
              "fr_BE_text",
              "it_IT_text",
              "ja_JP_text",
              "es_ES_text",
              "es_MX_text",
              "share_session_id_label",
              "connection_status_text",
              "ev_connect_status",
              "ev_cookie_policy_link",
              "use_of_cookies_text",
              "cookies_description_text",
              "cookie_table_text",
              "cookie_column_text",
              "purpose_column_text",
              "expires_column_text",
              "realm_cookie_description_text",
              "realm_cookie_expiration_text",
              "lang_cookie_description_text",
              "lang_cookie_expiration_text",
            ];
  element_ids.forEach(function(id) { document.getElementById(id).innerHTML = i18next.t(id); });

  document.getElementById("ev_connect_button").value =  i18next.t("ev_connect_button");

  window.g_tr.DISCONNECTED = i18next.t("disconnected_error_msg");
  window.g_tr.INCORRECT_USERNAME_OR_PASSWORD = i18next.t("incorrect_username_or_password_error_msg");
  window.g_tr.CODE = i18next.t("code_error_msg");
  window.g_tr.LOGGING_IN = i18next.t("logging_in_msg");
  window.g_tr.LAUNCHING_VNC = i18next.t("launching_vnc_msg");
  window.g_tr.PRECONNECTING = i18next.t("preconnecting_msg");
}

function changeLng(lng) {
  i18next.changeLanguage(lng);
}

i18next.on('languageChanged', function() {
  updateContent();
});
