import * as Log from '../novnc/core/util/logging.js'
import * as Backend from './backend.js'
import * as Vnc from './vnc.js'

// Copied from noVNC's ./app/webutil.js
function getQueryVar (name, defVal) {
    "use strict";
    var re = new RegExp('.*[?&]' + name + '=([^&#]*)'),
        match = document.location.href.match(re);
    if (typeof defVal === 'undefined') { defVal = null; }
    if (match) {
        return decodeURIComponent(match[1]);
    } else {
        return defVal;
    }
};

var g_keylog = "";              //contains the most recent keypresses

function makeLoginFunc(confirm_on_close) {
    return Backend.BackendLogin(Backend.HandleBackendSession(Backend.RequestVncSession(Backend.HandleVncSession(Vnc.VncConnect(confirm_on_close)))));
}

function ev_join_update()
{
    var session_id_node = $("#ev_share_session_id");
    var checked = $("#ev_join").attr("checked");
    if (checked) {
        $("#ev_roam").removeAttr("checked");
        session_id_node.closest(".toggle").show();
    } else {
        session_id_node.closest(".toggle").hide();
    }
};

function setup_login_form()
{
    var session_id_node = $("#ev_share_session_id");
    $('#ev_user').focus();
    $("#ev_realm").val($.cookie('realm'));

    var elem = document.getElementById('ev_lang');
    var idx = getDefaultLangIndex(elem);
    elem.selectedIndex = idx;

    $('#ev_login_form').submit(function() {
        $('#ev_connect_button').prop('disabled', true);
        // Do not allow empty username or password. Set focus on first
        // empty field.
        if ($('#ev_user').val() == "") {
            $('#ev_user').focus();
            $('#ev_connect_button').prop('disabled', false);
            return false;
        }
        if ($('#ev_password').val() == "") {
            $('#ev_password').focus();
            $('#ev_connect_button').prop('disabled', false);
            return false;
        }
        var session_mode = SessionMode.Create;
        var shared_session_id = "";

        if ($("#ev_roam").attr("checked"))
            session_mode = SessionMode.Resume;

        if ($("#ev_join").attr("checked")) {
            session_mode = SessionMode.Join;
            shared_session_id = jQuery.trim(session_id_node.val());
            if (shared_session_id == "") {
                session_id_node.focus();
                $('#ev_connect_button').prop('disabled', false);
                return false;
            }
        }

        try {
            var best_lang = elem.options[elem.selectedIndex].value;
            makeLoginFunc(true)({
                session_mode: session_mode,
                shared_session_id: shared_session_id,
                user: $('#ev_user')[0].value,
                realm: $('#ev_realm')[0].value,
                lang: best_lang
            }, $('#ev_password')[0].value);
        } catch (ex) {
            generic_error_callback(ex);
        }
        return false;
    });

    if ($("#ev_join").attr("checked")) {
        session_id_node.closest(".toggle").show();
    } else {
        session_id_node.closest(".toggle").hide();
    }

    $("#ev_join").change(function () {
        ev_join_update();
    });

    $("#ev_roam").change(function () {
        if ($("#ev_roam").attr("checked")) {
            $("#ev_join").removeAttr("checked");
            ev_join_update();
        }
    });
}

function attempt_autologin()
{
    var elem = document.getElementById('ev_lang');
    var best_lang = elem.options[elem.selectedIndex].value;
    var confirm_on_close = parseInt(getQueryVar("confirm_on_close", "1"), 10);

    // The vnc_* query variables make it possible to test the web-client
    // directly with evncserver, for instance using evtestdisplay.
    var vnc_port = getQueryVar("vnc_port");
    if (vnc_port) {
        var vnc_user = getQueryVar("vnc_user", "test");
        var vnc_server = getQueryVar("vnc_server", "localhost");
        var vnc_muxer_port = parseInt(getQueryVar("vnc_muxer_port", "0"), 10);
        var vnc_secret = getQueryVar("vnc_secret", "");
        var vnc_tls = getQueryVar("vnc_tls", "");
        var vnc_preconnect_host = getQueryVar("vnc_preconnect_host", "");
        try {
            VncConnect(confirm_on_close)({ "user": vnc_user, "lang": best_lang }, vnc_server, parseInt(vnc_port, 10),
                         vnc_muxer_port, vnc_secret, vnc_tls, vnc_preconnect_host);
        } catch (ex) {
            generic_error_callback(ex);
        }
        return;
    }

    var user = getQueryVar("username") || "<otp-cookie>";
    var password = getQueryVar("password");
    var session_id = getQueryVar("collab_join");
    var session_mode = SessionMode.Create;

    if (session_id !== null) {  // collab_join is set
        session_mode = SessionMode.Join;

        // Show the field were a session ID can be typed
        $("#ev_join").attr("checked",1);
        ev_join_update();

        // Allow user to type in session ID if none were given
        if (session_id === "") {
            $('#ev_user').val(user || "");
            $('#ev_password').val(password || "");
            $("#ev_share_session_id").focus();
            return;
        }
    }

    if (!password) {
        return;
    }

    $('#ev_login_screen').hide();

    var realm = getQueryVar("realm");
    if (realm == null) {
        realm = "";
    }

    var authz = getQueryVar("authzname");

    try {
        var activation_msg_id = getQueryVar('activation_msg_id');
        var is_integration = parseInt(getQueryVar("is_integration", "1"), 10);
        makeLoginFunc(confirm_on_close)({
            activation_key: getQueryVar('activation_key'),
            activation_msg_id: activation_msg_id,
            session_mode: session_mode,
            shared_session_id: session_id,
            user: user,
            authz: authz,
            realm: realm,
            lang: best_lang,
            no_wfm: activation_msg_id == "EV_WORKLIST_IMAGE",
            is_integration: is_integration
        }, password);
    } catch (ex) {
        generic_error_callback(ex);
    }
}

function document_ready()
{
    if (window.ev_build_info) {
        document.getElementById("ev_version_label").innerHTML = "EasyViz " + ev_build_info.EVVERSION + " (" + ev_build_info.BUILD_DATE + ")";
    }

    window.evlog = EVLog();

    // Force check of document.location (use ?logging=info in the URL)
    Log.init_logging(undefined);

    setup_login_form();

    $("#ev_cookie_policy_link").click(function () {
        $("#ev_cookie_policy").slideToggle();
    });

    if (getQueryVar('botmode_uri')) {
        window.BOTMODE_URI = getQueryVar('botmode_uri');
        //"vdb:///?patient.id=66;study.id=153;study.accession_number=MI.NJA.0B954094";
    }

    enableLogin(true);

    attempt_autologin();
}

document_ready();
