#!/usr/bin/perl -w

package vdb_auto_reorg_conf;

if (substr('@MY_MODULE_DIR@', 0, 1) ne '@')
{ use lib '@MY_MODULE_DIR@'; }
use vdb;

# ------------------- configuration -------------------
# Reorg is run only if current (1 min) load average is < high value.
# If reorg is paused due to high load average, it is resumed again
# when load average is <= low threshold.
our ($la_low_threshold, $la_high_threshold) = (2, 6);

# primary time (workday) window [start hour .. end hour]
our ($wd_start, $wd_end) = ("6:30", "18:30");

# If reorg cycle finishes faster than min_cycles_sec, tool will be idle
# for the remaining of the time
our $min_cycle_sec = 120 * 24 * 60 * 60; # 120 days in seconds

our $time_estimates_safety_factor = 0.75;
our $small_tables_n_rows = 1_000_000; # criteria for small tables
our %cluster_indexes =
  (
      "STATE" => "STATE_DATE2_IDX",
      "PREF_VAR" => "PREF_VAR_I_N_O_IDX"
  );

BEGIN
{
    use Exporter;

# ----------------------- exported globals -------------------------

    @ISA	= (Exporter);
    @EXPORT	= qw($la_low_threshold $la_high_threshold $wd_start $wd_end %cluster_indexes
		     $time_estimates_safety_factor $small_tables_n_rows $min_cycle_sec $schema);
    @EXPORT_OK	= qw();

}

$vdb::verbose = 3;
