Test/c7v-vitvcast01/      ---> PreProd Server in DCA
Test/c7v-vitvcast02/      ---> Test Server in DCB
Test/c7v-vitvcast03/      ---> Education Server (VACE) with Six Rialto instances

Prod/DCA/c7v-vitvcas01/   ---> Production App Server in DCA
Prod/DCA/c7v-vitvcas03/   ---> Production App Server in DCA
Prod/DCA/c7v-vitvcas05/   ---> Production App Server in DCA
Prod/DCA/c7v-vitvcas07/   ---> Production App Server in DCA
Prod/DCA/c7v-vitvcas09/   ---> Production App Server in DCA

Prod/DCB/c7v-vitvcas02/   ---> Production App Server in DCB
Prod/DCB/c7v-vitvcas04/   ---> Production App Server in DCB
Prod/DCB/c7v-vitvcas06/   ---> Production App Server in DCB
Prod/DCB/c7v-vitvcas08/   ---> Production App Server in DCB
Prod/DCB/c7v-vitvcas10/   ---> Production App Server in DCB

Migration/mig_vitmigas01/ ---> Migration App Server in DCA
Migration/mig_vitmigas03/ ---> Migration App Server in DCA
Migration/mig_vitmigas05/ ---> Migration App Server in DCA
Migration/mig_vitmigas07/ ---> Migration App Server in DCA


Prerequisite:
1) If your local repo is on MacBook, make sure you have gnu tar installed by running: 
# homebrew install gnu-tar
Otherwise Ansible does not like bsdtar shipped with MacOS

2) To work with the playbook, the absolute path of your local repo must be identical to the way it is declared in the inventory.
~/deployment-configuration

To backup existing configuration on all servers and bring it back to your laptop, run the following:

cd ~/deployment-configurations/x-Tools/Ansible/Rialto7

# Prod A
ansible-playbook -i inventories/ochsner_prod_dc1 vc_config_backup.yml
ansible-playbook -i inventories/ochsner_prod_dc1 vc_config_backup_rialto_only.yml

# Prod B
ansible-playbook -i inventories/ochsner_prod_dc2 vc_config_backup.yml
ansible-playbook -i inventories/ochsner_prod_dc2 vc_config_backup_rialto_only.yml

# Test A (Preprod)
ansible-playbook -i inventories/ochsner_test_dc1 vc_config_backup.yml
ansible-playbook -i inventories/ochsner_test_dc1 vc_config_backup_rialto_only.yml

# Test B (Test)
ansible-playbook -i inventories/ochsner_test_dc2 vc_config_backup.yml
ansible-playbook -i inventories/ochsner_test_dc2 vc_config_backup_rialto_only.yml

# Both DCA and DCB

