// Copied from noVNC's ./app/webutil.js
function getQueryVar (name, defVal) {
    "use strict";
    var re = new RegExp('.*[?&]' + name + '=([^&#]*)'),
        match = document.location.href.match(re);
    if (typeof defVal === 'undefined') { defVal = null; }
    if (match) {
        return decodeURIComponent(match[1]);
    } else {
        return defVal;
    }
};

// Use this for translating texts. It requires a global g_tr object.
function tr(key)
{
    if (typeof(g_tr) === "undefined") {
        $("#ev_connect_status").text("no translations (g_tr)");
        $("#ev_login_screen").append("<h1>No translations</h1>");
    }
    if (key in g_tr)
        return g_tr[key];
    return "[TRANSLATION] "+key;
}

function getHost() {
    var host = document.location.host;
    var p = host.indexOf(':');
    if (p !== -1) {
        host = host.substr(0, p);
    }
    return host;
}

// Use it when setting session_mode in the vncreq call
SessionMode = {
    Create: "CREATE",
    Resume: "RESUME",
    Join: "JOIN"
};

// if we're not logged in, this does nothing
var endSession = function(){};

// once we're logged in, endSession points to this
function endActualSession() {
    try {
        if (evlog.initialized) {
            evlog.debug(9, "Closing browser-side session");
            evlog.reset();
        }
    } catch(ex) {}
    endSession = function(){};
}

function displayLogin(visible) {
    if (!visible) {
        $('#ev_password')[0].value = "";
    }
    $('#ev_login_screen').css("display", visible ? "block" : "none");
    $('#ev_login_screen').blur();
    $('#ev_screen').css("display", visible ? "none" : "block");
    enableLogin(visible);
    if (visible) {
        $('#ev_login_form').show();
    } else {
        var c = document.getElementsByTagName('canvas')[0];
        c.focus();
    }
}

function enableLogin(enabled) {
    if (enabled) {
        $('#ev_password')[0].value = "";
        endSession();
    }
    $('#ev_login_form input').each(function(idx, node) { node.disabled = !enabled; });
}

function generic_error_callback(text)
{
    evlog.error("Generic error handler: " + text);
    // setTimeout is needed when an exception is thrown directly in vdbLogin
    // (also it gives a good visual effect)
    setTimeout(function () { enableLogin(true); }, 700);
    $("#ev_connect_status").text(text);
}

function xhr_error_callback(rsp, status, error)
{
    evlog.error("AJAX call failed: " + status + " " + error + ": " + rsp.responseText);
    enableLogin(true);
    // We use 412 as an alternative "401 Not authorized" to avoid login dialog from browser
    if (rsp.status == 412)
        $("#ev_connect_status").text(tr("INCORRECT_USERNAME_OR_PASSWORD"));
    else
        $("#ev_connect_status").text(error+" ("+tr("CODE")+" "+rsp.status+")");
}

function is_secure() {
    return document.location.protocol === "https:";
}

function getPreferredLangs()
{
    var langs = lang.split(",");
    return langs.map(function(l) { return l.split(";")[0]});
}

function indexOfMatchingLangOption(lang, elem)
{
    var options = Array.prototype.map.call(elem.children, function(child) {
        return child.value;
    });
    for (var idx in options) {
        if (options[idx].indexOf(lang) === 0) {
            return idx;
        }
    }
    return -1;
}

function getDefaultLangIndex(elem) {
    // If a lang is specified in the URL, that takes precedence
    var url_lang = getQueryVar("url_lang");
    if (url_lang !== null && url_lang !== "") {
        var idx = indexOfMatchingLangOption(url_lang, elem);
        if (idx !== -1) {
            evlog.debug(3, "Defaulting to language " + url_lang + " (" + idx + ") selected from URL Query param");
            return idx;
        }
    }

    // is the cookie set?
    var cookie_lang = $.cookie('lang');
    if (cookie_lang) {
        var idx = indexOfMatchingLangOption(cookie_lang, elem);
        if (idx !== -1) {
            evlog.debug(3, "Defaulting to language " + cookie_lang + " (" + idx + ") selected from cookie");
            return idx;
        }
    }

    // finally, run through the languages the browser specifies in the Accept-Language header
    var preferred = getPreferredLangs();
    for (var idx in preferred) {
        var browser_lang = preferred[idx];
        var idx = indexOfMatchingLangOption(browser_lang, elem);
        if (idx !== -1) {
            evlog.debug(3, "Defaulting to language " + browser_lang + " (" + idx + ") selected from browser locale");
            return idx;
        }
    }
    var idx = indexOfMatchingLangOption('en', elem);
    if (idx !== -1) {
        evlog.debug(3, "Defaulting to language en (" + idx + ") as a fallback");
        return idx;
    }
    evlog.debug(3, "Could not select a language");
    return 0;
}
