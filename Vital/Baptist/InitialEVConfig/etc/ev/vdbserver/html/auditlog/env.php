<?php
require_once("vars.php");
# Is there a better way to read the environment configuration $sysconf_dir/ev/ev.env
# that does not require us to change $sysconf_dir/init.d/httpd to source it than this

if (file_exists("$sysconf_dir/ev/auditlog.env")) {
    $evEnv = file("$sysconf_dir/ev/auditlog.env");
} else if (file_exists("$sysconf_dir/ev/ev.env")) {
    $evEnv = file("$sysconf_dir/ev/ev.env");
} else {
    exit ("No environment file found");
}

foreach ($evEnv as $value) {
# Match unquoted or quoted with " or '
    if (preg_match('/^\s*export\s+(\w+)=([^\'"][^\s]*)(?:\s.*)?$/', $value, $matches) ||
        preg_match('/^\s*export\s+(\w+)="((?:[^"\\\\]|\\\\.)*)"(?:\s.*)?$/', $value, $matches) ||
        preg_match('/^\s*export\s+(\w+)=\'((?:[^\'\\\\]|\\\\.)*)\'(?:\s.*)?$/', $value, $matches)) {
        putenv($matches[1] . "=" . $matches[2]);
    }
}
?>
