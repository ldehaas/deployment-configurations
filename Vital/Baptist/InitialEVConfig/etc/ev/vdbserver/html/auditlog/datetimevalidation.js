var dateTimeRegexp = /^\s*(\d?\d)[\/-](\d?\d)[\/-]((?:\d\d)?\d\d)(?:\s+(\d?\d)[:\-\.](\d?\d))?\s*$/;

function validateDateTime(aDateTime) {
  if (aDateTime.trim() == "")
   return "";
  var m = dateTimeRegexp.exec(aDateTime);
  if (!m)
    return null;

  if (m[3].length == 2)
    m[3] = "20"+m[3];

  var d = new Date(m[3].toInt(), m[2].toInt()-1, m[1].toInt());
  var dd = d.getDate()    <= 9 ? "0"+d.getDate() : ""+d.getDate();
  var mm = d.getMonth()+1 <= 9 ? "0"+(d.getMonth()+1) : ""+(d.getMonth()+1);

  var out = dd+"-"+mm+"-"+d.getFullYear();

  if (m[4]) {
   var hours   = m[4].toInt();
   var minutes = m[5].toInt();
   if (hours >= 0 && hours <= 23 && minutes >= 0 && minutes <= 59)
     out += " " + (hours <= 9 ? "0" : "")+hours + ":" + (minutes <= 9 ? "0" : "")+minutes;
   else
     return null;
  }

  return out;
}

var timeRegexp = /\s*\s*/;

function checkDateTimeField(aDateTimeField) {
  var valDateTime = validateDateTime(aDateTimeField.value);
  if (valDateTime == null) {
   alert("Datotidsangivelsen '"+aDateTimeField.value+"' er ugyldig.  Formattet er DD-MM-YYYY, f.eks 13-12-2004. Det er lovligt at angive et klokkeslet f.eks 13-12-2003 11:21");
   aDateTimeField.focus();
   aDateTimeField.select();
   return false;
  } else {
    aDateTimeField.value = valDateTime;
    return true;
  }
}

function toDb2Timestampfrom(aDateTimeString) {
  var m = dateTimeRegexp.exec(aDateTimeString);
  if (!m)
    return "";
  var date    = m[1] ? m[1] : "";
  var month   = m[2] ? m[2] : "";
  var year    = m[3] ? m[3] : "";
  var hours   = m[4] ? m[4] : "00";
  var minutes = m[5] ? m[5] : "00";
  return year+"-"+month+"-"+date+"-"+hours+"."+minutes+".00";
}

function toDb2Timestampto(aDateTimeString) {
  var m = dateTimeRegexp.exec(aDateTimeString);
  if (!m)
    return "";
  var date    = m[1] ? m[1] : "";
  var month   = m[2] ? m[2] : "";
  var year    = m[3] ? m[3] : "";
  var hours   = m[4] ? m[4] : "24";
  var minutes = m[5] ? m[5] : "00";
  return year+"-"+month+"-"+date+"-"+hours+"."+minutes+".00";
}
