<?php
class ActiveParticipant {
    function ActiveParticipant($id, $alternateId,
                               $userName, $networkAccessPointID,
                               $networkType)
    {
        $this->id = $id;
        $this->alternateId = $alternateId;
        $this->userName = $userName;
        $this->networkAccessPointID = $networkAccessPointID;
        $this->networkType = $networkType;
    }
  }

function auditLogUsed($outcome, $user)
{
    $logmsg =
        "<?xml version='1.0' encoding='UTF-8' ?>" .
        "<AuditMessage>" .
        " <EventIdentification EventActionCode='R' " .
        "  EventDateTime='".gmstrftime("%FT%T+00:00")."' " .
        "  EventOutcomeIndicator='".$outcome."'> " .
        "  <EventID code='110101' codeSystemName='DCM' displayName='Audit Log Used' />" .
        " </EventIdentification>" .
        " <ActiveParticipant UserID='".$user."' AlternativeUserID='' " .
        "  UserIsRequestor='true'  " .
        "  NetworkAccessPointID='".$_SERVER['REMOTE_ADDR']."' " .
        "  NetworkAccessPointTypeCode='2'>" .
        "  <RoleIDCode code='110151' codeSystemName='DCM' displayName='Application Launcher'/>" .
        " </ActiveParticipant>" .
        " <ActiveParticipant UserID='".getmypid()."' AlternativeUserID='' " .
        "  UserIsRequestor='true'  " .
        "  NetworkAccessPointID='".$_SERVER['SERVER_NAME']."' " .
        "  NetworkAccessPointTypeCode='1'>" .
        "  <RoleIDCode code='110150' codeSystemName='DCM' displayName='Application'/>" .
        " </ActiveParticipant>" .
        " <AuditSourceIdentification AuditSourceID='auditlogweb'> " .
        "   <AuditSourceTypeCode code='3'/> " .
        " </AuditSourceIdentification>" .
        " <ParticipantObjectIdentification ParticipantObjectID='".
        str_replace("'", "&quot;", htmlspecialchars($_SERVER['REQUEST_URI']))."' " .
        "                                   ParticipantObjectTypeCode='2' " .
        "                                    ParticipantObjectTypeCodeRole='13'>" .
        "   <ParticipantObjectIDTypeCode code='12' />" .
        "   <ParticipantObjectName>Security Audit Log</ParticipantObjectName>" .
        " </ParticipantObjectIdentification> " .
        "</AuditMessage>";

    //   openlog("EVAUDIT", LOG_CONS, LOG_LOCAL5);
    //   syslog(LOG_INFO, $logmsg);
    // Grrr, the php syslog wrapper truncates log messages to 1KB. The perl
    // syslog wrapper doesn't so we use that one instead..
    system('perl -e "use Sys::Syslog; openlog(\'EVAUDIT\', \'\',\'local5\'); syslog(\'info\', \"'.$logmsg.'\");"');
}

?>
