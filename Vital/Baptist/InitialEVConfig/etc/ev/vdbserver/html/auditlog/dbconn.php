<?php
require_once('env.php');

// This code is a php-port of vdbclient::Session::parseConnectionString(...).
class DSNParams {
    function DSNParams($conn_string)
    {
        $re = "/((\\w+):)?(\/\/([^:\/;]+)(:(\\d+))?\/?)?((\\w*)\\?{0,1})?((\\w+=[^;]*;)*)/";

        if (preg_match($re, $conn_string, $matches) == 0) {
            throw new Exception("Could not parse EV_VDB_DSN: " . $dsn . "\n");
        } else {
            $this->be    = $matches[2];
            $this->host  = $matches[4];
            $this->port  = $matches[6];
            $this->dsn   = $matches[8];

            $kw_re = "/(\\w+)=([^;]*);?/";
            $querystring = $matches[9];
            if ($count = preg_match_all($kw_re, $querystring, $kw_matches, PREG_SET_ORDER)) {
                for ($i=0; $i<$count; ++$i) {
                    if ($kw_matches[$i][1] == "user")
                        $this->user = $kw_matches[$i][2];
                    else if ($kw_matches[$i][1] == "password")
                        $this->passwd = $kw_matches[$i][2];
                }
            }
        }
    }
}

$dsn = shell_exec("$bin_dir/vdb_dsn -c");
$dsn_params = new DSNParams($dsn);
$dsn_params->user = "evuserro";
$dbconn = odbc_connect($dsn_params->dsn, $dsn_params->user, $dsn_params->passwd);
odbc_exec($dbconn, "set current schema evaudit");
?>
