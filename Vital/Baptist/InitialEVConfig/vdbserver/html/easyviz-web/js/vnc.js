// kind of hackish, but I don't see a better way around it
// need to work around old browsers which claim to have websockets, but which we know implement old draft versions

import * as Log from '../novnc/core/util/logging.js'
import RFB from '../novnc/core/rfb.js'

function handleApplicationMsg(e)
{
    var recipient = e.detail.recipient;
    var msg = e.detail.msg;
    var msg_id = e.detail.msg_id;
    evlog.debug(1, "handleApplicationMsg: recipient:" + recipient +
               " msg_id: " + msg_id + " msg: " + msg.substr(0,40) + "...");
    if (msg_id === "open-uri") {
        var uri = msg.trim();
        evlog.debug(1, "openuri('" + uri + "')");
        function openSilent() {
            evlog.debug(5, "openUri: opening as iframe: " + uri);
            $("#ev_openuris").html("<iframe src='" + uri + "'></iframe>");
        }
        function openVisible() {
            evlog.debug(5, "openUri: opening as new window: " + uri);
            window.open(uri);
        }

        var is_http = uri.indexOf("http://") === 0 ||
            uri.indexOf("https://") === 0 ||
            uri.indexOf('/') === 0;
        if (!is_http) {
            // if it's not http, we assume it's some kind of app launch
            evlog.debug(5, "openUri: Non-http protocol");
            openSilent();
            return;
        }
        $.ajax({
            cache: false,
            type: "HEAD",
            url: uri,
            success : function(data, status, xhr) {
                // do we have a Content-Disposition?
                var disp = xhr.getResponseHeader("Content-Disposition");
                if (disp) {
                    if (disp.indexOf("attachment") === 0) {
                        // this should be downloaded
                        evlog.debug(5, "openUri: attachment hint");
                        openSilent();
                    } else {
                        // We want it shown in the browser
                        evlog.debug(5, "openUri: inline hint");
                        openVisible();
                    }
                } else {
                    // otherwise, it should probably be shown in the browser. Probably...
                    evlog.debug(5, "openUri: No hints available");
                    openVisible();
                }
            },
            error: function (rsp, status, error)
            {
                // if HEAD request fails, we don't know what we're dealing with. Show it to the user
                evlog.debug(5, "openUri: HEAD failed");
                openVisible();
            }
        });
    }
}

function mi_encode_int(arr, val)
{
    arr.push(val>>24);
    arr.push(val>>16 & 0xFF);
    arr.push(val>>8 & 0xFF);
    arr.push(val & 0xFF);
}

function mi_encode_string(arr, str)
{
    mi_encode_int(arr, str.length);
    for (var i=0; i <str.length; i++)
        arr.push(str.charCodeAt(i));
}

// Send MIClientIdentification message (code 250)
// See also evncserver2/common/rfb/SMsgReaderMI.cxx
function send_mi_client_identification(websocket, user)
{
    var user_enc = unicode.toUtf8(unicode.fromString(user));
    websocket.send([250, 0,0,0,user_enc.length]);
    websocket.send(user_enc);
    var arr = [];
    mi_encode_int(arr, Math.floor(Math.pow(2,31)*Math.random()));
    websocket.send(arr);
}

// Send MISendApplicationMsg message (code 254)
// See also evncserver2/common/rfb/SMsgReaderMI.cxx
function send_mi_send_application_msg(websocket, activation_msg_id, activation_key)
{
    var data = [254];
    mi_encode_string(data, "evsm");
    mi_encode_string(data, activation_msg_id);
    mi_encode_string(data, activation_key);
    websocket.send(data);
}

function ConnectFunc(context, host, port, muxer_port, user, secret, tls, preconnect_host, statehandler) {
    return function() {
        var session_id = context['session-id'];
        if (muxer_port) {
            evlog.debug(3, "Attempting muxer preconnect against port " + muxer_port + " for display " + port);
            $("#ev_connect_status").html(tr("PRECONNECTING")+"....");
            $.ajax({
                cache: false,
                type: "POST",
                data: " ",
                dataType: "text",
                url: "/vdb/rest/session/" + session_id + "/vnc/preconnect?" +
                    "host=" + preconnect_host + "&displaynum=" + port
                    + "&logsessionid="+evlog.logsessionid(),
                success : function() {
                    evlog.debug(3, "Muxer preconnect successful");
                    var url = (tls ? "wss" : "ws") + "://" + host + ":" + muxer_port + "/vnc";
                    console.log("url is", url);
                    var rfb = createRfb(context, user, statehandler, url, secret);
                },
                error: function (rsp, status, error)
                {
                    evlog.error("Muxer preconnect failed");
                    xhr_error_callback(rsp, status, "vncmuxer preconnect: "+error);
                }
            });
        } else {
            Log.Info("Got port " + port);
            evlog.debug(3, "Connecting to port: " + port);
            var url = (tls ? "wss" : "ws") + "://" + host + ":" + port + "/vnc";
            console.log("url is", url);
            var rfb = createRfb(context, user, statehandler, url, secret);
        }

        enableLogin(false);

        $("#ev_connect_status").html(tr("LAUNCHING_VNC"));
    };
}

function askToLeave(e)
{
    var msg = "Leaving the page will disconnect from the EasyViz session. "
        + "Are you sure you want to leave?";
    (e || window.event).returnValue = msg;
    return msg;
}

function StateHandler(context, confirm_on_close) {
    var oldstate = 'disconnect';
    return function(rfb, state, detail) {
        if (state === oldstate) {
            return;
        }

        if (oldstate == 'connect') {
            window.removeEventListener('beforeunload', askToLeave, false);
        }
        oldstate = state;

        switch (state) {
            case 'disconnect':
                evlog.debug(3, "Entering 'disconnected' state");

                if (detail.clean) {
                    Log.Info("VNC Client disconnected normally");
                } else {
                    Log.Error("VNC Client disconnected unexpectedly");
                }
                $("#ev_connect_status").text(tr("DISCONNECTED"));
                displayLogin(true);
                break;
            case 'connect':
                evlog.debug(3, "Connected");
                if (confirm_on_close) {
                    window.addEventListener('beforeunload', askToLeave, false);
                }
                displayLogin(false);
                break;
            default:
                evlog.debug(5, "Entering state " + state);
                break;
        }
    };
}

function createRfb(context, qualifiedUser, statehandler, url, secret) {
    var has_evz = context.activation_msg_id && context.activation_key;
    function custom_send(e)
    {
        send_mi_client_identification(e.detail.sock, qualifiedUser);
        if (has_evz)
            send_mi_send_application_msg(e.detail.sock,
                                         context.activation_msg_id,
                                         "activation_key:" + context.activation_key);
        try {
            var metric = {
                eventType: 'web_system_info',
                timestamp: new Date().getTime(),
                host: window.location.host,
                userAgent: navigator.userAgent
            };
            send_mi_send_application_msg(websocket, "EV_CLIENT_METRICS", JSON.stringify(metric));
        } catch (ex) {
            evlog.error("Could not send system info metric");
        }
    };
    var print_clipboard_copy = false;

    var rfb = new RFB($('#ev_screen')[0], url,
        {
            credentials: {
                password: secret
            },
            scale_factor: context.scale_factor
        }
    );

    var lastSeenClipboardText = "";
    rfb.addEventListener("clipboard", function(e) {
        if (navigator.clipboard && navigator.clipboard.writeText &&
            lastSeenClipboardText !== e.detail.text) {
            lastSeenClipboardText = e.detail.text;
            navigator.clipboard.writeText(e.detail.text).catch(function() {
                console.log("clipboard.writeText failed");
            });
        }

        if (print_clipboard_copy) {
            console.log("Server-side clipboard copy: " + e.detail.text);
        }
    });

    if (navigator.clipboard && navigator.clipboard.readText) {
        window.addEventListener("focus", function() {
            navigator.clipboard.readText().then(function(text) {
                if (lastSeenClipboardText !== text) {
                    lastSeenClipboardText = text;
                    rfb.clipboardPasteFrom(text);
                }
            });
        });
    }

    rfb.addEventListener("connect", function(e) { statehandler(rfb, "connect", e.detail); });
    rfb.addEventListener("disconnect", function(e) { statehandler(rfb, "disconnect", e.detail); });

    rfb.addEventListener("postClientInit", custom_send);
    rfb.addEventListener("applicationMsg", handleApplicationMsg);

    window.evwebPaste = function(text) {
        rfb.clipboardPasteFrom(text);
    }
    window.evwebCopyEnabled = function(enabled) {
        print_clipboard_copy = enabled;
    }

    evlog.debug(3, "Creating RFB object");
    return rfb;
}

export function VncConnect(confirm_on_close) {
    return function(context, host, port, muxer_port, secret, tls, preconnect_host) {
        if (confirm_on_close) {
            evlog.debug(1, "confirm_on_close is set: User will be prompted for confirmation before closing the window");
        } else {
            evlog.debug(1, "confirm_on_close is not set: User will not be prompted for confirmation before closing the window");
        }
        evlog.debug(1, "Connecting to rendernode. Source page URL is " + window.location.href);
        var user = context.user;

        $("#ev_connect_status").html(tr("LOGGING_IN")+"..");
        Log.Info("vncConnect username="+user+" lang="+context.lang);
        if (!context.lang)
            throw new Error("lang not set in VncConnect");

        var realm = context.realm;
        $.cookie('realm', realm, { expires: 365 });
        $.cookie('lang', context.lang, { expires: 365 });

        $("#ev_connect_status").html(tr("LOGGING_IN")+"..");
        Log.Info("vncConnect username="+user+" lang="+context.lang);

        var statehandler = StateHandler(context, confirm_on_close);
        var connectFunc = ConnectFunc(context, host, port, muxer_port, user, secret, tls, preconnect_host, statehandler);
        $("#ev_connect_status").html(tr("LAUNCHING_VNC"));
        connectFunc();
    };
}
