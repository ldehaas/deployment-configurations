// -- Array extensions --

Array.prototype.select = function(filterUnaryFunc) {
        var i;
        var retval = [];
        for (i=0; i<this.length; ++i)
                if (filterUnaryFunc(this[i]))
                        retval.push(this[i]);
        return retval;
}

Array.prototype.grep = Array.prototype.select;

Array.prototype.map = function(filterUnaryFunc) {
        var i;
        var retval = [];
        for (i=0; i<this.length; ++i)
                retval.push(filterUnaryFunc(this[i]));
        return retval;
}

Array.prototype.find = function(anObject) {
        for (i=0; i<this.length; ++i)
                if (anObject == this[i])
                        return i;
        return -1;
}

Array.prototype.indexOf = Array.prototype.find;

Array.prototype.includes = function(anObject) {
        return this.indexOf(anObject) != -1;
}

Array.prototype.contains = Array.prototype.includes;

Array.prototype.findIf = function(filterUnaryFunc) {
        for (i=0; i<this.length; ++i)
                if (filterUnaryFunc(this[i]))
                        return this[i];
        return null;
}

Array.prototype.removeAt = function(anIndex) {
        return this.splice(anIndex, 1)[0];
}

Array.prototype.remove = function(anObject) {
        return this.removeAt(this.find(anObject));
}

Array.prototype.insert = function(anIndex, anObject) {
        return this.splice(anIndex, 0, anObject)[0];
}

Array.prototype.clone = function(elementCloner) {
        if (elementCloner)
                return this.map(elementCloner);
        else
                return [].concat(this);
}

function Array_clone(src, elementCloner) {
        if (elementCloner) {
                var dest = [];
                for (var i=0; i<src.length; ++i)
                        dest.push(elementCloner(src[i]));
                return dest;
        } else
                return [].concat(src);
}

Array.prototype.append = function(anArray) {
        // A faster implementation using splice is probably possible
        for (var i=0; i<anArray.length; ++i)
                this.push(anArray[i]);
        return this;
}

Array.prototype.clear = function() {
        this.splice(0, this.length);
        return this;
}

// Expects array to be sorted
Array.prototype.uniq = function() {
        for (var i=this.length-1; i>=1; --i)
                if (this[i-1] == this[i])
                        this.removeAt(i);
        return this;
}

// -- String extensions --

String.prototype.trimLeft= function() {
        return this.replace(/^[\s]+/g,"");
}

String.prototype.trimRight= function() {
        return this.replace(/[\s]+$/g,"");
}

String.prototype.trim= function() {
        return this.trimLeft().trimRight();
}

String.prototype.left= function(count) {
        return this.substr(0,count);
}

String.prototype.right= function(count) {
        return this.substr(this.length-count);
}

String.prototype.isInteger= function(allowSign) {
        return (allowSign ? /(^[-+]?\d\d*$)/ : /(^\d\d*$)/).test(this);
}

String.prototype.isNumeric= function(decimalComma) {
        return (decimalComma ? /(^[-+]?\d\d*\,\d*$)|(^[-+]?\d\d*$)|(^[-+]?\,\d\d*$)/ : /(^[-+]?\d\d*\.\d*$)|(^[-+]?\d\d*$)|(^[-+]?\.\d\d*$)/).test(this);	// Optional sign, optional decimal dot
}

String.prototype.padRight = function(totalWidth,paddingChar)
{
        var i;
        var retval = this;
        for (i=this.length; i < totalWidth ; i++)
                retval += paddingChar
        return retval;
}

String.prototype.padLeft = function(totalWidth,paddingChar)
{
        var i;
        var retval = "";
        for (i=this.length; i < totalWidth ; i++)
                retval += paddingChar
        retval += this;
        return retval;
}

String.prototype.toInt = function() {
        return this * 1;
}

/*
// Unit tests:
function assert(res, msg) {
        if (!res)
                throw new Error(-1, "Assertion failed"+(msg ? ": "+msg : ""));
}

function assertEqual(expected, actual, msg) {
        var text = (msg ? msg+", " : "")+expected+" == "+actual;
        assert(expected == actual, text);
}

function assertArrayEqual(expected, actual) {
        assertEqual(expected.length, actual.length, "Expected and actual arrays length should be the same");
        for (var i=0; i<expected.length; ++i)
                assertEqual(expected[i], actual[i], "Array elements at index "+i+" differ");
}

try {
        var a = [1,2,3];
        a.insert(1, "hej");
        assertArrayEqual([1,2,3], [1,2,3]);
        assertArrayEqual([1,"hej",2,3], a);

        assertEqual(2, a.removeAt(2));
        assertArrayEqual([1,"hej",3], a);

        assertEqual(1, a.find("hej"));
        assertEqual(null, a.findIf(function(e) { return e == "davs"}));
        assertEqual("hej", a.findIf(function(e) { return typeof(e) == "string"}));

        assertArrayEqual([1,3], a.select(function(e) { return typeof(e) == "number"}));
} catch(e) {
        var msg = e.description ? e.description : e;
        alert(msg);
}
*/
