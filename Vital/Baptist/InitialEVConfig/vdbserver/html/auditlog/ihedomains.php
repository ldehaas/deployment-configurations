<?php
$AUDIT_ACTION_CODE_DOMAIN =
    array("" => "",
          "C" => "Create",
          "R" => "Read",
          "U" => "Update",
          "D" => "Delete",
          "E" => "Execute"
          );

$AUDIT_EVENT_OUTCOME_DOMAIN =
    array("" => "",
          "0" => "Success",
          "4" => "Minor Failure",
          "8" => "Serious Failure",
          "12" => "Major Failure"
          );

$AUDIT_EVENT_ID_DOMAIN =
    array("" => "",
          "DCM,110100" => "Application Activity",
          "DCM,110101" => "Audit Log Used",
          "DCM,110102" => "Begin Transf. DICOM Instances",
          "DCM,110103" => "DICOM Instances Accessed",
          "DCM,110104" => "DICOM Instances Transferred",
          "DCM,110105" => "DICOM Study Deleted",
          "DCM,110106" => "Export",
          "DCM,110107" => "Import",
          "DCM,110108" => "Network Entry",
          "DCM,110109" => "Order Record",
          "DCM,110110" => "Patient Record",
          "DCM,110111" => "Procedure Record",
          "DCM,110112" => "Query",
          "DCM,110113" => "Security Alert",
          "DCM,110114" => "User Authentication"
          );

$AUDIT_EVENT_TYPE_CODE_DOMAIN =
    array("" => "",
          "DCM,110120" => "Application Start",
          "DCM,110121" => "Application Stop",
          "DCM,110122" => "Login",
          "DCM,110123" => "Logout",
          "DCM,110124" => "Attach",
          "DCM,110125" => "Detach",
          "DCM,110126" => "Node Authentication",
          "DCM,110127" => "Emergency Override",
          "DCM,110128" => "Network Configuration",
          "DCM,110129" => "Security Configuration",
          "DCM,110130" => "Hardware Configuration",
          "DCM,110131" => "Software Configuration",
          "DCM,110132" => "Use of Restricted Function",
          "DCM,110133" => "Audit Recording Stopped",
          "DCM,110134" => "Audit Recording Started",
          "DCM,110135" => "Object Security Attributes Changed",
          "DCM,110136" => "Security Roles Changed",
          "DCM,110137" => "User Security Attributes Changed"
          );

$ACTIVE_PARTICIPANT_ROLE_ID_CODE_DOMAIN =
    array("" => "",
          "DCM,110150" => "Application",
          "DCM,110151" => "Application Launcher",
          "DCM,110152" => "Destination",
          "DCM,110153" => "Source",
          "DCM,110154" => "Destination Media",
          "DCM,110155" => "Source Media",
          );

$SECURITY_ALERT_TYPE_CODE_DOMAIN =
    array("" => "",
          "DCM,110126" => "Node Authentication",
          "DCM,110127" => "Emergency Override",
          "DCM,110128" => "Network Configuration",
          "DCM,110129" => "Security Configuration",
          "DCM,110130" => "Hardware Configuration",
          "DCM,110131" => "Software Configuration",
          "DCM,110132" => "Use of Restricted Function",
          "DCM,110133" => "Audit Recording Stopped",
          "DCM,110134" => "Audit Recording Started",
          "DCM,110135" => "Object Security Attributes Changed",
          "DCM,110136" => "Security Roles Changed",
          "DCM,110137" => "User Security Attributes Changed"
          );

$PARTICIPANT_OBJECT_ID_TYPE_CODE_DOMAIN =
    array("" => "",
          "DCM,110180" => "Study Instance UID",
          "DCM,110181" => "SOP Class UID",
          "DCM,110182" => "Node ID"
          );

$PARTICIPANT_OBJECT_TYPE_CODE_DOMAIN =
    array("" => "",
          "1" => "Person",
          "2" => "System Object",
          "3" => "Organization",
          "4" => "Other"
          );

$PARTICIPANT_OBJECT_TYPE_CODE_ROLE_DOMAIN =
    array("" => "",
          "1" => "Patient",
          "2" => "Location",
          "3" => "Report",
          "4" => "Resource",
          "5" => "Master file",
          "6" => "User",
          "7" => "List",
          "8" => "Doctor",
          "9" => "Subscriber",
          "10" => "Guarantor",
          "11" => "Security User Entity",
          "12" => "Security User Group",
          "13" => "Security Resource",
          "14" => "Security Granularity Definition",
          "15" => "Provider",
          "16" => "Report Destination",
          "17" => "Report Library",
          "18" => "Schedule",
          "19" => "Customer",
          "20" => "Job",
          "21" => "Job Stream",
          "22" => "Table",
          "23" => "Routing Criteria",
          "24" => "Query"
          );

$TIMEZONE =
    array("12" => "NZST (New Zealand Standard Time)",
          "11" => "UTC+11",
          "10" => "EAST (East Australian Standard Time)",
          "9" => "KST  (Korea Standard Time)",
          "8" => "CCT (China Coastal Time",
          "7" => "WAST (West Australian Time)",
          "6" => "UTC+06",
          "5" => "UTC+05",
          "4" => "RET (Reunion Time)",
          "3" => "MSK (Moscow Time)/EEST",
          "2" => "EET (Eastern Europe Time)/CEST",
          "1" => "CET (Central Europe Time",
          "0" => "UTC/GMT",
          "-1" => "WAT (West Africa Time)",
          "-3" => "ADT (Atlantic Daylight Saving Time)",
          "-4" => "AST (Atlantic Standard Time)/EDT",
          "-5" => "EST (Eastern Standard Time)/CDT",
          "-6" => "CST (Central Standard Time)/MDT",
          "-7" => "MST (Mountain Standard Time)/PDT",
          "-8" => "PST (Pacific Standard Time)/AKDT",
          "-9" => "AKST (Alaska Standard Time)",
          "-10" => "HST (Hawaiian Standard Time)",
          "-11" => "UTC-11",
          "-12" => "ILDW (International Date Line, West)"
          );
?>
