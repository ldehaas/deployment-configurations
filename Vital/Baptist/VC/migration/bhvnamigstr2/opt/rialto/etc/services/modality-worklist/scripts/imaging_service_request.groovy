import com.karos.rtk.common.HL7v2Date;
import org.joda.time.DateTimeZone;

def scriptName="MWL Imaging Service Request Morpher - "

log.info(scriptName + "Order message:\n{}", input)
log.info(scriptName + "initial Imaging Service Request:\n{}", imagingServiceRequest)

if (imagingServiceRequest.getAccessionNumberUniversalId() == null) {
    imagingServiceRequest.setAccessionNumberNamespaceId("BH")
    imagingServiceRequest.setAccessionNumberUniversalId("2.16.124.113638.7.7.1.1")
    imagingServiceRequest.setAccessionNumberUniversalIdType("ISO")
}

if (get("ORC-3") != null) {
   log.debug(scriptName + "about to set Accession Number to {}", get("ORC-3"))
   imagingServiceRequest.setAccessionNumber(get("ORC-3"))
} else if (get("ORC-2") != null) {
   //log.debug(scriptName + "Accession Number is not found in ORC-3, will use Order number found in ORC-2, about to set Accession Number to {}", 'VNA' + get("ORC-2"))
   //imagingServiceRequest.setAccessionNumber('VNA' + get("ORC-2"))
   log.debug(scriptName + "Accession Number is not found in ORC-3, will use Order number found in ORC-2, about to set Accession Number to {}", get("ORC-2"))
   imagingServiceRequest.setAccessionNumber(get("ORC-2"))
} else {
   log.debug(scriptName + "NOT setting Accession Number as ORC-3 appeards to be NULL")
}

def patientIdentification = imagingServiceRequest.getPatientIdentification()
if (patientIdentification.getPatientIdUniversalId() == null) {
    patientIdentification.setPatientIdNamespaceId("BH")
    patientIdentification.setPatientIdUniversalId("2.16.124.113638.7.7.1.1")
    patientIdentification.setPatientIdUniversalIdType("ISO")
}

imagingServiceRequest.setPatientIdentification(patientIdentification)

def patientDemographics = imagingServiceRequest.getPatientDemographics()
//patientDemographics.setPatientBirthDate(get("PID-6"))
imagingServiceRequest.setPatientDemographics(patientDemographics)


// *******************************
// *     Requested Procedure     *
// *******************************

def requestedProcedure = imagingServiceRequest.getRequestedProcedureSequence().get(0)

if (get("OBR-4-1") != null) {
    log.debug(scriptName + "about to set ProcedureID based on OBR-4-1")
    requestedProcedure.setRequestedProcedureID(get("OBR-4-1"))
    requestedProcedure.setRequestedProcedureDescription(get("OBR-4-2"))
} else {
    log.debug(scriptName + "about to set ProcedureID based on OBR-4-4") 
    requestedProcedure.setRequestedProcedureID(get("OBR-4-4"))
    requestedProcedure.setRequestedProcedureDescription(get("OBR-4-5"))
}

requestedProcedure.setRequestedProcedurePriority(get("OBR-5"))
requestedProcedure.setReasonForTheRequestedProcedure(get("OBR-31"))
//requestedProcedure.setStudyInstanceUID(get("ZDS-1")

// ***********************************
// *     Scheduled Procedure Step    *
// ***********************************

def scheduledProcedureStep = requestedProcedure.getScheduledProcedureStepSequence().get(0)
scheduledProcedureStep.setScheduledProcedureStepIDString(imagingServiceRequest.getAccessionNumber())

if (scheduledProcedureStep.getScheduledProcedureStepLocation() == null) {
    log.debug(scriptName + "about to set Scheduled Procedure Step Location to UNKNOWN");
    scheduledProcedureStep.setScheduledProcedureStepLocation("UNKNOWN");
}

//if (scheduledProcedureStep.getScheduledStationName() == null) {
//    log.debug(scriptName + "about to set Scheduled Station Name to UNKNOWN");
//    scheduledProcedureStep.setScheduledStationName("UNKNOWN");
//}

def modalityType = scheduledProcedureStep.getModality()
if (modalityType == null) {
    log.debug(scriptName + "Modality type is NULL, will extract Modality type from ProcedureDescription");
    modalityType = get("OBR-4-5").substring(0,2) 
    scheduledProcedureStep.setModality(modalityType);
    log.debug(scriptName + "setting Modality type to: " + modalityType);
}

//scheduledProcedureStep.setModality(get("OBR-3"))
//if (scheduledProcedureStep.getModality() == null) {
//    scheduledProcedureStep.setModality("CT");
//}

// ORC-1 codes: http://hl7-definition.caristix.com:9010/Default.aspx?version=HL7%20v2.5.1&table=0119
def orderControlCode = get("ORC-1")

if (orderControlCode != null) {

    switch (orderControlCode) {

      case 'NW':
         log.debug(scriptName + "Order Control Code = NW (New order/service)")
         scheduledProcedureStep.setScheduledProcedureStepStatus('SCHEDULED')
         break

      case 'XO':
         log.debug(scriptName + "Order Control Code = XO (Change order/service request)")
         break

      default:
         log.debug(scriptName + "about to set Scheduled Procedure Step Status to SCHEDULED (Default)")
         scheduledProcedureStep.setScheduledProcedureStepStatus('SCHEDULED')
         break     

    }
}

scheduledProcedureStep.setScheduledProcedureStepLocation(get("PV1-3-1"))

// Order status could come in either ORC-5 or ORC-16
// possible values are: Ordered, Exam Started, Exam Completed, Preliminary, Final, Cancelled

def orderControlCodeReason = get("ORC-16")
if (orderControlCodeReason != null) {

    log.debug(scriptName + "ORC-16 segment has orderControlCodeReason = {}", orderControlCodeReason)

    switch (orderControlCodeReason) {
        case 'Ordered':
            log.debug(scriptName + "about to set Scheduled Procedure Step Status to SCHEDULED")
            scheduledProcedureStep.setScheduledProcedureStepStatus('SCHEDULED')
            break
        case 'Exam Started':
            log.debug(scriptName + "about to set Scheduled Procedure Step Status to INPROGRESS")
            scheduledProcedureStep.setScheduledProcedureStepStatus('INPROGRESS')
            break
        case 'Exam Completed':
            log.debug(scriptName + "about to set Scheduled Procedure Step Status to COMPLETED")
            scheduledProcedureStep.setScheduledProcedureStepStatus('COMPLETED')
            break
        case 'Preliminary':
            log.debug(scriptName + "about to set Scheduled Procedure Step Status to COMPLETED (Preliminary Report)")
            scheduledProcedureStep.setScheduledProcedureStepStatus('COMPLETED')
            break
        case 'Final':
            log.debug(scriptName + "about to set Scheduled Procedure Step Status to COMPLETED (Final Report)")
            scheduledProcedureStep.setScheduledProcedureStepStatus('COMPLETED')
            break
        case 'Cancelled':
            log.debug(scriptName + "about to set Scheduled Procedure Step Status to DISCONTINUED")
            scheduledProcedureStep.setScheduledProcedureStepStatus('DISCONTINUED')
            break
        default:
            log.debug(scriptName + "about to set Scheduled Procedure Step Status to SCHEDULED (Default)")
            scheduledProcedureStep.setScheduledProcedureStepStatus('SCHEDULED')
            break
    }

} else {
    log.debug( scriptName + "ORC-16 segment is not set...");
    scheduledProcedureStep.setScheduledProcedureStepStatus('SCHEDULED')
}

// NOTE: workaround for
// getOrderEnteringLocation() method extract ORC-17 (OrderEntererOrganization) and should use ORC-13-1 instead
imagingServiceRequest.setOrderEnteringLocation(get("ORC-13-1"))

// OBR-6  - Requested Date/Time
// OBR-36 - Scheduled Date/Time
//if (get("OBR-6") != null){
//    scheduledProcedureStep.setScheduledProcedureStepStartDateTime(HL7v2Date.parse(get("OBR-6").toString(), DateTimeZone.getDefault()))
//} else {
//    log.warn(scriptName + "OBR-6 is null. Unknow scheduled date time.")
//}

// OBR-6  - Requested Date/Time
// OBR-36 - Scheduled Date/Time
if (get("OBR-36") != null){
    scheduledProcedureStep.setScheduledProcedureStepStartDateTime(HL7v2Date.parse(get("OBR-36").toString(), DateTimeZone.getDefault()))
} else {
    log.warn(scriptName + "OBR-36 is null. Unknow scheduled date time.")
}

log.info(scriptName + "resulting Imaging Service Request:\n{}", imagingServiceRequest)
