def scriptName = "IA ILM Policy - "

// log.info(scriptName + "START")
log.info(scriptName + "SIUID: {}, about to start processing...", study.getStudyInstanceUID())

def modalities = study.getModalities()

log.info(scriptName + "SIUID: {}, Modalities: {}", study.getStudyInstanceUID(), modalities)

// NOTE: If the production site contains studies used by the
// health check service, these should never be removed from the system.
// e.g. Check if this is a Test study that is used by HealthCheck service.
// StudyInstanceUIDs below are examples only. Change to match production requirements.
if (   study.getStudyInstanceUID() == '10.11.69.30'
    || study.getStudyInstanceUID() == '10.11.100.159'
    || study.getStudyInstanceUID() == '10.11.100.160'
    || study.getStudyInstanceUID() == '10.11.100.161'
    || study.getStudyInstanceUID() == '10.11.100.170'
    || study.getStudyInstanceUID() == '10.11.100.171'
    || study.getStudyInstanceUID() == '10.110.20.177'
    || study.getStudyInstanceUID() == '10.110.20.178'
    || study.getStudyInstanceUID() == '10.110.20.179' ) {
    log.info(scriptName + "SIUID: {} is used by HealthCheck service, skipping...", study.getStudyInstanceUID())
    log.debug(scriptName + "END - Done processing StudyInstanceUID {}", study.getStudyInstanceUID())
    return
}

//if (modality.contains("CT")) {
//    ops.scheduleStudyForward("DESTINATION_AE")
//}

log.debug(scriptName + "END - Done processing StudyInstanceUID {}", study.getStudyInstanceUID())
