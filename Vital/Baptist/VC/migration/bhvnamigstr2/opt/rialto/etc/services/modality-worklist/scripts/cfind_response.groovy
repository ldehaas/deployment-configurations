// MWL DICOM C-Find Response morpher

LOAD('/opt/rialto/etc/services/common/common_bh.groovy')

def scriptName="MWL DICOM C-Find response Morpher - "

log.info(scriptName + "START ...")

callingAET = getCallingAETitle()

log.debug( scriptName + "Calling AET: {}", getCallingAETitle() );
log.debug( scriptName + "input: {}\n", input );

// Possible ScheduledProcedureStepStatus values:
//   DISCONTINUED
//   COMPLETED
//   INPROGRESS
//   SC
//   SCHEDULED

if ("VNA_CACHE".equalsIgnoreCase(callingAET)) {
    log.info(scriptName + "Source is Image Archive, will proceed");
    log.info(scriptName + "END ...")
    return true
}

def scheduledProcedureStepStatus = get("ScheduledProcedureStepSequence/ScheduledProcedureStepStatus");
log.debug( scriptName + "DEBUG: ScheduledProcedureStepStatus = {}", scheduledProcedureStepStatus);

if ( scheduledProcedureStepStatus != null && scheduledProcedureStepStatus == 'COMPLETED' ) {
    log.info( scriptName + "ScheduledProcedureStepStatus = {}, will NOT include this order in DMWL C-Finresponse...", scheduledProcedureStepStatus );
    log.info( scriptName + "END" );
    return false
}

if ( scheduledProcedureStepStatus != null && scheduledProcedureStepStatus == 'DISCONTINUED' ) {
    log.info( scriptName + "ScheduledProcedureStepStatus = {}, will NOT include this order in DMWL C-Finresponse...", scheduledProcedureStepStatus );
    log.info( scriptName + "END" );
    return false
}

validOrder = false

orderEnteringLocation = imagingServiceRequest.getOrderEnteringLocation()
log.debug( scriptName + "OrderenteringLocation: {}", orderEnteringLocation)

// check what AETitle Prefix should be for an OrderEnteringLocation and compare it to calling AETittle Prefix

callingAetPrefix2 = getCallingAETitle().substring(0,2)
expectedAetPrefix2 = BHCommon.GetAetPrefixByFacility2(orderEnteringLocation)

if (callingAetPrefix2 != null && expectedAetPrefix2 != null && callingAetPrefix2 == expectedAetPrefix2) {
    log.debug( scriptName + "OrderEnteringLocation = {}, expected AET Prefix2 = {}, Calling AETitle Prefix2 = {}", orderEnteringLocation, expectedAetPrefix2, callingAetPrefix2)
    validOrder = true
}

callingAetPrefix3 = getCallingAETitle().substring(0,3)
expectedAetPrefix3 = BHCommon.GetAetPrefixByFacility3(orderEnteringLocation)


if (callingAetPrefix3 != null && expectedAetPrefix3 != null && callingAetPrefix3 == expectedAetPrefix3) {
    log.debug( scriptName + "OrderEnteringLocation = {}, expected AET Prefix3 = {}, Calling AETitle Prefix3 = {}", orderEnteringLocation, expectedAetPrefix3, callingAetPrefix3)
    validOrder = true
}

// Neurosurgery Arkansas
if (callingAET != null && callingAET.startsWith("NSA") && (orderEnteringLocation == "10126109" || orderEnteringLocation == "10126") ) {
    log.debug( scriptName + "OrderEnteringLocation = {}, expected AET Prefix3 = {}, Calling AETitle = {}", orderEnteringLocation, 'NSA', callingAET)
    validOrder = true
}

if (validOrder) {
    log.debug( scriptName + "output: {}\n", output );
    log.info(scriptName + "END ...")
    return true
} else {
    log.debug( scriptName + "Not a valid order for the requesting AET / Location");
    log.info(scriptName + "END ...")
    return false
}
