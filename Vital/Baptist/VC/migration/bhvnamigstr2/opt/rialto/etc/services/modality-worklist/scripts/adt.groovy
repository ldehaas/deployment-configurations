// MWL ADT inbound morpher

def scriptName="MWL Inbound ADT Morpher - "

log.info(scriptName + "START ...")
//log.debug(scriptName + "input: \n{}", input)
log.info(scriptName + "input: \n{}", input)

output.set('/.PID-3-4-1', 'BH')
output.set('/.PID-3-4-2', '2.16.124.113638.7.7.1.1')
output.set('/.PID-3-4-3', 'ISO')

//log.debug(scriptName + "output: \n{}", output)
log.info(scriptName + "output: \n{}", output)

log.info(scriptName + "END ...")
