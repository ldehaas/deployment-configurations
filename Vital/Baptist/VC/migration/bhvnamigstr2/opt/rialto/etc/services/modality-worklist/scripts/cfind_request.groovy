// MWL DICOM C-Find morpher

import java.text.SimpleDateFormat

def scriptName="MWL DICOM C-Find request Morpher - "

log.info(scriptName + "START ...")
log.debug(scriptName + "input: \n{}", input)

// workaround for
// Failed to execute CFind
// com.karos.rialto.mwl.exception.MWLQueryException: com.karos.rialto.mwl.exception.MWLQueryException: Cannot execute unconstrained query

df = new SimpleDateFormat("yyyyMMdd")
today = df.format(new Date())

log.info(scriptName + "This is the host: {}", host)
log.info(scriptName + "Calling AE: {}", getCallingAETitle())
log.info(scriptName + "Current date: {}", today)

scheduledProcedureStepSequenceItems = get(ScheduledProcedureStepSequence)

if (scheduledProcedureStepSequenceItems != null) {

    scheduledProcedureStepSequenceItems.each { scheduledProcedureStepSequenceItem ->
        if ( scheduledProcedureStepSequenceItem.get(ScheduledProcedureStepStartDate) != null ) {
            log.info(scriptName + "Discovered another ScheduledProcedureStepStartDate {}", scheduledProcedureStepSequenceItem.get(ScheduledProcedureStepStartDate))
        } else {
            log.info(scriptName + "ScheduledProcedureStepStartDate is NULL, will use {}", today)
            scheduledProcedureStepSequenceItem.set(ScheduledProcedureStepStartDate, today)
        }
    }
} else {
        log.info(scriptName + "ScheduledProScheduledProcedureStepSequencecedureStep is NULL...")
}

log.debug(scriptName + "output: \n{}", output)
log.info(scriptName + "END ...")
