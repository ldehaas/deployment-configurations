class BHCommon {

    private static Def_iPid = 'BH'
    private static Def_iPid_UnivId = '2.16.124.113638.7.7.1.1'
    private static Def_UnivEntType = 'ISO'

    private static Def_Institution = 'BHMC-LITTLE ROCK'
    private static Def_Facility = '300'
    private static Def_UnknowAETPrefix = 'XYZ'

    private static final aet_to_institution_map = [
        'BNL': 'BHMC-NORTH LITTLE ROCK',
        'BLR': 'BHMC-LITTLE ROCK',
        'BAR': 'BHMC-ARKADELPHIA',
        'BSG': 'BHMC-STUTTGART',
        'BCW': 'BHMC-CONWAY',
        'BMV': 'BHMC-HOT SPRING COUNTY',
        'BHS': 'BHMC-HEBER SPRINGS',
        'BFS': 'BHMC-FORT SMITH',
        'BVB': 'BHMC-VAN BUREN'
    ]

    private static final facility_to_aet_2char_prefix_map = [
        '520': 'MV'
    ]

    private static final facility_to_aet_3char_prefix_map = [
        '100': 'BLR',
        '200': 'BNL',
        '300': 'BLR',
        '400': 'BAR',
        '500': 'BSG',
        '510': 'BCW',
        '520': 'BMV',
        '600': 'BHS',
        '700': 'BFS',
        '800': 'BLR'
    ]

    private static final institution_to_facility_map = [
        'BAPTIST HEALTH REHABILITATION': '100',
        'BHMC-NORTH LITTLE ROCK': '200',
        'BHMC-LITTLE ROCK': '300',
        'BHMC-ARKADELPHIA': '400',
        'BHMC-STUTTGART': '500',
        'BHMC-CONWAY': '510',
        'BHMC-HOT SPRING COUNTY': '520',
        'BHMC-HEBER SPRINGS': '600',
        'BHMC-FORT SMITH': '710',
        'BHMC-VAN BUREN': '720',
        'BHECH-BAPTIST HEALTH EXTENDED CARE HOSPITAL-LR': '800',
        'BAPTIST HEALTH AMBULATORY': '10126109',
        'BH OUTSIDE EXAMS': '999'
    ]

    static GetInstitutionbyAET(AET){
        def vInstitution = aet_to_institution_map[AET]
        if (vInstitution == null) {
            vInstitution = Def_Institution
        }
        return vInstitution
    }

    static GetFacilitybyInstitution(Institution){
        def vFacility = institution_to_facility_map[Institution]
        if (vFacility == null) {
            vFacility = Def_Facility
        }
        return vFacility
    }

    static GetAetPrefixByFacility2(FacilityCode){
        def vAETPrefix = facility_to_aet_2char_prefix_map[FacilityCode]
        if (vAETPrefix == null) {
            vAETPrefix = Def_UnknowAETPrefix
        }
        return vAETPrefix
    }

    static GetAetPrefixByFacility3(FacilityCode){
        def vAETPrefix = facility_to_aet_3char_prefix_map[FacilityCode]
        if (vAETPrefix == null) {
            vAETPrefix = Def_UnknowAETPrefix
        }
        return vAETPrefix
    }

}

