// MWL DICOM C-Find morpher

import java.text.SimpleDateFormat

LOAD('/opt/rialto/etc/services/common/common_bh.groovy')

def scriptName="MWL DICOM C-Find request Morpher - "

log.info(scriptName + "START ...")
log.debug(scriptName + "input: \n{}", input)

// workaround for
// Failed to execute CFind
// com.karos.rialto.mwl.exception.MWLQueryException: com.karos.rialto.mwl.exception.MWLQueryException: Cannot execute unconstrained query

df = new SimpleDateFormat("yyyyMMdd")
today = df.format(new Date())

callingAET = getCallingAETitle()

log.info(scriptName + "This is the host: {}", host)
log.info(scriptName + "Calling AET: {}", getCallingAETitle())
log.info(scriptName + "Current date: {}", today)

scheduledProcedureStepSequenceItems = get(ScheduledProcedureStepSequence)

if (scheduledProcedureStepSequenceItems != null) {

    def aetModalityTypes = []

    if (callingAET != null) {
        aetModalityTypes = BHCommon.getAetMultiModalityTypes(callingAET)
        //log.info(scriptName + "multi-modality list: {}", aetModalityTypes)
    }

    scheduledProcedureStepSequenceItems.each { scheduledProcedureStepSequenceItem ->

        if ( scheduledProcedureStepSequenceItem.get(ScheduledProcedureStepStartDate) != null ) {
            log.info(scriptName + "Discovered another ScheduledProcedureStepStartDate {}", scheduledProcedureStepSequenceItem.get(ScheduledProcedureStepStartDate))
        } else {
            log.info(scriptName + "ScheduledProcedureStepStartDate is NULL, will use {}", today)
            scheduledProcedureStepSequenceItem.set(ScheduledProcedureStepStartDate, today)
        }

        // strip out Modality if a request comes from one of AETs that have multi-modality mapping
        // this will allow MWL to find all orders based on the date/time range and not filter by Modality
        // filtering based on Modality will be done in C-Find response morpher

        // Various ways to get modality type
        // scheduledProcedureStepSequenceItem.get(Modality))
        // scheduledProcedureStepSequenceItem.get(tag(0x0008, 0x0060))

        if( aetModalityTypes ) {
            log.info(scriptName + "CallingAET: {}, requested Modality type; {}, multi-modality list: {}", callingAET, scheduledProcedureStepSequenceItem.get(Modality), aetModalityTypes)
            log.info(scriptName + "CallingAET {} is on multi-modality list, will strip out Modality tag (0008,0060)", callingAET)
            scheduledProcedureStepSequenceItem.set(Modality, null)
        } else {
            log.info(scriptName + "CallingAET is NOT on multi-modality list")
        }

    }
} else {
    log.info(scriptName + "ScheduledProcedureStepSequenceItems is NULL...")
}

log.debug(scriptName + "output: \n{}", output)
log.info(scriptName + "END ...")
