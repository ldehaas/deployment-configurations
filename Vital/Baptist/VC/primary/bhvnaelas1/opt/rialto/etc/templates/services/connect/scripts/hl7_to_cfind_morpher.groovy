/*
 * hl7_to_cfind_morpher.groovy
 *
 * This script is used in the <prop name="FetchPriors">/<hl7ToCFindMorpher> tag.
 *
 * Extract details from an inbound HL7 message into the DICOM C-FIND that
 * will be used to fetch relevant priors from each DICOM source. 
 *
 * To access the DICOM C-FIND object, use the "set()" method. In order to set a 
 * query parameter value in the DICOM C-FIND object set the value to what you 
 * wish to query by.
 * For example, to set the value of PatientId: 
 * 
 * set("PatientId","12345")
 * OR
 * set(PatientId,"12345")
 *
 * In order to request information be returned in the C-FIND response from 
 * the source, set the value to be blank or null.
 * For example, to request StudyDate from the source:
 * 
 * set("StudyDate",null)
 * OR
 * set(StudyDate,"")
 *
 * To retrieve values from the HL7 message, use the "get()" method.
 * For example, to retrieve the patientID from the HL7 message to a variable:
 *
 * def pid = get("PID-3-1")
 * 
 * This script may optionally return a boolean value (either true or false). If
 * the return value of the script is false then no C-FIND will be sent to any
 * DICOM source and the fetching of relevant priors will not be completed.
 *
 */

 
log.info("hl7_to_cfind_morpher.groovy: Start HL7 to CFind morpher")

/*
 * Retrieve the fully Patient ID from the HL7 message.
 */
def pid = get("PID-3-1")

/*
 * At this point in the workflow, we may examine the PatientID and choose stop
 * the fetching of priors. For example, if we wish to stop the workflow due to 
 * the patientId being prefixed with a certain value, we may do the following: 
 *
 *  if (pid.startsWith("000") || pid.startsWith("111")) {
 *    log.debug("hl7_to_cfind_morpher.groovy: Pid '{}' is to be ignored. " +
 				"Ignoring order message.")
 *    return false;
 *  }
 *
 */

/*
 * These calls will retrieve the fully qualified Issuer of Patient ID details 
 * from the HL7 message.
 */
def namespace = get("PID-3-4-1")
def universalID = get("PID-3-4-2")
def universalType = get("PID-3-4-3")

/*
 * If the Issuer of Patient ID details from the HL7 message are not complete,
 * then we want to set the value to empty strings so that we do not throw
 * a null pointer exception when accessing the variable.
 */
if (namespace == null) {
    namespace = ""
}
if (universalID == null) {
    universalID = ""
}
if (universalType == null) {
    universalType = ""
}

log.info("hl7_to_cfind_morpher.groovy: Found pid '{}' and issuer '{}&{}&{}'", 
	pid, namespace, universalID, universalType)

def issuer = namespace+"&"+universalID+"&"+universalType

log.info("hl7_to_cfind_morpher.groovy: The issuer is '{}'", issuer)

/*
 * This will set the the query parameters of our DICOM C-FIND to the source
 * DICOM AE's configured in the XDS service
 */
set(PatientID, pid)
set(IssuerOfPatientID, issuer)

/*
 * By setting these values to "null" in the DICOM C-FIND request, the 
 * destination AE should fill in these tags in each C-FIND response object so
 * that we may inspect them later in the workflow.
 */
set("StudyDate", null)
set("NumberOfStudyRelatedSeries", null)


log.info("hl7_to_cfind_morpher.groovy: Ending HL7 to CFind morpher")
