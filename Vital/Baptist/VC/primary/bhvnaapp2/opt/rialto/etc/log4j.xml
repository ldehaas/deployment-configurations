<?xml version="1.0" encoding="UTF-8"?>
<!DOCTYPE log4j:configuration SYSTEM "log4j.dtd">
<log4j:configuration xmlns:log4j="http://jakarta.apache.org/log4j/" debug="false">
    <!-- NB: do not use log4j's ConsoleAppender here.  Rialto redirects stdout
        and stderr to the logging system and printing logs back to stdout
        creates a loop. -->

    <appender name="MASTER_LOG" class="org.apache.log4j.RollingFileAppender">
        <param name="File" value="${rialto.logdir}/rialto.log" />
        <param name="Append" value="true" />
        <param name="MaxFileSize" value="100MB" />
        <param name="MaxBackupIndex" value="9" />
        <layout class="org.apache.log4j.PatternLayout">
            <param name="ConversionPattern" value="%d,%p,%c - [%t] %m\n" />
        </layout>
    </appender>

    <appender name="admintools" class="org.apache.log4j.RollingFileAppender">
        <param name="File" value="${rialto.logdir}/admin-tools.log" />
        <param name="Append" value="true" />
        <param name="MaxFileSize" value="100MB" />
        <param name="MaxBackupIndex" value="9" />
        <layout class="org.apache.log4j.PatternLayout">
            <param name="ConversionPattern" value="%d,%p,%c - [%t] %m\n" />
        </layout>
    </appender>

    <appender name="ILM" class="org.apache.log4j.RollingFileAppender">
        <param name="File" value="${rialto.logdir}/ilm.log" />
        <param name="Append" value="true" />
        <param name="MaxFileSize" value="100MB" />
        <param name="MaxBackupIndex" value="9" />
        <layout class="org.apache.log4j.PatternLayout">
            <param name="ConversionPattern" value="%d,%p,%c - [%t] %m\n" />
        </layout>
    </appender>

    <appender name="events" class="org.apache.log4j.RollingFileAppender">
        <param name="File" value="${rialto.logdir}/event.log" />
        <param name="Append" value="true" />
        <param name="MaxFileSize" value="100MB" />
        <param name="MaxBackupIndex" value="9" />
        <layout class="org.apache.log4j.PatternLayout">
            <param name="ConversionPattern" value="%m\n" />
        </layout>
    </appender>

    <appender name="metrics" class="org.apache.log4j.RollingFileAppender">
        <param name="File" value="${rialto.logdir}/metrics.log" />
        <param name="Append" value="true" />
        <param name="MaxFileSize" value="100MB" />
        <param name="MaxBackupIndex" value="9" />
        <layout class="org.apache.log4j.PatternLayout">
            <param name="ConversionPattern" value="%m\n" />
        </layout>
    </appender>

    <!-- Uncomment (1 of 2) to use local instance of Logstash -->

    <!-- delete-line-to-enable-logstash
    <appender name="LOGSTASH" class="org.apache.log4j.AsyncAppender">
      <param name="Blocking" value="false" />
      <param name="BufferSize" value="500" />
      <appender-ref ref="AppendRemote" />
    </appender>

    <appender name="AppendRemote" class="org.apache.log4j.net.SocketAppender">
        <param name="RemoteHost" value="0.0.0.0" />
        <param name="ReconnectionDelay" value="60000" />
        <param name="Threshold" value="DEBUG" />
    </appender>
    delete-line-to-enable-logstash -->

    <appender name="CQL3" class="org.apache.log4j.RollingFileAppender">
        <param name="File" value="${rialto.logdir}/cql3.log"/>
        <param name="Append" value="true"/>
        <param name="MaxFileSize" value="100MB"/>
        <param name="MaxBackupIndex" value="9"/>
        <!-- <layout class="org.apache.log4j.PatternLayout"> -->
        <layout class="com.karos.log4j.ColoredPatternLayout">
            <param name="ConversionPattern" value="%d,%p,%c - [%t] %m\n"/>
        </layout>
    </appender>

    <appender name="perf" class="org.apache.log4j.RollingFileAppender">
        <param name="File" value="${rialto.logdir}/rialto_perf.log" />
        <param name="Append" value="true" />
        <param name="MaxFileSize" value="25MB" />
        <param name="MaxBackupIndex" value="5" />
        <layout class="org.apache.log4j.PatternLayout">
            <param name="ConversionPattern" value="%d,%p,%c - %m\n" />
        </layout>
    </appender>

    <appender name="health-check" class="org.apache.log4j.RollingFileAppender">
        <param name="File" value="${rialto.logdir}/rialto_health_check.log" />
        <param name="Append" value="true" />
        <param name="MaxFileSize" value="25MB" />
        <param name="MaxBackupIndex" value="5" />
        <layout class="org.apache.log4j.PatternLayout">
            <param name="ConversionPattern" value="%d,%p,%c - [%t] %m\n" />
        </layout>
    </appender>

    <!-- Any services that want to back up CAStor tickets should log them here -->
    <appender name="tickets" class="org.apache.log4j.FileAppender">
        <!-- Note the location outside of the log directory.  This file should
             NOT be deleted, even if cleaning up the rest of the log files.
             There is important data in this file.  It should not roll over
             and delete old files. -->
        <param name="File" value="${rialto.rootdir}/var/rialto_tickets.log" />
        <param name="Append" value="true" />
        <layout class="org.apache.log4j.PatternLayout">
            <param name="ConversionPattern" value="%m\n" />
        </layout>
    </appender>

    <category name= "com.karos.rialto.imagearchive.performance" additivity="false">
        <appender-ref ref="perf"/>
    </category>

    <category name= "com.karos.rialtohealthcheck.http.HealthCheckResource" additivity="false">
        <appender-ref ref="health-check"/>
    </category>

    <!-- all ticket logs should have their category "tickets" prefix -->
    <category name="tickets" additivity="false">
        <appender-ref ref="tickets" />
    </category>

    <!-- These libraries tend to be a bit chatty at info -->
    <category name="ca.uhn.hl7v2">
        <priority value="warn" />
    </category>
    <category name="com.mchange.v2">
        <priority value="warn" />
    </category>
    <category name="httpclient.wire.content">
        <priority value="warn" />
    </category>
    <category name="navigator.blazeds">
        <priority value="warn" />
    </category>
    <category name="org.apache.commons.httpclient">
        <priority value="warn" />
    </category>
    <category name="org.dcm4che2.net">
        <priority value="warn" />
    </category>
    <category name="org.hibernate">
        <priority value="warn" />
    </category>
    <category name="org.openhealthtools.ihe">
        <priority value="warn" />
    </category>

    <!-- Make sure these libraries don't get into debug if we put root at debug
         because they spew way too much useless stuff -->
    <category name="org.apache.axiom">
        <priority value="info" />
    </category>
    <category name="org.apache.axis2">
        <priority value="info" />
    </category>
    <category name="org.mortbay.log">
        <priority value="info" />
    </category>
    <category name="org.apache.fop">
        <priority value="info" />
    </category>
    <category name="org.apache.http">
        <priority value="info" />
    </category>
    <category name="com.datastax.driver">
        <priority value="warn" />
    </category>
    <category name="io.searchbox">
        <priority value="info" />
    </category>

    <category name="org.openhealthtools.ihe.xds.metadata.extract.EbXML_3_0DocumentEntryExtractor">
        <!-- this class warns about extended metadata -->
        <priority value="ERROR" />
    </category>

    <category name="org.hibernate.ejb.Ejb3Configuration">
        <!-- this class warns about
         'hibernate.connection.autocommit = false break the EJB3 specification' -->
        <priority value="ERROR" />
    </category>

    <category name="com.datastax.driver" additivity="false">
        <priority value="warn"/>
        <appender-ref ref="CQL3"/>
    </category>

    <category name="com.karos.cql3" additivity="false">
        <priority value="debug"/>
        <appender-ref ref="CQL3"/>
    </category>

    <!-- Categories for the Rialto UI Admin Tools -->
    <category name="rialto.ui.active-resource" additivity="false">
        <priority value="info" />
        <appender-ref ref="admintools" />
    </category>

    <category name="rialto.ui.admin-tools" additivity="false">
        <priority value="info" />
        <appender-ref ref="admintools"/>
    </category>

    <category name="rialto.ui.assets" additivity="false">
        <priority value="warn" />
        <appender-ref ref="admintools"/>
    </category>


    <!-- Imaging Lifecycle Manager -->
    <category name="com.karos.rialto.imagearchive.ilm" additivity="false">
        <priority value="info" />
        <appender-ref ref="ILM"/>
    </category>

<!--
    <category name="com.karos.ids">
        <priority value="trace" />
    </category>
    <category name="service">
        <priority value="trace" />
    </category>
    <category name= "service.ids1.cache.janitor">
        <priority value="debug" />
    </category>
-->
    <!-- Additional chatty libraries that we usually don't need to see logs for -->
    <!-- Turn down extra logging from these categories since they are not usually useful -->
    <category name="com.karos.arr2">
        <priority value="info" />
    </category>

    <category name="service.arr">
        <priority value="info" />
    </category>

    <category name="io.netty.util.internal">
        <priority value="info" />
    </category>

    <!-- Useful for seeing queries against elasticsearch -->
    <category name="io.searchbox.client">
        <priority value="info" />
    </category>

    <category name="service.usermanagement" additivity="false">
        <priority value="info" />
        <appender-ref ref="events"/>
    </category>

    <category name="service.empi">
        <priority value="debug" />
    </category>

    <category name="service.modality-worklist">
        <priority value="debug" />
    </category>

    <category name="com.karos.groovy.morph.HL7ToHL7">
        <priority value="debug"/>
    </category>

    <category name="com.karos.groovy.morph.HL7ToDCM">
        <priority value="debug"/>
    </category>

    <category name="com.karos.groovy.morph.StudyMetadataToHL7">
        <priority value="debug"/>
    </category>

    <category name="com.karos.groovy.morph.StudyMetadataToStudyMetadata">
        <priority value="debug"/>
    </category>

    <category name="com.karos.rialto.imagearchive.mint.stream.MintBuilderInputHandler">
        <priority value="info" />
    </category>

    <category name="com.karos.cql3.persistence.DistributedIndexManager">
        <priority value="info" />
    </category>

    <category name="com.karos.rialto.imagearchive.mint.stream.EncapsulatedPixelIndexer">
        <priority value="info" />
    </category>

    <category name="net.sf.ehcache.store">
        <priority value="info" />
    </category>

    <category name="com.karos.rtk.syslog.rcv.UdpSyslogServer">
        <priority value="info" />
    </category>

    <category name="org.apache.commons.beanutils.converters">
        <priority value="info" />
    </category>

    <category name="service.image-archive.storage.collector.archivingpolling">
        <priority value="info" />
    </category>

    <category name="org.dcm4che3.io.DicomInputStream">
        <priority value="error" />
    </category>

    <!-- ARR is extra noisy, so don't turn this on unless you really need to see all the ARR/Cassandra interactions. -->
    <!-- Note: The final category component "audit" is actually the keyspace name. If this name changes, this config will also need to be updated-->
    <category name="com.karos.cql3.performance.StopWatch.audit">
        <priority value="info" />
    </category>
    <category name="com.karos.cql3.persistence.EntityManager.audit">
        <priority value="info" />
    </category>

    <category name="com.karos.rialto.events.Log4jEventLedger">
        <priority value="off" />
        <appender-ref ref="events"/>
    </category>

    <category name="metrics" additivity="false">
        <priority value="off" />
        <appender-ref ref="metrics"/>
    </category>

    <root>
        <priority value="info" />
        <appender-ref ref="MASTER_LOG" />
        <!-- Uncomment (2 of 2) to use Logstash -->

        <!-- delete-line-to-enable-logstash
        <appender-ref ref="LOGSTASH" />
        delete-line-to-enable-logstash -->
    </root>
</log4j:configuration>
