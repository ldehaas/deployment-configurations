/*
 * cfind_response_morpher.groovy
 *
 * This script is used in the 
 * <prop name="Sources">/<dicom>/<cfindResponseMorpher> tag.
 *
 * This morpher can be configured on each DICOM source to filter out studies. 
 * This is run once for each DICOM C-FIND response. 

 * To access the DICOM C-FIND object, use the "get()" method.
 * For example, to get the value of PatientId: 
 * 
 * get("PatientId")
 * OR
 * get(PatientId)
 *
 * In this example, if we know that a particular DICOM source returns TEST as  
 * the Patient issuer, we will not want to use this DICOM response. By returning 
 * false, the object is ignored.
 *
 * This script may optionally return a boolean value (either true or false). If
 * the return value of the script is false then the C-FIND response for this 
 * study will be ignored and not used.
 *
 */

log.info("cfind_response_morpher.groovy: Starting cfind_response_morpher")

def issuer = get(IssuerOfPatientId)

if (issuer == "TEST") {
    log.error("cfind_response_morpher.groovy: The issuer is a test value, do not fetch")
    return false
}

log.info("cfind_response_morpher.groovy: Ending cfind_response_morpher")
