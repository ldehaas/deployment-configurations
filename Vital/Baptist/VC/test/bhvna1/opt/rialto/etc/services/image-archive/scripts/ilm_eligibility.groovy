def scriptName = "ILM Eligibility - "

def localDomainUUIDlist = [
     "2.16.124.113638.7.7.0.1"
];

log.info(scriptName + "AccN: {}, START", accessionNumber)
log.info(scriptName + "AccN: {}, LocalPID = {}, DomainUUID = {}", accessionNumber, localPid, localPid.domainUUID)

// If the given study is from a domain that "belongs" to the local data center,
// the script returns true, and the ILM will operate on the study:
if (localPid != null && localDomainUUIDlist.contains(localPid.domainUUID)) {
    log.info(scriptName + "AccN: {}, found eligible study, will be processed by ILM", accessionNumber)
    log.info(scriptName + "AccN: {}, END", accessionNumber)
    return true;

} else {
    log.info(scriptName + "AccN: {}, not an eligible study, will NOT be processed by ILM", accessionNumber)
    log.info(scriptName + "AccN: {}, END", accessionNumber)
    return false;
}

log.info(scriptName + "AccN: {}, END", accessionNumber)

