/* cfind.groovy
 *
 * This script is intended to morph the IssuerOfPatientID tag of an inbound
 * C-FIND request when this tag is not present.
 *
 */

def scriptName = "IA C-Find Request Morpher: "

log.debug(scriptName + "START")

log.debug(scriptName + "Checking IssuerOfPatientID")

def issuerOfPatientId = get("IssuerOfPatientID")

if (issuerOfPatientId == null) {
    set("IssuerOfPatientID", "BH")
    set("IssuerOfPatientIDQualifiersSequence/UniversalEntityID", '2.16.124.113638.7.7.0.1')
    set("IssuerOfPatientIDQualifiersSequence/UniversalEntityIDType", 'ISO')
    log.debug(scriptName + "Morphed IssuerOfPatientID to be ")
    log.debug(scriptName + "namespace = {}, domain = {}, type = {}", 
        get("IssuerOfPatientID"), 
        get("IssuerOfPatientIDQualifiersSequence/UniversalEntityID"), 
        get("IssuerOfPatientIDQualifiersSequence/UniversalEntityIDType"))
}

log.debug(scriptName + "END")
