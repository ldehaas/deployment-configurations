import org.joda.time.DateTime
import org.joda.time.format.DateTimeFormat
import java.text.SimpleDateFormat

LOAD('/opt/rialto/etc/services/common/common_bh.groovy')

def scriptName = "IA SCN Morpher (VNA_MWL): ";

log.debug(scriptName + "START");

def studyDate = input.get(StudyDate)

def proceedWithORM = false

switch (type) {
    case "ARCHIVE":
        proceedWithORM = true
        break
    case "RESTORE":
        proceedWithORM = false
        break
    case "LOGICAL_DELETE":
        proceedWithORM = false
        break
    case "PHYSICAL_DELETE":
        proceedWithORM = false
        break
    case "CATALOG":
        proceedWithORM = false
        break
    default:
        proceedWithORM = false
        break
}

if (!proceedWithORM) {
    log.info(scriptName + "archiving status is {}, and studyDate is {}, will NOT send Order Update to MWL...", type, studyDate)
    log.info(scriptName + "END")
    return false
}


initialize( 'ORM', 'O01', '2.3' );
output.getMessage().addNonstandardSegment('IPC')
output.getMessage().addNonstandardSegment('ZDS')

//log.debug(scriptName + "input is: \n{}", input);

def callingAET = getCallingAETitle();
def calledAET = getCalledAETitle();

log.debug(scriptName + "CallingAET: {}, CalledAET: {}, and ModalitiesInStudy: {}", callingAET,  calledAET, input.get(ModalitiesInStudy));

log.debug(scriptName + "StudyVerifiedDate: {}, StudyVerifiedTime: {}", input.get(StudyVerifiedDate), input.get(StudyVerifiedTime));

log.debug(scriptName + "InstitutionName: {}", input.get(InstitutionName));

set('MSH-3', 'VNA_CACHE_T')
set('MSH-4', 'BH')
set('MSH-5', 'VNA_MWL')

log.debug(scriptName + "about to set ReceivingFacility to {}", BHCommon.GetFacilitybyInstitution(input.get(InstitutionName)) );
set('MSH-6', BHCommon.GetFacilitybyInstitution(input.get(InstitutionName)))

sdf = new SimpleDateFormat("yyyyMMddHHmmss")
set('MSH-7', sdf.format(new Date()))

set('PID-3-1', input.get(PatientID));
set('PID-3-4-1', input.get(IssuerOfPatientID))
set('PID-3-4-2', input.get("IssuerOfPatientIDQualifiersSequence/UniversalEntityID"))
set('PID-3-4-3', input.get("IssuerOfPatientIDQualifiersSequence/UniversalEntityIDType"))
setPersonName('PID-5', input.get(PatientName));

set('ORC-1', 'XO')
set('ORC-17','VNA_CACHE_T')

set('OBR-1', '1')
set('OBR-2', input.get(PlacerOrderNumberImagingServiceRequest))
set('OBR-3', input.get(AccessionNumber))

//set('OBR-4-1',  input.get(RequestedProcedureID))

set('OBR-4-2', input.get(StudyDescription))

def ReqPhysician = input.get(RequestingPhysician)
if (ReqPhysician != null && ReqPhysician != '') {
    if (ReqPhysician.contains("\\^")) {
        def name_parts = ReqPhysician.split("\\^")
        if (name_parts.length == 3) {
            // assuming requesting physician name follows "code^lastname^firstname format"
            set('OBR-16-1', name_parts[0])
            set('OBR-16-2', name_parts[1])
            set('OBR-16-3', name_parts[2])
            }else{
            set('OBR-16', ReqPhysician)
            }
    }else{
        set('OBR-16', ReqPhysician)
    }
}

set('OBR-25', 'I')

set('OBX-1', '1')
set('OBX-2', 'TX')
set('OBX-3-1-2', 'GDT')
set('OBX-5', 'Y')
set('OBX-9', input.get(StudyInstanceUID))

//set('IPC-1', input.get(AccessionNumber))
//set('IPC-3', input.get(StudyInstanceUID))

log.debug(scriptName + "output is: \n{}", output);

if ( !"10126109".equalsIgnoreCase(input.get(OrderEntererLocation)) ) {
    log.debug(scriptName + "OrderEntererLocation {} does not correspond to Neurosurgery Arkansas, will NOT send SCN to MWL", input.get(OrderEntererLocation))
    log.debug(scriptName + "END");
    return false
} else {
    log.debug(scriptName + "Setting Order Control Code Reason in ORC-16 to Exam Completed")
    set('ORC-16', 'Exam Completed')
}

log.debug(scriptName + "END");
