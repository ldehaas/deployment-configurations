// MWL HL7 ADT outbound morpher

def scriptName="MWL HL7 ADT Outbound Morpher - "

log.info(scriptName + "START ...")
log.info(scriptName + "input: \n{}", input)

// your custom code goes here

log.info(scriptName + "output: \n{}", output)

log.info(scriptName + "END ...")
