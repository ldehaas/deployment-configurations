class BHCommon {

    private static Def_iPid = 'BH'
    private static Def_iPid_UnivId = '2.16.124.113638.7.7.0.1'
    private static Def_UnivEntType = 'ISO'

    private static Def_Institution = 'BHMC-LITTLE ROCK'
    private static Def_Facility = '300'
    private static Def_UnknowAETPrefix = 'XYZ'

    private static final aet_to_institution_map = [
        'BNL': 'BHMC-NORTH LITTLE ROCK',
        'BLR': 'BHMC-LITTLE ROCK',
        'BAR': 'BHMC-ARKADELPHIA',
        'BSG': 'BHMC-STUTTGART',
        'BCW': 'BHMC-CONWAY',
        'BMV': 'BHMC-HOT SPRING COUNTY',
        'BHS': 'BHMC-HEBER SPRINGS',
        'BFS': 'BHMC-FORT SMITH',
        'BVB': 'BHMC-VAN BUREN'
    ]

    private static final facility_to_aet_2char_prefix_map = [
        '520': 'MV'
    ]

    private static final facility_to_aet_3char_prefix_map = [
        '100': 'BLR',
        '200': 'BNL',
        '300': 'BLR',
        '400': 'BAR',
        '500': 'BSG',
        '510': 'BCW',
        '520': 'BMV',
        '600': 'BHS',
        '700': 'BFS',
        '800': 'BLR'
    ]

    private static final institution_to_facility_map = [
        'BAPTIST HEALTH REHABILITATION': '100',
        'BHMC-NORTH LITTLE ROCK': '200',
        'BHMC-LITTLE ROCK': '300',
        'BHMC-ARKADELPHIA': '400',
        'BHMC-STUTTGART': '500',
        'BHMC-CONWAY': '510',
        'BHMC-HOT SPRING COUNTY': '520',
        'BHMC-HEBER SPRINGS': '600',
        'BHMC-FORT SMITH': '710',
        'BHMC-VAN BUREN': '720',
        'BHECH-BAPTIST HEALTH EXTENDED CARE HOSPITAL-LR': '800',
        'BAPTIST HEALTH AMBULATORY': '10126109',
        'BH OUTSIDE EXAMS': '999'
    ]

    private static final aet_to_facility_map = [
         'ARKMC':    ['10009100']
        ,'ARKORTH':  ['10172100']
        ,'ARKWCUS':  ['10042100']
        ,'AWCUS':    ['10606100']
        ,'BBFC':     ['10044100']
        ,'BENFMC':   ['10094101', '10094104']
        ,'BHFCW':    ['10024100']
        ,'BNLSC':    ['10128100']
        ,'BNLWCUS':  ['10185103']
        ,'CWFMC':    ['10089100']
        ,'CWORTH':   ['10166102']
        ,'CWORTHUS': ['10166102']
        ,'CWWCUS':   ['10166101']
        ,'FCBHD':    ['10126103']
        ,'FCMV':     ['10017100']
        ,'FCOC':     ['10019100']
        ,'GMEFM':    ['10185100', '10185101', '10185102']
        ,'GMEXR':    ['10185100', '10185101', '10185102']
        ,'IPCCUS':   ['10126112']
        ,'LAKEWOOD': ['10016100']
	,'LRSCUS1':  ['10133101']
        ,'MTIIMFUS': ['10127100']
        ,'NSAMT1DR': ['10126109']
        ,'NSAMTIRF': ['10126109']
        ,'STGWCUS':  ['10119103']
    ]


    // CR - Computed Radiography (plate)
    // DR - Digital Radiography (detector)
    // XR - Diagnostic X-ray

    private static final aet_to_modality_type_map = [
         'ARKMC':     ['XR','DR']
        ,'ARKORTH':   ['XR','DR']
        ,'BBFC':      ['XR','CR']
        ,'BENFMC':    ['XR','CR']
        ,'BHFCW':     ['XR','CR']
        ,'BNLSC':     ['XR','CR']
        ,'CWFMC':     ['XR','DR']
        ,'FCBHD':     ['XR','CR']
        ,'FCMV':      ['XR','CR']
        ,'FCOC':      ['XR','CR']
        ,'GMEFM':     ['US']
        ,'GMEXR':     ['XR']
        ,'LAKEWOOD':  ['XR','CR']
	,'LRSCUS1':   ['US']
        ,'NSAMTIRF':  ['XR','RF','FL']
    ]


    static getInstitutionbyAET(AET){
        def vInstitution = aet_to_institution_map[AET]
        if (vInstitution == null) {
            vInstitution = Def_Institution
        }
        return vInstitution
    }

    static getFacilitybyInstitution(Institution){
        def vFacility = institution_to_facility_map[Institution]
        if (vFacility == null) {
            vFacility = Def_Facility
        }
        return vFacility
    }

    static getAetPrefixByFacility2(FacilityCode){
        def vAETPrefix = facility_to_aet_2char_prefix_map[FacilityCode]
        if (vAETPrefix == null) {
            vAETPrefix = Def_UnknowAETPrefix
        }
        return vAETPrefix
    }

    static getAetPrefixByFacility3(FacilityCode){
        def vAETPrefix = facility_to_aet_3char_prefix_map[FacilityCode]
        if (vAETPrefix == null) {
            vAETPrefix = Def_UnknowAETPrefix
        }
        return vAETPrefix
    }

    static getFacilityIDsByCallingAET(AET){
        def vFacilityIDList = aet_to_facility_map[AET]
        return vFacilityIDList
    }

    static getAetMultiModalityTypes(AET){
        def vModalityTypeList = aet_to_modality_type_map[AET]
        return vModalityTypeList
    }

}

