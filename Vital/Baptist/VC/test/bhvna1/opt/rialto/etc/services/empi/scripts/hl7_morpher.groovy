// This file is used to modify incoming messages

def scriptName="EMPI Inbound HL7 Morpher - "

log.info(scriptName + "START ...")
log.debug(scriptName + "input: \n{}", input)

def messageType = get('MSH-9-1')
def triggerEvent = get('MSH-9-2')

def sendingApplication = get('MSH-3')
def sendingFacility = get('MSH-4')

if (sendingApplication != null && sendingFacility != null) {
    log.debug(scriptName + "Sending Application: {}, Sending Facility: {}", sendingApplication, sendingFacility)

    if ( 'PIF_MORPHER'.equalsIgnoreCase(sendingApplication) ) {
        if ( 'NORISFACILITY'.equalsIgnoreCase(sendingFacility) ) {
            log.debug(scriptName + "will populate EMPI for NORISFACILITY patient...")
        } else {
            log.debug(scriptName + "HL7 message from PIF_MORPHER, not a NORISFACILITY patient, will NOT proceed")
            log.info(scriptName + "END ...")
            return false
        }
    } else {
        log.debug(scriptName + "HL7 message NOT from PIF_MORPHER...")
    }

    if ( !'VNA_PDQ'.equalsIgnoreCase(sendingApplication) && !'VNA_PIX'.equalsIgnoreCase(sendingApplication) ) {
        def patientID = get('PID-3')
        if (patientID != null) {
          output.set('/.PID-3-4-1', 'BH')
          output.set('/.PID-3-4-2', '2.16.124.113638.7.7.0.1')
          output.set('/.PID-3-4-3', 'ISO')
        }
    }

} else {
    if ( sendingApplication == null ) {
        log.debug(scriptName + "Sending Application is null, will NOT proceed!!!")
        log.info(scriptName + "END ...")
        return false
    }
    if ( sendingFacility == null ) {
        log.debug(scriptName + "Sending Facility is null, will NOT proceed!!!")
        log.info(scriptName + "END ...")
        return false
    }
}

//def patientID = get('PID-3')
//if (patientID != null) {
//  output.set('/.PID-3-4-1', 'BH')
//  output.set('/.PID-3-4-2', '2.16.124.113638.7.7.0.1')
//  output.set('/.PID-3-4-3', 'ISO')
//}

log.debug(scriptName + "output: \n{}", output)

log.info(scriptName + "END ...")

