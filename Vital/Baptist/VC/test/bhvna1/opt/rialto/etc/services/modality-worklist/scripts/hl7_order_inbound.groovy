// MWL HL7 Order inbound morpher

def scriptName="MWL HL7 Order Inbound Morpher - "

log.info(scriptName + "START ...")
log.info(scriptName + "input: \n{}", input)

// your custom code goes here

log.info(scriptName + "output: \n{}", output)

log.info(scriptName + "END ...")
