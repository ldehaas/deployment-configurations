import java.time.LocalDateTime
import java.time.temporal.ChronoUnit
import java.time.format.DateTimeFormatter
import java.time.format.DateTimeParseException

def scriptName = "IA Inbound DICOM Morpher: ";

log.debug(scriptName + "START")

def callingAET = getCallingAETitle().toUpperCase();
def calledAET = getCalledAETitle().toUpperCase();
def sopIUID = get("SOPInstanceUID")
if (sopIUID == null) {return null}

log.info(scriptName + "SOP inst UID: {}, callingAET is {} and calledAET is {}", sopIUID, callingAET, calledAET)
log.info(scriptName + "PatientID: {}", get(PatientID))

def orig_issuerOfPatientId = get(IssuerOfPatientID)
def orig_universalEntityID = get("IssuerOfPatientIDQualifiersSequence/UniversalEntityID")
def orig_universalEntityIDType = get("IssuerOfPatientIDQualifiersSequence/UniversalEntityIDType")

// Preserve existing IPID
if ((orig_issuerOfPatientId != null && orig_issuerOfPatientId != 'BH') || orig_universalEntityID != null || orig_universalEntityIDType != null) {
   log.info(scriptName + "SOP Inst UID " + sopIUID + ": moving existing issuer information [IssuerOfPatientID: " + orig_issuerOfPatientId + ", UniversalEntityID: " + orig_universalEntityID + ", orig_universalEntityIDType: " + orig_universalEntityIDType + "] to private tag.")

   set(0x00350010, "VITAL IMAGES PRESERVED INFO 1.0", VR.LO)
   set(0x00351010, orig_issuerOfPatientId, VR.LO)
   set(0x00351011, orig_universalEntityID, VR.LO)
   set(0x00350012, orig_universalEntityIDType, VR.LO)
}

if (orig_issuerOfPatientId == null) {

    set(IssuerOfPatientID, 'BH')
    log.debug(scriptName + "original IssuerOfPatientId is NULL, new IssuerOfPatientId is {} ", get(IssuerOfPatientID))
    set("IssuerOfPatientIDQualifiersSequence/UniversalEntityID", '2.16.124.113638.7.7.1.1')
    log.debug(scriptName + "new UniversalEntityID is {}", get("IssuerOfPatientIDQualifiersSequence/UniversalEntityID"))
    set("IssuerOfPatientIDQualifiersSequence/UniversalEntityIDType", 'ISO')
    log.debug(scriptName + "new UniversalEntityIDType is {}", get("IssuerOfPatientIDQualifiersSequence/UniversalEntityIDType"))

} else {

    log.debug(scriptName + "original IssuerOfPatientId is {} ", orig_issuerOfPatientId)

    def (namespace, domain, type) = orig_issuerOfPatientId.tokenize("&")
    log.debug(scriptName + "namespace = {}, domain = {}, type = {}", namespace, domain, type)

    if (namespace != "") {
        set(IssuerOfPatientID, namespace)
    }

    if (orig_universalEntityID == null && domain == null) {
        set("IssuerOfPatientIDQualifiersSequence/UniversalEntityID", '2.16.124.113638.7.7.1.1')
    } else if (orig_universalEntityID == null && domain != null){
        set("IssuerOfPatientIDQualifiersSequence/UniversalEntityID", domain)
    }

    if (orig_universalEntityIDType == null && type == null) {
        set("IssuerOfPatientIDQualifiersSequence/UniversalEntityIDType", 'ISO')
    } else if (orig_universalEntityIDType == null && type != null) {
        set("IssuerOfPatientIDQualifiersSequence/UniversalEntityIDType", type)
    }

    log.debug(scriptName + "new IssuerOfPatientId is {} ", get(IssuerOfPatientID))

}

def patientDateOfBirth = get(PatientBirthDate)
def patientTimeOfBirth = get(PatientBirthTime)
def studyDate = get(StudyDate)
def studyTime = get(StudyTime)
def patientAge = get(PatientAge)

try {
    if (patientAge == null) {
        set("PatientAge", calculateAge(patientDateOfBirth, patientTimeOfBirth, studyDate, studyTime))
    }
} catch (Exception e) {
    log.warn(scriptName + "Failed to compute PatientAge. Proceeding without changing PatientAge. ", e)
}

String calculateAge(String patientDateOfBirth, String patientTimeOfBirth, String studyDate, String studyTime){
    def TIPPING_WEEKS_TO_DAYS = 4
    def TIPPING_MONTHS_TO_WEEKS = 3
    def TIPPING_YEARS_TO_MONTHS = 2
    
    if (patientDateOfBirth == null || studyDate == null) {
        return null
    }

    patientDateOfBirth = patientDateOfBirth.replace(":", "")
    studyDate = studyDate.replace(":", "")

    def patientDateTimeOfBirth
    def studyDateTime

    try {
        def dtf = DateTimeFormatter.ofPattern("yyyyMMddHHmmss")
        patientDateTimeOfBirth = LocalDateTime.parse(patientDateOfBirth + cleanTime(patientTimeOfBirth), dtf)
        studyDateTime = LocalDateTime.parse(studyDate + cleanTime(studyTime), dtf)
    } catch (DateTimeParseException e) {
        log.warn(scriptName + "Failed to parse PatientBirthDate or StudyDate.", e)
        return null
    }

    if (!patientDateTimeOfBirth.isBefore(studyDateTime)) {
        return null
    }

    def years = ChronoUnit.YEARS.between(patientDateTimeOfBirth, studyDateTime)
    def months = ChronoUnit.MONTHS.between(patientDateTimeOfBirth, studyDateTime)
    def weeks = ChronoUnit.WEEKS.between(patientDateTimeOfBirth, studyDateTime)
    def days = ChronoUnit.DAYS.between(patientDateTimeOfBirth, studyDateTime)
    def age = ""

    if (years > 999) {
        return null
    }
    if (years >= TIPPING_YEARS_TO_MONTHS) {
        age = years + "Y"
    } else if (months >= TIPPING_MONTHS_TO_WEEKS) {
        age = months + "M"
    } else if (weeks >= TIPPING_WEEKS_TO_DAYS) {
        age = weeks + "W"
    } else {
        age = days + "D"
    }

    return age.padLeft(4, "0")
}

String cleanTime(String str) {
    if (str == null) {
        return "000000"
    }
    str = str.replace(":", "") 
    if (str.indexOf(".") == 6) {
        str = str.substring(0, 6)
    }
    if (str.contains(".") || str.length() > 6 || str.length()%2 == 1) {
        return "000000"
    }
    return str.padRight(6, "0")
}

log.debug(scriptName + "END");
