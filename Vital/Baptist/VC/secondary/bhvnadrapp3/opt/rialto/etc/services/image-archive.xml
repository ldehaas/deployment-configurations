<?xml version="1.0"?>
<config>

    <server id="hl7-ia-update" type="hl7v2">
        <port>${UpdateStudies-HL7v2Port}</port>
    </server>

    <!-- Image Archive -->
    <service id="image-archive" type="image-archive">

        <!-- Usage of spaces is disabled in favour of using standard file system storage -->
        <!--
        <device idref="space-index" name="space" />
        <device idref="elastic" />
        -->

        <server idref="dicom-main"      name="CStoreSCP" />
        <server idref="dicom-main"      name="StgCmtSCP" />
        <server idref="dicom-main"      name="StgCmtSCU" />
        <server idref="dicom-main"      name="CFindSCP" />
        <server idref="dicom-main"      name="CMoveSCP" />
        <server idref="hl7-ia-update"   name="update" />
        <server idref="hl7-ia-update"   name="merge" />
        <server idref="hl7-ia-update"   name="order" />
        <server idref="hl7-ia-update"   name="oru" />

        <server idref="http-rest" name="MINT">
            <url>/vault/mint/*</url>
        </server>

        <server idref="http-rest" name="ILM">
            <url>/vault/ilm/*</url>
        </server>

        <server idref="http-rest" name="QC">
            <url>/vault/qc/*</url>
        </server>

        <server idref="http-rest" name="WADO">
            <url>/vault/wado</url>
        </server>

        <server idref="http-rest" name="StudyManagement">
            <url>${IA-StudyManagement-Path}/*</url>
        </server>

        <server idref="http-rest" name="StowRS">
          <url>/vault/stowrs/*</url>
        </server>

        <!-- Study Validation web service
        <server idref="http-rest" name="StudyValidation">
            <url>/vault/utils/*</url>
        </server>
        -->

        <device idref="xdsrep" />
        <device idref="xdsreg" />
        <device idref="pix" />
        <device idref="pdq" />
        <device idref="pif" />

        <device idref="cassandra-dc1" />

        <config>

            <prop name="StorerType" value="FILE" />
            <prop name="Cache" value="/data/ids1/cache" />
            <prop name="Inbox" value="/data/ids1/index" />
            <prop name="JobQueue" value="/data/ids1/jobs" />
            <prop name="QCWorkDirectory" value="/data/ids1/qc" />
            <prop name="CacheSize" value="10" />
            <prop name="AETitle" value="${IA-AETitle}" />

            <!-- DefaultDomain:  For incoming studies, if Issuer of Patient ID (0010,0021) is empty, the
                 study is assumed to be from this domain.  Format: namespace&domainUID&domainUIDType -->
            <prop name="DefaultDomain" value="${System-DefaultLocalFullyQualifiedDomain}" />
            <prop name="AffinityDomain" value="${System-AffinityFullyQualifiedDomain}" />

            <prop name="IndexerType" value="CQL3" />
            <prop name="IndexerRetryTime" value="1m" />

            <!-- An ILM configuration, for archive mode -->
            <!-- 
            <prop name="ImagingLifeCycleManagementConfiguration">
                <Mode>ARCHIVE</Mode>
                <DataCenter>DC1</DataCenter>
            </prop>
            -->

            <!-- An alternative ILM configuration, for cache mode -->
            <!-- For details, see: "Policy script" section of Admin Guide -->
            <prop name="ImagingLifeCycleManagementConfiguration">
              <Mode>CACHE</Mode>

              <MinASCThresholdRatio>0.10</MinASCThresholdRatio>
              <MinLRRUSCRecoveryRatio>0.5</MinLRRUSCRecoveryRatio>
              <LRRUSCCleanUpMode>CATALOG</LRRUSCCleanUpMode>
              <FCMDSleepTime>2m</FCMDSleepTime>

              <LRRUSCSleepTime>1m</LRRUSCSleepTime>
              <StudyStabilityTime>5m</StudyStabilityTime>

              <StudyRecordBatchRetrieveSize>100</StudyRecordBatchRetrieveSize>
              <OperationExecutorThreadPoolSize>15</OperationExecutorThreadPoolSize>
              <Policy>${rialto.rootdir}/etc/services/image-archive/scripts/ilm_policy.groovy</Policy>
              <DataCenter>DC1</DataCenter>
              <EligibilityScript>${rialto.rootdir}/etc/services/image-archive/scripts/ilm_eligibility.groovy</EligibilityScript>
              <PrefetchedFilter>
                <callingAE>*</callingAE>
                <calledAE>PF_VNA</calledAE>
              </PrefetchedFilter>

              <StgCmtMaxAttemptsToForward>5</StgCmtMaxAttemptsToForward>
            </prop>

            <!--
             Property to reject studies that contain multiple patient ID's
            -->
            <prop name="RejectMixedPatientIDsInStudy">false</prop>

            <prop name="LinkManagerAE" value="VNA_MWL" />

            <prop name="CassandraConfiguration">
                <keyspaceName>${CassandraKeyspacePrefix}imagearchive</keyspaceName>
                <keyspaceReplicationStrategy>NetworkTopologyStrategy</keyspaceReplicationStrategy>
                <keyspaceReplicationStrategyOptions>${CassandraReplicationOption}</keyspaceReplicationStrategyOptions>
                <keyspaceCreateEnabled>true</keyspaceCreateEnabled>
                <dynamicConsistencyLevelEnabled>false</dynamicConsistencyLevelEnabled>
                <schemaUpdateEnabled>false</schemaUpdateEnabled>
            </prop>

            <prop name="UsingLuceneIndex">false</prop>
            <prop name="LuceneIndexRefreshIntervalInSeconds">60</prop>
            <prop name="LuceneIndexingThreads">0</prop>
            <prop name="LuceneIndexingQueueSize">50</prop>
            <prop name="BuildingDistributedIndexes">true</prop>

            <prop name="DirectAccess" value="true" />
            <prop name="MintEnabled" value="true" />

            <prop name="FileSystemStorageConfiguration">
                <FileSystemStorageLocation>
                   <AbsoluteBasePath>/vnacache/archive</AbsoluteBasePath>
                   <ReadOnly>false</ReadOnly>
                   <SourceDataCenter>DC1</SourceDataCenter>
                </FileSystemStorageLocation>
            </prop>

            <!-- HCP Configuration
            <prop name="HcpConfiguration">
                <HcpNodeConfiguration>
                    <HcpUrl>${HCPURL}</HcpUrl>
                    <HcpHost>${HCPHost}</HcpHost>
                    <HcpUsername>${HCPUsername}</HcpUsername>
                    <HcpPassword>${HCPPassword}</HcpPassword>
                    <HcpAuthHeader>${HcpAuthHeader}</HcpAuthHeader>
                </HcpNodeConfiguration>
                <HttpConnectionConfiguration>
                    <poolMaxConnectionsPerHost>${HCPPoolMaxConnectionsPerHost}</poolMaxConnectionsPerHost>
                    <poolMaxTotalConnections>${HCPPoolMaxTotalConnections}</poolMaxTotalConnections>
                </HttpConnectionConfiguration>
            </prop>
            -->

            <prop name="MWLReconciliationEnabled">true</prop>
            <prop name="QCToolsReconciliationScript">${rialto.rootdir}/etc/services/image-archive/scripts/qcreconciliation.groovy</prop>
            <prop name="ReconciliationScript">${rialto.rootdir}/etc/services/image-archive/scripts/qcreconciliation.groovy</prop>

            <prop name="PatientNameInHL7Location" value="/.PID-5" />

            <!-- Manifest Publish Configuration -->
            <prop name="PublisherType" value="BASIC" />
            <prop name="MetadataStudyInstanceUIDKey">studyInstanceUid</prop>
            <prop name="HealthCareFacilityCode" value="HealthCareFacilityCodeValue, HealthCareFaciltiyCodeScheme, HealthCareFaciltiyCodeDisplay" />
            <prop name="PracticeSettingCode" value="PracticeSettingCodeValue, PracticeSettingCodeScheme, PracticeSettingCodeSchemeDisplay" />
            <prop name="ClassCode" value="ClassCodeValue, ClassCodeScheme, ClassCodeDisplay" />
            <prop name="TypeCode" value="TypeCodeValue, TypeCodeScheme,TypeCodeDisplay" />
            <prop name="ContentTypeCode" value="ContentTypeCodeValue, ContentTypeCodeScheme, ContentTypeCodeDisplay" />
            <prop name="ConfidentialityCode" value="ConfidentialityCodeValue, ConfidentialityCodeScheme,ConfidentialityCodeDisplay" />
            <!-- Manifest metadata is now determined from the groovy script -->
            <prop name="DocumentMetadataMorpher">${rialto.rootdir}/etc/services/image-archive/scripts/document_metadata_morpher.groovy</prop>

            <prop name="PatientIdentityFeedMorpher">${rialto.rootdir}/etc/services/image-archive/scripts/pif_morpher.groovy</prop>

            <prop name="TagMorphers">
                <script direction="IN" file="${rialto.rootdir}/etc/services/image-archive/scripts/in.groovy" />
                <!-- <script direction="CFIND_REQUEST" file="${rialto.rootdir}/etc/services/image-archive/scripts/cfind_request.groovy" /> -->
            </prop>

            <!-- Study Content Notification -->
            <!-- Note: When configuring notification destinations, please ensure the receivingApplication
                 and receivingFacility match up with a configured device -->
            <!--prop name="StudyContentNotifierType" value="BASIC" />

            <prop name="NotificationDestinations">
                <destination name="EPIC">
                    <receivingApplication>EPIC</receivingApplication>
                    <receivingFacility>BH</receivingFacility>
                    <morphingScript>${rialto.rootdir}/etc/services/image-archive/scripts/scn_epic.groovy</morphingScript>
                </destination>
            </prop-->
        </config>
    </service>

</config>
