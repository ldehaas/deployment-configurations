// MWL DICOM C-Find Response morpher

LOAD('/opt/rialto/etc/services/common/common_bh.groovy')

def scriptName="MWL DICOM C-Find response Morpher - "

log.info(scriptName + "START ...")

callingAET = getCallingAETitle()
log.debug( scriptName + "Calling AET: {}", getCallingAETitle() );

// uncomment the following line if you need to debug MWL C-Find Response mopher
//log.debug( scriptName + "input: {}\n", input );

// Possible ScheduledProcedureStepStatus values:
//   DISCONTINUED
//   COMPLETED
//   INPROGRESS
//   SC
//   SCHEDULED

if ("VNA_CACHE".equalsIgnoreCase(callingAET)) {
    log.info(scriptName + "Source is Image Archive, will proceed");
    log.info(scriptName + "END ...")
    return true
}

def scheduledProcedureStepStatus = get("ScheduledProcedureStepSequence/ScheduledProcedureStepStatus");
log.debug( scriptName + "ScheduledProcedureStepStatus = {}", scheduledProcedureStepStatus);

if ( scheduledProcedureStepStatus != null && scheduledProcedureStepStatus == 'COMPLETED' ) {
    log.info( scriptName + "ScheduledProcedureStepStatus = {}, will NOT include this order in DMWL C-Find response...", scheduledProcedureStepStatus );
    log.info( scriptName + "END" );
    return false
}

if ( scheduledProcedureStepStatus != null && scheduledProcedureStepStatus == 'DISCONTINUED' ) {
    log.info( scriptName + "ScheduledProcedureStepStatus = {}, will NOT include this order in DMWL C-Find response...", scheduledProcedureStepStatus );
    log.info( scriptName + "END" );
    return false
}

validOrder = false
validModality = true

log.debug( scriptName + "AccessionNumber = {}", imagingServiceRequest.getAccessionNumber() );
log.debug( scriptName + "ScheduledProcedureStartDate = {}", get("ScheduledProcedureStepSequence/ScheduledProcedureStepStartDate") );
log.debug( scriptName + "ScheduledProcedureStartTime = {}", get("ScheduledProcedureStepSequence/ScheduledProcedureStepStartTime") );


orderEnteringLocation = imagingServiceRequest.getOrderEnteringLocation()
if (orderEnteringLocation != null) {
    log.debug( scriptName + "OrderEnteringLocation = {}", orderEnteringLocation)
} else {
    log.debug( scriptName + "OrderEnteringLocation is NULL")
}

def aetFacilities = []

if (callingAET != null) {

    aetFacilities = BHCommon.getFacilityIDsByCallingAET(callingAET)

    if (aetFacilities) {
        if(orderEnteringLocation != null) {
            log.debug( scriptName + "Facility Match: OrderEnteringLocation = {}, multi-facility list = {}", orderEnteringLocation, aetFacilities)
            if ( aetFacilities.contains(orderEnteringLocation) ) {
                validOrder = true
                log.info( scriptName + "Facility Match: OrderEnteringLocation matches one of the facilities associated with this CallingAET")
            } else {
                log.info( scriptName + "Facility Match: order is NOT valid for this CallingAET")
            }
        } else {
            log.info( scriptName + "Facility Match: OrderEnteringLocation is NULL, skipping facility check...")
        }
    } else {
        log.info( scriptName + "Facility Match: could not find any facilities associated with this CallingAET {}", callingAET)
    }

} else {
    log.info( scriptName + "Facility Match: CallingAET is NULL, skipping facility check")
}



// check if requesting AET is on multi-modality list
def aetModalityTypes = []
if (callingAET != null) {
    aetModalityTypes = BHCommon.getAetMultiModalityTypes(callingAET)
}

if (aetModalityTypes) {

    def requestedProcedure = imagingServiceRequest.getRequestedProcedureSequence().get(0)
    def scheduledProcedureStep = requestedProcedure.getScheduledProcedureStepSequence().get(0)
    def modalityType = scheduledProcedureStep.getModality()

    if (modalityType != null) {
        log.debug( scriptName + "Multi-modality check: Modality in order = {}, multi-modality list = {}", modalityType, aetModalityTypes)
        if ( aetModalityTypes.contains(modalityType) ) {
            log.debug( scriptName + "Multi-modality check: Modality in order matches one of the modality types associated with this CallingAET")
        } else {
            validModality = false
            log.debug( scriptName + "Multi-modality check: order is NOT valid for this CallingAET")
        }
    } else {
        log.debug( scriptName + "Modality type is NULL in Scheduled Procedure Step...")
    }
}



callingAetPrefix3 = getCallingAETitle().substring(0,3)
expectedAetPrefix3 = BHCommon.getAetPrefixByFacility3(orderEnteringLocation)

if (!validOrder && callingAetPrefix3 != null && expectedAetPrefix3 != null) {
    if( callingAetPrefix3 == expectedAetPrefix3) {
        log.debug( scriptName + "3CharMatch: OrderEnteringLocation = {}, expected AET Prefix3 = {}, Calling AET Prefix3 = {}, order is valid for this location", orderEnteringLocation, expectedAetPrefix3, callingAetPrefix3)
        validOrder = true
    } else {
        log.debug( scriptName + "3CharMatch: OrderEnteringLocation = {}, expected AET Prefix3 = {}, Calling AET Prefix3 = {}, order is NOT valid for this location", orderEnteringLocation, expectedAetPrefix3, callingAetPrefix3)
    }
} else {
    if ( callingAetPrefix3 == null ) {
        log.debug( scriptName + "3CharMatch: callingAetPrefix3 is NULL" )
    }
    if ( expectedAetPrefix3 == null ) {
        log.debug( scriptName + "3CharMatch: expectedAetPrefix3 is NULL" )
    }
}



// check what AETitle Prefix should be for an OrderEnteringLocation and compare it to calling AETittle Prefix
callingAetPrefix2 = getCallingAETitle().substring(0,2)
expectedAetPrefix2 = BHCommon.getAetPrefixByFacility2(orderEnteringLocation)

if (!validOrder && callingAetPrefix2 != null && expectedAetPrefix2 != null) {
    if ( callingAetPrefix2 == expectedAetPrefix2) {
        log.debug( scriptName + "2CharMatch: OrderEnteringLocation = {}, expected AET Prefix2 = {}, Calling AET Prefix2 = {}, order is valid for this location", orderEnteringLocation, expectedAetPrefix2, callingAetPrefix2)
        validOrder = true
    } else {
        log.debug( scriptName + "2CharMatch: OrderEnteringLocation = {}, expected AET Prefix2 = {}, Calling AET Prefix2 = {}, order is NOT valid for this location", orderEnteringLocation, expectedAetPrefix2, callingAetPrefix2)
    }
} else {
    if ( callingAetPrefix2 == null ) {
        log.debug( scriptName + "2CharMatch: callingAetPrefix2 is NULL" )
    }
    if ( expectedAetPrefix2 == null ) {
        log.debug( scriptName + "2CharMatch: expectedAetPrefix2 is NULL" )
    }
}



if (validOrder && validModality) {
    // uncomment the following line if you need to debug MWL C-Find Response mopher
    //log.debug( scriptName + "output: {}\n", output );
    log.info(scriptName + "END ...")
    return true
} else {
    log.debug( scriptName + "Not a valid order for the requesting AET / Location");
    log.info(scriptName + "END ...")
    return false
}

