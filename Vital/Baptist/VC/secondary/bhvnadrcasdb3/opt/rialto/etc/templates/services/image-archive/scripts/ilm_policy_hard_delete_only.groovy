/*
 * ilm_policy_hard_delete_only.groovy
 *
 * The policy script makes decisions based on the events in a study's timeline 
 * to determine if the study is ready for further lifecycle actions, and if so,
 * schedules the actions.
 *
 * This example schedules the study for immediate physical deletion with no 
 * other conditions. This physical deletion will happen, and is not dependent on
 * cache space limitations.
 *
 * The morpher must at a minimum determine how studies become candidates for 
 * purging (for example, based on study age). When trying to recover cache 
 * space, the ILM can only purge a study if it meets the least recently 
 * retrieved criteria, and has been previously flagged as a candidate by the 
 * policy script.
 *
 * To schedule an action for a study, use the "ops" variable. This represents
 * the StudyOperationsAPI. Methods that may be used here are as follows:
 *  
 *  flagAsPurgeCandidate():    Flag a study as ready to be purged in order to 
 *                             create more space. The study will only be removed
 *							   when the system needs to create more space in the
 *							   cache.
 *  scheduleStudyHardDelete(): Schedule a study to be removed from the system
 *  scheduleStudyCatalog():    Prepare a study to be cataloged (remove the pixel
 *							   data but retain the metadata and indexes).
 *  scheduleStudyHardDelete(): Delete all study data from the system
 *  setPreventDeletion():      Flag the study as one that cannot be deleted,
 *							   cataloged, or purged from the system.
 *  scheduleStudyForward(AE):  Schedule a study to be forwarded to another [AE]

 *  scheduleStudyForwardWithStorageCommit(AE):  Schedule a study to be forwarded
 												to another [AE]
 *
 * About destinations
 * To enable a destination to forward studies to:
 * In the Rialto Admin Tools, configure each destination as a DICOM Device with 
 * an accepting interface that accepts C-Moves and C-Stores. (Alternatively, add
 * the destination as DICOM Device in the rialto.cfg.xml list of devices.)
 * Any references to the forward destinations in the script must match the 
 * <aetitle> properties of the destination DICOM devices. In the example below,
 * the AE Titles is "LTE_ARCHIVE1".
 */

log.info("ilm_policy_hard_delete_only.groovy: Starting Policy Script")

def lastUpdateTime = new org.joda.time.DateTime().minusDays(10)

	if (study.isOlderThan(Duration.standardDays(10)) && study.getLastUpdateTime() < lastUpdateTime) {
		log.info("ilm_policy_hard_delete_only.groovy: Hard deleting study {}", study.getStudyInstanceUID());
		ops.scheduleStudyHardDelete();
	}

log.info("ilm_policy_hard_delete_only.groovy: Finishing Policy Script")