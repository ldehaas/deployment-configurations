<?xml version="1.0"?>
<!--
 The following configuration file is used to define the Image Archive service
 that is used to store DICOM studies and make them accessible via MINT and DICOM
 requests. This configuration sets the Image Archive to be a study cache and not
 the LongTerm Archive. Description of how that may be configured is also 
 provided in this configuration.
-->
<config>

    <server id="ia-update-hl7" type="hl7v2">
        <port>${UpdateStudies-HL7v2Port}</port>
    </server>

    <service id="imagingArchive" type="image-archive">
        <!--
         The following server references represent the configuration that the
         Image Archive service uses to listen for DICOM requests.

         Each name represents the following DICOM request types:
             CStoreSCP: DICOM C-STORE requests
             StgCmtSCP: DICOM storage commit requests
             CFindSCP:  DICOM C-FIND requests
             CMoveSCP:  DICOM C-MOVE requests

         The "idref" represents a reference to a server configuration.
        -->
        <server idref="main-dicom"      name="CStoreSCP" />
        <server idref="main-dicom"      name="StgCmtSCP" />
        <server idref="main-dicom"      name="CFindSCP" />
        <server idref="main-dicom"      name="CMoveSCP" />

        <!--
         The following server references represent the configuration that the
         Image Archive service uses to listen for HL7 messages.

         Each name represents the following HL7 message types that are actioned
         on and the type of action that will be applied to the studies in the 
         archive:
             update:  Listens for ADT^A01, ADT^A04, ADT^A08, ADT^A31, or ADT^A49
                      messages for patient demographic updates that are to be 
                      applied to the studies in the archive.
                      (Required)

             merge:   Listen for ADT^A39 or ADT^A40 patient merge messages that
                      are to be applied to the studies in the archive. 
                      (Required)

             oru:     Listens for HL7 ORU^R01 messages, and optionally convert
                      the report messages to DICOM SRs. 
                      (Optional)

         The "idref" represents a reference to a server configuration.
        -->
        <server idref="ia-update-hl7"   name="update" />
        <server idref="ia-update-hl7"   name="merge" />
	    <server idref="ia-update-hl7"   name="oru" />

        <!--
         The following server references represent the configuration that the
         Image Archive service uses to listen for requests via HTTP.

         Each name represents the following service request types:
             MINT:   Enables traffic for MINT requests for studies. The <url> is
                     the base URL to which incoming requests will be sent.

             ILM:    Enables requests for retrieving events in a study's 
                     timeline. The <url> is the base URL to which incoming 
                     requests will be sent.

             WADO:   Enables traffic for WADO requests for DICOM study images in
                     JPEG format. The <url> is the base URL to which incoming 
                     requests will be sent.

             StowRS: Enables traffic for STOW-RS requests to store DICOM study 
                     images. The <url> is the base URL to which incoming 
                     requests will be sent.

             StudyManagement: Enables requests about the study in the archive, 
                              such as comparing a study in the archive with a 
                              study in the source PACS, or sending a 
                              notification to an external system indicating that
                              a study has been archived. The <url> is the base 
                              URL to which incoming requests will be sent.

         The "idref" represents a reference to a server configuration.
        -->
        <server idref="http-rest" name="MINT">
            <url>/vault/mint/*</url>
        </server>

        <server idref="http-rest" name="ILM">
            <url>/vault/ilm/*</url>
        </server>

        <server idref="http-rest" name="WADO">
            <url>/vault/wado</url>
        </server>

        <server idref="http-rest" name="StowRS">
            <url>/vault/stowrs/*</url>
        </server>

        <server idref="http-rest" name="StudyManagement">
            <url>${IA-StudyManagement-Path}/*</url>
        </server>

        <config>

            <!--
             This property is required.
             This specifies how we will store each objct and the associated 
             data.
            -->
            <prop name="StorerType" value="FILE" />
            <!--
             This property is required due to the StorerType being set to 
             "FILE".
             The <FileSystemStorageLocation> tag is required. This  container
             element defines each file system location configuration. The
             following tags within this container are:
                <AbsoluteBasePath>:  The location on disk for long term storage
                                     of imaging data.
                <ReadOnly>:          When Rialto starts up, it verifies if the 
                                     configured path is valid for object 
                                     retrieval. If this property is set to true,
                                     it will also verify if it can write to that
                                     path location.
                <SourceDataCenter>:  The Cassandra database Data Center 
                                     responsible for the data in the configured 
                                     path.
            -->
            <prop name="FileSystemStorageConfiguration">
                <FileSystemStorageLocation>
                   <AbsoluteBasePath>
                        ${rialto.rootdir}/var/ids1/archive
                    </AbsoluteBasePath>
                   <ReadOnly>false</ReadOnly>
                   <SourceDataCenter>DC1</SourceDataCenter>
                </FileSystemStorageLocation>
            </prop>

            <!--
             This property is required.
             The Cache property identifies the location on disk where Image 
             Archive will cache image data to speed up retrieves.

             The CacheSize property is also required.
             This represents the number of megabytes used for cache retrieval. 
             The size should be several Gigabytes but must not exceed 90% of the
             free space on the disk partition being used. Setting a value that
             is too small can result in errors.
            -->
            <prop name="Cache" value="${rialto.rootdir}/var/ids1/cache" />
            <prop name="CacheSize" value="10" />

            <!--
             The properties Inbox and JobQueue are required.

             As the Image Archive service receives the images, it does some 
             processing of the images, such as extracting metadata from the 
             DICOM header of the object, and stores them temporarily in an 
             internal "Inbox" folder using the local file system, until it 
             determines that no more images are coming.

                 Inbox:    Specifies the location on disk where Image Archive 
                           service holds incoming data before storing it 
                           permanently.
                 JobQueue: The location on disk where Image Archive manages 
                           tasks that may need to persist between reboots or 
                           crashes. The folder structure created is as follows:
                            toStore:   jobs to be stored into long term object 
                                       storage
                            toIndex:   jobs to be indexed into the database
                            toPublish: jobs to be published so that they are 
                                       discoverable using searcher
                            error:     jobs that have encounter unrecoverable 
                                       errors
             
            -->
            <prop name="Inbox" value="${rialto.rootdir}/var/ids1/index" />
            <prop name="JobQueue" value="${rialto.rootdir}/var/ids1/jobs" />

            <!--
             This property is required.

             This defines the directory where QC functionality will store 
             objects as they are being updated for merge/split/segment.
            -->
            <prop name="QCWorkDirectory" value="${rialto.rootdir}/var/ids1/qc" />
            
            <!--
             This property is optional.

             The AE title that the Image Archive will use when initiating
             associations and as the RetrieveAETitle in manifests that it 
             produces. The Image Archive service will respond to DICOM requests
             regardless of the calledAE in the request.
            -->
            <prop name="AETitle" value="${IA-AETitle}" />

            <!-- 
             This property is optional.

             For incoming studies, if Issuer of Patient ID (0010,0021) is empty,
             Image Archive assumes the study to be from this domain. Must be in 
             the format "namespace&domainUID&domainUIDType".
            -->
            <prop name="DefaultDomain">
                ${System-DefaultLocalFullyQualifiedDomain}
            </prop>
            <!-- 
             This property is required.
             
             When publishing study manifests to an XDS Repository, Rialto uses 
             the affinity domain identifier for the patient, (not the local 
             domain identifier).
            -->
            <prop name="AffinityDomain">
                ${System-AffinityFullyQualifiedDomain}
            </prop>

            <!--
             This property is required.
             This is used so that we can find the ingested study data later.
             We set the IndexerType to CQL3 (representing the Cassandra
             database). The IndexerRetryTime is used to determine when a 
             failed indexing task will be re-attempted. This takes a duration
             value. EG:
                 1m: One minutes
                 1h: One Hour
            -->
            <prop name="IndexerType" value="CQL3" />
            <prop name="IndexerRetryTime" value="1m" />

            <!--
             This property is required.
             This provides the details for configuration of the Cassandra
             database that we will index our studies in.
            -->
            <prop name="CassandraConfiguration">
                <clusterHosts>
                    ${CassandraClusterHosts}
                </clusterHosts>
                <clusterDatacenterName>
                    ${CassandraClusterDatacenterName}
                </clusterDatacenterName>
                <keyspaceName>
                    ${CassandraKeyspacePrefix}imagearchive
                </keyspaceName>
                <keyspaceReplicationStrategy>
                    NetworkTopologyStrategy
                </keyspaceReplicationStrategy>
                <keyspaceReplicationStrategyOptions>
                    ${CassandraReplicationOption}
                </keyspaceReplicationStrategyOptions>
                <consistencyLevel>
                    LOCAL_QUORUM
                </consistencyLevel>
                <serialConsistencyLevel>
                    LOCAL_SERIAL
                </serialConsistencyLevel>
                <keyspaceCreateEnabled>
                    true
                </keyspaceCreateEnabled>
                <dynamicConsistencyLevelEnabled>
                    false
                </dynamicConsistencyLevelEnabled>
                <schemaUpdateEnabled>
                    false
                </schemaUpdateEnabled>
            </prop>

            <!--
             This property is optional. If not set, the ILM mode of operation
             will default to "ARCHIVE".

             The Imaging Lifecycle Management (ILM) Configuration determines how
             studies will be managed within the Image Archive service. The 
             <Mode> tag is what determines this. The values that exist are as
             follows:
                ARCHIVE:  Set the ILM to Archive mode for Rialto VNA deployments
                          where the Image Archive will serve as permanent 
                          storage for imaging studies. Policy configuration is 
                          also available for study lifecycle management 
                          activities such as forwarding the studies to external
                          DICOM devices, or permanently deleting the studies.

                CACHE:    Set the ILM to Cache mode for deployments where Rialto
                          serves as an imaging study cache. In cache mode, the 
                          ILM will manage the size of the cache. It will purge 
                          one or more studies, based on when the studies were
                          last accessed, to free storage space as and when 
                          necessary, ensuring there is always a minimum amount
                          of storage space available on the associated storage
                          system. Additional policy configuration is available
                          to set up rules that specify further lifecycle actions
                          for the studies, such as forwarding studies to a 
                          long-term archive prior before purging them.

                DISABLED: In a multiple data center deployment, the ILM master 
                          election algorithm selects any of the Rialto instances
                          in either data center to be the ILM Master that
                          performs ILM activities. However, a deployment may
                          require the ILM master election to occur in a specific
                          data center (for example, DC1). If so, on each Rialto
                          instance in the other data center (DC2), set the ILM
                          Mode to DISABLED to turn off all ILM activities and
                          remove the instances from possible master election.

             The following configuration describes a CACHE configuration and
             the properties. A description of each property can be found above
             the property itself.
            -->
            <prop name="ImagingLifeCycleManagementConfiguration">
                <!--
                 This property is required.
                 Mode determines how the Image Archive service will manage
                 studies.
                -->
                <Mode>CACHE</Mode>

                <!--
                 This property is required.

                 The (MinASCThresholdRatio * the configured partition size) 
                 (automatically detected by the Image Archive service) is the 
                 minimum available storage capacity to maintain; when available
                 space drops below this threshold, it triggers the ILM to purge
                 studies to recover storage space.
                -->
                <MinASCThresholdRatio>0.05</MinASCThresholdRatio>
                <!--
                 This property is required.
                 The (MinLRRUSCRecoveryRatio * the configured partition size) 
                 (automatically detected by the Image Archive service) is the
                 minimum utilized storage capacity to recover from least
                 recently retrieved studies each time the ILM clears the cache.
                -->
                <MinLRRUSCRecoveryRatio>0.01</MinLRRUSCRecoveryRatio>
                <!--
                 This property is optional. If not set, the default is 
                 "HARD_DELETE".

                 This property specifies how to proceed with study purging. The
                 values this can be set to are:
                    CATALOG:     Delete all pixel data, retain study indexes and 
                                 metadata.

                    HARD_DELETE: Delete all study data.
                 
                 Verify that the ILM policy script has code to exclude the 
                 Health Check service test studies from any processing.
                -->
                <LRRUSCCleanUpMode>HARD_DELETE</LRRUSCCleanUpMode>

                <!--
                 This property is optional. If not set, the default value is 12
                 hours.

                 This property defines the minimum amount of time a study should
                 remain unchanged in order for it to be considered stable.
                -->
                <StudyStabilityTime>12h</StudyStabilityTime>

                <!--
                 This property is required.

                 Set to the name of the data center where this Rialto instance 
                 is installed. Ensure that this setting is the same for all 
                 Rialto instances in the same data center. As an ILM instance
                 runs, it acquires a distributed local lock on the local data
                 center name to ensure that only one instance of the ILM is
                 running within the data center.
                -->
                <DataCenter>DC1</DataCenter>

                <!-- 
                 This property is required.

                 The policy script makes decisions based on the events in a
                 study's ILM study timeline to determine if the study is ready
                 for further lifecycle actions, and if so, schedules the 
                 actions.

                 The policy must at a minimum determine how studies become 
                 candidates for purging (for example, based on study age). When
                 trying to recover cache space, the ILM can only purge a study
                 if it meets the least recently retrieved criteria, and has been
                 previously flagged as a candidate by the policy script.
                -->
                <Policy>
                    ${rialto.rootdir}/etc/services/image-archive/scripts/ilm_policy_forward_and_delete.groovy
                </Policy>

                <!--
                 This property is optional.
                 
                 This will identify studies as prefetched studies when coming 
                 from the configured callingAE title to the calledAE title.

                 Use when the deployment has prefetching enabled, where 
                 prefetched studies may require different rules than new 
                 studies. For example, if prefetched studies can be deleted 
                 without forwarding to a Long Term Archive.

                 This property may ne repeated for multiple callingAE titles. 
                 In this example, "*" means all callingAE titles.
                -->
                <PrefetchedFilter>
                    <callingAE>*</callingAE>
                    <calledAE>PF_RIALTO</calledAE>
                </PrefetchedFilter>
            </prop>

            <!--
             This property is optional.
             This will identify whether or not the Image Archive service should
             respond to MINT query and retrieve requests. If this is configured,
             an HTTP server with the name "MINT" must be configured. This 
             configuration enables MINT.
            -->
            <prop name="MintEnabled" value="true" />

            <!-- 
             Manifest Publish Configuration. These properties are required by
             the Image Archive service in order to start but are not required 
             unless using the XDS workflow. We will set these values to their
             default values.

             PublisherType represents whether or not we will perform any XDS
             actions. If set to "BASIC" we will publish to the XDS-Registry and
             Repository per study to the Image Archive service. If set to
             "DISCARD" no XDS related actions will be performed.
            -->
            <prop name="PublisherType" value="DISCARD" />
            <prop name="MetadataStudyInstanceUIDKey">
                studyInstanceUid
            </prop>

            <!--
             This property is optional.
             The TagMorphers property can be used to apply scripts to DICOM data
             that is either inbound to or outbound from the Image Archive 
             service. The direction property can be:
             "IN":              This script will be applied to all objects 
                                being stored to the Image Archive service.

             "OUT":             This script will be applied to all objects 
                                being moved/stored to another DICOM AE.

             "C_FIND_REQUEST":  Apply this morpher on incoming C-FIND requests.

             "C_FIND_RESPONSE": Apply this morpher to C-FIND responses before 
                                being sent to the callingAE.

             If callingAE is not specified, this script will apply to all 
             callingAE's.
             If calledAE is not specified, this script will apply to all 
             calledAE's.
            -->
            <prop name="TagMorphers">
                <script direction="IN" file="${rialto.rootdir}/etc/services/image-archive/scripts/in.groovy">
                    <callingAE>*</callingAE>
                    <calledAE>*</calledAE>
                </script>
            </prop>
        </config>
    </service>
</config>