cd ~/deployment-configurations/x-Tools/Ansible/Rialto7

# Prod - Primary site 
ansible-playbook -i inventories/bh_prod_pr vc_config_backup.yml
ansible-playbook -i inventories/bh_prod_pr vc_config_backup_rialto_only.yml

# Prod - DR site
ansible-playbook -i inventories/bh_prod_dr vc_config_backup.yml
ansible-playbook -i inventories/bh_prod_dr vc_config_backup_rialto_only.yml

# Migration
ansible-playbook -i inventories/bh_migration vc_config_backup.yml
ansible-playbook -i inventories/bh_migration vc_config_backup_rialto_only.yml

# Test
ansible-playbook -i inventories/bh_test vc_config_backup.yml
ansible-playbook -i inventories/bh_test vc_config_backup_rialto_only.yml

