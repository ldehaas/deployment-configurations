import org.joda.time.Duration;

if (study.isOlderThan(Duration.standardDays(0))) {
    ops.flagAsPurgeCandidate();
}
