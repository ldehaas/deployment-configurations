log.info("Start IA Update Morpher")

LOAD("common.groovy")

def messageType = get('/.MSH-9-1')
def triggerEvent = get('/.MSH-9-2')

log.info("messageType is: {}^{}", messageType, triggerEvent)

if (messageType != null && triggerEvent != null) {
    if (["ADT^A04", "ADT^A08", "ADT^A30", "ADT^A31", "ADT^A39", "ADT^A40", "ORU^RO1"].contains(messageType + "^" + triggerEvent)) {
        log.info("will forward message to IA Update")
    }
    else {
        log.info("will not forward to IA Update")
        return false
    }
}

def version = get('/.MSH-12')
if (version.equals("2.3")) {
    set('/.MSH-12', "2.3.1")
}


if ("ADT^A30".equals(messageType + "^" + triggerEvent)) {
    log.debug("MWL converting A30 to A40")
    set('/.MSH-9-2', 'A40')
    Domain.addFqdn('/.MRG-1', this)
}
if ("ADT^A31".equals(messageType + "^" + triggerEvent)) {
    log.debug("MWL converting A31 to A08")
    set('/.MSH-9-2', 'A08')
}

def ext_patientid = Domain.addFqdn( '/.PID-2', this )

if (get('/.PID-3') == null) {
    log.debug("MWL PID-3 is blank; copying PID-2 to PID3")
    set('/.PID-3', get('/.PID-2'))
}
def int_patientid = Domain.addFqdn( '/.PID-3', this )

log.info("End IA Update Morpher")
