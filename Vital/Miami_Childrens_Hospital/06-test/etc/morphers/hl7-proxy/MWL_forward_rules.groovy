log.info("Start MWL Morpher")

LOAD("common.groovy")

def messageType = get('/.MSH-9-1')
def triggerEvent = get('/.MSH-9-2')

log.info("messageType is: {}^{}", messageType, triggerEvent)

if (messageType != null && triggerEvent != null) {
    if (["ADT^A04", "ADT^A08", "ADT^A30", "ADT^A31", "ADT^A39", "ADT^A40", "ORM^O01"].contains(messageType + "^" + triggerEvent)) {
        log.info("will forward message to MWL")
    }
    else {
        log.info("will not forward to MWL")
        return false
    }
}

def version = get('/.MSH-12')
if (version.equals("2.3")) {
    set('/.MSH-12', "2.3.1")
}

if ("ORM^O01".equals(messageType + "^" + triggerEvent)) {
    if ((get("ORC-1") == "SC") && (get("ORC-5") == "SC")) {
        log.debug("MWL ORC-1=SC and ORC-5=SC; therefore skipping message")
        return false;
    }
}

if ("ADT^A30".equals(messageType + "^" + triggerEvent)) {
    log.debug("MWL converting A30 to A40")
    set('/.MSH-9-2', 'A40')
    Domain.addFqdn('/.MRG-1', this)
}
if ("ADT^A31".equals(messageType + "^" + triggerEvent)) {
    log.debug("MWL converting A31 to A08")
    set('/.MSH-9-2', 'A08')
}

def ext_patientid = Domain.addFqdn( '/.PID-2', this )

if (get('/.PID-3') == null) {
    log.debug("MWL PID-3 is null")
    set('/.PID-3', get('/.PID-2'))
}
def int_patientid = Domain.addFqdn( '/.PID-3', this )

log.info("End MWL Morpher")
