class DICOMTime {
    
    // This is still missing validation for sub-second time
    static validateDateTime(rawDateTime){
        if(9 < rawDateTime.length()){
         // Potentially valid DateTime range
            if(10 == rawDateTime.length()){
                return "DatePlusHour"
            } else if(12 == rawDateTime.length()) {
                return "DatePlusHourAndMinutes"
            } else if(14 == rawDateTime.length()) {
                return "DatePlusHourMinutesAndSeconds"
            } else if(15 <= rawDateTime.length()) {
               // The Time portion potentially has a subsecond portion
                if(rawDateTime.contains(".") && 16 <= rawDateTime) {
                    return "DatePlusHoursMinutesSecondsAndSubseconds"
                } else{
                   // The subsecond portion is not after a period, and it's thus not DICOM Standard compliant.
                    return false
                }
            }else {
               // Likely Invalid DateTime Range (This is just a sanity check)
                return false
            }
        } else {
         // Invalid DateTime range
                return false
        }
    }


    
    static parseDate(rawDateTime_date){
        
        if(validateDateTime(rawDateTime_date)){
         // Valid Date
            def dateToReturn = rawDateTime_date[0..7]
            return dateToReturn
     
        } else {
            return null
        }

    }


    static parseTime(rawDateTime_time){
        def typeOfTime = false
        def timeToReturn = null

        typeOfTime = validateDateTime(rawDateTime_time)
        
        if(typeOfTime){
            switch(typeOfTime) {
                case "DatePlusHour":
                    timeToReturn = rawDateTime_time[8..9]
                    break
                case "DatePlusHourAndMinutes" :
                    timeToReturn = rawDateTime_time[8..11]
                    break
                case "DatePlusHourMinutesAndSeconds" :
                    timeToReturn = rawDateTime_time[8..13]
                    break
                case "DatePlusHoursMinutesSecondsAndSubseconds":
                    timeToReturn = rawDateTime_time.substring(15, rawDateTime_time.length()-1)
                default:
                    timeToReturn = null
            }

            return timeToReturn
        
        } else {
            return null
        }
    }
    
}

