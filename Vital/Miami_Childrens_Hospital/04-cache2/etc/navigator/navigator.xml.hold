<config>
    <service type="authentication" id="authentication">
        <server idref="authenticated-http" name="web-api">
            <url>${Navigator-RootPath}</url>
        </server>

        <config>
            <include location="userauth.xml"/>
        </config>
    </service>

    <service type="usermanagement" id="usermanagement">
        <config>
            <prop name="SystemDefaultPermissions">
                <permission name="rialto.arr" value="false"/>
                <permission name="rialto.administration" value="false"/>
                <permission name="rialto.usermanagement.internalrealm" value="false"/>
                <permission name="rialto.events" value="false" />
                <permission name="rialto.quality.management" value="false" />
                <permission name="rialto.modality.worklist" value="false" />
                <permission name="rialto.modality.worklist.updates" value="false" />

                <!-- Workflows/Dataflows permissions - should never have both configured at the same time -->
                <permission name="rialto.dataflows" value="false" />
                <!--permission name="rialto.workflows" value="false" /-->
            </prop>
            <prop name="web-api-path" value="${Usermanagement-APIBasePath}"/>

            <include location="defaultuserprefs.xml"/>
        </config>
    </service>

    <!-- PLUGIN: External Viewer (Download) -->
    <var name="DownloadPlugin-Name" value="External Viewer (Download)" />
    <var name="DownloadPlugin-Description" value="External platform-specific viewer or download to local disk" />
    <service type="navigator.plugin.download" id="navigator.plugin.download">
        <device idref="pix" />
        <device idref="pdq" />
        <device idref="xdsrep"/>
        <device idref="xdsreg"/>

        <config>
            <prop name="CrossAffinityDomainID" value="${System-AffinityDomain}"/>
            <prop name="ForcedMimeTypeFileExtension">
                <prop mimeType="application/vnd.ms-excel" fileExtension="xls"/>
            </prop>
            <prop name="CassandraConfiguration">
                <clusterHosts>${CassandraClusterHosts}</clusterHosts>
                <clusterDatacenterName>${CassandraClusterDatacenterName}</clusterDatacenterName>
                <keyspaceName>${CassandraKeyspacePrefix}navigator</keyspaceName>
                <keyspaceReplicationStrategy>NetworkTopologyStrategy</keyspaceReplicationStrategy>
                <keyspaceReplicationStrategyOptions>${CassandraReplicationOption}</keyspaceReplicationStrategyOptions>
                <consistencyLevel>LOCAL_QUORUM</consistencyLevel>
                <serialConsistencyLevel>LOCAL_SERIAL</serialConsistencyLevel>
                <keyspaceCreateEnabled>true</keyspaceCreateEnabled>
                <dynamicConsistencyLevelEnabled>false</dynamicConsistencyLevelEnabled>
                <schemaUpdateEnabled>false</schemaUpdateEnabled>
            </prop>
            <prop name="web-api-path" value="/plugins/download"/>
            <prop name="UserManagementURL" value="${Usermanagement-AuthenticatedURL}"/>

            <include location="patientidentitydomains.xml"/>
            <include location="cacheStorage.xml"/>
        </config>
    </service>

    <!-- PLUGIN: CDA Viewer   -->
    <var name="CDAPlugin-Name" value="CDA Viewer" />
    <var name="CDAPlugin-Description" value="HL7 CDA (Clinical Document Architecture) Viewer" />
    <service type="navigator.plugin.cda" id="navigator.plugin.cda">
        <device idref="pix" />
        <device idref="pdq" />
        <device idref="xdsrep"/>
        <device idref="xdsreg"/>

        <config>
            <prop name="CrossAffinityDomainID" value="${System-AffinityDomain}"/>
            <prop name="CassandraConfiguration">
                <clusterHosts>${CassandraClusterHosts}</clusterHosts>
                <clusterDatacenterName>${CassandraClusterDatacenterName}</clusterDatacenterName>
                <keyspaceName>${CassandraKeyspacePrefix}navigator</keyspaceName>
                <keyspaceReplicationStrategy>NetworkTopologyStrategy</keyspaceReplicationStrategy>
                <keyspaceReplicationStrategyOptions>${CassandraReplicationOption}</keyspaceReplicationStrategyOptions>
                <consistencyLevel>LOCAL_QUORUM</consistencyLevel>
                <serialConsistencyLevel>LOCAL_SERIAL</serialConsistencyLevel>
                <keyspaceCreateEnabled>true</keyspaceCreateEnabled>
                <dynamicConsistencyLevelEnabled>false</dynamicConsistencyLevelEnabled>
                <schemaUpdateEnabled>false</schemaUpdateEnabled>
            </prop>
            <prop name="web-api-path" value="/plugins/cda"/>
            <prop name="DefaultStylesheet" value="${rialto.rootdir}/etc/navigator/plugins/cda/default.xsl"/>
            <prop name="UserManagementURL" value="${Usermanagement-AuthenticatedURL}"/>

            <include location="patientidentitydomains.xml"/>
            <include location="cacheStorage.xml"/>
        </config>
    </service>


    <service type="navigator.server" id="navigator.server">
        <device idref="pix" />
        <device idref="pdq" />
        <device idref="xdsrep"/>
        <device idref="xdsreg"/>

        <config>
            <prop name="CrossAffinityDomainID" value="${System-AffinityDomain}"/>
            <prop name="CassandraConfiguration">
                <clusterHosts>${CassandraClusterHosts}</clusterHosts>
                <clusterDatacenterName>${CassandraClusterDatacenterName}</clusterDatacenterName>
                <keyspaceName>${CassandraKeyspacePrefix}navigator</keyspaceName>
                <keyspaceReplicationStrategy>NetworkTopologyStrategy</keyspaceReplicationStrategy>
                <keyspaceReplicationStrategyOptions>${CassandraReplicationOption}</keyspaceReplicationStrategyOptions>
                <consistencyLevel>LOCAL_QUORUM</consistencyLevel>
                <serialConsistencyLevel>LOCAL_SERIAL</serialConsistencyLevel>
                <keyspaceCreateEnabled>true</keyspaceCreateEnabled>
                <dynamicConsistencyLevelEnabled>false</dynamicConsistencyLevelEnabled>
                <schemaUpdateEnabled>false</schemaUpdateEnabled>
            </prop>
            <prop name="Cluster">
                <name>NavigatorServerClusterGroupId</name>
                <udpstack>
                    <bindAddress>${Navigator-Server-Cluster-Bind-IP}</bindAddress>
                    <multicastAddress>${Navigator-Server-Cluster-Multicast-IP}</multicastAddress>
                    <multicastPort>${Navigator-Server-Cluster-Multicast-PORT}</multicastPort>
                </udpstack>
            </prop>

            <!-- Rialto XDS Registry cannot search by extended metadata -->
            <prop name="ExtendedMetadataAttributes"></prop>
            <prop name="SystemDefaultPermissions">
                <permission name="rialto.navigator.patients.search.internal" value="true"/>
                <permission name="rialto.navigator.patients.search.external" value="false"/>
                <permission name="rialto.navigator.study.metadata.view" value="true" />
                <permission name="rialto.navigator.study.move" value="true" />
                <permission name="rialto.navigator.study.changeavailability" value="false" />
                <permission name="rialto.navigator.study.physical.delete" value="false" />
                <permission name="rialto.navigator.study.qc" value="false" />
                <permission name="rialto.navigator.study.reconciliation" value="false"/>
                <permission name="rialto.navigator.study.version.restore" value="false" />
                <permission name="rialto.navigator.docs.metadata.view.unknown.internal" value="true"/>
                <permission name="rialto.navigator.docs.metadata.view.unknown.external" value="true"/>
                <permission name="rialto.navigator.docs.metadata.edit.unknown.internal" value="false"/>
                <permission name="rialto.navigator.docs.metadata.edit.unknown.external" value="false"/>
                <permission name="rialto.navigator.docs.move.unknown.internal" value="false" />
                <permission name="rialto.navigator.docs.view.unknown.internal.always" value="false"/>
                <permission name="rialto.navigator.docs.view.unknown.external.always" value="false"/>
                <permission name="rialto.navigator.docs.view.unknown.internal.onbreakglass" value="true"/>
                <permission name="rialto.navigator.docs.view.unknown.external.onbreakglass" value="true"/>
                <permission name="rialto.navigator.docs.upload.internal" value="false" />
            </prop>

            <prop name="RialtoImageArchiveAETitles">
                ${Router-AETitle}
            </prop>
            <prop name="DefaultConfidentialityCodeSchemeID" value="1.2.3.4.5.6.1" />
            <prop name="DefaultConfidentialityCodeSchemeName" value="Karos Health demo confidentialityCodes" />
            <prop name="MaximumPatientResultSetSize" value="${Navigator-MaxPatientSearchResults}"/>
            <prop name="MaximumDocumentResultSetSize" value="${Navigator-MaxDocumentSearchResults}"/>
            <prop name="AllowMetadataUpdates">${XDSRegistry-MetadataUpdateEnabled}</prop>
            <prop name="ConfidentialityCodeSchemaName">TEST</prop>
            <prop name="web-api-path" value="${Navigator-APIBasePath}"/>
            <prop name="UseTargetDomainForPDQ">true</prop>

            <include location="patientidentitydomains.xml"/>
            <include location="cacheStorage.xml"/>

            <prop name="XDSDocumentCodes">

                <classification name="classCodes">
                     <Code codeValue="R-3027B" schemeName="SRT" schemeID="2.16.840.1.113883.6.5" displayName="Radiology" />
                     <Code codeValue="R-300E3" schemeName="SRT" schemeID="2.16.840.1.113883.6.5" displayName="Emergency" />
                     <Code codeValue="R-30248" schemeName="SRT" schemeID="2.16.840.1.113883.6.5" displayName="Cardiology" />
                     <Code codeValue="R-3023B" schemeName="SRT" schemeID="2.16.840.1.113883.6.5" displayName="Oncology" />
                     <Code codeValue="R-30250" schemeName="SRT" schemeID="2.16.840.1.113883.6.5" displayName="Dermatology" />
                     <Code codeValue="R-421D4" schemeName="SRT" schemeID="2.16.840.1.113883.6.5" displayName="Endoscopy" />
                     <Code codeValue="R-30254" schemeName="SRT" schemeID="2.16.840.1.113883.6.5" displayName="General Medicine" />
                     <Code codeValue="R-3023D" schemeName="SRT" schemeID="2.16.840.1.113883.6.5" displayName="Intensive Care" />
                     <Code codeValue="R-3025E" schemeName="SRT" schemeID="2.16.840.1.113883.6.5" displayName="Neurology" />
                     <Code codeValue="R-30263" schemeName="SRT" schemeID="2.16.840.1.113883.6.5" displayName="Ob/Gyn" />
                     <Code codeValue="R-3025C" schemeName="SRT" schemeID="2.16.840.1.113883.6.5" displayName="Opthamology" />
                     <Code codeValue="R-3026B" schemeName="SRT" schemeID="2.16.840.1.113883.6.5" displayName="Pathology" />
                     <Code codeValue="R-305EA" schemeName="SRT" schemeID="2.16.840.1.113883.6.5" displayName="Pediatrics" />
                     <Code codeValue="S-8000A" schemeName="SRT" schemeID="2.16.840.1.113883.6.5" displayName="Primary Care" />
                     <Code codeValue="R-30246" schemeName="SRT" schemeID="2.16.840.1.113883.6.5" displayName="Allergy and Immunology" />
                     <Code codeValue="R-30283" schemeName="SRT" schemeID="2.16.840.1.113883.6.5" displayName="Dental Surgery" />
                     <Code codeValue="R-30294" schemeName="SRT" schemeID="2.16.840.1.113883.6.5" displayName="Orthopedic Surgery" />
                </classification>
                                
                <classification name="contentTypeCodes">
                    <Code codeValue="NotUsed" schemeName="Karos demo contentTypeCodes" schemeID="1.2.3.4.5.6.8" displayName="NotUsed" />
                </classification>
                                
                <classification name="typeCodes">
                    <Code codeValue="18842-5" schemeName="LOINC" schemeID="2.16.840.1.113883.6.1" displayName="Discharge Summary" />
                    <Code codeValue="51849-8" schemeName="LOINC" schemeID="2.16.840.1.113883.6.1" displayName="Admission History and Physical Note" />
                    <Code codeValue="67860-7" schemeName="LOINC" schemeID="2.16.840.1.113883.6.1" displayName="Post-operative Note" />
                    <Code codeValue="34094-3" schemeName="LOINC" schemeID="2.16.840.1.113883.6.1" displayName="Cardiology Admit H&amp;P Note" />
                    <Code codeValue="34109-9" schemeName="LOINC" schemeID="2.16.840.1.113883.6.1" displayName="Clinical Note" />
                    <Code codeValue="68629-5" schemeName="LOINC" schemeID="2.16.840.1.113883.6.1" displayName="Allergy and Immunology Note" />
                    <Code codeValue="34752-6" schemeName="LOINC" schemeID="2.16.840.1.113883.6.1" displayName="Cardiology Note" />
                    <Code codeValue="34754-2" schemeName="LOINC" schemeID="2.16.840.1.113883.6.1" displayName="Critical Care Note" />
                    <Code codeValue="28618-7" schemeName="LOINC" schemeID="2.16.840.1.113883.6.1" displayName="Dentistry Note" />
                    <Code codeValue="34759-1" schemeName="LOINC" schemeID="2.16.840.1.113883.6.1" displayName="Dermatology Note" />
                    <Code codeValue="34878-9" schemeName="LOINC" schemeID="2.16.840.1.113883.6.1" displayName="Emergency Medicine Note" />
                    <Code codeValue="34765-8" schemeName="LOINC" schemeID="2.16.840.1.113883.6.1" displayName="General Medicine Note" />
                    <Code codeValue="34780-7" schemeName="LOINC" schemeID="2.16.840.1.113883.6.1" displayName="Hematology Oncology Note" />
                    <Code codeValue="34778-1" schemeName="LOINC" schemeID="2.16.840.1.113883.6.1" displayName="Obstetrics and Gynecology Note" />
                    <Code codeValue="34815-1" schemeName="LOINC" schemeID="2.16.840.1.113883.6.1" displayName="Orthopaedic Note" />
                    <Code codeValue="34832-6" schemeName="LOINC" schemeID="2.16.840.1.113883.6.1" displayName="Radiation Oncology Note" />
                    <Code codeValue="11488-4" schemeName="LOINC" schemeID="2.16.840.1.113883.6.1" displayName="Consult Note" />
                    <Code codeValue="34099-2" schemeName="LOINC" schemeID="2.16.840.1.113883.6.1" displayName="Cardiology Consult Note" />
                    <Code codeValue="73575-3" schemeName="LOINC" schemeID="2.16.840.1.113883.6.1" displayName="Radiology Consult Note" />
                    <Code codeValue="18748-4" schemeName="LOINC" schemeID="2.16.840.1.113883.6.1" displayName="Diagnostic Imaging Report - text/scanned" />
                    <Code codeValue="72230-6" schemeName="LOINC" schemeID="2.16.840.1.113883.6.1" displayName="Diagnostic Imaging Report - structured CDA" />
                    <Code codeValue="57170-3" schemeName="LOINC" schemeID="2.16.840.1.113883.6.1" displayName="Cardiology Referral Note" />
                    <Code codeValue="34124-8" schemeName="LOINC" schemeID="2.16.840.1.113883.6.1" displayName="Cardiology Outpatient Progress Note" />
                    <Code codeValue="34896-1" schemeName="LOINC" schemeID="2.16.840.1.113883.6.1" displayName="Cardiology Interventional Procedure Report" />
                    <Code codeValue="18142-0" schemeName="LOINC" schemeID="2.16.840.1.113883.6.1" displayName="Cardiology Echo Heart Chambers Report" />
                    <Code codeValue="18145-3" schemeName="LOINC" schemeID="2.16.840.1.113883.6.1" displayName="Cardiology Echo Heart Valves Report" />
                    <Code codeValue="18140-4" schemeName="LOINC" schemeID="2.16.840.1.113883.6.1" displayName="Cardiology Echo Report" />
                    <Code codeValue="72170-4" schemeName="LOINC" schemeID="2.16.840.1.113883.6.1" displayName="Dermatology Image" />
                    <Code codeValue="34759-1" schemeName="LOINC" schemeID="2.16.840.1.113883.6.1" displayName="Dermatology Report" />
                    <Code codeValue="34122-2" schemeName="LOINC" schemeID="2.16.840.1.113883.6.1" displayName="Pathology Procedure Note" />
                    <Code codeValue="60570-9" schemeName="LOINC" schemeID="2.16.840.1.113883.6.1" displayName="Pathology Report - text/scanned" />
                    <Code codeValue="60571-7" schemeName="LOINC" schemeID="2.16.840.1.113883.6.1" displayName="Pathology Report - structured CDA" />
                </classification>
                                
                <classification name="formatCodes">
                    <Code codeValue="1.2.840.10008.1.2.4.50" schemeName="DICOM" schemeID="1.2.840.10008.2.6.1" displayName="JPEG" />
                    <Code codeValue="urn:ihe:rad:PDF" schemeName="IHE" schemeID="1.3.6.1.4.1.19376.1.2.3" displayName="PDF" />
                    <Code codeValue="urn:ihe:iti:xds-sd:text:2008" schemeName="IHE" schemeID="1.3.6.1.4.1.19376.1.2.3" displayName="Scanned Document" />
                    <Code codeValue="1.2.840.10008.5.1.4.1.1.88.59" schemeName="DICOM" schemeID="1.2.840.10008.2.6.1" displayName="DICOM KOS" />
                    <Code codeValue="urn:ihe:rad:TEXT" schemeName="IHE" schemeID="1.3.6.1.4.1.19376.1.2.3" displayName="CDA Wrapped Text" />
                    <Code codeValue="urn:ihe:rad:CDA:ImagingReportStructuredHeadings:2013" schemeName="IHE" schemeID="1.3.6.1.4.1.19376.1.2.3" displayName="CDA Document" />
                    <Code codeValue="urn:ihe:iti:bppc:2007" schemeName="IHE" schemeID="1.3.6.1.4.1.19376.1.2.3" displayName="Privacy and Consent Coded Form" />
                    <Code codeValue="urn:ihe:iti:bppc-sd:2007" schemeName="IHE" schemeID="1.3.6.1.4.1.19376.1.2.3" displayName="Privacy and Consent Scanned Form" />
                </classification>
                                
                <classification name="practiceSettingCodes">
                    <Code codeValue="NotUsed" schemeName="Karos demo practiceSettingCodes" schemeID="1.2.3.4.5.6.8" displayName="NotUsed" />
                </classification>
                                
                <classification name="healthcareFacilityTypeCodes">
                    <Code codeValue="HOSP" schemeName="HL7" schemeID="2.16.840.1.113883.5.111" displayName="Hospital" />
                    <Code codeValue="CHR" schemeName="HL7" schemeID="2.16.840.1.113883.5.111" displayName="Chronic Care Facility" />
                    <Code codeValue="RH" schemeName="HL7" schemeID="2.16.840.1.113883.5.111" displayName="Rehabilitation Hospital" />
                    <Code codeValue="ER" schemeName="HL7" schemeID="2.16.840.1.113883.5.111" displayName="Emergency Department" />
                    <Code codeValue="ICU" schemeName="HL7" schemeID="2.16.840.1.113883.5.111" displayName="Intensive Care Unit" />
                    <Code codeValue="NCCF" schemeName="HL7" schemeID="2.16.840.1.113883.5.111" displayName="Nursing/Custodial Care Facility" />
                    <Code codeValue="ORTHO" schemeName="HL7" schemeID="2.16.840.1.113883.5.111" displayName="Orthopedics Clinic" />
                    <Code codeValue="PEDC" schemeName="HL7" schemeID="2.16.840.1.113883.5.111" displayName="Pediatrics Clinic" />
                    <Code codeValue="OF" schemeName="HL7" schemeID="2.16.840.1.113883.5.111" displayName="Outpatient Clinic" />
                    <Code codeValue="PROFF" schemeName="HL7" schemeID="2.16.840.1.113883.5.111" displayName="Provider's Office" />
                    <Code codeValue="AMB" schemeName="HL7" schemeID="2.16.840.1.113883.5.111" displayName="Ambulance" />
                </classification>
                                
                <classification name="eventCodes">
                    <!-- Please do NOT reorder this list, e.g., do not put it into alphabetic order. -->
                    <!-- This is the correct order as listed here. -->
                    <Code codeValue="T-D1100" schemeName="SRT" schemeID="2.16.840.1.113883.6.5" displayName="Head" />
                    <Code codeValue="T-D1600" schemeName="SRT" schemeID="2.16.840.1.113883.6.5" displayName="Neck" />
                    <Code codeValue="T-D8000" schemeName="SRT" schemeID="2.16.840.1.113883.6.5" displayName="Upper Extremity" />
                    <Code codeValue="T-32000" schemeName="SRT" schemeID="2.16.840.1.113883.6.5" displayName="Cardiovascular" />
                    <Code codeValue="T-D3000" schemeName="SRT" schemeID="2.16.840.1.113883.6.5" displayName="Chest" />
                    <Code codeValue="T-11501" schemeName="SRT" schemeID="2.16.840.1.113883.6.5" displayName="Cervical Spine" />
                    <Code codeValue="T-11502" schemeName="SRT" schemeID="2.16.840.1.113883.6.5" displayName="Thoracic Spine" />
                    <Code codeValue="T-11503" schemeName="SRT" schemeID="2.16.840.1.113883.6.5" displayName="Lumbar Spine" />
                    <Code codeValue="T-D4000" schemeName="SRT" schemeID="2.16.840.1.113883.6.5" displayName="Abdomen" />
                    <Code codeValue="T-D6000" schemeName="SRT" schemeID="2.16.840.1.113883.6.5" displayName="Pelvis" />
                    <Code codeValue="T-D9000" schemeName="SRT" schemeID="2.16.840.1.113883.6.5" displayName="Lower Extremity" />
                    <Code codeValue="T-D0010" schemeName="SRT" schemeID="2.16.840.1.113883.6.5" displayName="Entire Body" />
                </classification>
                </prop>
        </config>
    </service>

    <service type="rialto.ui" id="rialto.ui">
        <device idref="pix" />
        <device idref="pdq" />

        <server idref="navigator-http" name="web-ui">
            <url>${Navigator-GUIBasePath}</url>
        </server>

        <server idref="authenticated-http" name="web-api">
            <url>/public/*</url>
        </server>

        <config>
            <prop name="ApiURL" value="${Navigator-HostProtocol}://${Navigator-Host}:${Navigator-APIPort}${Navigator-RootPath}/api${Navigator-APIBasePath}"/>
            <prop name="ArrURL" value="${Navigator-HostProtocol}://${Navigator-Host}:${ARR-GUIPort}"/>
            <prop name="UserManagementURL" value="${Usermanagement-URL}"/>
            <prop name="PublicURL" value="${Navigator-HostProtocol}://${Navigator-Host}:${Navigator-APIPort}/public"/>
            <prop name="StudyManagementURL" value="http://${HostIP}:8080${IA-StudyManagement-Path}"/>
            <prop name="WadoURL" value="http://${HostIP}:8080/vault/wado" />
            <prop name="MintApiURL" value="http://${HostIP}:8080/vault/mint" />
            <prop name="DataflowApiURL" value="http://${HostIP}:13337/monitoring" />
            <prop name="DeviceRegistryURL" value="http://${HostIP}:8080/rialto/deviceregistry" />
            <prop name="ModalityWorklistURL" value="http://${HostIP}:8085/mwl" />
            <prop name="DicomQcToolsURL" value="http://${HostIP}:8080/vault/qc" />
            <prop name="IlmURL" value="http://${HostIP}:8080/vault/ilm" />
            <prop name="GlobalInactivitySessionTimeout" value="${IdleUserSessionTimeout}" />
            <prop name="ExternalPDQConfigured" value="false" />
            <prop name="RialtoAsACache" value="false" />
	<prop name="ProductBranding">
		<productVersion>2.2.1</productVersion>
		<companyName>Vital Images</companyName>
		<copyrightDates>2016</copyrightDates>
		<contactAddress>5850 Opus Parkway, Suite 300</contactAddress>
		<contactPhoneNumber>(866) 433 4624</contactPhoneNumber>
		<contactEmail>info@vitalimages.com</contactEmail>
		<contactWebsite>www.vitalimages.com</contactWebsite>
		<supportPhoneNumber>(800) 208 3005</supportPhoneNumber>
		<supportWebsite>support.vitalimages.com</supportWebsite>
	</prop>

            <prop name="SupportedThemes">
                vital
            </prop>
            <prop name="DefaultTheme" value="${Navigator-DefaultTheme}"/>

            <prop name="SupportedLocales">
                en
                fr
            </prop>
            <prop name="DefaultLocale" value="${Navigator-DefaultLocale}"/>

            <prop name="BreakTheGlassOptions">
                <BreakTheGlassOption>Emergency access required for patient care.</BreakTheGlassOption>
            </prop>

            <prop name="Plugins">
                <plugin>
                    <name>${CDAPlugin-Name}</name>
                    <description>${CDAPlugin-Description}</description>
                    <mimeTypes>text/xml</mimeTypes>
                    <url>${Navigator-HostProtocol}://${Navigator-Host}:${Navigator-APIPort}${Navigator-RootPath}/api/plugins/cda</url>
                    <embedded>true</embedded>
                </plugin>

                <plugin>
                    <name>${DownloadPlugin-Name}</name>
                    <description>${DownloadPlugin-Description}</description>
                    <mimeTypes>*</mimeTypes>
                    <url>${Navigator-HostProtocol}://${Navigator-Host}:${Navigator-APIPort}${Navigator-RootPath}/api/plugins/download</url>
                    <embedded>true</embedded>
                </plugin>
            </prop>

            <!-- Configure eventCodeSchemes to support searching for documents by eventCode -->
            <!--prop name="EventCodeSchemes">
                <scheme schemeID="1.2.3" schemeName="Karos Health demo eventCodes" />
            </prop-->

        </config>
    </service>
</config>


