log.info("Start XDSRegistry Morpher")

LOAD("common.groovy")

def messageType = get('/.MSH-9-1')
def triggerEvent = get('/.MSH-9-2')

log.info("messageType is: {}^{}", messageType, triggerEvent)

if (messageType != null && triggerEvent != null) {
    if (["ADT^A30", "ADT^A39", "ADT^A40"].contains(messageType + "^" + triggerEvent)) {
        log.info("will forward message to XDSRegistry")
    }
    else {
        log.info("will not forward message to the XDSRegistry")
        return false
    }
}

def version = get('/.MSH-12')
if (version.equals("2.3")) {
    set('/.MSH-12', "2.3.1")
}


if ("ADT^A30".equals(messageType + "^" + triggerEvent)) {
    log.debug("MWL converting A30 to A40")
    set('/.MSH-9-2', 'A40')
    Domain.addFqdn('/.MRG-1', this)
}

def ext_patientid = Domain.addFqdn( '/.PID-2', this )

if (get('/.PID-3') == null) {
    set('/.PID-3', get('/.PID-2'))
}
def int_patientid = Domain.addFqdn( '/.PID-3', this )

log.info("End XDSRegistry Morpher")
