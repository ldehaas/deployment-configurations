
class Domain {
    static  addFqdn(seg, msg) {

        def patientid = msg.get(seg + '-1')
        def dn = msg.get(seg + '-4')

        if (dn == null) {
            msg.set(seg + '-4-1', 'MC')
            msg.set(seg + '-4-2', '2.16.124.113638.7.3.1.1')
            msg.set(seg + '-4-3', 'ISO')
        }

        return patientid
    }
}
