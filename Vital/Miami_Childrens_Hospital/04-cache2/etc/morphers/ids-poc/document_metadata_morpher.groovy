/*
*   DOCUMENT METADATA MORPHER
*   
*   This morpher takes in the Manifest that Vault will publish (as a KOS DICOM Object), and sets XDS Document Metadata fields that will be used
*   to publish the XDS Registry Document metadata for this particular study.
*
*   For Quebec, the high level goals of this script are:
*       0. As a temporary hack, make the RAMQ into the NIU; and publish with that
*       1. Set the Mandatory fields for the NIU Resolver to accept an XDS Sumbission:
*
*/

/* FOR PRODUCTION:
*       1. Use the RAMQ to publish, as opposed to NIU hack
*       2. Fail submissions that don't have a valid Date of Birth
*       3. Also add the "Missing ^" delimiter logic to the DICOM-receiving script to have correct contexts
*       4. Same as "Missing ^" above, but with PatientSex
*       5. Set the XDSTitle to something meaninful, either DICOM's StudyDescription, or something elicited from the ORU Order
*/

// Toggle this on and off to have Vault to publish the manifests against itself.
    // 'True' means Vault publishes the Manifest only to itself and does *NOT* publish it against the Quebec XDS Registry

    // set(XDSExtendedMetadata('karosDontRegister'), 'true')




/* Constants definition */
def CODE_SCHEME = "Imagerie Qu\u00E9bec-DSQ"



/* TEMPORARY PHASE 2 HACK: Use the RAMQ PID, and the NIU issuer of PID to construct an NIU-compatible Patient ID
*       This will allow us to publish into the XDS Registry without going through the NIU resolver
/

 // Assuming that the incoming PID is a RAMQ Number
    def ramqPid = get('PatientID')
    def ramqIssuer = get('IssuerOfPatientID')

    log.debug("RAMQ Patient ID as Provided in the Manifest: \"{}\"", ramqPid) 
    log.debug("RAMQ Issuer of Patient ID as Provided in the Manifest: \"{}\"", ramqPid) 

 
 // This was not supposed to be done in the script, according to the IDS Service documentation
    // construct a NIU-compatible Identifier to use as NIU-level Patient Identifier
    // use the NIU OID = 2.16.840.1.113883.4.56
    def niuPid = ramqPid + '&&&^' + "2.16.840.1.113883.4.56" + '^ISO'
    log.debug("Value to use in the PatientID XDS field will be: \"{}\"", niuPid)

    set(PatientID, niuPid)
*/


// Mandatory Publishing fields for Quebec

// Quebec-specific XDS Submission criteria: First Name, Last Name, PatientSex, Patient Birth Date 
// These must be supplied to get a high enough Patient Matching score in order to publish

 // Set 1st Mandatory field pair for Patient Resolution: Patient Name Pair 
  if (null != get(PatientName)) {
        def nameParts = get(PatientName)
        log.debug("Debuging XDS Metadata creation, manifest submission: \n RAW Patient Name is \"{}\"", nameParts)
        
        if( ! nameParts.contains("^")) {
        // be able to to handle names with no ^ (circumflex)
            
            log.warn("Debuging XDS Metadata creation, manifest submission: \nCurrent Patient Name \"{}\"did NOT contain a ^ delimiter. \n\n\n XDS Manifest sumbission might fail.\n", nameParts) 
            set(XDSSourcePatientFamilyName, nameParts)
            set(XDSSourcePatientGivenName,"")

        } else {
        
        nameParts = nameParts.split("\\^")
        log.debug("Debuging XDS Metadata creation, manifest submission: \nCurrent Patient Name is \"{}\",\"{}\"", nameParts[0], nameParts[1])
        set(XDSSourcePatientFamilyName, nameParts[0])
        set(XDSSourcePatientGivenName, nameParts[1])
        }
  }

 // Set 2nd Quebec Mandatory Field for Patient Resolution: Birth Date
  log.debug("\n\n\n\n\n\n\nDebuging XDS Metadata creation, manifest submission: \nCurent Patient Birth Date is \"{}\"\n\n\n\n\n\n", get(PatientBirthDate))
  if(null != get(PatientBirthDate)){
        set(XDSSourcePatientBirthDate, get(PatientBirthDate))
        //set(XDSSourcePatientBirthDate, get(PatientBirthDate).substring(0,8))
  } else {
        log.warn("\n\nWARNING:(XDS Metadata creation, manifest submission) Patient Birth Date from DICOM was blank.  XDS Submission will FAIL!!\n")
        log.warn("Temporarily hard setting the Patient Birth Date to \"1921-11-28\"")
        set(XDSSourcePatientBirthDate, '19211128')
  } 

 // Set 3rd Mandatory Field for Patient Resolution: Patient Sex
  if (null != get(PatientSex)) {
    def patientSex = get(PatientSex)
    set(XDSSourcePatientSex, patientSex) 
    log.debug("Debuging XDS Metadata creation, manifest submission: \n Patient Sex passed in from DICOM is \"{}\"", patientSex) 
  } else {
    log.warn("\n\nWARNING:(XDS Metadata creation, manifest submission) Patient Sex from DICOM was blank.  XDS Submission will FAIL!!\n\n")
    log.warn("Temporarily hard setting the Patient Birth Date to \"M\"")
    set(XDSSourcePatientSex, 'M')
  }




def suid = get('StudyInstanceUID')
    log.debug("\n\n\n\n\n\n******\n******\nDebuging XDS Metadata creation, manifest submission: \n Current Study Instance UID is \"{}\"", suid)
// aront - 130529
// Commenting the following line because our registry does not support this slot!
//set(XDSExtendedMetadata('studyInstanceUID'), suid)

set(XDSLanguage, 'fr-CA') // how do we know if it's french or english?
//set(XDSLanguage, 'en-CA') // how do we know if it's french or english?
set(XDSConfidentialityCode, code('Test Confidentiality Code R', 'Test Scheme'))


set(XDSHealthcareFacilityTypeCode, code('Test Health Care Facility Type Code R', 'Test Scheme'))


// THIS IS STILL A TO-DO
set(XDSAuthorFamilyName, 'Vault')
set(XDSAuthorGivenName, 'Rialto')
//set(XDSAuthorID, 'arod')

// Use the Quebec Default
// set(XDSClassCode, code('examen imagerie', "Imagerie Québec-DSQ", 'examen imagerie'))
set(XDSClassCode, code("examen imagerie", CODE_SCHEME, 'examen imagerie'))


// XDS creation time 
 
    if  ( null != get(StudyDate)) {
        log.debug("\nXDS Creation Date: Using Study Date\n")
        set(XDSCreationTime, get(StudyDate), get(StudyTime))
    } /* else if( null != get(InstanceCreationDate)) {
    // Instance Creation Date supercedes Instance Creation Time
        log.debug("\nXDS Creation Date: Using Instance Creation Date, {}\n", get(InstanceCreationDate))
        set(XDSCreationTime, get(InstanceCreationDate), get(InstanceCreationTime))
    } */ else {
        log.debug("\nXDS Creation Date: Using Current Date\n")
        set(XDSCreationTime, new Date())       
    }

 
// set(XDSCreationTime, get(SeriesDate), get(SeriesTime))

/*
def dateTime = get(SeriesDate) + get(SeriesTime)
if(null != dateTime) {
    set(XDSCreationTime, dateTime)
} else {
    set(XDSCreationTime, new Date())       
}
*/
// Set this to <Code code="urn:ihe:rad:1.2.840.10008.5.1.4.1.1.88.59" display="urn:ihe:rad:1.2.840.10008.5.1.4.1.1.88.59" codingScheme="1.2.840.10008.2.6.1" /> 
 set(XDSFormatCode, code('urn:ihe:rad:1.2.840.10008.5.1.4.1.1.88.59', '1.2.840.10008.2.6.1', 'urn:ihe:rad:1.2.840.10008.5.1.4.1.1.88.59'))
//set(XDSFormatCode, code('1.2.840.10008.5.1.4.1.1.88.59', '1.2.840.10008.2.6.1'))

// Set this to <Code code="application/dicom"/>
 set(XDSMimeType, 'application/dicom')

// Set EventCodeList
 modalitiesInStudy = new ArrayList( getList(ModalitiesInStudy) )
 log.debug("\n\n\n\n\n\n\nDebuging XDS Metadata creation, manifest submission: \nCurrent Modalities in Study: \"{}\"\n\n\n\n\n\n", modalitiesInStudy)
 def modalitiesWereNull = false

 if(null == modalitiesInStudy){
    // This is the official code for empty Modality List
    eventCodes = code("NONE", "DCM","NONE",'en-CA')   
    modalitiesWereNull = true 
    log.info("Debuging XDS Metadata creation, manifest submission: \nModalities in Study are NULL.  Falling back to \"NONE\" default.", modalitiesInStudy)
 } else{
   
    // Some McKesson Studies are NOT compatible with the Affinity Domain modalities defined for Quebec
    //   For the POC, we have to either change them into equivalents and WARN
    if (modalitiesInStudy.remove('PR')) {
        modalitiesInStudy.add('PS')
        log.warn("Invalid Modality \"PR\" was received. Using PS instead.")
    }


    giantMap = [
        "AR":"Autorefraction",
        "AU":"Audio",
        "BDUS":"Bone Densitometry (ultrasound)",
        "BI":"Biomagnetic imaging",
        "BMD":"Bone Densitometry  (X-Ray)",
        "CR":"Computed Radiography",
        "CT":"Computed Tomography",
        "DG":"Diaphanography",
        "DOC":"Document",
        "DX":"Digital Radiography",
        "ECG":"Electrocardiography",
        "EPS":"Cardiac Electrophysiology",
        "ES":"Endoscopy",
        "FID":"Fiducials",
        "GM":"General Microscopy",
        "HC":"Hard Copy",
        "HD":"Hemodynamic Waveform",
        "IO":"Intra-oral Radiography",
        "IVUS":"Intravascular Ultrasound",
        "KER":"Keratometry",
        "KO":"Key Object Selection",
        "LEN":"Lensometry",
        "LS":"Laser surface scan",
        "MG":"Mammography",
        "MR":"Magnetic Resonance",
        "NM":"Nuclear Medicine",
        "OAM":"Ophthalmic Axial Measurements",
        "OCT":"Optical Coherence Tomography",
        "OP":"Ophthalmic Photography",
        "OPM":"Ophthalmic Mapping",
        "OPR":"Ophthalmic Refraction",
        "OPT":"Ophthalmic Tomography",
        "OPV":"Ophthalmic Visual Field",
        "OT":"Other",
        "PS":"Presentation State",
        "PT":"Positron emission tomography",
        "PX":"Panoramic X-Ray",
        "RF":"Radiofluoroscopy",
        "RG":"Radiographic imaging",
        "RTIMAGE":"Radiotherapy Image",
        "REG":"Registration",
        "RESP":"Respiratory Waveform",
        "RTDOSE":" Radiotherapy Dose",
        "RTPLAN":" Radiotherapy Plan",
        "RTRECORD":"RT Treatment Record",
        "RTSTRUCT":"Radiotherapy Structure Set",
        "SC":"Secondary Capture",
        "SEG":"Segmentation",
        "SM":"Slide Microscopy",
        "SMR":"Stereometric Relationship",
        "SR":"SR Document",
        "SRF":"Subjective Refraction",
        "TG":"Thermography",
        "US":"Ultrasound",
        "VA":"Visual Acuity",
        "XA":"X-Ray Angiography",
        "XC":"External-camera Photography",
        "NONE":"NONE"
    ].withDefault{"Other"}
    
    eventCodes = modalitiesInStudy.collect{  code(it, 'DCM', giantMap[it], 'en-CA') }
    log.debug("Debuging XDS Metadata creation, manifest submission: \nEventCodes to Use are:\"{}\" default.", eventCodes)
     
 }

 // anatomicRegions = "TH"
 /* These are supposed to be set from the HL7 ORM, hard-coding them for the PoC, will have to modify this to actually get it from HL7, NOT DICOM
 anatomicRegions = get(AnatomicRegionSequence)
 anatomicRegions = anatomicRegions.split('\\\\')
 */

 // if(null != anatomicRegions) {
  // Anatomic Regions are optional

   /* Uncomment this when the Anatomic Regions are actually gotten from the HL7 ORM
        anatomicRegions = anatomicRegions.split('\\\\')
    
    anatomicMap = [
        "AB":"Abdomen",
        "AN":"Angiographie",
        "AU":"Autre",
        "CA":"Cardio-vasculaire",
        "CO":"Colonnes",
        "DI":"Digestif",
        "EI":"Extrémités inférieures",
        "EN":"Endocrinien",
        "ES":"Extrémités supérieures",
        "GO":"Gynéco-obstétrique",
        "HE":"Hémodynamie",
        "HP":"Hématopoïétique",
        "SE":"Sein",
        "SQ":"Squelettique",
        "TC":"Tête et cou",
        "TH":"Thorax",
        "TP":"Tomographie par émission de positrons"
    ].withDefault{"Autre"}
    // MUST Map regions to to anatomicMap using collect() when HL7 ORM workflow is ready
    
    
    if ( !modalitiesWereNull ) {

        eventCodes += anatomicRegions.collect {
            code(it, CODE_SCHEME, anatomicMap[it], 'fr-CA')
        }
        
        log.debug("Debuging XDS Metadata creation, manifest submission: \nEvent Codes + Anatomic Regions to Use are:\"{}\" default.", eventCodes)
    }

 // End of Anatomic Regions Logic
 }*/

 eventCodes += code('TH', CODE_SCHEME, 'Thorax', 'fr-CA') 

 set(XDSEventCode, eventCodes)
 //set(XDSEventCode, [eventCodes, code('TH', CODE_SCHEME, 'Thorax', 'fr-CA')]) 


 // HACKING THIS TEMPORARILY
 /* eventCodes = code("CT",'DCM',"Computed Tomography",'en-CA')
 eventCodes += code('TH', "Imagerie Québec-DSQ", 'Thorax', 'fr-CA')
 
 set(XDSEventCode, eventCodes)
 */
 // set (XDSEventCode, [code("CT",'DCM',"Computed Tomography",'en-CA'), code('TH', CODE_SCHEME, 'Thorax', 'fr-CA')]) 



// Set this to "Radiologie"
set(XDSPracticeSettingCode, code('Radiologie', CODE_SCHEME, 'Radiologie'))

// AuthorPerson -> Referring Physician in the XCN type form: licenseNumber^Family^Given
 def referringPhysician = get(ReferringPhysicianName)
 log.debug("Debuging XDS Metadata creation, manifest submission: \nCurrent Referring Physician Name is \"{}\"", referringPhysician)
    def refDocLastName = null
    def refDocFirstName = null
    def refDocLicense = null

 if(null != referringPhysician) {
 // Populate the Author Person from Referring Physician if passed 
    if(! referringPhysician.contains("^")) {
    // be able to to handle names with no ^ (caret)
        
        // write HARDCODEDLICENSE^last^NULL
        log.warn("Debuging XDS Metadata creation, manifest submission: \nCurrent Referring Physician Field \"{}\"did NOT contain a ^ delimiter. \n\n\n XDS Manifest sumbission might fail.\n", referringPhysician)
        set(XDSAuthorFamilyName, referringPhysician)
        set(XDSAuthorGivenName, '')
        //set(XDSAuthorID, '1042878')
        set(XDSAuthorID, '726300')

    } else {
        def docNameParts = referringPhysician.split("\\^") 
        refDocLastName = docNameParts[0]
        refDocFirstName = docNameParts[1] 
        log.debug("Debuging XDS Metadata creation, manifest submission: \nCurrent Referring Doc Firts Name is \"{}\", current last name is \"{}\"", refDocFirstName, refDocLastName)

        // write HARDCODEDLICENSE^last^first
        set(XDSAuthorFamilyName, refDocLastName)
        set(XDSAuthorGivenName, refDocFirstName)
//        set(XDSAuthorID, '1042878')
        set(XDSAuthorID, '726300')
    }

 } else {
    // If no Referrying Physician is passed, hardcode everything
    set(XDSAuthorFamilyName, 'LaPotion') 
    set(XDSAuthorGivenName, 'Paul')
    set(XDSAuthorID, '726300') 
 }


// The repository sets this, not the client
//set(RepositoryUniqueId, '2.16.124.10.101.1.60.2.81')

// Set the Service Start time from StudyDate + StudyTime
 def studyDate = get(StudyDate)
    if(null == StudyDate) {
        log.warn("Debuging XDS Metadata creation, manifest submission: \nCurrent Study Date was NULL.  XDS Manifest submission might fail!!")
    }
 def studyTime = get(StudyTime)
    if(null == StudyTime) {
        log.warn("Debuging XDS Metadata creation, manifest submission: \nCurrent Study Time was NULL.  XDS Manifest submission might fail!!")
    }
 def fullStudyTime = studyDate + studyTime
 log.debug("Debuging XDS Metadata creation, manifest submission: \nCurrent Study Time is \"{}\"", fullStudyTime)


 
 if ( null != get(StudyDate)) {
    log.debug("\nXDS Service Start Time: Using Study Date\n")
    set(XDSServiceStartTime, get(StudyDate), get(StudyTime))
 }
 /* else if( null != get(InstanceCreationDate)) {
 // Study Date supercedes Study Time
    log.debug("\nXDS Service Start Time: Using Instance Creation Date, {}\n", get(StudyDate))
    set(XDSServiceStartTime, get(InstanceCreationDate), get(InstanceCreationTime))
 } */ else { 
    log.debug("\nXDS Service Start Time: Using Current Date\n")
    log.warn("XDS Metadata Creation: Service Start Time was blank.  Using current Date and Time")
    set(XDSServiceStartTime, new Date())       
 }



// Setting (Study) Title in Metadata from DICOM Study Description
 // Hard-coding for now, as this debug should be in the ORM order
 //set(XDSTitle, '92203-POC-TEST')
 set(XDSTitle, get(StudyDescription) + get(Modality))

// Setting SourcePatientID from LocalID from the RIS, including the Assigning Authority
 def localPID = get(PatientID)
    // Sanity Check
    if(null == localPID) {
        log.warn("XDS Metadata creation, manifest submission:\nLocal Patient ID was NULL.  Manifest submission will likely fail!!")
    }
    
 def issuerOfLocalPID = get(IssuerOfPatientID)
    // Sanity Check
    if(null == issuerOfLocalPID) {
        log.warn("XDS Metadata creation, manifest submission:\nLocal Domain was NULL.  Manifest submission will likely fail!!")
    }

 def qualifiedLocalPID = localPID + issuerOfLocalPID
    // Sanity Check
    if(null == qualifiedLocalPID) {
        log.warn("XDS Metadata creation, manifest submission:\nLocal Patient ID and Issuer were *both*  NULL.  Manifest submission will likely fail!!")
    }
 
 log.debug("Debuging XDS Metadata creation, manifest submission: \nLocal Patient ID is \"{}\", and its Issuer is: \"{}\"", localPID, issuerOfLocalPID) 
 set(XDSSourcePatientID, localPID, issuerOfLocalPID)
 

// Setting TypeCode from the "Code Value" of the "Requested Procedure Code" sequence
 
 def reqProcCodCodeValue = get("RequestedProcedureCodeSequence/CodeValue")
 def reqProcCodDisplayName = get ("RequestedProcedureCodeSequence/CodeMeaning")
 // def reqProdCodCodeValue = getList("RequestedProcedureCodeSequence/CodeValue")
 log.debug("\n\n\n\n\n\n\n\n\n\nDebuging XDS Metadata creation, manifest submission: \nRequested Procedure Code Value is \"{}\"", reqProcCodCodeValue) 

 if(null != reqProcCodCodeValue) {
    if (null != reqProcCodDisplayName) {
        set(XDSTypeCode, code(reqProcCodCodeValue, CODE_SCHEME, reqProcCodDisplayName))
    } else {
        set(XDSTypeCode, code(reqProcCodCodeValue, CODE_SCHEME, "Test TypeCode Display Name 2 CHANGED"))
    }

 } else {
    // CodeValue passed in DICOM was null
    // Currently, hard-coding this value, as everything passed from the PACS will be blank
    // Eventually should get this info from HL7 ORM
    set(XDSTypeCode, code("74441", CODE_SCHEME, "Test TypeCode Display Name 2 CHANGED"))
    log.warn("\nDebuging XDS Metadata creation, manifest submission:\n Passed CodeValue (Requested Procedure Code) was NULL. Falling back to default value.")
 }

 /* OLD CODE:
 def codeValue = get(CodeValue)
 log.debug("Debuging XDS Metadata creation, manifest submission: \nProcedure Code Value is \"{}\"", codeValue) 

 if(null != codeValue) {
    set(XDSTypeCode, codeValue)
 } else {
    // CodeValue passed in DICOM was null
    // Currently, hard-coding this value, as everything passed from the PACS will be blank
    // Eventually should get this info from HL7 ORM
    set(XDSTypeCode, code("74441", CODE_SCHEME, "Test TypeCode Display Name 2 CHANGED"))
    log.warn("Debuging XDS Metadata creation, manifest submission:\n Passed CodeValue (Requested Procedure Code) was NULL. Falling back to default value.")
 }
 */



// patient id is set by the system
//set(XDSPatientID, 'pid', 'issuer')

// Write out the output object for debugging purposes:
//log.debug("XDS Manifest Publishing debug:\nThis is the object with the submissions about to be sent to the XDS Registry:\n{}\n\n\n", output)
