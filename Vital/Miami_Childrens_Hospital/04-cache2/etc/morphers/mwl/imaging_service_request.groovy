log.info("Here's how the order message looks:]\n{}", input)
log.info("And here's how the initial Imaging Service Request looks like\n{}", imagingServiceRequest)

if (imagingServiceRequest.getAccessionNumber() == null) {
    //accessionNumber OBR-18 is not set; will copy value from ORC-2 to OBR-18
    imagingServiceRequest.setAccessionNumber(input.get("ORC-2"))
    imagingServiceRequest.setAccessionNumberNamespaceId("MC")
    imagingServiceRequest.setAccessionNumberUniversalId("2.16.124.113638.7.3.1.1")
    imagingServiceRequest.setAccessionNumberUniversalIdType("ISO")
}

def requestedProcedure = imagingServiceRequest.getRequestedProcedureSequence().get(0)
requestedProcedure.setRequestedProcedureID(imagingServiceRequest.getAccessionNumber())

def scheduledProcedureStep = requestedProcedure.getScheduledProcedureStepSequence().get(0)
scheduledProcedureStep.setScheduledProcedureStepIDString(imagingServiceRequest.getAccessionNumber())

def currentStatus = scheduledProcedureStep.getScheduledProcedureStepStatus()
log.debug("currentStatus={}", currentStatus)

if (input.get("ORC-1") == "SC") {
    log.debug("ORC-1 is SC")

    if (input.get("ORC-5") == "IP") {
        scheduledProcedureStep.setScheduledProcedureStepStatus("INPROGRESS")
    } else if (input.get("ORC-5") == "CM") {
        scheduledProcedureStep.setScheduledProcedureStepStatus("COMPLETED")
    } else if (input.get("ORC-5") == "CA") {
        scheduledProcedureStep.setScheduledProcedureStepStatus("DISCONTINUED")
    } else {
        log.debug("imagingServiceRequest returning false")
        return false
    }
}
else {
    if (input.get("ORC-1") == "NW") {
        scheduledProcedureStep.setScheduledProcedureStepStatus("SCHEDULED")
    } else if (input.get("ORC-1") == "CA") {
        scheduledProcedureStep.setScheduledProcedureStepStatus("DISCONTINUED")
    } else {
        log.debug("imagingServiceRequest returning false")
        return false
    }
}

log.info("Finished fixing up the Imaging Service Request...Here it is\n{}", imagingServiceRequest)
