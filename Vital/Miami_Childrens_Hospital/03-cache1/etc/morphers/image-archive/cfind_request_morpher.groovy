// ****************************************************
// KHC #8477 - C-Find request morpher
// Purpose: Include fully qualified issuer of patient ID
// Date Created: 06/01/2017
// ****************************************************

log.info("Applying C-Find request morpher ")
log.debug("Entering C-Find request morpher")
def mrn = get(PatientID)
def issuer = get(IssuerOfPatientID)

if (mrn == null || mrn.isEmpty() || mrn.contains("MC&2.16.124.113638.7.3.1.1&ISO")) {
    log.debug("Patient ID is not specified or already contains fully qualified issuer")
    return false
}else{
    // overwrite issuer of patient ID no matter what the user input is
    // set(IssuerOfPatientID, "MC&2.16.124.113638.7.3.1.1&ISO")
    log.debug("Overwriting IssuerOfPatientID to MC&2.16.124.113638.7.3.1.1&ISO")

    set('IssuerOfPatientID', 'MC')
    set('IssuerOfPatientIDQualifiersSequence/UniversalEntityID', '2.16.124.113638.7.3.1.1')
    set('IssuerOfPatientIDQualifiersSequence/UniversalEntityIDType', 'ISO')
}

log.debug("Exiting C-Find request morpher. Query modified to: {}", output)
