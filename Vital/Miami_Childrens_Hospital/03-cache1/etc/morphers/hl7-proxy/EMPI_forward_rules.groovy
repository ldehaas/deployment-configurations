log.info("Start EMPI Morpher") 

LOAD("common.groovy")

def messageType = get('/.MSH-9-1') 
def triggerEvent = get('/.MSH-9-2') 

if (!"ADT".equalsIgnoreCase(messageType)) { 
    log.debug('EMPI Morpher - taking no action on message {}^{}', messageType, triggerEvent)
    return false
}

def version = get('/.MSH-12')
if (version.equals("2.3")) {
    set('/.MSH-12', "2.3.1")
}

if ("ADT^A30".equals(messageType + "^" + triggerEvent)) {
    log.debug("MWL converting A30 to A40")
    set('/.MSH-9-2', 'A40')
    Domain.addFqdn('/.MRG-1', this)
}
if ("ADT^A31".equals(messageType + "^" + triggerEvent)) {
    set('/.MSH-9-2', 'A08')
}

Domain.addFqdn( '/.PID-2', this )

log.debug('PID-3={}', get('/.PID-3'))
if (get('/.PID-3') == null) {
    log.debug("PID-3 is null; setting to {}", get('/.PID-2'))
    set('/.PID-3', get('/.PID-2'))
}
Domain.addFqdn( '/.PID-3', this )

log.info("End EMPI Morpher")
