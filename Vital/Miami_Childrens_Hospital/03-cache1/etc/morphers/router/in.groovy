log.debug("in.groovy: Start inbound morpher")

def issuerOfPatientId = get(IssuerOfPatientID)
def universalEntityID = get("IssuerOfPatientIDQualifiersSequence/UniversalEntityID")
def universalEntityIDType = get("IssuerOfPatientIDQualifiersSequence/UniversalEntityIDType")

set(IssuerOfPatientID, "MC")
set("IssuerOfPatientIDQualifiersSequence/UniversalEntityID", "2.16.124.113638.7.3.1.1")
set("IssuerOfPatientIDQualifiersSequence/UniversalEntityIDType", 'ISO')

log.debug("in.groovy: End inbound morpher")
