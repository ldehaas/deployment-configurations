# ---------- ---------- ---------- ---------- ----------
# English localization file for VioArchive Navigator.
# ---------- ---------- ---------- ---------- ----------

en:

    time:
        formats:
        # Use the strftime parameters for formats.
        # When no format has been given, it uses default.
        # You can provide other formats here if you like!
          default: "%Y-%m-%d"
          short: "%Y-%m-%d"
          long: "%Y-%m-%d %H:%M"
          extra_long: "%Y-%m-%d %H:%M:%S.%L"
          full: "%d %b %Y %I:%M:%S %p"
          full_date: "%d %b %Y"
          full_time: "%I:%M:%S %p"
        am: "am"
        pm: "pm"
    date:
        abbr_month_names:
          [~, 'Jan', 'Feb', 'Mar', 'Apr', 'May', 'June', 'July', 'Aug', 'Sept', 'Oct', 'Nov', 'Dec']
        month_names:
          [~, 'January', 'February', 'March', 'April', 'May', 'June', 'July', 'August', 'September', 'October', 'November', 'December']
        day_names:
          ['Sunday', 'Monday', 'Tuesday', 'Wednesday', 'Thursday', 'Friday', 'Saturday']

    select:
        confidentiality_code: "Select a confidentiality code"
        practiceSettingCode: "Select a practice setting code"
        formatCode: "Select a format code"
        contentTypeCode: "Select a content type code"
        classCode: "Select a class code"
        typeCode: "Select a type code"
        healthcareFacilityTypeCode: "Select a healthcare facility type code"
        languageCode: "Select a language code"
        gender: "Select a gender"
        domain: "Select a domain"
        dictionary: "Select a dictionary"
        extendedMetadata: "Select metadata"
        realm: "Select a realm"
        creationTime: "Select a creation time"
        studyDate: "Select a study date time"
        studyDateStart: "Select a start time"
        modifiedTime: "Select a modified time"
        studySendStatus: "Select a status"
        workflowType: "Select a type"

    # ---------- ---------- ---------- ---------- ----------
    # Sign-in screen
    # ---------- ---------- ---------- ---------- ----------

    sign_in_title: "Sign in"
    user_id: "User ID"
    user_password: "Password"
    sign_in: "Sign in"
    
    user_id_or_password_not_correct: "The username or password is incorrect."
    already_signed_in: "You are already signed in as '%{user_id}'. Do you want to stay signed in, or sign out and then sign in as another user?"
    already_signed_in_stay: "Stay signed in"
    already_signed_in_signout: "Sign out"
    already_signed_in_no_permissions: "You have signed in, but have no permissions set that enable you to do anything. Sign out, and talk with your administrator."
    
    # login: "Login"
    
    sign_in_page: "Sign in page"

    # ---------- ---------- ---------- ---------- ----------
    # Branding, general admin
    # ---------- ---------- ---------- ---------- ----------

    product_component_patients_documents: "Patients & Documents"
    product_component_audit_records: "Audit Records"
    product_component_events: "Event Records"
    product_component_users_groups: "Users & Groups"
    product_component_system_configuration: "System Configuration"
    product_component_studies: "DICOM Studies"
    product_component_workflows: "Workflow Tracking"
    product_component_dataflows: "Dataflow Tracking"
    product_component_quality_management: "Quality Management"
    product_name_short_navigator: "Navigator"
    product_name_short_audit_repository: "Repository"
    
    product_navigation: "Components"
    
    product_component:
        EventViewer: "Event Records"
        ArrViewer: "Audit Records"
        StudyViewer: "DICOM Studies"
        Systemconfig: "System Configuration"
        Usermanagement: "Users & Groups"
        PatientsAndDocuments: "Patients & Documents"
        Workflows: "Workflow Tracking"
        QualityControl: "Quality Management"
        Dataflows: "Dataflow Tracking"

    admin_link_signout: "Sign out"
    
    admin:
        prefs:
            signed_in: "Signed in as"
            name: "Name"
            org: "Domain"
            language: "Language"
            languageChoice:
                en: "English"
                fr: "Français"
                it: "Italiano"
            group: "Group"

            button:
                save:  "Save"
    
    page_generated_timestamp: "Page generated at %{time} on %{date}"

    about_title: "About VioArchive"
    product_branding:
        labels:
            release: "Release"
            build: "Build"
            website: "Website"
            manufactured_date: "Date of Manufacture"
            copyright: "Copyright"
            contact:
                telephone:
                    general: "Tel"
                    support: "Support"
                email: "Email"
                web:
                    general: "Website"
                    support: "Support"

        categories:
            intended_use: 
                "<p>VioArchive manages clinical information within a single facility, or across distributed healthcare information exchanges.</p>
                 <p>VioArchive is a secure, patient-centric platform based on open standards (HL7, DICOM, IHE XDS, and MINT) which provides cross-enterprise sharing of clinical images and documents and enables seamless integration between healthcare systems.</p>
                 <p>VioArchive's sophisticated yet simple architecture is highly scalable, data-format agnostic, and extremely performant, delivering the essential services required to manage massive amounts of health data, enable collaboration between providers, and provide a foundation for community-wide care.</p>"
            manufactured_by: "Manufactured by Vital Images, Inc."
            trademarks_statement:  "All rights reserved. All trademarks named and shown are trademarks of the respective proprietor and are recognized as being protected."
            copyright:
                symbol: "&#169;"
                statement: "%{symbol} %{dates} Vital Images, Inc."
    pre_release_warning: "This software is pre-release and is not intended for clinical use"


    # ---------- ---------- ---------- ---------- ----------
    # General labels
    # ---------- ---------- ---------- ---------- ----------

    search_page: "Search"
    document_title: "Title"
    
    unknown: "Unknown"

    edit_list: "Customize view"

    labels_other: "Other"

    button_label_verify: "Verify"
    button_label_cancel: "Cancel"
    button_label_search: "Search"
    button_label_refresh: "Refresh"
    button_label_confirm: "Confirm"
    label_search_time: "Last search time"
    label_search_criteria_minutes:
        one: "%{count} Minute"
        other: "%{count} Minutes"

    # Button processing message
    
    process: "Processing…"
    process_save: "Saving…"
    process_cancel: "Canceling…"
    process_search: "Searching…"
    process_delete: "Deleting…"
    process_signin: "Signing in&#8230;"
    done: "Done"
   
    search_pleaseStartSearch: "Please start a search"
    search_searchedFor: "Searched for"
    search_searchedAll: "Searched for all"
    search_unconstrained: "Please specify some parameters to perform a search."
    search_label_saved_search: "From saved search"
    search_message_missing_issuerOfPatientID: "When searching for a Patient ID, you must provide an Issuer of Patient ID."
    search_message_missing_patientID: "When searching for an Issuer of Patient ID, you must provide a Patient ID."

    saved_searches: "Saved searches"
    saved_searches_delete: "Delete"
    saved_searches_criteria: "Saved search criteria"
    save_changes: "Save"
    save_changes_cancel: "Cancel"
    view_document_go: "Go"
    
    previous_page: "Previous"
    next_page: "Next"
    first_page: "First"
    last_page: "Last"
    
    # Search labels
    
    search_label_saved_search: "From saved search"
    button_label_save_and_search: "Save search"
    documents_saved_search_instruction: "Name and save your search"
    documents_saved_search_name: "Name"
    no_saved_searches: "You have no saved searches"
    failed_to_retrieve_saved_searches: "Failed to retrieve saved searches"
    
    search_basic: "Basic"
    search_advanced: "Advanced search"
    advanced_search: "Advanced search"

    show_advanced_search: "Show advanced"
    show_basic_search: "Show basic"

    search_criteria_timerange_display: "%{searchParameter} between: <strong>%{notBefore}</strong> and <strong>%{notAfter}</strong>"
    search_criteria_notAfter_display: "%{searchParameter} before: <strong>%{notAfter}</strong>"
    search_criteria_notBefore_display: "%{searchParameter} after: <strong>%{notBefore}</strong>"
    search_criteria_notAfter: "To this date"
    search_criteria_notBefore: "From this date"

    # ---------- ---------- ---------- ---------- ----------
    # General error messages
    # ---------- ---------- ---------- ---------- ----------

    error_details: "Error details"
    
    go_to_previous_page: "Go back to the previous page."
    product_info_unavailable: "Could not access product information."
    navigator_server_unavailable: "VioArchive is unavailable at this time. Try again later."
    required_field_is_missing: "Some required fields are missing."
    invalid_confidentiality_code: "Invalid confidentiality code."
    invalid_configuration: "Please review your configuration. Either some required field are missing, or they contain invalid values."

    permission_missing: "You do not have permission to view %{page}."
    functionality_permission_missing: "You do not have permissions to perform %{action}."
    upload_permission_missing: "You do not have permission to upload documents."
    move_permission_missing: "You do not have permission to move studies."
    change_availability_permission_missing: "You do not have permission to change study availability."

    flash_notice_cannot_save_search: "Cannot save this search at this time."
    flash_notice_name_search: "Your saved search needs a name, and the name must contain only letters, numbers, hyphens '-', underscores '_', at symbols '@', periods '.', or spaces."

    unauthorized_msg_1: "You do not seem to be authenticated."
    unauthorized_msg_2: "Contact your administrator to determine what is wrong."
    unauthorized_function_msg_1: "You do not have access permission."
 
    internal_server_error_1: "We're sorry, but something went wrong."
    internal_server_error_2: "We have been notified about this issue."
    
    bad_request_1: "We're sorry, but that page does not exist."
    bad_request_2: "Use the browser back button and try another link, or select another link on this page."
    
    not_found_1: "The page you were looking for does not exist."
    not_found_2: "The address may have been mistyped or the page may have moved."

    session_expired: "Your session has expired. Sign in again."

    patients_search_domain_missing: "Please select a domain to search within."
    patients_search_too_many_results: "Your search for patients resulted in more results than can be returned. Try a different search."

    # ---------- ---------- ---------- ---------- ----------
    # Web Launcher Error Messages
    # ---------- ---------- ---------- ---------- ----------
    launcher_error_session_id_missing: "You must provide a valid token in order to verify authentication"
    launcher_error_session_id_invalid: "The authentication token provided was either invalid or expired"
    launcher_error_user_details_missing: "You must provide sufficient user details (id and realm), which match your provided authentication token"
    launcher_error_patient_context_missing: "You must provide a valid PatientID and DomainId to launch that patient's context"
    launcher_error_document_permissions_missing: "You do not have adequate permissions to view related patient data in this domain"

    # ---------- ---------- ---------- ---------- ----------
    # Patients
    # ---------- ---------- ---------- ---------- ----------

    patient_search: "Patient search"
    
    title_screen_patients: "Patients"

    list_patients: "List of patients"
    list_patient: "List of patients"   #Used by table preferences
    list_patients_last_search_time: "Last search time"
    
    missing_patient: "Missing patient"
    patient_label: "Patient"
    primary_domain_patient: "Current patient"
    patient_detail_tab: "Patient details"
    document_tab: "Documents"

    patient_search_criteria_birthDate: "Birth date"
    patient_search_criteria_domainID: "Domain"
    patient_search_criteria_familyName: "Family name"
    patient_search_criteria_givenName: "Given name"
    patient_search_criteria_healthCardNumber: "HCN"
    patient_search_criteria_patientID: "MRN"
    patient_search_criteria_regionalID: "Regional ID"

    patient_list_column_patientName: "Patient name"
    patient_list_column_birthDate: "Birth date"
    patient_list_column_domainName: "Domain"
    patient_list_column_regionalID: "Regional ID"
    patient_list_column_phoneNumber: "Phone number"
    patient_list_column_gender: "Gender"
    patient_list_column_patientID: "MRN"
    patient_list_column_healthCardNumber: "Health card number"
    patient_list_column_address: "Address"

    # - - - - - - - - - -
    # Patients - attribute labels
    
    # Identifier
    patients_attribute_section_patientIdentifiers: "Identifier"
    patients_attribute_label_local_patient_identifier: "Local patient identifier"
    patients_attribute_label_health_card_identifier: "Health card info"
    patients_attribute_label_patientIdentifiers_pid: "MRN"   # Shown in patients list
    
    patients_attribute_label_patientIdentifiers_domainUUID: "Universal ID"   # Shown in patients list
    patients_attribute_label_patientIdentifiers_domainUUIDType: "Universal ID type"
    patients_attribute_label_patientIdentifiers_regionalID: "Regional patient ID"

    patients_attribute_label_patientIdentifiers_domainUUIDType_organization: "Domain"
    patients_attribute_label_patientIdentifiers_domainUUIDType_organization_select: "-- Select a domain --" #used when editing

    patients_attribute_label_healthcard_patientIdentifiers_pid: "Health card number"   # Shown in patients list
    patients_attribute_label_healthcard_provinceOrTerritory: "Province/Territory"   # Shown in patients list
    patients_attribute_label_healthcard_provinceOrTerritory_select: "-- Select a province/territory --"   # Shown in patients list

    # Patient name
    patients_attribute_label_name: "Patient name" # Uses in subheads
    patients_attribute_section_patientName: "Patient name"
    patients_attribute_label_name_degree: "Degree"
    patients_attribute_label_name_namePrefix: "Name prefix"
    patients_attribute_label_name_givenName: "Given name"
    patients_attribute_label_name_familyName: "Family name"
    patients_attribute_label_name_otherName: "Other name"
    patients_attribute_label_name_nameSuffix: "Name suffix"
    patients_name_unknown: "Unknown"

    # Other
    patients_attribute_section_other: "Other"
    patients_attribute_label_birthDate: "Birth date"   # Shown in patients list
    patients_attribute_label_birthDate_format: " (yyyy-mm-dd)"   # Shown in patients list
    patients_attribute_label_gender: "Gender"   # Shown in patients list
    patients_attribute_label_privacy: "Privacy protection indicator"
    
    patients_attribute_label_gender_male: "Male"
    patients_attribute_label_gender_female: "Female"
    patients_attribute_label_gender_unknown: "Unknown"

    patients_attribute_label_privacy_enabled: "Enabled"
    patients_attribute_label_privacy_disabled: "Disabled"

    # Mother's maiden name
    patients_attribute_section_mothersMaidenName: "Mother's maiden name"
    patients_attribute_label_mothersMaidenName_degree: "Degree"
    patients_attribute_label_mothersMaidenName_namePrefix: "Name prefix"
    patients_attribute_label_mothersMaidenName_givenName: "Given name"
    patients_attribute_label_mothersMaidenName_familyName: "Family name"
    patients_attribute_label_mothersMaidenName_otherName: "Other name"
    patients_attribute_label_mothersMaidenName_nameSuffix: "Name suffix"

    # Address
    patients_attribute_section_address: "Address"
    patients_attribute_label_address_streetAddress: "Street"   # Shown in patients list
    patients_attribute_label_address_city: "City"
    patients_attribute_label_address_countyParishCode: "County/Parish"
    patients_attribute_label_address_otherGeographicDesignation: "Other geographic designation"
    patients_attribute_label_address_stateOrProvince: "Province/State"
    patients_attribute_label_address_zipOrPostalCode: "Postal/ZIP code"
    patients_attribute_label_address_country: "Country"

    # Phone
    patients_attribute_section_phone: "Phone"
    patients_attribute_label_homePhoneNumber: "Phone (home)"   # Shown in patients list
    patients_attribute_label_businessPhoneNumber: "Phone (business)"

    # Other institutions (when used with EMPI - Discover)
    patients_attribute_label_otherInstitutions: "Other institutions"
    patients_attribute_label_otherInstitutions_name: "Institution"
    
    patients_attribute_label_action: "Action"

    # - - - - - - - - - -
    # Search Menu for patients list
    
    patients_label_search_menu: "Search patients…"
    patients_button_label_search: "Search"
    patients_search_menu_label_givenName: "Given name"
    patients_search_menu_label_familyName: "Family name"
    patients_search_menu_label_patientID: "MRN"
    patients_search_menu_label_healthCardNumber: "HCN"
    patients_search_menu_label_regionalID: "Regional ID"
    patients_search_menu_label_birthDate: "Birth date"
    patients_search_menu_label_domain: "Domain"
    patients_search_default_label: "--Select--"
    
    # ---------- ---------- ---------- ---------- ----------
    # Documents
    # ---------- ---------- ---------- ---------- ----------

    title_list_documents: "List of documents"
    title_document_details: "Document details"
    title_move_document: "Move document"
    title_upload_document: "Upload document"
    list_doc: "List of documents" # Used by table preferences
    missing_document: "Missing document" # Not used? Remove?
    document_label: "Document"
    
    document_message_patient_not_found: "No patient record could be found for the patient ID. Try again."
    documents_progress_indicator: "Retrieving documents…"

    # - - - - - - - - - -
    # Documents - search
    
    doc_search_criteria_accessionNumber: "Accession number"
    doc_search_criteria_availabilityStatus: "Availability status"
    doc_search_criteria_creationTime: "Creation time"
    doc_search_criteria_classCode: "Class code"
    doc_search_criteria_eventCodes: "Event codes"
    doc_search_criteria_formatCode: "Format code"

    documents_search_label_in: "in"

    documents_search_extendedMetadataKey: "Other attribute (name)"
    documents_search_select_extendedMetadataKey: "Select an attribute"
    documents_search_extendedMetadataValue: "Other attribute (value)"
    documents_search_menu_label_creation_time: "Creation time"
    documents_search_menu_time_last_day: "Last 24 hours"
    documents_search_menu_time_last_week: "Last 7 days"
    documents_search_menu_time_last_2_week: "Last 14 days"
    documents_search_menu_time_last_month: "Last 30 days"
    documents_search_menu_time_other: "Other…"
    any: "Any"
    documents_search_menu_time_not_before: "From this date"
    documents_search_menu_time_not_after: "To this date"
    
    # - - - - - - - - - -
    # Documents - List column heads
    
    doc_list_column_extendedMetadata_accessionNumber: "Accession number"
    doc_list_column_availabilityStatus: "Availability status"
    doc_list_column_confidentialityCode: "Confidentiality code"
    doc_list_column_sourceOrganization: "Source domain"
    doc_list_column_author: "Author"
    doc_list_column_classCodeDisplayName: "Class code"
    doc_list_column_creationTime: "Creation time"
    doc_list_column_extendedMetadata_referringPhysician: "Referring physician"
    doc_list_column_title: "Title"
    
    # - - - - - - - - - -
    # Documents - attribute labels

    documents_attribute_label_title: "Title"   # Shown in patients list

    # Document status labels
    documents_attribute_availability_deprecated: "Deprecated"
    documents_attribute_availability_approved: "Approved"
    
    # Document
    documents_attribute_section_document: "Document"
    documents_attribute_section_document_codes: "Document codes"

    # Patient (use patient references instead?)
    documents_attribute_label_patient: "Patient (affinity)"
    documents_attribute_label_patient_patientId_domainUUID: "Universal ID"
    documents_attribute_label_patient_patientId_domainUUIDType: "Universal ID type"
    documents_attribute_label_patient_patientId_namespaceID: "Name space ID"
    documents_attribute_label_patient_patientId_pid: "Regional patient ID"    # This is the regional ID for the patient
    
    # Patient source (use patient references instead?)
    documents_attribute_label_patient_sourcePatientID: "Patient source (local)"
    documents_attribute_label_patient_sourcePatientID_domainUUID: "Universal ID"
    documents_attribute_label_patient_sourcePatientID_domainUUIDType: "Universal ID type"
    documents_attribute_label_patient_sourcePatientID_namespaceID: "Name space ID"
    documents_attribute_label_patient_sourcePatientID_pid: "MRN"

    # NOTE: Patient attributes shown in document details page use patient labels

    # Author
    documents_attribute_section_author: "Author"
    documents_attribute_label_author_institution: "Author institution"
    documents_attribute_label_author_person_degree: "Degree"

    documents_attribute_label_author_person_fullName: "Author"   # Shown in patients list

    documents_attribute_label_author_person_familyName: "Family name"
    documents_attribute_label_author_person_givenName: "Given name"
    documents_attribute_label_author_person_otherName: "Other name"
    documents_attribute_label_author_person_namePrefix: "Name prefix"
    documents_attribute_label_author_person_nameSuffix: "Name suffix"
    documents_attribute_label_author_author_role: "Author role"
    documents_attribute_label_author_author_specialty: "Author specialty"

    # Legal Authenticator
    documents_attribute_section_legalAuthenticator: "Legal authenticator"
    documents_attribute_label_legalAuthenticator_person_familyName: "Family name"
    documents_attribute_label_legalAuthenticator_person_givenName: "Given name"
    documents_attribute_label_legalAuthenticator_person_otherName: "Other name"
    documents_attribute_label_legalAuthenticator_person_namePrefix: "Name prefix"
    documents_attribute_label_legalAuthenticator_person_nameSuffix: "Name suffix"
    documents_attribute_label_legalAuthenticator_person_degree: "Degree"

    # NOTE: Use generic name labels here

    # Extended Metadata
    documents_attribute_section_extendedMetadata: "Extended metadata"
    documents_attribute_label_other: "Other"
    documents_attribute_label_AccessionNumber: "Accession #" # Why two?
    documents_attribute_label_patient_accessionNumber: "Accession #"   # Shown in patients list
    documents_attribute_label_study_instance_uid: "Study instance UID"
    documents_attribute_label_patient_studyDescription: "Study description"
    documents_attribute_label_series: "Series description"
    documents_attribute_label_number_images: "Number study related images"
    documents_attribute_label_number_instances: "Number study related instances"
    documents_attribute_label_patient_studyID: "Study ID"
    documents_attribute_label_ReferringPhysician: "Referring physician"
    documents_attribute_label_repositoryUniqueID: "Repository unique ID"
    documents_attribute_label_homeCommunityID: "Home community ID"
    documents_attribute_label_serviceStopTime: "Service stop time"
    documents_attribute_label_serviceStartTime: "Service start time"
    
    # Other
    documents_attribute_section_other: "Other"
    documents_attribute_label_patient_typeCodeDisplayName: "Type code"
    documents_attribute_label_short_availability_status: "Status"   # Shown in patients list
    documents_attribute_label_classCodeDisplayName: "Class code"   # Shown in patients list
    documents_attribute_label_confidentialityCode: "Confidentiality code"   # Shown in patients list
    documents_attribute_label_creationTime: "Created"   # Shown in patients list
    documents_attribute_label_format: "Format code"
    documents_attribute_label_hash: "Hash"
    documents_attribute_label_home_community_id: "Home community ID"
    documents_attribute_label_language_code: "Language code"
    documents_attribute_label_mime_type: "Mime type" # Why two of these?
    documents_attribute_label_mimetype: "Mime type"
    documents_attribute_label_repository_unique_id: "Repository unique ID"
    documents_attribute_label_size: "Document size"
    documents_attribute_label_comments: "Comments"
    documents_attribute_label_eventCodeList: "Event codes"
    documents_attribute_label_healthcareFacilityTypeCodeDisplayName: "Health care facility type code"
    documents_attribute_label_patient_practiceSettingCodeDisplayName: "Practice setting code"
    documents_attribute_label_patient_serviceStartTime: "Service start time"
    documents_attribute_label_patient_serviceStopTime: "Service stop time"
    


    # --- What are these for?
    documents_attribute_label_patient_sourcePatientInfo: "Source patient info"
    documents_attribute_label_patient_referringPhysician: "Referring physician"
    documents_attribute_label_patient_entryUUID: "Entry UUID"

    documents_attribute_label_sourceOrganization: "Source domain"

    
    documents_attribute_label_isRetrievable: "Retrievability"
       
    documents_attribute_value_type_manifest: "Manifest"
    documents_attribute_value_type_report: "Report"
    
    # - - - - - - - - - -
    # Search Menu for documents list
    
    documents_button_label_search_menu: "Search patients…"
    documents_button_label_search: "Search"
    
    documents_upload_file_button: "Upload document file…"

    # - - - - - - - - - -
    # Line item menu options in Documents list
    
    documents_item_show_view: "View…"
    documents_item_show_report: "Document"
    documents_item_show_webviewer: "Images (web)"
    documents_item_show_desktopviewer: "Images (desktop)"
    documents_item_show_metadata: "Metadata"
    documents_item_import_study: "Import to PACS"

    # - - - - - - - - - -
    # "Change Confidentiality Code" Workflow

    documents_button_label_change_confidentiality: "Change confidentiality code…"
    documents_button_label_set_confidentiality: "Set confidentiality code…"

    documents_message_change_confidentiality: "Enter a new confidentiality code for this document."
    documents_message_change_confidentiality_success: "The confidentiality code has been changed to %{new_code}."
    documents_message_change_confidentiality_failure: "Cannot change the confidentiality code at this time."
    documents_message_change_confidentiality_failure_ajax: "Cannot change the confidentiality code at this time."

    retrievability_always: "Always"
    retrievability_never: "Never"
    retrievability_once: "Once"
    retrievability_on_break_glass: "On break glass"
    
    # - - - - - - - - - -
    # "Change Status" (deprecate/approve) Workflow

    documents_button_label_change_status_deprecate: "Deprecate"
    documents_button_label_change_status_approve: "Approve"
    documents_button_label_change_status_deprecating: "Deprecating"


    documents_message_change_status_deprecate: "Are you sure you want to deprecate this document?"
    documents_message_change_status_approve: "Are you sure you want to approve this document?"
    documents_message_change_status_success: "The document '%{title}' has been %{new_status}."
    documents_message_change_status_failure: "The document '%{title}' cannot be %{new_status} at this time."
    documents_message_change_status_failure_ajax: "The document availability cannot be changed at this time."
 
    # - - - - - - - - - -
    # "Break Glass" Workflow

    break_glass_message_one: "You do not have permission to view this document under normal conditions."
    break_glass_message_two: 'To view this document, provide a reason why you need to do so, and then "break the glass".'
    break_glass_message_three: "Note that an audit record will be created that records your access to the document."
    break_glass_fieldset_reasons: "Reason for accessing document"
    break_the_glass_notes: "Additional notes"
    
    break_glass_reason_no_reason: "No reason"
    break_glass_reason_other: "Other"
    break_glass_reason_other_details: "Other reason"

    break_glass_button: "Break the glass"
    break_glass_no_reason_provided: "You must provide a reason in order to break the glass to view a document."

    flash_notice_break_glass_failure: "Cannot break the glass at this time. Try again later."
    flash_notice_non_retrievable_doc: "You do not have permission to access this document. Contact your administrator."

    flash_notice_select_domain: "Select a domain for this search."
    flash_notice_no_patient_found: "No patient record was found for the MRN provided."

    # - - - - - - - - - -
    # "Upload document files" Workflow
    
    document_upload_label_folder: "Folder name"
    document_upload_label_remove: "Remove"
    document_upload_drop_target_instruct: "Drop one or more document files anywhere in this window to upload them."
    document_upload_drop_target_instruct_2: "or"
    
    document_upload_button_select_files: "Select document files &#133;"
    document_upload_button_upload_all: "Upload all"
    document_upload_button_cancel_all: "Cancel all"
    document_upload_button_upload: "Start upload"
    document_upload_button_cancel: "Cancel"
    document_upload_add_extended_metadata: "Add extended metadata"
    document_upload_add_event_code: "Add event code"
    
    document_upload_folder_name_prefix: "Episode of care:"
    
    document_upload_instruct_choose: "Choose a document file to upload"
    document_upload_instruct_provide: "Provide some document metadata"
    
    document_upload_attribute_document_title: "Document title"

    document_upload_file_missing: "You did not select a document file to upload"
    
    document_upload_status_error: "The document file could not be uploaded"
    document_upload_status_complete: "Upload complete"
    document_upload_status_in_progress: "Uploading"
    
    document_upload_label_file: "Document file"
    document_upload_label_size: "Size"
    document_upload_language_en: "English"
    document_upload_language_fr: "French"

    document_upload_xds_code_classCode: "Class code"
    document_upload_xds_code_contentTypeCode: "Content type code"
    document_upload_xds_code_formatCode: "Format code"
    document_upload_xds_code_typeCode: "Type code"
    document_upload_xds_code_practiceSettingCode: "Practice setting code"
    document_upload_xds_code_healthcareFacilityTypeCode: "Healthcare facility type code"
    document_upload_xds_code_confidentialityCode: "Confidentiality code"
    document_upload_code_eventCodes: "Event codes"
    document_upload_xds_code_none: "There are no supported XDS document codes configured"
    document_upload_confidentialityCode_none: "There is no supported confidentiality code configured"

    # - - - - - - - - - -
    # "Move document" Workflow

    documents_button_label_move_document: "Move..."
    documents_message_move_from: "From this patient:"
    documents_message_move_to: "To this patient:"
    documents_message_prompt_id: "Enter a patient ID for the patient record to which this document will be moved."
    documents_message_patient_not_found: "No patient record could be found for the patient ID. Try again."
    documents_message_same_patient: "This is the same patient. Cannot move document."
    documents_error_patient_not_found: "Unable to retrieve the new patient at this time - failed to move document."

    documents_confirm_move: "Are you sure you want to move the document <strong>%{doc_title}</strong> from <strong>%{patient_from}</strong> to <strong>%{patient_to}</strong>? Doing so will permanently move the document. Move the document?"

    documents_button_label_move: "Move"
    
    #---------------------------------------------------------------
    # Theme Labels
    #---------------------------------------------------------------

    navigator_theme: "Theme"

    #---------------------------------------------------------------
    # JS Strings
    #---------------------------------------------------------------

    search:
        label:
            search: "Search"
            search_saved: "Saved searches"
            searchedFor: "Searched for"
            searchedAll: "Searched for all"
            searchedNone: "Search results"
            refresh: "Refresh"
            
            link_create_saved_search: "Add a saved search"
            link_create_saved_search_cancel: "Cancel"
            link_document_view: "View"

            event_codes_dictionary: "Dictionary"
            
    messages:
        confirm:  
            deleteConfidentialityCode: 'Are you sure you want to delete this confidentiality code? Doing so cannot be undone. Delete this code?' 
            deleteGroup: 'Are you sure you want to delete this group? Doing so cannot be undone. Delete this group?'
            deleteRealm: 'Are you sure you want to delete this realm? Doing so will delete related users and groups cannot be undone. Delete this realm?' 

    dataTable: 
        sProcessing: "Processing&#133;"
        sLengthMenu: "Show _MENU_ entries"
        sZeroRecords: "No matching records found"
        sInfo: "Showing _START_ to _END_ of _TOTAL_ entries"
        sInfoEmpty: "Showing 0 to 0 of 0 entries"
        sInfoFiltered: "(filtered from _MAX_ total entries)"
        sInfoPostFix:  ""
        sSearch: "Search:"
        sUrl: ""
        oPaginate: 
            sFirst:  "First"
            sPrevious: "Previous"
            sNext: "Next"
            sLast: "Last"

    general_clear_all: "Clear all"
    general_select_all: "Select all"
            
    #--------------------------------------------------------------- 

    no_search_performed: "No search performed"
    no_matches_message: "No matching records found"
    patient_list_truncated: "The maximum number of patients has been returned. Consider constraining your search criteria to reduce the number of patients found."
    document_list_truncated: "The maximum number of documents has been returned. Consider constraining your search criteria to reduce the number of documents found."
