log.debug("cfindrequest.groovy START")

def pid=get(PatientID)
log.debug("cfindrequest: PatientID is ${pid}")

if (pid == null)
    return

if (pid.indexOf('^') < 0) {
    set(IssuerOfPatientID, "RG")
    set("IssuerOfPatientIDQualifiersSequence/UniversalEntityID", "2.16.124.113638.7.4.1.1")
    set("IssuerOfPatientIDQualifiersSequence/UniversalEntityIDType", "ISO")
}


log.debug("cfindrequest.groovy END")
