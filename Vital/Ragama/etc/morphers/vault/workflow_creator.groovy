import org.dcm4che2.data.DicomObject
import org.dcm4che2.data.Tag.*;

import com.karos.rialto.workflow.model.GenericWorkflow
import com.karos.rialto.imagearchive.tasks.InitialSRToORUTask
import com.karos.rialto.imagearchive.tasks.ConvertSRToORUTask
import com.karos.rialto.workflow.events.imagearchive.convertsr.SRToORUMessageEvent
import com.karos.rialto.workflow.common.tasks.SendHL7Task
import com.karos.rialto.workflow.model.Task.Status

if (candidates.isEmpty()) {
    return null
}

// Initial Task to hold each of the candidates, as separate events.
def initialSRToORUTaskBuilder = new InitialSRToORUTask.Builder();

for (x in candidates) {
    def evt = new SRToORUMessageEvent(initialSRToORUTaskBuilder.getId(),1)
    evt.setStudyInstanceUID(x.get(StudyInstanceUID))
    evt.setSeriesInstanceUID(x.get(SeriesInstanceUID))
    evt.setSopUID(x.get(SOPInstanceUID))
    evt.setCalledAET(getCalledAETitle())
    evt.setCallingAET(getCallingAETitle())

    initialSRToORUTaskBuilder.addEvents(evt)
}
myCandidate = candidates[0]
def accessionNumber = myCandidate.get(AccessionNumber)
def patientID = myCandidate.get(PatientID)
def patientName = myCandidate.get(PatientName)

// Set the inital task to completed
initialSRToORUTaskBuilder.setStatus(Status.SUCCESS)

//This is the task that will convert the SRs to ORUs.
def srToORUTaskBuilder = new ConvertSRToORUTask.Builder();
//srToORUTaskBuilder.setMorpherScriptName("${rialto.rootdir}/etc/morphers/vault/sr_to_oru_morpher.groovy")
srToORUTaskBuilder.setMorpherScriptName("/home/rialto/rialto/etc/morphers/vault/sr_to_oru_morpher.groovy")

// This will send the HL7 messages.
def sendHL7TaskBuilder = new SendHL7Task.Builder();
//sendHL7TaskBuilder.setDestinationHost("172.16.120.61")
//sendHL7TaskBuilder.setDestinationPort(9912)
sendHL7TaskBuilder.setDestinationHost("10.10.90.5")
sendHL7TaskBuilder.setDestinationPort(5752)


// setup task dependencies.
srToORUTaskBuilder.addDataDependency(initialSRToORUTaskBuilder,Status.SUCCESS)
sendHL7TaskBuilder.addDataDependency(srToORUTaskBuilder,Status.SUCCESS)


def wfb = new GenericWorkflow.Builder("ConvertandSendSRToORUWorkflow")
wfb.setAccessionNumber(accessionNumber)
wfb.setPatientId(patientID)
wfb.setPatientName(patientName)
wfb.addTaskBuilder(initialSRToORUTaskBuilder)
wfb.addTaskBuilder(srToORUTaskBuilder)
wfb.addTaskBuilder(sendHL7TaskBuilder)

return wfb.build()
