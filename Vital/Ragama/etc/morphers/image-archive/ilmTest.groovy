log.info("starting ilmTest.groovy")

log.info("ilmToTape Study.StudyInstanceUID={}", study.getStudyInstanceUID())
log.info("ilmToTape Study.PatientName={}", study.get(0x00100010))
log.info("ilmToTape Study.PatientID={}", study.get(0x00100020))
log.info("ilmToTape Study.Accession={}", study.get(0x00080050))
log.info("ilmToTape Study.CompletionFlag={}", study.get(0x0040A491))
log.info("ilmToTape Study.VerificationFlag={}", study.get(0x0040A493))

log.info("ending ilmTest.groovy")
