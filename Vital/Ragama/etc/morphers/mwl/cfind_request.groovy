// empty groovy morpher
import java.text.SimpleDateFormat

log.debug("MWL CFind Request Morpher - input:{}\n", input)

callingAE = getCallingAETitle()
df = new SimpleDateFormat("yyyyMMdd")
today = df.format(new Date())

log.info("Current date: {}", today)
log.info("Calling AE: {}", getCallingAETitle())

if (callingAE == "TM_CT_CMW_V300"){
    if (get("ScheduledProcedureStepSequence/Modality") == null){
        set("ScheduledProcedureStepSequence/Modality", "CT")
    }
    if (get("ScheduledProcedureStepSequence/ScheduledProcedureStepStartDate") == null){
        set("ScheduledProcedureStepSequence/ScheduledProcedureStepStartDate", today)
    }
}


log.debug("MWL CFind Request Morpher - output:{}\n", output)
log.info("MWL CFind Request END")
