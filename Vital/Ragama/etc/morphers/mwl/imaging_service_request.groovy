log.info("MWL ISR Morpher - The Order Message:]\n{}", input)
log.info("MWL ISR Morhper - The initial Imaging Service Request:\n{}", imagingServiceRequest)

if (imagingServiceRequest.getAccessionNumberUniversalId() == null) {
    imagingServiceRequest.setAccessionNumberNamespaceId("RG")
    imagingServiceRequest.setAccessionNumberUniversalId("2.16.124.113638.7.4.1.1")
    imagingServiceRequest.setAccessionNumberUniversalIdType("ISO")
}

log.debug("MWL ISR Morpher - about to set PatientID to {}", get("PID-3-1"))

def patientIdentification = imagingServiceRequest.getPatientIdentification()
patientIdentification.setPatientID(get("PID-3-1"))
if (patientIdentification.getPatientIdUniversalId() == null) {
    patientIdentification.setPatientIdNamespaceId("RG")
    patientIdentification.setPatientIdUniversalId("2.16.124.113638.7.4.1.1")
    patientIdentification.setPatientIdUniversalIdType("ISO")
}

imagingServiceRequest.setPatientIdentification(patientIdentification)

def patientDemographics = imagingServiceRequest.getPatientDemographics()
patientDemographics.setPatientBirthDate(get("PID-7"))
imagingServiceRequest.setPatientDemographics(patientDemographics)

def requestedProcedure = imagingServiceRequest.getRequestedProcedureSequence().get(0)

imagingServiceRequest.setAccessionNumber(requestedProcedure.getRequestedProcedureID())
//requestedProcedure.setRequestedProcedureID(imagingServiceRequest.getAccessionNumber())
//requestedProcedure.setRequestedProcedureDescription(get("OBR-5"))
//requestedProcedure.setStudyInstanceUID(get("ZDS-1"))

def scheduledProcedureStep = requestedProcedure.getScheduledProcedureStepSequence().get(0)
//scheduledProcedureStep.setScheduledProcedureStepIDString(imagingServiceRequest.getAccessionNumber())

if (scheduledProcedureStep.getScheduledProcedureStepLocation() == null) {
    log.debug("MWL ISR Morpher - about to set Scheduled Procedure Step Location to UNKNOWN");
    scheduledProcedureStep.setScheduledProcedureStepLocation("UNKNOWN");
}

if (scheduledProcedureStep.getScheduledStationName() == null) {
    log.debug("MWL ISR Morpher - about to set Scheduled Station Name to UNKNOWN");
    scheduledProcedureStep.setScheduledStationName("UNKNOWN");
}

if (scheduledProcedureStep.getModality() == null) {
    log.debug("MWL ISR Morpher - about to set Modality to UNKNOWN");
    scheduledProcedureStep.setModality("UNKNOWN");
}


def scheduledProcedureStepStatus = scheduledProcedureStep.getScheduledProcedureStepStatus()
log.debug("MWL ISR Morpher - ScheduledProcedureStepStatus = {}", scheduledProcedureStepStatus)

def orderControlCodeReason = get("ORC-1")
if (orderControlCodeReason != null) {
    switch (orderControlCodeReason) {
        case 'DC':
            log.debug("MWL ISR Morpher - about to set Scheduled Procedure Step Status to DISCONTINUED")
            scheduledProcedureStep.setScheduledProcedureStepStatus('DISCONTINUED')
            break
        case 'XO':
            log.debug("MWL ISR Morpher - about to set Scheduled Procedure Step Status to SCHEDULED from code XO")
            scheduledProcedureStep.setScheduledProcedureStepStatus('SCHEDULED')
            break
        case 'SC':
            log.debug("MWL ISR Morpher - about to set Scheduled Procedure Step Status to SCHEDULED from code SC")
            scheduledProcedureStep.setScheduledProcedureStepStatus('SCHEDULED')
            break
        case 'SC(IP)':
            log.debug("MWL ISR Morpher - about to set Scheduled Procedure Step Status to INPROGRESS")
            scheduledProcedureStep.setScheduledProcedureStepStatus('INPROGRESS')
            break
        case 'SC(CM)':
            log.debug("MWL ISR Morpher - about to set Scheduled Procedure Step Status to COMPLETED")
            scheduledProcedureStep.setScheduledProcedureStepStatus('COMPLETED')
            break
        default:
            log.debug("MWL ISR Morhper - about to set Scheduled Procedure Step Status to UNKNOWN")
            scheduledProcedureStep.setScheduledProcedureStepStatus('UNKNOWN')
    }
}

//scheduledProcedureStep.setScheduledProcedureStepStatus(get("OBR-1"))

//scheduledProcedureStep.setScheduledProcedureStepLocation(get("OBR-2"))
//scheduledProcedureStep.setScheduledStationName(get("OBR-4"))

//scheduledProcedureStep.setModality(get("OBR-3"))
//if (scheduledProcedureStep.getModality() == null) {
//    scheduledProcedureStep.setModality("CT");
//}

log.info("Finished fixing up the Imaging Service Request: \n{}", imagingServiceRequest)

log.debug("imaging_service_request END")
