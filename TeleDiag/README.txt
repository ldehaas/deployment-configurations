2020-03-04 Troels:

At state 87d63d3ab1aec8efa1bb6f2a4489a30e98dd84ed:
Moved to https://bitbucket.org/karoshealth/tld/src/master/prod/rialto/
This was done, so that it's possible to have a git clone directly on the TeleDiag server(s), and because the Deployment Configurations repository has become so large that it's cumbersome to work with (and Bitbucket warns about its size).

When moving the files, the most recent change to them had happened in commit 0c44a1c07e3f057fc2f0e3e8fe54cc6238474308. And at the time of the move, this directory looked like this:

Name           Last commit Message
-------------- ----------- --------------------------------------------------------------------------------------------------
01-tlg-vc-app1  2019‑04‑25 TeleDiag fixed teststudy configuration. Former-commit-id: 8787ea0588cf2de5d5d0ec5fdb065884a5239a73
02-tlg-vc-app2  2019‑04‑25 TeleDiag fixed teststudy configuration. Former-commit-id: 8787ea0588cf2de5d5d0ec5fdb065884a5239a73
