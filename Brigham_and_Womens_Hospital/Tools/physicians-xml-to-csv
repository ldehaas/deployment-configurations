#!/usr/bin/perl

#
# Physician Names - XML to CSV Format
#
# Read XML format and translate to CSV format
# Useful for merging large lists of new names with old.
#
# Written by: John E. Lammers, March 2013
# Copyright (c) 2013 Karos Health Inc., Waterloo Ontario Canada
#

# Remove XML tags and leading/trailing whitespace
sub trim($)
{
    my $string = shift;
    $string =~ s/^.*<[a-zA-Z]*>\s*//; # Remove opening XML tag
    $string =~ s/\s*<\/[a-zA-Z]*>//; # Remove closing XML tag
    $string =~ s/\s\s/ /g; # Remove double spaces
    $string =~ s/,\s+/,/; # Remove space after comma
    return $string;
}

$arguments = @ARGV;
if ($arguments != 0) {
    print "Usage: $0 <physicians-XX.xml >out.csv\n";
    exit 1;
}

print "#FamilyName,#GivenNames,#Title,#ID,#Site\n";

while (<STDIN>) {

    chomp;

    if (/<FamilyName>/) {
        $last = trim($_);
    }
    if (/<GivenName>/) {
        $given = trim($_);
        $commas = $given =~ tr/,//;
        if ($commas != 1) {
            die "Given name must contain one comma: $given";
        }
        if ($given =~ / [A-Z],/) {
            die "Given name initial does not have period: $given";
        }
    }
    if (/<ID>/) {
        $id = trim($_);
        if ($id =~ /[^\d]/) {
            die "ID contains non-numeric character: $id";
        }
    }
    if (/<Site>/) {
        $site = trim($_);

        print "$last,$given,$id,$site\n";
    }
}
