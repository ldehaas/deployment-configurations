//
// DICOM Image Morphing script for Brigham & Womens (Consult-Filler)
//
// This script modifies all images received from Placer sites.
//
// It localizes the patient id and accession number by prefixing
// them with the two-letter prefix code (set by the morphing script
// at the placer site.
//

def sourcePid = get(PatientID)
if (sourcePid == null) {
    throw new UnsupportedOperationException(
        'Cannot localize image with null patient id. ' +
        "Got: [${sourcePid}]")
}

def sourceIssuer = get(IssuerOfPatientID)
if (sourceIssuer == null) {
    throw new UnsupportedOperationException(
        'Cannot localize image with null issuer of patient id. ' +
        "Got: [${sourceIssuer}]")
}

def sourceAcc = get(AccessionNumber)
if (sourceAcc == null) {
    //
    // Before Feb 2005, studies at Anna Jaques do not have accession numbers
    // As a hack, use the last 16 characters of the Study Instance UID
    // This could happen with studies from other sites too ...
    //
    log.info('Accession number is null')
    def studyUID = get(StudyInstanceUID)
    def temp = studyUID.replaceAll(/\./, '')
    if (temp.length() >= 15) {
        sourceAcc = temp[-15..-1]
        log.info("Faking accession number using last digits of study uid: {}", sourceAcc)
    }
}
if (sourceAcc == null) {
    throw new UnsupportedOperationException(
        'Cannot localize image with null accession number. ' +
        "Got: [${sourceAcc}]")
}

// Prepend patient id with two-letter placer site prefix
// prepend(PatientID, sourceIssuer) // no longer supported
set(PatientID, sourceIssuer + sourcePid)

// Prepend accession number with two-letter placer site prefix
// prepend(AccessionNumber, sourceIssuer) // no longer supported
set(AccessionNumber, sourceIssuer + sourceAcc)

// KHC-766 Trying to get iSite timeline refresh feature to work
def now = new Date()
set(AcquisitionDate, now.format('yyyyMMdd'))
set(AcquisitionTime, now.format('HHmmss'))
set(AcquisitionDateTime, now.format('yyyyMMddHHmmss'))
// set(InstanceCreationDate, now.format('yyyyMMdd'))
// set(InstanceCreationTime, now.format('HHmmss'))
// set(InstanceCreationDateTime, now.format('yyyyMMddHHmmss'))
