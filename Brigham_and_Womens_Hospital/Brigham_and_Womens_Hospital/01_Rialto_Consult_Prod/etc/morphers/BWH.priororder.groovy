//
// Prior Order Generation for Brigham & Womens (Consult-Filler)
//
// This script generates an HL7 order from the first DICOM image in a study.
//
// The NightWatch PACS system cannot accept a DICOM study without first
// receiving an HL7 order.  In order to send relevant prior studies into
// the NightWatch PACS, we must first create an HL7 order for the prior
// and send it before sending the study.
//
// Written by: John E. Lammers, john.lammers@karoshealth.com
//

def accessionNumber = get(AccessionNumber)

def dateTime = String.format('%tY%<tm%<td%<tH%<tM%<tS', new Date())

def studyDateTime = get(StudyDate) + get(StudyTime).substring(0, 6)

// These mappings, descriptions and codes required by BWH
def modality = get(Modality)
def description = ''
def procedureCode = ''
switch (modality) {
    case 'CR':
        description = 'CR_Prior'
        procedureCode = 'CRPri'
        break

    case 'CT':
        description = 'CT_Prior'
        procedureCode = 'CTPri'
        break

    case 'MR':
    case 'MA':
    case 'MS':
        description = 'MR_Prior'
        procedureCode = 'MRPri'
        break

    case 'NM':
    case 'ST':
        description = 'NM_Prior'
        procedureCode = 'NMPri'
        break

    case 'US':
    case 'CD':
    case 'DD':
    case 'EC':
        description = 'US_Prior'
        procedureCode = 'USPri'
        break

    case 'DX':
    case 'XA':
        description = 'XR_Prior'
        procedureCode = 'XRPri'
        break

    default:
        description = 'OT_Prior'
        procedureCode = 'OTPri'
        break
}

def fillerSite = 'BWH'

def prefix = get(PatientID).substring(0, 2)

def organizationId = ''
switch (prefix) {
    // #### Alphabetical order ####
    case 'AJ':
        organizationId = '51455'
        break
    case 'CC':
        organizationId = '3527'
        break
    case 'DD':
        orgId = '52003'
        break
    case 'DN':
        orgId = '52002'
        break
    case 'FH':
        organizationId = '2804'
        break
    case 'GC':
        organizationId = '52001'
        break
    case 'HM':
        organizationId = '52005'
        break
    case 'HU':
        organizationId = '13491'
        break
    case 'NW':
        organizationId = '2800'
        break
    case 'QM':
        organizationId = '2893'
        break
    case 'SM':
        organizationId = '2812'
        break
    case 'SS':
        organizationId = '2822'
        break
    case 'VB':
        organizationId = '24768'
        break
    case 'VJ':
        organizationId = '52004'
        break
    case 'VT':
        organizationId = '24772'
        break
    case 'VW':
        organizationId = '24764'
        break
    // #### Alphabetical order ####
}



// ### Message Header ###
initialize('OMI', 'O23', '2.5')
set('MSH-3', 'RIALTO')          // Sending Application
set('MSH-4', prefix)            // Sending Facility
set('MSH-5', 'INTELLIGO')       // Receiving Application
set('MSH-6', fillerSite)        // Receiving Facility
set('MSH-7', dateTime)

// ### Patient Information Segment ###
set('PID-3', get(PatientID))    // External Patient ID
set('PID-3-5', 'PRIOR')         // Requested by NightWatch to identify priors
set('PID-3(1)-1', '1001000')    // What is this?
set('PID-3(1)-4', '524')        // What is this?
set('PID-3-4-1', '1')           // What is this?
setPersonName('PID-5', get(PatientName))        // Patient Name
set('PID-7', get(PatientBirthDate))             // Date/Time of Birth
set('PID-8', get(PatientSex))                   // Sex

// ### Order Common Information ###
set('ORC-1', 'NW')              // Order control, NW=New
set('ORC-2-1', accessionNumber) // Placer Order Number
set('ORC-2-2', organizationId)  // Placer Order Number
set('ORC-3-1', accessionNumber) // Filler Order Number
set('ORC-3-2', fillerSite)      // Filler Order Number
// Not present in objects surveyed from AJ:
// set('ORC-12-1', get('ReferringPhysicianIdentificationSequence[1]/ID'))

setPersonName('ORC-12', get(ReferringPhysicianName), startingPosition=2)  // Ordering Provider

// ### Order Information ###
set('OBR-1', '1')                       // What is this?
set('OBR-2-1', accessionNumber)         // Placer Order Number
set('OBR-2-2', organizationId)          // Placer Order Number
set('OBR-3-1', accessionNumber)         // Filler Order Number
set('OBR-3-2', fillerSite)              // Filler Order Number
set('OBR-4-1', procedureCode)           // Universal Service Identifier
set('OBR-4-5', description)             // Universal Service Identifier
set('OBR-6', studyDateTime)             // Requested Date/Time
set('OBR-7', studyDateTime)             // Observation Date/Time
set('OBR-13', get(StudyDescription))    // Relevant Clinical Info.
// Not present in objects surveyed from AJ:
// set('OBR-16-1', get('ReferringPhysicianIdentificationSequence[1]/ID'))

setPersonName('OBR-16', get(ReferringPhysicianName), startingPosition=2)    // Ordering Provider
// set('OBR-25', 'F')                   // Result Status
// set('OBR-31', get(StudyDescription)) // Reason for Study
setPersonName('OBR-34-1', get(OperatorsName), startingPosition=2)           // Technician

// ### Diagnosis Segment ###
// set('DG1-3', '32')                   // What does 32 mean?

// ### Imaging Procedure Control ###
set('IPC-1', accessionNumber)       // Accession Identifier
set('IPC-2', procedureCode)         // Requested Procedure ID
set('IPC-5-1', '1507')              // Modality
set('IPC-5-2', get(Modality))       // Modality

