// HL7 Result Morphing script (Groovy) for BWH

// Verify HL7 message version
def m = input.msg
if (!(m instanceof ca.uhn.hl7v2.model.v25.message.ORU_R01)) {
    log.error("Result morphing script cannot run against message of type {}^{} (v{})." +
        ' Only ORU^R01 (v2.5) is supported at this time.',
        [ get('MSH-9-1'), get('MSH-9-2'), get('MSH-12') ] as Object[])
    return
}

// Remove the two character placer site prefix code from the patient identifier
def pid = get('PID-3')
def prefix = pid.substring(0, 2)
set('PID-3', pid.substring(2, pid.length()))

// Remove the prefix from the accession number
def numOrders = m.getPATIENT_RESULT().getORDER_OBSERVATIONReps()
log.info("Result references {} orders", numOrders)

for (int i = 0; i < numOrders; i++) {
    def orderobs = 'ORDER_OBSERVATION(' + i + ')/'
    def an = get(orderobs + 'ORC-2-1')
    an = an.substring(2, an.length())
    set(orderobs + 'ORC-2-1', an)
    set(orderobs + 'ORC-3-1', an)
    set(orderobs + 'OBR-2-1', an)
    set(orderobs + 'OBR-3-1', an)

    log.debug("Finished unprefixing order #{}", i)
}

// Set receiving facility by placer site prefix code (FUTURE)
def receivingFacility = prefix
if (prefix.equals('XX')) {
    receivingFacility = 'UNKNOWN'
    log.error("Placer site prefix on patient id is unrecognized: {}", prefix)
}

log.info("Mapping site prefix code {} to receiving facility {}", prefix, receivingFacility)
set('MSH-6', receivingFacility)
