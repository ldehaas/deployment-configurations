// HL7 Order Morphing script for BWH

// Subroutine to clean up an exam code.
def clean(examCode)
{
    // Split exam code into list on '$' delimiter
    codeList = examCode.split("\\\$")

    // Strip off unwanted suffixes
    codeList = codeList.collect {
        if (it.contains(':')) {
            // it's Modality_OtherX:some custom input, don't touch
            return it;
        }

        dotIndex = it.indexOf('.')
        if (dotIndex < 0) {
            return it
        } else {
            return it.substring(0, dotIndex)
        }
    }

    // Sort list of codes alphabetically and remove duplicates
    codeList.sort().unique()

    return codeList.join("\$")
}

// Determine a two letter placer site prefix code
def sendingFacility = get('MSH-4')
def prefix = 'XX'
if (sendingFacility.length() == 2)
    prefix = sendingFacility
else
    log.error("Unrecognized sending facility in HL7 order: {}", sendingFacility)

// Prefix Accession Number and Patient ID with placer site prefix code
set('PID-3', prefix + get('PID-3'))
set('ORC-2', prefix + get('ORC-2'))

// Remove the &&ISO from the Patient ID
set('PID-3-4-3', '')

// It seems we need to keep the ^^^ after PID.3.1
set('PID-3-4-1', '1')

// Add 1001000^^^524 to the PID.3 segment
set('PID-3(1)-1', '1001000')
set('PID-3(1)-4', '524')

// Fill out the accession number in the ORC & OBR segments
copy('ORC-2', 'ORC-3')
copy('ORC-2', 'OBR-2')
copy('ORC-2', 'OBR-3')

// Copy the accession number into the IPC segment
copy('ORC-2', 'IPC-1')

// Organization Id
def orgId = ''
switch ( prefix ) {
    // #### Alphabetical order ####
    case 'AJ':
        orgId = '51455'
        break
    case 'CC':
        orgId = '3527'
        break
    case 'DD':
        orgId = '52003'
        break
    case 'DN':
        orgId = '52002'
        break
    case 'FH':
        orgId = '2804'
        break
    case 'GC':
        orgId = '52001'
        break
    case 'HM':
        orgId = '52005'
        break
    case 'HU':
        orgId = '13491'
        break
    case 'NW':
        orgId = '2800'
        break
    case 'QM':
        orgId = '2893'
        break
    case 'SM':
        orgId = '2812'
        break
    case 'SS':
        orgId = '2822'
        break
    case 'VB':
        orgId = '24768'
        break
    case 'VJ':
        orgId = '52004'
        break
    case 'VM':
        orgId = '24770'
        break
    case 'VT':
        orgId = '24772'
        break
    case 'VW':
        orgId = '24764'
        break
    // #### Alphabetical order ####
}
set('ORC-2-2', orgId)
set('OBR-2-2', orgId)

// Filler Site
set('ORC-3-2', 'BWH')
set('OBR-3-2', 'BWH')

// Remove unwanted suffixes and sort exam codes
set('OBR-4-5', clean(get('OBR-4-5')))

// We need a '1' in OBR.1 - WHY?
set('OBR-1', '1')

// Add 1507 (not sure what this refers to) into the IPC segment - WHY?
set('IPC-5', '1507')

// 270556 in IPC.2 and OBR.4 - WHY?
set('OBR-4', '270556')
set('IPC-2', '270556')
