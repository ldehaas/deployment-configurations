#!/bin/sh

today="`date +%Y%m%d`"

if [ grep -qF "$today" /home/rialto/rialto/etc/us_holidays.txt ]; then
    echo "Skipping backup" $today >> rialto/log/backup-rialto-consult.out
    exit 0
else
    /bin/nice /usr/bin/ionice -n7 bin/backup-rialto-consult -v --silent &>rialto/log/backup-rialto-consult.out
fi