#!/usr/bin/perl

#
# Put Callback Numbers into XML Format
#
# Read list of callback phone numbers from stdin
# and output the names in XML format, suitable for inclusion in the
# configuration file for Rialto Consult, a product of Karos Health.
#
# Written by: John E. Lammers, June 2011.
# Copyright (c) 2011 Karos Health Inc., Waterloo Ontario Canada
#

sub trim($)
{
    my $string = shift;
    $string =~ s/^\s+//;
    $string =~ s/\s+$//;
    return $string;
}

$arguments = @ARGV;
if ($arguments != 0) {
    print "Usage: $0 <name_list.csv \n";
    exit 1;
}

#
# Output titles.
#
print "<config>\n";
$date=`date`;
chomp $date;
print "    <!-- $date -->";

#
# Read the list of names from stdin into hash tables for sorting.
#
while (<STDIN>) {

    chomp;
    $line = $_;

    # Skip comment lines
    if (/^#/) {
        next;
    }

    # Enforce number of commas
    $commas = $_ =~ tr/,//;
    if ($commas != 1) {
        die "Line must contain 1 comma: $_";
    }

    ($number, $site) = split /,/;

    # Remove leading / trailing whitespace
    $number = trim($number);
    $site = trim($site);
    $site = "\U$site"; # Uppercase

    if (length($number) < 2) {
        die "Number is one character: $line";
    }
    if (length($site) != 2) {
        die "Site name must be 2 characters long: $site";
    }

    print "
    <CallbackNumber>
        <Number>$number</Number>
        <Site>$site</Site>
    </CallbackNumber>";
}
print "\n";
print "</config>\n";
