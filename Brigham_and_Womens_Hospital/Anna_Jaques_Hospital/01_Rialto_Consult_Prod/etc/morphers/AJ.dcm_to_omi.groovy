//
// Consult Draft Order Creation for Anna Jaques Hospital (Consult-Placer)
//
// This script extracts fields from the last DICOM object in a study and
// assembles an HL7 OMI message that is sent to Consult-Filler.  The message
// is used to create a draft order in Consult.
//
// Written by John Lammers, June 2012.
//


// Subroutine to assemble command separated list of non-null values
def appendWithCommas(value, existing) {
    if (existing) {
        if (value) {
            return existing + ', ' + value
        } else {
            return existing
        }
    } else {
        return value
    }
}

// Subroutine to assemble comma separated list of label=value
def showValue(label, value, existing) {
    def defn = label + '=' + value
    if (existing) {
        return existing + ', ' + defn
    } else {
        return defn
    }
}

// Message type must be OMI^O23
initialize('OMI', 'O23', '2.5')

// Placer site ID - hard coded
set('MSH-4', 'AJ')

// Patient demographics
set('PID-3-1', get(PatientID))
set('PID-7', get(PatientBirthDate))

def patientSex = get(PatientSex)
if (patientSex == 'M' || patientSex == 'F') {
    set('PID-8', patientSex)
}

// Patient names from Anna Jaques are not delimited correctly.
def patientName = get(PatientName)
if (patientName.contains('^')) {
    // In case it is delimited correctly
    setPersonName('PID-5', patientName)
} else {
    // In case it contains exactly one space
    def nameList = patientName.split(' ')
    if (nameList.size() == 2) {
        set('PID-5-1', nameList[0]) // Guess that first word is last name
        set('PID-5-2', nameList[1]) // Guess that last word is first name
    // Otherwise just dump it in.  They'll have to fix it manually.
    } else {
        setPersonName('PID-5', patientName)
    }
}

def accessionNumber = get(AccessionNumber)
if (accessionNumber == null) {
    log.warn('Unable to create draft order - accession number missing, pid={}',
        get(PatientID))
    return false
}
set('ORC-2-1', accessionNumber)

// Modality to Order Type Mapping
//
// Select the most appropriate order type based on the modality of the
// images in the study.  Consult supports order types CT, MR, US, NM and XR.
//
// Details:
// - Note that "XR" is not a DICOM modality code, but Consult uses this
// two letter code to identify an order for any kind of X-Ray.
// - Many modalities do not send the optional (0008, 0061) Modalities
// in Study, so Rialto Consult collects this information from
// (0008, 0060) Modality from each image and then adds Modalities in
// Study to the last image (which is passed to this script).
def modalitiesInStudy=getList(ModalitiesInStudy);
if (modalitiesInStudy.size() == 0) {
    log.error('Modalities in Study is empty for study {}',
        accessionNumber)
    return false
}

log.debug("Modalities in study for study {} is {}",
    accessionNumber, modalitiesInStudy.join(", "))

def orderCode = null
modalitiesInStudy.find {
    switch (it) {
        // Modality codes that match order codes
        case 'CT':
            orderCode = it
            return true
        case 'MR':
            orderCode = it
            return true
        case 'NM':
            orderCode = it
            return true
        case 'US':
            orderCode = it
            return true

        // Map to order code 'MR'
        case 'MA': // Magnetic Resonance Angiography
        case 'MS': // Magnetic Resonance Spectroscopy
            orderCode = 'MR'
            return true

        // Map to order code 'NM'
        case 'ST': // Color Flow Doppler
            orderCode = 'NM'
            return true

        // Map to order code US
        case 'CD': // Color Flow Doppler
        case 'DD': // Duplex Doppler
        case 'EC': // Echocardiography
            orderCode = 'US'
            return true

        // All other modality codes are ignored
    }
}
if (orderCode == null) {
    log.info("Defaulting to order code XR (X-Ray) for order {}",
        accessionNumber)
    orderCode = 'XR' // Default
} else {
    log.info("Chose order code {} for study {}",
        orderCode, accessionNumber)
}
set('IPC-5-2', orderCode)

// Consult counts the number of images in the study and puts it into
// this fake field.
set('DG1-3-1', get(NumberOfStudyRelatedInstances))

// History, Signs and Symptoms
def history = ''
history = appendWithCommas(get(ReasonForStudy), history)
history = appendWithCommas(get(AdmittingDiagnosesDescription), history)
set('OBR-13', history)

// Order Comments
// Try to put some useful information in this field
def comment = ''
comment = showValue('RefPhys', get(ReferringPhysicianName), comment)
comment = showValue('Tech', get(OperatorsName), comment)
comment = showValue('Protocol', get(ProtocolName), comment)
comment = showValue('StudyDescription', get(StudyDescription), comment)

log.debug("Comment is '${comment}'")

set('/.ORDER/.NTE-2', 'comments') // Need to label the comment
set('/.ORDER/.NTE-3', comment)

// log.debug("DICOM to HL7 script complete")
// return false
