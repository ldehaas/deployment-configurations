//
// HL7 Result Morphing script (Groovy) for sending result messages
// from BWH Nightwatch to Iatric at Anna Jaques
//
// This script was developed by comparing a result msg from Rialto Consult
// (after morphing) with a sample result msg supplied by Anna Jaques.
//
// Written by: John Lammers, Karos Health, Oct 2011.
//

// For testing - these override if not empty
def FIXED_PID = ""          // "M0325104"
def FIXED_ACC = ""          // "001029451"
def FIXED_PLACER_ID = ""    // "51455"
def FIXED_UNIVERSAL_SERVICE_ID_1 = "" // "CHABDPELW"
def FIXED_UNIVERSAL_SERVICE_ID_2 = "" // "CHEST/ABD/PELVIS W CONTRAST"
def FIXED_UNIVERSAL_SERVICE_ID_3 = ""

//
// Subroutine to assemble name for addressing specific OBX segment.
//
def obxName(instance, address) {
    return "/.OBSERVATION(" + instance + ")/.OBX-" + address
}

//
// Subroutine to customize name field as requested by AJ.
//
def customizeObrName(fieldNumber) {

    field = "OBR-" + fieldNumber + "-"

    // Get names from sub-component fields
    familyName = get(field + "1-2")
    givenName = get(field + "1-3")

    set(field + "1-2", "")
    set(field + "1-3", "")

    // Put names in component fields instead
    set(field + "2", familyName)
    set(field + "3", givenName)
}

//
// Update message header.
//
def SENDING_APPLICATION = "Rialto"
def SENDING_FACILITY = "BWH"
def RECEIVING_APPLICATION = "MEDITECH"
def RECEIVING_FACILITY = "AJ"

set("MSH-3", SENDING_APPLICATION)
set("MSH-4", SENDING_FACILITY)
set("MSH-5", RECEIVING_APPLICATION)
set("MSH-5-2", RECEIVING_APPLICATION)
set("MSH-6", RECEIVING_FACILITY)

// For standalone testing only -- Rialto will normally do this
def today = new Date()
set("MSH-7", today.format('yyyyMMddHHmmss'))

set("MSH-13", "") // Sequence Number
set("MSH-14", "") // Continuation Pointer

//
// Adjust patient information.
//
set("PID-1", "") // Field name: "Set ID - PID"
if (FIXED_PID != "") {
    set("PID-3", FIXED_PID)  // Internal PID
}
set("PID-7", get("PID-7") + "000000") // AJ expects time on birthdate?

//
// Adjust Patient Visit Information.
//
set("PV1-2", "")    // Patient Class
set("PV1-7-1", "")  // Attending Doctor
set("PV1-7-2", "")  // Attending Doctor
set("PV1-7-3", "")  // Attending Doctor

//
// Adjust Order Common Information.
//
// set("ORC-2-2", FIXED_PLACER_ID) // Organization ID
set("ORC-2-2", "")

def placerOrderNumber
if (FIXED_ACC != "") {
    placerOrderNumber = FIXED_ACC
    set("ORC-2", placerOrderNumber)
} else {
    placerOrderNumber = get("ORC-2")
}
set("ORC-3", "") // Filler Order Number

//
// Adjust Order Information.
//
// set("OBR-2", placerOrderNumber)

// set("OBR-2-2", FIXED_PLACER_ID) // Organization ID
set("OBR-2-2", "")

set("OBR-3", placerOrderNumber)
if (FIXED_UNIVERSAL_SERVICE_ID_1 != "") {
    set("OBR-4-1", FIXED_UNIVERSAL_SERVICE_ID_1)
    set("OBR-4-2", FIXED_UNIVERSAL_SERVICE_ID_2)
    set("OBR-4-3", FIXED_UNIVERSAL_SERVICE_ID_3)
}

// OBR result status mappings for AJ (non-standard HL7)
def oldResultStatus = get("OBR-25")
def newResultStatus = oldResultStatus
switch (oldResultStatus) {
    case "C": // Correction
        newResultStatus = "AS" // Addendum Signed
        break
    case "F": // Final
        newResultStatus = "S" // Signed
        break
    case "P": // Preliminary
        newResultStatus = "D" // Draft
        break
    default:
        log.warn("Result status {} not recognized for order {}",
            oldResultStatus, placerOrderNumber)
        break
}
if (oldResultStatus != newResultStatus) {
    log.debug("Mapping result status {} to {}", oldResultStatus, newResultStatus)
    set("OBR-25", newResultStatus)
}

// OBR name field customizations for AJ (non-standard HL7)
customizeObrName(32)
customizeObrName(33)
customizeObrName(34)
customizeObrName(35)

// def code = get("OBR-32-1")
// def familyName = get("OBR-32-1-2")
// def givenName = get("OBR-32-1-3")
// set("OBR-32-1-2", "")
// set("OBR-32-1-3", "")
// set("OBR-32-2", familyName)
// set("OBR-32-3", givenName)

// set("OBR-3", "===== " + givenName + " =====") // Debug

//
// Observation Information.
//
for (i in 0..500) {
    setId = get(obxName(i, "1"))
    if (setId == null) {
        break
    }

    // Set Observation Id from Universal Service Identifier
    set(obxName(i, "3-1"), get("OBR-4-1"))
    set(obxName(i, "3-2"), get("OBR-4-2"))
    set(obxName(i, "3-3"), get("OBR-4-3"))
    set(obxName(i, "3-4"), get("OBR-4-4"))

    // AJ doesn't use GDT and MDT; map to BODY
    observationSubId = get(obxName(i, "3-1-2"))
    if (observationSubId == "GDT" || observationSubId == "MDT") {
        set(obxName(i, "3-1-2"), "BODY")
    }

    // AJ wants Observation Identifier sub-component values moved to components
    usid2 = get(obxName(i, "3-1-2"))
    usid3 = get(obxName(i, "3-2"))
    usid4 = get(obxName(i, "3-3"))
    usid5 = get(obxName(i, "3-4"))

    set(obxName(i, "3-1-2"), "")

    set(obxName(i, "3-2"), usid2)
    set(obxName(i, "3-3"), usid3)
    set(obxName(i, "3-4"), usid4)
    set(obxName(i, "3-5"), usid5)

    // set(obxName(i, "16-1"), "") // Responsible Observer, component 1
    // set(obxName(i, "16-2"), "") // Responsible Observer, component 2
    // set(obxName(i, "16-3"), "") // Responsible Observer, component 3

    // Debug
    //set("OBR-3", "===== i=$i, setId=$setId, subId=$observationSubId =====")
}

