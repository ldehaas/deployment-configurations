//cfind_to_find_documents.groovy
// this script converts dicom cfind request to xds find documents query
// this is a starting point, demonstrating some features of this script

// obtaining properties from cfind
pid = get(PatientID)
issuer = "LOCAL"

// setting properties in find documents query
set(XDSPatientID, pid, issuer)

// constraint by study date, if specified in cfind
dateRange = get(StudyDate)
set(XDSServiceStartTime, dateRange)



// work around a temporary bug in java code which sets wrong format code:
//set(XDSFormatCode, code("urn:ihe:rad:1.2.840.10008.5.1.4.1.1.88.59", "1.2.840.10008.2.6.1"))


/* Test-specific Functionality */

/*
//This part of the script sets the XDS Query for Event Code based on DICOM Modality

modalitiesInStudy = get(ModalitiesInStudy)

if (modalitiesInStudy != null) {
    set(XDSEventCode, code(modalitiesInStudy, "DCM"))
    }else{
        log.debug("DICOM C-FIND did not contain modalities in study")
        }
*/


//This part of the script is for test-specific use cases, if not in use, should be commented out


/*
 // Setting the Normalized Anatomic Region for Test 2.2.4
def bodyPart = get(BodyPartExamined)
if(null != bodyPart){
    set(XDSEventCode, code(bodyPart, 'Imagerie Québec-DSQ'))
    log.info("Setting Normalized Anatomic Region {} to Imagerie Québec-DSQ", bodyPart)
}
*/
