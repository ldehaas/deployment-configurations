/*
* This is a 'hybrid' script that takes the returned C-FIND values that the DIR gives back, and makes available the "xdsmetadata" variable, 
*   containing the extended XDS Metadata for this study.
*   The script runs one study at a time, and it makes available C-Find response information in the DIR, and its analogous XDS extended metadata.
*   This information then can be reconciled, updated, merged, etc. as needed.
*   The script is multi-purpose: (please see below)
*
* The ultimate aims of this script are to:
*   1. Localize the Patient IDs given back to the Calling PACS (i.e. so the PACS can understand the results, and assign to the correct patient)
*   2. Prefix Study Accession Numbers, so there are no collisions with local studies at the PACS
*   3. Change the Retreive AETitle to that of this Connect Instance itself.  This is so the PACS can call Rialto if it decides to fetch the studies it is 
*       querying.
*   4. (Optionally) either merge, or give preference to the Modalities in Study provided by the DIR and by the XDS Regsitry
*/


LOAD("common.groovy")

log.debug("\n\n**Starting C-Find Response Localizer Script**\n")

// Do not morph responses for our own local site
if( "2.16.124.10.101.1.60.1.1007.1" == sourceIssuer){
    log.debug("The Study Instance UID for this study was: {}", get(StudyInstanceUID))
    log.debug("The IssuerOfPatientID received from XDS Registry was: {}.  This patient is from Lakeshore", sourceIssuer )

    // Use DICOM modalities in study as opposed to Registry's 
    /* Using the Modalities In Study information contained ONLY in the DIR that holds this study */   

        // Get the list of --unique-- Modalities returned by the --DIR-- 
        def dirModalities = getList('ModalitiesInStudy') as Set
        log.debug("Modalities In Study, as passed from the DIR:\n{}", dirModalities)

        def finalModalities = dirModalities
        log.debug("Final merged Modalities in Study:\n{}", finalModalities)

        set(ModalitiesInStudy, finalModalities)

    remove(IssuerOfPatientID)

    //Output the current retreive AETitle for debugging purposes
    log.debug("Current Retreive AETitle is : \"{}\"", get(RetrieveAETitle))
}
else {
    log.debug("The IssuerOfPatientID received from XDS Registry was: {}.  This patient is foreign", sourceIssuer )

    /* Merging the Modalities In Study information contained in both the XDS Registry and the DIR that holds this study */   

     // Get the 'Modalities' as held by the XDS Registry using the 'xdsmetadata' variable made available to this script.
     
        // Get the XDS list of event codes that have a 'DCM' (DICOM) value
        def xdsModalities = xdsmetadata.get(XDSEventCodes).findAll { eventCode ->
                "DCM".equals(eventCode.getSchemeName())
        }
            // getSchemeName() is a Rialto scripting function for XDS objects
        log.debug("Event Codes (Modalities In Study) as passed raw from the XDS Registry:\n{}", xdsModalities)


        // Transform all the XDS-formatted codes holding modalities into a list of plain strings
        xdsModalities = xdsModalities.collect { 
            currentXDSmodality -> currenXDSmodality.getCodeValue()
        }
            // getCodeValue() is a Rialto scripting function for XDS objects


        // Ensure that the list of modalities in this study has unique entries
        xdsModalities = xdsModalities as Set
        log.debug("Cleaned Up Event Codes (Modalities In Study) from the XDS Registry:\n{}", xdsModalities)


        // Get the list of --unique-- Modalities returned by the --DIR-- 
        def dirModalities = getList('ModalitiesInStudy') as Set
        log.debug("Modalities In Study, as passed from the DIR:\n{}", dirModalities)

        // Finally, merge the unique Modalities in this study present in the XDS Registry and the DIR
        def finalModalities = dirModalities
        finalModalities.addAll(xdsModalities)
        log.debug("Final merged Modalities in Study:\n{}", finalModalities)

        set(ModalitiesInStudy, finalModalities)

    // Checking Source Patient IDs and Issuers of PIDs
    def sourcePid = get(PatientID)
    log.debug("Source pid in cfind response before morphing: {}", sourcePid)
    def sourceIssuer = get(IssuerOfPatientID)
    log.debug("Source Issuer of PID in cfind response before morphing: {}", sourceIssuer)

    // Localizing the PID for responding to the calling PACS
    /* NOTE:  Please ENSURE that the "LocalIssuerIDs" Constant defined in the class "Constants" in common.groovy is set correctly 
    *           to the Issuer Of Patient ID corresponding to the PACS that this Connect is servicing.
    *         Otherwise, all ad-hoc C-Find responses will contain incorrect foreing patients, and the C-Find responses will
    *           be useless to the calling PACSs
    */
    def localPid = Pids.localize([sourcePid, sourceIssuer], getAllPidsQualified())
    log.debug("Setting local pid in cfind response: {}", localPid)

        // In this case, local PIDS should NOT have IssuerOfPID populated
    set(PatientID, localPid[0])
        // The localize() method, defined in common.groovy returns the actual local PID  in localPid[0], by using a PIX lookup
        //  This Issuer Of Patient ID might have been obtained from the DIR, or populated by the Java Code

    remove(IssuerOfPatientID)

    log.debug("For test 2.1.5.3: Removing Other Patient IDs Sequence")
    remove(OtherPatientIDsSequence)

    // Prefixing the Accession Numbers so they can *uniquely* and clearly be recognized as foreign studies in the PACS using Connect
    sourceIssuerToPrefix = [
        "2.16.124.10.101.1.60.1.1004.1":"SJ",
    //    "2.16.124.10.101.1.60.1.1004.1":"STJS",
        "1000016640": "CAB",
        "1000017127": "LUC",
        "1000021780": "ENF",
        "6000000031": "HMR",
        "1000017259": "STM",
        "1000016855": "SDL",
        "1000021780":"HIDJ",
        "1000015949":"CHUL",
        "1000019966":"HDDQ",
        "1.2.3.4.5.1": ""
    ]

    prefix = sourceIssuerToPrefix[sourceIssuer]

    /* if (prefix == null) {
        prefix = "XX"
    }
    */

    // Prepend the "localizer" prefix to the Accession Number
    prepend(AccessionNumber, prefix)

    //  Setting the Retreive AETitle to Connect itself, so that the PACS goes through Connect for study localization
    set(RetrieveAETitle, "RIALTO_TEST")

    //  Log monitoring of the events in this morpher
    log.trace("CFind response after morphing:\n{}", output)
}

log.debug("\n\n**Ending C-Find Response Localizer Script**\n")

