
set(PatientID, get(XDSPatientID).pid)
set(IssuerOfPatientID, get(XDSPatientID).domainUUID)

set(StudyDate, get(XDSServiceStartTime))
set(StudyTime, get(XDSServiceStartTime))

set(StudyDescription, get(XDSTitle))

// map modality from event codes
// set(Modality, eventCodeToModality[ get(XDSEventCodes)[0] ])

/*
 * get the following from extended metadata
 */
 
set(StudyInstanceUID, get(XDSExtendedMetadata("studyInstanceUid")))

// This method uses the "RepositoryUIDToDIRAETitleMap" inside the Connect Service in rialto.cfg.xml
    // The Registry itself only knows Repository UIDs.  These have to be hard-mapped to known AETitles (Query/Retrieve SCPs) in the DIR
set(RetrieveAETitle, get(XDSExtendedMetadata("retrieveAETitle")))
