/* CFind Response Filter
*
*   This morpher is part of the Fetch Prior Studies workflow.
*   It takes an individual study (XDS and CFind Responses; interfaced as a DICOM object) and optionally
*   filters it out studies based on different criteria, so they are not given to the local PACS as part of a Fetch
*   Prior Studies workflow.
*
*   (There is a different, though related script, CFind Response Rank Filter, that can filter studies based on study age
*   or total number of responses)
*
*   Common Uses for this Cfind Response Filter script can be: 
*       - Exclude or include local studies (i.e. local to the PACS)
*
*       - Rule out studies based on Modality (if a site never wants to prefetch modality x or y, it can be achieved here)
*               Note: this type of modality filtering can only be black or white (i.e. absolute).  Setting a modality to be 
*                   discarded here will --ALWAYS-- get dropped in a Fetch Prior Studies workflow.
*                   To set up modality filtering that, say, only matches the passed modality in the HL7 order, this must be
*                   set in the 'HL7 to CFind' morpher script.
*
*       - Rule out specific anatomic regions (same rules as above apply)
*
*
*   Currently, this script is doing the following:
*       1. Only processing (Fething Priors from) Ste-Justine.  (It is ignoring locals = Lakeshore, and all else)
*
*/



log.info("\n\n**Starting C-Find Response FILTER Script**\n")

def sourceIssuer = get(IssuerOfPatientID)
log.info("The Issuer of Patient ID, as retreived from the DIR was \"{}\".", sourceIssuer)
log.info("The Study Instance UID, as retreived from the DIR was \"{}\".\n", get(StudyInstanceUID))

def LakeShore_Study = false
def SteJustine_Study = false
def OtherStudy = false

log.info("The current Retreive AETitle given from the the DIR is \"{}\".", get(RetrieveAETitle)) 

// Temporarily ignoring local (i.e. Lakeshore) studies; AND only sending Ste-Justine Studies
 
 if( "SJ" == sourceIssuer){
    log.info("This is a Ste-Justine Study")
    // This should fall through and the Ste-Justine study should be processed
 } else if( "LS" == sourceIssuer) {
    log.info("This is a LAKESHORE Local Study. Sinking this study.  You should see *nothing* in the object output for this study")
    return false
 } else {
    log.info(" OMMITTING study from C-Find Responses. You should see *nothing* in the object output for this study")
    return false
 }

 log.info("This is the Object to be localized in the C-Find Response Localizer: {} \n\n\n **End of Object**", output)


log.info("\n\n**Ending C-Find Response FILTER Script**\n")

