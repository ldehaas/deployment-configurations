#!/bin/bash

echo "This script will install heartbeat config files"
echo "Old config will be renamed to have suffix .bk"
echo "Hit any key to proceed, or ctrl+c to cancel"

read junk


function run {
    echo $*
    $*
}

function backup {
    run mv -f "$*" "$*".bk
}

HARIALTO=`dirname "$0"`/ha.d
HARIALTO=`cd "$HARIALTO"; pwd`
function link {
    run ln -sf "$HARIALTO/$1" "$HAETC/$1"
}

HAETC=/etc/ha.d

echo "Backing up files"
backup $HAETC/ha.cf
backup $HAETC/haresources
backup $HAETC/authkeys

echo "Linking new configs"
link ha.cf
link haresources
link authkeys

echo "Enabling heartbeat on startup"
chkconfig heartbeat on

echo ""
echo "================="
echo "Remember to edit $HAETC/ha.cf and $HAETC/haresources to "
echo "the correct machine name (e.g. uname -n) and virtual ip"
echo "see the linked files for more documentation"
echo ""
echo "Start heartbeat by issuing"
echo "service heartbeat start"
echo "and check /var/log/messages to ensure everything is ok"
echo ""
echo "Tools for debugging (should be on PATH already):"
echo "show_virtual_ip"
