<config>

    <!-- Variable Declaration -->

    <var name="morphers">${rialto.rootdir}/etc/morphers</var>
    <var name="xds-morphers">${rialto.rootdir}/etc/connectxds/morphers</var>

    <!-- Listening Servers -->
 
    <server id="dicomserver" type="dicom">
        <port>4104</port>
    </server>

    <server id="pix" type="hl7v2">
        <port>5555</port>
    </server>

    <server id="fpshl7" type="hl7v2">
        <port>5556</port>
    </server>

    <server type="http" id="internal-http">
        <port>8081</port>
    </server>
 
    <server type="http" id="navigator-http" default="true">
        <port>8080</port>
    </server>


    <!-- Devices to Connect To -->

    <device type="dicom" id="RIALTO">
        <host>localhost</host>
        <port>4104</port>
        <aetitle>RIALTO_TEST</aetitle>
    </device>

    <device type="dicom" id="Lakeshore-HMI-PACS">
        <host>10.135.160.199</host>
        <port>4000</port>
        <aetitle>ALI_STORE_SCP</aetitle>
    </device>

    <device type="hl7v2" id="Lakeshore-HMI-PACS-HL7">
        <host>10.135.160.199</host>
        <port>15551</port>
        <sendingApplication>KARXDS02</sendingApplication>
        <sendingFacility>KARXDS02</sendingFacility>
    </device>

    <device type="dicom" id="XDL-McGill-DIR">
        <host>10.128.55.66</host>
        <port>4020</port>
        <aetitle>XDL_KAROS_QRSCP</aetitle>
    </device>

    <device type="xdsreg" id="IBM-xdsreg">
        <url>http://10.241.7.31:9085/IBMXDSRegistry/XDSb/SOAP12/Registry</url>
    </device>

    <!-- START Devices for QuebecSim environment - ->
    <device type="dicom" id="Lakeshore-HMI-PACS">
        <host>10.0.1.73</host>
        <port>11112</port>
        <aetitle>ASC_PACS</aetitle>
    </device>

    <device type="dicom" id="XDL-McGill-DIR">
        <host>10.0.1.89</host>
        <port>11112</port>
        <aetitle>DCM4CHEE</aetitle>
    </device>

    <device type="xdsreg" id="IBM-xdsreg">
        <url>http://10.0.1.86:8010/axis2/services/xdsregistryb</url>
    </device>
    <!- - END Devices for QuebecSim environment -->



    <device id="audit" type="audit">
            <port>4000</port>
            <host>localhost</host>
    </device>

    <!-- Rialto Services -->

    <service id="txn-records" type="txn-records">
        <config>
            <prop name="StorageDir">storage</prop>
        </config>
    </service>
    
    <service id="arrudp" type="arr">
            <server idref="internal-http" name="arr-api">
                <url>/arrudp/auditRecords/*</url>
            </server>
            <config>
                <prop name="IndexDir">${rialto.rootdir}/var/arr/index</prop>
                <prop name="IndexBackupDir">${rialto.rootdir}/var/arr/backup</prop>
                <prop name="ArchiveDir">${rialto.rootdir}/var/arr/archive</prop>
                <prop name="TempDir">${rialto.rootdir}/var/arr/temp</prop>
                <prop name="Protocol">udp</prop>
                <prop name="Port">4000</prop>
                <prop name="BulkSize">1000</prop>
            </config>
    </service>

    <service type="connect-xds" id="connect-xds">
        <server idref="dicomserver" name="cfind" />
        <server idref="dicomserver" name="cmove" />
        <server idref="dicomserver" name="cstore" />
        <server idref="fpshl7" name="fps"/>
        <server idref="pix" name="pix" />
    
        <device idref="Lakeshore-HMI-PACS" name="PACSStorage"/>
        <device idref="Lakeshore-HMI-PACS-HL7" name="PACSHL7"/>
        <device idref="XDL-McGill-DIR" name="HomeDIR" />
        <device idref="IBM-xdsreg" />

        <config>
            <prop name="PIXDatabaseURL" value="jdbc:postgresql://localhost/pix?user=pix" />
            <prop name="PIXMorpher" value="${morphers}/pix_morpher.groovy" />

            <!-- Key aspect of cfind workflow: -->
            <prop name="CFindToFindDocumentsMorpher" value="${morphers}/cfind_to_find_documents.groovy" />
            <prop name="CFindResponseLocalizer" value="${morphers}/cfind_response_localizer.groovy" />

<!-- TESTING flushing cache WAAAAY to early -->
            <prop name="RetrieveAECacheTimeout" value="60s" />
            <prop name="RetrieveAECacheCheckFrequency" value="10s" />

<!-- TESTING Morpher to IGNORE Home DI-r  - ->
            <prop name="HomeDIRCFindResponseMorpher" value="${morphers}/cfind_response_localizer_homedir.groovy" />
        -->

            <!-- Key aspect of fps workflow: -->
            <prop name="HL7ToCFindMorpher" value="${morphers}/hl7_to_cfind_morpher.groovy" />

            <prop name="ForeignImageLocalizer" value="${morphers}/foreign_image_localizer.groovy" />

            <!-- not used, temporarily "required", but will be removed: -->
            <prop name="DocumentMetadataToCFindMorpher" value="${morphers}/document_metadata_to_cfind.sample.groovy" />
            <prop name="ForeignImageMorpher" value="${morphers}/foreign_image_morpher.sample.groovy" />

            <prop name="ImageToHL7OrderMorpher" value="${morphers}/image_to_hl7_morpher.groovy" />

        
            <prop name="AffinityDomain" value="2.16.124.10.101.1.60.100" />
            <prop name="RepositoryUIDToDIRAETitleMap">
                2.16.124.10.101.1.60.2.61:XDL_KAROS_QRSCP
                2.16.124.10.101.1.60.2.51:IDCTSAS
            </prop>
            <prop name="LocalAETitle" value="RIALTO_TEST" />

            <!-- START Configuration for QuebecSim - ->
            <prop name="AffinityDomain" value="1.3.6.1.4.1.21367.13.20.300" />
            <prop name="RepositoryUIDToDIRAETitleMap">
                1.3.6.1.4.1.21367.2010.1.2.1125:DCM4CHEE
            </prop>
            <!- - END Configuration for QuebecSim -->
        </config>
    </service>
    
    <service id="navigator" type="navigator">
        <device idref="RIALTO" name="proxyDIR" />

        <server idref="navigator-http" name="navigator-war">
            <url>/</url>
        </server>
        <config>
            <prop name="ArrURL">http://localhost:8081/arrudp</prop>
        </config>
    </service>

</config>
