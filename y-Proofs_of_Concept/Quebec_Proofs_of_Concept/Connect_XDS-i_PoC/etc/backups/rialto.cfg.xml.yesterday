<config>

<!-- Variable Declaration -->

    <var name="morphers">${rialto.rootdir}/etc/morphers</var>
    <var name="CFindToFindDocumentsMorpher">${rialto.rootdir}/etc/connectxds/morphers/cfind_to_find_documents.sample.groovy</var>
    <var name="CFindResponseLocalizer">${rialto.rootdir}/etc/connectxds/morphers/cfind_response_localizer.groovy</var>
    <var name="DocumentMetadataToCFindMorpher">${rialto.rootdir}/etc/connectxds/morphers/document_metadata_to_cfind.sample.groovy</var>
    <var name="imageLocalizer">${rialto.rootdir}/etc/morphers/foreign_image_morpher.sample.groovy</var>
    <var name="hl72cfind">${rialto.rootdir}/etc/morphers/hl7_to_cfind_morpher.sample.groovy</var>
    <var name="cfindresponsefilter">${rialto.rootdir}/etc/morphers/cfind_response_filter.sample.groovy"</var>
    <var name="pixMorpher">${morphers}/pix_morpher.groovy</var>

<!-- Listening Servers -->
 
    <server id="dicomserver" type="dicom">
        <port>4104</port>
    </server>

    <server id="pix" type="hl7v2">
        <port>5555</port>
    </server>

    <server id="fpshl7" type="hl7v2">
        <port>5556</port>
    </server>

    <server type="http" id="internal-http">
        <port>8081</port>
    </server>
 
    <server type="http" id="navigator-http" default="true">
        <port>8080</port>
    </server>


<!-- Devices to Connect To -->

    <device type="dicom" id="RIALTO">
        <host>localhost</host>
        <port>4104</port>
        <aetitle>RIALTO_TEST</aetitle>
    </device>

    <device type="dicom" id="IDCTSAS">
        <host>10.28.73.199</host>
        <port>11112</port>
        <aetitle>IDCTSAS</aetitle>
    </device>

    <device type="dicom" id="Lakeshore-HMI-PACS">
        <host>10.135.160.199</host>
        <port>4000</port>
        <aetitle>ALI_QUERY_SCP</aetitle>
    </device>

    <device type="dicom" id="XDL-McGill-DIR">
        <host>10.128.55.66</host>
        <port>4020</port>
        <aetitle>XDL_KAROS_QRSCP</aetitle>
    </device>

    <device type="dicom" id="XDL-McGill-DIR2">
        <host>10.128.55.66</host>
        <port>4020</port>
        <aetitle>2.1 - RIDTestMontreal</aetitle>
    </device>

    <device type="xdsreg" id="IBM-xdsreg">
        <url>http://10.241.7.31:9085/IBMXDSRegistry/XDSb/SOAP12/Registry</url>
    </device>

    <device id="audit" type="audit">
            <port>4000</port>
            <host>localhost</host>
    </device>

<!-- Rialto Services -->

    <service id="txn-records" type="txn-records">
        <config>
            <prop name="StorageDir">storage</prop>
        </config>
    </service>
    
    <service id="arrudp" type="arr">
            <server idref="internal-http" name="arr-api">
                <url>/arrudp/auditRecords/*</url>
            </server>
            <config>
                <prop name="IndexDir">${rialto.rootdir}/var/arr/index</prop>
                <prop name="IndexBackupDir">${rialto.rootdir}/var/arr/backup</prop>
                <prop name="ArchiveDir">${rialto.rootdir}/var/arr/archive</prop>
                <prop name="TempDir">${rialto.rootdir}/var/arr/temp</prop>
                <prop name="Protocol">udp</prop>
                <prop name="Port">4000</prop>
                <prop name="BulkSize">1000</prop>
            </config>
    </service>

    <service type="connect-xds" id="connect-xds">
        <server idref="dicomserver" name="cfind" />
        <server idref="dicomserver" name="cmove" />
        <server idref="dicomserver" name="cstore" />
        <server idref="fpshl7" name="fps"/>
        <server idref="pix" name="pix" />
    
        <device idref="Lakeshore-HMI-PACS" name="PACS"/>
        <device idref="XDL-McGill-DIR" name="HomeDIR" />
        <device idref="IBM-xdsreg" />

        <config>
            <prop name="LocalAETitle" value="RIALTO_TEST"/>
            <prop name="CFindResponseFilter" value="${rialto.rootdir}/etc/morphers/cfind_response_filter.sample.groovy" />
            <prop name="CFindToFindDocumentsMorpher" value="${CFindToFindDocumentsMorpher}" />
            <prop name="CFindResponseLocalizer" value="${CFindResponseLocalizer}" />
            <prop name="DocumentMetadataToCFindMorpher" value="${DocumentMetadataToCFindMorpher}" />
            <prop name="HL7ToCFindMorpher" value="${hl72cfind}" />
            <prop name="PIXDatabaseURL" value="jdbc:postgresql://localhost/pix?user=pix" />
            <prop name="AffinityDomain" value="2.16.124.10.101.1.60.100" />
            <prop name="PIXMorpher" value="${pixMorpher}"/>
            <prop name="ForeignImageMorpher" value="${imageLocalizer}" />
            <prop name="RepositoryUIDToDIRAETitleMap">
                2.16.124.10.101.1.60.2.61:XDL_KAROS_QRSCP
                2.16.124.10.101.1.60.2.51:IDCTSAS
            </prop>
        </config>
    </service>
    
    <service id="navigator" type="navigator">
        <device idref="RIALTO" name="proxyDIR" />

        <server idref="navigator-http" name="navigator-war">
            <url>/</url>
        </server>
        <config>
            <prop name="ArrURL">http://localhost:8081/arrudp</prop>
        </config>
    </service>

</config>
