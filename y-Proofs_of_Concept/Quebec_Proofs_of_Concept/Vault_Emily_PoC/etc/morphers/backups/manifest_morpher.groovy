log.trace("MANIFEST Creation: raw object before being modified by the script:\n {} \n\n", input)


def originalStudyInstanceUID = get('StudyInstanceUID')
set('StudyInstanceUID', originalStudyInstanceUID)
log.debug("Creating Manifest for Study Instance UID: {}", originalStudyInstanceUID)


// Explicitly set the "Retrieve Location UID" inside the "Referenced Series Sequence" to the  Repo's OID
set('CurrentRequestedProcedureEvidenceSequence/ReferencedSeriesSequence/RetrieveLocationUID', '2.16.124.10.101.1.60.2.81')
set('CurrentRequestedProcedureEvidenceSequence/ReferencedSeriesSequence/RetrieveAETitle', 'ALI_QUERY_EIRSCP')



/* Infer and Populate the IssuerOfPatientID based on the Combination of (Calling + Called AETitle) and Instution Name
*   NOTE: This is a crucial part of the Manifest Creation workflow.  If the Issuer Of Patient ID must be correctly populated
*       for the PIX lookup to work.  A working PIX lookup is needed to publish the Manifest against the correct patient
*/
 def issuerOfPID = get(IssuerOfPatientID)
 log.debug("\n Issuer Of Patient ID as received from the HMI EIR is \"{}\"", issuerOfPID)
 
 def callingAE = getCallingAETitle()
 def calledAE = getCalledAETitle()
 log.debug("Calling AETitle was \"{}\", and Called AETitle was \"{}\".",callingAE, calledAE)

 def institutionNameTopLevel = get(InstitutionName)

 if('H_LKSHR_SCU' == callingAE && 'VLT_H_LKSHR' == calledAE){
    set('IssuerOfPatientID', '2.16.124.10.101.1.60.1.1007.1')
    log.debug("Assuming this Study is from Lakeshore.")

 } else if('ALI_QUERY_SCU' == callingAE && 'RIALTO_TEST' == calledAE){
    set('IssuerOfPatientID', '2.16.124.10.101.1.60.1.1007.1')
    log.debug("Assuming this Study is from Lakeshore.")
 
 } else if ('H_STJSTN_SCU' == callingAE && 'VLT_H_STJSTN' == calledAE) {
    set('IssuerOfPatientID', '2.16.124.10.101.1.60.1.1004.1')
    log.debug("Assuming this Study is from Ste-Justine.")

 } else {
    log.warn("HMI EIR Pushing studies to Vault is using a different Called / Calling AETitle combination than expected.\n Will try to infer the Issuer Of Patient ID of this study based on the Institution Name field")
    
    log.info("Institution Name in this SOP is \"{}\"", institutionNameTopLevel)

    if("Lakeshore"==institutionNameTopLevel){
        set('IssuerOfPatientID', '2.16.124.10.101.1.60.1.1007.1')
        log.info("Explicitly setting the IssuerOfPatientID from a known populated Institution.  Setting to the Quebec normalized IssuerOfPID for Hôpital général du Lakeshore")
        log.debug("Assuming this Study is from Lakeshore.")
    }

    if("HSJ"==institutionNameTopLevel){
        set('IssuerOfPatientID', '2.16.124.10.101.1.60.1.1004.1')
        log.info("Explicitly setting the IssuerOfPatientID from a known populated Institution.  Setting to the Quebec normalized IssuerOfPID for CHU Sainte-Justine") 
        log.debug("Assuming this Study is from Ste-Justine.")
    }

    else {
        if('2.16.124.10.101.1.60.1.1007.1' != get(IssuerOfPatientID) || '2.16.124.10.101.1.60.1.1004.1' != get(IssuerOfPatientID) ){

            throw new Exception ("Unrecognized Calling and Called AETitles + unknown Institution Name.  Aborting Manifest Publishing.") 
            }
    }      
 }





// Set Study Date and Time
def modality = get(Modality)

modality = modality.toUpperCase()
log.info("\nManifest Creation:  Current SOP Modality is \"{}\"",modality)

if(null != modality) {
    log.info("\nManifest Creation:  Current Study Date is: \"{}\"", get(StudyDate))
    if('SR' != modality) {
      // If the DICOM object is NOT an SR, use StudyDateTime to set Manifest "Study Date / Time".  If StudyDate is not available, use the SOP Date/Time      
        if(null != get(StudyDate)) {
            set(StudyDate, get(StudyDate))
            set(StudyTime, get(StudyTime))
        } else {
            // DONT KNOW WHAT THE TAG FOR SOP DATE TIME IS
        }
    } else {
      // This could actually be null.  It's better to set an empty field and fail submission than to put in a wrong date
        set(StudyDate, get(StudyDate))
        set(StudyTime, get(StudyTime))
    }
}


// Explictly set top-level institution name in an SR publshing (i.e. besides the Referring Physician Identification Sequence)
def institutionName = get('ReferringPhysicianIdentificationSequence/InstitutionName')
if(null != modality) {
    if('SR' == modality) {
        // SRs don't have institution name explicitly in their structure.  Set it to empty if an SR comes in. (Just to have the tag, it shouldn't overwrite what's in the SOPs)
        if(null != institutionName){
            set(InstitutionName, institutionName)
            log.info("Expliclitly setting top level institution name for an SR publishing.  Setting to  \"{}\"", institutionName)
        } else {
            log.info("Institution name is blank.")
        }
    } /*else {
       // Temporarily SETTING a value for Testing
        set(InstitutionName, 'LakeShore1') 
    }

} else {
    // Temporarily SETTING a value for Testing
    set(InstitutionName, 'LakeShore1')  
*/
}


// Temporarily setting the Code Meaning to null
// set('AnatomicRegionSequence/CodeMeaning', null)

// NEED TO FINISH THIS, MAPS, ETC
// Explicitly set the Anatomic Region Sequence Code Meaning if not Populated
/*if(null == get('AnatomicRegionSequence/CodeMeaning')) {
    set('AnatomicRegionSequence/CodeMeaning', 'Autre')
    //set('AnatomicRegionSequence/CodeMeaning', 'Anatomic Region Code')
    log.info("Manifest (DICOM KOS) Creation: Explicitly setting the Anatomic Region Code, as it was NULL in the Study or SR Report")
}
*/


// Explicitly setting the Additional Patient History field
set(AdditionalPatientHistory, get(AdditionalPatientHistory))

// Explicitly set the StudyPriorityID
set(StudyPriorityID, get(StudyPriorityID))


// Explicitly set the ConfidentialityCode
set(ConfidentialityCode, get(ConfidentialityCode))





// Set Study Date / Study Time

/*
// If more than 1 Site sends images without an IssuerOfPatientID, set it by inferring it from the CallingAETitle
if(null==get(IssuerOfPatientID)) {
 //       set('IssuerOfPatientID', 'LOCAL')
         set('IssuerOfPatientID', '2.16.124.10.101.1.60.1.1006.1')
   }
*/

// Logging the entrie DICOM object for debug
log.trace("This is the current manifest being created:\n {}",output)

