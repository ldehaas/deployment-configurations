// Currently turning ON orders altogether for Connect
//return false


def sendingFacility = get('MSH-4')
sendingFacility = sendingFacility.toUpperCase()
log.debug("Connect ORM HL7 Forwarder: the Sending Facility was \"{}\"", sendingFacility)

def hl7Type = get('MSH-9-1')
hl7Type = hl7Type.toUpperCase()
log.debug("Connect ORM HL7 Forwarder: the HL7 Message Type was \"{}\"", hl7Type)

if ("SVR-TSERV0" != sendingFacility) {
    return false
}

if("ORM" != hl7Type){
    return false
}


def encoding = get("MSH-18")
if (encoding == null || encoding.isEmpty()){
    log.info("No character set specified.  Hard-coding to 8859/1")
    set('MSH-18', '8859/1')
}
