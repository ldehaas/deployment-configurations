// Check Character Set.  If not set, explicitly set it to Unicode to support French Characters
 
 def encoding = get("MSH-18")
 if (encoding == null || encoding.isEmpty()){
     log.info("No character set specified.  Hard-coding to 8859/1")
     set('MSH-18', '8859/1')
 }
