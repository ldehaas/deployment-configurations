// Temporarily limiting the converter to Ste-Justine's ORMs

 def encoding = get("MSH-18")
 if (encoding == null || encoding.isEmpty()){
    log.info("No character set specified.  Hard-coding to 8859/1")
    set('MSH-18', '8859/1')
 }

 def messageIssuer = null
 def SteJustineMessage = false 

 messageIssuer = get('MSH-4-1')
    log.info("MSH-4-1 in this message is: {}", messageIssuer)
 if(null != messageIssuer){
    messageIssuer = messageIssuer.toUpperCase()
 
    if('HSJ' == messageIssuer) {
        SteJustineMessage = true
    } else {
        log.info("ORM to ADT Converter: Non Ste-Justine Message.  Ignoring.")
        return false
    }

 } else {
    log.info("ORM to ADT Converter: Unknown Message Issuer.  Ignoring")
    return false
 }



// Logic to Parse Message types and validate if this is an ORM message
 def messageType = null

 messageType = get('MSH-9-1')
    log.info("ORM to ADT Converter: This is an \"{}\" message.", messageType)

 if(null != messageType) {
    messageType = messageType.toUpperCase()
    if('ORM' != messageType) {
        log.info("ORM to ADT Converter: Non-ORM Message.  Ignoring")
        return false
    } else {
        log.info("Converting ORM to ADT")
    }

 } else {
    throw new Exception("Invalid HL7 message without message type!")
 }


// Convert ORM to ADT
def convertedMessageType = 'ADT'
def convertedMessageSubType = 'A31'

set('MSH-9-1', convertedMessageType)
set('MSH-9-2', convertedMessageSubType)
