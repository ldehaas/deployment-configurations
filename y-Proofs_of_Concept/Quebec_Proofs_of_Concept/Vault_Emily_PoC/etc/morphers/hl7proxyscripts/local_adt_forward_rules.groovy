//log.debug("Message as Received from HL7 Feed into Vault's HL7 Proxy: \n", input.toString().replaceAll("\r","\n"))

log.debug("Message as Received from HL7 Feed into Vault's HL7 Proxy: \n", input)

def hl7Type = get('MSH-9-1')
hl7Type = hl7Type.toUpperCase()
log.debug("Local Vault ADT Forwarder: Received MSH-9-1 was: \"{}\"", hl7Type)

if("ADT" != hl7Type){
    return false
}

def encoding = get("MSH-18")
if (encoding == null || encoding.isEmpty()){
    log.info("No character set specified.  Hard-coding to 8859/1")
    set('MSH-18', '8859/1')
}

log.debug("Message as going out to the PIX-handling workflow", output)
//log.debug("Message as going out to the PIX-handling workflow", output.toString().replaceAll("\r","\n"))
