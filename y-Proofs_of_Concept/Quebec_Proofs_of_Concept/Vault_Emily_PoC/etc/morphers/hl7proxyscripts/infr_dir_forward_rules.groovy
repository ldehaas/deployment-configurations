// The DIR should get ADTs and ORMs

/* This DIR entry only references the 10.241.40.232 McKesson DIR Instance.
In order to keep thier logs clean, only Lakeshore Traffic should be sent to this DIR Destination
*/

def sendingApplication = null
def sendingFacility = null
def studyFromLakeShore = false
def studyFromSteJustine = false

// Check Character Set.  If not set, explicitly set it to Unicode to support French Characters
def encoding = get("MSH-18")
if (encoding == null || encoding.isEmpty()){
    log.info("No character set specified.  Hard-coding to 8859/1")
    set('MSH-18', '8859/1')
}  
           
if ("HSJ" == get('MSH-4')) {
    studyFromSteJustine = true
} 

if (studyFromSteJustine){
    log.info("This is HL7 Traffic coming from Ste Justine, passing it through to the 10.241.40.232 McKesson DIR. (This DIR's logs are dedicated for Ste-Justine traffic)")
} else {
    log.info("This is a non-Ste-Justine study. Ignoring it.")
    return false
}

/*def hl7Type = get("MSH-9-1")
hl7Type = hl7Type.toUpperCase()

if('ADT' != hl7Type){
    if('ORM' != hl7Type) {
        return false
    }
}*/
