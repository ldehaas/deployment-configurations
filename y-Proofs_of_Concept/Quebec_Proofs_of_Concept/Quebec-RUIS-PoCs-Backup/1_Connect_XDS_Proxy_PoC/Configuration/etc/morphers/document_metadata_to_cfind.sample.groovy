
set(PatientID, get(XDSPatientID).pid)
set(IssuerOfPatientID, get(XDSPatientID).domainUUID)

set(StudyDate, get(XDSServiceStartTime))
set(StudyTime, get(XDSServiceStartTime))

set(StudyDescription, get(XDSTitle))

// map modality from event codes
// set(Modality, eventCodeToModality[ get(XDSEventCodes)[0] ])

/*
 * get the following from extended metadata
 */
 
set(StudyInstanceUID, get(XDSExtendedMetadata("studyInstanceUid")))
set(RetrieveAETitle, get(XDSExtendedMetadata("retrieveAETitle")))
