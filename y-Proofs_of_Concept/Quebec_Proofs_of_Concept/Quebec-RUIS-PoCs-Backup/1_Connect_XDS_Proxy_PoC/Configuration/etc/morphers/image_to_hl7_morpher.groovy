

LOAD("common.groovy")

/*
//Only works for test case 1.9
    //Now TURNING OFF  for other tests
if(get(Modality) ==  'SR'){
    return false
}*/


initialize('ORM', 'O01', '2.3')
set('MSH-18', '8859/1')

set('PID-3', get(PatientID))

def name = split(get(PatientName)) // Expected Format: lastname^firstname
if (name.length > 0) {
    set('PID-5-1', name[0])
} 
if (name.length > 1) {
    set('PID-5-2', name[1])
}

set('ORC-1', 'NW')
set('ORC-2', get(AccessionNumber))
set('ORC-5', 'SC')

set('OBR-2', get(AccessionNumber))
// Is this needed???  set('OBR-4-1',get())
set('OBR-19', get(AccessionNumber))
set('OBR-27-4', get(StudyDate))
set('OBR-36', get(StudyDate))

output.getMessage().addNonstandardSegment('ZDS')
set('ZDS-1-1', get(StudyInstanceUID))
set('ZDS-1-2', 'RadImage')
set('ZDS-1-3', 'Application')
set('ZDS-1-4', 'DICOM')
