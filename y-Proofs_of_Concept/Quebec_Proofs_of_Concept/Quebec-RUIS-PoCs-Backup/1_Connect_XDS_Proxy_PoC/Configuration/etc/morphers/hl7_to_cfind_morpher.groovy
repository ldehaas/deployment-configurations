
//The below if statement added for Test 2.2.1
if("EATTEN" != get("ORC-16")) {
    return false
}


def pid = get('PID-3-1')
set(PatientID, pid)
//set(IssuerOfPatientID, 'LOCAL')

/*
//Setting the Normalized Anatomic Region for Test 2.2.4
def anatomicRegion = get('OBR-15-4')
set(BodyPartExamined, anatomicRegion)
log.info("Setting Normalized Anatomic Region from HL7 to C-FIND as: {}", anatomicRegion)
*/


/*
// Constraining by modality for test 2.2.3
def modality = get('OBR-24')

log.info("Modality in this incomign ORM is {}", modality) 

if(null != modality){
    set(ModalitiesInStudy, modality)
    log.info("Setting modalities in study to {}", modality)
} else 
{
log.info("No modality specified in this ORM order...")
}
*/

/*
//Constraining by Study Age for case 2.2.5

import org.joda.time.DateTime

def oneYearAgo = new DateTime().minusYears(2)

import org.dcm4che2.data.DateRange
output.getDicomObject().putDateRange(StudyDate, null, new DateRange(oneYearAgo.toDate(), null))

log.info("{}", output.getDicomObject())
log.info("The current Study Age constraint will be from {} onward", oneYearAgo)
*/




