<?xml version="1.0" encoding="UTF-8"?>
<!-- Configuration file for Rialto Connect -->

<config>
    <!-- ####################### -->
    <!--  VARIABLE DECLARATIONS  -->
    <!-- ####################### -->

    <var name="HL7ToCFindMorpher">${rialto.rootdir}/etc/morphers/hl7_to_cfind_morpher.sample.groovy</var>
    <var name="CFindResponseFilter">${rialto.rootdir}/etc/morphers/cfind_response_filter.sample.groovy</var>
    <var name="ImageToHL7OrderMorpher">${rialto.rootdir}/etc/morphers/image_to_hl7_order_morpher.sample.groovy</var>
    <var name="ForeignImageMorpher">${rialto.rootdir}/etc/morphers/foreign_image_morpher.sample.groovy</var>

    <var name="AdHocCFindRequestMorpher">${rialto.rootdir}/etc/morphers/adhoc_cfind_req_morpher.sample.groovy</var>
    <var name="AdHocCFindResponseMorpher">${rialto.rootdir}/etc/morphers/adhoc_cfind_resp_morpher.sample.groovy</var>
    <var name="AdHocCMoveRequestMorpher">${rialto.rootdir}/etc/morphers/adhoc_cmove_req_morpher.sample.groovy</var>

    <var name="AuditRecordRepositoryPort">4000</var>
    <var name="AuditRecordRepositoryAPIPort">7070</var>
    <var name="WebUIPort">8080</var>

    <include location="local/cluster.xml" />

    <!-- ####################### -->
    <!--         DEVICES         -->
    <!-- ####################### -->

    <!-- Self-reference to the Rialto DICOM Server -->
    <device type="dicom" id="RIALTO">
        <host>localhost</host>
        <port>4109</port> <!-- make this a variable x -->
        <aetitle>RIALTO</aetitle>
    </device>

    <!-- Defines host and port for the DIR -->
    <device type="dicom" id="DIR">
        <host>10.0.1.63</host>
        <port>11112</port>
        <aetitle>DCM4CHEE</aetitle>
    </device>

    <!-- Defines host and port for the local PACS -->
    <device type="dicom" id="PACS">
        <aetitle>PACS</aetitle>
        <host>localhost</host>
        <port>11112</port>
    </device>

    <!-- Defines host and port for the RIS, or the HL7 interface on the PACS -->
    <device type="hl7v2" id="PACSHL7">
        <host>localhost</host>
        <port>4110</port>
        <aetitle>PACSHL7</aetitle>
    </device>

    <!-- Defines host and port for Audit Records -->
    <device type="audit" id="audit">
        <host>localhost</host>
        <port>${AuditRecordRepositoryPort}</port>
    </device>

    <!-- ####################### -->
    <!--         SERVERS         -->
    <!-- ####################### -->

    <!-- Specify port used to listen for DICOM C-Find and C-Move -->
    <server id="dicomserver" type="dicom">
        <port>4109</port> <!-- var x -->
    </server>

    <!-- Specify port used to listen for HL7 orders for prefetch -->    
    <server id="hl7srv" type="hl7v2">
        <port>4106</port>
    </server>

    <!-- Specify where to store transaction records -->
    <service id="txn-records" type="txn-records">
        <config>
            <prop name="StorageDir">${rialto.rootdir}/var/txn-records</prop>
        </config>
    </service>

    <!-- ####################### -->
    <!--  SERVICE CONFIGURATION  -->
    <!-- ####################### -->

    <!-- Connect DICOM Service -->
    <service id="connect" type="connect-dicom">
        <device idref="PACS" name="PACS" />
        <device idref="PACSHL7" name="PACSHL7" />
        <device idref="DIR" name="DIR" />

        <server idref="dicomserver" name="cstore" />
        <server idref="dicomserver" name="cfind" />
        <server idref="dicomserver" name="cmove" />
        <server idref="hl7srv" />

        <config>
            <prop name="HL7ToCFindMorpher">${HL7ToCFindMorpher}</prop>
            <prop name="CFindResponseFilter">${CFindResponseFilter}</prop>
            <prop name="ImageToHL7OrderMorpher">${ImageToHL7OrderMorpher}</prop>
            <prop name="ForeignImageMorpher">${ForeignImageMorpher}</prop>

            <prop name="AdHocCFindRequestMorpher">${AdHocCFindRequestMorpher}</prop>
            <prop name="AdHocCFindResponseMorpher">${AdHocCFindResponseMorpher}</prop>
            <prop name="AdHocCMoveRequestMorpher">${AdHocCMoveRequestMorpher}</prop>
        </config>
    </service>

    <!-- Audit Record Repository Service -->
    <server type="http" id="auditRecordRepository">
        <port>${AuditRecordRepositoryAPIPort}</port>
    </server>
    <service id="arr" type="arr">
        <server idref="auditRecordRepository" name="arr-api">
            <url>/auditRecords/*</url>
        </server>
        <server idref="auditRecordRepository" name="arr-index-backup-api">
            <url>/auditRecordsIndexBackup/*</url>
        </server>

        <config>
            <prop name="IndexDir" value="${rialto.rootdir}/var/arr/index" />
            <prop name="IndexBackupDir" value="${rialto.rootdir}/var/arr/index-backup" />
            <prop name="ArchiveDir" value="${rialto.rootdir}/var/arr/archive" />
            <prop name="TempDir" value="${rialto.rootdir}/var/arr/temp" />
            <prop name="Listener" value="udp:${AuditRecordRepositoryPort}" />
            <prop name="ForwardDestination" value="${ClusterPartnerHost}:${AuditRecordRepositoryPort}" />
            <prop name="ForwarderCheckpointFolder" value="${rialto.rootdir}/var/arr/checkpoints" />
        </config>
    </service>

    <!-- Rialto Navigator GUI -->
    <!-- Off by default; may be useful for debugging
    <server type="http" id="navigator-http" default="true">
        <port>${WebUIPort}</port>
    </server>

    <service id="navigator" type="navigator">
        <device idref="RIALTO" name="proxyDIR" />

        <server idref="navigator-http" name="navigator-war">
            <url>/</url>
        </server>

        <config>
            <prop name="ArrURL">http://localhost:${AuditRecordRepositoryAPIPort}</prop>
            <prop name="ReportURL">http://10.0.1.210:8000/samplehtml.html/?patient_id={0}&amp;issuer_of_patient_id=KINGSTON&amp;accession_number={1}</prop>
        </config>
    </service>
    -->
</config>
