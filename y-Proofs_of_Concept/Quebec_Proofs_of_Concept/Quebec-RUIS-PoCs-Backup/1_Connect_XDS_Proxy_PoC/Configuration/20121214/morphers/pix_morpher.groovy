log.debug("PIX Patient Creation: controlling what's in MSH-4. {}", get('MSH-4'))

def SteJustineMessage = false
def LakeshoreMessage = false

def issuerOfPID = get('MSH-4')

if("HSJ" == issuerOfPID) {
    set('PID-3-4-2', '2.16.124.10.101.1.60.1.1004.1')
    SteJustineMessage = true
    log.debug("The PIX is assuming this Patient is from Ste-Justine RIS' Production Feed")

} else if ("LKSHR" == issuerOfPID){

  // The MSH-4 at the Lakeshore Production feed is regularly empty.  This has to be set from the HD Proxy @ Lakeshore

    set('PID-3-4-2', '2.16.124.10.101.1.60.1.1007.1')
    LakeshoreMessage = true
    log.info("The PIX is assuming this Patient is from Lakeshore, with the MSH-4 modified at Lakeshore's HD Connect Proxy")

} else if( null == issuerOfPID || issuerOfPID.isEmpty() ) {
    log.warn("This ADT has *NO* Explicit User of Patient ID. Aborting")
    throw new Exception("This ADT has *NO* Explicit Issuer of Patient. Aborting")
} 

// Logic to deal with ADTs that don't have a local MRN
def mrn = get('PID-3-1')
if(null == mrn){
    log.warn("This ADT has *NO* local MRN. Aborting")
    throw new Exception("This ADT has *NO* local MRN. Aborting")
}


// Logic to deal with sometimes incomplete Ste-Justine messages
    if(SteJustineMessage){
        def RAMQ_ID = get('PID-2-1') 
        if( null == RAMQ_ID || RAMQ_ID.isEmpty() ) {
            log.warn("This Ste-Justine ADT Message has *NO* RAMQ Identifier. Aborting")
            throw new Exception("This Ste-Justine ADT Message has *NO* RAMQ Identifier. Aborting")
        }
    }

   
// Logic to deal with RAMQ issuer of PID that is almost never populated   
def RAMQ_IssuerOfPID = get('PID-2-4-2') 
if(null == RAMQ_IssuerOfPID || RAMQ_IssuerOfPID.isEmpty() ) {
    log.debug("RAMQ Issuer of PID (Affinity Domain) is *blank*.  Hardcoding it.")
    set('PID-2-4-2', '2.16.124.10.101.1.60.100')
}
