//cfind_to_find_documents.groovy
// this script converts dicom cfind request to xds find documents query
// this is a starting point, demonstrating some features of this script

// Printing RAW DICOM C-Find Message received from PACS
log.info("Printing RAW DICOM C-Find Message received from PACS:\n {}", input)


// obtaining properties from cfind
def pid = get(PatientID)
 

if(null == pid) {
    throw new Exception("DICOM C-FIND to XDS \"Find Documents\" Query Conversion:\n Patient ID in incoming C-Find was NULL.  Need a local Patient ID to work.\n")
} else {
    // handle variations in cfinds
    pid = pid.toUpperCase()
}

def callingAE = getCallingAETitle()
    log.info("Ad-Hoc C-Find: Calling AETitle initiating C-Find was \"{}\"", callingAE)
    callingAE = callingAE.trim()
    callingAE = callingAE.toUpperCase()

def issuer = null


if('ALI_SCU' == callingAE){
    // Explicitly set the Issuer of PID for PIX lookup to work.  This is for when then PACS does an Ad-Hoc Query
    issuer = '2.16.124.10.101.1.60.1.1007.1'
} else if ('PROXY_SELF' == callingAE) {
  // This case is for Fetch Prior Studies: when Rialto Connect calls itself
    log.info("DICOM C-Find to XDS \"Find Documents\" Query Conversion: This is a Fetch Prior Study, attempting to Read IssuerOfPatientID")
    issuer = get(IssuerOfPatientID)
    log.info("DICOM C-Find to XDS \"Find Documents\" Query Conversion: IssuerOfPatientID is {}", issuer)
}

// setting properties in find documents query
if(null != pid && null != issuer) {
    set(XDSPatientID, pid, issuer)
} else {
    throw new Exception("DICOM C-Find to XDS \"Find Documents\" Query Conversion:  either Patient ID or Issuer of Patient ID are NULL.")
}


// constraint by study date, if specified in cfind
dateRange = get(StudyDate)
set(XDSServiceStartTime, dateRange)



// work around a temporary bug in java code which sets wrong format code:
//set(XDSFormatCode, code("urn:ihe:rad:1.2.840.10008.5.1.4.1.1.88.59", "1.2.840.10008.2.6.1"))


/* Test-specific Functionality */

/*
//This part of the script sets the XDS Query for Event Code based on DICOM Modality

modalitiesInStudy = get(ModalitiesInStudy)

if (modalitiesInStudy != null) {
    set(XDSEventCode, code(modalitiesInStudy, "DCM"))
    }else{
        log.debug("DICOM C-FIND did not contain modalities in study")
        }
*/


//This part of the script is for test-specific use cases, if not in use, should be commented out


/*
 // Setting the Normalized Anatomic Region for Test 2.2.4
def bodyPart = get(BodyPartExamined)
if(null != bodyPart){
    set(XDSEventCode, code(bodyPart, 'Imagerie Québec-DSQ'))
    log.info("Setting Normalized Anatomic Region {} to Imagerie Québec-DSQ", bodyPart)
}
*/
