/**
 * This script creates an ORU report message from a DICOM Structured Report.
 * 
 * It is optional.  If not configured, SRs will be passed directly to the PACS.
 * If configured, the SRs will not be delivered, but the generated ORU will be.
 * 
 * The input Structured Report will have already been localized by the
 * foreign image localizer script.
 * 
 * This script can return false to signal that the SR should be dropped.
 */

initialize("ORU", "R01", "2.3")

// Setting Character Set supported by HMI PACS
set('MSH-18', '8859/1')


/*
// Adding localization of PatientID for the ORU - Rodrigo
def sourcePID = get(PatientID)
def sourceIssuer = get(IssuerOfPatientID)

def localPid = Pids.localize([sourcePid, sourceIssuer], getAllPidsQualified())

set(PatientID, localPid[0])

sourceIssuerToPrefix = [
    "1000016640": "CAB",
    "1000017127": "LUC",
    "1000021780": "ENF",
    "6000000031": "HMR",
    "1000017259": "STM",
    "1000016855": "SDL",
    "1000021780":"HIDJ",
    "1000015949":"CHUL",
    "1000019966":"HDDQ",
    "1.2.3.4.5.1": ""
                        ]

def prefix = sourceIssuerToPrefix[sourceIssuer]
*/

// TODO: we probably actually need to dig through OtherPatientIDsSequence for the ramq
set("PID-3", get(PatientID))
// TODO: find the local pid for PID-3

// Splitting  name and using sub-components of PID-5 - Rodrigo

def name = split(get(PatientName)) // Expected Format: lastname^firstname
if (name.length > 0) {
    set('PID-5-1', name[0])
}
if (name.length > 1) {
    set('PID-5-2', name[1])
}



set("PID-7", get(PatientBirthDate))
set("PID-8", get(PatientSex))

set("ORC-1", "RE")
set("ORC-3", get(AccessionNumber))

set("OBR-1", "1")
set("OBR-3", get(AccessionNumber))
set("OBR-4", get(StudyDescription))
//set("OBR-6", requested date time ??)
set("OBR-7", get(StudyDate) + get(StudyTime))

anatomicRegionSeq = get(AnatomicRegionSequence)
if (!anatomicRegionSeq.isEmpty()) {
    anatomicRegion = anatomicRegionSeq.first()
    set("OBR-15-1-1", anatomicRegion.get(CodeValue))
    set("OBR-15-1-2", anatomicRegion.get(CodeMeaning))
    set("OBR-15-1-3", anatomicRegion.get(CodingSchemeDesignator))
}

// TODO: split name and use subcomponents of OBR-16, starting at OBR-16-2
//set("OBR-16", get(ReferringPhysicianName))

set("OBR-19", get(AccessionNumber))
set("OBR-20", get(StudyID))
set("OBR-22", get(ContentDate) + get(ContentTime))
set("OBR-24", get(Modality))

completionFlag = null
performedProcedureSeq = get(PerformedProcedureCodeSequence)
if (!performedProcedureSeq.isEmpty()) {
    performedProcedure = performedProcedureSeq.first()
    completionFlag = performedProcedure.get(CompletionFlag)
}

set("OBR-25", completionFlag)

verifyingObserverName = null
verifyingObserverSeq = get(VerifyingObserverSequence)
if (!verifyingObserverSeq.isEmpty()) {
    verifyingObserver = verifyingObserverSeq.first()
    verifyingObserverName = verifyingObserver.get(VerifyingObserverName)
}

// TODO: split name and use subcomponents of OBR-32-1, starting at OBR-32-1-2
//set("OBR-32-1", verifyingObserver.get(VerifyingObserverName))

def gatherTextContent(contentSeq, output) {
    contentSeq.each { content ->
        if ("TEXT".equals(content.get(ValueType))) {
            meaning = null
            conceptNameCodeSeq = content.get(ConceptNameCodeSequence)
            if (!conceptNameCodeSeq.isEmpty()) {
                meaning = conceptNameCodeSeq.first().get(CodeMeaning)
            }
            
            output.add([meaning, content.get(TextValue)])
            
        } else if ("CONTAINER".equals(content.get(ValueType))) {
            gatherTextContent(content.get(ContentSequence), output)
        }
    }
}

reportTextItems = []
gatherTextContent(get(ContentSequence), reportTextItems)

set("OBX-1", "1")
set("OBX-2", "TX")
textRepetition = 0
reportTextItems.each { reportTextItem ->
    title = reportTextItem[0] == null ? "___" : reportTextItem[0].toUpperCase()
    text = reportTextItem[1]
    set("OBX-5(" + textRepetition++ + ")", title)
    // blank line after title
    set("OBX-5(" + textRepetition++ + ")", "")
    set("OBX-5(" + textRepetition++ + ")", text)
    // 2 blank lines after each section
    set("OBX-5(" + textRepetition++ + ")", "")
    set("OBX-5(" + textRepetition++ + ")", "")
}
set("OBX-11", completionFlag)
set("OBX-14", get(ContentDate) + get(ContentTime))
// TODO: split name and use subcomponents of OBX-16, starting at OBX-16-2
//set("OBX-16", verifyingObserverName)
