class Constants {
    static final LocalIssuerIDs = ["2.16.124.10.101.1.60.1.1007.1"]
    static final PrimaryLocalIssuerID = LocalIssuerIDs[0]
}

class Pids {
    /**
     * this is a commonly needed localize function. it looks through all the 
     * issuers of patient ids, and check if they are local accordingly to 
     * Constants.LocalIssuers. It would either return the local patient id if
     * there is one, or prefix the source patient id to generate a new local
     * patient id.
     * @return a two element array containing the localized pid and issuer
     * @param qualifiedSourcePid a two element array containing source
     *      pid and source issuer
     * @param qualifiedOtherPids an array of qualified pids
     */
    static localize(qualifiedSourcePid, qualifiedOtherPids) {
        def sourcePid = qualifiedSourcePid[0]
        def sourceIssuer = qualifiedSourcePid[1]
    
        if ( Constants.LocalIssuerIDs.contains(sourceIssuer) ) {
            // this image originated from the local PACS
            return qualifiedSourcePid;
        }
    
        def foundPid = null
        for (def pid : qualifiedOtherPids) {
            if ( Constants.LocalIssuerIDs.contains(pid[1]) ) {
                foundPid = pid
                break
            }
        }
    
        // no linked pid found, create one by prefixing
        if (foundPid == null) {
            foundPid = [ sourceIssuer+"_"+sourcePid, Constants.PrimaryLocalIssuerID]
        }
        
        return foundPid
    }
}


class ProcedureCodes {
    static localize(modality, rawText) {
        if (rawText == null || rawText.trim().isEmpty()) {
            return null
        }
        
        if (modality == null) {
            modality = ""
            // still try to do the matching without modality
        }
        
        modality = modality.toUpperCase()
        def text = rawText.toLowerCase() 
        
        // example of regular expression
        if (modality == "CT" && text =~ /chest/ ) {
            return 'CT_Chest'
        }
        
        // example of a map
        def ret = lookup.get( [modality, text] )
        if (ret != null) {
            return ret
        }
        
        // can't localize, let the caller decide
        return null
    }
    
    private static final lookup = [
        ['CT', 'abdomen']:'CT_Abdomen',
        ['CT', 'abdn']:'CT_Abdomen',
    ]
}
