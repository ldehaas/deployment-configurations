/* HL7 to CFind Morpher
*
*   This script is part of the 'Fetch Prior Studies' workflow.  It takes the incoming HL7 ORM order, and translates it into a DICOM C-Find.
*   Later on in the workflow, the DICOM C-Find that this script outputs is further processed by Connect, and transformed into and 
*   XDS Find Documents query by using the 'CFind to Find Documents Morpher'.
*
*   If you want to filter results for --ONLY-- for the Fetch Prior Studies workflow (without affecting Ad-Hoc Query / Retrieve),
*   populate the 'CFind Response Filter', which only affects results for Fetch Prior Studies originally derived from this current, below morpher.
*   (HL7 to CFind Morpher).
*
*   The high level workflow in this script, currently is:
*       1. Populate Local Affinity Domains (Missing in ORM feed)
*
*/

//The below if statement added for Test 2.2.1
if("EATTEN" != get("ORC-16")) {
    return false
}


def pid = get('PID-3-1')
set(PatientID, pid)
//set(IssuerOfPatientID, 'LOCAL')

// This is to set the local Affinity Domain because it is missing in their ADT Feed
if ("SHSJPACSTST2" == get('MSH-4')){
    set(IssuerOfPatientID, '2.16.124.10.101.1.60.1.1004.1')
    log.info("Fetch Prior Studies: The PIX is assuming this Patient is from Ste-Justine")
} else if ("SVR-TSERV0" == get('MSH-4')) {
    set(IssuerOfPatientID, '2.16.124.10.101.1.60.1.1007.1')
    log.info("Fetch Prior Studies: The PIX is assuming this Patient is from Lakeshore")
} else if (null==get('PID-3-4-2')) {
    set(IssuerOfPatientID, '2.16.124.10.101.1.60.1.1007.1')
    // This is for Lakeshore
    log.warn("Fetch Prior Studies: The MSH-4 in this HL7 Message was blank!\nThe PIX is assuming this Patient is from Lakeshore")
}

log.info("Fetch Prior Studies:  The is the Current C-Find being Generated: \n {}", output)

/*
//Setting the Normalized Anatomic Region for Test 2.2.4
def anatomicRegion = get('OBR-15-4')
set(BodyPartExamined, anatomicRegion)
log.info("Setting Normalized Anatomic Region from HL7 to C-FIND as: {}", anatomicRegion)
*/


/*
// Constraining by modality for test 2.2.3
def modality = get('OBR-24')

log.info("Modality in this incomign ORM is {}", modality) 

if(null != modality){
    set(ModalitiesInStudy, modality)
    log.info("Setting modalities in study to {}", modality)
} else 
{
log.info("No modality specified in this ORM order...")
}
*/

/*
//Constraining by Study Age for case 2.2.5

import org.joda.time.DateTime

def oneYearAgo = new DateTime().minusYears(2)

import org.dcm4che2.data.DateRange
output.getDicomObject().putDateRange(StudyDate, null, new DateRange(oneYearAgo.toDate(), null))

log.info("{}", output.getDicomObject())
log.info("The current Study Age constraint will be from {} onward", oneYearAgo)
*/




