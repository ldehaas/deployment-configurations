/**
 * This script creates an ORU report message from a DICOM Structured Report.
 * 
 * It is optional.  If not configured, SRs will be passed directly to the PACS.
 * If configured, the SRs will not be delivered, but the generated ORU will be.
 * 
 * The input Structured Report will have already been localized by the
 * foreign image localizer script.
 * 
 * This script can return false to signal that the SR should be dropped.
 */

LOAD("common.groovy")

// Explicitly setting the Char Set for ORUs that don't have them, assuming ISO IR 100
if (input.get(SpecificCharacterSet) == null) {
    input.set(SpecificCharacterSet, "ISO_IR 100")
   }



initialize("ORU", "R01", "2.3")

// Setting Character Set supported by HMI PACS
set('MSH-18', '8859/1')

// Adding localization of PatientID for the ORU - Rodrigo
def sourcePID = get(PatientID)
def sourceIssuer = get(IssuerOfPatientID)

def localPid = Pids.localize([sourcePID, sourceIssuer], getAllPidsQualified())

if(null != sourcePID) {
    set('PID-3', sourcePID) 
} else {
    throw new Exception("The Patient Localization failed for ORU Creation!!  The localized Patient is null")
}

log.info("Checking the Patient Number with old method: {}", localPid[0])
log.info("Patient with the new method: {}", get(PatientID))

sourceIssuerToPrefix = [
    "1000016640": "CAB",
    "1000017127": "LUC",
    "1000021780": "ENF",
    "6000000031": "HMR",
    "1000017259": "STM",
    "1000016855": "SDL",
    "1000021780":"HIDJ",
    "1000015949":"CHUL",
    "1000019966":"HDDQ",
    "1.2.3.4.5.1": ""
                        ]

def prefix = sourceIssuerToPrefix[sourceIssuer]

// Setting Additional PID fields needed
set("PID-1", "1")
// RAMQ would be here set("PID-2", "RAMQPLACEHOLDER")
//set("PID-3", get(PatientID))
// TODO: find the local pid for PID-3
/*
if(null != prefix) {
  // Patient is foreign
    set('PID-3', localPidi[0]) 
} else {
  // Patient is local
}
*/

/**
 * Copies up to three components of a name from the SR into the hl7 message.
 * DICOM person name elements are supposed to be of the form
 * family^given^middle^prefix^suffix.  HL7 names are sometimes of the form
 * family^given^middle^suffix^prefix but some components are actually 
 * id^family^given^...
 * 
 * In the case where the id is there, pass startingComponent=2 to offset where
 * we put the name.  However, in our sample SRs, the values of the person names
 * actually include the id (they are in DICOM formath instead of HL7).  In this
 * case, just leave the default startingComponent.
 * 
 * @param dicomTag place to get name from in SR
 * @param hl7prefix place to put name in hl7
 * @param startingComponent if name doesn't line up at beginning of 
 * @return
 */
def setName(dicomTag, hl7prefix, startingComponent=1) {
    nameParts = split(dicomTag)
    if (null == nameParts) {
       return
    }

    // loop far enough to get the id if present, plus the family and given names
    int maxPartsToCopy = 3
    
    for (int i = 0; i < maxPartsToCopy && i < nameParts.size(); i++) {
        set(hl7prefix + "-" + (startingComponent++), nameParts[i])
    }
}

setName(PatientName, "PID-5")

// Set Mother's Maiden Name, if present in the DICOM SR
set("PID-6", get(PatientMotherBirthName))

set("PID-7", get(PatientBirthDate))
set("PID-8", get(PatientSex))
set("PID-11", get(PatientAddress))
set("PID-13", get(PatientTelephoneNumbers))





set("ORC-1", "RE")
set("ORC-3", get(AccessionNumber))

set("OBR-1", "1")
set("OBR-3", get(AccessionNumber))
set("OBR-4", get(StudyDescription))
//set("OBR-6", requested date time ??)

if((null != get(StudyDate)) && (null != get(StudyTime))) {
    set("OBR-7", get(StudyDate) + get(StudyTime))
}

anatomicRegionSeq = get(AnatomicRegionSequence)
if (!anatomicRegionSeq.isEmpty()) {
    anatomicRegion = anatomicRegionSeq.first()
    set("OBR-15-1-1", anatomicRegion.get(CodeValue))
    set("OBR-15-1-2", anatomicRegion.get(CodeMeaning))
    set("OBR-15-1-3", anatomicRegion.get(CodingSchemeDesignator))
}

// our sample SR includes the physician id (non-standard)
setName(ReferringPhysicianName, "OBR-16")

set("OBR-19", get(AccessionNumber))
set("OBR-20", get(StudyID))
set("OBR-22", get(ContentDate) + get(ContentTime))
set("OBR-24", get(Modality))

completionFlag = get(CompletionFlag)
//performedProcedureSeq = get(PerformedProcedureCodeSequence)
//if (!performedProcedureSeq.isEmpty()) {
    //performedProcedure = performedProcedureSeq.first()
    //completionFlag = performedProcedure.get(CompletionFlag)
//}

set("OBR-25", completionFlag)

/* Leaving Jeremy's code for reference sake - Rodrigo
verifyingObserverName = null
verifyingObserverSeq = get(VerifyingObserverSequence)
if (!verifyingObserverSeq.isEmpty()) {
    verifyingObserver = verifyingObserverSeq.first()
    verifyingObserverName = verifyingObserver.get(VerifyingObserverName)
}

SetName(verifyingObserver.get(VerifyingObserverName), "OBR-33-1", startingComponent=2)
// TODO: sample has verifying observer id buried in another sequence, could grab it
*/

// Setting the Study Date to populate the Scheduled date / time - Same as the ORM - if not present use content date/time
def timeBack = null
if((null != get(StudyDate)) && (null != get(StudyTime))) {
    timeBack =  get(StudyDate) + get(StudyTime)
    set("OBR-27-4", timeBack)
} else if ((null != get(ContentDate)) && (null !=get (ContentTime))) {
    timeBack = get(ContentDate) + get(ContentTime)
    set("OBR-27-4",timeBack)
}

log.info("SR to ORU Workflow: Setting the OBR-27-4 TO {}", get(StudyDate))

//Setting Verifying Observer ID and Names (Lastname,Firstname)into OBR-33
def verifyingObserverName = null
def vObsIdCode = null

def verifyingObserverSeq = get(VerifyingObserverSequence)
if (! verifyingObserverSeq.isEmpty() ) {
    def verifyingObserver = verifyingObserverSeq.first()
    verifyingObserverName = verifyingObserver.get(VerifyingObserverName)

    def verifyingObserverIDCodeSeq = verifyingObserver.get(VerifyingObserverIdentificationCodeSequence)
    if(!verifyingObserverIDCodeSeq.isEmpty()) {
        def vOIDSeq = verifyingObserverIDCodeSeq.first()
        vObsIdCode = vOIDSeq.get(CodeValue)
    }
}

//Setting Verifying Observer ID
if(null != vObsIdCode) {
    set("OBR-32-1", vObsIdCode)
}

//Setting Verifying Observer Name (Lakeshore specific format)
 def verifyingObserverNameArray = null
 def useNameAsPassed = false
 def finalverifyingObserverName = null

 if(! verifyingObserverName.isEmpty() ) {
    log.info("\n\n Checking what the Verifying Observer Name is \"{}\"", verifyingObserverName)
    
    if(verifyingObserverName.contains('^')) {  
        verifyingObserverNameArray = verifyingObserverName.split('\\^')
    } else if(verifyingObserverName.contains(',')) {
        verifyingObserverNameArray = verifyingObserverName.split(',')
    } else {
        log.info("Verifying Observer Name does not contain a caret (^) nor a comma (,)")
        useNameAsPassed = true
    }

    if(null != verifyingObserverNameArray ) {
        def arrayLength = verifyingObserverNameArray.length()
        log.info("Verifying Observer Array length is \"{}\"", arrayLength)

        if(2 >= arrayLength) {
            log.info("Length of Verifying Observer Name is 2 strings")
            finalverifyingObserverName = verifyingObserverName[0]+","+verifyingObserverName[1]
        } else if (1 >= arrayLength) {
            log.info("Length of Verifying Observer Name is 1 string")
            finalverifyingObserverName = verifyingObserverName[0]+","
        } else {
            log.info("Sanity Check: Length of Verifying Observer Name Array is less than 1")
            finalverifyingObserverName = null
        }

    } else if(useNameAsPassed) {
        finalverifyingObserverName = verifyingObserverName
        log.info("Non-delimited name passed.  Using original Verifying Observer Name in OBR-32-2")

    } else {
        log.info("Length of Verifying Observer Name is NULL")
        finalverifyingObserverName = null
    }

    set("OBR-32-2", finalverifyingObserverName)
}

def gatherTextContent(contentSeq, output) {
    contentSeq.each { content ->
        if ("TEXT".equals(content.get(ValueType))) {
            meaning = null
            conceptNameCodeSeq = content.get(ConceptNameCodeSequence)
            if (!conceptNameCodeSeq.isEmpty()) {
                meaning = conceptNameCodeSeq.first().get(CodeMeaning)
            }
            
            output.add([meaning, content.get(TextValue)])
            
        } else if ("CONTAINER".equals(content.get(ValueType))) {
            gatherTextContent(content.get(ContentSequence), output)
        }
    }
}

reportTextItems = []
gatherTextContent(get(ContentSequence), reportTextItems)

set("OBX-1", "1")
set("OBX-2", "TX")
textRepetition = 0
reportTextItems.each { reportTextItem ->
    title = reportTextItem[0] == null ? "___" : reportTextItem[0].toUpperCase()
    text = reportTextItem[1]
    set("OBX-5(" + textRepetition++ + ")", title)
    // blank line after title
    set("OBX-5(" + textRepetition++ + ")", "")
    set("OBX-5(" + textRepetition++ + ")", text)
    // 2 blank lines after each section
    set("OBX-5(" + textRepetition++ + ")", "")
    set("OBX-5(" + textRepetition++ + ")", "")
}

def verificationFlag = get(VerificationFlag)
if("VERIFIED" == verificationFlag) {
    verificationFlag = "F"
} else if("UNVERIFIED" == verificationFlag) {
    verificationFlag = "P"
}

set("OBX-11", verificationFlag)
log.info("DICOM SR to HL7 ORU Report creation: Setting Completion Flag in OBX-11 as {}", completionFlag)


set("OBX-14", get(ContentDate) + get(ContentTime))
// TODO: split name and use subcomponents of OBX-16, starting at OBX-16-2
//set("OBX-16", verifyingObserverName)

output.getMessage().addNonstandardSegment('ZDS')
set('ZDS-1-1', get(StudyInstanceUID))
set('ZDS-1-2', 'RadImage')
set('ZDS-1-3', 'Application')
set('ZDS-1-4', 'DICOM')

log.info("This is the Report that will get sent: \n {}", output)
