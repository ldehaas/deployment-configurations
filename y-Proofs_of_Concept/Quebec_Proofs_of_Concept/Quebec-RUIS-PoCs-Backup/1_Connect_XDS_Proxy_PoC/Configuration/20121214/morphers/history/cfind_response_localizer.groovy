LOAD("common.groovy")

/*  TURNING ON MODALITIES IN STUDY PASSING FROM REGISTRY */
// get the list of event codes and then get their code values as a list of strings:
modalities = xdsmetadata.get(XDSEventCodes).collect { 
    eventCode -> eventCode.getCodeValue()
}

// set that list as the modalities in the response:
set(ModalitiesInStudy, modalities)


def sourcePid = get(PatientID)
log.info("Source pid in cfind response before morphing: {}", sourcePid)
def sourceIssuer = get(IssuerOfPatientID)

if( "1.2.3.4.5.1" == sourceIssuer){
    log.info("The Study Instance UID for this study was: {}", get(StudyInstanceUID))
    log.info("The IssuerOfPatientID received from XDS Registry was: {}.  This patient is considered LOCAL", sourceIssuer )
    //return false
} else{
    log.info("The Study Instance UID for this study was: {}", get(StudyInstanceUID))
    log.info("Acceptance Test 2.2.9: IssuerOfPatientID in this Registry response was {}.  Deemed a FOREIGN study", sourceIssuer)
}



// Test-specific morphing
    //if("1.2.3.4.5.1" == sourceIssuer){
        //return false
    //}

//def patientSex = get(PatientSex)
//log.info("For Test 2.1.5.3: Source PatientSex before morphing: {}", patientSex)

//if(patientSex == "M"){
//    patientSex = "F"
//} else {
//    patientSex = "M"
//}

//set(PatientSex, patientSex)
//log.info("For Test 2.1.5.3: Morphed  PatientSex: {}", patientSex)
// End test-specific morphing

localPid = Pids.localize([sourcePid, sourceIssuer], getAllPidsQualified())
log.info("Setting local pid in cfind response: {}", localPid)

set(PatientID, localPid[0])

log.info("For test 2.1.5.3: Removing Other Patient IDs Sequence")
remove(IssuerOfPatientID)
remove(OtherPatientIDsSequence)

sourceIssuerToPrefix = [
    "1000016640": "CAB",
    "1000017127": "LUC",
    "1000021780": "ENF",
    "6000000031": "HMR",
    "1000017259": "STM",
    "1000016855": "SDL",
    "1000021780":"HIDJ",
    "1000015949":"CHUL",
    "1000019966":"HDDQ",
    "1.2.3.4.5.1": ""
]
prefix = sourceIssuerToPrefix[sourceIssuer]

if (prefix == null) {
    prefix = "XX"
}

prepend(AccessionNumber, prefix)

set(RetrieveAETitle, "RIALTO_TEST")

log.info("CFind response after morphing:\n{}", input)


