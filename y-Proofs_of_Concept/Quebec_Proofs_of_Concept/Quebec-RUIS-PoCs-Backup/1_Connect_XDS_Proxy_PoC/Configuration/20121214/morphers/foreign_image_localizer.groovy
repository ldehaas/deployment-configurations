LOAD("common.groovy")

log.info("\n\n\n*** Starting Foreign Image LOCALIZER Script *** :\n")

def sourcePid = get(PatientID)
log.info("Source pid in cfind response before morphing: {}", sourcePid)
def sourceIssuer = get(IssuerOfPatientID)

localPid = Pids.localize([sourcePid, sourceIssuer], getAllPidsQualified())
log.info("Setting local pid in CStore response: {}", localPid[0])


// Test-specific morphing
//def patientSex = get(PatientSex)

//if (patientSex == "F"){
//    set(PatientSex, "M")
//}else {
//    set(PatientSex, "F")
//}


//set(0x09030010, 'GEIISPACS', VR.LO)
//set(0x09031010, '1', VR.LO)



// End Test-specific morphing


// Filtering out KOS objects, but leaving other objects from the SOP Class 1.2.840.10008.5.1.4.1.1.88.59 (i.e. KIN objects)
if("113030" == get("ConceptNameCodeSequence/CodeValue")) {
    return false
}


set(PatientID, localPid[0])
remove(IssuerOfPatientID)
remove(OtherPatientIDsSequence)

sourceIssuerToPrefix = [
    "2.16.124.10.101.1.60.1.1004.1":"SJ",
    "1000016640": "CAB",
    "1000017127": "LUC",
    "1000021780": "ENF",
    "6000000031": "HMR",
    "1000017259": "STM",
    "1000016855": "SDL",
    "1000021780":"HIDJ",
    "1000015949":"CHUL",
    "1000019966":"HDDQ",
    "1.2.3.4.5.1": ""
]

prefix = sourceIssuerToPrefix[sourceIssuer]

prepend(AccessionNumber, prefix)



// Log checking
log.info("CStore after morphing:\n{}", input)

log.info("\n\n\n*** Ending Foreign Image LOCALIZER Script *** :\n")

