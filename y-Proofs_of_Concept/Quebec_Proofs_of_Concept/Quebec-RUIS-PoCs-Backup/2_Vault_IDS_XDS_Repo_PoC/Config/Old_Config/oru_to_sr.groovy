/**
 * converts HL7 ORU to a DICOM SR object
 * draft according to the SAD v0.5
 * update by Rodrigo Tue 12 Jun 2012, based on SAD v0.8 - oru_to_sr v.0.2
 *
 * many of the HL7 locations are potentially different in the field. this 
 * script is simply a starting point.
 */

import org.dcm4che2.util.UIDUtils
import org.joda.time.DateTime


 // Declare commonly used variables
 	
 	// This instance of Vault's unique ID to use it as the ID of the creator of SR
 	def specific_vault_instance = '1.2.3.4.5.6.7.8.9'

 	// This is the Accession number, used in the script multiple times
 	/* The SAD specifies that it should be OBR-18.  However, in all the orders we generate in Connect, we put them in OBR-19.  */
 	def acc_num = get('OBR-19')



set(SpecificCharacterSet, 'ISO_IR_100')	
set(InstanceCreationDate, new DateTime())
set(InstanceCreationDate, new DateTime())
set(InstanceCreatorUID, specific_vault_instance)

set(SOPClassUID, '1.2.840.10008.5.1.4.1.1.88.11') // basic SR

// This is currently being set to the same as ZDS segment in the ORM, not "generated"  (is this correct??)
set(StudyInstanceUID, get('ZDS-1'))
def uid_root = '2.25.123456' // this must be no longer than 23 chars
set(SeriesInstanceUID, UIDUtils.createUID(uid_root))
set(SOPInstanceUID, UIDUtils.createUID(uid_root))


set(StudyDate, get('OBR-7'))
set(StudyTime, get('OBR-7'))
set(SeriesDate, get('OBR-7'))
set(SeriesTime, get('OBR-7'))

set(ContentDate, get('OBR-22'))
set(ContentTime, get('OBR-22'))


set(AccessionNumber, acc_num)
set(Modality, 'SR')
set(ModalitiesInStudy, get('OBR-24'))




setPersonName(ReferringPhysicianName, 'OBR-16', startingPosition=2)
set('ReferringPhysicianIdentificationSequence/InstitutionName', get('MSH-4'))

def seq = 'ReferringPhysicianIdentificationSequence/PersonIdentificationCodeSequence';
set(seq+'/CodeValue', get('OBR-16-1'))
set(seq+'/CodingSchemeDesignator', get('MSH-4'))
set(seq+'/CodeMeaning', 'Referring Physician ID')

set(StationName, get('ORC-13'))
set(StudyDescription, get('OBR-4-2'))
set(StudyID, acc_num)

set(NameOfPhysiciansReadingStudy, get('OBR-32-2'))

set('AnatomicRegionSequence/CodeValue', get('OBR-15-1-1'))
set('AnatomicRegionSequence/CodingSchemeDesignator', get('OBR-15-1-3'))
set('AnatomicRegionSequence/CodeMeaning', get('OBR-15-1-2'))

setPersonName(PatientName, 'PID-5')
set(PatientID, get('PID-3-1'))

// Set  IssuerOfPatientID.  If valid in HL7 ORM Report, copy it through.  Otherwise infer the IssuerOfPID from the HL7 Sending Facility that sent in the ORU Report
def hl7issuerofpid = get('PID-3-4-2')
log.info(" oru_to_sr.groovy:  The Sending Facility that came through in the ORU report was {}", hl7issuerofpid)

	// Check whether the Issuer of PID that we got from the HL7 order is valid
	def valid_issuers_of_pid = [
		"2.16.124.10.101.1.60.1.1006.1" = "Hôpital général du Lakeshore"
        		"2.16.124.10.101.1.60.1.1004.1" = "CHU Sainte-Justine"
		]

	def valid_pid = valid_issuers_of_pid.get(hl7issuerofpid)

	// If the Issuer of PID is known and valid, set it in the DICOM SR; otherwise, infer the Issuer of PID from the HL7 Sending Facility that sent in the ORU Report
	// CURRENTLY, SENDING FACILITY IS INFERRED TO BE IN ZDS-1-2, according to the ORM Orders we are generating in Connect
	
	//Set the IssuerOfPatientID if known and valid
	if(null != valid_pid) { 
		set(IssuerOfPatientID, hl7issuerofpid) 
		log.info("IssuerOfPatientID found in the HL7 Sending Facility is valid")
		log.info("Writing IssuerOfPatientID {} to the SR Report.\nThis IssuerOfPatientID belongs to {}", hl7issuerofpid, valid_pid)
	 } else {
	  //  Otherwise, infer the issuer of PID from the Sending Facility
	 	def hl7SendingFacility = get('ZDS-1-2')
	 	
	 	def knownHl7SendingFacilities = [
	 		 "RadImage" =  "2.16.124.10.101.1.60.1.1006.1"	// "Hôpital général du Lakeshore"
	 		 "POPULATE-ME" = "2.16.124.10.101.1.60.1.1004.1" 	// "CHU Sainte-Justine"
	 	  ]
	 		 
	 	def hl7SFtoDicom = knownHl7SendingFacilities.get(hl7SendingFacility)

	 	// Write the inferred IssuerOfPID; otherwise, set it to the default IssuerOfPID
	 	 if(null != hl7SFtoDicom) {
	 		 set(IssuerOfPatientID, hl7SFtoDicom)
	 		 log.info("Setting inferred IssuerOfPatientID to {}", hl7SFtoDicom)
	 	  } else {
	 	  	def defaultIssuerOfPID = "2.16.124.10.101.1.60.1.1006.1"	//Default IssuerOfPID currently is Lakeshore
	 	  	set(IssuerOfPatientID, defaultIssuerOfPID)
	 	  	log.warn(" oru_to_sr.groovy:  IssuerOfPatientID was blank and could not be inferred.  Setting it to the default {}", defaultIssuerOfPID)
	 	  }

	  }

set(PatientBirthDate, get('PID-7')) 	//Confirmed, most test patients have a birth date of 1921, October 9
set(PatientSex, get('PID-8')) 		// Confirmed

set(InstanceNumber, '1') 		// Confirmed; hard set
set(StudyStatusID, get('OBR-25'))  	// Confirmed; most test data seems to say "F" for finished (?)
set(ReasonForStudy, get('ORC-16')) 	// ***Pending***  none of the reports seens have an ORC-16 populated
set(RequestingPhysician, get('ORC-12'))	
set(RequestingService, get('MSH-4'))

set('RequestedProcedureCodeSequence/CodeValue', get('OBR-4-1'))
set('RequestedProcedureCodeSequence/CodingSchemeDesignator', 'Imagerie Québec-DSQ')
set('RequestedProcedureCodeSequence/CodeMeaning', get('OBR-4-2'))

set(RequestedProcedureID, acc_num)
set(ValueType, 'CONTAINER')

set('ConceptNameCodeSequence/CodeValue', '18748-4')
set('ConceptNameCodeSequence/CodingSchemeDesignator', 'LN')
set('ConceptNameCodeSequence/CodeMeaning', 'Diagnostic Imaging Report')

set(ContinuityOfContent, 'CONTINUOUS')

set('VerifyingObserverSequence/VerifyingOrganization', get('MSH-4'))
set('VerifyingObserverSequence/VerificationDateTime', get('OBR-7'))
set('VerifyingObserverSequence/VerifyingObserverName', get('OBR-32-2'))
seq = 'VerifyingObserverSequence/VerifyingObserverIdentificationCodeSequence'
set(seq+'/CodeValue', get('OBR-32-1'))
set(seq+'/CodingSchemeDesignator', get('MSH-4'))
set(seq+'/CodeMeaning', get('OBR-32-2'))

set('ReferencedRequestSequence/AccessionNumber', acc_num)
set('ReferencedRequestSequence/StudyInstanceUID', get('ZDS-1'))
set('ReferencedRequestSequence/RequestedProcedureDescription', get('OBR-4-2'))
set('ReferencedRequestSequence/RequestedProcedureID', RequestedProcedureID)
set('ReferencedRequestSequence/PlacerOrderNumberProcedure', get('ORC-2'))
set('ReferencedRequestSequence/FillerOrderNumberProcedure', get('ORC-3'))

seq = 'ReferencedRequestSequence/RequestedProcedureCodeSequence'
set(seq+'/CodeValue', get('OBR-4-1'))
set(seq+'/CodeMeaning', get('OBR-4-2'))

set(CompletionFlag, 'COMPLETE')
if (get('OBR-25') == 'F') {
    set(VerificationFlag, 'VERIFIED')
} else {
    set(VerificationFlag, 'UNVERIFIED')
}

set('ContentSequence[1]/RelationshipType', 'HAS CONCEPT MOD')
set('ContentSequence[1]/ValueType', 'CODE')
set('ContentSequence[1]/ConceptNameCodeSequence/CodeValue', '121058')
set('ContentSequence[1]/ConceptNameCodeSequence/CodingSchemeDesignator', 'DCM')
set('ContentSequence[1]/ConceptNameCodeSequence/CodeMeaning', 'Procedure Reported')

set('ContentSequence[1]/ConceptCodeSequence/CodeValue', get('OBR-4-1'))
set('ContentSequence[1]/ConceptCodeSequence/CodingSchemeDesignator', 'RP')
set('ContentSequence[1]/ConceptCodeSequence/CodeMeaning', get('OBR-4-2'))

set('ContentSequence[2]/RelationshipType', 'HAS CONCEPT MOD')
set('ContentSequence[2]/ValueType', 'CODE')
set('ContentSequence[2]/ConceptNameCodeSequence/CodeValue', '121049')
set('ContentSequence[2]/ConceptNameCodeSequence/CodingSchemeDesignator', 'DCM')
set('ContentSequence[2]/ConceptNameCodeSequence/CodeMeaning', 'Language of Content Item and Descendants')

set('ContentSequence[2]/ConceptCodeSequence/CodeValue', 'en') // or fr?
set('ContentSequence[2]/ConceptCodeSequence/CodingSchemeDesignator', 'RFC3066')
set('ContentSequence[2]/ConceptCodeSequence/CodeMeaning', 'English') // or french


set('ContentSequence[3]/RelationshipType', 'HAS OBS CONTEXT')
set('ContentSequence[3]/ValueType', 'CODE')
set('ContentSequence[3]/ConceptNameCodeSequence/CodeValue', '121005')
set('ContentSequence[3]/ConceptNameCodeSequence/CodingSchemeDesignator', 'DCM')
set('ContentSequence[3]/ConceptNameCodeSequence/CodeMeaning', 'Observer Type')

set('ContentSequence[3]/ConceptCodeSequence/CodeValue', '121006')
set('ContentSequence[3]/ConceptCodeSequence/CodingSchemeDesignator', 'DCM')
set('ContentSequence[3]/ConceptCodeSequence/CodeMeaning', 'Person')

set('ContentSequence[4]/RelationshipType', 'HAS OBS CONTEXT')
set('ContentSequence[4]/ValueType', 'CODE')
set('ContentSequence[4]/ConceptNameCodeSequence/CodeValue', '121011')
set('ContentSequence[4]/ConceptNameCodeSequence/CodingSchemeDesignator', 'DCM')
set('ContentSequence[4]/ConceptNameCodeSequence/CodeMeaning', 'Person Observer\'s Role in this Procedure')

set('ContentSequence[4]/ConceptCodeSequence/CodeValue', '121097')
set('ContentSequence[4]/ConceptCodeSequence/CodingSchemeDesignator', 'DCM')
set('ContentSequence[4]/ConceptCodeSequence/CodeMeaning', 'Recording')


set('ContentSequence[5]/RelationshipType', 'CONTAINS')
set('ContentSequence[5]/ValueType', 'CONTAINER')
set('ContentSequence[5]/ConceptNameCodeSequence/CodeValue', '121064')
set('ContentSequence[5]/ConceptNameCodeSequence/CodingSchemeDesignator', 'DCM')
set('ContentSequence[5]/ConceptNameCodeSequence/CodeMeaning', 'Current Procedure Description')
set('ContentSequence[5]/ContinuityOfContent', 'CONTINUOUS')

seq = 'ContentSequence[5]/ContentSequence'

set(seq+'/RelationshipType', 'CONTAINS')
set(seq+'/ValueType', 'TEXT')
set(seq+'/ConceptNameCodeSequence/CodeValue', '121065')
set(seq+'/ConceptNameCodeSequence/CodingSchemeDesignator', 'DCM')
set(seq+'/ConceptNameCodeSequence/CodeMeaning', 'Procedure Description')
set(seq+'/TextValue', get('OBR-4-1') + '^' + get('OBR-4-2'))

set('ContentSequence[6]/RelationshipType', 'CONTAINS')
set('ContentSequence[6]/ValueType', 'CONTAINER')
set('ContentSequence[6]/ConceptNameCodeSequence/CodeValue', '121070')
set('ContentSequence[6]/ConceptNameCodeSequence/CodingSchemeDesignator', 'DCM')
set('ContentSequence[6]/ConceptNameCodeSequence/CodeMeaning', 'Findings')

seq = 'ContentSequence[6]/ContentSequence'
set(seq+'/ValueType', 'TEXT')
set(seq+'/ConceptNameCodeSequence/CodeValue', '121071')
set(seq+'/ConceptNameCodeSequence/CodingSchemeDesignator', 'DCM')
set(seq+'/ConceptNameCodeSequence/CodeMeaning', 'Finding')
set(seq+'/TextValue', getList('OBSERVATION(*)/OBX-5(*)').join('\n'))

// don't yet know how to distinguish impression for regular findings
/*
set('ContentSequence[7]/RelationshipType', 'CONTAINS')
set('ContentSequence[7]/ValueType', 'CONTAINER')
set('ContentSequence[7]/ConceptNameCodeSequence/CodeValue', '121072')
set('ContentSequence[7]/ConceptNameCodeSequence/CodingSchemeDesignator', 'DCM')
set('ContentSequence[7]/ConceptNameCodeSequence/CodeMeaning', 'Impressions')

seq = 'ContentSequence[7]/ContentSequence'
set(seq+'/RelationshipType', 'CONTAINS')
set(seq+'/ValueType', 'TEXT')
set(seq+'/ConceptNameCodeSequence/CodeValue', '121073')
set(seq+'/ConceptNameCodeSequence/CodingSchemeDesignator', 'DCM')
set(seq+'/ConceptNameCodeSequence/CodeMeaning', 'Impression')
set(seq+'/TextValue', '');
*/

