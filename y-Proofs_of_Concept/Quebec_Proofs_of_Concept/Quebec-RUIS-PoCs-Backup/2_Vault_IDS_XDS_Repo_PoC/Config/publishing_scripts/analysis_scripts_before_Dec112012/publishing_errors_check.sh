#! /bin/bash

# Copyright Karos Health 2012
# Script written by Rodrigo Gonzalez

# This script checks for Common Errors in the Manifest Publishing Workflow

# Include the Libraries File
. /home/rialto/var/publishing_scripts/lib_publishing_errors.sh



set -x
    
    Initialize_Logs_for_Analysis

    Check_for_Connectivity_Errors_Against_XDS_Registry

    Check_for_Manifests_Publishing_Failures

    Check_for_Unexpected_Called_AETitles

    Check_for_DICOM_IO_Exceptions

    Check_for_HL7_Proxying_Errors

    Clean_up_Log_Dir

echo "all done"


#Check_Logs_Exist()
    #TODO add an if statement that will exit the script if Initialize() fails

