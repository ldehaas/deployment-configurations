#! /bin/bash                         

# Copyright Karos Health 2012
# Script written by Rodrigo Gonzalez


# This library sets up the common errors in Manifest Publishing


# Declare Variable
date_format=`date +"%y-%m-%d-%a"`

# Path to the Monitoring Logs
Path_to_Monitoring_Logs=/home/rialto/var/publishing_scripts/monitoring_logs/

# Path to the individual Rialto Logs
Path_to_Zipped_Logs=/home/rialto/log/logs_for_automatic_analysis/
Path_to_Rialto_Logs=/home/rialto/log/logs_for_automatic_analysis/working_space/

Rialto_dot_log=${Path_to_Rialto_Logs}rialto.log
Manifests_Publishing_log=${Path_to_Rialto_Logs}manifests.log
XDS_Repo_log=${Path_to_Rialto_Logs}xdsrepo.log
Vault_IDS_Service_log=${Path_to_Rialto_Logs}vaultIDS.log
dicom_log=${Path_to_Rialto_Logs}dicom.log
hl7_log=${Path_to_Rialto_Logs}hl7.log

# Paths to Publishing Errors Reports Files
xds_Connectvt_log=${Path_to_Monitoring_Logs}xds_Connectivity_errors_${date_format}.log
Manifest_Pub_Fails=${Path_to_Monitoring_Logs}manifest_publishing_errors_${date_format}.log
Wrong_CalledAE_Log=${Path_to_Monitoring_Logs}wrong_called_aetitles_${date_format}.log
DICOM_IO_Exception_Log=${Path_to_Monitoring_Logs}dicom_io_exceptions_${date_format}.log
hl7_proxy_errors_log=${Path_to_Monitoring_Logs}hl7_proxy_errors_${date_format}.log

# Paths to Temporary Files
Connectivity_Fails_Temp=${Path_to_Monitoring_Logs}ztemp_connectivity_fails
High_Level_Errors_Temp=${Path_to_Monitoring_Logs}ztemp_high_level_manifest_errors
Reg_Stored_Query_Errors_Temp=${Path_to_Monitoring_Logs}ztemp_reg_stored_query_errors
JavaIO_Errors_Temp=${Path_to_Monitoring_Logs}ztemp_javaio_errors
Unexpected_Errors_Temp=${Path_to_Monitoring_Logs}ztemp_unexpected_errors
Wrong_CalledAEs_Temp=${Path_to_Monitoring_Logs}ztemp_wrong_calledAEs
hl7_errors_temp=${Path_to_Monitoring_Logs}ztemp_hl7_errors
hl7_errors_temp_2=${Path_to_Monitoring_Logs}ztemp_hl7_errors2


# Initialize Log Files
function Initialize_Logs_for_Analysis {

    # Unzip the rolled logs from the previous day; log4j will zip each log nightly and put them in ther ${Path_to_Zipped_Logs} directory
    mv /home/rialto/rialto/log/*.gz ${Path_to_Zipped_Logs}

    # Operate on the lastest (previous day's) logs
    cp ${Path_to_Zipped_Logs}*.gz ${Path_to_Rialto_Logs}
    gunzip -f ${Path_to_Rialto_Logs}*.gz
    
    # Move the zipped original logs to an 'offline' directory
    mv ${Path_to_Zipped_Logs}*.gz /home/rialto/rialto/log/zippedlogs/

    # Standardize the log names
    mv ${Path_to_Rialto_Logs}rialto* ${Path_to_Rialto_Logs}rialto.log
    mv ${Path_to_Rialto_Logs}xdrepo* ${Path_to_Rialto_Logs}xdsrepo.log
    mv ${Path_to_Rialto_Logs}vaultIDS* ${Path_to_Rialto_Logs}vaultIDS.log
    mv ${Path_to_Rialto_Logs}dicom* ${Path_to_Rialto_Logs}dicom.log
    mv ${Path_to_Rialto_Logs}hl7* ${Path_to_Rialto_Logs}hl7.log
}


# Check for high level Connectivity Errors to the XDS Registry
function Check_for_Connectivity_Errors_Against_XDS_Registry {

    #Initialize Temporary File
    echo '' > ${Connectivity_Fails_Temp}
    
    # Find Errors
    grep 'Error connecting to' ${Rialto_dot_log} >> ${Connectivity_Fails_Temp}
    grep 'Socket Connect Retries Exhausted' ${Rialto_dot_log} >> ${Connectivity_Fails_Temp}
    grep 'Error sending SOAP message' ${Rialto_dot_log} >> ${Connectivity_Fails_Temp}

    # Write Final Report
    sort ${Connectivity_Fails_Temp} > ${xds_Connectvt_log}
}



function Check_for_Manifests_Publishing_Failures {
    # Initialize Files
    echo '' | tee ${Manifest_Pub_Fails} ${High_Level_Errors_Temp} ${Reg_Stored_Query_Errors_Temp} ${JavaIO_Errors_Temp} ${Unexpected_Errors_Temp} 

    # grep high level publishing errors, write to  Errors File; count total number of publishing failures
    grep 'Publish failed' ${Manifests_Publishing_log} | tee ${Manifest_Pub_Fails} | wc -l > ${High_Level_Errors_Temp}

    # filter out individual suberror types, count each suberror for each type
    grep 'RegistryStoredQuery returned with a status of urn' ${Manifest_Pub_Fails} | wc -l > ${Reg_Stored_Query_Errors_Temp}

    grep 'java.io.IOException' ${Manifest_Pub_Fails} | wc -l > ${JavaIO_Errors_Temp}

    grep 'Unexpected error' ${Manifest_Pub_Fails} | wc -l > ${Unexpected_Errors_Temp}

    
    # Itemize Number of each Suberror at the end of the daily summary
    echo "" >> ${Manifest_Pub_Fails}
    echo "" >> ${Manifest_Pub_Fails}
    echo "Summary of Manifest Publishing Errors:" >> ${Manifest_Pub_Fails}
    
    echo "All Publishing Errors: " >> ${Manifest_Pub_Fails}
    cat ${High_Level_Errors_Temp} >> ${Manifest_Pub_Fails} 

    echo "Registry Stored Query Errors: " >> ${Manifest_Pub_Fails}   
    cat ${Reg_Stored_Query_Errors_Temp} >> ${Manifest_Pub_Fails} 

    echo "Java IO Exceptions: " >> ${Manifest_Pub_Fails}
    cat ${JavaIO_Errors_Temp} >> ${Manifest_Pub_Fails} 

    echo "Unexpected Errors: " >> ${Manifest_Pub_Fails} 
    cat ${Unexpected_Errors_Temp} >> ${Manifest_Pub_Fails} 
}



function Check_for_Unexpected_Called_AETitles {

    # Look for invalid called AETitles and clobber the previous contents of the log
    grep 'using a different Called' ${Manifests_Publishing_log} | tee ${Wrong_CalledAE_Log} | wc -l > ${Wrong_CalledAEs_Temp}

    # Summarize the number of incorrect AETitles used at the end of the Report
    echo "" >> ${Wrong_CalledAE_Log}
    echo "" >> ${Wrong_CalledAE_Log}
    echo "Individual Instances of Wrong AETitles Used: " >> ${Wrong_CalledAE_Log}
    cat ${Wrong_CalledAEs_Temp} >> ${Wrong_CalledAE_Log}

}



function Check_for_DICOM_IO_Exceptions {
    
    # Look for IO Exceptions in DICOM; clobber the previous contents of the lo
    grep ' i/o exception in' ${dicom_log} > ${DICOM_IO_Exception_Log}
}



function Check_for_HL7_Proxying_Errors {
    
    # Look for HL7 Proxying Errors, write them in context to the log.  Count how many hl7 proxying errors
    grep -C 15 'Proxying to a destination' ${hl7_log} | tee ${hl7_proxy_errors_log} | grep 'Proxying to a destination' | wc -l > ${hl7_errors_temp}

    # Look for HL7 received errors 
    echo "" >> ${hl7_proxy_errors_log}
    echo "" >> ${hl7_proxy_errors_log}
    echo "" >> ${hl7_proxy_errors_log}
    echo "" >> ${hl7_proxy_errors_log}
    echo "" >> ${hl7_proxy_errors_log}
    grep -B 10 "Got a non-successful response" ${hl7_log} | tee ${hl7_proxy_errors_log} | grep "Got a non-successful response" | wc -l ${hl7_errors_temp_2}

    # Summarize HL7 Proxying Errors
    echo "" >> ${hl7_proxy_errors_log}
    echo "" >> ${hl7_proxy_errors_log}
    echo "Total Number of HL7 Proxy Actual Errors (Timeout or Connectivity Errors): " >> ${hl7_proxy_errors_log}
    cat ${hl7_errors_temp} >> ${hl7_proxy_errors_log}
    
    # Summarize non-successful Response Errors
    echo "" >> ${hl7_proxy_errors_log}
    echo "Total Number of HL7 Proxy received errors (Explicit \"Non-Successful\" AE Errors Passed from downstream: " >> ${hl7_proxy_errors_log}
    cat ${hl7_errors_temp_2} >> ${hl7_proxy_errors_log}

    # Output Global Total Errors this Date
    echo "" >> ${hl7_proxy_errors_log}
    echo "" >> ${hl7_proxy_errors_log}
    echo "Global (i.e. Grand Total) Number of Error: " >> ${hl7_proxy_errors_log}
    local global_errors=`cat ${hl7_errors_temp}`
    let tot_global_errors=$global_errors + `cat ${hl7_errors_temp_2}`
    cat ${tot_global_errors} >> ${hl7_proxy_errors_log}

}


function Clean_up_Log_Dir {
    
    # Make a directory to store the daily Manifest Publishing logs
    local todays_report_folder=${Path_to_Monitoring_Logs}Publish_Reports_${date_format}/
    mkdir ${todays_report_folder}

    # Copy today's Publishing Report to today's directory
    mv ${Path_to_Monitoring_Logs}*.log ${todays_report_folder}

    # Clean up Manifest Report Directory
    rm -f ${Path_to_Monitoring_Logs}ztemp* 
    
    #Clean up working log Dir
    rm -f ${Path_to_Rialto_Logs}*
}

<< BLOCKING
# Initialize the Log checking 
function Check_Logs_Exist {
#TODO Check if each of the files exist, otherwise, give back an error

#TODO add an if statement that will exit with an error if any of the files DON'T exist (print out 1 by 1)

}
*/
BLOCKING 
