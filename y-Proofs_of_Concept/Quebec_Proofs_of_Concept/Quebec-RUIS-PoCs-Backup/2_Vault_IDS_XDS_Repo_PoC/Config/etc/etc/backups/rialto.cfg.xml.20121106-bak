<?xml version="1.0" encoding="UTF-8"?>

<!-- Hosted on this Server are:

        Product: Rialto Vault (Image Document Source) 
	
        Functional (ie. Business) Services:
         1. XDS-i Imaging Document Source, taking DICOM studies and creates XDS Manifest for them.
             It also publishes these XDS Manifests into the XDS Repository
         2. XDS-i Repository, storing, indexing and serving the XDS Manifests to XDS Document Consumers
	 3. A WADO interface, used to serve Reports in a WADO format for IBM Concerto Viewer clients
         4. An HL7 Server that converts ORU Reports into DICOM Structured Reports (SRs)
             This HL7 server is part of the main Vault-IDS service

        Utility Services:
	 1. An HL7 Proxy Service, serving as a single-port interface for 3 internal HL7 servers: 1 for HL7 ADT messages, 1 for ORM orders, 1 for ORU Reports
             NOTE: Currently, this HL7 Proxy is also being used as a centralized HL7 distribution point for various infrastructure services
                HL7 Recipients currently include the HMI DI-r, the PACS at Hopital General du Lakeshore, and Rialto Connect XDS-i Consumer
         2. An (internal only) mini-PIX manager, to map local patients to Provincial (i.e. RAMQ identifiers)
            The PIX manager is strictly populated by ADT Messages received through the HL7 Proxy
        -->

<config>

  <!-- Rialto Configuration Variables -->
    <var name ="dicomServerForManifestsPort">4104</var>
    <var name ="dicomServerForWADOImagesPort">5104</var>
    
    <var name ="hl7ProxyReceiverServerPort">5554</var>
        <!-- Defines the default character set for HL7 Servers-->
    <var name= "defaultCharacterSet">8859/1</var>
    <var name ="internalHL7ServerForPIXadtsPort">5555</var>
        <!-- Receiving Application and Sending Facility are used by the HL7 Proxy to identify devices -->
        <var name ="internalHL7ServerForPIXadtsReceivingApplication">VAULT-ADT-RCV</var>
        <var name ="internalHL7ServerForPIXadtsReceivingFacility">TCR06-RUISMM</var>
        <!-- These additional Receiving Application and Facility reference the same server but are used to identify the "ORM to ADT" Transformer -->
        <var name="internalHL7_ORMtoADT_TransformerReceivingApplication">VAULT-ORM-TRF</var>
        <var name="internalHL7_ORMtoADT_TransformerReceivingFacility">VAULT-ORM-TRF</var>
    <var name ="internalHL7ServerForORUreportsPort">5557</var>
        <!-- Receiving Application and Sending Facility are used by the HL7 Proxy to identify devices -->
        <var name ="internalHL7ServerForORUreportsReceivingApplication">VAULT-ORU-RCV</var>
        <var name ="internalHL7ServerForORUreportsReceivingFacility">TCR06-RUISMM</var>
    <!-- The Orders Server is currently turned off - ->
    <var name ="internalHL7ServerForORMordersPort">5556</var>
        <!- - Receiving Application and Sending Facility are used by the HL7 Proxy to identify devices - ->
        <var name ="internalHL7ServerForORMordersReceivingApplication">VAULT-ORM-RCV</var>
        <var name ="internalHL7ServerForORMordersReceivingFacility">TCR06-RUISMM</var>
    -->
    
    <var name ="xdsRepoAndWADORequestsPort">8080</var>

   	<!-- Full XDS Repo URI to be used as a device --> 
    <var name ="XDSRepoURI">http://localhost:8080/vault/repo</var>
	<!-- The base URI that is attached to the webserver -->
    <var name="XDSRepoBaseURI">/vault/repo</var>
        <!-- Repository OID for this Vault Instance (provided by the End Customer) -->
    <var name="RepositoryUID">2.16.124.10.101.1.60.2.81</var>
        <!-- Affinity Domain OID is the Issuer of Patient ID at the Highest Identification Level (i.e. the RAMQ Issuer of PID in Quebec) -->
    <var name="AffinityDomainOID">2.16.124.10.101.1.60.100</var>

       <!-- AETitle to use for:
        1. Calling a C-Move SCP (WADO conversion is currently the only IDS workflow that does this)
        2. Requesting a destination AETitle for the ensuing C-Store (generated from the C-Move).
	(For our own WADO Server SCP they can call us anything they like)
       -->
    <var name="dicom-aetitle-for-wado-server-and-scu">VAULT_WADO</var>
        <!-- WADO base url to attach to the WADO web server -->  
    <var name="WADOBaseURI">/vault/wado</var>  
    
    <var name ="eventCollectorPort">9090</var>
    <var name ="Audit_Record_Repository_Server_API_Port">8081</var>
    <var name ="Audit_Record_Repository_Server_UDP_Port">4000</var>


  <!-- Infrastructure Systems Configuration -->
    
    <var name ="XDSRegistryURI">http://10.241.135.19:9085/IBMXDSRegistry/XDSb/SOAP12/Registry</var>
    <!--  Old XDS Registry Entry <var name ="XDSRegistryURI">http://10.241.7.31:9085/IBMXDSRegistry/XDSb/SOAP12/Registry</var> -->
    
    <!-- DIR imaging archive (e.g. to send cmove request to for WADO conversion) -->
    <var name ="DIRdicomHost">10.241.40.232</var>
    <var name ="DIRdicomPort">5000</var>
    <var name ="DIRdicomAETitle">ALI_QUERY_EIRSCP</var>
        <!-- <var name ="DIRdicomAETitle">ALI_QUERY_SCP</var> -->
    <!-- Forward HL7 Traffic from HL7 Proxy to DIR Instance 1-->
    <var name ="DIRhl7Port">5550</var>
    <var name ="DIRhl7ReceivingApplication">TEST-DIR-MCKESSON</var>
    <var name ="DIRhl7ReceivingFacility">TCR06-RUISMM</var>

    <!-- Forward HL7 Traffic from Ste-Justine to DIR Instance 2 -->
    <var name="DIR_2hl7Host">10.241.40.230</var>
    <var name="DIR_2hl7Port">5550</var>
    <var name="DIR_2hl7ReceivingApplication">TEST-DIR2-MCKESSON</var>
    <var name="DIR_2hl7ReceivingFacility">TCR06-RUISMM</var>

    <!-- Forward HL7 Traffic from HL7 Proxy to Rialto Connect -->
    <var name ="ConnectHL7Host">10.241.40.226</var>
    <var name ="ConnectHL7ADTPort">5555</var>
    <var name ="ConnectHL7ADTReceivingApplication">CONNECT-TEST-ADT</var>
    <var name ="ConnectHL7ADTReceivingFacility">TCR06-RUISMM</var>
    <var name ="ConnectHL7ORMPort">5556</var> 
    <var name ="ConnectHL7ORMReceivingApplication">CONNECT-TEST-ORM</var>
    <var name ="ConnectHL7ORMReceivingFacility">TCR06-RUISMM</var>
        <!-- These Properties are the same as the ADT, but are used for the ORM to ADT Transformer -->
    <var name ="ConnectTRANSF_ORMtoADT_ReceivingApplication">CONNECT-TEST-TRNS</var>
    <var name ="ConnectTRANSF_ORMtoADTORM_ReceivingFacility">TCR06-RUISMM</var>
   
    <!-- Forward HL7 Traffic from HL7 Proxy to PACS -->
    <var name ="PACSHL7Host">10.135.160.199</var>
    <var name ="PACShl7Port">15551</var>
    <var name ="PACShl7ReceivingApplication">SDXS</var>
    <var name ="PACShl7ReceivingFacility">PACS1-McKessonLakeshore</var>


    <!-- Local Assigning Authorities (for reference only) 
        Hôpital général du Lakeshore = 2.16.124.10.101.1.60.1.1006.1
        CHU Sainte-Justine = 2.16.124.10.101.1.60.1.1004.1
    -->




  <!-- Service Declaration -->
    
    <!-- IHE Audit Record Repository -->
        <!-- this HAS to be declared as the first service, otherwise events that start before will not be logged -->
    <service id="IHE_Audit_Record_Repository_UDP" type="arr">
        <server idref="Audit_Record_Repository_Server-http" name="arr-api">
            <url>/arrudp/auditRecords/*</url>
        </server>
        <config>
            <prop name="IndexDir">${rialto.rootdir}/var/arr/index</prop>
            <prop name="IndexBackupDir">${rialto.rootdir}/var/arr/backup</prop>
            <prop name="ArchiveDir">${rialto.rootdir}/var/arr/archive</prop>
            <prop name="TempDir">${rialto.rootdir}/var/arr/temp</prop>
            <prop name="Listener">udp:${Audit_Record_Repository_Server_UDP_Port}</prop>
            <prop name="BulkSize">1000</prop>
        </config>
    </service>


    <!-- Service that keeps the events that the Event Collector reads to display on its web interface -->
    <service id="TransactionRecordsServiceForEventCollector" type="txn-records">
        <config>
            <prop name="StorageDir">${rialto.rootdir}/var/txn_records</prop>
        </config>
    </service>



    <!-- Event Collector for Debugging / Transactions -->
    <service id="EventCollectorService" type="navigator">
        <server idref="eventCollector-http" name="navigator-war">
            <url>/</url>
        </server>
    </service>
   

    <!-- The Main Vault Service "Imaging Document Source" -->
    <service id="Main_Service-VaultIds" type="ids">
        <device idref="xdsRegistry" />
        <device idref="xdsRepository" />
        <device idref="DIR-dicom" name="WADO" />

	
        <!-- dependencies for receiving HL7 ADT feeds for the MiniPIX functionality -->
        <server idref="intHl7AdtforPIX" name="PIXFeed" />
		
        <!-- dependencies for receiving / storing ORUs -->
        <server idref="intHl7Oru" name="ORU" />

        <!-- dependencies for generating manifests -->
        <server idref="dicomForManifestFeed" name="manifest" />
        
        <!-- dependencies to WADO enable the IA -->
        <server idref="dicomImageReceiveForWado" name="WADO" />
        <server idref="httpXDSRepoAndWadoRequests" name="WADO">
            <url>${WADOBaseURI}</url>
            <!-- Sample WADO query  (change the **THIS** items):
            http:localhost:**PORT**/${WADOBaseURI}?requestType=WADO&studyUID=**STUDY_INSTANCE_UID**&seriesUID=**SERIES_INSTANCE_UID**&objectUID=**SOP_INSTANCE_UID**&contentType=text/plain
            -->
        </server>
        
        <config>
            <prop name="PIXDatabaseURL">jdbc:postgresql://localhost/pix?user=rialto</prop>
            
            <prop name="AffinityDomain">${AffinityDomainOID}</prop>
            
            <!-- Local AETitle for Vault IDS to use when:
                 1. Calling a C-Move SCP (WADO conversion is currently the only IDS workflow that does this)
                 2. Requesting a destination AETitle for the ensuing C-Store (generated from the C-Move)
             -->
            <prop name="LocalAE">${dicom-aetitle-for-wado-server-and-scu}</prop>
            
            <prop name="WADOCacheFolder">${rialto.rootdir}/var/ids/wadocache</prop>
            
            <prop name="MetadataStudyInstanceUIDKey">studyInstanceUID</prop>
            
            <prop name="ManifestContentTypeCode">examen imagerie^KOS étude simple^Imagerie Québec-DSQ^junk</prop>
            <!-- THIS ONE WORKS!! <prop name="ManifestContentTypeCode">examen imagerie^KOS étude simple^Imagerie Québec-DSQ^junk</prop> -->
            <!--prop name="ManifestContentTypeCode">manifest^^test</prop -->>
            <!-- prop name="ManifestContentTypeCode">^^</prop-->


            <!-- Morphers --> 

            <!-- Modifies the HL7 ADT Feeds as it is coming in -->           
            <prop name="PIXMorpher">${rialto.rootdir}/etc/morphers/pix_morpher.groovy</prop>

            <!-- Runs once, after all the images for a study have been received.  Sets ad-hoc tags in the Manifest
                    Gotchas are things like local IssuerOfPatientID so that Vault IDS can properly publish the manifest to the XDS Registry -->
            <prop name="ManifestMorpher">${rialto.rootdir}/etc/morphers/manifest_morpher.groovy</prop>

            <!--  Generates and modifies the metadada that will be published in the registry as part of the "Register" in "Provide and Register Document Set" -->
            <!-- If you want IDS to generate and store the Manifest in the local XDS Repo without publishing to the XDS Registry, use:
                            set(XDSExtendedMetadata('karosDontRegister'), 'true')    at the top of the script  -->
            <prop name="DocumentMetadataMorpher">${rialto.rootdir}/etc/morphers/document_metadata_morpher.groovy</prop>

            <!-- Converts an ORU Report (received from RISs via the HL7 Server) into a DICOM SR -->
            <prop name="ORUToSRMorpher">
                ${rialto.rootdir}/etc/morphers/oru_to_sr.groovy
            </prop>
            
            <!-- Runs when the WADO interface is called to provide a report.  Takes the stored corresponding DICOM SR and extracts the text to serve it back to caller -->
            <prop name="SRToTextMorpher">
                ${rialto.rootdir}/etc/morphers/sr_to_text.groovy
            </prop>

        </config>
    </service>


    <!--The XDS Repository Interface -->
    <service id="xdsRepositoryService" type="xdsrepo">
       
	 <!-- Link in the Existing Web Server-->
	<server idref="httpXDSRepoAndWadoRequests" name="repository">
            <url>${XDSRepoBaseURI}</url>
        </server>
        <device idref="xdsRegistry" />
        
        <config>
            <prop name="UniqueId" value="${RepositoryUID}" />
            <prop name="IndexType" value="JPA" />
            <prop name="StorageType" value="JPA" />
            <prop name="TempDir" value="${rialto.rootdir}/var/xdsrepo/tmp" />
            <prop name="IndexerDatabaseURL">
                jdbc:postgresql://localhost/xdsrepo?user=rialto
            </prop>
        </config>
    </service>
    

    
    <!-- HL7 Proxy Service -->
    <service id="HL7ProxyService" type="hl7-proxy">
    	<!-- Server to Listen On -->
    	<server idref="hl7ProxyReceiver" />
        <!-- "name"=   is not needed in this server -->
    
        <config>
            <!-- General Tag Morphers not used for the moment
            <prop name="TagMorphers">
            </prop>
            -->
           
            <prop name="ResponseMorpher" value="${rialto.rootdir}/etc/morphers/hl7proxyscripts/hl7_proxy_response_morpher.groovy" />

            <prop name="Destinations">	
                <!-- Destination Devices for which to Proxy -->
                <!-- NOTE: Please respect the order of the HL7 Proxy listing, or you will have performance issues and unecessary complex logs -->

    <!--  Turning off traffific  - ->
                <!- - Foward to local HL7 ADT server - ->
                <destination>
                    <facility>${internalHL7ServerForPIXadtsReceivingFacility}</facility>
                    <application>${internalHL7ServerForPIXadtsReceivingApplication}</application>
                    <script>${rialto.rootdir}/etc/morphers/hl7proxyscripts/local_adt_forward_rules.groovy</script>
                </destination>
                
                <!- - Forward ORMs to local (internal) "ORM to ADT Transformer"- ->
                <destination>
                    <facility>${internalHL7_ORMtoADT_TransformerReceivingFacility}</facility>
                    <application>${internalHL7_ORMtoADT_TransformerReceivingApplication}</application>
                    <script>${rialto.rootdir}/etc/morphers/hl7proxyscripts/local_orm_to_adt_transformer_forward_rules.groovy</script>
                </destination>
              
                <!- - Forward ORMs transformed into ADTs via the  "ORM to ADT Transformer"- ->
                <destination>
                    <facility>${ConnectTRANSF_ORMtoADTORM_ReceivingFacility}</facility>
                    <application>${ConnectTRANSF_ORMtoADT_ReceivingApplication}</application>
                    <script>${rialto.rootdir}/etc/morphers/hl7proxyscripts/infr_connect_orm_to_adt_transformer_forward_rules.groovy</script>
                </destination>
                
                <! - - Forward ADT to Connect POC at TCR06 - ->
                <destination>
                    <facility>${ConnectHL7ADTReceivingFacility}</facility>
                    <application>${ConnectHL7ADTReceivingApplication}</application>
                    <script>${rialto.rootdir}/etc/morphers/hl7proxyscripts/infr_connect_adt_forward_rules.groovy</script>
                </destination>
               
                <!- - Forward ALL Lakeshore RIS HL7 Traffic to Lakeshore McKesson PACS - ->
                <destination>
                    <facility>${PACShl7ReceivingFacility}</facility>
                    <application>${PACShl7ReceivingApplication}</application>	        
                    <script>${rialto.rootdir}/etc/morphers/hl7proxyscripts/infr_pacs1_forward_rules.groovy</script>
                </destination>
-->
                <!-- Forward ALL Ste-Justine HL7 Traffic to McKesson DIR 1 -->
                <destination>
                    <facility>${DIRhl7ReceivingFacility}</facility>
                    <application>${DIRhl7ReceivingApplication}</application>        
                    <script>${rialto.rootdir}/etc/morphers/hl7proxyscripts/infr_dir_forward_rules.groovy</script>
                </destination>

		<!-- Forward ALL Lakeshore HL7 Traffic to McKesson DIR 2 -->
                <destination>
                    <facility>${DIR_2hl7ReceivingFacility}</facility>
                    <application>${DIR_2hl7ReceivingApplication}</application>
                    <script>${rialto.rootdir}/etc/morphers/hl7proxyscripts/infr_dir_2_forward_rules.groovy</script>
                </destination>

                <!-- CURRENTLY, FUNCTIONALITY NOT AVAILABLE
                    Forward to local HL7 ORM Order server- ->
                <destination>
                    <facility>${internalHL7ServerForORMordersReceivingFacility}</facility>
                    <application>${internalHL7ServerForORMordersReceivingApplication}</application>
                    <script>${rialto.rootdir}/etc/morphers/hl7proxyscripts/local_orm_forward_rules.groovy</script>
                </destination>
                -->
   <!--             
		<!- - Forward ORM Orders to Connect POC at TCR06  - ->
                <destination>
                    <facility>${ConnectHL7ORMReceivingFacility}</facility>
                    <application>${ConnectHL7ORMReceivingApplication}</application>
                    <script>${rialto.rootdir}/etc/morphers/hl7proxyscripts/infr_connect_orm_forward_rules.groovy</script>
                </destination>

                
                <!- - Foward to local HL7 ORU Report server - ->
                <destination>
                    <facility>${internalHL7ServerForORUreportsReceivingFacility}</facility>
                    <application>${internalHL7ServerForORUreportsReceivingApplication}</application>
                    <script>${rialto.rootdir}/etc/morphers/hl7proxyscripts/local_oru_forward_rules.groovy</script>
                </destination>
                    -->
                    
		
            </prop> <!-- End of Destinations Property -->
        </config> <!-- End of Configuration for hl7proxy service -->
    </service>    

  <!-- Servers Declaration -->

   <!-- Utility Servers -->
    
    <!-- UI Server for IHE Transaction records -->
    <server id="Audit_Record_Repository_Server-http" type="http">
         <port>${Audit_Record_Repository_Server_API_Port}</port>
    </server>

    <!-- UI Server for Event Collector -->
    <server id="eventCollector-http" type="http">
        <port>${eventCollectorPort}</port>
    </server>


   <!-- Server for HL7 Proxy -->    
    <!--hl7 server for hl7 proxy -->
    <server id="hl7ProxyReceiver" type="hl7v2">
    <port>${hl7ProxyReceiverServerPort}</port>
        <timeout>3m</timeout>
        <defaultEncoding>${defaultCharacterSet}</defaultEncoding> 
    </server>


    <!-- Servers for Vault - XDS Imaging Document Source -->
    <!-- receive dicom images to generate manfiests -->
    <server id="dicomForManifestFeed" type="dicom">
        <port>${dicomServerForManifestsPort}</port>
    </server>
    
    <!-- receive dicom images for WADO  *CURRENTLY NOT USED IN THE POC* -->
    <server id="dicomImageReceiveForWado" type="dicom">
        <port>${dicomServerForWADOImagesPort}</port>
    </server>
    
    <!-- receive http XDS Repo queries / publish; receive WADO requests -->
    <server id="httpXDSRepoAndWadoRequests" type="http">
        <port>${xdsRepoAndWADORequestsPort}</port>
    </server>
    
    <!-- internal receive HL7 ADT feeds for PIX functionality -->
    <server id="intHl7AdtforPIX" type="hl7v2">
        <port>${internalHL7ServerForPIXadtsPort}</port>
        <!-- Limit this Server for only locahost traffic -->
        <host>127.0.0.1</host>
        <defaultEncoding>${defaultCharacterSet}</defaultEncoding> 
    </server>

    <!-- receive HL7 ORU reports -->
    <server id="intHl7Oru" type="hl7v2">
        <port>${internalHL7ServerForORUreportsPort}</port>
        <!-- Limit this Server for only locahost traffic -->
        <host>127.0.0.1</host>
        <defaultEncoding>${defaultCharacterSet}</defaultEncoding>
    </server>
    
    <!-- receive HL7 ORM orders  -JUST A PLACE HOLDER FOR NOW  - ->
    <server id="intHl7Orm" type="hl7v2">
        <port>${internalHL7ServerForORMordersPort}</port>
        <host>127.0.0.1</host>
        <defaultEncoding>${defaultCharacterSet}</defaultEncoding>
    </server> -->



    <!-- Devices Declaration --> 
   
    <!-- xds registry and repository for publishing manifests -->
 
    <!-- In Quebec, XDS Registry is IBM Registry -->
    <device id="xdsRegistry" type="xdsreg">
        <url>${XDSRegistryURI}</url>
        <!-- <url>http://10.128.56.214:9085/IBMXDSRegistry/XDSb/SOAP12/Registry</url> -->
        <!-- <url>https://10.128.56.214:9448/IBMXDSRegistry/XDSb/SOAP12/Registry</url> -->
    </device>
    
    <!-- This is the local (@vault machine) XDS Repo Service to Create and Serve the XDS Manifests-->
    <!-- Currently, according to the Rules in Quebec, we are using the same value for the IDS Unique ID and the XDS Repo Unique ID  -->
    <device id="xdsRepository" type="xdsrep">
        <url>${XDSRepoURI}</url>
        <uniqueid>${RepositoryUID}</uniqueid>
        <sourceid>${RepositoryUID}</sourceid>
    </device>

    
    <!-- DIR imaging archive (e.g. to send cmove request to for WADO conversion) -->
    <device id="DIR-dicom" type="dicom">
        <host>${DIRdicomHost}</host>
        <port>${DIRdicomPort}</port>
        <aetitle>${DIRdicomAETitle}</aetitle>
    </device>


    <!-- hl7 devices for connecting to the local hl7 services -->
    <device id="localAdtForPixDevice" type="hl7v2">
        <host>localhost</host>
        <port>${internalHL7ServerForPIXadtsPort}</port>
        <receivingApplication>${internalHL7ServerForPIXadtsReceivingApplication}</receivingApplication>
        <receivingFacility>${internalHL7ServerForPIXadtsReceivingFacility}</receivingFacility>
    </device>

    <device id="localORMtoADTTransformer" type="hl7v2">
        <host>localhost</host>
        <port>${internalHL7ServerForPIXadtsPort}</port>
        <receivingApplication>${internalHL7_ORMtoADT_TransformerReceivingApplication}</receivingApplication>
        <receivingFacility>${internalHL7_ORMtoADT_TransformerReceivingFacility}</receivingFacility>
    </device>

    <device id="localHl7OruDevice" type="hl7v2">
        <host>localhost</host>
        <port>${internalHL7ServerForORUreportsPort}</port>
        <timeout>2m</timeout>
        <receivingApplication>${internalHL7ServerForORUreportsReceivingApplication}</receivingApplication>
        <receivingFacility>${internalHL7ServerForORUreportsReceivingFacility}</receivingFacility>
    </device>

    <!-- device to proxy HL7 ORM orders  -JUST A PLACE HOLDER FOR NOW  - ->
    <device id="localHl7OrmDevice" type="hl7v2">
        <host>localhost</host>
        <port>${internalHL7ServerForORMordersPort}</port>
        <receivingApplication>${internalHL7ServerForORMordersReceivingApplication}</receivingApplication>
        <receivingFacility>${internalHL7ServerForORMordersReceivingFacility}</receivingFacility>
    </device> -->
   
 
   <!-- hl7 devices for proxying hl7 traffic to infrastructure servers -->

    <device id="DIR-hl7" type="hl7v2">
        <host>${DIRdicomHost}</host>
        <port>${DIRhl7Port}</port>
        <receivingApplication>${DIRhl7ReceivingApplication}</receivingApplication>
        <receivingFacility>${DIRhl7ReceivingFacility}</receivingFacility>
    </device>

    <device id="DIR_2-hl7" type="hl7v2">
        <host>${DIR_2hl7Host}</host>
        <port>${DIR_2hl7Port}</port>
        <receivingApplication>${DIR_2hl7ReceivingApplication}</receivingApplication>
        <receivingFacility>${DIR_2hl7ReceivingFacility}</receivingFacility>
    </device>

    <device id="Connect-hl7AdtDevice" type="hl7v2">
        <host>${ConnectHL7Host}</host>
        <port>${ConnectHL7ADTPort}</port>
        <receivingApplication>${ConnectHL7ADTReceivingApplication}</receivingApplication>
        <receivingFacility>${ConnectHL7ADTReceivingFacility}</receivingFacility>
    </device>

    <device id="Connect-Transform_ORM_TO_ADTDevice" type="hl7v2">
        <host>${ConnectHL7Host}</host>
        <port>${ConnectHL7ADTPort}</port>
        <receivingApplication>${ConnectTRANSF_ORMtoADT_ReceivingApplication}</receivingApplication>
        <receivingFacility>${ConnectTRANSF_ORMtoADTORM_ReceivingFacility}</receivingFacility>
    </device>
    
    <device id="Connect-hl7OrmDevice" type="hl7v2">
        <host>${ConnectHL7Host}</host>
        <port>${ConnectHL7ORMPort}</port>
        <receivingApplication>${ConnectHL7ORMReceivingApplication}</receivingApplication>
        <receivingFacility>${ConnectHL7ORMReceivingFacility}</receivingFacility>
    </device>

    <device id="PACS" type="hl7v2">
        <host>${PACSHL7Host}</host>
        <port>${PACShl7Port}</port>
        <timeout>20s</timeout>
        <receivingApplication>${PACShl7ReceivingApplication}</receivingApplication>
        <receivingFacility>${PACShl7ReceivingFacility}</receivingFacility>
    </device>

</config> 
