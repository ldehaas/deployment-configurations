/**
 * converts HL7 ORU to a DICOM SR object
 * draft according to the SAD v0.5
 *
 * many of the HL7 locations are potentially different in the field. this 
 * script is simply a starting point.
 */
import org.dcm4che2.util.UIDUtils
import org.joda.time.DateTime

set(SpecificCharacterSet, 'ISO_IR_100')

// Top-level Variables used
def acc_num = get('OBR-19')

def studyFromLakeShore = null
def studyFromSteJustine = null


// Set local Affinity Domain.  It will be missing in the ORU feed
def issuerOfPID = null
if ( "SHSJPACSTST2" == get('MSH-4') ) {
    issuerOfPID = '2.16.124.10.101.1.60.1.1004.1'
    set( IssuerOfPatientID, issuerOfPID)
    log.info("The ORU to SR Conversion is assuming this Patient is from Ste-Justine")
    studyFromLakeShore = true
} else if ( "SVR-TSERV0" == get('MSH-4') ) {
    issuerOfPID = '2.16.124.10.101.1.60.1.1007.1'
    set( IssuerOfPatientID, issuerOfPID)
    log.info("The ORU to SR Conversion is assuming this Patient is from Lakeshore")
    studyFromSteJustine = true
} else if ( null == get('PID-3-4-2') ) {
    // The Issuer of PID officially should be in PID-3-4-2, but most of the time, it is not sent
    //  If we find out in production that it is actually used, we need to move this last "else if" to a first level "if"
    issuerOfPID = '2.16.124.10.101.1.60.1.1007.1'
    set( IssuerOfPatientID, issuerOfPID)
    // This is for Lakeshore
    // I previously had the erroneous value: '2.16.124.10.101.1.60.1.1006.1'
    log.warn("The MSH-4 in this HL7 Message was blank!\nThe ORU to SR Conversion is assuming this Patient is from Lakeshore")
    studyFromLakeShore = true
}
log.info("\n\nORU to SR Conversion: the Issuer of Patient ID being set is \"{}\"", issuerOfPID)

now = new DateTime()
set(InstanceCreationDate, now)
set(InstanceCreationTime, now)

// This is a unique Value per instance of Vault
set(InstanceCreatorUID, '1.2.3.4.5.6.7.8.9')


set(SOPClassUID, '1.2.840.10008.5.1.4.1.1.88.11') // basic SR

set(StudyInstanceUID, get('ZDS-1'))
def uid_root = '2.25.123456' // this must be no longer than 23 chars
set(SeriesInstanceUID, UIDUtils.createUID(uid_root))
set(SOPInstanceUID, UIDUtils.createUID(uid_root))

// Set study time and date
 def obr6 = get('OBR-6')
 def obr7 = get('OBR-7')
 log.info("\nORU to SR Conversion:  Contents of the OBR-6 = \"{}\"",obr6)
 def obr22 = get('OBR-22')

 def studyDate = null
 def studyTime = null
 def studyDateAlt = null
 def studyTimeAlt = null
 def contentDate = null
 def contentTime = null

 // Validate Dates and Times
  if (14 != obr6.length()) {
   // Fail the OBR-6 as a valid date segment if it is not complete
    obr6 = null
    log.warn("\n\n\nORU to SR Conversion: OBR-6 Segment did **NOT** contain a Valid Date Format.  ORU to SR Conversion might fail.")
  } else {
    // Parse the date and time
    studyDate = obr6[0..7]
    studyTime = obr6[8..13]
  }

  if (14 != obr7.length()) {
   // Fail the OBR-7 as a valid date segment if it is not complete
    obr7 = null
    if(null == obr6) {
      log.warn("\n\n\nORU to SR Conversion: OBR-7 Segment did **NOT** contain a Valid Date Format.  ORU to SR Conversion might fail.")
    }
  } else {
   // Parse the date and time
    studyDateAlt = obr7[0..7]
    studyTimeAlt = obr7[8..13]
  }

  if (14 != obr22.length()) {
   // Fail the OBR-22 as a valid date segment if it is not complete
    obr22 = null
    log.warn("\n\n\nORU to SR Conversion: OBR-22 Segment did **NOT** contain a Valid Date Format.  ORU to SR Conversion might fail.")
  } else {
    // Parse the date and time
    contentDate = obr22[0..7]
    contentTime = obr22[8..13]
  }


 // Set Dates and Times
 if(null != obr6){
  // Use OBR-6 as primary source
    set(StudyDate, studyDate)
 } else if(null != obr7) {
  // Use OBR-7 as secondary source
    set(StudyDate, studyDateAlt)
 } else {
    throw new Exception ("ORU to SR Conversion: Invalid Date Formats in both OBR-6 and OBR-7.  Aborting SR to ORU Conversion\n")
 }

 if(null != obr6){
  // Use OBR-6 as primary source
    set(StudyTime, studyTime)
 } else if(null != obr7) {
  // Use OBR-7 as secondary source
    set(StudyTime, studyTimeAlt)
 } else {
    throw new Exception ("ORU to SR Conversion: Invalid Time Formats in both OBR-6 and OBR-7.  Aborting SR to ORU Conversion\n")
 }

 if(null != obr6){
    set(SeriesDate, studyDate)
 } else if(null != obr7) {
    set(SeriesDate, studyDateAlt)
 }

 if(null != obr6){
    set(SeriesTime, studyTime)
 } else if(null != obr7) {
    set(SeriesTime, studyTimeAlt)
 }

 if(null != obr22) {
    set(ContentDate, contentDate)
    set(ContentTime, contentTime)
 } else {
    throw new Exception ("ORU to SR Conversion: Invalid Date / Time Format in both OBR-22.  Aborting SR to ORU Conversion\n")
 }


// Some more top-level fields 
 set(AccessionNumber, acc_num)
 set(Modality, 'SR')

 set(StudyID, acc_num)

// Referring Physicians Name and Sequence
 setPersonName(ReferringPhysicianName, 'OBR-16', startingPosition=2)
 set('ReferringPhysicianIdentificationSequence/InstitutionName', get('MSH-4'))

 def seq = 'ReferringPhysicianIdentificationSequence/PersonIdentificationCodeSequence';
 set(seq+'/CodeValue', get('OBR-16-1'))
 set(seq+'/CodingSchemeDesignator', get('MSH-4'))
 set(seq+'/CodeMeaning', 'Referring Physician ID')

// Some more top-level fields 
 set(StationName, get('ORC-13'))
 set(StudyDescription, get('OBR-4-2'))

 def nameOfDrReadingStudy = get('OBR-32-2')
 nameOfDrReadingStudy = nameOfDrReadingStudy.replace(',', '^')
 set(NameOfPhysiciansReadingStudy, nameOfDrReadingStudy)

// Anatomic Region Sequence

 anatomicMap = [
    "AB":"Abdomen",
    "AN":"Angiographie",
    "AU":"Autre",
    "CA":"Cardio-vasculaire",
    "CO":"Colonnes",
    "DI":"Digestif",
    "EI":"Extrémités inférieures",
    "EN":"Endocrinien",
    "ES":"Extrémités supérieures",
    "GO":"Gynéco-obstétrique",
    "HE":"Hémodynamie",
    "HP":"Hématopoïétique",
    "SE":"Sein",
    "SQ":"Squelettique",
    "TC":"Tête et cou",
    "TH":"Thorax",
    "TP":"Tomographie par émission de positrons"
    ]

 def anatomicRegionCode = null
 def anatomicCodeDescription = null
 def anatomicCodeScheme = null

 // LakeShore sends the anatomic region in a different place
 if(studyFromLakeShore){
    anatomicRegionCode = get('OBR-15-4')
    anatomicCodeScheme = 'Imagerie Québec-DSQ'
 
 } else {
    anatomicRegionCode = get('OBR-15-1-1')
    
    if (null != get('OBR-15-1-3')) {
        anatomicCodeScheme = get('OBR-15-1-3')
    } else {
       log.warn("ORU to SR Report Conversion: Non-LakeShore ORU had a blank segment in OBR-15-1-3.  Falling back to default value, \"Imagerie Québec-DSQ\"")
       anatomicCodeScheme = 'Imagerie Québec-DSQ'
    }
 }

 // Derive Anatomic Region Code Display Name from Anatomic Region
 if(null != anatomicRegionCode) {
    if(anatomicMap[anatomicRegionCode] != null){
        anatomicCodeDescription = anatomicMap[anatomicRegionCode]
    } else {
        anatomicCodeDescription = 'Autre'
    }
 } else {
    anatomicRegionCode = "OT"
    anatomicCodeDescription = 'Autre'
 }

 set('AnatomicRegionSequence/CodeValue', anatomicRegionCode)
 set('AnatomicRegionSequence/CodingSchemeDesignator', anatomicCodeScheme)
 set('AnatomicRegionSequence/CodeMeaning', anatomicCodeDescription)
 


// Patient ID and Demographics
 setPersonName(PatientName, 'PID-5')
 set(PatientID, get('PID-3-1'))
 set(PatientBirthDate, get('PID-7'))
 set(PatientSex, get('PID-8'))

// Study Metadata
 set(InstanceNumber, '1')
 //  This is not needed: set(StudyStatusID, get('OBR-25'))
 set(ReasonForStudy, get('ORC-16'))
 if(studyFromLakeShore){
    def orc12_2 = get('ORC-12-2')
    orc12_2 = orc12_2.replace(',', '^')
    set(RequestingPhysician, orc12_2)
 } else {
    set(RequestingPhysician, get('ORC-12'))
 }
 set(RequestingService, get('MSH-4'))

// Requested Procedure Code Sequence
 set('RequestedProcedureCodeSequence/CodeValue', get('OBR-4-1'))
 log.info("\n\n\n\n\n\n\n\n\n\n THE CURRENT VALUE OF OBR-4-1 in HL7 ORU is: {}",get('OBR-4-1'))
 set('RequestedProcedureCodeSequence/CodingSchemeDesignator', 'Imagerie Québec-DSQ')
 set('RequestedProcedureCodeSequence/CodeMeaning', get('OBR-4-2'))

 set(RequestedProcedureID, acc_num)

// Current Patient Location
 currentLocation = get('PV1-3')
 if(null != currentLocation) {
    set(CurrentPatientLocation, currentLocation)
    log.info("\nORU To SR Conversion: Setting Current Patient Location to \"{}\"", currentLocation)
 }

set(ValueType, 'CONTAINER')

set('ConceptNameCodeSequence/CodeValue', '18748-4')
set('ConceptNameCodeSequence/CodingSchemeDesignator', 'LN')
set('ConceptNameCodeSequence/CodeMeaning', 'Diagnostic Imaging Report')

set(ContinuityOfContent, 'CONTINUOUS')

// Verifying Observer Sequence
 def obr32_2 = get('OBR-32-2')
    obr32_2 = obr32_2.replace(',', '^')
 set('VerifyingObserverSequence/VerifyingOrganization', get('MSH-4'))
 set('VerifyingObserverSequence/VerificationDateTime', get('OBR-7'))
 set('VerifyingObserverSequence/VerifyingObserverName', obr32_2)

 // Verifying Observer Identification code sub-sequence
   // The sequence itself is optional; if provided, however, all constituent values are mandatory
    seq = 'VerifyingObserverSequence/VerifyingObserverIdentificationCodeSequence'
    set(seq+'/CodeValue', get('OBR-32-1'))
    set(seq+'/CodingSchemeDesignator', get('MSH-4'))
    set(seq+'/CodeMeaning', obr32_2)


// Referenced Request Sequence
 set('ReferencedRequestSequence/AccessionNumber', acc_num)
 set('ReferencedRequestSequence/StudyInstanceUID', get('ZDS-1'))
 set('ReferencedRequestSequence/RequestedProcedureDescription', get('OBR-4-2'))
 set('ReferencedRequestSequence/RequestedProcedureID', acc_num)
 
 if(studyFromLakeShore) {
    // set('ReferencedRequestSequence/PlacerOrderNumberProcedure', get('OBR-19'))
    set('ReferencedRequestSequence/PlacerOrderNumberProcedure', get('OBR-3'))
 } else {
    set('ReferencedRequestSequence/PlacerOrderNumberProcedure', get('ORC-3'))
    // set('ReferencedRequestSequence/PlacerOrderNumberProcedure', get('ORC-2'))
 }

 set('ReferencedRequestSequence/FillerOrderNumberImagingServiceRequest', get('OBR-19'))
 // set('ReferencedRequestSequence/FillerOrderNumberProcedure', get('ORC-3'))

 seq = 'ReferencedRequestSequence/RequestedProcedureCodeSequence'
    set(seq+'/CodeValue', get('OBR-4-1'))
    set(seq+'/CodeMeaning', get('OBR-4-2'))
    if(studyFromLakeShore) {
        set(seq+'/CodingSchemeDesignator', get('MSH-4'))
    }



// Some more Top-Level Attributes
 set(CompletionFlag, 'COMPLETE')
 
 // Verification Flag
 if(studyFromLakeShore){
    log.info("Study is from Lakeshore, and OBX-11 has \"{}\" in its value.", get('OBX-11'))
   // Use OBX-11 for Lakeshore
    if (get('OBX-11') == 'F') {
        set(VerificationFlag, 'VERIFIED')
    } else {
        log.info("Study is NOT from Lakeshore, and OBX-11 has \"{}\" in its value.", get('OBX-11'))
        set(VerificationFlag, 'UNVERIFIED')
    }
    
 } else {
   // Use OBR-25 for everything else
    if (get('OBR-25') == 'F') {
        set(VerificationFlag, 'VERIFIED')
    } else {
        set(VerificationFlag, 'UNVERIFIED')
    }
 }

// ContentSequence
 set('ContentSequence[1]/RelationshipType', 'HAS CONCEPT MOD')
 set('ContentSequence[1]/ValueType', 'CODE')
 set('ContentSequence[1]/ConceptNameCodeSequence/CodeValue', '121058')
 set('ContentSequence[1]/ConceptNameCodeSequence/CodingSchemeDesignator', 'DCM')
 set('ContentSequence[1]/ConceptNameCodeSequence/CodeMeaning', 'Procedure Reported')

 set('ContentSequence[1]/ConceptCodeSequence/CodeValue', get('OBR-4-1'))
 set('ContentSequence[1]/ConceptCodeSequence/CodingSchemeDesignator', 'RP')
 set('ContentSequence[1]/ConceptCodeSequence/CodeMeaning', get('OBR-4-2'))
 
 set('ContentSequence[2]/RelationshipType', 'HAS CONCEPT MOD')
 set('ContentSequence[2]/ValueType', 'CODE')
 set('ContentSequence[2]/ConceptNameCodeSequence/CodeValue', '121049')
 set('ContentSequence[2]/ConceptNameCodeSequence/CodingSchemeDesignator', 'DCM')
 set('ContentSequence[2]/ConceptNameCodeSequence/CodeMeaning', 'Language of Content Item and Descendants')

 set('ContentSequence[2]/ConceptCodeSequence/CodeValue', 'fr') // or en?
 set('ContentSequence[2]/ConceptCodeSequence/CodingSchemeDesignator', 'RFC3066')
 set('ContentSequence[2]/ConceptCodeSequence/CodeMeaning', 'French') // or English?

 set('ContentSequence[3]/RelationshipType', 'HAS OBS CONTEXT')
 set('ContentSequence[3]/ValueType', 'CODE')
 set('ContentSequence[3]/ConceptNameCodeSequence/CodeValue', '121005')
 set('ContentSequence[3]/ConceptNameCodeSequence/CodingSchemeDesignator', 'DCM')
 set('ContentSequence[3]/ConceptNameCodeSequence/CodeMeaning', 'Observer Type')

 set('ContentSequence[3]/ConceptCodeSequence/CodeValue', '121006')
 set('ContentSequence[3]/ConceptCodeSequence/CodingSchemeDesignator', 'DCM')
 set('ContentSequence[3]/ConceptCodeSequence/CodeMeaning', 'Person')

 set('ContentSequence[4]/RelationshipType', 'HAS OBS CONTEXT')
 set('ContentSequence[4]/ValueType', 'CODE')
 set('ContentSequence[4]/ConceptNameCodeSequence/CodeValue', '121011')
 set('ContentSequence[4]/ConceptNameCodeSequence/CodingSchemeDesignator', 'DCM')
 set('ContentSequence[4]/ConceptNameCodeSequence/CodeMeaning', 'Person Observer\'s Role in this Procedure')

 set('ContentSequence[4]/ConceptCodeSequence/CodeValue', '121097')
 set('ContentSequence[4]/ConceptCodeSequence/CodingSchemeDesignator', 'DCM')
 set('ContentSequence[4]/ConceptCodeSequence/CodeMeaning', 'Recording')
 
 set('ContentSequence[5]/RelationshipType', 'CONTAINS')
 set('ContentSequence[5]/ValueType', 'CONTAINER')
 set('ContentSequence[5]/ConceptNameCodeSequence/CodeValue', '121064')
 set('ContentSequence[5]/ConceptNameCodeSequence/CodingSchemeDesignator', 'DCM')
 set('ContentSequence[5]/ConceptNameCodeSequence/CodeMeaning', 'Current Procedure Description')
 set('ContentSequence[5]/ContinuityOfContent', 'CONTINUOUS')

 // Content Sequence [5] "Content Sequence" subsequence
 seq = 'ContentSequence[5]/ContentSequence'
    set(seq+'/RelationshipType', 'CONTAINS')
    set(seq+'/ValueType', 'TEXT')
    set(seq+'/ConceptNameCodeSequence/CodeValue', '121065')
    set(seq+'/ConceptNameCodeSequence/CodingSchemeDesignator', 'DCM')
    set(seq+'/ConceptNameCodeSequence/CodeMeaning', 'Procedure Description')
    set(seq+'/TextValue', get('OBR-4-1') + '^' + get('OBR-4-2'))

 set('ContentSequence[6]/RelationshipType', 'CONTAINS')
 set('ContentSequence[6]/ValueType', 'CONTAINER')
 set('ContentSequence[6]/ContinuityOfContent', 'CONTINUOUS')
 set('ContentSequence[6]/ConceptNameCodeSequence/CodeValue', '121070')
 set('ContentSequence[6]/ConceptNameCodeSequence/CodingSchemeDesignator', 'DCM')
 set('ContentSequence[6]/ConceptNameCodeSequence/CodeMeaning', 'Findings')

 // Content Sequence [6] "Content Sequence" subsequence
 seq = 'ContentSequence[6]/ContentSequence'
 set(seq+'/RelationshipType', 'CONTAINS') // Not sure if this line-item should actually be here
 set(seq+'/ValueType', 'TEXT')
 set(seq+'/ConceptNameCodeSequence/CodeValue', '121071')
 set(seq+'/ConceptNameCodeSequence/CodingSchemeDesignator', 'DCM')
 set(seq+'/ConceptNameCodeSequence/CodeMeaning', 'Finding')

 set(seq+'/TextValue', getList('OBSERVATION(*)/OBX-5(*)').join('\n'))

 set('ContentSequence[7]/RelationshipType', 'CONTAINS')
 set('ContentSequence[7]/ValueType', 'CONTAINER')
 set('ContentSequence[7]/ContinuityOfContent', 'SEPARATE')

 // Concept Name Code Sequence
 set('ContentSequence[7]/ConceptNameCodeSequence/CodeValue', '121072')
 set('ContentSequence[7]/ConceptNameCodeSequence/CodingSchemeDesignator', 'DCM')
 set('ContentSequence[7]/ConceptNameCodeSequence/CodeMeaning', 'Impressions')

 // don't yet know how to distinguish impression for regular findings
 set('ContentSequence[7]/ConceptCodeSequence/CodeValue', '**What to put here??** -Rodrigo')
 set('ContentSequence[7]/ConceptCodeSequence/CodingSchemeDesignator','**What to put here??** -Rodrigo')
 set('ContentSequence[7]/ConceptCodeSequence/CodeMeaning', '**What to put here??** -Rodrigo')

 set('ContentSequence[8]/ValueType', 'CONTAINER')
 set('ContentSequence[8]/ContinuityOfContent', 'CONTINUOUS') // Does this make sense???
 set('ContentSequence[8]/ContentSequence/RelationshipType', 'CONTAINS')
 set('ContentSequence[8]/ContentSequence/ValueType', 'TEXT')
 seq = 'ContentSequence[8]/ContentSequence/ConceptNameCodeSequence'
 set(seq+'/CodeValue', '121073')
 set(seq+'/CodingSchemeDesignator', 'DCM')
 set(seq+'/CodeMeaning', 'Impression')
 set('ContentSequence[8]/ContentSequence/TextValue', getList('OBSERVATION(*)/OBX-5(*)').join('\n'))


/*seq = 'ContentSequence[7]/ContentSequence'
set(seq+'/RelationshipType', 'CONTAINS')
set(seq+'/ValueType', 'TEXT')
set(seq+'/ConceptNameCodeSequence/CodeValue', '121073')
set(seq+'/ConceptNameCodeSequence/CodingSchemeDesignator', 'DCM')
set(seq+'/ConceptNameCodeSequence/CodeMeaning', 'Impression')
set(seq+'/TextValue', '');
*/

