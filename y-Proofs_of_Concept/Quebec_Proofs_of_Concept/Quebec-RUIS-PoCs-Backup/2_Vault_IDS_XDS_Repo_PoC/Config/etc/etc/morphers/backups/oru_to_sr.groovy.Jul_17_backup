/**
 * converts HL7 ORU to a DICOM SR object
 * draft according to the SAD v0.5
 *
 * many of the HL7 locations are potentially different in the field. this 
 * script is simply a starting point.
 *
 * This is Script v0.2
 */
import org.dcm4che2.util.UIDUtils
import org.joda.time.DateTime

// Start Required Fields
 set(SpecificCharacterSet, 'ISO_IR_100')
 set(InstanceCreationDate, new DateTime())
 set(InstanceCreationDate, new DateTime())
 set(InstanceCreatorUID, '1.2.3.4.5.6.7.8.9')
    // '1.2.3.4.5.6.7.8.9' Is a number that should change for each specific instance / deployment of Vault
    
 set(SOPClassUID, '1.2.840.10008.5.1.4.1.1.88.11') // basic SR
 set(SOPInstanceUID, UIDUtils.createUID(uid_root))

 def acc_num = get('OBR-19')
    // Value -seemingly erroneous- used on v.0.1 def acc_num = get('OBR-19')
 if(null != acc_num){
    set(AccessionNumber, acc_num)
 } else {
    log.error("\nHL7 ORU to DICOM SR Report Conversion: Accession Number in the Received HL7 ORU Report was Blank!!!\nThere will be issues idenfying its Manifest.  Using fallback Accession \"Number 12345678901234\"\n\n")
    set(AccessionNumber, '12345678901234')
 }

 set(Modality, 'SR')

 // Set Modalities In Study
 def modaltiesInStudy = get('OBR-24')
 if(null != modalitiesInStudy){
    set(ModalitiesInStudy, modalitiesInStudy) 
 } else {
    log.warn("HL7 ORU to DICOM SR Report Conversion: Modality field in received HL7 ORU Repor was Blank!\nFalling back to default modality \"CR\"\n")
    set(ModalitiesInStudy, 'CR')
 }

 // Set Patient Name
 if(null == get('PID-5')){
    throw new Exception("\n\nHL7 ORU to DICOM SR Report Conversion: Patient Name passed in HL7 ORU Report was BLANK.  Aborting conversion.\n\n")
 } else {
    setPersonName(PatientName, 'PID-5')
 }

 // Set Patient ID
 if(null == get('PID-3-1')){
    throw new Exception("\n\nHL7 ORU to DICOM SR Report Conversion: Patient ID  passed in HL7 ORU Report was BLANK.  Aborting conversion.\n\n")
 } else {
    set(PatientID, get('PID-3-1'))
 }
 
 // Set issuer of PID if present, if not, assume it's a Lakeshore study
 def issuerOfPID = get('PID-3-4-2')
 if(null != issuerOfPID) {
    set(IssuerOfPatientID, '2.16.124.10.101.1.60.1.1006.1')
    //set(IssuerOfPatientID, get('PID-3-4-2')) // or inferred
    log.warn("\n\nHL7 ORU Report to DICOM SR Conversion: Received Patient ID did not have an explicit Issuer Of Patient ID.  Assuming this Report is from Hopital General du Lakeshore! \n\n")
 } else {
    set(IssueOfPatientID, issuerOfPID)
 }

 // Set Study Instance UID
 if(null == get('ZDS-1')) {
    throw new Exception("\n\nHL7 ORU to DICOM SR Report Conversion: Study Istance UID passed in HL7 ORU Report was BLANK. (This ID References the study that the Report was generated for)\n  Aborting conversion.\n\n")
 } else {
    set(StudyInstanceUID, get('ZDS-1'))
 }

 // Set Series Istance UID
 set(SeriesInstanceUID, UIDUtils.createUID(uid_root))

 set(InstanceNumber, '1')

 // Set Study Status ID
 if(null == get('OBR-25')){
    log.warn("HL7 ORU to DICOM SR Report Conversion: \"Study Result Status\" passed in HL7 ORU Report was BLANK.  Defaulting to Status ID of \"F\"")
    set(StudyStatusID, 'F') 
 } else {
    set(StudyStatusID, get('OBR-25'))
 }


 // Set Requested Procedure Code Sequence
 if(null == get('OBR-4-1')){
    log.warn("HL7 ORU to DICOM SR Report Conversion: Procedure Code (i.e Universal Service Identifier) passed in HL7 ORU Report was BLANK.  Defaulting to Procedure Code \"74441\"")
    set('RequestedProcedureCodeSequence/CodeValue', '74441') 
 } else {
    set('RequestedProcedureCodeSequence/CodeValue', get('OBR-4-1'))
 }
 
 set('RequestedProcedureCodeSequence/CodingSchemeDesignator', 'Imagerie Québec-DSQ')

 if(null == get('OBR-4-2')) {
    log.warn("HL7 ORU to DICOM SR Report Conversion: Procedure Code Code Meaning passed in HL7 ORU Report was BLANK.  Defaulting to Procedure Code Meaning \"8021 - Waynescopie\"")
    set('RequestedProcedureCodeSequence/CodeMeaning','8021 - Waynescopie') 
 } else {
    set('RequestedProcedureCodeSequence/CodeMeaning', get('OBR-4-2'))
 }


 set(ValueType, 'CONTAINER')


 // Set Concept Name Code Sequence
 set('ConceptNameCodeSequence/CodeValue', '18748-4')
 set('ConceptNameCodeSequence/CodingSchemeDesignator', 'LN')
 set('ConceptNameCodeSequence/CodeMeaning', 'Diagnostic Imaging Report')
 

// ****** End of Required fields *****


// not specified in SAD so far
def uid_root = '2.25.123456' // this must be no longer than 23 chars

set(StudyID, acc_num)

// END not specified

set(StudyDate, get('OBR-7'))
set(StudyTime, get('OBR-7'))
set(SeriesDate, get('OBR-7'))
set(SeriesTime, get('OBR-7'))

set(ContentDate, get('OBR-22'))

setPersonName(ReferringPhysicianName, 'OBR-16', startingPosition=2)
set('ReferringPhysicianIdentificationSequence/InstitutionName', get('MSH-4'))

def seq = 'ReferringPhysicianIdentificationSequence/PersonIdentificationCodeSequence';
if(null == get('OBR-16-1')){
    // Set to deafult Phyisician License if Referring Physician License is blank in passed HL7 Message
    log.error("HL7 ORU Report to DICOM SR Conversion: Referring Physician License is BLANK in passed HL7 Message.  Defaulting to Paul, LaPotion's License.  Which might be wrong.")
    set(seqt+'/CodeValue', "726300")
} else {
    set(seq+'/CodeValue', get('OBR-16-1'))
}

if(null == get('MSH-4')){
    // Set to default Sending Facility if Sending Faciliy is blank in passed HL7 Message
    log.error("HL7 ORU Report to DICOM SR Conversion: Sending Faciliy is BLANK in passed HL7 Message. Defaulting to \"RISA\", which might be wrong") 
    set(seq+'/CodingSchemeDesignator', get('MSH-4'))
} else {
    set(seq+'/CodingSchemeDesignator', get('MSH-4'))
}
set(seq+'/CodeMeaning', 'Referring Physician ID')

set(StationName, get('ORC-13'))
set(StudyDescription, get('OBR-4-2'))
set(NameOfPhysiciansReadingStudy, get('OBR-32-2'))


set('AnatomicRegionSequence/CodeValue', get('OBR-15-1-1'))
set('AnatomicRegionSequence/CodingSchemeDesignator', get('OBR-15-1-3'))
set('AnatomicRegionSequence/CodeValue', get('OBR-15-1-2'))


set(PatientBirthDate, get('PID-7'))
set(PatientSex, get('PID-8'))

set(ReasonForStudy, get('ORC-16'))
set(RequestingPhysician, get('ORC-12'))
set(RequestingService, get('MSH-4'))

set(CurrentPatientLocation, get('PV-1-3'))

set(RequestedProcedureID, acc_num)


set(ContinuityOfContent, 'CONTINUOUS')

set('VerifyingObserverSequence/VerifyingOrganization', get('MSH-4'))
set('VerifyingObserverSequence/VerificationDateTime', get('OBR-7'))
set('VerifyingObserverSequence/VerifyingObserverName', get('OBR-32-2'))
seq = 'VerifyingObserverSequence/VerifyingObserverIdentificationCodeSequence'
set(seq+'/CodeValue', get('OBR-32-1'))
set(seq+'/CodingSchemeDesignator', get('MSH-4'))
set(seq+'/CodeMeaning', get('OBR-32-2'))

set('ReferencedRequestSequence/AccessionNumber', acc_num)
set('ReferencedRequestSequence/StudyInstanceUID', get('ZDS-1'))
set('ReferencedRequestSequence/RequestedProcedureDescription', get('OBR-4-2'))
set('ReferencedRequestSequence/RequestedProcedureID', RequestedProcedureID)
set('ReferencedRequestSequence/PlacerOrderNumberProcedure', get('ORC-2'))
set('ReferencedRequestSequence/FillerOrderNumberProcedure', get('ORC-3'))

seq = 'ReferencedRequestSequence/RequestedProcedureCodeSequence'
set(seq+'/CodeValue', get('OBR-4-1'))
set(seq+'/CodeMeaning', get('OBR-4-2'))

set(CompletionFlag, 'COMPLETE')
if (get('OBR-25') == 'F') {
    set(VerificationFlag, 'VERIFIED')
} else {
    set(VerificationFlag, 'UNVERIFIED')
}

set('ContentSequence[1]/RelationshipType', 'HAS CONCEPT MOD')
set('ContentSequence[1]/ValueType', 'CODE')
set('ContentSequence[1]/ConceptNameCodeSequence/CodeValue', '121058')
set('ContentSequence[1]/ConceptNameCodeSequence/CodingSchemeDesignator', 'DCM')
set('ContentSequence[1]/ConceptNameCodeSequence/CodeMeaning', 'Procedure Reported')

set('ContentSequence[1]/ConceptCodeSequence/CodeValue', get('OBR-4-1'))
set('ContentSequence[1]/ConceptCodeSequence/CodingSchemeDesignator', 'RP')
set('ContentSequence[1]/ConceptCodeSequence/CodeMeaning', get('OBR-4-2'))

set('ContentSequence[2]/RelationshipType', 'HAS CONCEPT MOD')
set('ContentSequence[2]/ValueType', 'CODE')
set('ContentSequence[2]/ConceptNameCodeSequence/CodeValue', '121049')
set('ContentSequence[2]/ConceptNameCodeSequence/CodingSchemeDesignator', 'DCM')
set('ContentSequence[2]/ConceptNameCodeSequence/CodeMeaning', 'Language of Content Item and Descendants')

set('ContentSequence[2]/ConceptCodeSequence/CodeValue', 'en') // or fr?
set('ContentSequence[2]/ConceptCodeSequence/CodingSchemeDesignator', 'RFC3066')
set('ContentSequence[2]/ConceptCodeSequence/CodeMeaning', 'English') // or french


set('ContentSequence[3]/RelationshipType', 'HAS OBS CONTEXT')
set('ContentSequence[3]/ValueType', 'CODE')
set('ContentSequence[3]/ConceptNameCodeSequence/CodeValue', '121005')
set('ContentSequence[3]/ConceptNameCodeSequence/CodingSchemeDesignator', 'DCM')
set('ContentSequence[3]/ConceptNameCodeSequence/CodeMeaning', 'Observer Type')

set('ContentSequence[3]/ConceptCodeSequence/CodeValue', '121006')
set('ContentSequence[3]/ConceptCodeSequence/CodingSchemeDesignator', 'DCM')
set('ContentSequence[3]/ConceptCodeSequence/CodeMeaning', 'Person')

set('ContentSequence[4]/RelationshipType', 'HAS OBS CONTEXT')
set('ContentSequence[4]/ValueType', 'CODE')
set('ContentSequence[4]/ConceptNameCodeSequence/CodeValue', '121011')
set('ContentSequence[4]/ConceptNameCodeSequence/CodingSchemeDesignator', 'DCM')
set('ContentSequence[4]/ConceptNameCodeSequence/CodeMeaning', 'Person Observer\'s Role in this Procedure')

set('ContentSequence[4]/ConceptCodeSequence/CodeValue', '121097')
set('ContentSequence[4]/ConceptCodeSequence/CodingSchemeDesignator', 'DCM')
set('ContentSequence[4]/ConceptCodeSequence/CodeMeaning', 'Recording')


set('ContentSequence[5]/RelationshipType', 'CONTAINS')
set('ContentSequence[5]/ValueType', 'CONTAINER')
set('ContentSequence[5]/ConceptNameCodeSequence/CodeValue', '121064')
set('ContentSequence[5]/ConceptNameCodeSequence/CodingSchemeDesignator', 'DCM')
set('ContentSequence[5]/ConceptNameCodeSequence/CodeMeaning', 'Current Procedure Description')
set('ContentSequence[5]/ContinuityOfContent', 'CONTINUOUS')

seq = 'ContentSequence[5]/ContentSequence'

set(seq+'/RelationshipType', 'CONTAINS')
set(seq+'/ValueType', 'TEXT')
set(seq+'/ConceptNameCodeSequence/CodeValue', '121065')
set(seq+'/ConceptNameCodeSequence/CodingSchemeDesignator', 'DCM')
set(seq+'/ConceptNameCodeSequence/CodeMeaning', 'Procedure Description')
set(seq+'/TextValue', get('OBR-4-1') + '^' + get('OBR-4-2'))

set('ContentSequence[6]/RelationshipType', 'CONTAINS')
set('ContentSequence[6]/ValueType', 'CONTAINER')
set('ContentSequence[6]/ConceptNameCodeSequence/CodeValue', '121070')
set('ContentSequence[6]/ConceptNameCodeSequence/CodingSchemeDesignator', 'DCM')
set('ContentSequence[6]/ConceptNameCodeSequence/CodeMeaning', 'Findings')

seq = 'ContentSequence[6]/ContentSequence'
set(seq+'/ValueType', 'TEXT')
set(seq+'/ConceptNameCodeSequence/CodeValue', '121071')
set(seq+'/ConceptNameCodeSequence/CodingSchemeDesignator', 'DCM')
set(seq+'/ConceptNameCodeSequence/CodeMeaning', 'Finding')
set(seq+'/TextValue', getList('OBSERVATION(*)/OBX-5(*)').join('\n'))


// don't yet know how to distinguish impression for regular findings
/*
set('ContentSequence[7]/RelationshipType', 'CONTAINS')
set('ContentSequence[7]/ValueType', 'CONTAINER')
set('ContentSequence[7]/ConceptNameCodeSequence/CodeValue', '121072')
set('ContentSequence[7]/ConceptNameCodeSequence/CodingSchemeDesignator', 'DCM')
set('ContentSequence[7]/ConceptNameCodeSequence/CodeMeaning', 'Impressions')

seq = 'ContentSequence[7]/ContentSequence'
set(seq+'/RelationshipType', 'CONTAINS')
set(seq+'/ValueType', 'TEXT')
set(seq+'/ConceptNameCodeSequence/CodeValue', '121073')
set(seq+'/ConceptNameCodeSequence/CodingSchemeDesignator', 'DCM')
set(seq+'/ConceptNameCodeSequence/CodeMeaning', 'Impression')
set(seq+'/TextValue', '');
*/

log.info("XDS Manifest Publishing debug:\n This is the SR to be published:\n{}\n\n\n", output)
