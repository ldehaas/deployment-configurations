// Connect should receive traffic from Lakeshore ALL ADT requests.
// Non-Lakeshore studies ADTs are *not* needed (as Connect only services Lakeshore)

def hl7Type = get('MSH-9-1')
hl7Type = hl7Type.toUpperCase()
log.debug("Local Vault ADT Forwarder: Received MSH-9-1 was: \"{}\"", hl7Type)

if("ADT" != hl7Type){
    return false
}

def studyFromLakeShore = false

if ("SVR-TSERV0" == get('MSH-4')) {
    studyFromLakeShore = true
    log.info("Connect ADT Forwarder: This ADT is from Lakeshore and will be sent to the Infrasctructure XDS-i Consumer Connect.")
}

if(!studyFromLakeShore) {
   log.info("This ADT is *not* from Lakeshore.  Ignoring")
   return false
}


def encoding = get("MSH-18")
if (encoding == null || encoding.isEmpty()){
    log.info("No character set specified.  Hard-coding to 8859/1")
    set('MSH-18', '8859/1')
}  

