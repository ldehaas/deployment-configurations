def hl7Type = get('MSH-9-1')
hl7Type = hl7Type.toUpperCase()
log.debug("Local Vault ORU Report Forwarder: Received MSH-9-1 was: \"{}\"", hl7Type)

if("ORU" != hl7Type){
    return false
}

log.info("Controlling MSH-18 on ORU Receiver: \"{}\"", get('MSH-18'))

def encoding = get("MSH-18")
if (encoding == null || encoding.isEmpty()){
    log.info("No character set specified.  Hard-coding to 8859/1")
    set('MSH-18', '8859/1')
}

log.info("Controlling the outgoing traffic to the *internal* ORU server \n {}", output)
