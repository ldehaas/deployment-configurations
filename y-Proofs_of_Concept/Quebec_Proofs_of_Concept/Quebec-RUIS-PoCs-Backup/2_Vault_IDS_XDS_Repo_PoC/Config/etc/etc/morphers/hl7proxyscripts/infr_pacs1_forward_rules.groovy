def sendingFacility = get('MSH-4')
log.debug("Lakeshore PACS HL7 Forwarder: MSH-4 Sending Facility was \"{}\"", sendingFacility)

if ("SVR-TSERV0" != sendingFacility) {
    // The Lakeshore PACS SHOULD only get traffic from Lakeshore RIS
    return false
}
