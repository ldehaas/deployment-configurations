/* 
The Mini-Pix uses 'update' messages to create patient links (Local MRN to RAMQ).
The supported 'update' messages are A01, A04, A05 and A08 
    A01 ADMIT A PATIENT
    A04 REGISTER A PATIENT
    A05 PREADMIT A PATIENT
    A08 UPDATE PATIENT INFORMATION
    A31 UPDATE PERSON INFORMATION

The Mini-Pix uses 'merge' messages to say that 2 MRNs are actually 1.  The 'surviving' MRN can exist or be a new MRN.
In either case, if the RAMQ is populated correctly, MiniPIX will update the RAMQ to MRN link correctly.
The supported 'merge' messages are A34, A36, A39, A40 and A43.
    A34 MERGE PATIENT INFORMATION - PATIENT ID ONLY
    A36 MERGE PATIENT INFORMATION - PATIENT ID AND ACCOUNT NUMBER
*/

// Outputting Received Object from internal HL7 Proxy (@ Vault)
log.debug("Outputting Received Object from internal HL7 Proxy:\n {}", input.toString().replaceAll("\r","\n"))


// Logic to Parse Message types and valid ADT message subtype
 def messageType = null
 def messageSubType = null

 def validSubMessage = false

 def ignoreMessage = false

 messageType = get('MSH-9-1')
    log.info("Vault Internal PIX Manager: This is an \"{}\" message.", messageType) 
 messageSubType = get('MSH-9-2')
    log.info("The message subtype is \"{}\".", messageSubType)

 if (null != messageType){
    messageType = messageType.toUpperCase()
    if('ADT' != messageType){
        log.info("Non-ADT Message Type.  Ignoring")
        return false
    }
 } else {
    log.info("Message Type not Specified. Aborting.")
    throw new Exception("HL7 Message Type not Specified. Aborting.")
 }


 if(null != messageSubType){
    //Checking for the ADT Message subtype
    messageSubType = messageSubType.toUpperCase()
    log.info("Controlling the message subtype to check for valid message types: \"{}\"", messageSubType)
    
    validSubMessage = [ 
        "A01", "A04", "A05", "A08", "A31", "A34", "A36"
    ]

    // Checking to see if the receive ADT Message is a valid message that the Mini-PIX can handle
    if( ! validSubMessage.contains(messageSubType) ){
        log.info("Unsupported (and likely irrelevant) ADT Message Subtype.  Ignoring.")
        return false
    }

 }



// Logic to assing Issuer of PIDs to local MRNs

 def msh4 = get('MSH-4')
 log.info("Controlling simply what appears in MSH-4: \"{}\"", msh4)
 //set('MSH-4', msh4)
 
 /*
 } else if (null==get('PID-3-4-2')) {
    set('PID-3-4-2', '2.16.124.10.101.1.60.1.1007.1')
    // This is for Lakeshore
    // I previously had the erroneous value: '2.16.124.10.101.1.60.1.1006.1'
    log.info("The PIX is assuming this Patient is from Lakeshore")
 */


 def SteJustineMessage = false
 def sendingFacility = get('MSH-4')

 // This is to set the local Affinity Domain because it is missing in their ADT Feed
 if ("SHSJPACSTST2" == sendingFacility){
    set('PID-3-4-2', '2.16.124.10.101.1.60.1.1004.1')
    SteJustineMessage = true
    log.info("The PIX is assuming this Patient is from Ste-Justine (fake study)")
 } else if ("HSJ" == sendingFacility) {
    set('PID-3-4-2', '2.16.124.10.101.1.60.1.1004.1')
    SteJustineMessage = true
    log.info("The PIX is assuming this Patient is from Ste-Justine (real study)")
 } else if ("SVR-TSERV0" == sendingFacility){
    set('PID-3-4-2', '2.16.124.10.101.1.60.1.1007.1')
    log.info("The PIX is assuming this Patient is from Lakeshore (fake study)")
 } else if("LKSHR" == sendingFacility) {
    set('PID-3-4-2', '2.16.124.10.101.1.60.1.1007.1')
    log.info("The PIX is assuming this Patient is from Lakeshore (real study)")
 } else {
    throw new Exception("Unrecognized HL7 Sending Facility, aborting")
 }


// Logic to deal with incomplete messages
 
 // Abort messages with null MRNs (i.e. PIX won't be able to add them to the db)
 def mrn = get('PID-3-1')
 if(null == mrn){
    if(SteJustineMessage){
        log.warn("This Ste-Justine ADT has *NO* local MRN. Aborting")
        throw new Exception("This Ste-Justine ADT has *NO* local MRN. Aborting")
    } else{
        log.warn("This Lakeshore ADT has *NO* local MRN. Aborting")
        throw new Exception("This Lakeshore ADT has *NO* local MRN. Aborting")
    }
 }

 // Abort message with null RAMQ values (i.e. PIX won't be able to add them to the db)
 def ramq = get('PID-2-1') 
 if(null == ramq) {
    if(SteJustineMessage){
        throw new Exception("This Ste-Justine ADT has *NO* RAMQ specified!!  Aborting")
        //log.info("This Ste-Justine Message has no RAMQ Value.  Defaulting to \"{}\"", mrn)
        //set('PID-2-1', mrn)
    } else {
        throw new Exception("This Lakeshore ADT has *NO* RAMQ specified!!  Aborting")
    }
 }

 // Populate the RAMQ Issuer of Patient ID (i.e top-level oid) when missing
 def ramqIssuerOfPID = get('PID-2-4-2') 
 if(null == ramqIssuerOfPID) {
    if(SteJustineMessage){
        log.info("Ste-Justine Message - RAMQ Issuer of PID (Affinity Domain) is *blank*.  Hardcoding it.")
    } else {
        log.info("Lakeshore Message - RAMQ Issuer of PID (Affinity Domain) is *blank*.  Hardcoding it.")
    }

    // This is the RAMQ Issuer Of PID
    //set('PID-2-4-2', '2.16.124.10.101.1.60.100')
    
    // As a hack, set the NIU Issuer of PID in PID-2-4-2
    set('PID-2-4-2','2.16.840.1.113883.4.56')

    set('PID-2-4-3', 'ISO')
 } else{
    // As a hack, set the NIU Issuer of PID in PID-2-4-2
    set('PID-2-4-2','2.16.840.1.113883.4.56')

    set('PID-2-4-3', 'ISO')
 }


log.debug("Outputting what is getting passed to PIX: \n {}", output.toString().replaceAll("\r","\n"))
