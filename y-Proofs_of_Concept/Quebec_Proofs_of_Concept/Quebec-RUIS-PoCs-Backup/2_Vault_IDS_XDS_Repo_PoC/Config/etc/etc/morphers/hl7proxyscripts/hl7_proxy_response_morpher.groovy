
def timestamp = get('MSH-7')

if(null != timestamp) {
    // Chop the subsecond part of the timestamp
    log.info("Removing sub-second part of the time.  The previous timestap was \"{}\"", timestamp)
    timestamp = timestamp.replaceAll("\\..*", "")
    log.info("The current timestap is \"{}\"", timestamp)
    set('MSH-7', timestamp)
}

log.info("Outputting HL7 Proxy Response Message to Caller:\n{}", output)


