// The DIR should get ADTs and ORMs

/* This DIR entry only references the 10.241.40.230 McKesson DIR Instance.
In order to keep thier logs clean, only Ste-Justine Traffic should be sent to this DIR Destination
*/

def characterSet = null
def sendingApplication = null
def sendingFacility = null
def studyFromLakeShore = false
def studyFromSteJustine = false

// Check Character Set.  If not set, explicitly set it to Unicode to support French Characters
def encoding = get("MSH-18")
if (encoding == null || encoding.isEmpty()){
    log.info("No character set specified.  Hard-coding to 8859/1")
    set('MSH-18', '8859/1')
}  

if ("HSJ" == get('MSH-4')) {
    studyFromSteJustine = true
}  else if ("LKSHR" == get ('MSH-4')) {
    studyFromLakeShore = true 
}

if (studyFromLakeShore){
    log.info("This is a study from Lakeshore.  Passing it through to the 10.241.40.230 McKesson DIR for PoC Phase 2. (This DIR's logs are dedicated for Lakeshore traffic)")
} else {
    log.info("This is HL7 Traffic is NOT coming from Lakeshore. Ignoring")
    return false
}

/*
def hl7Type = get("MSH-9-1")
hl7Type = hl7Type.toUpperCase()

if('ADT' != hl7Type){
    if('ORM' != hl7Type) {
        return false
    }
}*/
