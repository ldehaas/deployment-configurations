log.debug("Morphing PIF")

set("/.MSH-3", "RIALTO_CG")
set("/.MSH-4", "KAROS_CG")
set("/.MSH-5", "RIALTO")
set("/.MSH-6", "RIALTO_EMPI")

def patientName = get(PatientName)
def patientNameParts = patientName.split("\\^")
log.debug("Full Patient Name={}", patientName)

def nameInAdt = null
if (patientNameParts.length >= 2) {
    def patientFamilyName = patientNameParts[0]
    log.debug("Family Name={}", patientFamilyName)
    def patientGivenName = patientNameParts[1] 
    def patientGivenNameParts = patientGivenName.split(" ")
    log.debug("Given Name={}", patientGivenNameParts[0])
    nameInAdt = patientFamilyName + patientGivenNameParts[0]
    set("PID-5-1", patientFamilyName)
    set("PID-5-2", patientGivenNameParts[0])
} else {
    nameInAdt = patientName
    set("PID-5-1", patientName)
}

def patientSex = parseSex(get(PatientSex))
def patientDob = get(PatientBirthDate)

def fabricatedAffinityPid = nameInAdt + patientDob + patientSex
fabricatedAffinityPid = fabricatedAffinityPid.replaceAll("\\s","")
fabricatedAffinityPid = fabricatedAffinityPid.replaceAll(",","")
fabricatedAffinityPid = fabricatedAffinityPid.toUpperCase()
def fabricatedPidNamespaceID = "RIALTO"
def fabricatedPidUniversalID = "EMPI_CROSS_AFFINITY_DOMAIN_ID"
def fabricatedPidUniversalIDType = "ISO"

set("PID-3(0)-4-1", "CG")

set("PID-3(1)-1", fabricatedAffinityPid)
set("PID-3(1)-4-1", fabricatedPidNamespaceID)
set("PID-3(1)-4-2", fabricatedPidUniversalID)
set("PID-3(1)-4-3", fabricatedPidUniversalIDType)

set("PID-7", patientSex)
set("PID-8", patientDob)
