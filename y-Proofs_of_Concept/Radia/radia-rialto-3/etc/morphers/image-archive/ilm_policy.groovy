import org.joda.time.LocalDateTime;
import org.joda.time.Duration;

if (study.isOlderThan(Duration.standardDays(90))) {
    ops.scheduleStudyHardDelete();
}

log.trace("configurable policy DONE with study {}", study.getStudyInstanceUID());
