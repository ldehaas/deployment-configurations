log.info("IA Inbound Order Morpher - START")

//The accession number should be in ORC 3.1.  If ORC 3.1 is blank, then pull from ORC 2.1.
def ORC31 = get('ORC-3-1')
def ORC21 = get('ORC-2-1')

if (ORC31 == null || ORC31 == ''){
    if (ORC21 == null || ORC == ''){
        log.info("Both ORC-2-1 and ORC-3-1 are blank")
        return false
    } else {
       set('OBR-18', ORC21)
    }
} else {
    set('OBR-18', ORC31)
}

// RADIA confirmed Study Description should already be in OBR-4-2

set('PID-3-4-1', 'CG')
set('PID-3-4-2', 'county.general')
set('PID-3-4-3', 'ISO')

log.info("IA Inbound Order Morpher - END")
