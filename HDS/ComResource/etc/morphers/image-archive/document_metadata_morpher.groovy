// use extended metadata to keep track of study instance uid
set(XDSExtendedMetadata('studyInstanceUid'), get('StudyInstanceUID'))
set(XDSExtendedMetadata('accessionNumber'), get('AccessionNumber'))
set(XDSCreationTime, get('StudyDate'))

set(XDSLanguage, 'English')

// these codes are environment specific. 
set(XDSTypeCode, code('Test Type Code', 'Test Scheme', 'Test Type Code'))

set(XDSClassCode, 
    code('DICOM Study Manifest', 'Karos Health demo classCodes', 'DICOM Study Manifest'))
    
set(XDSHealthcareFacilityTypeCode, 
    code('Hospital Unit', 'Karos Health demo healthcareFacilityTypeCodes', 'Hospital Unit'))
    
set(XDSPracticeSettingCode, 
    code('Test Practice Setting Code', 'Test Scheme', 'Test Practice Setting Code'))
    
set(XDSFormatCode, 
    code('DICOM', 'Karos Health demo formatCodes', 'DICOM'))
    
set(XDSConfidentialityCode,
    code('N', 'Karos Health demo confidentialityCodes', 'Normal'))

set(XDSEventCode, code('Test', 'Test', 'Test'))

def localPID = get(PatientID)
   // Sanity Check
   if(null == localPID) {
       log.warn("XDS Metadata creation, manifest submission:\nLocal Patient ID was NULL.  Manifest submission will likely fail!!")
   }

def issuerOfLocalPID = get(IssuerOfPatientID).split('&')[1]
   // Sanity Check
   if(null == issuerOfLocalPID) {
       log.warn("XDS Metadata creation, manifest submission:\nLocal Domain was NULL.  Manifest submission will likely fail!!")
   }

def qualifiedLocalPID = localPID + issuerOfLocalPID
   // Sanity Check
   if(null == qualifiedLocalPID) {
       log.warn("XDS Metadata creation, manifest submission:\nLocal Patient ID and Issuer were *both*  NULL.  Manifest submission will likely fail!!")
   }

log.debug("Debuging XDS Metadata creation, manifest submission: \nLocal Patient ID is \"{}\", and its Issuer is: \"{}\"", localPID, issuerOfLocalPID)
set(XDSSourcePatientID, localPID, issuerOfLocalPID)


def referringPhysician = get(tag(0x0008,0x0090))
    if (null == referringPhysician) {
        log.debug("XDS Metadata creation, manifest submission:\nReferringPhysicianName was null. Not setting referringPhysician in manifest metadata.")
    } else {
        set(XDSExtendedMetadata('referringPhysician'), referringPhysician)
    }

set(XDSTitle, get(StudyDescription))

set(XDSAuthorGivenName, ' ')
