<config>
    <service type="authentication" id="authentication">
        <server idref="authenticated-http" name="web-api">
            <url>${NavigatorAPIBasePath}</url>
        </server>

        <config>
            <include location="userauth.cfg.xml"/>
        </config>
    </service>

    <service type="usermanagement" id="usermanagement">
        <config>
            <prop name="SystemDefaultPermissions">
                <permission name="rialto.arr" value="false"/>
                <permission name="rialto.administration" value="false"/>
                <permission name="rialto.usermanagement.internalrealm" value="false"/>
            </prop>
            <prop name="web-api-path" value="/usermanagement"/>
            <prop name="DatabaseURL" value="${DatabaseURL}"/>
            <prop name="UserPreferencesMode" value="USER_WITH_DEFAULTS"/>

            <include location="defaultuserprefs.cfg.xml"/>
            <include location="userauth.cfg.xml"/>
        </config>
    </service>

    <service type="navigator.server" id="navigator.server">
        <device idref="pix" />          <!-- Optional; one only; corresponding pdq required -->
        <device idref="pdq" />          <!-- Optional; one only; corresponding pix required -->
        <device idref="xdsrep"/>        <!-- Required; one or more per affinity domain -->
        <device idref="xdsreg"/>        <!-- Required; one per affinity domain -->
        <device idref="audit"/>         <!-- Required; one only -->

        <server idref="http" name="web-ui">
            <url>/</url>
        </server>

        <server idref="authenticated-http" name="web-api">
            <url>/public/*</url>
        </server>

        <config>
            <prop name="Cluster">
                    <name>NavigatorServerClusterGroupId</name>
                    <udpstack>
                        <bindAddress>${Navigator-Server-Cluster-Bind-IP}</bindAddress>
                        <multicastAddress>${Navigator-Server-Cluster-Multicast-IP}</multicastAddress>
                        <multicastPort>${Navigator-Server-Cluster-Multicast-PORT}</multicastPort>
                    </udpstack>
            </prop>
            <prop name="CrossAffinityDomainID" value="${XAffinityDomain}"/>
            <!--prop name="ExtendedMetadataAttributes">
                <ExtendedMetadataAttribute displayName="accessionNumber" searchKey="$XDSDocumentEntryAccessionNumber"/>
                <ExtendedMetadataAttribute displayName="referringPhysician" searchKey="$XDSDocumentEntryReferringPhysician"/>
                <ExtendedMetadataAttribute displayName="numberStudyRelatedImages" searchKey="$XDSDocumentEntryNumberStudyRelatedImages"/>
                <ExtendedMetadataAttribute displayName="numberStudyRelatedInstances" searchKey="$XDSDocumentEntryNumberStudyRelatedInstances"/>
                <ExtendedMetadataAttribute displayName="studyDescription" searchKey="$XDSDocumentEntryStudyDescription"/>
                <ExtendedMetadataAttribute displayName="seriesDescription" searchKey="$XDSDocumentEntrySeriesDescription"/>
                <ExtendedMetadataAttribute displayName="studyId" searchKey="$XDSDocumentEntryStudyId"/>
                <ExtendedMetadataAttribute displayName="studyInstanceUID" searchKey="$XDSDocumentEntryStudyInstanceUID"/>
            </prop-->
            <prop name="DocumentTypeCodes">
                <DocumentTypeCode codeValue="DI_EXAM" schemeName="UHN" schemeId="1.2.3.4.5.6.7" displayName="Diagnostic Imaging Exam"/>
                <DocumentTypeCode codeValue="DI_REPORT" schemeName="UHN" schemeId="1.2.3.4.5.6.7" displayName="Diagnostic Imaging Report"/>
            </prop>
            <prop name="BreakTheGlassOptions">
                <BreakTheGlassOption>Emergency access required for patient care.</BreakTheGlassOption>
            </prop>
            <prop name="SystemDefaultPermissions">
                <permission name="rialto.navigator.patients.search.internal" value="true"/>
                <permission name="rialto.navigator.patients.search.external" value="false"/>
                <permission name="rialto.navigator.docs.metadata.view.unknown.internal" value="true"/>
                <permission name="rialto.navigator.docs.metadata.view.unknown.external" value="true"/>
                <permission name="rialto.navigator.docs.metadata.edit.unknown.internal" value="false"/>
                <permission name="rialto.navigator.docs.metadata.edit.unknown.external" value="false"/>
                <permission name="rialto.navigator.docs.view.unknown.internal.always" value="false"/>
                <permission name="rialto.navigator.docs.view.unknown.external.always" value="false"/>
                <permission name="rialto.navigator.docs.view.unknown.internal.onbreakglass" value="true"/>
                <permission name="rialto.navigator.docs.view.unknown.external.onbreakglass" value="true"/>
            </prop>
            <prop name="SupportedThemes">
                hitachi-light
                hitachi-dark
            </prop>
            <prop name="SupportedLocales">
                        en
                        fr
            </prop>
            <prop name="Plugins">
                <plugin>
                    <name>CDA Viewer</name>
                    <description>HL7 CDA (Clinical Document Architecture) Viewer</description>
                    <mimeTypes>text/xml</mimeTypes>
                    <url>${NavigatorHostProtocol}://${NavigatorHost}:${NavigatorAPIHTTPPort}${NavigatorAPIBasePath}/api/plugins/cda</url>
                    <embedded>true</embedded>
                </plugin>

                <plugin>
                    <name>External Viewer (Download)</name>
                    <description>External platform-specific viewer or download to local disk</description>
                    <mimeTypes>*</mimeTypes>
                    <url>${NavigatorHostProtocol}://${NavigatorHost}:${NavigatorAPIHTTPPort}${NavigatorAPIBasePath}/api/plugins/download</url>
                    <embedded>true</embedded>
                </plugin>

                <plugin>
                    <name>Calgary Scientific</name>
                    <description>Embedded viewer for DICOM images</description>
                    <mimeTypes>application/dicom</mimeTypes>
                    <url>${NavigatorHostProtocol}://${NavigatorHost}:${NavigatorAPIHTTPPort}${NavigatorAPIBasePath}/api/plugins/dicom</url>
                    <embedded>true</embedded>
                </plugin>

                <plugin>
                    <name>Calgary Scientific Mobile</name>
                    <description>Mobile DICOM Image Viewer from Calgary Scientific</description>
                    <mimeTypes>application/dicom</mimeTypes>
                    <url>${NavigatorHostProtocol}://${NavigatorHost}:${NavigatorAPIHTTPPort}${NavigatorAPIBasePath}/api/plugins/dicomMobile</url>
                    <embedded>true</embedded>
                </plugin>
            </prop>
            <prop name="MaximumPatientResultSetSize" value="${MaxPatientSearchResults}"/>
            <prop name="MaximumDocumentResultSetSize" value="${MaxDocumentSearchResults}"/>
            <prop name="AllowMetadataUpdates">${MetadataUpdateEnabled}</prop>
            <prop name="ApiURL" value="${NavigatorHostProtocol}://${NavigatorHost}:${NavigatorAPIHTTPPort}${NavigatorAPIBasePath}/api/navigator"/>
            <prop name="ArrURL" value="${NavigatorHostProtocol}://${NavigatorHost}:${AuditGUIHTTPPort}"/>
            <prop name="UserManagementURL" value="${NavigatorHostProtocol}://${NavigatorHost}:${NavigatorAPIHTTPPort}${NavigatorAPIBasePath}"/>
            <prop name="PublicURL" value="${NavigatorHostProtocol}://${NavigatorHost}:${NavigatorAPIHTTPPort}/public"/>
            <prop name="GlobalInactivitySessionTimeout" value="${IdleUserSessionTimeout}" />
            <prop name="ConfidentialityCodeSchemaName">${ConfidentialityCodeSchemaName}</prop>
            <prop name="DatabaseURL" value="${DatabaseURL}"/>
            <prop name="web-api-path" value="/navigator"/>
            <prop name="DefaultTheme" value="${GUIDefaultTheme}"/>
            <prop name="DefaultLocale" value="${GUIDefaultLocale}"/>
            <prop name="UseTargetDomainForPDQ">${IncludeTargetDomainForPDQ}</prop>

            <include location="patientidentitydomains.cfg.xml"/>
            <include location="cacheStorage.cfg.xml"/>
        </config>
    </service>
</config>


